---
title: "VALLEY NATIONAL BANCORP (VLY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>VALLEY NATIONAL BANCORP</td></tr>
    <tr><td>Symbol</td><td>VLY</td></tr>
    <tr><td>Web</td><td><a href="https://www.valley.com">www.valley.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.11 |
| 2020 | 0.44 |
| 2019 | 0.44 |
| 2018 | 0.44 |
| 2017 | 0.44 |
| 2016 | 0.44 |
| 2015 | 0.44 |
| 2014 | 0.44 |
| 2013 | 0.598 |
| 2012 | 0.66 |
| 2011 | 0.698 |
| 2010 | 0.73 |
| 2009 | 0.57 |
| 2008 | 0.81 |
| 2007 | 0.845 |
| 2006 | 0.645 |
| 2005 | 0.885 |
| 2004 | 0.9 |
| 2003 | 0.9 |
| 2002 | 0.94 |
| 2001 | 1.32 |
| 2000 | 0.52 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
