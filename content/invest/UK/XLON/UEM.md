---
title: "Utilico Em.mkts (UEM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Utilico Em.mkts</td></tr>
    <tr><td>Symbol</td><td>UEM</td></tr>
    <tr><td>Web</td><td><a href="https://www.uemtrust.co.uk">www.uemtrust.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 7.58 |
| 2019 | 7.2 |
| 2018 | 7.0 |
| 2017 | 6.65 |
| 2016 | 6.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
