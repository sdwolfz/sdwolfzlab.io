---
title: "SEI INVESTMENT CO (SEIC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SEI INVESTMENT CO</td></tr>
    <tr><td>Symbol</td><td>SEIC</td></tr>
    <tr><td>Web</td><td><a href="https://www.seic.com">www.seic.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.72 |
| 2019 | 0.68 |
| 2018 | 0.63 |
| 2017 | 0.58 |
| 2016 | 0.54 |
| 2015 | 0.5 |
| 2014 | 0.46 |
| 2013 | 0.42 |
| 2012 | 0.63 |
| 2011 | 0.27 |
| 2010 | 0.2 |
| 2009 | 0.25 |
| 2008 | 0.15 |
| 2007 | 0.14 |
| 2006 | 0.24 |
| 2005 | 0.22 |
| 2004 | 0.29 |
| 2003 | 0.07 |
| 2002 | 0.17 |
| 2001 | 0.13 |
| 2000 | 0.44 |
| 1999 | 0.2 |
| 1998 | 0.32 |
| 1997 | 0.28 |
| 1996 | 0.24 |
| 1995 | 0.2 |
| 1994 | 0.16 |
| 1993 | 0.18 |
| 1992 | 0.15 |
| 1991 | 0.125 |
| 1990 | 0.1 |
| 1989 | 0.1 |
| 1988 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
