---
title: "TC PIPELINES (TCP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TC PIPELINES</td></tr>
    <tr><td>Symbol</td><td>TCP</td></tr>
    <tr><td>Web</td><td><a href="http://www.tcpipelineslp.com">www.tcpipelineslp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.6 |
| 2019 | 2.6 |
| 2018 | 2.95 |
| 2017 | 3.88 |
| 2016 | 3.66 |
| 2015 | 3.46 |
| 2014 | 3.3 |
| 2013 | 1.62 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
