---
title: " (ALX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ALX</td></tr>
    <tr><td>Web</td><td><a href="https://www.alx-inc.com">www.alx-inc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 9.0 |
| 2020 | 18.0 |
| 2019 | 18.0 |
| 2018 | 18.0 |
| 2017 | 17.0 |
| 2016 | 16.0 |
| 2015 | 14.0 |
| 2014 | 13.0 |
| 2013 | 5.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
