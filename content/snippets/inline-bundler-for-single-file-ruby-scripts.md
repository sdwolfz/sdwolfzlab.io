---
title: "Inline Bundler for single file Ruby scripts"
date: 2020-10-04T13:58:08Z
type: posts
slug: inline-bundler-for-single-file-ruby-scripts
tags: ["ruby", "development", "productivity"]
---

To set up `Gemfile` functionality do this:

```ruby
#!/usr/bin/env ruby
# frozen_string_literal: true

ENV['GEM_HOME'] = File.join(Dir.pwd, 'vendor', 'ruby')
Gem.clear_paths

require 'bundler/inline'

gemfile do
  source 'https://rubygems.org'

  gem 'minitest'
end
```

Now you can define and test your code in the same file:

```ruby
def quick_maths(a, b)
  a + b
end

require 'minitest/autorun'

describe '#quick_maths' do
  it '1 + 1 = 2' do
    assert_equal(2, quick_maths(1, 1))
  end
end
```
