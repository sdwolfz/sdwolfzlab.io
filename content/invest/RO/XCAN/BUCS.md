---
title: "BUCOVINA SA SCHEIA (BUCS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>BUCOVINA SA SCHEIA</td></tr>
    <tr><td>Symbol</td><td>BUCS</td></tr>
    <tr><td>Web</td><td><a href="https://www.bucovina-sa.ro">www.bucovina-sa.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2010 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
