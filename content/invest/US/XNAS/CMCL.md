---
title: "CALEDONIA MINING CORP PLC (CMCL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CALEDONIA MINING CORP PLC</td></tr>
    <tr><td>Symbol</td><td>CMCL</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.23 |
| 2020 | 0.335 |
| 2019 | 0.275 |
| 2018 | 0.275 |
| 2017 | 0.069 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
