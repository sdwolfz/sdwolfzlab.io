---
title: "Britvic (BVIC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Britvic</td></tr>
    <tr><td>Symbol</td><td>BVIC</td></tr>
    <tr><td>Web</td><td><a href="https://www.britvic.com">www.britvic.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 21.6 |
| 2019 | 30.0 |
| 2018 | 28.2 |
| 2017 | 26.5 |
| 2016 | 24.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
