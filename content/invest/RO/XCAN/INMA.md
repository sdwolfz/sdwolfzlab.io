---
title: "INDUSTRIE MICA PRAHOVA SA (INMA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>INDUSTRIE MICA PRAHOVA SA</td></tr>
    <tr><td>Symbol</td><td>INMA</td></tr>
    <tr><td>Web</td><td><a href="https://www.industriemica.ro">www.industriemica.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
