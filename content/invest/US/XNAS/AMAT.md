---
title: "APPLIED MATERIALS INC (AMAT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>APPLIED MATERIALS INC</td></tr>
    <tr><td>Symbol</td><td>AMAT</td></tr>
    <tr><td>Web</td><td><a href="https://www.appliedmaterials.com">www.appliedmaterials.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.46 |
| 2020 | 0.87 |
| 2019 | 0.83 |
| 2018 | 0.7 |
| 2017 | 0.4 |
| 2016 | 0.4 |
| 2015 | 0.4 |
| 2014 | 0.4 |
| 2013 | 0.39 |
| 2012 | 0.35 |
| 2011 | 0.31 |
| 2010 | 0.27 |
| 2009 | 0.24 |
| 2008 | 0.24 |
| 2007 | 0.23 |
| 2006 | 0.18 |
| 2005 | 0.09 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
