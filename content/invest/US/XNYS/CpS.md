---
title: "CITIGROUP INC DEP SHS REP 1/1000TH INT PFD SER S % (CpS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CITIGROUP INC DEP SHS REP 1/1000TH INT PFD SER S %</td></tr>
    <tr><td>Symbol</td><td>CpS</td></tr>
    <tr><td>Web</td><td><a href="http://www.citigroup.com">www.citigroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
