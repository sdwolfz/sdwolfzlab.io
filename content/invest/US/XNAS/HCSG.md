---
title: "HEALTHCARE SERVICES GROUP INC (HCSG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HEALTHCARE SERVICES GROUP INC</td></tr>
    <tr><td>Symbol</td><td>HCSG</td></tr>
    <tr><td>Web</td><td><a href="https://www.hcsg.com">www.hcsg.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.414 |
| 2020 | 0.813 |
| 2019 | 0.793 |
| 2018 | 0.773 |
| 2017 | 0.753 |
| 2016 | 0.733 |
| 2015 | 0.713 |
| 2014 | 0.693 |
| 2013 | 0.673 |
| 2012 | 0.653 |
| 2011 | 0.633 |
| 2010 | 0.893 |
| 2009 | 0.74 |
| 2008 | 0.58 |
| 2007 | 0.57 |
| 2006 | 0.46 |
| 2005 | 0.365 |
| 2004 | 0.29 |
| 2003 | 0.13 |
| 1991 | 0.09 |
| 1990 | 0.07 |
| 1989 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
