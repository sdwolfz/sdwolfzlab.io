---
title: "Docker compose nginx proxy"
date: 2021-01-23T19:39:33Z
type: posts
slug: docker-compose-nginx-proxy
series: ["Docker Compose"]
tags: ["docker"]
---

1. Create some nginx config files for custom setup:

```sh
mkdir -p ./config/settings/per_host

echo 'client_max_body_size 10m;' > ./config/settings/proxy_wide.conf
echo 'client_max_body_size 20m;' > ./config/settings/per_host/myporject.test
```

2. Save this as your `docker-compose.yml` file:

```yml
#-------------------------------------------------------------------------------
# Version
#-------------------------------------------------------------------------------

version: '2.4'

#-------------------------------------------------------------------------------
# Networks
#-------------------------------------------------------------------------------

networks:
  backend_network:
    driver: bridge
  frontend_network:
    driver: bridge
    name: nginx-reverse-proxy-network

#-------------------------------------------------------------------------------
# Volumes
#-------------------------------------------------------------------------------

volumes:
  proxy_dhparam:
    driver: local
  proxy_certs:
    driver: local
  proxy_html:
    driver: local

#-------------------------------------------------------------------------------
# Logging
#-------------------------------------------------------------------------------

x-default-logging: &logging
  logging:
    options:
      max-size: '12m'
      max-file: '5'
    driver: json-file

#-------------------------------------------------------------------------------
# Services
#-------------------------------------------------------------------------------

services:

  #-----------------------------------------------------------------------------
  # Proxy

  proxy:
    image: jwilder/nginx-proxy:latest
    <<: *logging
    mem_limit: '200m'
    memswap_limit: '500m'
    networks:
      - backend_network
      - frontend_network
    ports:
      - '127.0.0.1:80:80'
      - '127.0.0.1:443:443'
    restart: on-failure
    ulimits:
      nproc: 10000
      nofile: 90000
    volumes:
      - proxy_dhparam:/etc/nginx/dhparam
      - proxy_certs:/etc/nginx/certs
      - proxy_html:/usr/share/nginx/html
      - /var/run/docker.sock:/tmp/docker.sock:ro
      - ./config/settings/proxy_wide.conf:/etc/nginx/conf.d/proxy_wide.conf:ro
      - ./config/settings/per_host:/etc/nginx/vhost.d

  #-----------------------------------------------------------------------------
  # Let's Encrypt

  letsencrypt:
    image: jrcs/letsencrypt-nginx-proxy-companion:latest
    <<: *logging
    mem_limit: '500m'
    memswap_limit: '1g'
    networks:
      - backend_network
    restart: on-failure
    ulimits:
      nproc: 10000
      nofile: 90000
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - ./config/settings/per_host:/etc/nginx/vhost.d
    volumes_from:
      - proxy
```

3. Spin it up with:

```sh
docker-compose up -d
```

4. Connect to the external network in all your other projects:

```yml
#-------------------------------------------------------------------------------
# Networks
#-------------------------------------------------------------------------------

networks:
  backend_network:
    driver: bridge
  frontend_network:
    external: true
    name: nginx-reverse-proxy-network
```

5. Add your vhost to the container environment:

```sh
services:
  web:
    environment:
      VIRTUAL_HOST=myporject.test
      VIRTUAL_PORT=3000
      # LETSENCRYPT_HOST=myporject.test
      # LETSENCRYPT_EMAIL=mail@myproject.test
```

6. Don't forget you edit your hosts file:

```sh
grep -q '127.0.0.1 myproject.test' /etc/hosts ||
  sudo sed -i '$ a 127.0.0.1 myproject.test' /etc/hosts
```
