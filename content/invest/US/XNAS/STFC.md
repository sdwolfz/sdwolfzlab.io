---
title: "STATE AUTO FINANCIAL CORP (STFC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>STATE AUTO FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>STFC</td></tr>
    <tr><td>Web</td><td><a href="https://www.stateauto.com">www.stateauto.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.4 |
| 2019 | 0.4 |
| 2018 | 0.4 |
| 2017 | 0.4 |
| 2016 | 0.4 |
| 2015 | 0.4 |
| 2014 | 0.4 |
| 2013 | 0.4 |
| 2012 | 0.55 |
| 2011 | 0.6 |
| 2010 | 0.6 |
| 2009 | 0.6 |
| 2008 | 0.6 |
| 2007 | 0.5 |
| 2006 | 0.38 |
| 2005 | 0.27 |
| 2004 | 0.17 |
| 2003 | 0.15 |
| 2002 | 0.135 |
| 2001 | 0.125 |
| 2000 | 0.115 |
| 1999 | 0.105 |
| 1998 | 0.14 |
| 1997 | 0.17 |
| 1996 | 0.19 |
| 1995 | 0.21 |
| 1994 | 0.19 |
| 1993 | 0.21 |
| 1992 | 0.3 |
| 1991 | 0.14 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
