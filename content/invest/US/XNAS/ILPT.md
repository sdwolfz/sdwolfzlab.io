---
title: " (ILPT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ILPT</td></tr>
    <tr><td>Web</td><td><a href="https://www.ilptreit.com">www.ilptreit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.66 |
| 2020 | 1.32 |
| 2019 | 1.32 |
| 2018 | 0.93 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
