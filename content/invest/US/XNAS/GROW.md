---
title: "US GLOBAL INVESTORS INC (GROW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>US GLOBAL INVESTORS INC</td></tr>
    <tr><td>Symbol</td><td>GROW</td></tr>
    <tr><td>Web</td><td><a href="https://www.usfunds.com">www.usfunds.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.027 |
| 2020 | 0.03 |
| 2019 | 0.03 |
| 2018 | 0.03 |
| 2017 | 0.03 |
| 2016 | 0.03 |
| 2015 | 0.052 |
| 2014 | 0.06 |
| 2013 | 0.06 |
| 2012 | 0.26 |
| 2011 | 0.24 |
| 2010 | 0.24 |
| 2009 | 0.24 |
| 2008 | 0.24 |
| 2007 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
