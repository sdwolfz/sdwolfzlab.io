---
title: "Quick way to format numbers in Ruby"
date: 2020-09-27T10:58:53Z
type: posts
slug: quick-way-to-format-numbers-in-ruby
tags: ["ruby", "tutorial", "development"]
---

```ruby
number = 12345678
number.to_s.gsub(/\B(?=(...)*\b)/, ',')

# => "12,345,678"
```

This does not work if your number has a negative sign (`-1234`), neither if it
has a decimal separator (`1.234`). I said it was a _quick_ way, not a _good_ way
;).
