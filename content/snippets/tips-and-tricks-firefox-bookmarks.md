---
title: "Tips and Tricks: Firefox bookmarks"
date: 2020-12-27T14:14:55Z
type: posts
slug: tips-and-tricks-firefox-bookmarks
series: ["Tips and Tricks"]
tags: ["firefox", "desktop"]
---

## Toggle

You can show or hide the bookmarks toolbar with `Ctrl + Shift + B`. Uses:

* Gives you more vertical space!
* Allows you to hide your bookmarks when you screen shot/share your browser
  window.

## Density

Right click on a bookmak, choose `Properties` and delete the `Name`. Now you
only have the `Favicon` visible on the bookmarks toolbar.

## Separators

Right click on the space between two bookmarks and select `New Separator`. Great
for organizing clusters.

## Folders

Put everything under a folder to hide verbose bookmarks or to be able to open
all bookmarks inside with a single action.

1. Right click on the bookmarks toolbar and select `New Folder`.
2. Then drag things around to move them.
