---
title: "Micro Focus (MCRO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Micro Focus</td></tr>
    <tr><td>Symbol</td><td>MCRO</td></tr>
    <tr><td>Web</td><td><a href="https://www.microfocus.com">www.microfocus.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 15.5 |
| 2019 | 116.66 |
| 2018 | 151.26 |
| 2017 | 58.33 |
| 2017 | 88.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
