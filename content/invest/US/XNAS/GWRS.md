---
title: "GLOBAL WATER RESOURCES INC (GWRS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GLOBAL WATER RESOURCES INC</td></tr>
    <tr><td>Symbol</td><td>GWRS</td></tr>
    <tr><td>Web</td><td><a href="https://www.gwresources.com">www.gwresources.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.122 |
| 2020 | 0.289 |
| 2019 | 0.287 |
| 2018 | 0.284 |
| 2017 | 0.274 |
| 2016 | 0.172 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
