---
title: "SANDERSON FARM INC (SAFM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SANDERSON FARM INC</td></tr>
    <tr><td>Symbol</td><td>SAFM</td></tr>
    <tr><td>Web</td><td><a href="https://www.sandersonfarms.com">www.sandersonfarms.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.88 |
| 2020 | 1.4 |
| 2019 | 1.28 |
| 2018 | 1.28 |
| 2017 | 2.04 |
| 2016 | 1.9 |
| 2015 | 1.38 |
| 2014 | 1.32 |
| 2013 | 0.71 |
| 2012 | 0.68 |
| 2011 | 0.68 |
| 2010 | 0.62 |
| 2009 | 0.57 |
| 2008 | 0.56 |
| 2007 | 0.5 |
| 2006 | 0.48 |
| 2005 | 0.42 |
| 2004 | 0.88 |
| 2003 | 0.92 |
| 2002 | 0.4 |
| 2001 | 0.2 |
| 2000 | 0.2 |
| 1999 | 0.2 |
| 1998 | 0.2 |
| 1997 | 0.15 |
| 1996 | 0.2 |
| 1995 | 0.225 |
| 1994 | 0.3 |
| 1993 | 0.3 |
| 1992 | 0.3 |
| 1991 | 0.3 |
| 1990 | 0.3 |
| 1989 | 1.938 |
| 1988 | 0.063 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
