---
title: "Luceco (LUCE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Luceco</td></tr>
    <tr><td>Symbol</td><td>LUCE</td></tr>
    <tr><td>Web</td><td><a href="https://www.luceco.com">www.luceco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.6 |
| 2018 | 0.6 |
| 2017 | 0.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
