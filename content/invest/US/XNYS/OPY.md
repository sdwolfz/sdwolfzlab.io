---
title: "OPPENHEIMER HOLDINGS INC (OPY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>OPPENHEIMER HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>OPY</td></tr>
    <tr><td>Web</td><td><a href="https://www.oppenheimer.com">www.oppenheimer.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.24 |
| 2020 | 1.48 |
| 2019 | 0.46 |
| 2018 | 0.44 |
| 2017 | 0.44 |
| 2016 | 0.44 |
| 2015 | 0.44 |
| 2014 | 0.44 |
| 2013 | 0.22 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
