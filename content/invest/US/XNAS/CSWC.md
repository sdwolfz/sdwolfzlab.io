---
title: "CAPITAL SOUTHWEST CORPORATION (CSWC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CAPITAL SOUTHWEST CORPORATION</td></tr>
    <tr><td>Symbol</td><td>CSWC</td></tr>
    <tr><td>Web</td><td><a href="https://www.capitalsouthwest.com">www.capitalsouthwest.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.52 |
| 2020 | 2.04 |
| 2019 | 2.72 |
| 2018 | 2.07 |
| 2017 | 1.16 |
| 2016 | 0.38 |
| 2015 | 0.1 |
| 2014 | 0.2 |
| 2013 | 3.25 |
| 2012 | 18.39 |
| 2011 | 0.8 |
| 2010 | 0.8 |
| 2009 | 0.8 |
| 2008 | 3.26 |
| 2007 | 0.6 |
| 2006 | 0.6 |
| 2005 | 0.6 |
| 2004 | 0.6 |
| 2003 | 0.6 |
| 2002 | 0.6 |
| 2001 | 0.6 |
| 2000 | 0.6 |
| 1999 | 0.6 |
| 1998 | 0.6 |
| 1997 | 0.6 |
| 1996 | 0.6 |
| 1995 | 0.6 |
| 1994 | 0.6 |
| 1993 | 0.6 |
| 1992 | 0.6 |
| 1991 | 0.6 |
| 1990 | 0.5 |
| 1989 | 1.44 |
| 1988 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
