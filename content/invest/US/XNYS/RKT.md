---
title: "ROCKET COMPANIES INC (RKT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ROCKET COMPANIES INC</td></tr>
    <tr><td>Symbol</td><td>RKT</td></tr>
    <tr><td>Web</td><td><a href="https://ir.rocketcompanies.com">ir.rocketcompanies.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 0.509 |
| 2014 | 1.063 |
| 2013 | 0.65 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
