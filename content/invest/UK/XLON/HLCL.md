---
title: "Helical Bar (HLCL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Helical Bar</td></tr>
    <tr><td>Symbol</td><td>HLCL</td></tr>
    <tr><td>Web</td><td><a href="https://www.helical.co.uk">www.helical.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 8.7 |
| 2019 | 10.1 |
| 2018 | 9.5 |
| 2017 | 8.6 |
| 2016 | 8.17 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
