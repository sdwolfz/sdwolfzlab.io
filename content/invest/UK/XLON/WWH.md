---
title: "Worldwide Healthcare Trust (WWH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Worldwide Healthcare Trust</td></tr>
    <tr><td>Symbol</td><td>WWH</td></tr>
    <tr><td>Web</td><td><a href="https://www.worldwidewh.com">www.worldwidewh.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 25.0 |
| 2019 | 26.5 |
| 2018 | 17.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
