---
title: "CAMBRIDGE BANCORP (CATC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CAMBRIDGE BANCORP</td></tr>
    <tr><td>Symbol</td><td>CATC</td></tr>
    <tr><td>Web</td><td><a href="https://www.cambridgetrust.com">www.cambridgetrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.16 |
| 2020 | 2.12 |
| 2019 | 2.04 |
| 2018 | 1.96 |
| 2017 | 0.47 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
