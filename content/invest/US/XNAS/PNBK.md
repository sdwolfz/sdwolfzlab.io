---
title: "PATRIOT NATL BANCP (PNBK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PATRIOT NATL BANCP</td></tr>
    <tr><td>Symbol</td><td>PNBK</td></tr>
    <tr><td>Web</td><td><a href="https://www.bankpatriot.com">www.bankpatriot.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.04 |
| 2018 | 0.04 |
| 2017 | 0.02 |
| 2009 | 0.045 |
| 2008 | 0.135 |
| 2007 | 0.18 |
| 2006 | 0.175 |
| 2005 | 0.155 |
| 2004 | 0.165 |
| 2003 | 0.085 |
| 2002 | 0.095 |
| 2001 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
