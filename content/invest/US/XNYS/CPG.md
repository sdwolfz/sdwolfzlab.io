---
title: "CRESCENT POINT ENERGY CORP (CPG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CRESCENT POINT ENERGY CORP</td></tr>
    <tr><td>Symbol</td><td>CPG</td></tr>
    <tr><td>Web</td><td><a href="https://www.crescentpointenergy.com">www.crescentpointenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.013 |
| 2019 | 0.031 |
| 2018 | 0.278 |
| 2017 | 0.301 |
| 2016 | 0.4 |
| 2015 | 1.876 |
| 2014 | 2.76 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
