---
title: "URBANA SA BISTRITA (URBA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>URBANA SA BISTRITA</td></tr>
    <tr><td>Symbol</td><td>URBA</td></tr>
    <tr><td>Web</td><td><a href="https://www.urbana.ro">www.urbana.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 10.0 |
| 2013 | 5.0 |
| 2009 | 2.2503 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
