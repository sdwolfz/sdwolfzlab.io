---
title: " (KRC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>KRC</td></tr>
    <tr><td>Web</td><td><a href="https://www.kilroyrealty.com">www.kilroyrealty.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.5 |
| 2020 | 1.97 |
| 2019 | 1.91 |
| 2018 | 1.79 |
| 2017 | 1.65 |
| 2016 | 3.375 |
| 2015 | 1.4 |
| 2014 | 1.4 |
| 2013 | 1.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
