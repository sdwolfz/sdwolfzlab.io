---
title: "KINGSTONE COMPANIES INC (KINS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>KINGSTONE COMPANIES INC</td></tr>
    <tr><td>Symbol</td><td>KINS</td></tr>
    <tr><td>Web</td><td><a href="https://www.kingstonecompanies.com">www.kingstonecompanies.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.08 |
| 2020 | 0.183 |
| 2019 | 0.325 |
| 2018 | 0.4 |
| 2017 | 0.303 |
| 2016 | 0.25 |
| 2015 | 0.213 |
| 2014 | 0.18 |
| 2013 | 0.16 |
| 2012 | 0.14 |
| 2011 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
