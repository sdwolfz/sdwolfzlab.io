---
title: "FERGUSON PLC (FERG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FERGUSON PLC</td></tr>
    <tr><td>Symbol</td><td>FERG</td></tr>
    <tr><td>Web</td><td><a href="https://www.fergusonplc.com">www.fergusonplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.529 |
| 2020 | 0.178 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
