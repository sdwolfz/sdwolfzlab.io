---
title: "ATN INTERNATIONAL INC (ATNI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ATN INTERNATIONAL INC</td></tr>
    <tr><td>Symbol</td><td>ATNI</td></tr>
    <tr><td>Web</td><td><a href="https://www.atni.com">www.atni.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.17 |
| 2020 | 0.68 |
| 2019 | 0.68 |
| 2018 | 0.68 |
| 2017 | 1.02 |
| 2016 | 1.32 |
| 2015 | 1.22 |
| 2014 | 1.12 |
| 2013 | 1.04 |
| 2012 | 0.96 |
| 2011 | 0.9 |
| 2010 | 0.84 |
| 2009 | 0.76 |
| 2008 | 0.84 |
| 2007 | 0.44 |
| 2006 | 0.7 |
| 2005 | 1.15 |
| 2004 | 1.048 |
| 2003 | 0.95 |
| 2002 | 0.85 |
| 2001 | 0.975 |
| 2000 | 0.525 |
| 1999 | 0.3 |
| 1993 | 0.2 |
| 1992 | 0.32 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
