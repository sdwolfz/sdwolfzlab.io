---
title: "NEMAURA MEDICAL INC (NMRD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NEMAURA MEDICAL INC</td></tr>
    <tr><td>Symbol</td><td>NMRD</td></tr>
    <tr><td>Web</td><td><a href="https://www.nemauramedical.com">www.nemauramedical.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
