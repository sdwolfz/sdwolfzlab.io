---
title: "SANDY SPRING BANCORP INC (SASR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SANDY SPRING BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>SASR</td></tr>
    <tr><td>Web</td><td><a href="https://www.sandyspringbank.com">www.sandyspringbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.64 |
| 2020 | 1.2 |
| 2019 | 1.18 |
| 2018 | 1.1 |
| 2017 | 1.04 |
| 2016 | 0.98 |
| 2015 | 0.9 |
| 2014 | 0.76 |
| 2013 | 0.64 |
| 2012 | 0.48 |
| 2011 | 0.34 |
| 2010 | 0.04 |
| 2009 | 0.37 |
| 2008 | 0.96 |
| 2007 | 0.92 |
| 2006 | 0.88 |
| 2005 | 0.84 |
| 2004 | 0.78 |
| 2003 | 0.74 |
| 2002 | 0.69 |
| 2001 | 0.91 |
| 2000 | 0.81 |
| 1999 | 0.75 |
| 1998 | 0.63 |
| 1997 | 0.94 |
| 1996 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
