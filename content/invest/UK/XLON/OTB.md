---
title: "On The Beach (OTB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>On The Beach</td></tr>
    <tr><td>Symbol</td><td>OTB</td></tr>
    <tr><td>Web</td><td><a href="https://www.onthebeach.co.uk">www.onthebeach.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 3.3 |
| 2018 | 3.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
