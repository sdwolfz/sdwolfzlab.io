---
title: "Stvg (STVG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Stvg</td></tr>
    <tr><td>Symbol</td><td>STVG</td></tr>
    <tr><td>Web</td><td><a href="https://www.stvplc.tv">www.stvplc.tv</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 21.0 |
| 2018 | 20.0 |
| 2017 | 17.0 |
| 2016 | 15.0 |
| 2015 | 10.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
