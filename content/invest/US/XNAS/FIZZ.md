---
title: "NATIONAL BEVERAGE CORP (FIZZ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NATIONAL BEVERAGE CORP</td></tr>
    <tr><td>Symbol</td><td>FIZZ</td></tr>
    <tr><td>Web</td><td><a href="https://www.nationalbeverage.com">www.nationalbeverage.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.0 |
| 2018 | 2.9 |
| 2017 | 1.5 |
| 2016 | 1.5 |
| 2012 | 2.55 |
| 2010 | 3.65 |
| 2007 | 0.8 |
| 2006 | 1.0 |
| 2004 | 1.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
