---
title: "Vpc Specialty (VSL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Vpc Specialty</td></tr>
    <tr><td>Symbol</td><td>VSL</td></tr>
    <tr><td>Web</td><td><a href="https://www.victoryparkcapital.com">www.victoryparkcapital.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 8.0 |
| 2018 | 8.0 |
| 2017 | 6.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
