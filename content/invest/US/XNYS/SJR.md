---
title: "SHAW COMMUNICATIONS INC (SJR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SHAW COMMUNICATIONS INC</td></tr>
    <tr><td>Symbol</td><td>SJR</td></tr>
    <tr><td>Web</td><td><a href="https://www.shaw.ca">www.shaw.ca</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.078 |
| 2020 | 0.885 |
| 2019 | 0.891 |
| 2018 | 0.919 |
| 2017 | 0.909 |
| 2016 | 0.827 |
| 2015 | 1.0 |
| 2014 | 1.09 |
| 2013 | 0.34 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
