---
title: "GLADSTONE CAPITAL CORP (GLAD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GLADSTONE CAPITAL CORP</td></tr>
    <tr><td>Symbol</td><td>GLAD</td></tr>
    <tr><td>Web</td><td><a href="https://www.gladstonecapital.com">www.gladstonecapital.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.39 |
| 2020 | 0.795 |
| 2019 | 0.84 |
| 2018 | 0.84 |
| 2017 | 0.84 |
| 2016 | 0.84 |
| 2015 | 0.84 |
| 2014 | 0.84 |
| 2013 | 0.84 |
| 2012 | 0.84 |
| 2011 | 0.84 |
| 2010 | 0.84 |
| 2009 | 1.05 |
| 2008 | 1.68 |
| 2007 | 1.68 |
| 2006 | 1.65 |
| 2005 | 1.56 |
| 2004 | 1.395 |
| 2003 | 1.2 |
| 2002 | 0.86 |
| 2001 | 0.38 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
