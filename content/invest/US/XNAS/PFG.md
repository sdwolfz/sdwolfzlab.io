---
title: "PRINCIPAL FINANCIAL GROUP (PFG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PRINCIPAL FINANCIAL GROUP</td></tr>
    <tr><td>Symbol</td><td>PFG</td></tr>
    <tr><td>Web</td><td><a href="https://www.principal.com">www.principal.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.17 |
| 2020 | 2.24 |
| 2019 | 2.18 |
| 2018 | 2.1 |
| 2017 | 1.87 |
| 2016 | 1.61 |
| 2015 | 1.5 |
| 2014 | 1.28 |
| 2013 | 0.98 |
| 2012 | 0.78 |
| 2011 | 0.7 |
| 2010 | 0.55 |
| 2009 | 0.5 |
| 2008 | 0.45 |
| 2007 | 0.9 |
| 2006 | 0.8 |
| 2005 | 0.65 |
| 2004 | 0.55 |
| 2003 | 0.45 |
| 2002 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
