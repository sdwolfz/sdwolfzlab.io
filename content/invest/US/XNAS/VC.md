---
title: "VISTEON CORPORATION (VC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>VISTEON CORPORATION</td></tr>
    <tr><td>Symbol</td><td>VC</td></tr>
    <tr><td>Web</td><td><a href="https://www.visteon.com">www.visteon.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 43.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
