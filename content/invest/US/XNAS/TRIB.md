---
title: " (TRIB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>TRIB</td></tr>
    <tr><td>Web</td><td><a href="https://www.trinitybiotech.com">www.trinitybiotech.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 0.22 |
| 2014 | 0.22 |
| 2013 | 0.2 |
| 2012 | 0.15 |
| 2011 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
