---
title: "MERCADOLIBRE INC (MELI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MERCADOLIBRE INC</td></tr>
    <tr><td>Symbol</td><td>MELI</td></tr>
    <tr><td>Web</td><td><a href="https://www.mercadolibre.com">www.mercadolibre.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 0.6 |
| 2016 | 0.6 |
| 2015 | 0.412 |
| 2014 | 0.664 |
| 2013 | 0.572 |
| 2012 | 0.436 |
| 2011 | 0.32 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
