---
title: "Synthomer (SYNT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Synthomer</td></tr>
    <tr><td>Symbol</td><td>SYNT</td></tr>
    <tr><td>Web</td><td><a href="https://www.synthomer.com">www.synthomer.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 10.9 |
| 2018 | 13.1 |
| 2017 | 12.2 |
| 2016 | 11.3 |
| 2015 | 8.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
