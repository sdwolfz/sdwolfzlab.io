---
title: "CLOROX CO (CLX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CLOROX CO</td></tr>
    <tr><td>Symbol</td><td>CLX</td></tr>
    <tr><td>Web</td><td><a href="https://www.thecloroxcompany.com">www.thecloroxcompany.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.22 |
| 2020 | 4.34 |
| 2019 | 4.04 |
| 2018 | 3.72 |
| 2017 | 3.28 |
| 2016 | 3.14 |
| 2015 | 3.02 |
| 2014 | 2.9 |
| 2013 | 1.42 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
