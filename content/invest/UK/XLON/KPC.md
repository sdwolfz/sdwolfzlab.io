---
title: "Keystone Pos. (KPC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Keystone Pos.</td></tr>
    <tr><td>Symbol</td><td>KPC</td></tr>
    <tr><td>Web</td><td><a href="https://www.invesco.co.uk">www.invesco.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 11.2 |
| 2019 | 11.93 |
| 2018 | 11.55 |
| 2017 | 11.94 |
| 2016 | 11.66 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
