---
title: "PROCTER & GAMBLE CO (PG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PROCTER & GAMBLE CO</td></tr>
    <tr><td>Symbol</td><td>PG</td></tr>
    <tr><td>Web</td><td><a href="https://www.pginvestor.com">www.pginvestor.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.661 |
| 2020 | 3.119 |
| 2019 | 2.955 |
| 2018 | 2.841 |
| 2017 | 2.739 |
| 2016 | 2.67 |
| 2015 | 2.633 |
| 2014 | 2.533 |
| 2013 | 1.202 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
