---
title: "PACIFIC MERCANTILE BANCORP (PMBC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PACIFIC MERCANTILE BANCORP</td></tr>
    <tr><td>Symbol</td><td>PMBC</td></tr>
    <tr><td>Web</td><td><a href="https://www.pmbank.com">www.pmbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2008 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
