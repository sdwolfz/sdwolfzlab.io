---
title: "HENNESSY CAPITAL INVESTMENT CORP V (HCIC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HENNESSY CAPITAL INVESTMENT CORP V</td></tr>
    <tr><td>Symbol</td><td>HCIC</td></tr>
    <tr><td>Web</td><td><a href="https://www.hennessycapllc.com">www.hennessycapllc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
