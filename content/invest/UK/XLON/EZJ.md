---
title: "easyJet (EZJ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>easyJet</td></tr>
    <tr><td>Symbol</td><td>EZJ</td></tr>
    <tr><td>Web</td><td><a href="https://www.easyjet.com">www.easyjet.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 43.9 |
| 2018 | 58.6 |
| 2017 | 40.9 |
| 2016 | 53.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
