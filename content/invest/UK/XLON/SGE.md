---
title: "Sage Group (SGE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Sage Group</td></tr>
    <tr><td>Symbol</td><td>SGE</td></tr>
    <tr><td>Web</td><td><a href="https://www.sage.com">www.sage.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 17.25 |
| 2019 | 16.91 |
| 2018 | 16.5 |
| 2017 | 15.42 |
| 2016 | 14.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
