---
title: "Mid Wynd International Investment Trust (MWY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Mid Wynd International Investment Trust</td></tr>
    <tr><td>Symbol</td><td>MWY</td></tr>
    <tr><td>Web</td><td><a href="https://www.midwynd.com">www.midwynd.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.12 |
| 2019 | 5.83 |
| 2018 | 5.55 |
| 2017 | 5.0 |
| 2016 | 4.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
