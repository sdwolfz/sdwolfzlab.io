---
title: "GENERAL MOTORS CO (GM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GENERAL MOTORS CO</td></tr>
    <tr><td>Symbol</td><td>GM</td></tr>
    <tr><td>Web</td><td><a href="https://www.gm.com">www.gm.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.38 |
| 2019 | 1.52 |
| 2018 | 1.52 |
| 2017 | 1.52 |
| 2016 | 1.52 |
| 2015 | 1.38 |
| 2014 | 1.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
