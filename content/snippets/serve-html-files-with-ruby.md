---
title: "Serve HTML files with Ruby"
date: 2020-07-13T20:44:08Z
type: posts
slug: serve-html-files-with-ruby
tags: ["ruby", "linux", "python"]
---

Python has:

```sh
python3 -m http.server
```

But I like Ruby, so I do this instead:

```sh
# Basic:
ruby -run -e httpd

# With arguments:
ruby -run -e httpd -- -b 0.0.0.0 -p 9090
```

> Note: there is no such thing as a `-run` flag for ruby, you're doing `-r` for
> `require` and you're loading the `un.rb` file from the ruby core. Then `-e` to
> evaluate the `httpd` function in that file.
