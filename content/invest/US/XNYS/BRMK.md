---
title: "BROADMARK REALTY CAPITAL INC (BRMK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BROADMARK REALTY CAPITAL INC</td></tr>
    <tr><td>Symbol</td><td>BRMK</td></tr>
    <tr><td>Web</td><td><a href="https://www.broadmark.com">www.broadmark.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 0.78 |
| 2019 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
