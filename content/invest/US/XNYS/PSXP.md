---
title: " (PSXP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>PSXP</td></tr>
    <tr><td>Web</td><td><a href="https://www.phillips66partners.com">www.phillips66partners.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.75 |
| 2020 | 3.5 |
| 2019 | 3.4 |
| 2018 | 2.936 |
| 2017 | 2.405 |
| 2016 | 1.975 |
| 2015 | 1.538 |
| 2014 | 1.118 |
| 2013 | 0.155 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
