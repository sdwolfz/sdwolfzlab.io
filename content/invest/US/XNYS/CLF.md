---
title: "CLEVELAND CLIFFS INC (CLF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CLEVELAND CLIFFS INC</td></tr>
    <tr><td>Symbol</td><td>CLF</td></tr>
    <tr><td>Web</td><td><a href="https://www.clevelandcliffs.com">www.clevelandcliffs.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.12 |
| 2019 | 0.26 |
| 2014 | 0.6 |
| 2013 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
