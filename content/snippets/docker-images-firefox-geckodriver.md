---
title: "Docker Images: Firefox with Geckodriver"
date: 2020-08-01T18:42:55Z
type: posts
slug: docker-images-firefox-geckodriver
series: ["Docker Images"]
tags: ["docker", "firefox", "linux"]
---

```sh
# ------------------------------------------------------------------------------
# Base

FROM alpine:3.12.1

# ------------------------------------------------------------------------------
# Packages

RUN set -exo pipefail             && \
                                     \
    echo 'Install firefox'        && \
    apk add --update --no-cache \
      firefox

RUN set -exo pipefail                                                    && \
                                                                            \
    echo "Installing geckodriver"                                        && \
                                                                            \
    SHA=61bfc547a623d7305256611a81ecd24e6bf9dac555529ed6baeafcf8160900da && \
    VERSION=v0.28.0                                                      && \
    PACKAGE=geckodriver-$VERSION-linux64.tar.gz                          && \
    BASE=https://github.com/mozilla/geckodriver/releases/download        && \
    DOWNLOAD=$BASE/$VERSION/$PACKAGE                                     && \
                                                                            \
    wget -O "$PACKAGE" "$DOWNLOAD"                                       && \
    sha256sum "$PACKAGE"                                                 && \
    echo "$SHA  $PACKAGE" | sha256sum -c -                               && \
    tar -xzf "$PACKAGE" -C /usr/local/bin                                && \
    rm -rf "$PACKAGE"

# ------------------------------------------------------------------------------
# Command

CMD ["/bin/sh"]
```
