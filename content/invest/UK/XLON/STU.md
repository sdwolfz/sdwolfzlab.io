---
title: "Studio Ret (STU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Studio Ret</td></tr>
    <tr><td>Symbol</td><td>STU</td></tr>
    <tr><td>Web</td><td><a href="https://www.studioretail.group">www.studioretail.group</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
