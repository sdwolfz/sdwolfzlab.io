---
title: "CONSOLIDATED WATER CO (CWCO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CONSOLIDATED WATER CO</td></tr>
    <tr><td>Symbol</td><td>CWCO</td></tr>
    <tr><td>Web</td><td><a href="https://www.cwco.com">www.cwco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.085 |
| 2020 | 0.34 |
| 2019 | 0.34 |
| 2018 | 0.425 |
| 2017 | 0.225 |
| 2016 | 0.3 |
| 2015 | 0.3 |
| 2014 | 0.3 |
| 2013 | 0.3 |
| 2012 | 0.3 |
| 2011 | 0.3 |
| 2010 | 0.3 |
| 2009 | 0.28 |
| 2008 | 0.325 |
| 2007 | 0.195 |
| 2006 | 0.24 |
| 2005 | 0.355 |
| 2004 | 0.46 |
| 2003 | 0.42 |
| 2002 | 0.42 |
| 2001 | 0.3 |
| 2000 | 0.26 |
| 1999 | 0.2 |
| 1998 | 0.08 |
| 1997 | 0.07 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
