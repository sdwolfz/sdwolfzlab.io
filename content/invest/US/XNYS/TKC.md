---
title: " (TKC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>TKC</td></tr>
    <tr><td>Web</td><td><a href="https://www.turkcell.com.tr">www.turkcell.com.tr</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.08 |
| 2020 | 0.08 |
| 2019 | 0.18 |
| 2018 | 0.323 |
| 2017 | 0.781 |
| 2015 | 1.435 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
