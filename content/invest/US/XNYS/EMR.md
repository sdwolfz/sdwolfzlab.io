---
title: "EMERSON ELECTRIC CO (EMR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>EMERSON ELECTRIC CO</td></tr>
    <tr><td>Symbol</td><td>EMR</td></tr>
    <tr><td>Web</td><td><a href="https://www.emerson.com">www.emerson.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.01 |
| 2020 | 2.005 |
| 2019 | 1.97 |
| 2018 | 1.945 |
| 2017 | 1.925 |
| 2016 | 1.905 |
| 2015 | 1.885 |
| 2014 | 1.76 |
| 2013 | 0.84 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
