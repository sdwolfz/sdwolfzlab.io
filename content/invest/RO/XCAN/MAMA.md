---
title: "MAGAZIN UNIVERSAL MARAMURES SA BAIA MARE (MAMA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>MAGAZIN UNIVERSAL MARAMURES SA BAIA MARE</td></tr>
    <tr><td>Symbol</td><td>MAMA</td></tr>
    <tr><td>Web</td><td><a href="https://www.maramuresul.ro">www.maramuresul.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
