---
title: "ANTHEM INC (ANTM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ANTHEM INC</td></tr>
    <tr><td>Symbol</td><td>ANTM</td></tr>
    <tr><td>Web</td><td><a href="https://www.antheminc.com">www.antheminc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.26 |
| 2020 | 3.8 |
| 2019 | 3.2 |
| 2018 | 3.0 |
| 2017 | 2.7 |
| 2016 | 2.6 |
| 2015 | 2.5 |
| 2014 | 0.438 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
