---
title: " (PSA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>PSA</td></tr>
    <tr><td>Web</td><td><a href="https://www.publicstorage.com">www.publicstorage.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 4.0 |
| 2020 | 8.0 |
| 2019 | 8.0 |
| 2018 | 8.0 |
| 2017 | 8.0 |
| 2016 | 7.3 |
| 2015 | 6.5 |
| 2014 | 5.6 |
| 2013 | 2.65 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
