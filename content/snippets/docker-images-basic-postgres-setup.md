---
title: "Docker Images: basic Postgres setup"
date: 2020-12-22T14:31:50Z
type: posts
slug: docker-images-basic-postgres-setup
series: ["Docker Images"]
tags: ["docker", "ruby", "postgres", "productivity", "development"]
draft: true
---

```dockerfile
FROM postgres:10.7-alpine

ARG HOST_USER_UID=1000
ARG HOST_USER_GID=1000

ADD ./docker/postgres/entry /

RUN set -ex                                            && \
                                                          \
    echo 'Creating the git user and group from host'   && \
    addgroup -g $HOST_USER_GID -S notroot              && \
    adduser -u $HOST_USER_UID -G notroot -D notroot    && \
    adduser notroot postgres                           && \
                                                          \
    echo 'Giving permissions to files and directories' && \
    chmod +x /entry                                    && \
    mkdir -p                                              \
      /var/lib/postgresql/data                            \
      /var/lib/postgresql/upgrade                      && \
    chown -R notroot:notroot                              \
      /var/lib/postgresql/upgrade

# Uses the old entrypoint as a parameter to the new one.
ENTRYPOINT ["/entry", "docker-entrypoint.sh"]

# Aparently you have to redefine the CMD from the parent image!
CMD ["postgres"]
```

Build it like this:

```sh
docker build \
  --build-arg=HOST_USER_UID=`id -u` \
  --build-arg=HOST_USER_GID=`id -g` \
  -t postgres-for-project \
  "$PWD"
```

Or in `docker-compose` using:

```yml
---

services:
  app:
    build:
      args:
        HOST_USER_UID: ${HOST_USER_UID}
        HOST_USER_GID: ${HOST_USER_GID}
      context: .
      dockerfile: ./Dockerfile
    healthcheck:
      test: pg_isready -U notroot -h localhost -p 5432 -d notroot || exit 1
      interval: "10s"
      retries: 20
      start_period: "10s"
      timeout: "7s"
```
