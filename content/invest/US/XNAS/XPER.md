---
title: "XPERI HOLDING CORPORATION (XPER)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>XPERI HOLDING CORPORATION</td></tr>
    <tr><td>Symbol</td><td>XPER</td></tr>
    <tr><td>Web</td><td><a href="https://www.xperi.com">www.xperi.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.5 |
| 2019 | 0.8 |
| 2018 | 0.8 |
| 2017 | 0.8 |
| 2016 | 0.8 |
| 2015 | 0.8 |
| 2014 | 0.92 |
| 2013 | 0.7 |
| 2012 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
