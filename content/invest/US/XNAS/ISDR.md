---
title: "ISSUER DIRECT CORPORATION (ISDR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ISSUER DIRECT CORPORATION</td></tr>
    <tr><td>Symbol</td><td>ISDR</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.15 |
| 2017 | 0.2 |
| 2016 | 0.16 |
| 2015 | 0.03 |
| 2013 | 0.06 |
| 2012 | 0.14 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
