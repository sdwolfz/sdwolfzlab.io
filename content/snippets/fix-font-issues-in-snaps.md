---
title: "Fix font issues in Snaps"
date: 2020-06-29T18:09:53Z
type: posts
slug: fix-font-issues-in-snaps
tags: ["snap", "linux"]
---

Website: https://snapcraft.io

Snaps are amazing, I don't understand why people hate them, tell me another way
you can reliably install and update apps like these with a single command:

- `acrordrdc` (Adobe Acrobat Reader)
- `androidsdk` (Android SDK)
- `drawio` (Diagrams)
- `freecad` (CAD)
- `heroku` (PAAS)
- `signal-desktop` (Chat)

In the linux world this is unheard of!

But sometimes you get font issues, where all characters are squares; here's how
you clear them:

```sh
sudo rm -v /var/cache/fontconfig/*
rm ~/.cache/fontconfig/*
rm ~/snap/*/common/.cache/fontconfig/*

sudo fc-cache -r
fc-cache -r
```
