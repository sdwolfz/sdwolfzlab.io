---
title: "NORDSTROM INC (JWN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NORDSTROM INC</td></tr>
    <tr><td>Symbol</td><td>JWN</td></tr>
    <tr><td>Web</td><td><a href="https://www.Nordstrom.com">www.Nordstrom.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.37 |
| 2019 | 1.48 |
| 2018 | 1.48 |
| 2017 | 1.48 |
| 2016 | 1.48 |
| 2015 | 1.48 |
| 2014 | 1.32 |
| 2013 | 0.9 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
