---
title: "CAMPBELL SOUP CO (CPB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CAMPBELL SOUP CO</td></tr>
    <tr><td>Symbol</td><td>CPB</td></tr>
    <tr><td>Web</td><td><a href="https://www.campbellsoupcompany.com">www.campbellsoupcompany.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.74 |
| 2020 | 1.4 |
| 2019 | 1.4 |
| 2018 | 1.4 |
| 2017 | 1.4 |
| 2016 | 1.286 |
| 2015 | 1.248 |
| 2014 | 1.248 |
| 2013 | 0.602 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
