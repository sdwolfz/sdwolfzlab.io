---
title: "TYSON FOODS INC (TSN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TYSON FOODS INC</td></tr>
    <tr><td>Symbol</td><td>TSN</td></tr>
    <tr><td>Web</td><td><a href="https://www.tysonfoods.com">www.tysonfoods.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.89 |
| 2020 | 1.705 |
| 2019 | 1.545 |
| 2018 | 1.275 |
| 2017 | 0.975 |
| 2016 | 0.6 |
| 2015 | 0.4 |
| 2014 | 0.3 |
| 2013 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
