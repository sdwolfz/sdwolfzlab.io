---
title: "Henderson Smaller Companies Trust (HSL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Henderson Smaller Companies Trust</td></tr>
    <tr><td>Symbol</td><td>HSL</td></tr>
    <tr><td>Web</td><td><a href="https://www.hendersonsmallercompanies.com">www.hendersonsmallercompanies.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 23.5 |
| 2019 | 23.0 |
| 2018 | 21.0 |
| 2017 | 18.0 |
| 2016 | 15.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
