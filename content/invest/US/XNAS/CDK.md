---
title: "CDK GLOBAL INC (CDK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CDK GLOBAL INC</td></tr>
    <tr><td>Symbol</td><td>CDK</td></tr>
    <tr><td>Web</td><td><a href="https://www.cdkglobal.com">www.cdkglobal.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.15 |
| 2020 | 0.6 |
| 2019 | 0.6 |
| 2018 | 0.6 |
| 2017 | 0.57 |
| 2016 | 0.545 |
| 2015 | 0.495 |
| 2014 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
