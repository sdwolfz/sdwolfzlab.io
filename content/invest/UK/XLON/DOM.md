---
title: "Dominos (DOM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Dominos</td></tr>
    <tr><td>Symbol</td><td>DOM</td></tr>
    <tr><td>Web</td><td><a href="https://corporate.dominos.co.uk">corporate.dominos.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 9.76 |
| 2017 | 9.0 |
| 2016 | 8.0 |
| 2015 | 6.91 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
