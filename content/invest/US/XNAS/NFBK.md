---
title: "NORTHFIELD BANCORP INC (NFBK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NORTHFIELD BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>NFBK</td></tr>
    <tr><td>Web</td><td><a href="https://www.eNorthfield.com">www.eNorthfield.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.24 |
| 2020 | 0.44 |
| 2019 | 0.43 |
| 2018 | 0.4 |
| 2017 | 0.34 |
| 2016 | 0.31 |
| 2015 | 0.28 |
| 2014 | 0.26 |
| 2013 | 0.49 |
| 2012 | 0.12 |
| 2011 | 0.23 |
| 2010 | 0.19 |
| 2009 | 0.16 |
| 2008 | 0.04 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
