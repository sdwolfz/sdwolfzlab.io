---
title: " (FORTY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>FORTY</td></tr>
    <tr><td>Web</td><td><a href="https://www.formulasystems.com">www.formulasystems.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.648 |
| 2020 | 0.516 |
| 2019 | 0.981 |
| 2018 | 0.68 |
| 2017 | 0.34 |
| 2016 | 1.16 |
| 2015 | 0.875 |
| 2014 | 0.79 |
| 2013 | 0.37 |
| 2011 | 0.71 |
| 2010 | 1.156 |
| 2009 | 1.796 |
| 2008 | 0.579 |
| 2006 | 1.125 |
| 2005 | 2.995 |
| 2001 | 0.535 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
