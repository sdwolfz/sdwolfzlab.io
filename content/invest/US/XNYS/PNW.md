---
title: "PINNACLE WEST CAPITAL CORP (PNW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PINNACLE WEST CAPITAL CORP</td></tr>
    <tr><td>Symbol</td><td>PNW</td></tr>
    <tr><td>Web</td><td><a href="https://www.pinnaclewest.com">www.pinnaclewest.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.66 |
| 2020 | 3.179 |
| 2019 | 2.997 |
| 2018 | 2.823 |
| 2017 | 2.66 |
| 2016 | 2.53 |
| 2015 | 2.41 |
| 2014 | 2.296 |
| 2013 | 1.112 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
