---
title: "FALCON MINERALS CORP (FLMN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FALCON MINERALS CORP</td></tr>
    <tr><td>Symbol</td><td>FLMN</td></tr>
    <tr><td>Web</td><td><a href="https://www.falconminerals.com">www.falconminerals.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.175 |
| 2020 | 0.255 |
| 2019 | 0.66 |
| 2018 | 0.095 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
