---
title: "REV GROUP INC (REVG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>REV GROUP INC</td></tr>
    <tr><td>Symbol</td><td>REVG</td></tr>
    <tr><td>Web</td><td><a href="https://www.revgroup.com">www.revgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.1 |
| 2019 | 0.2 |
| 2018 | 0.2 |
| 2017 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
