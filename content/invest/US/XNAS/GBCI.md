---
title: "GLACIER BANCORP (GBCI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GLACIER BANCORP</td></tr>
    <tr><td>Symbol</td><td>GBCI</td></tr>
    <tr><td>Web</td><td><a href="https://www.glacierbancorp.com">www.glacierbancorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.46 |
| 2020 | 1.38 |
| 2019 | 1.41 |
| 2018 | 1.01 |
| 2017 | 1.44 |
| 2016 | 1.1 |
| 2015 | 1.05 |
| 2014 | 0.68 |
| 2013 | 0.6 |
| 2012 | 0.66 |
| 2011 | 0.52 |
| 2010 | 0.39 |
| 2009 | 0.65 |
| 2008 | 0.52 |
| 2007 | 0.49 |
| 2006 | 0.65 |
| 2005 | 0.65 |
| 2004 | 0.75 |
| 2003 | 0.75 |
| 2002 | 0.64 |
| 2001 | 0.6 |
| 2000 | 0.65 |
| 1999 | 0.66 |
| 1998 | 0.55 |
| 1997 | 0.56 |
| 1996 | 0.64 |
| 1995 | 0.69 |
| 1994 | 0.6 |
| 1993 | 0.33 |
| 1992 | 0.73 |
| 1991 | 0.29 |
| 1990 | 0.44 |
| 1989 | 0.48 |
| 1988 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
