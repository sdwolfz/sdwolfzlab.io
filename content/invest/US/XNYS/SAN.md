---
title: " (SAN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SAN</td></tr>
    <tr><td>Web</td><td><a href="https://www.santander.com">www.santander.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.193 |
| 2018 | 0.203 |
| 2017 | 0.144 |
| 2016 | 0.171 |
| 2015 | 0.347 |
| 2014 | 0.624 |
| 2013 | 0.315 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
