---
title: " (WSBCP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>WSBCP</td></tr>
    <tr><td>Web</td><td><a href="https://www.wesbanco.com">www.wesbanco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.844 |
| 2020 | 0.441 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
