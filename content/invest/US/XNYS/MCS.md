---
title: "MARCUS CORP (MCS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MARCUS CORP</td></tr>
    <tr><td>Symbol</td><td>MCS</td></tr>
    <tr><td>Web</td><td><a href="https://www.marcuscorp.com">www.marcuscorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.17 |
| 2019 | 0.64 |
| 2018 | 0.6 |
| 2017 | 0.487 |
| 2016 | 0.448 |
| 2015 | 0.41 |
| 2014 | 0.37 |
| 2013 | 0.17 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
