---
title: "DUPLEX SA FAGARAS (DUPX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>DUPLEX SA FAGARAS</td></tr>
    <tr><td>Symbol</td><td>DUPX</td></tr>
    <tr><td>Web</td><td><a href="https://www.duplexfagaras.ro">www.duplexfagaras.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2007 | 0.8 |
| 2005 | 0.302 |
| 2004 | 0.1035 |
| 2003 | 0.0712 |
| 2002 | 0.095 |
| 2000 | 0.076 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
