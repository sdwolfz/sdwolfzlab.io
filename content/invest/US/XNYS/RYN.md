---
title: " (RYN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>RYN</td></tr>
    <tr><td>Web</td><td><a href="https://www.rayonier.com">www.rayonier.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.27 |
| 2020 | 1.08 |
| 2019 | 1.08 |
| 2018 | 1.06 |
| 2017 | 1.0 |
| 2016 | 1.0 |
| 2015 | 1.0 |
| 2014 | 1.53 |
| 2013 | 0.98 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
