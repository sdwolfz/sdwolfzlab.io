---
title: "FIAT CHRYSLER AUTOMOBILES NV (FCAU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FIAT CHRYSLER AUTOMOBILES NV</td></tr>
    <tr><td>Symbol</td><td>FCAU</td></tr>
    <tr><td>Web</td><td><a href="http://www.fcagroup.com">www.fcagroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 1.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
