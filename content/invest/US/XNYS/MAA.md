---
title: " (MAA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>MAA</td></tr>
    <tr><td>Web</td><td><a href="https://www.maac.com">www.maac.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.05 |
| 2020 | 4.0 |
| 2019 | 3.84 |
| 2018 | 3.692 |
| 2017 | 3.48 |
| 2016 | 3.28 |
| 2015 | 3.08 |
| 2014 | 2.92 |
| 2013 | 1.39 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
