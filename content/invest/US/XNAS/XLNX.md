---
title: "XILINX INC (XLNX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>XILINX INC</td></tr>
    <tr><td>Symbol</td><td>XLNX</td></tr>
    <tr><td>Web</td><td><a href="https://www.xilinx.com">www.xilinx.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.51 |
| 2019 | 1.47 |
| 2018 | 1.43 |
| 2017 | 1.38 |
| 2016 | 1.3 |
| 2015 | 1.22 |
| 2014 | 1.12 |
| 2013 | 0.97 |
| 2012 | 0.85 |
| 2011 | 0.73 |
| 2010 | 0.64 |
| 2009 | 0.58 |
| 2008 | 0.54 |
| 2007 | 0.45 |
| 2006 | 0.34 |
| 2005 | 0.26 |
| 2004 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
