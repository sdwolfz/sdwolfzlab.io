---
title: "HARVEST CAPITAL CREDIT CORPORATION (HCAP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HARVEST CAPITAL CREDIT CORPORATION</td></tr>
    <tr><td>Symbol</td><td>HCAP</td></tr>
    <tr><td>Web</td><td><a href="https://www.harvestcapitalcredit.com">www.harvestcapitalcredit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.4 |
| 2019 | 0.975 |
| 2018 | 1.158 |
| 2017 | 1.45 |
| 2016 | 1.35 |
| 2015 | 1.35 |
| 2014 | 1.35 |
| 2013 | 0.765 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
