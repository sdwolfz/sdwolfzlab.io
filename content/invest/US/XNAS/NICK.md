---
title: "NICHOLAS FINANCIAL INC (NICK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NICHOLAS FINANCIAL INC</td></tr>
    <tr><td>Symbol</td><td>NICK</td></tr>
    <tr><td>Web</td><td><a href="https://www.nicholasfinancial.com">www.nicholasfinancial.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2013 | 0.36 |
| 2012 | 2.44 |
| 2011 | 0.2 |
| 2005 | 0.05 |
| 2004 | 0.1 |
| 2003 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
