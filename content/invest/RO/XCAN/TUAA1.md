---
title: "TURISM COVASNA SA COVASNA - actiuni preferentiale, cu dividend prioritar, fara drept vot (TUAA1)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>TURISM COVASNA SA COVASNA - actiuni preferentiale, cu dividend prioritar, fara drept vot</td></tr>
    <tr><td>Symbol</td><td>TUAA1</td></tr>
    <tr><td>Web</td><td><a href="https://www.turismcovasna.ro">www.turismcovasna.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
