---
title: " (AGNCM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>AGNCM</td></tr>
    <tr><td>Web</td><td><a href="https://www.agnc.com">www.agnc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.43 |
| 2020 | 1.719 |
| 2019 | 1.475 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
