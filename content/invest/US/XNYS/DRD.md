---
title: " (DRD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>DRD</td></tr>
    <tr><td>Web</td><td><a href="https://www.drdgold.com">www.drdgold.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.197 |
| 2020 | 0.384 |
| 2019 | 0.099 |
| 2018 | 0.031 |
| 2017 | 0.028 |
| 2016 | 0.326 |
| 2015 | 0.06 |
| 2014 | 0.014 |
| 2013 | 0.227 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
