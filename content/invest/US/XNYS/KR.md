---
title: "KROGER CO (KR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>KROGER CO</td></tr>
    <tr><td>Symbol</td><td>KR</td></tr>
    <tr><td>Web</td><td><a href="https://www.thekrogerco.com">www.thekrogerco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.36 |
| 2020 | 0.68 |
| 2019 | 0.6 |
| 2018 | 0.985 |
| 2017 | 0.49 |
| 2016 | 0.45 |
| 2015 | 0.685 |
| 2014 | 0.68 |
| 2013 | 0.315 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
