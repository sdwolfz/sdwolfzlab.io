---
title: " (YPF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>YPF</td></tr>
    <tr><td>Web</td><td><a href="https://www.ypf.com">www.ypf.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.14 |
| 2018 | 0.079 |
| 2017 | 0.099 |
| 2016 | 0.11 |
| 2015 | 0.072 |
| 2014 | 0.087 |
| 2013 | 0.095 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
