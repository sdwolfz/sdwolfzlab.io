---
title: "FIRST HAWAIIAN (FHB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST HAWAIIAN</td></tr>
    <tr><td>Symbol</td><td>FHB</td></tr>
    <tr><td>Web</td><td><a href="https://www.fhb.com">www.fhb.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.52 |
| 2020 | 1.04 |
| 2019 | 1.04 |
| 2018 | 0.96 |
| 2017 | 0.88 |
| 2016 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
