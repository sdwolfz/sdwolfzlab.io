---
title: "CHINA UNICOM (HONG KONG) LIMITED ADR EACH REP 10 ORD HKD0.10 LVL111 (CHU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CHINA UNICOM (HONG KONG) LIMITED ADR EACH REP 10 ORD HKD0.10 LVL111</td></tr>
    <tr><td>Symbol</td><td>CHU</td></tr>
    <tr><td>Web</td><td><a href="http://www.chinaunicom.com.hk">www.chinaunicom.com.hk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.188 |
| 2019 | 0.179 |
| 2018 | 0.073 |
| 2016 | 0.235 |
| 2015 | 0.294 |
| 2014 | 0.234 |
| 2013 | 0.174 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
