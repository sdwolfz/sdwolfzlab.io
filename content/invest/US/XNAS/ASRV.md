---
title: "AMERISERV FINANCIAL INC (ASRV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AMERISERV FINANCIAL INC</td></tr>
    <tr><td>Symbol</td><td>ASRV</td></tr>
    <tr><td>Web</td><td><a href="https://www.ameriserv.com">www.ameriserv.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.05 |
| 2020 | 0.1 |
| 2019 | 0.095 |
| 2018 | 0.075 |
| 2017 | 0.06 |
| 2016 | 0.05 |
| 2015 | 0.04 |
| 2014 | 0.04 |
| 2013 | 0.03 |
| 2008 | 0.025 |
| 2002 | 0.3 |
| 2001 | 0.36 |
| 2000 | 0.42 |
| 1999 | 0.59 |
| 1998 | 1.11 |
| 1997 | 1.95 |
| 1996 | 1.37 |
| 1995 | 1.06 |
| 1994 | 0.97 |
| 1993 | 1.08 |
| 1992 | 0.75 |
| 1991 | 0.55 |
| 1990 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
