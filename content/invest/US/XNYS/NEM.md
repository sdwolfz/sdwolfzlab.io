---
title: "NEWMONT CORPORATION (NEM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NEWMONT CORPORATION</td></tr>
    <tr><td>Symbol</td><td>NEM</td></tr>
    <tr><td>Web</td><td><a href="https://www.newmont.com">www.newmont.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.55 |
| 2020 | 1.04 |
| 2019 | 1.44 |
| 2018 | 0.56 |
| 2017 | 0.25 |
| 2016 | 0.125 |
| 2015 | 0.1 |
| 2014 | 0.225 |
| 2013 | 0.45 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
