---
title: "KFORCE.COM INC (KFRC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>KFORCE.COM INC</td></tr>
    <tr><td>Symbol</td><td>KFRC</td></tr>
    <tr><td>Web</td><td><a href="https://www.kforce.com">www.kforce.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.46 |
| 2020 | 0.8 |
| 2019 | 0.72 |
| 2018 | 0.6 |
| 2017 | 0.48 |
| 2016 | 0.48 |
| 2015 | 0.45 |
| 2014 | 0.41 |
| 2013 | 0.1 |
| 2012 | 1.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
