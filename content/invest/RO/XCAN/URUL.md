---
title: "URB RULMENTI SA SUCEAVA (URUL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>URB RULMENTI SA SUCEAVA</td></tr>
    <tr><td>Symbol</td><td>URUL</td></tr>
    <tr><td>Web</td><td><a href="https://www.urb-s.ro">www.urb-s.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
