---
title: "NVIDIA CORP (NVDA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NVIDIA CORP</td></tr>
    <tr><td>Symbol</td><td>NVDA</td></tr>
    <tr><td>Web</td><td><a href="https://www.nvidia.com">www.nvidia.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.16 |
| 2020 | 0.64 |
| 2019 | 0.64 |
| 2018 | 0.61 |
| 2017 | 0.57 |
| 2016 | 0.485 |
| 2015 | 0.395 |
| 2014 | 0.34 |
| 2013 | 0.31 |
| 2012 | 0.075 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
