---
title: "STEEL DYNAMICS INC (STLD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>STEEL DYNAMICS INC</td></tr>
    <tr><td>Symbol</td><td>STLD</td></tr>
    <tr><td>Web</td><td><a href="https://www.steeldynamics.com">www.steeldynamics.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.26 |
| 2020 | 1.0 |
| 2019 | 0.96 |
| 2018 | 0.75 |
| 2017 | 0.62 |
| 2016 | 0.56 |
| 2015 | 0.55 |
| 2014 | 0.46 |
| 2013 | 0.44 |
| 2012 | 0.4 |
| 2011 | 0.4 |
| 2010 | 0.3 |
| 2009 | 0.325 |
| 2008 | 0.5 |
| 2007 | 0.6 |
| 2006 | 0.85 |
| 2005 | 0.4 |
| 2004 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
