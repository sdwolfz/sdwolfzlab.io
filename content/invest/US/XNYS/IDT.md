---
title: "IDT CORP (IDT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>IDT CORP</td></tr>
    <tr><td>Symbol</td><td>IDT</td></tr>
    <tr><td>Web</td><td><a href="https://www.idt.net">www.idt.net</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 1.549 |
| 2017 | 0.57 |
| 2016 | 0.76 |
| 2015 | 0.55 |
| 2014 | 0.51 |
| 2013 | 0.17 |
| 2012 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
