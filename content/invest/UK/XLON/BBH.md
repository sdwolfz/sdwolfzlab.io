---
title: "Bb Healthcare (BBH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Bb Healthcare</td></tr>
    <tr><td>Symbol</td><td>BBH</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 5.0 |
| 2019 | 4.85 |
| 2018 | 0.4 |
| 2017 | 0.35 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
