---
title: "TEEKAY CORPORATION (TK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TEEKAY CORPORATION</td></tr>
    <tr><td>Symbol</td><td>TK</td></tr>
    <tr><td>Web</td><td><a href="https://www.teekay.com">www.teekay.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.055 |
| 2018 | 0.22 |
| 2017 | 0.22 |
| 2016 | 0.715 |
| 2015 | 1.732 |
| 2014 | 1.264 |
| 2013 | 0.632 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
