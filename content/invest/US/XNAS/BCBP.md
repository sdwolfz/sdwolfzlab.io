---
title: "BCB BANCORP INC (BCBP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BCB BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>BCBP</td></tr>
    <tr><td>Web</td><td><a href="https://www.bcb.bank">www.bcb.bank</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 0.56 |
| 2019 | 0.56 |
| 2018 | 0.56 |
| 2017 | 0.56 |
| 2016 | 0.56 |
| 2015 | 0.56 |
| 2014 | 0.54 |
| 2013 | 0.48 |
| 2012 | 0.48 |
| 2011 | 0.48 |
| 2010 | 0.48 |
| 2009 | 0.48 |
| 2008 | 0.41 |
| 2007 | 0.32 |
| 2006 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
