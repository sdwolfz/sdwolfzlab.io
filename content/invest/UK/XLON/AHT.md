---
title: "Ashtead Group (AHT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Ashtead Group</td></tr>
    <tr><td>Symbol</td><td>AHT</td></tr>
    <tr><td>Web</td><td><a href="https://www.ashtead-group.com">www.ashtead-group.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 40.65 |
| 2019 | 40.0 |
| 2018 | 33.0 |
| 2017 | 27.5 |
| 2016 | 22.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
