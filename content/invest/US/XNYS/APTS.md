---
title: " (APTS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>APTS</td></tr>
    <tr><td>Web</td><td><a href="https://www.pacapts.com">www.pacapts.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.175 |
| 2020 | 0.787 |
| 2019 | 1.046 |
| 2018 | 1.02 |
| 2017 | 0.94 |
| 2016 | 0.818 |
| 2015 | 0.727 |
| 2014 | 0.655 |
| 2013 | 0.31 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
