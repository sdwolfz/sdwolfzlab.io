---
title: "NATIONAL FUEL GAS CO (NFG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NATIONAL FUEL GAS CO</td></tr>
    <tr><td>Symbol</td><td>NFG</td></tr>
    <tr><td>Web</td><td><a href="https://www.nationalfuelgas.com">www.nationalfuelgas.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.445 |
| 2020 | 1.77 |
| 2019 | 1.73 |
| 2018 | 1.69 |
| 2017 | 1.65 |
| 2016 | 1.61 |
| 2015 | 1.57 |
| 2014 | 1.53 |
| 2013 | 1.125 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
