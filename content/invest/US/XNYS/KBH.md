---
title: "KB HOME (KBH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>KB HOME</td></tr>
    <tr><td>Symbol</td><td>KBH</td></tr>
    <tr><td>Web</td><td><a href="https://www.kbhome.com">www.kbhome.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 0.42 |
| 2019 | 0.455 |
| 2018 | 0.1 |
| 2017 | 0.1 |
| 2016 | 0.1 |
| 2015 | 0.1 |
| 2014 | 0.1 |
| 2013 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
