---
title: "RCM TECHNOLOGIES INC (RCMT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>RCM TECHNOLOGIES INC</td></tr>
    <tr><td>Symbol</td><td>RCMT</td></tr>
    <tr><td>Web</td><td><a href="https://www.rcmt.com">www.rcmt.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 1.0 |
| 2015 | 1.0 |
| 2014 | 2.0 |
| 2012 | 1.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
