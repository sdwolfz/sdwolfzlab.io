---
title: "IntegraFin Holdings (IHP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>IntegraFin Holdings</td></tr>
    <tr><td>Symbol</td><td>IHP</td></tr>
    <tr><td>Web</td><td><a href="https://www.integrafin.co.uk">www.integrafin.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 8.3 |
| 2019 | 7.8 |
| 2018 | 6.4 |
| 2017 | 5.9 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
