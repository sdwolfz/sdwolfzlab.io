---
title: "TOWNEBANK(PORTSMOUTH VIRGINIA) (TOWN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TOWNEBANK(PORTSMOUTH VIRGINIA)</td></tr>
    <tr><td>Symbol</td><td>TOWN</td></tr>
    <tr><td>Web</td><td><a href="https://www.townebank.com">www.townebank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.18 |
| 2020 | 0.72 |
| 2019 | 0.7 |
| 2018 | 0.62 |
| 2017 | 0.55 |
| 2016 | 0.51 |
| 2015 | 0.47 |
| 2014 | 0.43 |
| 2013 | 0.38 |
| 2012 | 0.33 |
| 2011 | 0.32 |
| 2010 | 0.32 |
| 2009 | 0.32 |
| 2008 | 0.32 |
| 2007 | 0.32 |
| 2006 | 0.53 |
| 2005 | 0.13 |
| 2004 | 0.1 |
| 2003 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
