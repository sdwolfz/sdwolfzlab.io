---
title: " (NRZ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>NRZ</td></tr>
    <tr><td>Web</td><td><a href="https://www.newresi.com">www.newresi.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.5 |
| 2019 | 2.0 |
| 2018 | 2.0 |
| 2017 | 1.98 |
| 2016 | 1.84 |
| 2015 | 1.75 |
| 2014 | 1.08 |
| 2013 | 0.42 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
