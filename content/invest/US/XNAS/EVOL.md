---
title: "EVOLVING SYSTEMS (EVOL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>EVOLVING SYSTEMS</td></tr>
    <tr><td>Symbol</td><td>EVOL</td></tr>
    <tr><td>Web</td><td><a href="https://www.evolving.com">www.evolving.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.22 |
| 2015 | 0.44 |
| 2014 | 0.42 |
| 2013 | 0.36 |
| 2012 | 4.0 |
| 2011 | 0.15 |
| 2010 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
