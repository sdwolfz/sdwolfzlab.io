---
title: "GAP INC (GPS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GAP INC</td></tr>
    <tr><td>Symbol</td><td>GPS</td></tr>
    <tr><td>Web</td><td><a href="https://www.gapinc.com">www.gapinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.243 |
| 2020 | 0.486 |
| 2019 | 0.972 |
| 2018 | 0.959 |
| 2017 | 0.69 |
| 2016 | 1.15 |
| 2015 | 0.91 |
| 2014 | 0.86 |
| 2013 | 0.55 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
