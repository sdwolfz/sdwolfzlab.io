---
title: "CAE INC (CAE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CAE INC</td></tr>
    <tr><td>Symbol</td><td>CAE</td></tr>
    <tr><td>Web</td><td><a href="https://www.cae.com">www.cae.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.082 |
| 2019 | 0.316 |
| 2018 | 0.292 |
| 2017 | 0.263 |
| 2016 | 0.233 |
| 2015 | 0.257 |
| 2014 | 0.26 |
| 2013 | 0.11 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
