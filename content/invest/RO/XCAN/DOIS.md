---
title: "DORNA TURISM SA VATRA DORNEI (DOIS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>DORNA TURISM SA VATRA DORNEI</td></tr>
    <tr><td>Symbol</td><td>DOIS</td></tr>
    <tr><td>Web</td><td><a href="https://www.dornaturism.ro">www.dornaturism.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2008 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
