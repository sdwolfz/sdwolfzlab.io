---
title: "METALUL MESA SA SALONTA (MESA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>METALUL MESA SA SALONTA</td></tr>
    <tr><td>Symbol</td><td>MESA</td></tr>
    <tr><td>Web</td><td><a href="https://www.mesa.ro">www.mesa.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2004 | 0.005 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
