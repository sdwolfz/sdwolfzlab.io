---
title: "UNIVERS SA Rm. Valcea (UNVR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>UNIVERS SA Rm. Valcea</td></tr>
    <tr><td>Symbol</td><td>UNVR</td></tr>
    <tr><td>Web</td><td><a href="https://www.univers-valcea.ro">www.univers-valcea.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.29 |
| 2019 | 3.665 |
| 2018 | 2.9 |
| 2017 | 1.72 |
| 2016 | 3.17 |
| 2015 | 1.5088 |
| 2014 | 1.3818 |
| 2013 | 0.9145 |
| 2012 | 1.4015 |
| 2010 | 0.85 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
