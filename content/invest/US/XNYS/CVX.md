---
title: "CHEVRON CORPORATION (CVX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CHEVRON CORPORATION</td></tr>
    <tr><td>Symbol</td><td>CVX</td></tr>
    <tr><td>Web</td><td><a href="https://www.chevron.com">www.chevron.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.63 |
| 2020 | 5.16 |
| 2019 | 4.76 |
| 2018 | 4.48 |
| 2017 | 4.32 |
| 2016 | 4.29 |
| 2015 | 4.28 |
| 2014 | 4.21 |
| 2013 | 2.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
