---
title: "Ruby Minitest and Simplecov starter"
date: 2020-09-30T18:38:47Z
type: posts
slug: ruby-minitest-and-simplecov-starter
tags: ["ruby", "testing", "development", "tutorial"]
---

1. Add these gems to your `Gemfile`:

```ruby
# frozen_string_literal: true

group :development, :test do
  gem 'minitest'
  gem 'rake'
  gem 'simplecov'
end
```

2. Run `bundle install` to install them.

3. Create a `test/test_helper.rb` file containing:

```ruby
# frozen_string_literal: true

require 'simplecov'
SimpleCov.start do
  enable_coverage :branch
end

$LOAD_PATH.unshift File.expand_path('../lib', __dir__)

require 'minitest/autorun'
```

4. Add the following lines to your `Rakefile`:

```ruby
# frozen_string_literal: true

require 'rake/testtask'

Rake::TestTask.new(:test) do |t|
  t.libs << 'test'
  t.libs << 'lib'
  t.test_files = FileList['test/**/*_test.rb']
end
```

5. Create a sample test at `test/unit/trial_test.rb` to try it out:

```ruby
# frozen_string_literal: true

require 'test_helper'

describe 'trial' do
  it 'is worthy' do
    assert(true)
  end
end
```

6. Run the tests with `bundle exec rake test`

7. Learn to use the tools:

| Name      | Link                                        |
|-----------|---------------------------------------------|
| Minitest  | https://github.com/seattlerb/minitest       |
| Rake      | https://github.com/ruby/rake                |
| Simplecov | https://github.com/simplecov-ruby/simplecov |
