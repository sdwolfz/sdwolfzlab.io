---
title: "AUTOMATIC DATA PROCESSING INC (ADP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AUTOMATIC DATA PROCESSING INC</td></tr>
    <tr><td>Symbol</td><td>ADP</td></tr>
    <tr><td>Web</td><td><a href="https://www.adp.com">www.adp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.86 |
| 2020 | 3.66 |
| 2019 | 3.28 |
| 2018 | 2.8 |
| 2017 | 2.34 |
| 2016 | 2.16 |
| 2015 | 2.0 |
| 2014 | 1.93 |
| 2013 | 1.785 |
| 2012 | 1.62 |
| 2011 | 1.475 |
| 2010 | 1.38 |
| 2009 | 1.33 |
| 2008 | 1.2 |
| 2007 | 0.75 |
| 2006 | 0.785 |
| 2005 | 0.65 |
| 2004 | 0.575 |
| 2003 | 0.5 |
| 2002 | 0.465 |
| 2001 | 0.218 |
| 2000 | 0.19 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
