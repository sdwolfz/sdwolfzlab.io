---
title: "AAR CORP (AIR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AAR CORP</td></tr>
    <tr><td>Symbol</td><td>AIR</td></tr>
    <tr><td>Web</td><td><a href="https://www.aarcorp.com">www.aarcorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.15 |
| 2019 | 0.3 |
| 2018 | 0.3 |
| 2017 | 0.3 |
| 2016 | 0.3 |
| 2015 | 0.3 |
| 2014 | 0.3 |
| 2013 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
