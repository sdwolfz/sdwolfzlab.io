---
title: "MYERS INDUSTRIES INC (MYE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MYERS INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>MYE</td></tr>
    <tr><td>Web</td><td><a href="https://www.myersindustries.com">www.myersindustries.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.135 |
| 2020 | 0.54 |
| 2019 | 0.54 |
| 2018 | 0.54 |
| 2017 | 0.54 |
| 2016 | 0.54 |
| 2015 | 0.54 |
| 2014 | 0.52 |
| 2013 | 0.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
