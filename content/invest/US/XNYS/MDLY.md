---
title: "MEDLEY MANAGEMENT INC (MDLY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MEDLEY MANAGEMENT INC</td></tr>
    <tr><td>Symbol</td><td>MDLY</td></tr>
    <tr><td>Web</td><td><a href="https://www.mdly.com">www.mdly.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.07 |
| 2019 | 0.03 |
| 2018 | 0.8 |
| 2017 | 0.8 |
| 2016 | 0.8 |
| 2015 | 0.6 |
| 2014 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
