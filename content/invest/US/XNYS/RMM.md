---
title: "RIVERNORTH MANAGED DUR MUN INCOM FD (RMM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>RIVERNORTH MANAGED DUR MUN INCOM FD</td></tr>
    <tr><td>Symbol</td><td>RMM</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.46 |
| 2020 | 1.104 |
| 2019 | 0.368 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
