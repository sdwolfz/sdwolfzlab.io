---
title: "Bank Of Georgia Group (BGEO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Bank Of Georgia Group</td></tr>
    <tr><td>Symbol</td><td>BGEO</td></tr>
    <tr><td>Web</td><td><a href="https://www.bankofgeorgiagroup.com">www.bankofgeorgiagroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 255.0 |
| 2018 | 244.0 |
| 2017 | 310.0 |
| 2016 | 260.0 |
| 2015 | 240.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
