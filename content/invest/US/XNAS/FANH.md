---
title: " (FANH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>FANH</td></tr>
    <tr><td>Web</td><td><a href="https://www.fanhuaholdings.com">www.fanhuaholdings.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 1.05 |
| 2019 | 1.15 |
| 2018 | 0.95 |
| 2017 | 0.32 |
| 2010 | 0.24 |
| 2009 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
