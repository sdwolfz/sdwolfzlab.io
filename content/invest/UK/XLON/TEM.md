---
title: "Templeton Emerging Markets (TEM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Templeton Emerging Markets</td></tr>
    <tr><td>Symbol</td><td>TEM</td></tr>
    <tr><td>Web</td><td><a href="https://www.temit.co.uk">www.temit.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 21.6 |
| 2019 | 16.0 |
| 2017 | 8.25 |
| 2016 | 8.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
