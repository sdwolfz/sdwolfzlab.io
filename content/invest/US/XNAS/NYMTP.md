---
title: " (NYMTP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>NYMTP</td></tr>
    <tr><td>Web</td><td><a href="https://www.nymtrust.com">www.nymtrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.484 |
| 2020 | 1.938 |
| 2019 | 1.938 |
| 2018 | 1.938 |
| 2017 | 1.938 |
| 2016 | 1.938 |
| 2015 | 1.938 |
| 2014 | 1.938 |
| 2013 | 1.189 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
