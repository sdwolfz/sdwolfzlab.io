---
title: "ACCO BRANDS CORPORATION (ACCO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ACCO BRANDS CORPORATION</td></tr>
    <tr><td>Symbol</td><td>ACCO</td></tr>
    <tr><td>Web</td><td><a href="https://www.accobrands.com">www.accobrands.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.13 |
| 2020 | 0.26 |
| 2019 | 0.245 |
| 2018 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
