---
title: "UZUC S.A. (UZC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>UZUC S.A.</td></tr>
    <tr><td>Symbol</td><td>UZC</td></tr>
    <tr><td>Web</td><td><a href="https://www.uzuc.ro">www.uzuc.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2007 | 0.0021 |
| 2006 | 0.277 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
