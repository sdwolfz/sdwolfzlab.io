---
title: "VALERO ENERGY CORP (VLO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>VALERO ENERGY CORP</td></tr>
    <tr><td>Symbol</td><td>VLO</td></tr>
    <tr><td>Web</td><td><a href="https://www.valero.com">www.valero.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.96 |
| 2020 | 3.92 |
| 2019 | 3.6 |
| 2018 | 3.2 |
| 2017 | 2.8 |
| 2016 | 2.4 |
| 2015 | 1.7 |
| 2014 | 1.05 |
| 2013 | 0.45 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
