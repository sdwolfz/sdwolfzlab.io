---
title: "COCA-COLA EUROPACIFIC PARTNERS PLC (CCEP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>COCA-COLA EUROPACIFIC PARTNERS PLC</td></tr>
    <tr><td>Symbol</td><td>CCEP</td></tr>
    <tr><td>Web</td><td><a href="https://www.cocacolaep.com">www.cocacolaep.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.32 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
