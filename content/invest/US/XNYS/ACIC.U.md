---
title: "ATLAS CREST INVT CORP UNIT 1 CL A & 1/3 WT EXP (ACIC.U)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ATLAS CREST INVT CORP UNIT 1 CL A & 1/3 WT EXP</td></tr>
    <tr><td>Symbol</td><td>ACIC.U</td></tr>
    <tr><td>Web</td><td><a href="http://www.atlascrestcorp.com">www.atlascrestcorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
