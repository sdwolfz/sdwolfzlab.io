---
title: "BMO Global Smaller Companies Trust (BGSC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>BMO Global Smaller Companies Trust</td></tr>
    <tr><td>Symbol</td><td>BGSC</td></tr>
    <tr><td>Web</td><td><a href="https://www.bmoglobalsmallers.com">www.bmoglobalsmallers.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.7 |
| 2019 | 1.65 |
| 2018 | 1.44 |
| 2017 | 1.23 |
| 2016 | 1.07 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
