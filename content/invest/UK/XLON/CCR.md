---
title: "C&c Grp (CCR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>C&c Grp</td></tr>
    <tr><td>Symbol</td><td>CCR</td></tr>
    <tr><td>Web</td><td><a href="https://www.candcgroupplc.com">www.candcgroupplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 5.5 |
| 2019 | 15.31 |
| 2018 | 14.58 |
| 2017 | 14.33 |
| 2016 | 13.65 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
