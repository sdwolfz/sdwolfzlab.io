---
title: "MAVERIX METALS INC (MMX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MAVERIX METALS INC</td></tr>
    <tr><td>Symbol</td><td>MMX</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.01 |
| 2020 | 0.04 |
| 2019 | 0.01 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
