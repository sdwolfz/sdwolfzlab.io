---
title: "VECTOR GROUP (VGR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>VECTOR GROUP</td></tr>
    <tr><td>Symbol</td><td>VGR</td></tr>
    <tr><td>Web</td><td><a href="https://www.vectorgroupltd.com">www.vectorgroupltd.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.8 |
| 2019 | 1.6 |
| 2018 | 1.6 |
| 2017 | 1.6 |
| 2016 | 1.6 |
| 2015 | 1.6 |
| 2014 | 1.6 |
| 2013 | 1.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
