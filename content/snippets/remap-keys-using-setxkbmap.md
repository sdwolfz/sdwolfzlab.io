---
title: "Remap keys using setxkbmap"
date: 2021-01-24T17:15:32Z
type: posts
slug: remap-keys-using-setxkbmap
tags: ["linux", "keyboard"]
---

Remap `CapsLock` to `Escape` and `AltGr` to `Alt`:

```sh
setxkbmap -option caps:escape,lv3:ralt_alt
```

Reset with:

```sh
setxkbmap -option
```

## Persistence

Set `XkbOptions` in `/etc/X11/xorg.conf.d/00-keyboard.conf`:

```conf
Section "InputClass"
  Identifier "system-keyboard"
  MatchIsKeyboard "on"
  Option "XkbLayout" "gb"
  Option "XkbModel" "pc105"
  Option "XkbOptions" "caps:escape,lv3:ralt_alt"
EndSection
```

Now restart your machine.

## Alternative

If the above did not work try `/etc/default/keyboard`:

```conf
XKBMODEL="pc105"
XKBLAYOUT="gb"
XKBVARIANT=""
XKBOPTIONS="caps:escape,lv3:ralt_alt"

BACKSPACE="guess"
```

You can either restart or execute:

```sh
sudo udevadm trigger --subsystem-match=input --action=change
```

## Documentation

There is none, you can look at these options listed here:

```sh
cat /usr/share/X11/xkb/rules/base.lst
```

Stare at them for a few hours, try some out, and you might stumble upon
something useful.
