---
title: " (MUFG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>MUFG</td></tr>
    <tr><td>Web</td><td><a href="https://www.mufg.jp">www.mufg.jp</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.193 |
| 2019 | 0.18 |
| 2018 | 0.147 |
| 2017 | 0.07 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
