---
title: "CARLISLE COS INC (CSL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CARLISLE COS INC</td></tr>
    <tr><td>Symbol</td><td>CSL</td></tr>
    <tr><td>Web</td><td><a href="https://www.carlisle.com">www.carlisle.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.05 |
| 2020 | 2.05 |
| 2019 | 1.8 |
| 2018 | 1.54 |
| 2017 | 1.44 |
| 2016 | 1.3 |
| 2015 | 1.1 |
| 2014 | 0.94 |
| 2013 | 0.44 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
