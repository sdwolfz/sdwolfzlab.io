---
title: "Greggs (GRG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Greggs</td></tr>
    <tr><td>Symbol</td><td>GRG</td></tr>
    <tr><td>Web</td><td><a href="https://www.greggs.co.uk">www.greggs.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 79.9 |
| 2018 | 35.7 |
| 2017 | 32.3 |
| 2017 | 32.3 |
| 2016 | 31.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
