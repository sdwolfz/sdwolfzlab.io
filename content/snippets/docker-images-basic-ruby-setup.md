---
title: "Docker Images: basic Ruby setup"
date: 2020-07-16T18:51:03Z
type: posts
slug: docker-images-basic-ruby-setup
series: ["Docker Images"]
tags: ["docker", "ruby", "shell", "productivity", "development"]
---

Use this as a template for ruby apps:

```dockerfile
#-------------------------------------------------------------------------------
# Base

FROM ruby:2.7.2-alpine

SHELL ["/bin/ash", "-c"]

#-------------------------------------------------------------------------------
# Packages

RUN set -exo pipefail              && \
                                      \
    echo 'Install system packages' && \
    apk add --update --no-cache \
      build-base                \
      shadow

#-------------------------------------------------------------------------------
# Workspace

RUN set -exo pipefail       && \
                               \
    echo 'Create workspace' && \
    mkdir -p /work

WORKDIR /work

#-------------------------------------------------------------------------------
# User

ARG HOST_USER_UID=1000
ARG HOST_USER_GID=1000

RUN set -exo pipefail                                  && \
                                                          \
    echo 'Create the notroot user and group from host' && \
    groupadd -g $HOST_USER_GID notroot                 && \
    useradd -lm -u $HOST_USER_UID -g notroot notroot   && \
                                                          \
    echo 'Set direcotry permissions'                   && \
    chown notroot:notroot /work

#-------------------------------------------------------------------------------
# Gems

COPY --chown=notroot:notroot Gemfile* /work/

USER notroot

RUN set -exo pipefail                 && \
                                         \
    echo 'Install gems'               && \
    cd /work                          && \
    bundle install --jobs 3 --retry 3

#-------------------------------------------------------------------------------
# Command

CMD ["sh"]
```

Build it like this:

```sh
docker build \
  --build-arg=HOST_USER_UID=`id -u` \
  --build-arg=HOST_USER_GID=`id -g` \
  -t ruby-project \
  "$PWD"
```

Or in `docker-compose` using:

```yml
---

services:
  app:
    build:
      args:
        HOST_USER_UID: ${HOST_USER_UID}
        HOST_USER_GID: ${HOST_USER_GID}
      context: .
      dockerfile: ./Dockerfile
```

And have your host variables defined in your `.bash_profile`:

```sh
HOST_USER_UID=$(id -u)
HOST_USER_GID=$(id -g)

export HOST_USER_UID
export HOST_USER_GID
```
