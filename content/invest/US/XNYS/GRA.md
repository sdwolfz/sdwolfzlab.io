---
title: "GRACE(W.R.)& CO (GRA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GRACE(W.R.)& CO</td></tr>
    <tr><td>Symbol</td><td>GRA</td></tr>
    <tr><td>Web</td><td><a href="https://www.grace.com">www.grace.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.33 |
| 2020 | 1.2 |
| 2019 | 1.08 |
| 2018 | 0.96 |
| 2017 | 0.84 |
| 2016 | 0.51 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
