---
title: "G. WILLI-FOOD (WILC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>G. WILLI-FOOD</td></tr>
    <tr><td>Symbol</td><td>WILC</td></tr>
    <tr><td>Web</td><td><a href="https://www.willi-food.com">www.willi-food.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.378 |
| 2006 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
