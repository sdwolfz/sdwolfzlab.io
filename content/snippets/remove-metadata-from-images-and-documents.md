---
title: "Remove metadata from images and documents"
date: 2021-05-20T09:37:23Z
type: posts
slug: remove-metadata-from-images-and-documents
tags: ["linux", "privacy", "security"]
---

Every image or document you create has built in spyware, remove it like this:

## Install

```sh
sudo pacman -S perl-image-exiftool
```

## Usage

```sh
exiftool -overwrite_original_in_place -all= path-to-file
```
