---
title: "Basic golang setup"
date: 2020-12-19T11:49:48Z
type: posts
slug: basic-golang-setup
tags: ["golang", "linux", "development"]
---

## Install

```sh
sudo pacman -S go
```

## Configure

Add these to your `~/.bash_profile`:

```sh
export GOPATH=~/.gopath
export PATH=$GOPATH/bin:$PATH
```

## Use

Install packages with:

```sh
go get -u -v package-name
```

And modules with:

```sh
env GO111MODULE=on go get -v module-name
```
