---
title: "BANNER CORP (BANR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BANNER CORP</td></tr>
    <tr><td>Symbol</td><td>BANR</td></tr>
    <tr><td>Web</td><td><a href="https://www.bannerbank.com">www.bannerbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.82 |
| 2020 | 2.64 |
| 2019 | 1.61 |
| 2018 | 1.83 |
| 2017 | 1.98 |
| 2016 | 0.65 |
| 2015 | 0.72 |
| 2014 | 0.72 |
| 2013 | 0.55 |
| 2012 | 0.04 |
| 2011 | 0.04 |
| 2010 | 0.04 |
| 2009 | 0.03 |
| 2008 | 0.5 |
| 2007 | 0.77 |
| 2006 | 0.73 |
| 2005 | 0.69 |
| 2004 | 0.65 |
| 2003 | 0.61 |
| 2002 | 0.6 |
| 2001 | 0.56 |
| 2000 | 0.56 |
| 1999 | 0.48 |
| 1998 | 0.36 |
| 1997 | 0.28 |
| 1996 | 0.2 |
| 1995 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
