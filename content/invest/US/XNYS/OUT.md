---
title: " (OUT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>OUT</td></tr>
    <tr><td>Web</td><td><a href="https://www.outfrontmedia.com">www.outfrontmedia.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.38 |
| 2019 | 1.44 |
| 2018 | 1.44 |
| 2017 | 1.44 |
| 2016 | 1.36 |
| 2015 | 1.36 |
| 2014 | 0.37 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
