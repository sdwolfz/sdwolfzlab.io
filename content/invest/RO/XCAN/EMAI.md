---
title: "EMAILUL SA MEDIAS (EMAI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>EMAILUL SA MEDIAS</td></tr>
    <tr><td>Symbol</td><td>EMAI</td></tr>
    <tr><td>Web</td><td><a href="https://www.emailul.ro">www.emailul.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.22105 |
| 2019 | 1.1053 |
| 2018 | 1.1053 |
| 2017 | 1.1053 |
| 2016 | 1.01 |
| 2015 | 0.95 |
| 2014 | 0.8571 |
| 2013 | 0.77 |
| 2012 | 0.7143 |
| 2008 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
