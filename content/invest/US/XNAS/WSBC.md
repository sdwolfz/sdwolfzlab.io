---
title: "WESBANCO INC (WSBC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>WESBANCO INC</td></tr>
    <tr><td>Symbol</td><td>WSBC</td></tr>
    <tr><td>Web</td><td><a href="https://www.wesbanco.com">www.wesbanco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.33 |
| 2020 | 1.28 |
| 2019 | 1.24 |
| 2018 | 1.16 |
| 2017 | 1.04 |
| 2016 | 0.96 |
| 2015 | 0.92 |
| 2014 | 0.88 |
| 2013 | 0.78 |
| 2012 | 0.7 |
| 2011 | 0.62 |
| 2010 | 0.56 |
| 2009 | 0.84 |
| 2008 | 1.12 |
| 2007 | 1.1 |
| 2006 | 1.06 |
| 2005 | 1.04 |
| 2004 | 1.0 |
| 2003 | 0.96 |
| 2002 | 0.935 |
| 2001 | 0.92 |
| 2000 | 0.895 |
| 1999 | 0.88 |
| 1998 | 0.84 |
| 1997 | 0.98 |
| 1996 | 1.08 |
| 1995 | 0.96 |
| 1994 | 0.86 |
| 1993 | 0.978 |
| 1992 | 1.4 |
| 1991 | 1.35 |
| 1990 | 1.3 |
| 1989 | 1.2 |
| 1988 | 0.288 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
