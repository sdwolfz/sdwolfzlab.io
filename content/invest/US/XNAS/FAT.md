---
title: "FAT BRANDS INC (FAT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FAT BRANDS INC</td></tr>
    <tr><td>Symbol</td><td>FAT</td></tr>
    <tr><td>Web</td><td><a href="https://www.fatbrands.com">www.fatbrands.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.13 |
| 2018 | 0.36 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
