---
title: "FARMERS NATIONAL BANCORP (FMNB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FARMERS NATIONAL BANCORP</td></tr>
    <tr><td>Symbol</td><td>FMNB</td></tr>
    <tr><td>Web</td><td><a href="https://www.farmersbankgroup.com">www.farmersbankgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.11 |
| 2020 | 0.44 |
| 2019 | 0.38 |
| 2018 | 0.3 |
| 2017 | 0.22 |
| 2016 | 0.16 |
| 2015 | 0.12 |
| 2014 | 0.12 |
| 2013 | 0.12 |
| 2012 | 0.18 |
| 2011 | 0.12 |
| 2010 | 0.12 |
| 2009 | 0.48 |
| 2008 | 0.4 |
| 2007 | 0.64 |
| 2006 | 0.64 |
| 2005 | 0.64 |
| 2004 | 0.64 |
| 2003 | 0.61 |
| 2002 | 0.57 |
| 2001 | 0.53 |
| 2000 | 0.51 |
| 1999 | 0.72 |
| 1998 | 0.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
