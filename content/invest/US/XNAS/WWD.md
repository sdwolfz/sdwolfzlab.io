---
title: "WOODWARD INC (WWD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>WOODWARD INC</td></tr>
    <tr><td>Symbol</td><td>WWD</td></tr>
    <tr><td>Web</td><td><a href="https://www.woodward.com">www.woodward.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.325 |
| 2020 | 0.524 |
| 2019 | 0.65 |
| 2018 | 0.57 |
| 2017 | 0.5 |
| 2016 | 0.44 |
| 2015 | 0.4 |
| 2014 | 0.32 |
| 2013 | 0.32 |
| 2012 | 0.32 |
| 2011 | 0.28 |
| 2010 | 0.24 |
| 2009 | 0.24 |
| 2008 | 0.3 |
| 2007 | 0.44 |
| 2006 | 0.6 |
| 2005 | 1.1 |
| 2004 | 0.96 |
| 2003 | 0.96 |
| 2002 | 0.93 |
| 2001 | 0.93 |
| 2000 | 0.93 |
| 1999 | 0.93 |
| 1998 | 0.93 |
| 1997 | 0.93 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
