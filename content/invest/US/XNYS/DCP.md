---
title: " (DCP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>DCP</td></tr>
    <tr><td>Web</td><td><a href="https://www.dcpmidstream.com">www.dcpmidstream.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.78 |
| 2020 | 1.95 |
| 2019 | 3.12 |
| 2018 | 3.12 |
| 2017 | 3.12 |
| 2016 | 0.78 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
