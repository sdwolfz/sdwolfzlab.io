---
title: "Metro Bank (MTRO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Metro Bank</td></tr>
    <tr><td>Symbol</td><td>MTRO</td></tr>
    <tr><td>Web</td><td><a href="https://www.metrobankonline.co.uk">www.metrobankonline.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
