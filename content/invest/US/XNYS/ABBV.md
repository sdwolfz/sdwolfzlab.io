---
title: "ABBVIE INC (ABBV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ABBVIE INC</td></tr>
    <tr><td>Symbol</td><td>ABBV</td></tr>
    <tr><td>Web</td><td><a href="https://www.abbvie.com">www.abbvie.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.6 |
| 2020 | 4.72 |
| 2019 | 4.28 |
| 2018 | 3.59 |
| 2017 | 2.56 |
| 2016 | 2.28 |
| 2015 | 2.02 |
| 2014 | 1.66 |
| 2013 | 0.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
