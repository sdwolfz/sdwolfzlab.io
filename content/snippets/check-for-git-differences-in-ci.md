---
title: "Check for Git differences in CI"
date: 2022-05-03T07:49:16Z
type: posts
slug: check-git-diff-in-ci
tags: ["git", "CI", "linux"]
---

The command is:

```sh
git diff --exit-code
```

Prints the diff and exits with status code `1`.

## CI

```yml
generate:
  image: ruby:3.1.2-alpine
  script:
    - apk add --update git make
    - make generate
    - git diff --exit-code
```
