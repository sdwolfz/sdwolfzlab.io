---
title: "SINCLAIR BROADCAST GROUP INC (SBGI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SINCLAIR BROADCAST GROUP INC</td></tr>
    <tr><td>Symbol</td><td>SBGI</td></tr>
    <tr><td>Web</td><td><a href="https://www.sbgi.net">www.sbgi.net</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.4 |
| 2020 | 0.8 |
| 2019 | 0.8 |
| 2018 | 0.74 |
| 2017 | 0.72 |
| 2016 | 0.705 |
| 2015 | 0.66 |
| 2014 | 0.63 |
| 2013 | 0.6 |
| 2012 | 1.54 |
| 2011 | 0.48 |
| 2010 | 0.43 |
| 2008 | 0.8 |
| 2007 | 0.625 |
| 2006 | 0.45 |
| 2005 | 0.3 |
| 2004 | 0.075 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
