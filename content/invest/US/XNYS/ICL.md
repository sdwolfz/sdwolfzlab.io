---
title: "ICL GROUP LTD (ICL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ICL GROUP LTD</td></tr>
    <tr><td>Symbol</td><td>ICL</td></tr>
    <tr><td>Web</td><td><a href="https://www.icl-group.com">www.icl-group.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.02 |
| 2020 | 0.044 |
| 2018 | 0.054 |
| 2017 | 0.114 |
| 2016 | 0.174 |
| 2015 | 0.273 |
| 2014 | 0.098 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
