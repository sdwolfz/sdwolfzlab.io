---
title: "IHS MARKIT LTD (INFO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>IHS MARKIT LTD</td></tr>
    <tr><td>Symbol</td><td>INFO</td></tr>
    <tr><td>Web</td><td><a href="https://www.ihsmarkit.com">www.ihsmarkit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.4 |
| 2020 | 0.68 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
