---
title: "VIOMI TECHNOLOGY CO LTD SPON ADS EACH REPR 3 ORD SHS (VIOT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>VIOMI TECHNOLOGY CO LTD SPON ADS EACH REPR 3 ORD SHS</td></tr>
    <tr><td>Symbol</td><td>VIOT</td></tr>
    <tr><td>Web</td><td><a href="http://www.viomi.com.cn">www.viomi.com.cn</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
