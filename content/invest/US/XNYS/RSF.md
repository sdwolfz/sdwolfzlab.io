---
title: "RIVERNORTH SPECIALTY FINANCE CORP (RSF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>RIVERNORTH SPECIALTY FINANCE CORP</td></tr>
    <tr><td>Symbol</td><td>RSF</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.76 |
| 2020 | 1.95 |
| 2019 | 1.26 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
