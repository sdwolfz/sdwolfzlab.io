---
title: "UWM HLDGS CORP (UWMC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>UWM HLDGS CORP</td></tr>
    <tr><td>Symbol</td><td>UWMC</td></tr>
    <tr><td>Web</td><td><a href="https://www.uwm.com">www.uwm.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
