---
title: "Rolls-Royce Holdings (RR.)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Rolls-Royce Holdings</td></tr>
    <tr><td>Symbol</td><td>RR.</td></tr>
    <tr><td>Web</td><td><a href="https://www.rolls-royce.com">www.rolls-royce.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
