---
title: " (AU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>AU</td></tr>
    <tr><td>Web</td><td><a href="https://www.anglogoldashanti.com">www.anglogoldashanti.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.373 |
| 2020 | 0.075 |
| 2019 | 0.054 |
| 2018 | 0.046 |
| 2017 | 0.089 |
| 2014 | 0.003 |
| 2013 | 0.047 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
