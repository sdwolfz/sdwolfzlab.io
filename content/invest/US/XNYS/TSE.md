---
title: "TRINSEO S.A. (TSE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TRINSEO S.A.</td></tr>
    <tr><td>Symbol</td><td>TSE</td></tr>
    <tr><td>Web</td><td><a href="https://www.trinseo.com">www.trinseo.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.16 |
| 2020 | 1.6 |
| 2019 | 1.6 |
| 2018 | 1.52 |
| 2017 | 1.32 |
| 2016 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
