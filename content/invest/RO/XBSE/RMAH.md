---
title: "FARMACEUTICA REMEDIA SA (RMAH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>FARMACEUTICA REMEDIA SA</td></tr>
    <tr><td>Symbol</td><td>RMAH</td></tr>
    <tr><td>Web</td><td><a href="https://www.remedia.ro">www.remedia.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.15 |
| 2019 | 0.02 |
| 2018 | 0.015 |
| 2017 | 0.01 |
| 2016 | 0.0066 |
| 2014 | 0.015 |
| 2013 | 0.015 |
| 2012 | 0.02 |
| 2011 | 0.02 |
| 2010 | 0.01 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
