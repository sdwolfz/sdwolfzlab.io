---
title: "How to configure your Bash history"
date: 2020-04-25T18:43:42Z
type: posts
slug: configure-your-bash-history
tags: ["bash", "linux", "productivity"]
---

## Good

```bash
export HISTCONTROL=ignoreboth:erasedups
export HISTSIZE=5000000
export HISTFILESIZE=5000000
export HISTFILE=/home/$USER/.bash_history_persistent
shopt -s histappend

export PROMPT_COMMAND="history -a; $PROMPT_COMMAND"
```

It's crucial that you change the location of your history file, otherwise it
might get accidentally truncated. You always learn this the hard way.

## Bad

Don't do:

```bash
export PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"
```

It will mess up your `Ctrl + p` and `Ctrl + n` functionality and that is
infinetly more useful than having your command history shared between terminal
tabs.

## Further Reading

Best used with `fzf` as described here:
* [How to Use: fzf](/snippets/how-to-use-fzf/)
