---
title: "COMTECH TELECOMMUNICATIONS (CMTL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COMTECH TELECOMMUNICATIONS</td></tr>
    <tr><td>Symbol</td><td>CMTL</td></tr>
    <tr><td>Web</td><td><a href="https://www.comtechtel.com">www.comtechtel.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.4 |
| 2019 | 0.4 |
| 2018 | 0.4 |
| 2017 | 0.4 |
| 2016 | 1.2 |
| 2015 | 1.2 |
| 2014 | 1.2 |
| 2013 | 0.825 |
| 2012 | 1.375 |
| 2011 | 1.025 |
| 2010 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
