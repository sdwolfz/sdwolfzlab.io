---
title: "How to Use: envsubst"
date: 2020-06-12T22:29:13Z
type: posts
slug: how-to-use-envsubst
series: ["How to Use"]
tags: ["envsubst", "tutorial", "linux", "bash", "productivity"]
---

## Install

```sh
# Manjaro / Arch
sudo pacman -S gettext

# Alpine
apk add --update gettext
```

## Usage

Create a template input file `./truth.template` containing:

```sh
Pizza is ${WHAT}
```

Export your environment variable:

```sh
export WHAT=love
```

Substitute it inside an output file:
```sh
envsubst < ./truth.template > ./output.file
```

You can see the result:
```sh
cat ./output.file
# Pizza is love
```

## Real world usage

Use it to generate random passwords and save them in env files:

```sh
# Generate password
export SECRET_PASSWORD=$(tr -dc "a-zA-Z0-9" < /dev/urandom | head -c 32)

# Generate template
echo 'POSTGRES_PASSWORD=${SECRET_PASSWORD}' > ./secrets.env.template

# Substitute
envsubst < ./secrets.env.template > ./secrets.env

cat ./secrets.env
# POSTGRES_PASSWORD=password1lol
```

Now you can use that secrets.env file in your docker entrypoint like detailed here:
* [ Docker Images: basic entrypoint](/snippets/docker-images-basic-entrypoint/)
