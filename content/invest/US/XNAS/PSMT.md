---
title: "PRICESMART INC (PSMT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PRICESMART INC</td></tr>
    <tr><td>Symbol</td><td>PSMT</td></tr>
    <tr><td>Web</td><td><a href="https://www.pricesmart.com">www.pricesmart.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.7 |
| 2020 | 0.7 |
| 2019 | 0.7 |
| 2018 | 0.7 |
| 2017 | 0.7 |
| 2016 | 0.7 |
| 2015 | 0.7 |
| 2014 | 0.7 |
| 2013 | 0.3 |
| 2012 | 0.9 |
| 2011 | 0.6 |
| 2010 | 0.5 |
| 2009 | 0.5 |
| 2008 | 0.32 |
| 2007 | 0.32 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
