---
title: "Hilton Foods (HFG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Hilton Foods</td></tr>
    <tr><td>Symbol</td><td>HFG</td></tr>
    <tr><td>Web</td><td><a href="https://www.hiltonfoodgroupplc.com">www.hiltonfoodgroupplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 21.4 |
| 2018 | 21.4 |
| 2017 | 19.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
