---
title: "BLACKBAUD INC (BLKB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BLACKBAUD INC</td></tr>
    <tr><td>Symbol</td><td>BLKB</td></tr>
    <tr><td>Web</td><td><a href="https://www.blackbaud.com">www.blackbaud.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.12 |
| 2019 | 0.48 |
| 2018 | 0.48 |
| 2017 | 0.48 |
| 2016 | 0.48 |
| 2015 | 0.48 |
| 2014 | 0.48 |
| 2013 | 0.48 |
| 2012 | 0.48 |
| 2011 | 0.48 |
| 2010 | 0.44 |
| 2009 | 0.4 |
| 2008 | 0.4 |
| 2007 | 0.34 |
| 2006 | 0.28 |
| 2005 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
