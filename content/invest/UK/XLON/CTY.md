---
title: "City Of London Investment (CTY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>City Of London Investment</td></tr>
    <tr><td>Symbol</td><td>CTY</td></tr>
    <tr><td>Web</td><td><a href="https://www.janushenderson.com">www.janushenderson.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 19.0 |
| 2019 | 18.6 |
| 2018 | 17.7 |
| 2017 | 16.7 |
| 2016 | 15.9 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
