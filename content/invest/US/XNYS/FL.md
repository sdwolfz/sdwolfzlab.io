---
title: "FOOT LOCKER INC (FL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FOOT LOCKER INC</td></tr>
    <tr><td>Symbol</td><td>FL</td></tr>
    <tr><td>Web</td><td><a href="https://www.footlocker-inc.com">www.footlocker-inc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.35 |
| 2020 | 0.93 |
| 2019 | 1.485 |
| 2018 | 1.345 |
| 2017 | 1.205 |
| 2016 | 1.075 |
| 2015 | 0.97 |
| 2014 | 0.86 |
| 2013 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
