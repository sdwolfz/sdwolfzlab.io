---
title: "PENSKE AUTOMOTIVE GROUP INC (PAG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PENSKE AUTOMOTIVE GROUP INC</td></tr>
    <tr><td>Symbol</td><td>PAG</td></tr>
    <tr><td>Web</td><td><a href="https://www.penskeautomotive.com">www.penskeautomotive.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.43 |
| 2020 | 0.84 |
| 2019 | 1.58 |
| 2018 | 1.42 |
| 2017 | 1.26 |
| 2016 | 1.1 |
| 2015 | 0.94 |
| 2014 | 0.78 |
| 2013 | 0.33 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
