---
title: "ALLIED MOTION TECHNOLGIES INC (AMOT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ALLIED MOTION TECHNOLGIES INC</td></tr>
    <tr><td>Symbol</td><td>AMOT</td></tr>
    <tr><td>Web</td><td><a href="https://www.alliedmotion.com">www.alliedmotion.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.055 |
| 2020 | 0.12 |
| 2019 | 0.12 |
| 2018 | 0.115 |
| 2017 | 0.1 |
| 2016 | 0.1 |
| 2015 | 0.1 |
| 2014 | 0.1 |
| 2013 | 0.1 |
| 2012 | 0.1 |
| 2011 | 0.04 |
| 1995 | 0.1 |
| 1994 | 0.22 |
| 1993 | 0.105 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
