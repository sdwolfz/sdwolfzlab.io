---
title: "SOMPLAST SA NASAUD (SOPL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>SOMPLAST SA NASAUD</td></tr>
    <tr><td>Symbol</td><td>SOPL</td></tr>
    <tr><td>Web</td><td><a href="https://www.somplast.ro">www.somplast.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
