---
title: "PATHFINDER BANCORP INC (PBHC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PATHFINDER BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>PBHC</td></tr>
    <tr><td>Web</td><td><a href="https://www.pathfinderbank.com">www.pathfinderbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.13 |
| 2020 | 0.24 |
| 2019 | 0.24 |
| 2018 | 0.238 |
| 2017 | 0.208 |
| 2016 | 0.2 |
| 2015 | 0.14 |
| 2014 | 0.12 |
| 2013 | 0.12 |
| 2012 | 0.12 |
| 2011 | 0.12 |
| 2010 | 0.12 |
| 2009 | 0.193 |
| 2008 | 0.41 |
| 2007 | 0.41 |
| 2006 | 0.41 |
| 2005 | 0.308 |
| 2004 | 0.404 |
| 2003 | 0.4 |
| 2002 | 0.3 |
| 2001 | 0.26 |
| 2000 | 0.24 |
| 1999 | 0.23 |
| 1998 | 0.2 |
| 1997 | 0.21 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
