---
title: " (AJX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>AJX</td></tr>
    <tr><td>Web</td><td><a href="https://www.greatajax.com">www.greatajax.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.17 |
| 2020 | 0.83 |
| 2019 | 1.33 |
| 2018 | 1.22 |
| 2017 | 1.13 |
| 2016 | 0.99 |
| 2015 | 0.64 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
