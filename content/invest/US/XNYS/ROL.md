---
title: "ROLLINS INC (ROL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ROLLINS INC</td></tr>
    <tr><td>Symbol</td><td>ROL</td></tr>
    <tr><td>Web</td><td><a href="https://www.rollins.com">www.rollins.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.16 |
| 2020 | 0.42 |
| 2019 | 0.47 |
| 2018 | 0.607 |
| 2017 | 0.56 |
| 2016 | 0.5 |
| 2015 | 0.42 |
| 2014 | 0.42 |
| 2013 | 0.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
