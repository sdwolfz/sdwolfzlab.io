---
title: "DAKTRONIC INC (DAKT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>DAKTRONIC INC</td></tr>
    <tr><td>Symbol</td><td>DAKT</td></tr>
    <tr><td>Web</td><td><a href="https://www.daktronics.com">www.daktronics.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.05 |
| 2019 | 0.22 |
| 2018 | 0.28 |
| 2017 | 0.28 |
| 2016 | 0.34 |
| 2015 | 0.4 |
| 2014 | 0.39 |
| 2013 | 0.3 |
| 2012 | 0.73 |
| 2011 | 0.62 |
| 2010 | 0.6 |
| 2009 | 0.095 |
| 2008 | 0.09 |
| 2007 | 0.07 |
| 2006 | 0.12 |
| 2005 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
