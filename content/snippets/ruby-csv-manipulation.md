---
title: "Ruby CSV manipulation"
date: 2021-05-22T12:27:56Z
type: posts
slug: ruby-csv-manipulation
tags: ["ruby", "csv"]
---

## Read

```ruby
file = CSV.open('path/to/file.csv', col_sep: ',')
file.each do |line|
  pp line
end
```

## Write

```ruby
header  = ['A', 'B', 'C']
content = [[1, 2, 3], [4, 5, 6]]

csv = CSV.generate do |output|
  output << header

  content.each do |item|
    output << item
  end
end

File.write('result.csv', csv)
```
