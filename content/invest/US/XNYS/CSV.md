---
title: "CARRIAGE SERVICES INC (CSV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CARRIAGE SERVICES INC</td></tr>
    <tr><td>Symbol</td><td>CSV</td></tr>
    <tr><td>Web</td><td><a href="https://www.carriageservices.com">www.carriageservices.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.337 |
| 2019 | 0.3 |
| 2018 | 0.3 |
| 2017 | 0.225 |
| 2016 | 0.15 |
| 2015 | 0.1 |
| 2014 | 0.1 |
| 2013 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
