---
title: "SCHLUMBERGER LIMITED (SLB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SCHLUMBERGER LIMITED</td></tr>
    <tr><td>Symbol</td><td>SLB</td></tr>
    <tr><td>Web</td><td><a href="https://www.slb.com">www.slb.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 0.875 |
| 2019 | 2.0 |
| 2018 | 2.0 |
| 2017 | 2.0 |
| 2016 | 2.0 |
| 2015 | 2.0 |
| 2014 | 1.6 |
| 2013 | 0.626 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
