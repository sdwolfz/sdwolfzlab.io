---
title: "FAST ACQUISITION CORP (FST)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FAST ACQUISITION CORP</td></tr>
    <tr><td>Symbol</td><td>FST</td></tr>
    <tr><td>Web</td><td><a href="https://www.fastacq.com">www.fastacq.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
