---
title: "SIFI CJ LOGISTIC SA (CACU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>SIFI CJ LOGISTIC SA</td></tr>
    <tr><td>Symbol</td><td>CACU</td></tr>
    <tr><td>Web</td><td><a href="https://www.comatcluj.ro">www.comatcluj.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.49 |
| 2018 | 0.36 |
| 2017 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
