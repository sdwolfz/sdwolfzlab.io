---
title: "VAIL RESORTS INC (MTN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>VAIL RESORTS INC</td></tr>
    <tr><td>Symbol</td><td>MTN</td></tr>
    <tr><td>Web</td><td><a href="https://www.vailresorts.com">www.vailresorts.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.76 |
| 2019 | 7.04 |
| 2018 | 5.88 |
| 2017 | 4.212 |
| 2016 | 3.24 |
| 2015 | 2.492 |
| 2014 | 1.66 |
| 2013 | 0.621 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
