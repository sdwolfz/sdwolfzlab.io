---
title: "Ashmore (ASHM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Ashmore</td></tr>
    <tr><td>Symbol</td><td>ASHM</td></tr>
    <tr><td>Web</td><td><a href="https://www.ashmoregroup.com">www.ashmoregroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 16.9 |
| 2019 | 16.65 |
| 2018 | 16.65 |
| 2017 | 16.7 |
| 2016 | 16.65 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
