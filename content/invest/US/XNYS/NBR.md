---
title: "NABORS INDUSTRIES LTD (NBR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NABORS INDUSTRIES LTD</td></tr>
    <tr><td>Symbol</td><td>NBR</td></tr>
    <tr><td>Web</td><td><a href="https://www.nabors.com">www.nabors.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.01 |
| 2019 | 0.04 |
| 2018 | 0.24 |
| 2017 | 0.24 |
| 2016 | 0.24 |
| 2015 | 0.18 |
| 2014 | 0.2 |
| 2013 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
