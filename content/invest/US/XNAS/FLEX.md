---
title: "FLEX LTD (FLEX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FLEX LTD</td></tr>
    <tr><td>Symbol</td><td>FLEX</td></tr>
    <tr><td>Web</td><td><a href="https://www.flex.com">www.flex.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
