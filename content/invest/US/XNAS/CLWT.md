---
title: "EURO TECH HLDGS (CLWT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>EURO TECH HLDGS</td></tr>
    <tr><td>Symbol</td><td>CLWT</td></tr>
    <tr><td>Web</td><td><a href="https://www.euro-tech.com">www.euro-tech.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.42 |
| 2018 | 0.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
