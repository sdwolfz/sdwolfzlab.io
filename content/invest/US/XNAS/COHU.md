---
title: "COHU INC (COHU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COHU INC</td></tr>
    <tr><td>Symbol</td><td>COHU</td></tr>
    <tr><td>Web</td><td><a href="https://www.cohu.com">www.cohu.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.06 |
| 2019 | 0.24 |
| 2018 | 0.24 |
| 2017 | 0.24 |
| 2016 | 0.24 |
| 2015 | 0.24 |
| 2014 | 0.24 |
| 2013 | 0.24 |
| 2012 | 0.24 |
| 2011 | 0.24 |
| 2010 | 0.24 |
| 2009 | 0.24 |
| 2008 | 0.24 |
| 2007 | 0.24 |
| 2006 | 0.24 |
| 2005 | 0.22 |
| 2004 | 0.2 |
| 2003 | 0.2 |
| 2002 | 0.2 |
| 2001 | 0.2 |
| 2000 | 0.2 |
| 1999 | 0.315 |
| 1998 | 0.32 |
| 1997 | 0.24 |
| 1996 | 0.2 |
| 1995 | 0.19 |
| 1994 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
