---
title: "SONY CORP SPON ADR EACH REPR 1 ORD (SNE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SONY CORP SPON ADR EACH REPR 1 ORD</td></tr>
    <tr><td>Symbol</td><td>SNE</td></tr>
    <tr><td>Web</td><td><a href="http://www.sony.net">www.sony.net</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.372 |
| 2019 | 0.267 |
| 2018 | 0.192 |
| 2017 | 0.173 |
| 2016 | 0.058 |
| 2014 | 0.105 |
| 2013 | 0.225 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
