---
title: "FIRST FINANCIAL NORTHWEST INC (FFNW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST FINANCIAL NORTHWEST INC</td></tr>
    <tr><td>Symbol</td><td>FFNW</td></tr>
    <tr><td>Web</td><td><a href="https://www.ffnwb.com">www.ffnwb.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.11 |
| 2020 | 0.4 |
| 2019 | 0.35 |
| 2018 | 0.31 |
| 2017 | 0.27 |
| 2016 | 0.24 |
| 2015 | 0.24 |
| 2014 | 0.2 |
| 2013 | 0.12 |
| 2010 | 0.085 |
| 2009 | 0.34 |
| 2008 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
