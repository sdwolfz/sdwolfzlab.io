---
title: "BCE INC (BCE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BCE INC</td></tr>
    <tr><td>Symbol</td><td>BCE</td></tr>
    <tr><td>Web</td><td><a href="https://www.bce.ca">www.bce.ca</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 3.117 |
| 2019 | 2.387 |
| 2018 | 2.295 |
| 2017 | 2.789 |
| 2016 | 2.054 |
| 2015 | 2.436 |
| 2014 | 2.472 |
| 2013 | 1.164 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
