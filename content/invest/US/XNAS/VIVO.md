---
title: "MERIDIAN BIOSCIENCE INC COM (VIVO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MERIDIAN BIOSCIENCE INC COM</td></tr>
    <tr><td>Symbol</td><td>VIVO</td></tr>
    <tr><td>Web</td><td><a href="https://www.meridianbioscience.com">www.meridianbioscience.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.125 |
| 2018 | 0.5 |
| 2017 | 0.5 |
| 2016 | 0.8 |
| 2015 | 0.8 |
| 2014 | 0.8 |
| 2013 | 0.57 |
| 2012 | 0.95 |
| 2011 | 0.76 |
| 2010 | 0.76 |
| 2009 | 0.68 |
| 2008 | 0.56 |
| 2007 | 0.54 |
| 2006 | 0.46 |
| 2005 | 0.44 |
| 2004 | 0.4 |
| 2003 | 0.36 |
| 2002 | 0.28 |
| 2001 | 0.26 |
| 2000 | 0.24 |
| 1999 | 0.2 |
| 1998 | 0.2 |
| 1997 | 0.195 |
| 1996 | 0.165 |
| 1995 | 0.172 |
| 1994 | 0.12 |
| 1993 | 0.1 |
| 1992 | 0.075 |
| 1991 | 0.1 |
| 1990 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
