---
title: "HERITAGE COMMERCE CORP (HTBK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HERITAGE COMMERCE CORP</td></tr>
    <tr><td>Symbol</td><td>HTBK</td></tr>
    <tr><td>Web</td><td><a href="https://www.heritagecommercecorp.com">www.heritagecommercecorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.26 |
| 2020 | 0.52 |
| 2019 | 0.48 |
| 2018 | 0.44 |
| 2017 | 0.4 |
| 2016 | 0.36 |
| 2015 | 0.32 |
| 2014 | 0.18 |
| 2013 | 0.06 |
| 2009 | 0.02 |
| 2008 | 0.32 |
| 2007 | 0.26 |
| 2006 | 0.2 |
| 2005 | 0.001 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
