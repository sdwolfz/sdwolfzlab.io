---
title: "AMPLIFY ENERGY CORP (AMPY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AMPLIFY ENERGY CORP</td></tr>
    <tr><td>Symbol</td><td>AMPY</td></tr>
    <tr><td>Web</td><td><a href="https://www.amplifyenergy.com">www.amplifyenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.1 |
| 2019 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
