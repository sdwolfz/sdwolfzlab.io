---
title: "CUMMINS INC (CMI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CUMMINS INC</td></tr>
    <tr><td>Symbol</td><td>CMI</td></tr>
    <tr><td>Web</td><td><a href="https://www.cummins.com">www.cummins.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.35 |
| 2020 | 5.283 |
| 2019 | 4.902 |
| 2018 | 4.44 |
| 2017 | 4.21 |
| 2016 | 4.0 |
| 2015 | 3.51 |
| 2014 | 2.81 |
| 2013 | 1.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
