---
title: "TECHNICAL COMMUNICATIONS CORP (TCCO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TECHNICAL COMMUNICATIONS CORP</td></tr>
    <tr><td>Symbol</td><td>TCCO</td></tr>
    <tr><td>Web</td><td><a href="http://www.tccsecure.com">www.tccsecure.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2012 | 0.4 |
| 2011 | 0.4 |
| 2010 | 2.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
