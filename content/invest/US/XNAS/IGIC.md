---
title: "INTERNATIONAL GENERAL INS HLDGS LTD (IGIC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>INTERNATIONAL GENERAL INS HLDGS LTD</td></tr>
    <tr><td>Symbol</td><td>IGIC</td></tr>
    <tr><td>Web</td><td><a href="https://www.iginsure.com">www.iginsure.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.17 |
| 2020 | 0.09 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
