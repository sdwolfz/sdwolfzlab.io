---
title: "Pacman cheatsheet"
date: 2020-11-22T12:05:44Z
type: posts
slug: pacman-cheatsheet
series: ["Cheatsheets"]
tags: ["linux", "pacman"]
---

## Install

1. Update all pacakges:

```sh
sudo pacman -Syu
```

2. Install a package:

```sh
sudo pacman -S packagename
```

3. Install a package from a file:

```sh
sudo pacman -U /path/to/package
```

## Remove

1. Remove a package:

```sh
sudo pacman -Rns packagename
```

2. Remove orphan packages:

```sh
sudo pacman -Rns $(pacman -Qtdq)
```

3. Clear cache:

```sh
sudo paccache -r
```

4. Remove uninstalled packages from cache:

```sh
sudo pacman -Sc
```

## Search

1. Search for a package:

```sh
sudo pacman -Ss package
```

2. Search for a local file:

```sh
sudo pacman -Qo filename
```

3. Search for a remote file:

```sh
# First sync the file database
sudo pacman -Fy

# Then you can search
sudo pacman -F filename
```

## Display

1. View all installed pacakges:

```sh
sudo pacman -Q
```

2. View all packages explicitly installed:

```sh
sudo pacman -Qet
```

3. View package dependencies:

```sh
# As a list
pactree -u packagename

# As a tree
pactree packagename
```

4. View package reverse dependencies

```sh
pacman -Sii packagename
```

5. List orphan packages:

```sh
sudo pacman -Qtdq
```

6. Inspect package metadata:

```sh
pacman -Qi packagename
```

## Configure

1. Install keyring:

```sh
sudo pacman -Sy --noconfirm archlinux-keyring manjaro-keyring
```

2. Populate keyring:

```sh
sudo pacman-key --populate archlinux manjaro
```

3. Refresh keys:

```sh
sudo pacman-key --refresh-keys
```

4. Use the closest mirrors:

```sh
sudo pacman-mirrors --geoip
```

## Other

The rest, you can find it here:
* https://wiki.archlinux.org/index.php/Pacman
