---
title: "GARMIN LTD (GRMN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GARMIN LTD</td></tr>
    <tr><td>Symbol</td><td>GRMN</td></tr>
    <tr><td>Web</td><td><a href="https://www.garmin.com">www.garmin.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.61 |
| 2020 | 2.4 |
| 2019 | 2.24 |
| 2018 | 2.1 |
| 2017 | 2.04 |
| 2016 | 2.04 |
| 2015 | 2.01 |
| 2014 | 1.89 |
| 2013 | 1.8 |
| 2012 | 1.75 |
| 2011 | 1.6 |
| 2010 | 1.501 |
| 2009 | 0.75 |
| 2008 | 0.75 |
| 2007 | 0.75 |
| 2006 | 0.5 |
| 2005 | 0.5 |
| 2004 | 0.5 |
| 2003 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
