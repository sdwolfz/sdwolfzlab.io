---
title: "Johnson Matthey (JMAT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Johnson Matthey</td></tr>
    <tr><td>Symbol</td><td>JMAT</td></tr>
    <tr><td>Web</td><td><a href="https://www.matthey.com">www.matthey.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 55.63 |
| 2019 | 85.5 |
| 2018 | 80.0 |
| 2017 | 75.0 |
| 2016 | 71.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
