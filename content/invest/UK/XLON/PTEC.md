---
title: "Playtech (PTEC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Playtech</td></tr>
    <tr><td>Symbol</td><td>PTEC</td></tr>
    <tr><td>Web</td><td><a href="https://www.playtech.com">www.playtech.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 18.1 |
| 2018 | 24.1 |
| 2017 | 36.0 |
| 2016 | 32.7 |
| 2015 | 28.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
