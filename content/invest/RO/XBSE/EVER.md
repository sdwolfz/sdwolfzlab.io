---
title: "EVERGENT INVESTMENTS S.A. (EVER)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>EVERGENT INVESTMENTS S.A.</td></tr>
    <tr><td>Symbol</td><td>EVER</td></tr>
    <tr><td>Web</td><td><a href="https://www.evergent.ro">www.evergent.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.043 |
| 2019 | 0.06 |
| 2018 | 0.0304 |
| 2017 | 0.05 |
| 2016 | 0.044 |
| 2015 | 0.045 |
| 2014 | 0.1012 |
| 2013 | 0.066 |
| 2012 | 0.24 |
| 2011 | 0.22 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
