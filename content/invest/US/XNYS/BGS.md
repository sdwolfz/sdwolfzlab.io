---
title: "B & G FOODS INC (BGS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>B & G FOODS INC</td></tr>
    <tr><td>Symbol</td><td>BGS</td></tr>
    <tr><td>Web</td><td><a href="https://www.bgfoods.com">www.bgfoods.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.475 |
| 2020 | 1.9 |
| 2019 | 1.9 |
| 2018 | 1.89 |
| 2017 | 1.86 |
| 2016 | 1.725 |
| 2015 | 1.38 |
| 2014 | 1.36 |
| 2013 | 0.65 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
