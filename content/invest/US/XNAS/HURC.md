---
title: "HURCO COMPANIES INC (HURC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HURCO COMPANIES INC</td></tr>
    <tr><td>Symbol</td><td>HURC</td></tr>
    <tr><td>Web</td><td><a href="https://www.hurco.com">www.hurco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.27 |
| 2020 | 0.39 |
| 2019 | 0.59 |
| 2018 | 0.43 |
| 2017 | 0.3 |
| 2016 | 0.36 |
| 2015 | 0.32 |
| 2014 | 0.28 |
| 2013 | 0.15 |
| 1992 | 0.14 |
| 1991 | 0.2 |
| 1990 | 0.2 |
| 1989 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
