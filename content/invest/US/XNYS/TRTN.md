---
title: "TRITON INTL LTD (TRTN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TRITON INTL LTD</td></tr>
    <tr><td>Symbol</td><td>TRTN</td></tr>
    <tr><td>Web</td><td><a href="https://www.trtn.com">www.trtn.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.14 |
| 2020 | 2.13 |
| 2019 | 2.08 |
| 2018 | 2.01 |
| 2017 | 1.8 |
| 2016 | 1.35 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
