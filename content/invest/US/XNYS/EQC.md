---
title: " (EQC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>EQC</td></tr>
    <tr><td>Web</td><td><a href="https://www.eqcre.com">www.eqcre.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
