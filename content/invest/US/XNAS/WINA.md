---
title: "WINMARK CORPORATION (WINA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>WINMARK CORPORATION</td></tr>
    <tr><td>Symbol</td><td>WINA</td></tr>
    <tr><td>Web</td><td><a href="https://www.winmarkcorporation.com">www.winmarkcorporation.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.7 |
| 2020 | 3.8 |
| 2019 | 0.9 |
| 2018 | 0.56 |
| 2017 | 0.43 |
| 2016 | 0.37 |
| 2015 | 0.27 |
| 2014 | 5.23 |
| 2013 | 0.19 |
| 2012 | 5.15 |
| 2011 | 0.11 |
| 2010 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
