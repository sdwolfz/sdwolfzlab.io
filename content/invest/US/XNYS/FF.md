---
title: "FUTUREFUEL CORP (FF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FUTUREFUEL CORP</td></tr>
    <tr><td>Symbol</td><td>FF</td></tr>
    <tr><td>Web</td><td><a href="https://www.futurefuelcorporation.com">www.futurefuelcorporation.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 3.24 |
| 2019 | 0.24 |
| 2018 | 0.24 |
| 2017 | 0.24 |
| 2016 | 0.24 |
| 2015 | 0.24 |
| 2014 | 0.48 |
| 2013 | 0.22 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
