---
title: "Shaftesbury (SHB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Shaftesbury</td></tr>
    <tr><td>Symbol</td><td>SHB</td></tr>
    <tr><td>Web</td><td><a href="https://www.shaftesbury.co.uk">www.shaftesbury.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 8.7 |
| 2019 | 17.7 |
| 2018 | 16.8 |
| 2017 | 16.0 |
| 2016 | 14.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
