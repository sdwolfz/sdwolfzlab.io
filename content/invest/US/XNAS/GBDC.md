---
title: "GOLUB CAPITAL BDC INC (GBDC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GOLUB CAPITAL BDC INC</td></tr>
    <tr><td>Symbol</td><td>GBDC</td></tr>
    <tr><td>Web</td><td><a href="https://golubcapitalbdc.com">golubcapitalbdc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.29 |
| 2020 | 1.2 |
| 2019 | 1.42 |
| 2018 | 1.4 |
| 2017 | 1.36 |
| 2016 | 1.53 |
| 2015 | 1.28 |
| 2014 | 1.28 |
| 2013 | 1.28 |
| 2012 | 1.28 |
| 2011 | 1.28 |
| 2010 | 0.86 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
