---
title: "HOME FED BANCORP INE LA NEW (HFBL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HOME FED BANCORP INE LA NEW</td></tr>
    <tr><td>Symbol</td><td>HFBL</td></tr>
    <tr><td>Web</td><td><a href="https://www.hfbla.com">www.hfbla.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.248 |
| 2020 | 0.65 |
| 2019 | 0.6 |
| 2018 | 0.52 |
| 2017 | 0.42 |
| 2016 | 0.34 |
| 2015 | 0.3 |
| 2014 | 0.26 |
| 2013 | 0.24 |
| 2012 | 0.24 |
| 2011 | 0.24 |
| 2010 | 0.24 |
| 2009 | 0.24 |
| 2008 | 0.24 |
| 2007 | 0.24 |
| 2006 | 0.24 |
| 2005 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
