---
title: "TRADEWEB MARKETS INC (TW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TRADEWEB MARKETS INC</td></tr>
    <tr><td>Symbol</td><td>TW</td></tr>
    <tr><td>Web</td><td><a href="https://www.tradeweb.com">www.tradeweb.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.16 |
| 2020 | 0.32 |
| 2019 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
