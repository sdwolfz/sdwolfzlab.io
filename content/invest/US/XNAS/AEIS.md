---
title: "ADVANCED ENERGY INDUSTRIES (AEIS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ADVANCED ENERGY INDUSTRIES</td></tr>
    <tr><td>Symbol</td><td>AEIS</td></tr>
    <tr><td>Web</td><td><a href="https://www.advancedenergy.com">www.advancedenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
