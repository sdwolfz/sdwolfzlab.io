---
title: "REINSURANCE GROUP OF AMERICA (RGA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>REINSURANCE GROUP OF AMERICA</td></tr>
    <tr><td>Symbol</td><td>RGA</td></tr>
    <tr><td>Web</td><td><a href="https://www.rgare.com">www.rgare.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.7 |
| 2020 | 2.8 |
| 2019 | 2.6 |
| 2018 | 2.2 |
| 2017 | 1.82 |
| 2016 | 1.56 |
| 2015 | 1.4 |
| 2014 | 1.26 |
| 2013 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
