---
title: "GALLAGHER(ARTHUR J.)& CO (AJG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GALLAGHER(ARTHUR J.)& CO</td></tr>
    <tr><td>Symbol</td><td>AJG</td></tr>
    <tr><td>Web</td><td><a href="https://www.ajg.com">www.ajg.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.96 |
| 2020 | 1.8 |
| 2019 | 1.72 |
| 2018 | 1.64 |
| 2017 | 1.56 |
| 2016 | 1.52 |
| 2015 | 1.48 |
| 2014 | 1.44 |
| 2013 | 0.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
