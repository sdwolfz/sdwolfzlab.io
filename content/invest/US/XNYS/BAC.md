---
title: "BANK OF AMERICA CORPORATION (BAC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BANK OF AMERICA CORPORATION</td></tr>
    <tr><td>Symbol</td><td>BAC</td></tr>
    <tr><td>Web</td><td><a href="https://www.bankofamerica.com">www.bankofamerica.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.36 |
| 2020 | 0.72 |
| 2019 | 0.66 |
| 2018 | 0.54 |
| 2017 | 0.39 |
| 2016 | 0.25 |
| 2015 | 0.2 |
| 2014 | 0.12 |
| 2013 | 0.02 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
