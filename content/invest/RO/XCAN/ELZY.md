---
title: "ELECTROPRECIZIA SA Sacele (ELZY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>ELECTROPRECIZIA SA Sacele</td></tr>
    <tr><td>Symbol</td><td>ELZY</td></tr>
    <tr><td>Web</td><td><a href="https://www.electroprecizia.ro">www.electroprecizia.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.4 |
| 2019 | 0.25 |
| 2018 | 0.4 |
| 2016 | 0.4 |
| 2015 | 0.203 |
| 2014 | 0.346 |
| 2013 | 1.19 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
