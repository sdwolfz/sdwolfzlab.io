---
title: "APTARGROUP INC (ATR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>APTARGROUP INC</td></tr>
    <tr><td>Symbol</td><td>ATR</td></tr>
    <tr><td>Web</td><td><a href="https://www.aptar.com">www.aptar.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.74 |
| 2020 | 1.44 |
| 2019 | 1.42 |
| 2018 | 1.32 |
| 2017 | 1.28 |
| 2016 | 1.22 |
| 2015 | 1.14 |
| 2014 | 1.09 |
| 2013 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
