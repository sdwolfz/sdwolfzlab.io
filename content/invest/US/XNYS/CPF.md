---
title: "CENTRAL PACIFIC FINANCIAL CORP (CPF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CENTRAL PACIFIC FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>CPF</td></tr>
    <tr><td>Web</td><td><a href="https://www.cpb.bank">www.cpb.bank</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.47 |
| 2020 | 0.92 |
| 2019 | 0.9 |
| 2018 | 0.82 |
| 2017 | 0.7 |
| 2016 | 0.6 |
| 2015 | 0.82 |
| 2014 | 0.36 |
| 2013 | 0.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
