---
title: "Saga (SAGA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Saga</td></tr>
    <tr><td>Symbol</td><td>SAGA</td></tr>
    <tr><td>Web</td><td><a href="https://www.saga.co.uk">www.saga.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.3 |
| 2019 | 4.0 |
| 2018 | 9.0 |
| 2017 | 8.5 |
| 2016 | 7.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
