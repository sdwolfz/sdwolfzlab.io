---
title: "FIRST FINANCIAL BANCORP (FFBC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST FINANCIAL BANCORP</td></tr>
    <tr><td>Symbol</td><td>FFBC</td></tr>
    <tr><td>Web</td><td><a href="https://www.bankatfirst.com">www.bankatfirst.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.46 |
| 2020 | 0.92 |
| 2019 | 0.9 |
| 2018 | 0.78 |
| 2017 | 0.68 |
| 2016 | 0.64 |
| 2015 | 0.64 |
| 2014 | 0.61 |
| 2013 | 0.94 |
| 2012 | 1.18 |
| 2011 | 0.78 |
| 2010 | 0.4 |
| 2009 | 0.4 |
| 2008 | 0.68 |
| 2007 | 0.65 |
| 2006 | 0.64 |
| 2005 | 0.64 |
| 2004 | 0.6 |
| 2003 | 0.6 |
| 2002 | 0.6 |
| 2001 | 0.45 |
| 2000 | 0.6 |
| 1999 | 0.45 |
| 1998 | 0.7 |
| 1997 | 0.9 |
| 1996 | 1.2 |
| 1995 | 1.08 |
| 1994 | 0.86 |
| 1993 | 1.26 |
| 1992 | 0.99 |
| 1991 | 0.99 |
| 1990 | 1.23 |
| 1989 | 0.9 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
