---
title: "How to Use: restclient"
date: 2020-04-03T19:59:54Z
type: posts
slug: how-to-use-restclient
series: ["How to Use"]
tags: ["restclient", "tutorial", "spacemacs", "emacs", "envchain", "productivity"]
---

## Setup

Add the `restclient` layer to your `.spacemacs`:

```elisp
dotspacemacs-configuration-layers '(
  restclient
)
```

## Secrets

Define the following function in your `dotspacemacs/user-config`:

```elisp
(defun restclient-envchain-get (namespace variable)
  (cadr
    (split-string
      (string-trim
        (shell-command-to-string
          (format "envchain %s env | grep ^%s=" namespace variable)))
      "=")))
```

## Usage

With secrets:

```restclient
:token := (restclient-envchain-get "secrets" "MY_TOKEN")

GET https://www.codrut.pro/?token=:token
```

Normal GET:

```restclient
# ------------------------------------------------------------------------------

GET https://www.codrut.pro/page/endpoint
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:88.0) Gecko/20100101 Firefox/88.0
```

Normal POST:

```restclient
# ------------------------------------------------------------------------------

POST https://www.codrut.pro/api/endpoint
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:88.0) Gecko/20100101 Firefox/88.0
Content-Type: application/json

{
  "name": "Codrut",
  "lang": "en"
}
```
