---
title: "MATSON INC (MATX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MATSON INC</td></tr>
    <tr><td>Symbol</td><td>MATX</td></tr>
    <tr><td>Web</td><td><a href="https://www.matson.com">www.matson.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.46 |
| 2020 | 0.9 |
| 2019 | 0.86 |
| 2018 | 0.82 |
| 2017 | 0.78 |
| 2016 | 0.74 |
| 2015 | 0.7 |
| 2014 | 0.66 |
| 2013 | 0.32 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
