---
title: "ACME UNITED CORP (ACU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ACME UNITED CORP</td></tr>
    <tr><td>Symbol</td><td>ACU</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.26 |
| 2020 | 0.48 |
| 2019 | 0.48 |
| 2018 | 0.44 |
| 2017 | 0.42 |
| 2016 | 0.4 |
| 2015 | 0.36 |
| 2014 | 0.33 |
| 2013 | 0.23 |
| 2012 | 0.35 |
| 2011 | 0.25 |
| 2010 | 0.21 |
| 2009 | 0.2 |
| 2008 | 0.17 |
| 2007 | 0.19 |
| 2006 | 0.12 |
| 2005 | 0.1 |
| 2004 | 0.04 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
