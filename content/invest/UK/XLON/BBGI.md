---
title: "BBGI (BBGI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>BBGI</td></tr>
    <tr><td>Symbol</td><td>BBGI</td></tr>
    <tr><td>Web</td><td><a href="https://www.bb-gi.com">www.bb-gi.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 6.88 |
| 2018 | 6.63 |
| 2017 | 6.38 |
| 2016 | 3.13 |
| 2015 | 6.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
