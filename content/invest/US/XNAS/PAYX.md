---
title: "PAYCHEX INC (PAYX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PAYCHEX INC</td></tr>
    <tr><td>Symbol</td><td>PAYX</td></tr>
    <tr><td>Web</td><td><a href="https://www.paychex.com">www.paychex.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.28 |
| 2020 | 2.48 |
| 2019 | 2.42 |
| 2018 | 2.18 |
| 2017 | 1.92 |
| 2016 | 1.76 |
| 2015 | 1.6 |
| 2014 | 1.46 |
| 2013 | 0.7 |
| 2012 | 1.95 |
| 2011 | 1.25 |
| 2010 | 1.24 |
| 2009 | 1.24 |
| 2008 | 1.22 |
| 2007 | 1.02 |
| 2006 | 0.69 |
| 2005 | 0.55 |
| 2004 | 0.49 |
| 2003 | 0.45 |
| 2002 | 0.44 |
| 2001 | 0.38 |
| 2000 | 0.33 |
| 1999 | 0.33 |
| 1998 | 0.33 |
| 1997 | 0.33 |
| 1996 | 0.33 |
| 1995 | 0.33 |
| 1994 | 0.27 |
| 1993 | 0.24 |
| 1992 | 0.22 |
| 1991 | 0.21 |
| 1990 | 0.17 |
| 1989 | 0.13 |
| 1988 | 0.03 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
