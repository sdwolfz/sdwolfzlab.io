---
title: "REXNORD CORPORATION (NEW) (RXN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>REXNORD CORPORATION (NEW)</td></tr>
    <tr><td>Symbol</td><td>RXN</td></tr>
    <tr><td>Web</td><td><a href="https://www.rexnordcorporation.com">www.rexnordcorporation.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.18 |
| 2020 | 0.32 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
