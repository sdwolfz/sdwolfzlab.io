---
title: "Aberforth Spli. (ASIT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Aberforth Spli.</td></tr>
    <tr><td>Symbol</td><td>ASIT</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 4.22 |
| 2019 | 7.55 |
| 2018 | 4.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
