---
title: "Menzies (MNZS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Menzies</td></tr>
    <tr><td>Symbol</td><td>MNZS</td></tr>
    <tr><td>Web</td><td><a href="https://www.johnmenziesplc.com">www.johnmenziesplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 20.5 |
| 2016 | 18.5 |
| 2015 | 16.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
