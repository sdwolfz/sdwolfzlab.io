---
title: "UNUM GROUP (UNM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>UNUM GROUP</td></tr>
    <tr><td>Symbol</td><td>UNM</td></tr>
    <tr><td>Web</td><td><a href="https://www.unum.com">www.unum.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.57 |
| 2020 | 1.14 |
| 2019 | 1.09 |
| 2018 | 0.98 |
| 2017 | 0.86 |
| 2016 | 0.77 |
| 2015 | 0.7 |
| 2014 | 0.62 |
| 2013 | 0.29 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
