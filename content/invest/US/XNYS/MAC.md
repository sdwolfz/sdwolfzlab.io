---
title: " (MAC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>MAC</td></tr>
    <tr><td>Web</td><td><a href="https://www.macerich.com">www.macerich.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 1.15 |
| 2019 | 3.0 |
| 2018 | 2.97 |
| 2017 | 2.87 |
| 2016 | 2.75 |
| 2015 | 4.63 |
| 2014 | 2.51 |
| 2013 | 1.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
