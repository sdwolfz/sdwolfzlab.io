---
title: "MGP INGREDIENTS INC NEW (MGPI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MGP INGREDIENTS INC NEW</td></tr>
    <tr><td>Symbol</td><td>MGPI</td></tr>
    <tr><td>Web</td><td><a href="https://www.mgpingredients.com">www.mgpingredients.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 0.48 |
| 2019 | 0.4 |
| 2018 | 0.32 |
| 2017 | 1.01 |
| 2016 | 0.12 |
| 2015 | 0.06 |
| 2014 | 0.05 |
| 2013 | 0.05 |
| 2012 | 0.05 |
| 2011 | 0.05 |
| 2010 | 0.05 |
| 2008 | 0.1 |
| 2007 | 0.25 |
| 2006 | 0.2 |
| 2005 | 0.15 |
| 2004 | 0.15 |
| 2003 | 0.15 |
| 2002 | 0.15 |
| 2001 | 0.15 |
| 2000 | 0.1 |
| 1995 | 0.375 |
| 1994 | 0.5 |
| 1993 | 0.5 |
| 1992 | 0.6 |
| 1991 | 0.7 |
| 1990 | 0.7 |
| 1989 | 0.6 |
| 1988 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
