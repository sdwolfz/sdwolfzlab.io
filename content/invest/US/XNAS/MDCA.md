---
title: "MDC PARTNERS INC (MDCA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MDC PARTNERS INC</td></tr>
    <tr><td>Symbol</td><td>MDCA</td></tr>
    <tr><td>Web</td><td><a href="https://www.mdc-partners.com">www.mdc-partners.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.63 |
| 2015 | 0.84 |
| 2014 | 0.74 |
| 2013 | 0.69 |
| 2012 | 0.7 |
| 2011 | 0.56 |
| 2010 | 0.34 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
