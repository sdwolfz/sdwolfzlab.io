---
title: "BEST BUY CO INC (BBY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BEST BUY CO INC</td></tr>
    <tr><td>Symbol</td><td>BBY</td></tr>
    <tr><td>Web</td><td><a href="https://www.investors.bestbuy.com">www.investors.bestbuy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.7 |
| 2020 | 2.2 |
| 2019 | 2.0 |
| 2018 | 1.8 |
| 2017 | 1.36 |
| 2016 | 1.57 |
| 2015 | 0.92 |
| 2014 | 0.72 |
| 2013 | 0.51 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
