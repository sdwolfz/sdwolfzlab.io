---
title: "APA CORPORATION (APA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>APA CORPORATION</td></tr>
    <tr><td>Symbol</td><td>APA</td></tr>
    <tr><td>Web</td><td><a href="https://apacorp.com">apacorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.05 |
| 2020 | 0.325 |
| 2019 | 1.0 |
| 2018 | 1.0 |
| 2017 | 1.0 |
| 2016 | 1.0 |
| 2015 | 1.0 |
| 2014 | 0.95 |
| 2013 | 0.77 |
| 2012 | 0.66 |
| 2011 | 0.6 |
| 2010 | 0.6 |
| 2009 | 0.6 |
| 2008 | 0.7 |
| 2007 | 0.6 |
| 2006 | 0.45 |
| 2005 | 0.34 |
| 2004 | 0.26 |
| 2003 | 0.42 |
| 2002 | 0.4 |
| 2001 | 0.28 |
| 2000 | 0.14 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
