---
title: "RLI CORP (RLI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>RLI CORP</td></tr>
    <tr><td>Symbol</td><td>RLI</td></tr>
    <tr><td>Web</td><td><a href="https://www.rlicorp.com">www.rlicorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.24 |
| 2020 | 1.95 |
| 2019 | 1.91 |
| 2018 | 1.87 |
| 2017 | 2.58 |
| 2016 | 2.79 |
| 2015 | 2.75 |
| 2014 | 0.71 |
| 2013 | 0.51 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
