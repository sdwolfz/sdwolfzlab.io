---
title: "MARRIOTT VACATIONS WORLDWIDE CORP (VAC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MARRIOTT VACATIONS WORLDWIDE CORP</td></tr>
    <tr><td>Symbol</td><td>VAC</td></tr>
    <tr><td>Web</td><td><a href="https://www.marriottvacationsworldwide.com">www.marriottvacationsworldwide.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.54 |
| 2019 | 1.89 |
| 2018 | 1.65 |
| 2017 | 1.45 |
| 2016 | 1.25 |
| 2015 | 1.05 |
| 2014 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
