---
title: "PEMBINA PIPELINE CORPORATION (PBA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PEMBINA PIPELINE CORPORATION</td></tr>
    <tr><td>Symbol</td><td>PBA</td></tr>
    <tr><td>Web</td><td><a href="https://www.pembina.com">www.pembina.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.165 |
| 2020 | 1.88 |
| 2019 | 1.783 |
| 2018 | 1.735 |
| 2017 | 1.568 |
| 2016 | 1.473 |
| 2015 | 1.729 |
| 2014 | 1.72 |
| 2013 | 0.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
