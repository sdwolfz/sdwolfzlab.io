---
title: "IDACORP INC (IDA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>IDACORP INC</td></tr>
    <tr><td>Symbol</td><td>IDA</td></tr>
    <tr><td>Web</td><td><a href="https://www.idacorpinc.com">www.idacorpinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.42 |
| 2020 | 2.72 |
| 2019 | 2.56 |
| 2018 | 2.4 |
| 2017 | 2.24 |
| 2016 | 2.08 |
| 2015 | 1.92 |
| 2014 | 1.76 |
| 2013 | 0.81 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
