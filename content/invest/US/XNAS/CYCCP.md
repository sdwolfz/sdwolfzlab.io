---
title: " (CYCCP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CYCCP</td></tr>
    <tr><td>Web</td><td><a href="https://www.cyclacel.com">www.cyclacel.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 0.45 |
| 2019 | 0.6 |
| 2018 | 0.6 |
| 2017 | 0.6 |
| 2016 | 0.6 |
| 2015 | 0.45 |
| 2014 | 0.6 |
| 2013 | 0.6 |
| 2011 | 0.3 |
| 2009 | 0.15 |
| 2008 | 0.6 |
| 2007 | 0.6 |
| 2006 | 0.45 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
