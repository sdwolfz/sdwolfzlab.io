---
title: "GEROM SA BUZAU (GROB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>GEROM SA BUZAU</td></tr>
    <tr><td>Symbol</td><td>GROB</td></tr>
    <tr><td>Web</td><td><a href="https://www.gerom-buzau.ro">www.gerom-buzau.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
