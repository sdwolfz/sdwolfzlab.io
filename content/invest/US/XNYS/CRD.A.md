---
title: "CRAWFORD & CO (CRD.A)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CRAWFORD & CO</td></tr>
    <tr><td>Symbol</td><td>CRD.A</td></tr>
    <tr><td>Web</td><td><a href="https://www.crawco.com">www.crawco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.06 |
| 2020 | 0.19 |
| 2019 | 0.28 |
| 2018 | 0.28 |
| 2017 | 0.28 |
| 2016 | 0.28 |
| 2015 | 0.28 |
| 2014 | 0.24 |
| 2013 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
