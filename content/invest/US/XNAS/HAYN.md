---
title: "HAYNES INTERNATIONAL INC (HAYN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HAYNES INTERNATIONAL INC</td></tr>
    <tr><td>Symbol</td><td>HAYN</td></tr>
    <tr><td>Web</td><td><a href="https://www.haynesintl.com">www.haynesintl.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.44 |
| 2020 | 0.88 |
| 2019 | 0.88 |
| 2018 | 0.88 |
| 2017 | 0.88 |
| 2016 | 0.88 |
| 2015 | 0.88 |
| 2014 | 0.88 |
| 2013 | 0.88 |
| 2012 | 0.88 |
| 2011 | 0.82 |
| 2010 | 0.8 |
| 2009 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
