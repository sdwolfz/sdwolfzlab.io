---
title: " (DOC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>DOC</td></tr>
    <tr><td>Web</td><td><a href="https://www.docreit.com">www.docreit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.46 |
| 2020 | 0.92 |
| 2019 | 0.92 |
| 2018 | 0.92 |
| 2017 | 0.91 |
| 2016 | 0.9 |
| 2015 | 0.9 |
| 2014 | 0.9 |
| 2013 | 0.225 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
