---
title: "CF BANKSHARES INC (CFBK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CF BANKSHARES INC</td></tr>
    <tr><td>Symbol</td><td>CFBK</td></tr>
    <tr><td>Web</td><td><a href="https://www.cfbankonline.com">www.cfbankonline.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.06 |
| 2020 | 0.03 |
| 2008 | 0.25 |
| 2007 | 0.23 |
| 2006 | 0.36 |
| 2005 | 0.36 |
| 2004 | 0.45 |
| 2003 | 0.27 |
| 2002 | 0.45 |
| 2001 | 0.29 |
| 2000 | 6.18 |
| 1999 | 0.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
