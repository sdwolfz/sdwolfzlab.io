---
title: "SUNLINK HEALTH SYSTEMS INC (SSY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SUNLINK HEALTH SYSTEMS INC</td></tr>
    <tr><td>Symbol</td><td>SSY</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 1989 | 0.03 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
