---
title: "UNITIL CORP (UTL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>UNITIL CORP</td></tr>
    <tr><td>Symbol</td><td>UTL</td></tr>
    <tr><td>Web</td><td><a href="https://www.unitil.com">www.unitil.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.76 |
| 2020 | 1.5 |
| 2019 | 1.48 |
| 2018 | 1.46 |
| 2017 | 1.44 |
| 2016 | 1.42 |
| 2015 | 1.4 |
| 2014 | 1.38 |
| 2013 | 0.69 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
