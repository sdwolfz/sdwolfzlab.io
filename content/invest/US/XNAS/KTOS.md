---
title: "KRATOS DEFENSE & SECURITY SOLUTIONS (KTOS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>KRATOS DEFENSE & SECURITY SOLUTIONS</td></tr>
    <tr><td>Symbol</td><td>KTOS</td></tr>
    <tr><td>Web</td><td><a href="https://www.kratosdefense.com">www.kratosdefense.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
