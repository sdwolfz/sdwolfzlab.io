---
title: "BROOKLINE BANCORP INC (BRKL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BROOKLINE BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>BRKL</td></tr>
    <tr><td>Web</td><td><a href="https://www.brooklinebank.com">www.brooklinebank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.235 |
| 2020 | 0.46 |
| 2019 | 0.44 |
| 2018 | 0.395 |
| 2017 | 0.36 |
| 2016 | 0.36 |
| 2015 | 0.355 |
| 2014 | 0.34 |
| 2013 | 0.34 |
| 2012 | 0.34 |
| 2011 | 0.34 |
| 2010 | 0.34 |
| 2009 | 0.54 |
| 2008 | 0.74 |
| 2007 | 0.74 |
| 2006 | 0.74 |
| 2005 | 0.74 |
| 2004 | 0.74 |
| 2003 | 0.54 |
| 2002 | 0.49 |
| 2001 | 0.46 |
| 2000 | 0.24 |
| 1999 | 0.21 |
| 1998 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
