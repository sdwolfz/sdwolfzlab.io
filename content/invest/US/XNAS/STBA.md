---
title: "S & T BANCORP INC (STBA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>S & T BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>STBA</td></tr>
    <tr><td>Web</td><td><a href="https://www.stbancorp.com">www.stbancorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.56 |
| 2020 | 1.12 |
| 2019 | 1.09 |
| 2018 | 0.99 |
| 2017 | 0.82 |
| 2016 | 0.77 |
| 2015 | 0.73 |
| 2014 | 0.68 |
| 2013 | 0.61 |
| 2012 | 0.6 |
| 2011 | 0.6 |
| 2010 | 0.6 |
| 2009 | 0.61 |
| 2008 | 1.24 |
| 2007 | 1.21 |
| 2006 | 1.17 |
| 2005 | 1.13 |
| 2004 | 1.07 |
| 2003 | 1.02 |
| 2002 | 0.97 |
| 2001 | 0.92 |
| 2000 | 0.84 |
| 1999 | 0.76 |
| 1998 | 1.14 |
| 1997 | 1.11 |
| 1996 | 0.94 |
| 1995 | 0.74 |
| 1994 | 0.89 |
| 1993 | 0.99 |
| 1992 | 0.62 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
