---
title: "EBIX INC (EBIX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>EBIX INC</td></tr>
    <tr><td>Symbol</td><td>EBIX</td></tr>
    <tr><td>Web</td><td><a href="https://www.ebix.com">www.ebix.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.075 |
| 2020 | 0.3 |
| 2019 | 0.3 |
| 2018 | 0.3 |
| 2017 | 0.3 |
| 2016 | 0.3 |
| 2015 | 0.3 |
| 2014 | 0.3 |
| 2013 | 0.075 |
| 2012 | 0.19 |
| 2011 | 0.04 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
