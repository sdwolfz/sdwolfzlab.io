---
title: "ENPRO INDUSTRIES INC (NPO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ENPRO INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>NPO</td></tr>
    <tr><td>Web</td><td><a href="https://www.enproindustries.com">www.enproindustries.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.54 |
| 2020 | 1.04 |
| 2019 | 1.0 |
| 2018 | 0.96 |
| 2017 | 0.88 |
| 2016 | 0.84 |
| 2015 | 0.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
