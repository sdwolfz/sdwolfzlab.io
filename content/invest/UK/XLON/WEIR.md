---
title: "Weir Group (WEIR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Weir Group</td></tr>
    <tr><td>Symbol</td><td>WEIR</td></tr>
    <tr><td>Web</td><td><a href="https://www.global.weir">www.global.weir</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 46.95 |
| 2018 | 19.34 |
| 2017 | 19.55 |
| 2016 | 44.0 |
| 2016 | 44.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
