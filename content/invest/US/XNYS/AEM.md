---
title: "AGNICO EAGLE MINES LTD (AEM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AGNICO EAGLE MINES LTD</td></tr>
    <tr><td>Symbol</td><td>AEM</td></tr>
    <tr><td>Web</td><td><a href="https://www.agnicoeagle.com">www.agnicoeagle.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.95 |
| 2019 | 0.55 |
| 2018 | 0.44 |
| 2017 | 0.41 |
| 2016 | 0.32 |
| 2015 | 0.32 |
| 2014 | 0.32 |
| 2013 | 0.44 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
