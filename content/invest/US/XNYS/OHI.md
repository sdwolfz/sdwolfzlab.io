---
title: " (OHI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>OHI</td></tr>
    <tr><td>Web</td><td><a href="https://www.omegahealthcare.com">www.omegahealthcare.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.34 |
| 2020 | 2.68 |
| 2019 | 2.65 |
| 2018 | 2.64 |
| 2017 | 2.54 |
| 2016 | 2.36 |
| 2015 | 2.18 |
| 2014 | 2.02 |
| 2013 | 0.95 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
