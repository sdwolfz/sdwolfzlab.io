---
title: "COMREP SA Ploiesti (COTN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>COMREP SA Ploiesti</td></tr>
    <tr><td>Symbol</td><td>COTN</td></tr>
    <tr><td>Web</td><td><a href="https://www.comrepsa.ro">www.comrepsa.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.5 |
| 2016 | 0.5 |
| 2015 | 1.0 |
| 2014 | 1.0 |
| 2009 | 1.4243 |
| 2008 | 1.4243 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
