---
title: "CITIZENS FINANCIAL GROUP INC (CFG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CITIZENS FINANCIAL GROUP INC</td></tr>
    <tr><td>Symbol</td><td>CFG</td></tr>
    <tr><td>Web</td><td><a href="https://www.citizensbank.com">www.citizensbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.78 |
| 2020 | 1.56 |
| 2019 | 1.36 |
| 2018 | 0.98 |
| 2017 | 0.64 |
| 2016 | 0.46 |
| 2015 | 0.4 |
| 2014 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
