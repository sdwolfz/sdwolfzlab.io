---
title: " (CEPU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CEPU</td></tr>
    <tr><td>Web</td><td><a href="https://www.centralpuerto.com">www.centralpuerto.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.099 |
| 2018 | 0.281 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
