---
title: "GREIF INC (GEF.B)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GREIF INC</td></tr>
    <tr><td>Symbol</td><td>GEF.B</td></tr>
    <tr><td>Web</td><td><a href="https://www.greif.com">www.greif.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.66 |
| 2020 | 2.63 |
| 2019 | 2.63 |
| 2018 | 2.57 |
| 2017 | 2.51 |
| 2016 | 2.51 |
| 2015 | 2.51 |
| 2014 | 2.51 |
| 2013 | 1.88 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
