---
title: " (UBP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>UBP</td></tr>
    <tr><td>Web</td><td><a href="https://www.ubproperties.com">www.ubproperties.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 0.688 |
| 2019 | 0.98 |
| 2018 | 0.96 |
| 2017 | 1.175 |
| 2016 | 0.69 |
| 2015 | 0.905 |
| 2014 | 0.9 |
| 2013 | 0.675 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
