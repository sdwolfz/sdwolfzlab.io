---
title: "Photo-Me (PHTM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Photo-Me</td></tr>
    <tr><td>Symbol</td><td>PHTM</td></tr>
    <tr><td>Web</td><td><a href="https://www.photo-me.co.uk">www.photo-me.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 8.44 |
| 2018 | 8.44 |
| 2017 | 7.03 |
| 2016 | 8.68 |
| 2015 | 4.88 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
