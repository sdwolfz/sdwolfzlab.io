---
title: "TIFFANY & CO (TIF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TIFFANY & CO</td></tr>
    <tr><td>Symbol</td><td>TIF</td></tr>
    <tr><td>Web</td><td><a href="http://www.tiffany.com">www.tiffany.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.32 |
| 2019 | 2.29 |
| 2018 | 2.15 |
| 2017 | 1.95 |
| 2016 | 1.75 |
| 2015 | 1.58 |
| 2014 | 1.48 |
| 2013 | 0.68 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
