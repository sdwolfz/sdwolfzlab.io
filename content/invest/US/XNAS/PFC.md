---
title: "PREMIER FINANCIAL CORP (PFC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PREMIER FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>PFC</td></tr>
    <tr><td>Web</td><td><a href="https://www.premierfincorp.com">www.premierfincorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.5 |
| 2020 | 0.88 |
| 2019 | 0.79 |
| 2018 | 0.94 |
| 2017 | 1.0 |
| 2016 | 0.88 |
| 2015 | 0.775 |
| 2014 | 0.625 |
| 2013 | 0.4 |
| 2012 | 0.2 |
| 2011 | 0.05 |
| 2009 | 0.465 |
| 2008 | 1.04 |
| 2007 | 1.0 |
| 2006 | 0.96 |
| 2005 | 0.88 |
| 2004 | 0.8 |
| 2003 | 0.6 |
| 2002 | 0.52 |
| 2001 | 0.48 |
| 2000 | 0.44 |
| 1999 | 0.4 |
| 1998 | 0.36 |
| 1997 | 0.32 |
| 1996 | 0.28 |
| 1995 | 0.6 |
| 1994 | 0.6 |
| 1993 | 0.125 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
