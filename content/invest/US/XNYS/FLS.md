---
title: "FLOWSERVE CORP (FLS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FLOWSERVE CORP</td></tr>
    <tr><td>Symbol</td><td>FLS</td></tr>
    <tr><td>Web</td><td><a href="https://www.flowserve.com">www.flowserve.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.8 |
| 2019 | 0.95 |
| 2018 | 0.76 |
| 2017 | 0.57 |
| 2016 | 0.95 |
| 2015 | 0.72 |
| 2014 | 0.64 |
| 2013 | 0.42 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
