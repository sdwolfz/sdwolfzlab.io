---
title: "AUBURN NATL BANCORP (AUBN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AUBURN NATL BANCORP</td></tr>
    <tr><td>Symbol</td><td>AUBN</td></tr>
    <tr><td>Web</td><td><a href="https://www.auburnbank.com">www.auburnbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.26 |
| 2020 | 1.02 |
| 2019 | 1.0 |
| 2018 | 0.96 |
| 2017 | 0.92 |
| 2016 | 0.9 |
| 2015 | 0.88 |
| 2014 | 0.86 |
| 2013 | 0.84 |
| 2012 | 0.82 |
| 2011 | 0.8 |
| 2010 | 0.78 |
| 2009 | 0.76 |
| 2008 | 0.74 |
| 2007 | 0.7 |
| 2006 | 0.64 |
| 2005 | 0.58 |
| 2004 | 0.5 |
| 2003 | 0.48 |
| 2002 | 0.44 |
| 2001 | 0.4 |
| 2000 | 0.4 |
| 1999 | 0.32 |
| 1998 | 0.38 |
| 1997 | 0.48 |
| 1996 | 0.32 |
| 1995 | 0.09 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
