---
title: "REMARUL 16 FEBRUARIE SA CLUJ NAPOCA (REFE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>REMARUL 16 FEBRUARIE SA CLUJ NAPOCA</td></tr>
    <tr><td>Symbol</td><td>REFE</td></tr>
    <tr><td>Web</td><td><a href="https://www.remarul.eu">www.remarul.eu</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 2.16 |
| 2009 | 0.4889 |
| 2003 | 0.7178 |
| 2002 | 0.2508 |
| 2001 | 0.9215 |
| 2000 | 1.275 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
