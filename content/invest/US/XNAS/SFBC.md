---
title: "SOUND FINL BANCORP INC (SFBC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SOUND FINL BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>SFBC</td></tr>
    <tr><td>Web</td><td><a href="https://www.soundcb.com">www.soundcb.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.44 |
| 2020 | 0.8 |
| 2019 | 0.56 |
| 2018 | 0.54 |
| 2017 | 0.6 |
| 2016 | 0.3 |
| 2015 | 0.23 |
| 2014 | 0.2 |
| 2013 | 0.15 |
| 2010 | 0.02 |
| 2009 | 0.12 |
| 2008 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
