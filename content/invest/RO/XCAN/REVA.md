---
title: "REVA SA SIMERIA (REVA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>REVA SA SIMERIA</td></tr>
    <tr><td>Symbol</td><td>REVA</td></tr>
    <tr><td>Web</td><td><a href="https://www.revasimeria.ro">www.revasimeria.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 2.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
