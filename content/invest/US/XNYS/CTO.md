---
title: "CTO RTLY GROWTH INC NEW (CTO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CTO RTLY GROWTH INC NEW</td></tr>
    <tr><td>Symbol</td><td>CTO</td></tr>
    <tr><td>Web</td><td><a href="https://www.ctlc.com">www.ctlc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.0 |
| 2020 | 12.73 |
| 2019 | 0.44 |
| 2018 | 0.27 |
| 2017 | 0.18 |
| 2016 | 0.16 |
| 2015 | 0.08 |
| 2014 | 0.07 |
| 2013 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
