---
title: "NAM TAI PPTY INC (NTP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NAM TAI PPTY INC</td></tr>
    <tr><td>Symbol</td><td>NTP</td></tr>
    <tr><td>Web</td><td><a href="https://www.namtai.com">www.namtai.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.21 |
| 2017 | 0.28 |
| 2016 | 0.13 |
| 2015 | 0.08 |
| 2014 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
