---
title: " (TRTX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>TRTX</td></tr>
    <tr><td>Web</td><td><a href="https://www.tpgrefinance.com">www.tpgrefinance.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 1.21 |
| 2019 | 1.72 |
| 2018 | 1.71 |
| 2017 | 0.71 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
