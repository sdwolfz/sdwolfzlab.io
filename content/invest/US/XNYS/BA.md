---
title: "BOEING CO (BA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BOEING CO</td></tr>
    <tr><td>Symbol</td><td>BA</td></tr>
    <tr><td>Web</td><td><a href="https://www.boeing.com">www.boeing.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.055 |
| 2019 | 8.22 |
| 2018 | 6.84 |
| 2017 | 5.68 |
| 2016 | 4.36 |
| 2015 | 3.64 |
| 2014 | 2.92 |
| 2013 | 0.97 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
