---
title: " (PSO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>PSO</td></tr>
    <tr><td>Web</td><td><a href="https://plc.pearson.com">plc.pearson.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.189 |
| 2020 | 0.243 |
| 2019 | 0.243 |
| 2018 | 0.239 |
| 2017 | 0.505 |
| 2016 | 0.727 |
| 2015 | 0.794 |
| 2014 | 0.815 |
| 2013 | 0.253 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
