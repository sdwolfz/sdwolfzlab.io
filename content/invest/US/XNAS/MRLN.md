---
title: "MARLIN BUSINESS SERVICES CORP (MRLN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MARLIN BUSINESS SERVICES CORP</td></tr>
    <tr><td>Symbol</td><td>MRLN</td></tr>
    <tr><td>Web</td><td><a href="https://www.marlincapitalsolutions.com">www.marlincapitalsolutions.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 0.56 |
| 2019 | 0.56 |
| 2018 | 0.56 |
| 2017 | 0.56 |
| 2016 | 0.56 |
| 2015 | 2.53 |
| 2014 | 0.47 |
| 2013 | 2.42 |
| 2012 | 0.28 |
| 2011 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
