---
title: " (BHFAO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BHFAO</td></tr>
    <tr><td>Web</td><td><a href="https://www.brighthousefinancial.com">www.brighthousefinancial.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.422 |
| 2020 | 1.017 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
