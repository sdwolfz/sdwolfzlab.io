---
title: "POWELL INDUSTRIES INC (POWL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>POWELL INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>POWL</td></tr>
    <tr><td>Web</td><td><a href="https://www.powellind.com">www.powellind.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.52 |
| 2020 | 1.04 |
| 2019 | 1.04 |
| 2018 | 1.04 |
| 2017 | 1.04 |
| 2016 | 1.04 |
| 2015 | 1.04 |
| 2014 | 1.01 |
| 2013 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
