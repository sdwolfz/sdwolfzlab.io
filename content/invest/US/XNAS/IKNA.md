---
title: "IKENA ONCOLOGY INC (IKNA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>IKENA ONCOLOGY INC</td></tr>
    <tr><td>Symbol</td><td>IKNA</td></tr>
    <tr><td>Web</td><td><a href="https://www.ikenaoncology.com">www.ikenaoncology.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
