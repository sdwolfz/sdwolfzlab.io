---
title: "DATTO HLDG CORP (MSP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DATTO HLDG CORP</td></tr>
    <tr><td>Symbol</td><td>MSP</td></tr>
    <tr><td>Web</td><td><a href="https://www.datto.com">www.datto.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.78 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
