---
title: "LONDON STOCK EXCHANGE GROUP PLC ORD SHS 6 79/86P (LSE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>LONDON STOCK EXCHANGE GROUP PLC ORD SHS 6 79/86P</td></tr>
    <tr><td>Symbol</td><td>LSE</td></tr>
    <tr><td>Web</td><td><a href="http://www.lseg.com">www.lseg.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 70.0 |
| 2018 | 60.4 |
| 2017 | 51.6 |
| 2016 | 43.2 |
| 2015 | 36.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
