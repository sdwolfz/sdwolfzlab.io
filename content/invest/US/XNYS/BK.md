---
title: "BANK OF NEW YORK MELLON CORP (BK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BANK OF NEW YORK MELLON CORP</td></tr>
    <tr><td>Symbol</td><td>BK</td></tr>
    <tr><td>Web</td><td><a href="https://www.bnymellon.com">www.bnymellon.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.62 |
| 2020 | 1.24 |
| 2019 | 1.18 |
| 2018 | 1.04 |
| 2017 | 0.86 |
| 2016 | 0.72 |
| 2015 | 0.68 |
| 2014 | 0.66 |
| 2013 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
