---
title: "Signature Aviat (SIG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Signature Aviat</td></tr>
    <tr><td>Symbol</td><td>SIG</td></tr>
    <tr><td>Web</td><td><a href="https://www.signatureaviation.com">www.signatureaviation.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 95.48 |
| 2018 | 17.59 |
| 2017 | 16.75 |
| 2016 | 34.39 |
| 2015 | 16.91 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
