---
title: "ARCELORMITTAL (MT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ARCELORMITTAL</td></tr>
    <tr><td>Symbol</td><td>MT</td></tr>
    <tr><td>Web</td><td><a href="https://www.arcelormittal.com">www.arcelormittal.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.17 |
| 2018 | 0.085 |
| 2015 | 0.17 |
| 2014 | 0.17 |
| 2013 | 0.17 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
