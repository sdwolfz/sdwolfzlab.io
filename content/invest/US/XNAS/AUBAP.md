---
title: " (AUBAP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>AUBAP</td></tr>
    <tr><td>Web</td><td><a href="https://www.atlanticunionbank.com">www.atlanticunionbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.86 |
| 2020 | 0.82 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
