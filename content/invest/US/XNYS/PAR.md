---
title: "PAR TECHNOLOGY CORP (PAR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PAR TECHNOLOGY CORP</td></tr>
    <tr><td>Symbol</td><td>PAR</td></tr>
    <tr><td>Web</td><td><a href="https://www.partech.com">www.partech.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.31 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
