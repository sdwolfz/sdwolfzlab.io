---
title: "SERVISFIRST BANCSHARES INC (SFBS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SERVISFIRST BANCSHARES INC</td></tr>
    <tr><td>Symbol</td><td>SFBS</td></tr>
    <tr><td>Web</td><td><a href="https://www.servisfirstbank.com">www.servisfirstbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.725 |
| 2019 | 0.625 |
| 2018 | 0.48 |
| 2017 | 0.2 |
| 2016 | 0.34 |
| 2015 | 0.23 |
| 2014 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
