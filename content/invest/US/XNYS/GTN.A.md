---
title: "GRAY TELEVISION INC (GTN.A)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GRAY TELEVISION INC</td></tr>
    <tr><td>Symbol</td><td>GTN.A</td></tr>
    <tr><td>Web</td><td><a href="https://www.gray.tv">www.gray.tv</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
