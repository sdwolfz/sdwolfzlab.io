---
title: " (RELX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>RELX</td></tr>
    <tr><td>Web</td><td><a href="https://www.relx.com">www.relx.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.469 |
| 2020 | 0.576 |
| 2019 | 0.54 |
| 2018 | 0.531 |
| 2017 | 0.485 |
| 2016 | 0.458 |
| 2015 | 0.408 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
