---
title: "MORNINGSTAR INC (MORN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MORNINGSTAR INC</td></tr>
    <tr><td>Symbol</td><td>MORN</td></tr>
    <tr><td>Web</td><td><a href="https://www.morningstar.com">www.morningstar.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.315 |
| 2020 | 1.515 |
| 2019 | 1.12 |
| 2018 | 1.0 |
| 2017 | 0.92 |
| 2016 | 0.88 |
| 2015 | 0.76 |
| 2014 | 0.68 |
| 2013 | 0.375 |
| 2012 | 0.525 |
| 2011 | 0.15 |
| 2010 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
