---
title: " (CANG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CANG</td></tr>
    <tr><td>Web</td><td><a href="https://www.cangoonline.com">www.cangoonline.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.98 |
| 2020 | 0.23 |
| 2019 | 0.23 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
