---
title: "Schroder Orient (SOI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Schroder Orient</td></tr>
    <tr><td>Symbol</td><td>SOI</td></tr>
    <tr><td>Web</td><td><a href="https://www.schroders.co.uk">www.schroders.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 10.3 |
| 2019 | 10.1 |
| 2018 | 9.7 |
| 2017 | 9.2 |
| 2016 | 8.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
