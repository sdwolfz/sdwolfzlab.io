---
title: " (LXP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>LXP</td></tr>
    <tr><td>Web</td><td><a href="https://www.lxp.com">www.lxp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.108 |
| 2020 | 0.423 |
| 2019 | 0.411 |
| 2018 | 0.708 |
| 2017 | 0.702 |
| 2016 | 0.69 |
| 2015 | 0.68 |
| 2014 | 0.675 |
| 2013 | 0.465 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
