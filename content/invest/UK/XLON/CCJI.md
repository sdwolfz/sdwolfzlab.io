---
title: "Cc Japan (CCJI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Cc Japan</td></tr>
    <tr><td>Symbol</td><td>CCJI</td></tr>
    <tr><td>Web</td><td><a href="https://www.ccjapanincomeandgrowthtrust.com">www.ccjapanincomeandgrowthtrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 4.6 |
| 2019 | 4.5 |
| 2018 | 3.75 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
