---
title: " (JMP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>JMP</td></tr>
    <tr><td>Web</td><td><a href="https://www.jmpg.com">www.jmpg.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.17 |
| 2018 | 0.36 |
| 2017 | 0.36 |
| 2016 | 1.35 |
| 2015 | 0.456 |
| 2014 | 0.225 |
| 2013 | 0.075 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
