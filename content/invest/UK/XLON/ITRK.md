---
title: "Intertek Group (ITRK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Intertek Group</td></tr>
    <tr><td>Symbol</td><td>ITRK</td></tr>
    <tr><td>Web</td><td><a href="https://www.intertek.com">www.intertek.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 105.8 |
| 2018 | 99.1 |
| 2017 | 71.3 |
| 2016 | 62.4 |
| 2015 | 52.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
