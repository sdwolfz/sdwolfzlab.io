---
title: "Local packages in Spacemacs"
date: 2022-05-02T08:53:58Z
type: posts
slug: local-packages-in-spacemacs
tags: ["spacemacs", "elisp", "emacs"]
---

Are you a hardcore `elisp` programmer? Add this to your `~/.spacemacs`:

```elisp
dotspacemacs-additional-packages '(
 (treemacs :location
             (recipe :fetcher file
                     :path "~/path/to/treemacs/src/elisp")
           :update nil))
```

Now you can hack on any `emacs` package in your live editor!

## Notes

* You will need to manually delete the package from `~/.emacs.d/elpa/29.0/develop`.
* The path is supposed to point to the location of `*.el` files.
* Heavy lifting is done by `quelpa`, read their docs!

## Reference

* https://github.com/quelpa/quelpa
