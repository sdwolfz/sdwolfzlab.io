---
title: "SSE (SSE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>SSE</td></tr>
    <tr><td>Symbol</td><td>SSE</td></tr>
    <tr><td>Web</td><td><a href="https://www.scottish-southern.co.uk">www.scottish-southern.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 80.0 |
| 2019 | 97.5 |
| 2018 | 94.7 |
| 2017 | 86.28 |
| 2016 | 86.45 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
