---
title: "Beazley (BEZ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Beazley</td></tr>
    <tr><td>Symbol</td><td>BEZ</td></tr>
    <tr><td>Web</td><td><a href="https://www.beazley.com">www.beazley.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 12.3 |
| 2018 | 11.7 |
| 2017 | 110.0 |
| 2016 | 50.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
