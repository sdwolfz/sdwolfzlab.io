---
title: "CONTANGO OIL & GAS CO (MCF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CONTANGO OIL & GAS CO</td></tr>
    <tr><td>Symbol</td><td>MCF</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2012 | 2.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
