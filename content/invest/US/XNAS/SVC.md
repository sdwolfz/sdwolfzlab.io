---
title: "SERVICE PROPERTIES TRUST (SVC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SERVICE PROPERTIES TRUST</td></tr>
    <tr><td>Symbol</td><td>SVC</td></tr>
    <tr><td>Web</td><td><a href="http://www.svcreit.com">www.svcreit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.57 |
| 2019 | 2.15 |
| 2018 | 2.11 |
| 2017 | 2.07 |
| 2016 | 2.03 |
| 2015 | 1.99 |
| 2014 | 1.95 |
| 2013 | 1.89 |
| 2012 | 1.82 |
| 2011 | 1.8 |
| 2010 | 1.8 |
| 2009 | 0.77 |
| 2008 | 3.08 |
| 2007 | 3.03 |
| 2006 | 2.94 |
| 2005 | 2.17 |
| 2004 | 2.948 |
| 2003 | 3.6 |
| 2002 | 2.86 |
| 2001 | 2.82 |
| 2000 | 0.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
