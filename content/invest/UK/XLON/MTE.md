---
title: "Montanaro (MTE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Montanaro</td></tr>
    <tr><td>Symbol</td><td>MTE</td></tr>
    <tr><td>Web</td><td><a href="https://montanaro.co.uk">montanaro.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 9.25 |
| 2019 | 9.0 |
| 2018 | 8.5 |
| 2017 | 8.25 |
| 2016 | 7.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
