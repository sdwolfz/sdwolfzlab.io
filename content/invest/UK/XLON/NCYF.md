---
title: "Cqs New C.h.y.f (NCYF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Cqs New C.h.y.f</td></tr>
    <tr><td>Symbol</td><td>NCYF</td></tr>
    <tr><td>Web</td><td><a href="https://www.ncim.co.uk">www.ncim.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 4.46 |
| 2019 | 4.45 |
| 2018 | 4.42 |
| 2017 | 4.39 |
| 2016 | 4.36 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
