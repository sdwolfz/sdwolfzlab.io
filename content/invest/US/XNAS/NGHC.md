---
title: "NATIONAL GENERAL HLDGS CO (NGHC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NATIONAL GENERAL HLDGS CO</td></tr>
    <tr><td>Symbol</td><td>NGHC</td></tr>
    <tr><td>Web</td><td><a href="http://www.nationalgeneral.com">www.nationalgeneral.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.2 |
| 2019 | 0.18 |
| 2018 | 0.16 |
| 2017 | 0.16 |
| 2016 | 0.14 |
| 2015 | 0.09 |
| 2014 | 0.06 |
| 2013 | 0.01 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
