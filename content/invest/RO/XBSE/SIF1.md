---
title: "SIF BANAT CRISANA S.A. (SIF1)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>SIF BANAT CRISANA S.A.</td></tr>
    <tr><td>Symbol</td><td>SIF1</td></tr>
    <tr><td>Web</td><td><a href="https://www.sif1.ro">www.sif1.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 0.1 |
| 2011 | 0.1 |
| 2010 | 0.103 |
| 2009 | 0.05 |
| 2008 | 0.03 |
| 2007 | 0.07 |
| 2006 | 0.06 |
| 2005 | 0.05 |
| 2004 | 0.05 |
| 2003 | 0.048 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
