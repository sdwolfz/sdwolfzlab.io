---
title: "BANK OZK (OZK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BANK OZK</td></tr>
    <tr><td>Symbol</td><td>OZK</td></tr>
    <tr><td>Web</td><td><a href="https://www.ozk.com">www.ozk.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.558 |
| 2020 | 1.078 |
| 2019 | 0.94 |
| 2018 | 0.795 |
| 2017 | 0.71 |
| 2016 | 0.63 |
| 2015 | 0.55 |
| 2014 | 0.695 |
| 2013 | 0.72 |
| 2012 | 0.5 |
| 2011 | 0.64 |
| 2010 | 0.6 |
| 2009 | 0.52 |
| 2008 | 0.5 |
| 2007 | 0.43 |
| 2006 | 0.4 |
| 2005 | 0.37 |
| 2004 | 0.3 |
| 2003 | 0.46 |
| 2002 | 0.44 |
| 2001 | 0.46 |
| 2000 | 0.42 |
| 1999 | 0.4 |
| 1998 | 0.23 |
| 1997 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
