---
title: " (AVGOP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>AVGOP</td></tr>
    <tr><td>Web</td><td><a href="https://www.broadcom.com">www.broadcom.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2022 | 60.0 |
| 2021 | 80.0 |
| 2020 | 80.0 |
| 2019 | 20.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
