---
title: "NETWORK-1 TECHNOLOGIES INC (NTIP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NETWORK-1 TECHNOLOGIES INC</td></tr>
    <tr><td>Symbol</td><td>NTIP</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.05 |
| 2020 | 0.1 |
| 2019 | 0.1 |
| 2018 | 0.1 |
| 2017 | 0.1 |
| 2010 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
