---
title: "ARGAN INC (AGX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ARGAN INC</td></tr>
    <tr><td>Symbol</td><td>AGX</td></tr>
    <tr><td>Web</td><td><a href="https://www.arganinc.com">www.arganinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 3.25 |
| 2019 | 1.0 |
| 2018 | 0.75 |
| 2017 | 1.0 |
| 2016 | 1.0 |
| 2015 | 0.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
