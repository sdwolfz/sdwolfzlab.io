---
title: " (KRG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>KRG</td></tr>
    <tr><td>Web</td><td><a href="https://www.kiterealty.com">www.kiterealty.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.32 |
| 2020 | 0.449 |
| 2019 | 1.585 |
| 2018 | 1.268 |
| 2017 | 1.212 |
| 2016 | 1.134 |
| 2015 | 1.079 |
| 2014 | 0.645 |
| 2013 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
