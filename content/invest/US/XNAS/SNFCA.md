---
title: "SECURITY NATIONAL FINANCIAL CORP (SNFCA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SECURITY NATIONAL FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>SNFCA</td></tr>
    <tr><td>Web</td><td><a href="https://www.securitynational.com">www.securitynational.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 1992 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
