---
title: " (STAR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>STAR</td></tr>
    <tr><td>Web</td><td><a href="https://www.istar.com">www.istar.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.11 |
| 2020 | 0.43 |
| 2019 | 0.39 |
| 2018 | 0.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
