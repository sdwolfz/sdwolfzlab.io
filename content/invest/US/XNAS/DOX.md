---
title: "AMDOCS LTD (DOX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AMDOCS LTD</td></tr>
    <tr><td>Symbol</td><td>DOX</td></tr>
    <tr><td>Web</td><td><a href="https://www.amdocs.com">www.amdocs.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.36 |
| 2020 | 1.31 |
| 2019 | 1.14 |
| 2018 | 1.0 |
| 2017 | 0.88 |
| 2016 | 0.78 |
| 2015 | 0.68 |
| 2014 | 0.62 |
| 2013 | 0.52 |
| 2012 | 0.26 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
