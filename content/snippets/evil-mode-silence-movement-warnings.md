---
title: "Evil Mode: silence movement warnings"
date: 2023-01-28T13:21:29Z
type: posts
slug: evil-mode-silence-movement-warnings
tags: ["emacs", "spacemacs", "evil"]
---

If you're tired of seeing these:

```
evil-line-move: Beginning of buffer [81 times]
evil-line-move: End of buffer [32 times]
evil-forward-char: End of line [66 times]
evil-backward-char: Beginning of line [66 times]
```

Add these lines to your `dotspacemacs/user-config`:

```elisp
(with-eval-after-load 'evil
  (advice-add 'evil-line-move
    :filter-args
    (lambda (args) (append args '(t))))

  (advice-add 'evil-forward-char
    :filter-args
    (lambda (args) (append (butlast args) '(t))))

  (advice-add 'evil-backward-char
    :filter-args
    (lambda (args) (append (butlast args) '(t)))))
```
