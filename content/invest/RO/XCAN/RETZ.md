---
title: "RETEZAT SA SIBIU (RETZ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>RETEZAT SA SIBIU</td></tr>
    <tr><td>Symbol</td><td>RETZ</td></tr>
    <tr><td>Web</td><td><a href="https://www.retezatsibiu.ro">www.retezatsibiu.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.1047 |
| 2019 | 2.2094 |
| 2018 | 2.2094 |
| 2017 | 6.87 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
