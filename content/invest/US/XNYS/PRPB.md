---
title: "CC NEUBERGER PRINCIPAL HOLDINGS II (PRPB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CC NEUBERGER PRINCIPAL HOLDINGS II</td></tr>
    <tr><td>Symbol</td><td>PRPB</td></tr>
    <tr><td>Web</td><td><a href="https://www.ccnbprincipal.com">www.ccnbprincipal.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
