---
title: "BRUKER CORPORATION (BRKR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BRUKER CORPORATION</td></tr>
    <tr><td>Symbol</td><td>BRKR</td></tr>
    <tr><td>Web</td><td><a href="https://www.bruker.com">www.bruker.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.04 |
| 2020 | 0.16 |
| 2019 | 0.16 |
| 2018 | 0.16 |
| 2017 | 0.16 |
| 2016 | 0.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
