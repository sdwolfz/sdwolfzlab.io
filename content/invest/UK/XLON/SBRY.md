---
title: "Sainsbury (J) (SBRY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Sainsbury (J)</td></tr>
    <tr><td>Symbol</td><td>SBRY</td></tr>
    <tr><td>Web</td><td><a href="https://www.j-sainsbury.co.uk">www.j-sainsbury.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 3.3 |
| 2019 | 11.0 |
| 2018 | 10.2 |
| 2017 | 10.2 |
| 2016 | 12.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
