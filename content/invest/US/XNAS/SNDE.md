---
title: "SUNDANCE ENERGY INC (SNDE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SUNDANCE ENERGY INC</td></tr>
    <tr><td>Symbol</td><td>SNDE</td></tr>
    <tr><td>Web</td><td><a href="http://www.sundanceenergy.net">www.sundanceenergy.net</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
