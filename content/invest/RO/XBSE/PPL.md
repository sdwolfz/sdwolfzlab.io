---
title: "PROMATERIS S.A. (PPL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>PROMATERIS S.A.</td></tr>
    <tr><td>Symbol</td><td>PPL</td></tr>
    <tr><td>Web</td><td><a href="https://www.prodplast.ro">www.prodplast.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2006 | 0.012 |
| 2005 | 0.035 |
| 2004 | 0.5 |
| 2003 | 0.5 |
| 2002 | 0.5 |
| 2001 | 0.305 |
| 2000 | 0.45 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
