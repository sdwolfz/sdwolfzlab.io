---
title: "ITV (ITV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>ITV</td></tr>
    <tr><td>Symbol</td><td>ITV</td></tr>
    <tr><td>Web</td><td><a href="https://www.itvplc.com">www.itvplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 8.0 |
| 2018 | 8.0 |
| 2017 | 7.8 |
| 2016 | 12.2 |
| 2015 | 16.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
