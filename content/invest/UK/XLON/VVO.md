---
title: "Vivo Energy (VVO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Vivo Energy</td></tr>
    <tr><td>Symbol</td><td>VVO</td></tr>
    <tr><td>Web</td><td><a href="https://www.vivoenergy.com">www.vivoenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.5 |
| 2019 | 3.8 |
| 2018 | 2.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
