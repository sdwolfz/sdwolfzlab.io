---
title: "GOLDEN OCEAN GROUP LTD (GOGL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GOLDEN OCEAN GROUP LTD</td></tr>
    <tr><td>Symbol</td><td>GOGL</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.05 |
| 2019 | 0.325 |
| 2018 | 0.45 |
| 2014 | 0.625 |
| 2013 | 0.7 |
| 2012 | 1.2 |
| 2011 | 2.0 |
| 2010 | 1.7 |
| 2009 | 0.25 |
| 2008 | 2.75 |
| 2007 | 2.5 |
| 2006 | 3.6 |
| 2005 | 4.55 |
| 2004 | 4.55 |
| 2003 | 2.74 |
| 2002 | 1.36 |
| 2001 | 4.24 |
| 2000 | 2.66 |
| 1999 | 1.8 |
| 1998 | 2.36 |
| 1997 | 1.26 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
