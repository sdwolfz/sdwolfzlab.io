---
title: "MANNING & NAPIER INC (MN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MANNING & NAPIER INC</td></tr>
    <tr><td>Symbol</td><td>MN</td></tr>
    <tr><td>Web</td><td><a href="https://www.manning-napier.com">www.manning-napier.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.04 |
| 2019 | 0.08 |
| 2018 | 0.32 |
| 2017 | 0.4 |
| 2016 | 0.64 |
| 2015 | 0.64 |
| 2014 | 0.64 |
| 2013 | 0.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
