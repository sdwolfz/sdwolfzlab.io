---
title: " (BBAR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BBAR</td></tr>
    <tr><td>Web</td><td><a href="https://www.bbva.com.ar">www.bbva.com.ar</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.261 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
