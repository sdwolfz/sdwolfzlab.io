---
title: "AMPCO-PITTSBURGH CORP (AP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AMPCO-PITTSBURGH CORP</td></tr>
    <tr><td>Symbol</td><td>AP</td></tr>
    <tr><td>Web</td><td><a href="https://www.ampcopgh.com">www.ampcopgh.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 0.18 |
| 2016 | 0.45 |
| 2015 | 0.72 |
| 2014 | 0.72 |
| 2013 | 0.36 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
