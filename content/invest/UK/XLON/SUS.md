---
title: "S & U (SUS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>S & U</td></tr>
    <tr><td>Symbol</td><td>SUS</td></tr>
    <tr><td>Web</td><td><a href="https://www.suplc.co.uk">www.suplc.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 120.0 |
| 2019 | 118.0 |
| 2018 | 105.0 |
| 2017 | 91.0 |
| 2016 | 76.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
