---
title: "SB FINANCIAL GROUP INC (SBFG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SB FINANCIAL GROUP INC</td></tr>
    <tr><td>Symbol</td><td>SBFG</td></tr>
    <tr><td>Web</td><td><a href="https://www.yoursbfinancial.com">www.yoursbfinancial.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.215 |
| 2020 | 0.4 |
| 2019 | 0.36 |
| 2018 | 0.32 |
| 2017 | 0.28 |
| 2016 | 0.24 |
| 2015 | 0.2 |
| 2014 | 0.16 |
| 2013 | 0.12 |
| 2009 | 0.36 |
| 2008 | 0.34 |
| 2007 | 0.26 |
| 2006 | 0.21 |
| 2005 | 0.2 |
| 2002 | 0.26 |
| 2001 | 0.37 |
| 2000 | 0.34 |
| 1999 | 0.41 |
| 1998 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
