---
title: " (ENBL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ENBL</td></tr>
    <tr><td>Web</td><td><a href="https://www.enablemidstream.com">www.enablemidstream.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.33 |
| 2020 | 0.826 |
| 2019 | 1.298 |
| 2018 | 1.272 |
| 2017 | 1.272 |
| 2016 | 1.272 |
| 2015 | 1.256 |
| 2014 | 0.549 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
