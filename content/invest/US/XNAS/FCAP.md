---
title: "1ST CAPITAL INC (FCAP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>1ST CAPITAL INC</td></tr>
    <tr><td>Symbol</td><td>FCAP</td></tr>
    <tr><td>Web</td><td><a href="https://www.firstharrison.com">www.firstharrison.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.26 |
| 2020 | 0.96 |
| 2019 | 0.95 |
| 2018 | 0.92 |
| 2017 | 0.86 |
| 2016 | 0.84 |
| 2015 | 0.84 |
| 2014 | 0.84 |
| 2013 | 0.8 |
| 2012 | 0.76 |
| 2011 | 0.76 |
| 2010 | 0.74 |
| 2009 | 0.72 |
| 2008 | 0.71 |
| 2007 | 0.68 |
| 2006 | 0.68 |
| 2005 | 0.62 |
| 2004 | 0.6 |
| 2003 | 0.56 |
| 2002 | 0.52 |
| 2001 | 0.48 |
| 2000 | 0.41 |
| 1999 | 0.35 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
