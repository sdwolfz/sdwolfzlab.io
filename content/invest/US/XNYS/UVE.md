---
title: "UNIVERSAL INSURANCE INC (UVE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>UNIVERSAL INSURANCE INC</td></tr>
    <tr><td>Symbol</td><td>UVE</td></tr>
    <tr><td>Web</td><td><a href="https://universalinsuranceholdings.com">universalinsuranceholdings.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.32 |
| 2020 | 0.77 |
| 2019 | 0.77 |
| 2018 | 0.73 |
| 2017 | 0.69 |
| 2016 | 0.69 |
| 2015 | 0.63 |
| 2014 | 0.4 |
| 2013 | 0.26 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
