---
title: "Watches Switz (WOSG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Watches Switz</td></tr>
    <tr><td>Symbol</td><td>WOSG</td></tr>
    <tr><td>Web</td><td><a href="https://www.thewosgroupplc.com">www.thewosgroupplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
