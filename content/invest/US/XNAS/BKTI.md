---
title: "BK TECHNOLOGIES CORPORATION (BKTI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BK TECHNOLOGIES CORPORATION</td></tr>
    <tr><td>Symbol</td><td>BKTI</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.02 |
| 2020 | 0.1 |
| 2019 | 0.06 |
| 2018 | 0.08 |
| 2017 | 0.15 |
| 2016 | 0.27 |
| 2007 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
