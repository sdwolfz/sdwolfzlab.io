---
title: "BLACKROCK CAPITAL INVESTMENT CORP (BKCC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BLACKROCK CAPITAL INVESTMENT CORP</td></tr>
    <tr><td>Symbol</td><td>BKCC</td></tr>
    <tr><td>Web</td><td><a href="https://www.blackrockbkcc.com">www.blackrockbkcc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.44 |
| 2019 | 0.64 |
| 2018 | 0.72 |
| 2017 | 0.72 |
| 2016 | 0.84 |
| 2015 | 0.84 |
| 2014 | 0.89 |
| 2013 | 1.04 |
| 2012 | 1.04 |
| 2011 | 1.1 |
| 2010 | 1.28 |
| 2009 | 0.8 |
| 2008 | 1.72 |
| 2007 | 0.85 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
