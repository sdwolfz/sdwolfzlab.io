---
title: "Petrofac (PFC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Petrofac</td></tr>
    <tr><td>Symbol</td><td>PFC</td></tr>
    <tr><td>Web</td><td><a href="https://www.petrofac.com">www.petrofac.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 38.0 |
| 2018 | 38.0 |
| 2017 | 38.0 |
| 2016 | 65.8 |
| 2015 | 65.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
