---
title: "TRANE TECHNOLOGIES PLC (TT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TRANE TECHNOLOGIES PLC</td></tr>
    <tr><td>Symbol</td><td>TT</td></tr>
    <tr><td>Web</td><td><a href="https://www.tranetechnologies.com">www.tranetechnologies.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.18 |
| 2020 | 2.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
