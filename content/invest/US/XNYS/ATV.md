---
title: "ACORN INTERNATIONAL INC SPON ADR EA REP 20 ORD SHS (ATV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ACORN INTERNATIONAL INC SPON ADR EA REP 20 ORD SHS</td></tr>
    <tr><td>Symbol</td><td>ATV</td></tr>
    <tr><td>Web</td><td><a href="http://www.acorninternationalgroup.com">www.acorninternationalgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 1.69 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
