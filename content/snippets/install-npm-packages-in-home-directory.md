---
title: "Install NPM packages in home directory"
date: 2020-04-04T21:41:19Z
type: posts
slug: install-npm-packages-in-home-directory
tags: ["npm", "node", "javascript", "bash", "linux", "productivity"]
---

Create the `~/.npmrc` file with the following content:

```
prefix = "~/.local"
```

Add the following line to your `.bash_profile`:

```bash
export PATH=~/.local/bin:$PATH
```

Now you can install a npm package without your root password:

```sh
npm install -g eslint

which eslint
# /home/me/.local/bin/eslint
```
