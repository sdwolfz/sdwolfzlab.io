---
title: "OPEN TEXT CO (OTEX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>OPEN TEXT CO</td></tr>
    <tr><td>Symbol</td><td>OTEX</td></tr>
    <tr><td>Web</td><td><a href="https://www.opentext.com">www.opentext.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.201 |
| 2020 | 0.725 |
| 2019 | 0.676 |
| 2018 | 0.587 |
| 2017 | 0.511 |
| 2016 | 0.89 |
| 2015 | 0.773 |
| 2014 | 0.668 |
| 2013 | 0.9 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
