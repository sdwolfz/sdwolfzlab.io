---
title: " (CCLP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CCLP</td></tr>
    <tr><td>Web</td><td><a href="https://www.csicompressco.com">www.csicompressco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.02 |
| 2020 | 0.04 |
| 2019 | 0.04 |
| 2018 | 0.75 |
| 2017 | 0.94 |
| 2016 | 1.51 |
| 2015 | 1.983 |
| 2014 | 1.795 |
| 2013 | 1.7 |
| 2012 | 1.56 |
| 2011 | 0.435 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
