---
title: "Murray Inc.tst. (MUT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Murray Inc.tst.</td></tr>
    <tr><td>Symbol</td><td>MUT</td></tr>
    <tr><td>Web</td><td><a href="https://www.murray-income.co.uk">www.murray-income.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 34.25 |
| 2019 | 34.0 |
| 2018 | 33.25 |
| 2017 | 32.75 |
| 2016 | 32.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
