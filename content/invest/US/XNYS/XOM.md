---
title: "EXXON MOBIL CORPORATION (XOM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>EXXON MOBIL CORPORATION</td></tr>
    <tr><td>Symbol</td><td>XOM</td></tr>
    <tr><td>Web</td><td><a href="https://www.exxonmobil.com">www.exxonmobil.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.74 |
| 2020 | 3.48 |
| 2019 | 3.43 |
| 2018 | 3.23 |
| 2017 | 3.81 |
| 2016 | 2.98 |
| 2015 | 2.88 |
| 2014 | 2.7 |
| 2013 | 1.26 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
