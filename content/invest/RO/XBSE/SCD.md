---
title: "ZENTIVA S.A. (SCD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>ZENTIVA S.A.</td></tr>
    <tr><td>Symbol</td><td>SCD</td></tr>
    <tr><td>Web</td><td><a href="https://www.zentiva.ro">www.zentiva.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.15589 |
| 2015 | 0.0959 |
| 2014 | 0.16788 |
| 2013 | 0.072 |
| 2012 | 0.048 |
| 2011 | 0.0812 |
| 2010 | 0.2998 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
