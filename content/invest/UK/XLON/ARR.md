---
title: "Aurora Inv.tst. (ARR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Aurora Inv.tst.</td></tr>
    <tr><td>Symbol</td><td>ARR</td></tr>
    <tr><td>Web</td><td><a href="https://www.aurorainvestmenttrust.com">www.aurorainvestmenttrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 4.5 |
| 2018 | 4.0 |
| 2017 | 2.75 |
| 2016 | 1.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
