---
title: "CONSTRUCTII FEROVIARE CRAIOVA SA CRAIOVA (CFED)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>CONSTRUCTII FEROVIARE CRAIOVA SA CRAIOVA</td></tr>
    <tr><td>Symbol</td><td>CFED</td></tr>
    <tr><td>Web</td><td><a href="https://www.constructiiferoviare-craiova.ro">www.constructiiferoviare-craiova.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2005 | 0.1187 |
| 2003 | 0.1883 |
| 2002 | 0.1155 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
