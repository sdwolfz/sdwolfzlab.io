---
title: "OMEGA FLEX INC (OFLX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>OMEGA FLEX INC</td></tr>
    <tr><td>Symbol</td><td>OFLX</td></tr>
    <tr><td>Web</td><td><a href="https://www.omegaflexcorp.com">www.omegaflexcorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 1.12 |
| 2019 | 4.58 |
| 2018 | 0.94 |
| 2017 | 0.66 |
| 2016 | 0.85 |
| 2015 | 0.85 |
| 2014 | 0.49 |
| 2013 | 0.425 |
| 2012 | 1.0 |
| 2009 | 2.0 |
| 2008 | 0.5 |
| 2007 | 0.7 |
| 2006 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
