---
title: "OPTICAL CABLE CORP (OCC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>OPTICAL CABLE CORP</td></tr>
    <tr><td>Symbol</td><td>OCC</td></tr>
    <tr><td>Web</td><td><a href="https://www.occfiber.com">www.occfiber.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 0.08 |
| 2014 | 0.08 |
| 2013 | 0.06 |
| 2012 | 0.08 |
| 2011 | 0.04 |
| 2010 | 0.01 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
