---
title: "ADIENT PLC (ADNT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ADIENT PLC</td></tr>
    <tr><td>Symbol</td><td>ADNT</td></tr>
    <tr><td>Web</td><td><a href="https://www.adient.com">www.adient.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 1.1 |
| 2017 | 0.825 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
