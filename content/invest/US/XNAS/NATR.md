---
title: "NATURE'S SUNSHINE PRODUCTS INC (NATR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NATURE'S SUNSHINE PRODUCTS INC</td></tr>
    <tr><td>Symbol</td><td>NATR</td></tr>
    <tr><td>Web</td><td><a href="https://www.naturessunshine.com">www.naturessunshine.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.0 |
| 2017 | 0.1 |
| 2016 | 0.4 |
| 2015 | 0.4 |
| 2014 | 1.9 |
| 2013 | 1.9 |
| 2012 | 0.15 |
| 2008 | 0.2 |
| 2007 | 0.2 |
| 2006 | 0.2 |
| 2005 | 0.2 |
| 2004 | 0.2 |
| 2003 | 0.133 |
| 2002 | 0.133 |
| 2001 | 0.133 |
| 2000 | 0.133 |
| 1999 | 0.133 |
| 1998 | 0.133 |
| 1997 | 0.133 |
| 1996 | 0.15 |
| 1995 | 0.2 |
| 1994 | 0.2 |
| 1993 | 0.2 |
| 1992 | 0.2 |
| 1991 | 0.24 |
| 1990 | 0.24 |
| 1989 | 0.24 |
| 1988 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
