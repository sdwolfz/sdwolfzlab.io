---
title: "GREENHILL & CO INC (GHL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GREENHILL & CO INC</td></tr>
    <tr><td>Symbol</td><td>GHL</td></tr>
    <tr><td>Web</td><td><a href="https://www.greenhill.com">www.greenhill.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.2 |
| 2019 | 0.2 |
| 2018 | 0.2 |
| 2017 | 1.4 |
| 2016 | 1.8 |
| 2015 | 1.8 |
| 2014 | 1.8 |
| 2013 | 0.9 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
