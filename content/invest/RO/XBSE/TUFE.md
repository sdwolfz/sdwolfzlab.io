---
title: "TURISM FELIX S.A. (TUFE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>TURISM FELIX S.A.</td></tr>
    <tr><td>Symbol</td><td>TUFE</td></tr>
    <tr><td>Web</td><td><a href="https://www.felixspa.com">www.felixspa.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.0101 |
| 2018 | 0.0093 |
| 2017 | 0.0045 |
| 2016 | 0.0058 |
| 2015 | 0.0064 |
| 2014 | 0.0039 |
| 2013 | 0.0042 |
| 2012 | 0.003 |
| 2003 | 0.096 |
| 2002 | 0.03 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
