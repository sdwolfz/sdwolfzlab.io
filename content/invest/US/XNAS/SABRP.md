---
title: " (SABRP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SABRP</td></tr>
    <tr><td>Web</td><td><a href="https://investors.sabre.com">investors.sabre.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 3.25 |
| 2020 | 1.751 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
