---
title: "Aptitude (APTD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Aptitude</td></tr>
    <tr><td>Symbol</td><td>APTD</td></tr>
    <tr><td>Web</td><td><a href="https://www.aptitudesoftware.com">www.aptitudesoftware.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 5.4 |
| 2019 | 5.4 |
| 2018 | 6.25 |
| 2017 | 6.25 |
| 2016 | 5.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
