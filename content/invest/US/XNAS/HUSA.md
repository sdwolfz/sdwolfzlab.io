---
title: "HOUSTON AMERICAN ENERGY CORPORATION (HUSA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HOUSTON AMERICAN ENERGY CORPORATION</td></tr>
    <tr><td>Symbol</td><td>HUSA</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2010 | 0.22 |
| 2009 | 0.035 |
| 2008 | 0.04 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
