---
title: "Informa (INF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Informa</td></tr>
    <tr><td>Symbol</td><td>INF</td></tr>
    <tr><td>Web</td><td><a href="https://www.informa.com">www.informa.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 23.5 |
| 2018 | 21.9 |
| 2017 | 20.45 |
| 2016 | 19.3 |
| 2015 | 20.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
