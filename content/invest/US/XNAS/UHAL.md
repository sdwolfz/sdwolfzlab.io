---
title: "AMERCO (UHAL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AMERCO</td></tr>
    <tr><td>Symbol</td><td>UHAL</td></tr>
    <tr><td>Web</td><td><a href="https://www.amerco.com">www.amerco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.5 |
| 2019 | 1.5 |
| 2018 | 2.0 |
| 2017 | 2.5 |
| 2016 | 2.0 |
| 2015 | 5.0 |
| 2014 | 1.0 |
| 2012 | 5.0 |
| 2011 | 1.0 |
| 1995 | 0.53 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
