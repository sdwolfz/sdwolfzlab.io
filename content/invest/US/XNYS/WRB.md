---
title: "BERKLEY(W.R.)CORP (WRB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BERKLEY(W.R.)CORP</td></tr>
    <tr><td>Symbol</td><td>WRB</td></tr>
    <tr><td>Web</td><td><a href="https://www.wrberkley.com">www.wrberkley.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 0.47 |
| 2019 | 1.68 |
| 2018 | 1.59 |
| 2017 | 1.55 |
| 2016 | 1.01 |
| 2015 | 0.47 |
| 2014 | 0.43 |
| 2013 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
