---
title: "CIRCOR INTERNATIONAL INC (CIR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CIRCOR INTERNATIONAL INC</td></tr>
    <tr><td>Symbol</td><td>CIR</td></tr>
    <tr><td>Web</td><td><a href="https://www.circor.com">www.circor.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 0.152 |
| 2016 | 0.152 |
| 2015 | 0.152 |
| 2014 | 0.152 |
| 2013 | 0.076 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
