---
title: "TIMBERLAND BANCORP (TSBK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TIMBERLAND BANCORP</td></tr>
    <tr><td>Symbol</td><td>TSBK</td></tr>
    <tr><td>Web</td><td><a href="https://www.timberlandbank.com">www.timberlandbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.52 |
| 2020 | 0.8 |
| 2019 | 0.8 |
| 2018 | 0.72 |
| 2017 | 0.52 |
| 2016 | 0.34 |
| 2015 | 0.31 |
| 2014 | 0.18 |
| 2013 | 0.12 |
| 2010 | 0.01 |
| 2009 | 0.31 |
| 2008 | 0.44 |
| 2007 | 0.56 |
| 2006 | 0.68 |
| 2005 | 0.62 |
| 2004 | 0.58 |
| 2003 | 0.52 |
| 2002 | 0.46 |
| 2001 | 0.42 |
| 2000 | 0.37 |
| 1999 | 0.29 |
| 1998 | 0.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
