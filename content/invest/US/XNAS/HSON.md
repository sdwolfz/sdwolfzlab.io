---
title: "HUDSON GLOBAL INC (HSON)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HUDSON GLOBAL INC</td></tr>
    <tr><td>Symbol</td><td>HSON</td></tr>
    <tr><td>Web</td><td><a href="https://www.hudson.com">www.hudson.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
