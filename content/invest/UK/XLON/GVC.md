---
title: "GVC HOLDINGS PLC ORD EUR0.01 (GVC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>GVC HOLDINGS PLC ORD EUR0.01</td></tr>
    <tr><td>Symbol</td><td>GVC</td></tr>
    <tr><td>Web</td><td><a href="http://www.gvc-plc.com">www.gvc-plc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 35.2 |
| 2018 | 32.0 |
| 2017 | 46.5 |
| 2015 | 56.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
