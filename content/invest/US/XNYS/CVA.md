---
title: "COVANTA HOLDING CORPORATION (CVA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>COVANTA HOLDING CORPORATION</td></tr>
    <tr><td>Symbol</td><td>CVA</td></tr>
    <tr><td>Web</td><td><a href="https://www.covanta.com">www.covanta.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.08 |
| 2020 | 0.49 |
| 2019 | 1.0 |
| 2018 | 1.0 |
| 2017 | 1.0 |
| 2016 | 1.0 |
| 2015 | 1.0 |
| 2014 | 0.86 |
| 2013 | 0.495 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
