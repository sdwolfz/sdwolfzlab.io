---
title: "WESTERN DIGITAL CORP (WDC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>WESTERN DIGITAL CORP</td></tr>
    <tr><td>Symbol</td><td>WDC</td></tr>
    <tr><td>Web</td><td><a href="https://www.westerndigital.com">www.westerndigital.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.0 |
| 2019 | 1.5 |
| 2018 | 2.0 |
| 2017 | 2.0 |
| 2016 | 2.0 |
| 2015 | 2.0 |
| 2014 | 1.5 |
| 2013 | 1.05 |
| 2012 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
