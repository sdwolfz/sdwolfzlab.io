---
title: "SPARTANNASH CO (SPTN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SPARTANNASH CO</td></tr>
    <tr><td>Symbol</td><td>SPTN</td></tr>
    <tr><td>Web</td><td><a href="https://www.spartannash.com">www.spartannash.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.77 |
| 2019 | 0.76 |
| 2018 | 0.72 |
| 2017 | 0.66 |
| 2016 | 0.6 |
| 2015 | 0.54 |
| 2014 | 0.48 |
| 2013 | 0.35 |
| 2012 | 0.305 |
| 2011 | 0.245 |
| 2010 | 0.2 |
| 2009 | 0.2 |
| 2008 | 0.2 |
| 2007 | 0.2 |
| 2006 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
