---
title: "Renishaw (RSW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Renishaw</td></tr>
    <tr><td>Symbol</td><td>RSW</td></tr>
    <tr><td>Web</td><td><a href="https://www.renishaw.com">www.renishaw.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 60.0 |
| 2018 | 60.0 |
| 2017 | 52.0 |
| 2016 | 48.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
