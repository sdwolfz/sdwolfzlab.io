---
title: "ONEMAIN HLDGS INC (OMF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ONEMAIN HLDGS INC</td></tr>
    <tr><td>Symbol</td><td>OMF</td></tr>
    <tr><td>Web</td><td><a href="https://www.omf.com">www.omf.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 4.65 |
| 2020 | 5.94 |
| 2019 | 3.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
