---
title: "ZYMEWORKS INC (ZYME)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ZYMEWORKS INC</td></tr>
    <tr><td>Symbol</td><td>ZYME</td></tr>
    <tr><td>Web</td><td><a href="https://www.zymeworks.com">www.zymeworks.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
