---
title: " (SBS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SBS</td></tr>
    <tr><td>Web</td><td><a href="https://www.sabesp.com.br">www.sabesp.com.br</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.053 |
| 2020 | 0.202 |
| 2019 | 0.244 |
| 2018 | 0.463 |
| 2017 | 0.288 |
| 2016 | 0.105 |
| 2015 | 0.088 |
| 2014 | 0.283 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
