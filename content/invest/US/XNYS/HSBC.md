---
title: " (HSBC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>HSBC</td></tr>
    <tr><td>Web</td><td><a href="https://www.hsbc.com">www.hsbc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.745 |
| 2020 | 1.045 |
| 2019 | 2.545 |
| 2018 | 2.55 |
| 2017 | 2.05 |
| 2016 | 2.55 |
| 2015 | 2.0 |
| 2014 | 2.45 |
| 2013 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
