---
title: " (UMC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>UMC</td></tr>
    <tr><td>Web</td><td><a href="https://www.umc.com">www.umc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.098 |
| 2019 | 0.065 |
| 2018 | 0.082 |
| 2017 | 0.058 |
| 2016 | 0.063 |
| 2015 | 0.059 |
| 2013 | 0.047 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
