---
title: "Inter. Pers. (IPF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Inter. Pers.</td></tr>
    <tr><td>Symbol</td><td>IPF</td></tr>
    <tr><td>Web</td><td><a href="https://www.ipfin.co.uk">www.ipfin.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 12.4 |
| 2018 | 12.4 |
| 2017 | 12.4 |
| 2016 | 12.4 |
| 2015 | 12.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
