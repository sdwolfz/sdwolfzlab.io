---
title: "IAC/INTERACTIVECORP NEW (IAC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>IAC/INTERACTIVECORP NEW</td></tr>
    <tr><td>Symbol</td><td>IAC</td></tr>
    <tr><td>Web</td><td><a href="https://www.iac.com">www.iac.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
