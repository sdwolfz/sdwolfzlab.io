---
title: "PPG INDUSTRIES INC (PPG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PPG INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>PPG</td></tr>
    <tr><td>Web</td><td><a href="https://www.ppg.com">www.ppg.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.08 |
| 2020 | 2.1 |
| 2019 | 1.98 |
| 2018 | 1.86 |
| 2017 | 1.7 |
| 2016 | 1.56 |
| 2015 | 1.75 |
| 2014 | 2.62 |
| 2013 | 1.22 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
