---
title: "BLACKROCK TCP CAPITAL CORP (TCPC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BLACKROCK TCP CAPITAL CORP</td></tr>
    <tr><td>Symbol</td><td>TCPC</td></tr>
    <tr><td>Web</td><td><a href="https://www.tcpcapital.com">www.tcpcapital.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 1.32 |
| 2019 | 1.44 |
| 2018 | 1.44 |
| 2017 | 1.44 |
| 2016 | 1.44 |
| 2015 | 1.44 |
| 2014 | 1.54 |
| 2013 | 1.53 |
| 2012 | 1.09 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
