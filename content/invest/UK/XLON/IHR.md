---
title: "Impact Health (IHR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Impact Health</td></tr>
    <tr><td>Symbol</td><td>IHR</td></tr>
    <tr><td>Web</td><td><a href="https://www.impactreit.uk">www.impactreit.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 6.17 |
| 2018 | 6.0 |
| 2017 | 4.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
