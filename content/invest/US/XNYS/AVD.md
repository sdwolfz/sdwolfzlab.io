---
title: "AMERICAN VANGUARD CORP (AVD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AMERICAN VANGUARD CORP</td></tr>
    <tr><td>Symbol</td><td>AVD</td></tr>
    <tr><td>Web</td><td><a href="https://www.american-vanguard.com">www.american-vanguard.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.02 |
| 2020 | 0.04 |
| 2019 | 0.08 |
| 2018 | 0.08 |
| 2017 | 0.06 |
| 2016 | 0.03 |
| 2015 | 0.02 |
| 2014 | 0.17 |
| 2013 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
