---
title: "OP BANCORP (OPBK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>OP BANCORP</td></tr>
    <tr><td>Symbol</td><td>OPBK</td></tr>
    <tr><td>Web</td><td><a href="https://www.myopenbank.com">www.myopenbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.14 |
| 2020 | 0.28 |
| 2019 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
