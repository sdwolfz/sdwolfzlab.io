---
title: " (CPT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CPT</td></tr>
    <tr><td>Web</td><td><a href="https://www.camdenliving.com">www.camdenliving.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.83 |
| 2020 | 3.32 |
| 2019 | 3.2 |
| 2018 | 3.08 |
| 2017 | 3.0 |
| 2016 | 3.0 |
| 2015 | 2.8 |
| 2014 | 2.64 |
| 2013 | 1.89 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
