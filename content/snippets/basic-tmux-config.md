---
title: "Basic tmux config"
date: 2020-12-20T13:42:17Z
type: posts
slug: basic-tmux-config
tags: ["tmux", "linux", "productivity"]
draft: true
---

Tmux is one of those pieces of software whose developers refuse to update it to
modern standards out of arrogance, just like `vim` and `emacs`, so you will have
to configure it alot in order to get a decent setup.

## Install

```sh
sudo pacman -S tmux
```

## Configure

```sh
# Xterm keys.
set -g xterm-keys on

# Mouse support.
set -g mouse on

# Use vim style navigation inside tmux.
setw -g mode-keys vi

# Configure the history limit.
set -g history-limit 500000

# Use `C-Space` as prefix.
set -g prefix C-Space
bind C-Space send-prefix
unbind C-b

# Fixes weird characters appearing on screen when yanking.
set -g set-clipboard off

# Clipboard usage for Linux.
if-shell "[[ `uname -a | grep Linux` ]]"                                                          \
  '                                                                                               \
    bind-key -T copy-mode-vi v                 send-keys -X begin-selection;                      \
    bind-key -T copy-mode-vi y                 send-keys -X copy-pipe "xclip -in -sel clipboard"; \
    bind-key -T copy-mode-vi MouseDragEnd1Pane send-keys -X copy-pipe "xclip -in -sel clipboard"  \
    bind-key -T copy-mode-vi DoubleClick1Pane                                                     \
      select-pane \;                                                                              \
      send-keys -X select-word \;                                                                 \
      send-keys -X copy-pipe "xclip -in -sel primary";                                            \
    bind-key -n DoubleClick1Pane                                                                  \
      select-pane \;                                                                              \
      copy-mode -M \;                                                                             \
      send-keys -X select-word \;                                                                 \
      send-keys -X copy-pipe "xclip -in -sel primary";                                            \
    bind-key -T copy-mode-vi TripleClick1Pane                                                     \
      select-pane \;                                                                              \
      send-keys -X select-line \;                                                                 \
      send-keys -X copy-pipe "xclip -in -sel primary";                                            \
    bind-key -n TripleClick1Pane                                                                  \
      select-pane \;                                                                              \
      copy-mode -M \;                                                                             \
      send-keys -X select-line \;                                                                 \
      send-keys -X copy-pipe "xclip -in -sel primary"                                             \
  '

# Navigate to panes.
bind-key h select-pane -L
bind-key j select-pane -D
bind-key k select-pane -U
bind-key l select-pane -R

# Resize panes
bind-key C-h resize-pane -L 10
bind-key C-j resize-pane -D 5
bind-key C-k resize-pane -U 5
bind-key C-l resize-pane -R 10

# Split panes.
bind '\' split-window -h -c '#{pane_current_path}'
bind  |  split-window -h -c '#{pane_current_path}'
bind  -  split-window -v -c '#{pane_current_path}'
bind  _  split-window -v -c '#{pane_current_path}'

# Create new window
bind c new-window -c '#{pane_current_path}'

# Clear scrollback and pane.
bind r send-keys -R C-l \; clear-history

# Type in multiple panes at the same time.
bind S set-window-option synchronize-panes

# Hide tmux status bar.
set -g status off

# Fix `ESC` key delay.
set -gs escape-time 10
```
