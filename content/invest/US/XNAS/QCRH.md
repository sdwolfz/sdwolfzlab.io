---
title: "QCR HOLDINGS INC (QCRH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>QCR HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>QCRH</td></tr>
    <tr><td>Web</td><td><a href="https://www.qcrh.com">www.qcrh.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.06 |
| 2020 | 0.24 |
| 2019 | 0.24 |
| 2018 | 0.24 |
| 2017 | 0.2 |
| 2016 | 0.16 |
| 2015 | 0.08 |
| 2014 | 0.08 |
| 2013 | 0.08 |
| 2012 | 0.08 |
| 2011 | 0.08 |
| 2010 | 0.08 |
| 2009 | 0.08 |
| 2008 | 0.08 |
| 2007 | 0.08 |
| 2006 | 0.08 |
| 2005 | 0.08 |
| 2004 | 0.08 |
| 2003 | 0.11 |
| 2002 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
