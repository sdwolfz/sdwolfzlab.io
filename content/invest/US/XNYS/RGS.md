---
title: "REGIS CORP (RGS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>REGIS CORP</td></tr>
    <tr><td>Symbol</td><td>RGS</td></tr>
    <tr><td>Web</td><td><a href="https://www.regiscorp.com">www.regiscorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2013 | 0.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
