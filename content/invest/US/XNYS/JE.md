---
title: "JUST ENERGY GROUP INC (JE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>JUST ENERGY GROUP INC</td></tr>
    <tr><td>Symbol</td><td>JE</td></tr>
    <tr><td>Web</td><td><a href="http://www.justenergygroup.com">www.justenergygroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.188 |
| 2018 | 0.383 |
| 2017 | 0.387 |
| 2016 | 0.375 |
| 2015 | 0.439 |
| 2014 | 0.67 |
| 2013 | 0.35 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
