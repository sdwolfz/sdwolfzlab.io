---
title: "CC NEUBERGER PRINCIPAL HOLDINGS I (PCPL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CC NEUBERGER PRINCIPAL HOLDINGS I</td></tr>
    <tr><td>Symbol</td><td>PCPL</td></tr>
    <tr><td>Web</td><td><a href="http://www.ccnbprincipal.com">www.ccnbprincipal.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
