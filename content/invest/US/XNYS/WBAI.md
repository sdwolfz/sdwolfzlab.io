---
title: "500.COM LTD SPONS ADR REP 10 SHS CL 'A' (WBAI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>500.COM LTD SPONS ADR REP 10 SHS CL 'A'</td></tr>
    <tr><td>Symbol</td><td>WBAI</td></tr>
    <tr><td>Web</td><td><a href="http://www.500.com">www.500.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
