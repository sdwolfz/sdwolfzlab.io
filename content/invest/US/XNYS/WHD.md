---
title: "CACTUS INC (WHD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CACTUS INC</td></tr>
    <tr><td>Symbol</td><td>WHD</td></tr>
    <tr><td>Web</td><td><a href="https://www.cactuswhd.com">www.cactuswhd.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.18 |
| 2020 | 0.36 |
| 2019 | 0.09 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
