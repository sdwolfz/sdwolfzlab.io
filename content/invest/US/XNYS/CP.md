---
title: "CANADIAN PACIFIC RAILWAY COMPANY (CP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CANADIAN PACIFIC RAILWAY COMPANY</td></tr>
    <tr><td>Symbol</td><td>CP</td></tr>
    <tr><td>Web</td><td><a href="https://www.cpr.ca">www.cpr.ca</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.656 |
| 2019 | 2.381 |
| 2018 | 1.911 |
| 2017 | 1.687 |
| 2016 | 1.394 |
| 2015 | 1.228 |
| 2014 | 1.4 |
| 2013 | 0.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
