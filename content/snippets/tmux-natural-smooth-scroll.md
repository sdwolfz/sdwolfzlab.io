---
title: "Tmux natural smooth scroll"
date: 2021-04-21T09:17:53Z
type: posts
slug: tmux-natural-smooth-scroll
tags: ["tmux", "linux", "terminal"]
---

Tmux is not capable of picking up natural scroll, you need to configure it:

```conf
# Mouse support.
set -g mouse on

# Natural scroll, line by line.
bind-key -T copy-mode-vi WheelUpPane   send-keys -X scroll-down-and-cancel
bind-key -T copy-mode-vi WheelDownPane send-keys -X scroll-up

# Don't overwrite scroll for tools that already know about it.
bind-key -n WheelDownPane if-shell -F "#{||:#{pane_in_mode},#{mouse_any_flag}}" "send -M" "copy-mode -e"
```
