---
title: "Schroder Eur.r (SERE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Schroder Eur.r</td></tr>
    <tr><td>Symbol</td><td>SERE</td></tr>
    <tr><td>Web</td><td><a href="https://www.schroders.co.uk">www.schroders.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 5.7 |
| 2019 | 7.4 |
| 2018 | 7.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
