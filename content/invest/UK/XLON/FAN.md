---
title: "Volution Group PLS (FAN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Volution Group PLS</td></tr>
    <tr><td>Symbol</td><td>FAN</td></tr>
    <tr><td>Web</td><td><a href="https://www.volutiongroupplc.uk">www.volutiongroupplc.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 4.9 |
| 2018 | 4.44 |
| 2017 | 4.15 |
| 2016 | 3.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
