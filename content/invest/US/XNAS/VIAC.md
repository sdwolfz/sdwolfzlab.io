---
title: "VIACOMCBS INC (VIAC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>VIACOMCBS INC</td></tr>
    <tr><td>Symbol</td><td>VIAC</td></tr>
    <tr><td>Web</td><td><a href="https://www.viacbs.com">www.viacbs.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.24 |
| 2020 | 0.96 |
| 2019 | 0.78 |
| 2018 | 0.72 |
| 2017 | 0.72 |
| 2016 | 0.66 |
| 2015 | 0.6 |
| 2014 | 0.54 |
| 2013 | 0.48 |
| 2012 | 0.44 |
| 2011 | 0.35 |
| 2010 | 0.2 |
| 2009 | 0.2 |
| 2008 | 1.06 |
| 2007 | 0.94 |
| 2006 | 0.74 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
