---
title: "AMKOR TECHNOLOGY INC (AMKR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AMKOR TECHNOLOGY INC</td></tr>
    <tr><td>Symbol</td><td>AMKR</td></tr>
    <tr><td>Web</td><td><a href="https://www.amkor.com">www.amkor.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.04 |
| 2020 | 0.04 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
