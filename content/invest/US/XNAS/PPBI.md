---
title: "PACIFIC PREMIER BANCORP INC (PPBI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PACIFIC PREMIER BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>PPBI</td></tr>
    <tr><td>Web</td><td><a href="https://www.ppbi.com">www.ppbi.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.63 |
| 2020 | 1.03 |
| 2019 | 0.88 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
