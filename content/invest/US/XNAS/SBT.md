---
title: "STERLING BANCORP INC (SBT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>STERLING BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>SBT</td></tr>
    <tr><td>Web</td><td><a href="https://www.sterlingbank.com">www.sterlingbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.01 |
| 2019 | 0.04 |
| 2018 | 0.04 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
