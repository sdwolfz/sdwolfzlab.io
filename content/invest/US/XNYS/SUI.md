---
title: " (SUI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SUI</td></tr>
    <tr><td>Web</td><td><a href="https://www.suncommunities.com">www.suncommunities.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.83 |
| 2020 | 3.16 |
| 2019 | 3.0 |
| 2018 | 2.84 |
| 2017 | 2.68 |
| 2016 | 2.6 |
| 2015 | 2.6 |
| 2014 | 2.6 |
| 2013 | 1.89 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
