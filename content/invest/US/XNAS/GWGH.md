---
title: "GWG HOLDINGS INC (GWGH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GWG HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>GWGH</td></tr>
    <tr><td>Web</td><td><a href="https://www.gwgh.com">www.gwgh.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 4.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
