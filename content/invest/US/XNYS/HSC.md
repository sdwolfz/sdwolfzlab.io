---
title: "HARSCO CORP (HSC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HARSCO CORP</td></tr>
    <tr><td>Symbol</td><td>HSC</td></tr>
    <tr><td>Web</td><td><a href="https://www.harsco.com">www.harsco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.051 |
| 2015 | 0.82 |
| 2014 | 0.82 |
| 2013 | 0.205 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
