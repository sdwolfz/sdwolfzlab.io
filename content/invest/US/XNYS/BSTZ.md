---
title: "BLACKROCK SCIENCE & TECH TR II (BSTZ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BLACKROCK SCIENCE & TECH TR II</td></tr>
    <tr><td>Symbol</td><td>BSTZ</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.743 |
| 2020 | 1.245 |
| 2019 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
