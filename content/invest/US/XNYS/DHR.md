---
title: "DANAHER CORP (DHR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DANAHER CORP</td></tr>
    <tr><td>Symbol</td><td>DHR</td></tr>
    <tr><td>Web</td><td><a href="https://www.danaher.com">www.danaher.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.42 |
| 2020 | 0.72 |
| 2019 | 0.68 |
| 2018 | 0.64 |
| 2017 | 0.56 |
| 2016 | 0.57 |
| 2015 | 0.675 |
| 2014 | 0.4 |
| 2013 | 0.075 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
