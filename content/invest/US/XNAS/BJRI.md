---
title: "BJS RESTAURANTS INC (BJRI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BJS RESTAURANTS INC</td></tr>
    <tr><td>Symbol</td><td>BJRI</td></tr>
    <tr><td>Web</td><td><a href="https://www.bjsrestaurants.com">www.bjsrestaurants.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.13 |
| 2019 | 0.49 |
| 2018 | 0.45 |
| 2017 | 0.11 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
