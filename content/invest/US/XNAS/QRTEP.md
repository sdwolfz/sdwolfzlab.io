---
title: " (QRTEP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>QRTEP</td></tr>
    <tr><td>Web</td><td><a href="https://www.qurateretail.com">www.qurateretail.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.0 |
| 2020 | 2.016 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
