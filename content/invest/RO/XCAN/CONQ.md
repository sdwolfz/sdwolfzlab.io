---
title: "CONTACTOARE SA BUZAU (CONQ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>CONTACTOARE SA BUZAU</td></tr>
    <tr><td>Symbol</td><td>CONQ</td></tr>
    <tr><td>Web</td><td><a href="https://www.contactoare.ro">www.contactoare.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.052366 |
| 2015 | 0.073313 |
| 2014 | 0.09876 |
| 2002 | 0.0558 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
