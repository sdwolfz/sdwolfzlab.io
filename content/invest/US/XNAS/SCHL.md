---
title: "SCHOLASTIC CORP (SCHL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SCHOLASTIC CORP</td></tr>
    <tr><td>Symbol</td><td>SCHL</td></tr>
    <tr><td>Web</td><td><a href="https://www.scholastic.com">www.scholastic.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 0.6 |
| 2019 | 0.6 |
| 2018 | 0.6 |
| 2017 | 0.6 |
| 2016 | 0.6 |
| 2015 | 0.6 |
| 2014 | 0.6 |
| 2013 | 0.525 |
| 2012 | 0.5 |
| 2011 | 0.4 |
| 2010 | 0.3 |
| 2009 | 0.3 |
| 2008 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
