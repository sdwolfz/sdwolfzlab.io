---
title: "FLOWERS FOODS INC (FLO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FLOWERS FOODS INC</td></tr>
    <tr><td>Symbol</td><td>FLO</td></tr>
    <tr><td>Web</td><td><a href="https://www.flowersfoods.com">www.flowersfoods.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.79 |
| 2019 | 0.75 |
| 2018 | 0.71 |
| 2017 | 0.67 |
| 2016 | 0.625 |
| 2015 | 0.567 |
| 2014 | 0.484 |
| 2013 | 0.224 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
