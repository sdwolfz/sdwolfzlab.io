---
title: "UNIPER S.E. (UNP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>UNIPER S.E.</td></tr>
    <tr><td>Symbol</td><td>UNP</td></tr>
    <tr><td>Web</td><td><a href="https://www.uniper.energy">www.uniper.energy</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
