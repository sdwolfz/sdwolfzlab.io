---
title: "INGREDION INC (INGR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>INGREDION INC</td></tr>
    <tr><td>Symbol</td><td>INGR</td></tr>
    <tr><td>Web</td><td><a href="https://www.ingredion.com">www.ingredion.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.64 |
| 2020 | 2.54 |
| 2019 | 2.51 |
| 2018 | 2.45 |
| 2017 | 2.2 |
| 2016 | 1.9 |
| 2015 | 1.74 |
| 2014 | 1.68 |
| 2013 | 1.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
