---
title: " (QTS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>QTS</td></tr>
    <tr><td>Web</td><td><a href="https://www.qtsdatacenters.com">www.qtsdatacenters.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.0 |
| 2020 | 1.88 |
| 2019 | 1.76 |
| 2018 | 1.64 |
| 2017 | 1.56 |
| 2016 | 1.44 |
| 2015 | 1.28 |
| 2014 | 1.16 |
| 2013 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
