---
title: "Marks & Spencer (MKS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Marks & Spencer</td></tr>
    <tr><td>Symbol</td><td>MKS</td></tr>
    <tr><td>Web</td><td><a href="https://www.marksandspencer.com">www.marksandspencer.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 3.9 |
| 2019 | 18.7 |
| 2018 | 18.7 |
| 2017 | 18.7 |
| 2016 | 18.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
