---
title: "EDUCATIONAL DEVELOPMENT CORP (EDUC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>EDUCATIONAL DEVELOPMENT CORP</td></tr>
    <tr><td>Symbol</td><td>EDUC</td></tr>
    <tr><td>Web</td><td><a href="https://www.edcpub.com">www.edcpub.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.27 |
| 2019 | 0.2 |
| 2018 | 0.2 |
| 2016 | 0.36 |
| 2015 | 0.34 |
| 2014 | 0.32 |
| 2013 | 0.32 |
| 2012 | 0.48 |
| 2011 | 0.48 |
| 2010 | 0.51 |
| 2009 | 0.55 |
| 2008 | 0.8 |
| 2007 | 0.22 |
| 2006 | 0.2 |
| 2005 | 0.15 |
| 2004 | 0.12 |
| 2003 | 0.1 |
| 2002 | 0.06 |
| 2001 | 0.04 |
| 2000 | 0.02 |
| 1999 | 0.02 |
| 1998 | 0.02 |
| 1997 | 0.01 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
