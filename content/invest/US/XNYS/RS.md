---
title: "RELIANCE STEEL & ALUMINIUM (RS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>RELIANCE STEEL & ALUMINIUM</td></tr>
    <tr><td>Symbol</td><td>RS</td></tr>
    <tr><td>Web</td><td><a href="https://www.rsac.com">www.rsac.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.376 |
| 2020 | 2.5 |
| 2019 | 2.2 |
| 2018 | 2.0 |
| 2017 | 1.8 |
| 2016 | 1.65 |
| 2015 | 1.6 |
| 2014 | 1.4 |
| 2013 | 0.66 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
