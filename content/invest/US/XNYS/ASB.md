---
title: "ASSOCIATED BANC-CORP (ASB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ASSOCIATED BANC-CORP</td></tr>
    <tr><td>Symbol</td><td>ASB</td></tr>
    <tr><td>Web</td><td><a href="https://www.associatedbank.com">www.associatedbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.36 |
| 2020 | 0.72 |
| 2019 | 0.69 |
| 2018 | 0.62 |
| 2017 | 0.5 |
| 2016 | 0.45 |
| 2015 | 0.41 |
| 2014 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
