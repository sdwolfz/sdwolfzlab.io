---
title: "OTTER TAIL CORP (OTTR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>OTTER TAIL CORP</td></tr>
    <tr><td>Symbol</td><td>OTTR</td></tr>
    <tr><td>Web</td><td><a href="https://www.ottertail.com">www.ottertail.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.78 |
| 2020 | 1.48 |
| 2019 | 1.4 |
| 2018 | 1.34 |
| 2017 | 1.28 |
| 2016 | 1.25 |
| 2015 | 1.23 |
| 2014 | 1.21 |
| 2013 | 1.19 |
| 2012 | 1.19 |
| 2011 | 1.19 |
| 2010 | 1.19 |
| 2009 | 1.19 |
| 2008 | 1.19 |
| 2007 | 1.17 |
| 2006 | 1.15 |
| 2005 | 1.12 |
| 2004 | 1.1 |
| 2003 | 1.08 |
| 2002 | 1.06 |
| 2001 | 1.04 |
| 2000 | 1.275 |
| 1999 | 1.98 |
| 1998 | 1.92 |
| 1997 | 1.86 |
| 1996 | 1.8 |
| 1995 | 1.76 |
| 1994 | 1.72 |
| 1993 | 1.68 |
| 1992 | 1.64 |
| 1991 | 1.6 |
| 1990 | 1.56 |
| 1989 | 1.52 |
| 1988 | 0.37 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
