---
title: "Jupiter European Opportunities Trust (JEO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Jupiter European Opportunities Trust</td></tr>
    <tr><td>Symbol</td><td>JEO</td></tr>
    <tr><td>Web</td><td><a href="https://www.devonem.com">www.devonem.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 3.5 |
| 2019 | 5.5 |
| 2018 | 6.5 |
| 2017 | 6.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
