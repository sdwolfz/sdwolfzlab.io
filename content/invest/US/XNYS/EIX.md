---
title: "EDISON INTERNATIONAL (EIX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>EDISON INTERNATIONAL</td></tr>
    <tr><td>Symbol</td><td>EIX</td></tr>
    <tr><td>Web</td><td><a href="https://www.edison.com">www.edison.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.663 |
| 2020 | 2.574 |
| 2019 | 2.476 |
| 2018 | 2.428 |
| 2017 | 2.234 |
| 2016 | 1.983 |
| 2015 | 1.731 |
| 2014 | 1.482 |
| 2013 | 1.031 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
