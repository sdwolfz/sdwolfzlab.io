---
title: "DELTA APPAREL INC (DLA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>DELTA APPAREL INC</td></tr>
    <tr><td>Symbol</td><td>DLA</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2007 | 0.15 |
| 2006 | 0.19 |
| 2005 | 0.19 |
| 2004 | 0.27 |
| 2003 | 0.23 |
| 2002 | 0.25 |
| 2000 | 0.001 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
