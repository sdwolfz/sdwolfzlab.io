---
title: " (STON)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>STON</td></tr>
    <tr><td>Web</td><td><a href="https://www.stonemor.com">www.stonemor.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 0.66 |
| 2016 | 2.31 |
| 2015 | 2.58 |
| 2014 | 2.43 |
| 2013 | 1.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
