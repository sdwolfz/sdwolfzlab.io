---
title: "HEARTLAND FINANCIAL USA INC (HTLF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HEARTLAND FINANCIAL USA INC</td></tr>
    <tr><td>Symbol</td><td>HTLF</td></tr>
    <tr><td>Web</td><td><a href="https://www.htlf.com">www.htlf.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.44 |
| 2020 | 0.8 |
| 2019 | 0.68 |
| 2018 | 0.59 |
| 2017 | 0.51 |
| 2016 | 0.5 |
| 2015 | 0.45 |
| 2014 | 0.4 |
| 2013 | 0.4 |
| 2012 | 0.5 |
| 2011 | 0.4 |
| 2010 | 0.4 |
| 2009 | 0.4 |
| 2008 | 0.4 |
| 2007 | 0.37 |
| 2006 | 0.36 |
| 2005 | 0.33 |
| 2004 | 0.32 |
| 2003 | 0.41 |
| 2002 | 0.4 |
| 2001 | 0.37 |
| 2000 | 0.36 |
| 1999 | 0.34 |
| 1998 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
