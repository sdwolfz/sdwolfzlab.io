---
title: "CHUBB LIMITED (CB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CHUBB LIMITED</td></tr>
    <tr><td>Symbol</td><td>CB</td></tr>
    <tr><td>Web</td><td><a href="https://www.chubb.com">www.chubb.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.78 |
| 2020 | 3.09 |
| 2019 | 2.98 |
| 2018 | 2.9 |
| 2017 | 2.82 |
| 2016 | 2.74 |
| 2015 | 2.95 |
| 2014 | 2.0 |
| 2013 | 1.32 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
