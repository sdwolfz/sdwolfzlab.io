---
title: "CONSOLIDATED COMMUNICATIONS HLDGS (CNSL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CONSOLIDATED COMMUNICATIONS HLDGS</td></tr>
    <tr><td>Symbol</td><td>CNSL</td></tr>
    <tr><td>Web</td><td><a href="https://www.consolidated.com">www.consolidated.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.775 |
| 2018 | 1.55 |
| 2017 | 1.55 |
| 2016 | 1.55 |
| 2015 | 1.55 |
| 2014 | 1.55 |
| 2013 | 1.55 |
| 2012 | 1.55 |
| 2011 | 1.55 |
| 2010 | 1.55 |
| 2009 | 1.55 |
| 2008 | 1.55 |
| 2007 | 1.55 |
| 2006 | 1.55 |
| 2005 | 0.409 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
