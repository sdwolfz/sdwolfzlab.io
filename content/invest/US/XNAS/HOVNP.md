---
title: " (HOVNP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>HOVNP</td></tr>
    <tr><td>Web</td><td><a href="https://www.khov.com">www.khov.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2007 | 1.43 |
| 2006 | 1.906 |
| 2005 | 0.969 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
