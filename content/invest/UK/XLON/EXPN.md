---
title: "Experian (EXPN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Experian</td></tr>
    <tr><td>Symbol</td><td>EXPN</td></tr>
    <tr><td>Web</td><td><a href="https://www.experianplc.com">www.experianplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 46.5 |
| 2018 | 44.75 |
| 2017 | 41.5 |
| 2016 | 40.0 |
| 2015 | 39.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
