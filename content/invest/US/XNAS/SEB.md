---
title: "SEABOARD CORP DELAWARE (SEB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SEABOARD CORP DELAWARE</td></tr>
    <tr><td>Symbol</td><td>SEB</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 4.5 |
| 2020 | 9.0 |
| 2019 | 9.0 |
| 2018 | 6.0 |
| 2017 | 6.0 |
| 2012 | 12.0 |
| 2010 | 2.25 |
| 2009 | 3.0 |
| 2008 | 3.0 |
| 2007 | 3.0 |
| 2006 | 3.0 |
| 2005 | 3.0 |
| 2004 | 3.0 |
| 2003 | 2.25 |
| 2002 | 2.5 |
| 2001 | 1.0 |
| 2000 | 1.0 |
| 1999 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
