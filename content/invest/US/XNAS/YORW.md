---
title: "YORK WATER CO (YORW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>YORK WATER CO</td></tr>
    <tr><td>Symbol</td><td>YORW</td></tr>
    <tr><td>Web</td><td><a href="https://www.yorkwater.com">www.yorkwater.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.375 |
| 2020 | 0.728 |
| 2019 | 0.7 |
| 2018 | 0.673 |
| 2017 | 0.647 |
| 2016 | 0.627 |
| 2015 | 0.604 |
| 2014 | 0.579 |
| 2013 | 0.558 |
| 2012 | 0.539 |
| 2011 | 0.527 |
| 2010 | 0.515 |
| 2009 | 0.506 |
| 2008 | 0.489 |
| 2007 | 0.475 |
| 2006 | 0.566 |
| 2005 | 0.636 |
| 2004 | 0.591 |
| 2003 | 0.55 |
| 2002 | 0.655 |
| 2001 | 1.01 |
| 2000 | 0.98 |
| 1999 | 0.945 |
| 1998 | 0.235 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
