---
title: "BANKUNITED INC (BKU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BANKUNITED INC</td></tr>
    <tr><td>Symbol</td><td>BKU</td></tr>
    <tr><td>Web</td><td><a href="https://www.bankunited.com">www.bankunited.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.46 |
| 2020 | 0.9 |
| 2019 | 0.84 |
| 2018 | 0.84 |
| 2017 | 0.84 |
| 2016 | 0.84 |
| 2015 | 0.63 |
| 2014 | 1.05 |
| 2013 | 0.42 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
