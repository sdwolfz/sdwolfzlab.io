---
title: "NORTHWEST NATURAL HOLDING COMPANY (NWN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NORTHWEST NATURAL HOLDING COMPANY</td></tr>
    <tr><td>Symbol</td><td>NWN</td></tr>
    <tr><td>Web</td><td><a href="https://www.nwnaturalholdings.com">www.nwnaturalholdings.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.96 |
| 2020 | 1.911 |
| 2019 | 1.902 |
| 2018 | 1.891 |
| 2017 | 1.882 |
| 2016 | 1.874 |
| 2015 | 1.863 |
| 2014 | 1.845 |
| 2013 | 0.915 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
