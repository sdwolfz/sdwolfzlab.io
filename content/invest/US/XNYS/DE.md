---
title: "DEERE & CO (DE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DEERE & CO</td></tr>
    <tr><td>Symbol</td><td>DE</td></tr>
    <tr><td>Web</td><td><a href="https://www.JohnDeere.com">www.JohnDeere.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.9 |
| 2020 | 3.04 |
| 2019 | 3.04 |
| 2018 | 2.74 |
| 2017 | 2.4 |
| 2016 | 2.4 |
| 2015 | 2.4 |
| 2014 | 2.31 |
| 2013 | 1.53 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
