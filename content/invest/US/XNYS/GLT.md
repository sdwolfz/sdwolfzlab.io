---
title: "GLATFELTER CORPORATION (GLT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GLATFELTER CORPORATION</td></tr>
    <tr><td>Symbol</td><td>GLT</td></tr>
    <tr><td>Web</td><td><a href="https://www.glatfelter.com">www.glatfelter.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.135 |
| 2020 | 0.665 |
| 2019 | 0.52 |
| 2018 | 0.52 |
| 2017 | 0.39 |
| 2016 | 0.5 |
| 2015 | 0.48 |
| 2014 | 0.44 |
| 2013 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
