---
title: "EQUITABLE HOLDINGS INC (EQH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>EQUITABLE HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>EQH</td></tr>
    <tr><td>Web</td><td><a href="https://www.equitable.com">www.equitable.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.17 |
| 2020 | 0.66 |
| 2019 | 0.58 |
| 2018 | 0.26 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
