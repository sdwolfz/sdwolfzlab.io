---
title: "Vitec (VTC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Vitec</td></tr>
    <tr><td>Symbol</td><td>VTC</td></tr>
    <tr><td>Web</td><td><a href="https://www.vitecgroup.com">www.vitecgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 39.0 |
| 2018 | 37.0 |
| 2017 | 30.5 |
| 2016 | 27.2 |
| 2015 | 24.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
