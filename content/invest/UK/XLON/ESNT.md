---
title: "Essentra (ESNT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Essentra</td></tr>
    <tr><td>Symbol</td><td>ESNT</td></tr>
    <tr><td>Web</td><td><a href="https://www.essentra.com">www.essentra.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 20.7 |
| 2018 | 20.7 |
| 2017 | 20.7 |
| 2016 | 20.7 |
| 2015 | 20.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
