---
title: "MID SOUTHERN BANCORP INC (MSVB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MID SOUTHERN BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>MSVB</td></tr>
    <tr><td>Web</td><td><a href="https://www.mid-southern.com">www.mid-southern.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.06 |
| 2020 | 0.08 |
| 2019 | 0.08 |
| 2018 | 0.02 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
