---
title: "EXPEDIA GROUP INC (EXPE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>EXPEDIA GROUP INC</td></tr>
    <tr><td>Symbol</td><td>EXPE</td></tr>
    <tr><td>Web</td><td><a href="https://www.expediagroup.com">www.expediagroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.34 |
| 2019 | 1.32 |
| 2018 | 1.24 |
| 2017 | 1.16 |
| 2016 | 1.0 |
| 2015 | 0.84 |
| 2014 | 0.66 |
| 2013 | 0.56 |
| 2012 | 0.96 |
| 2011 | 1.98 |
| 2010 | 0.28 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
