---
title: "NUTRIEN LTD (NTR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NUTRIEN LTD</td></tr>
    <tr><td>Symbol</td><td>NTR</td></tr>
    <tr><td>Web</td><td><a href="https://www.nutrien.com">www.nutrien.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.8 |
| 2019 | 1.76 |
| 2018 | 1.63 |
| 2017 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
