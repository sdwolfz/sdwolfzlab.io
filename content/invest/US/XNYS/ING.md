---
title: " (ING)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ING</td></tr>
    <tr><td>Web</td><td><a href="https://www.ing.com">www.ing.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.123 |
| 2019 | 0.616 |
| 2018 | 0.639 |
| 2017 | 0.605 |
| 2016 | 0.598 |
| 2015 | 0.328 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
