---
title: "KRAFT HEINZ CO (KHC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>KRAFT HEINZ CO</td></tr>
    <tr><td>Symbol</td><td>KHC</td></tr>
    <tr><td>Web</td><td><a href="https://www.kraftheinzcompany.com">www.kraftheinzcompany.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.8 |
| 2020 | 1.6 |
| 2019 | 1.6 |
| 2018 | 2.5 |
| 2017 | 2.45 |
| 2016 | 2.35 |
| 2015 | 1.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
