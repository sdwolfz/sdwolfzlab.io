---
title: "SINATEX SA BUCURESTI (SINA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>SINATEX SA BUCURESTI</td></tr>
    <tr><td>Symbol</td><td>SINA</td></tr>
    <tr><td>Web</td><td><a href="https://www.sinatex.ro">www.sinatex.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 3.876 |
| 2018 | 2.543 |
| 2017 | 8.171 |
| 2016 | 2.9693 |
| 2015 | 0.9418 |
| 2014 | 0.5622 |
| 2013 | 0.925 |
| 2009 | 3.714 |
| 2008 | 3.3131 |
| 2006 | 1.69 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
