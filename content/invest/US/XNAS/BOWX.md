---
title: "BOWX ACQUISITION CORP (BOWX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BOWX ACQUISITION CORP</td></tr>
    <tr><td>Symbol</td><td>BOWX</td></tr>
    <tr><td>Web</td><td><a href="https://www.bowcapital.com">www.bowcapital.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
