---
title: "9 METERS BIOPHARMA INC (NMTR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>9 METERS BIOPHARMA INC</td></tr>
    <tr><td>Symbol</td><td>NMTR</td></tr>
    <tr><td>Web</td><td><a href="https://www.9meters.com">www.9meters.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
