---
title: " (GGAL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>GGAL</td></tr>
    <tr><td>Web</td><td><a href="https://www.gfgsa.com">www.gfgsa.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.076 |
| 2019 | 0.307 |
| 2018 | 0.362 |
| 2017 | 0.119 |
| 2016 | 0.082 |
| 2015 | 0.06 |
| 2014 | 0.036 |
| 2013 | 0.037 |
| 2012 | 0.032 |
| 2011 | 0.049 |
| 2004 | 0.025 |
| 2001 | 0.284 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
