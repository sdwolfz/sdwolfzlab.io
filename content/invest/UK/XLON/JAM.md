---
title: "JPMorgan American (JAM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>JPMorgan American</td></tr>
    <tr><td>Symbol</td><td>JAM</td></tr>
    <tr><td>Web</td><td><a href="https://www.jpmfamerican.com">www.jpmfamerican.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 6.5 |
| 2018 | 6.5 |
| 2017 | 5.5 |
| 2016 | 5.0 |
| 2015 | 4.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
