---
title: "LAWSON PRODUCTS INC (LAWS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LAWSON PRODUCTS INC</td></tr>
    <tr><td>Symbol</td><td>LAWS</td></tr>
    <tr><td>Web</td><td><a href="https://www.lawsonproducts.com">www.lawsonproducts.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2012 | 0.24 |
| 2011 | 0.48 |
| 2010 | 0.32 |
| 2009 | 0.38 |
| 2008 | 0.8 |
| 2007 | 0.8 |
| 2006 | 0.6 |
| 2005 | 0.8 |
| 2004 | 0.72 |
| 2003 | 0.66 |
| 2002 | 0.64 |
| 2001 | 0.79 |
| 2000 | 0.6 |
| 1999 | 0.42 |
| 1998 | 0.56 |
| 1997 | 0.54 |
| 1996 | 0.52 |
| 1995 | 0.64 |
| 1994 | 0.48 |
| 1993 | 0.44 |
| 1992 | 0.58 |
| 1991 | 0.4 |
| 1990 | 0.37 |
| 1989 | 0.33 |
| 1988 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
