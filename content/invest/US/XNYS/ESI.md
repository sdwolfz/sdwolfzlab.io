---
title: "ELEMENT SOLUTIONS INC (ESI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ELEMENT SOLUTIONS INC</td></tr>
    <tr><td>Symbol</td><td>ESI</td></tr>
    <tr><td>Web</td><td><a href="https://www.elementsolutionsinc.com">www.elementsolutionsinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.05 |
| 2020 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
