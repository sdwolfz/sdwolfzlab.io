---
title: "CELANESE CORP (CE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CELANESE CORP</td></tr>
    <tr><td>Symbol</td><td>CE</td></tr>
    <tr><td>Web</td><td><a href="https://www.celanese.com">www.celanese.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.36 |
| 2020 | 2.48 |
| 2019 | 2.4 |
| 2018 | 2.08 |
| 2017 | 1.74 |
| 2016 | 1.38 |
| 2015 | 1.15 |
| 2014 | 0.93 |
| 2013 | 0.36 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
