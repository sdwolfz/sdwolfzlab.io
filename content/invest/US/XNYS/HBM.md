---
title: "HUDBAY MINERALS INC (HBM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HUDBAY MINERALS INC</td></tr>
    <tr><td>Symbol</td><td>HBM</td></tr>
    <tr><td>Web</td><td><a href="https://www.hudbayminerals.com">www.hudbayminerals.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.007 |
| 2019 | 0.016 |
| 2018 | 0.016 |
| 2017 | 0.016 |
| 2016 | 0.015 |
| 2015 | 0.018 |
| 2014 | 0.02 |
| 2013 | 0.01 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
