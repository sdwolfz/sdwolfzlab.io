---
title: "Bluefield Solar (BSIF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Bluefield Solar</td></tr>
    <tr><td>Symbol</td><td>BSIF</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 7.9 |
| 2019 | 8.31 |
| 2018 | 7.43 |
| 2017 | 7.25 |
| 2016 | 7.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
