---
title: "Kingfisher (KGF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Kingfisher</td></tr>
    <tr><td>Symbol</td><td>KGF</td></tr>
    <tr><td>Web</td><td><a href="https://www.kingfisher.co.uk">www.kingfisher.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 3.33 |
| 2019 | 10.82 |
| 2018 | 10.82 |
| 2017 | 10.4 |
| 2016 | 10.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
