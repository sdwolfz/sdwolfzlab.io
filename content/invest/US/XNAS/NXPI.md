---
title: "NXP SEMICONDUCTORS N V (NXPI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NXP SEMICONDUCTORS N V</td></tr>
    <tr><td>Symbol</td><td>NXPI</td></tr>
    <tr><td>Web</td><td><a href="https://www.nxp.com">www.nxp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.563 |
| 2020 | 1.5 |
| 2019 | 1.25 |
| 2018 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
