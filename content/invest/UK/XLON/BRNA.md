---
title: "Blackrock Brna (BRNA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Blackrock Brna</td></tr>
    <tr><td>Symbol</td><td>BRNA</td></tr>
    <tr><td>Web</td><td><a href="https://www.blackrock.com">www.blackrock.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 8.0 |
| 2018 | 8.0 |
| 2017 | 4.95 |
| 2016 | 4.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
