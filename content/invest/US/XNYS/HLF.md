---
title: "HERBALIFE NUTRITION LTD (HLF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HERBALIFE NUTRITION LTD</td></tr>
    <tr><td>Symbol</td><td>HLF</td></tr>
    <tr><td>Web</td><td><a href="https://www.herbalife.com">www.herbalife.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 0.3 |
| 2013 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
