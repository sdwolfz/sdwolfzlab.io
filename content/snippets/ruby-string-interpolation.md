---
title: "Ruby string interpolation"
date: 2020-06-10T22:01:20Z
type: posts
slug: ruby-string-interpolation
tags: ["ruby"]
---

1. The bad way:
```ruby
count = 3.to_s
puts 'I ate ' + count + ' burgers today!'
```

2. The lame way:
```ruby
yuck = 'salad'
puts "No #{yuck} for me please!"
```

3. The woke way (notice where the `.chomp` call goes):
```ruby
template = <<~TEMPLATE.chomp
  A large steak, %{how_done}, please!
TEMPLATE

puts template % { how_done: 'medium rare' }
```

For templating, follow this link:
* [Ruby templating with ERB](/snippets/ruby-templating-with-erb)
