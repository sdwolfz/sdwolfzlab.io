---
title: "MIDDLESEX WATER CO (MSEX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MIDDLESEX WATER CO</td></tr>
    <tr><td>Symbol</td><td>MSEX</td></tr>
    <tr><td>Web</td><td><a href="https://www.middlesexwater.com">www.middlesexwater.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.545 |
| 2020 | 1.041 |
| 2019 | 0.976 |
| 2018 | 0.911 |
| 2017 | 0.857 |
| 2016 | 0.808 |
| 2015 | 0.776 |
| 2014 | 0.763 |
| 2013 | 0.753 |
| 2012 | 0.742 |
| 2011 | 0.733 |
| 2010 | 0.722 |
| 2009 | 0.712 |
| 2008 | 0.702 |
| 2007 | 0.52 |
| 2006 | 0.683 |
| 2005 | 0.673 |
| 2004 | 0.663 |
| 2003 | 0.81 |
| 2002 | 0.845 |
| 2001 | 1.245 |
| 2000 | 1.225 |
| 1999 | 1.19 |
| 1998 | 1.15 |
| 1997 | 1.125 |
| 1996 | 1.105 |
| 1995 | 1.085 |
| 1994 | 1.057 |
| 1993 | 1.013 |
| 1992 | 1.69 |
| 1991 | 1.89 |
| 1990 | 1.85 |
| 1989 | 1.795 |
| 1988 | 0.445 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
