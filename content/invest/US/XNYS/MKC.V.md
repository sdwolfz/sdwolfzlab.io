---
title: "MCCORMICK & COMPANY INC (MKC.V)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MCCORMICK & COMPANY INC</td></tr>
    <tr><td>Symbol</td><td>MKC.V</td></tr>
    <tr><td>Web</td><td><a href="https://www.mccormickcorporation.com">www.mccormickcorporation.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.34 |
| 2020 | 2.2 |
| 2019 | 2.33 |
| 2018 | 2.13 |
| 2017 | 1.93 |
| 2016 | 1.76 |
| 2015 | 1.63 |
| 2014 | 1.51 |
| 2013 | 1.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
