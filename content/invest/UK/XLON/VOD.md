---
title: "Vodafone (VOD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Vodafone</td></tr>
    <tr><td>Symbol</td><td>VOD</td></tr>
    <tr><td>Web</td><td><a href="https://www.vodafone.com">www.vodafone.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 9.0 |
| 2019 | 9.0 |
| 2018 | 15.07 |
| 2017 | 13.46 |
| 2016 | 10.9 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
