---
title: " (YRD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>YRD</td></tr>
    <tr><td>Web</td><td><a href="https://www.yirendai.com">www.yirendai.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.26 |
| 2017 | 2.96 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
