---
title: "KNOLL INC (KNL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>KNOLL INC</td></tr>
    <tr><td>Symbol</td><td>KNL</td></tr>
    <tr><td>Web</td><td><a href="https://www.knoll.com">www.knoll.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.06 |
| 2020 | 0.33 |
| 2019 | 0.66 |
| 2018 | 0.6 |
| 2017 | 0.6 |
| 2016 | 0.6 |
| 2015 | 0.51 |
| 2014 | 0.48 |
| 2013 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
