---
title: "TOLL BROS INC (TOL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TOLL BROS INC</td></tr>
    <tr><td>Symbol</td><td>TOL</td></tr>
    <tr><td>Web</td><td><a href="https://www.tollbrothers.com">www.tollbrothers.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 0.44 |
| 2019 | 0.44 |
| 2018 | 0.41 |
| 2017 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
