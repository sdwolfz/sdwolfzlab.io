---
title: "Twentyfour Sel (SMIF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Twentyfour Sel</td></tr>
    <tr><td>Symbol</td><td>SMIF</td></tr>
    <tr><td>Web</td><td><a href="https://www.twentyfouram.com">www.twentyfouram.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.34 |
| 2019 | 6.55 |
| 2018 | 6.55 |
| 2017 | 6.56 |
| 2016 | 6.85 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
