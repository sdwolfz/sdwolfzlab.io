---
title: "U S SILICA HLDGS INC (SLCA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>U S SILICA HLDGS INC</td></tr>
    <tr><td>Symbol</td><td>SLCA</td></tr>
    <tr><td>Web</td><td><a href="https://www.ussilica.com">www.ussilica.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.02 |
| 2019 | 0.252 |
| 2018 | 0.252 |
| 2017 | 0.252 |
| 2016 | 0.252 |
| 2015 | 0.438 |
| 2014 | 0.5 |
| 2013 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
