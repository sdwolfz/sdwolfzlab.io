---
title: "EMAGIN CORP (EMAN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>EMAGIN CORP</td></tr>
    <tr><td>Symbol</td><td>EMAN</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2012 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
