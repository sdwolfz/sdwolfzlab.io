---
title: "GANNETT CO INC (GCI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GANNETT CO INC</td></tr>
    <tr><td>Symbol</td><td>GCI</td></tr>
    <tr><td>Web</td><td><a href="https://www.gannett.com">www.gannett.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.86 |
| 2018 | 0.64 |
| 2017 | 0.64 |
| 2016 | 0.64 |
| 2015 | 0.72 |
| 2014 | 0.8 |
| 2013 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
