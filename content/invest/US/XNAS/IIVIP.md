---
title: " (IIVIP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>IIVIP</td></tr>
    <tr><td>Web</td><td><a href="https://www.ii-vi.com">www.ii-vi.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 3.0 |
| 2020 | 5.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
