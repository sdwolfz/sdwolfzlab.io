---
title: "DTE ENERGY CO (DTE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DTE ENERGY CO</td></tr>
    <tr><td>Symbol</td><td>DTE</td></tr>
    <tr><td>Web</td><td><a href="https://www.dteenergy.com">www.dteenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.085 |
| 2020 | 4.124 |
| 2019 | 3.848 |
| 2018 | 3.591 |
| 2017 | 3.357 |
| 2016 | 3.055 |
| 2015 | 2.84 |
| 2014 | 2.69 |
| 2013 | 1.31 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
