---
title: "PENNS WOODS BANCORP INC (PWOD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PENNS WOODS BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>PWOD</td></tr>
    <tr><td>Web</td><td><a href="https://www.pwod.com">www.pwod.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.32 |
| 2020 | 1.28 |
| 2019 | 1.73 |
| 2018 | 1.88 |
| 2017 | 1.88 |
| 2016 | 1.88 |
| 2015 | 1.88 |
| 2014 | 1.88 |
| 2013 | 2.13 |
| 2012 | 1.88 |
| 2011 | 1.84 |
| 2010 | 1.84 |
| 2009 | 1.84 |
| 2008 | 1.84 |
| 2007 | 1.79 |
| 2006 | 1.73 |
| 2005 | 1.79 |
| 2004 | 1.76 |
| 2003 | 1.58 |
| 2002 | 1.36 |
| 2001 | 1.22 |
| 2000 | 1.1 |
| 1999 | 1.03 |
| 1998 | 0.46 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
