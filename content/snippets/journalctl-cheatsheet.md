---
title: "JournalCTL cheatsheet"
date: 2021-10-26T16:02:29Z
type: posts
slug: journalctl-cheatsheet
tags: ["linux", "journalctl", "systemd"]
---

## History

View history:

```sh
journalctl -b
```

Tail history:

```sh
journalctl -b -f
```

## Cleanup

```sh
sudo journalctl --vacuum-size=100M
```
