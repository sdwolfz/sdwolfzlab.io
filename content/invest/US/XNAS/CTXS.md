---
title: "CITRIX SYSTEMS INC (CTXS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CITRIX SYSTEMS INC</td></tr>
    <tr><td>Symbol</td><td>CTXS</td></tr>
    <tr><td>Web</td><td><a href="https://www.citrix.com">www.citrix.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.74 |
| 2020 | 1.4 |
| 2019 | 1.4 |
| 2018 | 0.35 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
