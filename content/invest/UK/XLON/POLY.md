---
title: "Polymetal International (POLY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Polymetal International</td></tr>
    <tr><td>Symbol</td><td>POLY</td></tr>
    <tr><td>Web</td><td><a href="https://www.polymetalinternational.com">www.polymetalinternational.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 129.0 |
| 2019 | 82.0 |
| 2018 | 48.0 |
| 2017 | 32.0 |
| 2016 | 42.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
