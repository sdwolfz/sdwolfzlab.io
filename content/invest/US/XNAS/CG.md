---
title: " (CG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CG</td></tr>
    <tr><td>Web</td><td><a href="https://www.carlyle.com">www.carlyle.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.5 |
| 2020 | 1.0 |
| 2019 | 1.36 |
| 2018 | 1.24 |
| 2017 | 1.24 |
| 2016 | 1.68 |
| 2015 | 3.39 |
| 2014 | 1.88 |
| 2013 | 1.33 |
| 2012 | 0.27 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
