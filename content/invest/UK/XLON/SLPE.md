---
title: "Std Life Priv. (SLPE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Std Life Priv.</td></tr>
    <tr><td>Symbol</td><td>SLPE</td></tr>
    <tr><td>Web</td><td><a href="https://www.slcapitalpartners.com">www.slcapitalpartners.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 13.2 |
| 2019 | 12.8 |
| 2018 | 12.4 |
| 2017 | 12.0 |
| 2016 | 5.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
