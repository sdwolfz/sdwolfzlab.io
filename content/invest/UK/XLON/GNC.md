---
title: "Greencore (GNC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Greencore</td></tr>
    <tr><td>Symbol</td><td>GNC</td></tr>
    <tr><td>Web</td><td><a href="https://www.greencore.com">www.greencore.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 6.2 |
| 2018 | 5.57 |
| 2017 | 5.47 |
| 2016 | 5.47 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
