---
title: "CERNER CORP (CERN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CERNER CORP</td></tr>
    <tr><td>Symbol</td><td>CERN</td></tr>
    <tr><td>Web</td><td><a href="https://www.cerner.com">www.cerner.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.22 |
| 2020 | 0.76 |
| 2019 | 0.54 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
