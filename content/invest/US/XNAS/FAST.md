---
title: "FASTENAL (FAST)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FASTENAL</td></tr>
    <tr><td>Symbol</td><td>FAST</td></tr>
    <tr><td>Web</td><td><a href="https://www.fastenal.com">www.fastenal.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.56 |
| 2020 | 1.4 |
| 2019 | 1.3 |
| 2018 | 1.54 |
| 2017 | 1.28 |
| 2016 | 1.2 |
| 2015 | 1.12 |
| 2014 | 1.0 |
| 2013 | 0.8 |
| 2012 | 1.24 |
| 2011 | 1.03 |
| 2010 | 1.24 |
| 2009 | 0.72 |
| 2008 | 0.79 |
| 2007 | 0.44 |
| 2006 | 0.4 |
| 2005 | 0.62 |
| 2004 | 0.4 |
| 2003 | 0.21 |
| 2002 | 0.1 |
| 2001 | 0.09 |
| 2000 | 0.08 |
| 1999 | 0.04 |
| 1998 | 0.02 |
| 1997 | 0.02 |
| 1996 | 0.02 |
| 1995 | 0.04 |
| 1994 | 0.04 |
| 1993 | 0.03 |
| 1992 | 0.06 |
| 1991 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
