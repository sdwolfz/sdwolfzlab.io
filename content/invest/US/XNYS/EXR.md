---
title: " (EXR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>EXR</td></tr>
    <tr><td>Web</td><td><a href="https://www.extraspace.com">www.extraspace.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.0 |
| 2020 | 3.6 |
| 2019 | 3.56 |
| 2018 | 3.36 |
| 2017 | 3.12 |
| 2016 | 2.93 |
| 2015 | 2.24 |
| 2014 | 1.81 |
| 2013 | 1.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
