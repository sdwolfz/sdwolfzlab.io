---
title: "FEDERAL SIGNAL CORP (FSS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FEDERAL SIGNAL CORP</td></tr>
    <tr><td>Symbol</td><td>FSS</td></tr>
    <tr><td>Web</td><td><a href="https://www.federalsignal.com">www.federalsignal.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.18 |
| 2020 | 0.32 |
| 2019 | 0.32 |
| 2018 | 0.31 |
| 2017 | 0.28 |
| 2016 | 0.28 |
| 2015 | 0.25 |
| 2014 | 0.09 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
