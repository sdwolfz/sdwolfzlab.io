---
title: "TRIBUNE PUBLISHING COMPANY NEW (TPCO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TRIBUNE PUBLISHING COMPANY NEW</td></tr>
    <tr><td>Symbol</td><td>TPCO</td></tr>
    <tr><td>Web</td><td><a href="https://www.tribpub.com">www.tribpub.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.25 |
| 2019 | 1.75 |
| 2016 | 0.175 |
| 2015 | 0.525 |
| 2014 | 0.175 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
