---
title: "CODORUS VY BANCORP INC (CVLY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CODORUS VY BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>CVLY</td></tr>
    <tr><td>Web</td><td><a href="https://www.peoplesbanknet.com">www.peoplesbanknet.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.26 |
| 2020 | 0.52 |
| 2019 | 0.48 |
| 2018 | 0.465 |
| 2017 | 0.405 |
| 2016 | 0.39 |
| 2015 | 0.38 |
| 2014 | 0.365 |
| 2013 | 0.34 |
| 2012 | 0.29 |
| 2011 | 0.35 |
| 2010 | 0.25 |
| 2009 | 0.26 |
| 2008 | 0.38 |
| 2007 | 0.46 |
| 2006 | 0.26 |
| 2005 | 0.38 |
| 2004 | 0.375 |
| 2003 | 0.36 |
| 2002 | 0.36 |
| 2001 | 0.36 |
| 2000 | 0.46 |
| 1999 | 0.325 |
| 1998 | 0.31 |
| 1997 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
