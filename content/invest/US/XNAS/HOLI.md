---
title: "HOLLYSYS AUTOMATION TECHNOLOGIES (HOLI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HOLLYSYS AUTOMATION TECHNOLOGIES</td></tr>
    <tr><td>Symbol</td><td>HOLI</td></tr>
    <tr><td>Web</td><td><a href="https://www.hollysys.com.sg">www.hollysys.com.sg</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.2 |
| 2019 | 0.21 |
| 2018 | 0.18 |
| 2017 | 0.12 |
| 2016 | 0.2 |
| 2015 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
