---
title: "PRODVINALCO S.A. (VAC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>PRODVINALCO S.A.</td></tr>
    <tr><td>Symbol</td><td>VAC</td></tr>
    <tr><td>Web</td><td><a href="https://www.prodvinalco.ro">www.prodvinalco.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.3176 |
| 2020 | 0.0636 |
| 2019 | 0.1588 |
| 2019 | 0.0953 |
| 2018 | 0.31751 |
| 2017 | 0.2699 |
| 2016 | 0.34609 |
| 2015 | 0.16596 |
| 2015 | 0.092078 |
| 2014 | 0.066677 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
