---
title: "ESPEY MAN & ELECTRONICS CORP (ESP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ESPEY MAN & ELECTRONICS CORP</td></tr>
    <tr><td>Symbol</td><td>ESP</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.0 |
| 2019 | 1.0 |
| 2018 | 2.0 |
| 2017 | 1.0 |
| 2016 | 1.0 |
| 2015 | 1.0 |
| 2014 | 1.0 |
| 2013 | 2.0 |
| 2012 | 1.925 |
| 2011 | 1.9 |
| 2010 | 1.9 |
| 2009 | 1.9 |
| 2008 | 3.85 |
| 2007 | 0.65 |
| 2006 | 0.44 |
| 2005 | 0.625 |
| 2004 | 1.05 |
| 2003 | 0.95 |
| 2002 | 1.2 |
| 2001 | 0.25 |
| 2000 | 0.2 |
| 1999 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
