---
title: "CARPATHIA CAPITAL ALTERNATYWNA SPOLKA INWESTYCYJNA SA (CRPC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>CARPATHIA CAPITAL ALTERNATYWNA SPOLKA INWESTYCYJNA SA</td></tr>
    <tr><td>Symbol</td><td>CRPC</td></tr>
    <tr><td>Web</td><td><a href="https://www.carpathiacapital.eu">www.carpathiacapital.eu</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.08 |
| 2018 | 0.08 |
| 2017 | 0.09 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
