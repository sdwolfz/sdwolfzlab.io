---
title: "US ECOLOGY INC (ECOL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>US ECOLOGY INC</td></tr>
    <tr><td>Symbol</td><td>ECOL</td></tr>
    <tr><td>Web</td><td><a href="https://www.usecology.com">www.usecology.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.18 |
| 2019 | 0.72 |
| 2018 | 0.72 |
| 2017 | 0.72 |
| 2016 | 0.72 |
| 2015 | 0.72 |
| 2014 | 0.72 |
| 2013 | 0.54 |
| 2012 | 0.9 |
| 2011 | 0.72 |
| 2010 | 0.72 |
| 2009 | 0.72 |
| 2008 | 0.66 |
| 2007 | 0.6 |
| 2006 | 0.45 |
| 2005 | 0.45 |
| 2004 | 0.25 |
| 1995 | 0.035 |
| 1994 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
