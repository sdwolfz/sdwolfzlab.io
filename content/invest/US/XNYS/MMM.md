---
title: "3M COMPANY (MMM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>3M COMPANY</td></tr>
    <tr><td>Symbol</td><td>MMM</td></tr>
    <tr><td>Web</td><td><a href="https://www.3m.com">www.3m.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.48 |
| 2020 | 5.88 |
| 2019 | 5.76 |
| 2018 | 5.44 |
| 2017 | 4.7 |
| 2016 | 4.44 |
| 2015 | 4.1 |
| 2014 | 3.42 |
| 2013 | 1.27 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
