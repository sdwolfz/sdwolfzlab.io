---
title: "NAVSIGHT HOLDINGS INC (NSH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NAVSIGHT HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>NSH</td></tr>
    <tr><td>Web</td><td><a href="https://www.navsight.com">www.navsight.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.875 |
| 2017 | 2.18 |
| 2016 | 2.18 |
| 2015 | 2.18 |
| 2014 | 2.18 |
| 2013 | 1.09 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
