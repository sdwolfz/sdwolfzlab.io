---
title: "CLARIVATE PLC (CCC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CLARIVATE PLC</td></tr>
    <tr><td>Symbol</td><td>CCC</td></tr>
    <tr><td>Web</td><td><a href="http://www.clarivate.com">www.clarivate.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.05 |
| 2017 | 0.15 |
| 2016 | 0.2 |
| 2015 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
