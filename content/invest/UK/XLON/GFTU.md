---
title: "Grafton Group (GFTU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Grafton Group</td></tr>
    <tr><td>Symbol</td><td>GFTU</td></tr>
    <tr><td>Web</td><td><a href="https://www.graftonplc.com">www.graftonplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 18.5 |
| 2018 | 18.0 |
| 2017 | 15.5 |
| 2016 | 13.75 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
