---
title: " (EQR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>EQR</td></tr>
    <tr><td>Web</td><td><a href="https://www.equityapartments.com">www.equityapartments.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.603 |
| 2020 | 2.412 |
| 2019 | 2.268 |
| 2018 | 2.16 |
| 2017 | 2.016 |
| 2016 | 5.016 |
| 2015 | 2.212 |
| 2014 | 2.0 |
| 2013 | 1.45 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
