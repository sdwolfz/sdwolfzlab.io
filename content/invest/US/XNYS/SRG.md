---
title: " (SRG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SRG</td></tr>
    <tr><td>Web</td><td><a href="https://www.seritage.com">www.seritage.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.25 |
| 2018 | 1.0 |
| 2017 | 1.0 |
| 2016 | 1.0 |
| 2015 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
