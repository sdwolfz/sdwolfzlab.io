---
title: "MANULIFE FINANCIAL CORP (MFC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MANULIFE FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>MFC</td></tr>
    <tr><td>Web</td><td><a href="https://www.manulife.com">www.manulife.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.832 |
| 2019 | 0.756 |
| 2018 | 0.705 |
| 2017 | 0.63 |
| 2016 | 0.527 |
| 2015 | 0.795 |
| 2014 | 0.57 |
| 2013 | 0.26 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
