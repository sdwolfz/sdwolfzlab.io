---
title: "Severfield (SFR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Severfield</td></tr>
    <tr><td>Symbol</td><td>SFR</td></tr>
    <tr><td>Web</td><td><a href="https://www.severfield.com">www.severfield.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.1 |
| 2019 | 2.5 |
| 2018 | 2.6 |
| 2017 | 2.3 |
| 2016 | 1.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
