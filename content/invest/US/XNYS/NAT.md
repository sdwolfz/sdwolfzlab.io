---
title: "NORDIC AMERICAN TANKERS LIMITED (NAT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NORDIC AMERICAN TANKERS LIMITED</td></tr>
    <tr><td>Symbol</td><td>NAT</td></tr>
    <tr><td>Web</td><td><a href="https://www.nat.bm">www.nat.bm</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.02 |
| 2020 | 0.45 |
| 2019 | 0.1 |
| 2018 | 0.07 |
| 2017 | 0.579 |
| 2016 | 1.37 |
| 2015 | 1.38 |
| 2014 | 0.61 |
| 2013 | 0.48 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
