---
title: "UNICO AMERICAN (UNAM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>UNICO AMERICAN</td></tr>
    <tr><td>Symbol</td><td>UNAM</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2012 | 1.2 |
| 2010 | 0.36 |
| 2009 | 0.36 |
| 2002 | 0.05 |
| 2001 | 0.05 |
| 2000 | 0.15 |
| 1999 | 0.25 |
| 1998 | 0.07 |
| 1997 | 0.07 |
| 1996 | 0.07 |
| 1995 | 0.07 |
| 1994 | 0.07 |
| 1993 | 0.07 |
| 1992 | 0.06 |
| 1991 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
