---
title: "How to configure Git properly"
date: 2020-07-01T19:16:38Z
type: posts
slug: how-to-configure-git-properly
tags: ["git", "productivity", "tutorial"]
---

## Required

You need a name and email otherwise commits won't work:

```sh
git config --global user.name 'Your Name'
git config --global user.email 'your.email@faang.com'
```

## Global .gitignore

Don't put junk like these in your poject's `.gitignore`:

```
.DS_Store
.idea/
*.swp
```

Use your own config:

```sh
touch ~/.gitignore
git config --global core.excludesfile '~/.gitignore'
```

## Symlink .dockerignore

Add the git directory to your `.gitignore` file:

```
# Required since .dockerignore is a symlink to this file.
/.git
```

Now you can execute the symlink command:

```sh
ln -s .gitignore .dockerignore
```

## Line endings

Some people use wrong line endings, set this to use the proper ones:

```sh
git config --global core.autocrlf input
```

## Diff

Show whitespace errors in terminal diffs:

```sh
git config --global diff.wsErrorHighlight all
```

Better diff output:

```sh
git config --global merge.conflictStyle zdiff3
```
