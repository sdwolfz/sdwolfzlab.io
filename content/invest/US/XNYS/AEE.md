---
title: "AMEREN CORP (AEE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AMEREN CORP</td></tr>
    <tr><td>Symbol</td><td>AEE</td></tr>
    <tr><td>Web</td><td><a href="https://www.ameren.com">www.ameren.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.55 |
| 2020 | 2.0 |
| 2019 | 1.92 |
| 2018 | 1.849 |
| 2017 | 1.778 |
| 2016 | 2.14 |
| 2015 | 1.655 |
| 2014 | 1.61 |
| 2013 | 0.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
