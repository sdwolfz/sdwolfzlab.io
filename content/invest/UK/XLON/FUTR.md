---
title: "Future (FUTR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Future</td></tr>
    <tr><td>Symbol</td><td>FUTR</td></tr>
    <tr><td>Web</td><td><a href="https://www.futureplc.com">www.futureplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.6 |
| 2018 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
