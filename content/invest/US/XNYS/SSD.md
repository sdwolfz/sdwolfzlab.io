---
title: "SIMPSON MFG CO INC (SSD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SIMPSON MFG CO INC</td></tr>
    <tr><td>Symbol</td><td>SSD</td></tr>
    <tr><td>Web</td><td><a href="https://www.simpsonmfg.com">www.simpsonmfg.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.46 |
| 2020 | 0.69 |
| 2019 | 1.13 |
| 2018 | 0.86 |
| 2017 | 0.78 |
| 2016 | 0.68 |
| 2015 | 0.6 |
| 2014 | 0.405 |
| 2013 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
