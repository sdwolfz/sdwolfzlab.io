---
title: "Concatenate shell commands"
date: 2022-10-13T08:16:04Z
type: posts
slug: concatenate-shell-commands
tags: ["shell", "bash", "productivity"]
---

## Steps

Fetch a command with either:
* `C-e C-u`: go to end of line, cut until beginning of line.
* `C-a C-k`: go to start of line, cut until end of line.

Navigate to older command with:
* `C-p`: previous command.
* `C-r`: search command (use [fzf](/snippets/how-to-use-fzf/) for better
  results).

Navigate to the end with `C-e` and type `&&`.

Now you can paste with `C-y`.

## Result

This will turn...

```sh
git add .
git commit --amend --no-edit
git push origin changes
```

...into:

```sh
git add . && git commit --amend --no-edit && git push origin changes
```

And you will also have it in you history movingforward so you can:
* `C-r` to search for it easily.
* `C-e C-w` to change the branch name.
