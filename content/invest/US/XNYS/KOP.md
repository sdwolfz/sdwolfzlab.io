---
title: "KOPPERS HOLDINGS INC (KOP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>KOPPERS HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>KOP</td></tr>
    <tr><td>Web</td><td><a href="https://www.koppers.com">www.koppers.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 1.0 |
| 2013 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
