---
title: "DAVITA INC (DVA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DAVITA INC</td></tr>
    <tr><td>Symbol</td><td>DVA</td></tr>
    <tr><td>Web</td><td><a href="https://www.davita.com">www.davita.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
