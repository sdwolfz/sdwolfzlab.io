---
title: " (TCOM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>TCOM</td></tr>
    <tr><td>Web</td><td><a href="https://www.ctrip.com">www.ctrip.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2008 | 0.107 |
| 2007 | 0.257 |
| 2006 | 0.235 |
| 2005 | 0.28 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
