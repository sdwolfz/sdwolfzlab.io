---
title: "RIT Capital Partners (RCP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>RIT Capital Partners</td></tr>
    <tr><td>Symbol</td><td>RCP</td></tr>
    <tr><td>Web</td><td><a href="https://www.ritcap.co.uk">www.ritcap.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 35.0 |
| 2019 | 34.0 |
| 2018 | 33.0 |
| 2017 | 32.0 |
| 2016 | 31.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
