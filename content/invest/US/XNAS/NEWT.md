---
title: "NEWTEK BUSINESS SERVICES CORP (NEWT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NEWTEK BUSINESS SERVICES CORP</td></tr>
    <tr><td>Symbol</td><td>NEWT</td></tr>
    <tr><td>Web</td><td><a href="https://www.newtekone.com">www.newtekone.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.5 |
| 2020 | 2.05 |
| 2019 | 2.15 |
| 2018 | 1.8 |
| 2017 | 1.64 |
| 2016 | 1.93 |
| 2015 | 4.05 |
| 2009 | 0.02 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
