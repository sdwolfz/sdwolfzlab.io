---
title: "PROSPERITY BANCSHARES INC (PB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PROSPERITY BANCSHARES INC</td></tr>
    <tr><td>Symbol</td><td>PB</td></tr>
    <tr><td>Web</td><td><a href="https://www.prosperitybankusa.com">www.prosperitybankusa.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.98 |
| 2020 | 1.87 |
| 2019 | 1.69 |
| 2018 | 1.49 |
| 2017 | 1.38 |
| 2016 | 1.24 |
| 2015 | 1.119 |
| 2014 | 0.993 |
| 2013 | 0.67 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
