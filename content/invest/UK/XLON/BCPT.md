---
title: "Bmo Comm Prop. (BCPT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Bmo Comm Prop.</td></tr>
    <tr><td>Symbol</td><td>BCPT</td></tr>
    <tr><td>Web</td><td><a href="https://www.bmogam.com">www.bmogam.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 6.0 |
| 2018 | 6.0 |
| 2017 | 6.0 |
| 2016 | 6.0 |
| 2015 | 6.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
