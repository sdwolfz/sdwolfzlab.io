---
title: "Ferguson (FERG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Ferguson</td></tr>
    <tr><td>Symbol</td><td>FERG</td></tr>
    <tr><td>Web</td><td><a href="https://www.fergusonplc.com">www.fergusonplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 208.2 |
| 2019 | 208.2 |
| 2018 | 189.3 |
| 2017 | 116.11 |
| 2016 | 105.56 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
