---
title: " (ATHM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ATHM</td></tr>
    <tr><td>Web</td><td><a href="https://www.autohome.com.cn">www.autohome.com.cn</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.85 |
| 2020 | 0.77 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
