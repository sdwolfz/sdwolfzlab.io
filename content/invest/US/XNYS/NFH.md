---
title: "NEW FRONTIER HEALTH CORPORATION (NFH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NEW FRONTIER HEALTH CORPORATION</td></tr>
    <tr><td>Symbol</td><td>NFH</td></tr>
    <tr><td>Web</td><td><a href="https://www.nfh.com.cn">www.nfh.com.cn</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
