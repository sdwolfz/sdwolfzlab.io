---
title: "RBC BEARINGS INC. (ROLL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>RBC BEARINGS INC.</td></tr>
    <tr><td>Symbol</td><td>ROLL</td></tr>
    <tr><td>Web</td><td><a href="https://www.rbcbearings.com">www.rbcbearings.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 2.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
