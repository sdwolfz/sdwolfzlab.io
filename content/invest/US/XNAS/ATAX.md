---
title: " (ATAX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ATAX</td></tr>
    <tr><td>Web</td><td><a href="https://www.ataxfund.com">www.ataxfund.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.09 |
| 2020 | 0.305 |
| 2019 | 0.5 |
| 2018 | 0.5 |
| 2017 | 0.5 |
| 2016 | 0.5 |
| 2015 | 0.5 |
| 2014 | 0.5 |
| 2013 | 0.5 |
| 2012 | 0.5 |
| 2011 | 0.5 |
| 2010 | 0.5 |
| 2009 | 0.545 |
| 2008 | 0.54 |
| 2007 | 0.675 |
| 2006 | 0.405 |
| 2005 | 0.54 |
| 2004 | 0.54 |
| 2003 | 0.54 |
| 2002 | 0.54 |
| 2001 | 0.54 |
| 2000 | 0.54 |
| 1999 | 0.495 |
| 1998 | 0.54 |
| 1997 | 0.54 |
| 1996 | 0.54 |
| 1995 | 0.585 |
| 1994 | 0.495 |
| 1993 | 0.813 |
| 1992 | 1.008 |
| 1991 | 1.008 |
| 1990 | 1.008 |
| 1989 | 1.642 |
| 1988 | 0.425 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
