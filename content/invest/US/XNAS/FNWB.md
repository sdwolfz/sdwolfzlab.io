---
title: "FIRST NORTHWEST BANCORP (FNWB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST NORTHWEST BANCORP</td></tr>
    <tr><td>Symbol</td><td>FNWB</td></tr>
    <tr><td>Web</td><td><a href="https://www.ourfirstfed.com">www.ourfirstfed.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 0.21 |
| 2019 | 0.13 |
| 2018 | 0.03 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
