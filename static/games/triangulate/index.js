// Triangulate

// When page is ready to execute our code.
document.addEventListener('DOMContentLoaded', (_event) => {
  // Root is the dom container for our game.
  const root = document.getElementById('game')

  // Remove everything inside our container before starting.
  root.innerHTML = ''

  // Game parameters.
  const params = {
    width:  500,
    height: 500,
    offset: 5,

    log: 'debug'
  }

  // Initialize game.
  const game = new window.Game(document, root, params)

  // Restart button.
  const restartButton = document.getElementById('restart')
  game.bindRestart(restartButton)

  // Trail.
  const trail = document.getElementById('trail')
  game.bindTrail(trail)

  // Cheat.
  const cheat = document.getElementById('cheat')
  game.bindCheat(cheat)

  // Run the game.
  game.mount().start()
})
