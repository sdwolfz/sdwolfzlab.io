---
title: "ASSURANT INC 6.50% MANDATORY CONV PREF STOCK SERIES D (AIZP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ASSURANT INC 6.50% MANDATORY CONV PREF STOCK SERIES D</td></tr>
    <tr><td>Symbol</td><td>AIZP</td></tr>
    <tr><td>Web</td><td><a href="http://www.assurant.com">www.assurant.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
