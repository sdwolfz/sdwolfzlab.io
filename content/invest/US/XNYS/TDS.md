---
title: "TELEPHONE & DATA SYSTEMS INC (TDS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TELEPHONE & DATA SYSTEMS INC</td></tr>
    <tr><td>Symbol</td><td>TDS</td></tr>
    <tr><td>Web</td><td><a href="https://www.tdsinc.com">www.tdsinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.175 |
| 2020 | 0.68 |
| 2019 | 0.66 |
| 2018 | 0.64 |
| 2017 | 0.62 |
| 2016 | 0.592 |
| 2015 | 0.564 |
| 2014 | 0.536 |
| 2013 | 0.381 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
