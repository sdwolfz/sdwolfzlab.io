---
title: "Scottish Inv (SCIN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Scottish Inv</td></tr>
    <tr><td>Symbol</td><td>SCIN</td></tr>
    <tr><td>Web</td><td><a href="https://www.thescottish.co.uk">www.thescottish.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 23.2 |
| 2019 | 30.25 |
| 2018 | 25.2 |
| 2017 | 25.0 |
| 2016 | 27.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
