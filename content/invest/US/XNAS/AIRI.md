---
title: "AIR INDUSTRIES GROUP (AIRI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AIR INDUSTRIES GROUP</td></tr>
    <tr><td>Symbol</td><td>AIRI</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 0.45 |
| 2014 | 0.725 |
| 2013 | 0.25 |
| 2012 | 0.063 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
