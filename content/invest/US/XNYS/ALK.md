---
title: "ALASKA AIR GROUP INC (ALK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ALASKA AIR GROUP INC</td></tr>
    <tr><td>Symbol</td><td>ALK</td></tr>
    <tr><td>Web</td><td><a href="https://www.alaskaair.com">www.alaskaair.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.375 |
| 2019 | 1.4 |
| 2018 | 1.28 |
| 2017 | 1.2 |
| 2016 | 1.1 |
| 2015 | 0.8 |
| 2014 | 0.875 |
| 2013 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
