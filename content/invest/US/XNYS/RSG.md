---
title: "REPUBLIC SERVICES INC (RSG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>REPUBLIC SERVICES INC</td></tr>
    <tr><td>Symbol</td><td>RSG</td></tr>
    <tr><td>Web</td><td><a href="https://www.republicservices.com">www.republicservices.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.85 |
| 2020 | 2.065 |
| 2019 | 1.56 |
| 2018 | 1.44 |
| 2017 | 1.33 |
| 2016 | 1.24 |
| 2015 | 1.44 |
| 2014 | 1.34 |
| 2013 | 0.52 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
