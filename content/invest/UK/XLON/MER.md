---
title: "Mears (MER)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Mears</td></tr>
    <tr><td>Symbol</td><td>MER</td></tr>
    <tr><td>Web</td><td><a href="https://www.mearsgroup.co.uk">www.mearsgroup.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 3.65 |
| 2018 | 12.4 |
| 2017 | 12.0 |
| 2016 | 11.7 |
| 2015 | 11.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
