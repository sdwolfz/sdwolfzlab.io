---
title: "Intermediate Capital (ICP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Intermediate Capital</td></tr>
    <tr><td>Symbol</td><td>ICP</td></tr>
    <tr><td>Web</td><td><a href="https://www.icgam.com">www.icgam.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 50.8 |
| 2019 | 45.0 |
| 2018 | 30.0 |
| 2017 | 27.0 |
| 2016 | 23.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
