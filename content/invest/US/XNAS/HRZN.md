---
title: "HORIZON TECHNOLOGY FINANCE CORP (HRZN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HORIZON TECHNOLOGY FINANCE CORP</td></tr>
    <tr><td>Symbol</td><td>HRZN</td></tr>
    <tr><td>Web</td><td><a href="https://www.horizontechfinance.com">www.horizontechfinance.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.8 |
| 2020 | 1.25 |
| 2019 | 1.2 |
| 2018 | 1.2 |
| 2017 | 1.2 |
| 2016 | 1.365 |
| 2015 | 1.38 |
| 2014 | 1.38 |
| 2013 | 1.38 |
| 2012 | 1.915 |
| 2011 | 1.18 |
| 2010 | 0.22 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
