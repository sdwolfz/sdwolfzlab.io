---
title: " (VER)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>VER</td></tr>
    <tr><td>Web</td><td><a href="https://www.vereit.com">www.vereit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.924 |
| 2020 | 0.677 |
| 2019 | 0.552 |
| 2018 | 0.552 |
| 2017 | 0.552 |
| 2016 | 0.552 |
| 2015 | 0.276 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
