---
title: "SARATOGA INVESTMENT CORP (SAR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SARATOGA INVESTMENT CORP</td></tr>
    <tr><td>Symbol</td><td>SAR</td></tr>
    <tr><td>Web</td><td><a href="https://www.saratogainvestmentcorp.com">www.saratogainvestmentcorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.85 |
| 2020 | 1.37 |
| 2019 | 1.65 |
| 2018 | 2.06 |
| 2017 | 2.35 |
| 2016 | 1.68 |
| 2015 | 1.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
