---
title: "DESWELL INDS INC (DSWL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>DESWELL INDS INC</td></tr>
    <tr><td>Symbol</td><td>DSWL</td></tr>
    <tr><td>Web</td><td><a href="https://www.deswell.com">www.deswell.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.18 |
| 2019 | 0.15 |
| 2018 | 0.1 |
| 2017 | 0.07 |
| 2016 | 0.14 |
| 2015 | 0.19 |
| 2014 | 0.15 |
| 2013 | 0.22 |
| 2012 | 0.27 |
| 2011 | 0.15 |
| 2010 | 0.05 |
| 2009 | 0.14 |
| 2008 | 0.37 |
| 2007 | 0.61 |
| 2006 | 0.62 |
| 2005 | 0.75 |
| 2004 | 0.96 |
| 2003 | 0.9 |
| 2002 | 0.9 |
| 2001 | 1.05 |
| 2000 | 0.88 |
| 1999 | 0.88 |
| 1998 | 0.83 |
| 1997 | 0.7 |
| 1996 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
