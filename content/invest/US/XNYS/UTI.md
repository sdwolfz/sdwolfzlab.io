---
title: "UNIVERSAL TECHNICAL INSTITUTE (UTI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>UNIVERSAL TECHNICAL INSTITUTE</td></tr>
    <tr><td>Symbol</td><td>UTI</td></tr>
    <tr><td>Web</td><td><a href="https://www.uti.edu">www.uti.edu</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.02 |
| 2015 | 0.24 |
| 2014 | 0.4 |
| 2013 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
