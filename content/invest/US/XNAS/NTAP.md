---
title: "NETAPP INC (NTAP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NETAPP INC</td></tr>
    <tr><td>Symbol</td><td>NTAP</td></tr>
    <tr><td>Web</td><td><a href="https://www.netapp.com">www.netapp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.96 |
| 2020 | 1.92 |
| 2019 | 1.76 |
| 2018 | 1.2 |
| 2017 | 0.78 |
| 2016 | 0.74 |
| 2015 | 0.69 |
| 2014 | 0.63 |
| 2013 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
