---
title: "EPLUS INC (PLUS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>EPLUS INC</td></tr>
    <tr><td>Symbol</td><td>PLUS</td></tr>
    <tr><td>Web</td><td><a href="https://www.eplus.com">www.eplus.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2012 | 2.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
