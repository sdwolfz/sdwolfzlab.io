---
title: "LAKELAND FINANCIAL CORP (LKFN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LAKELAND FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>LKFN</td></tr>
    <tr><td>Web</td><td><a href="https://www.lakecitybank.com">www.lakecitybank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.68 |
| 2020 | 1.2 |
| 2019 | 1.16 |
| 2018 | 1.0 |
| 2017 | 0.85 |
| 2016 | 0.995 |
| 2015 | 0.945 |
| 2014 | 0.82 |
| 2013 | 0.57 |
| 2012 | 0.835 |
| 2011 | 0.62 |
| 2010 | 0.62 |
| 2009 | 0.62 |
| 2008 | 0.605 |
| 2007 | 0.545 |
| 2006 | 0.73 |
| 2005 | 0.9 |
| 2004 | 0.82 |
| 2003 | 0.74 |
| 2002 | 0.66 |
| 2001 | 0.58 |
| 2000 | 0.5 |
| 1999 | 0.42 |
| 1998 | 0.33 |
| 1997 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
