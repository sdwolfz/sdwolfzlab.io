---
title: "BROOKFIELD RENEWABLE CORP (BEPC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BROOKFIELD RENEWABLE CORP</td></tr>
    <tr><td>Symbol</td><td>BEPC</td></tr>
    <tr><td>Web</td><td><a href="https://www.brookfield.com">www.brookfield.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.304 |
| 2020 | 0.723 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
