---
title: "MARRIOTT INTERNATIONAL INC (MAR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MARRIOTT INTERNATIONAL INC</td></tr>
    <tr><td>Symbol</td><td>MAR</td></tr>
    <tr><td>Web</td><td><a href="https://www.marriott.com">www.marriott.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.48 |
| 2019 | 1.85 |
| 2018 | 1.56 |
| 2017 | 1.29 |
| 2016 | 1.15 |
| 2015 | 0.95 |
| 2014 | 0.77 |
| 2013 | 0.64 |
| 2012 | 0.49 |
| 2011 | 0.388 |
| 2010 | 0.208 |
| 2009 | 0.088 |
| 2008 | 0.337 |
| 2007 | 0.288 |
| 2006 | 0.293 |
| 2005 | 0.4 |
| 2004 | 0.33 |
| 2003 | 0.295 |
| 2002 | 0.275 |
| 2001 | 0.255 |
| 2000 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
