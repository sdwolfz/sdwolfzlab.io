---
title: "REMAT MARAMURES SA BAIA MARE (REMM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>REMAT MARAMURES SA BAIA MARE</td></tr>
    <tr><td>Symbol</td><td>REMM</td></tr>
    <tr><td>Web</td><td><a href="https://www.remat-mm.ro">www.remat-mm.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 9.118382 |
| 2010 | 2.7 |
| 2009 | 4.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
