---
title: " (AVAL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>AVAL</td></tr>
    <tr><td>Web</td><td><a href="https://www.grupoaval.com">www.grupoaval.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.095 |
| 2020 | 0.288 |
| 2019 | 0.315 |
| 2018 | 0.314 |
| 2017 | 0.402 |
| 2016 | 0.388 |
| 2015 | 0.431 |
| 2014 | 0.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
