---
title: "Real Est.cred (RECI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Real Est.cred</td></tr>
    <tr><td>Symbol</td><td>RECI</td></tr>
    <tr><td>Web</td><td><a href="https://www.recreditinvest.com">www.recreditinvest.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 12.0 |
| 2019 | 12.0 |
| 2018 | 12.0 |
| 2017 | 11.1 |
| 2016 | 11.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
