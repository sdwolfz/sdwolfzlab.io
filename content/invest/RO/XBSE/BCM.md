---
title: "CASA DE BUCOVINA-CLUB DE MUNTE (BCM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>CASA DE BUCOVINA-CLUB DE MUNTE</td></tr>
    <tr><td>Symbol</td><td>BCM</td></tr>
    <tr><td>Web</td><td><a href="https://www.bestwesternbucovina.ro">www.bestwesternbucovina.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.014 |
| 2017 | 0.004 |
| 2016 | 0.0034 |
| 2015 | 0.003 |
| 2014 | 0.0033 |
| 2013 | 0.008 |
| 2011 | 0.009 |
| 2009 | 0.007 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
