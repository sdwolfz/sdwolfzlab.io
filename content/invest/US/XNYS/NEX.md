---
title: "NEXTIER OILFIELD SOLUTIONS INC (NEX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NEXTIER OILFIELD SOLUTIONS INC</td></tr>
    <tr><td>Symbol</td><td>NEX</td></tr>
    <tr><td>Web</td><td><a href="https://www.nextierofs.com">www.nextierofs.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
