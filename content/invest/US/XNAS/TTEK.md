---
title: "TETRA TECH INC (TTEK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TETRA TECH INC</td></tr>
    <tr><td>Symbol</td><td>TTEK</td></tr>
    <tr><td>Web</td><td><a href="https://www.tetratech.com">www.tetratech.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.37 |
| 2020 | 0.66 |
| 2019 | 0.57 |
| 2018 | 0.46 |
| 2017 | 0.39 |
| 2016 | 0.35 |
| 2015 | 0.31 |
| 2014 | 0.21 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
