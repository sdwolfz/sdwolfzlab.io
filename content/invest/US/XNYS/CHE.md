---
title: "CHEMED CORP (CHE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CHEMED CORP</td></tr>
    <tr><td>Symbol</td><td>CHE</td></tr>
    <tr><td>Web</td><td><a href="https://www.chemed.com">www.chemed.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.34 |
| 2020 | 1.32 |
| 2019 | 1.24 |
| 2018 | 1.16 |
| 2017 | 1.08 |
| 2016 | 1.0 |
| 2015 | 0.92 |
| 2014 | 0.84 |
| 2013 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
