---
title: "QUALIGEN THERAPEUTICS INC (QLGN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>QUALIGEN THERAPEUTICS INC</td></tr>
    <tr><td>Symbol</td><td>QLGN</td></tr>
    <tr><td>Web</td><td><a href="https://www.qualigeninc.com">www.qualigeninc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
