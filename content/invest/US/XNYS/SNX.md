---
title: "SYNNEX CORP (SNX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SYNNEX CORP</td></tr>
    <tr><td>Symbol</td><td>SNX</td></tr>
    <tr><td>Web</td><td><a href="https://www.synnex.com">www.synnex.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.4 |
| 2020 | 80.4 |
| 2019 | 1.5 |
| 2018 | 1.4 |
| 2017 | 1.05 |
| 2016 | 1.05 |
| 2015 | 0.575 |
| 2014 | 0.125 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
