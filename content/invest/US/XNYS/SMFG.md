---
title: " (SMFG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SMFG</td></tr>
    <tr><td>Web</td><td><a href="https://www.smfg.co.jp">www.smfg.co.jp</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.294 |
| 2019 | 0.272 |
| 2018 | 0.249 |
| 2017 | 0.237 |
| 2016 | 0.259 |
| 2015 | 0.216 |
| 2014 | 0.196 |
| 2013 | 0.33 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
