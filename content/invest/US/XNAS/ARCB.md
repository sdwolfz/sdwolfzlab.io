---
title: "ARCBEST CORP (ARCB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ARCBEST CORP</td></tr>
    <tr><td>Symbol</td><td>ARCB</td></tr>
    <tr><td>Web</td><td><a href="https://www.arcb.com">www.arcb.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.16 |
| 2020 | 0.32 |
| 2019 | 0.32 |
| 2018 | 0.32 |
| 2017 | 0.32 |
| 2016 | 0.32 |
| 2015 | 0.26 |
| 2014 | 0.15 |
| 2013 | 0.12 |
| 2012 | 0.12 |
| 2011 | 0.12 |
| 2010 | 0.12 |
| 2009 | 0.6 |
| 2008 | 0.6 |
| 2007 | 0.6 |
| 2006 | 0.6 |
| 2005 | 0.54 |
| 2004 | 0.48 |
| 2003 | 0.32 |
| 1999 | 0.719 |
| 1996 | 0.01 |
| 1995 | 0.04 |
| 1994 | 0.04 |
| 1993 | 0.04 |
| 1992 | 0.02 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
