---
title: "FORAJ SONDE SA CRAIOVA (FOSB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>FORAJ SONDE SA CRAIOVA</td></tr>
    <tr><td>Symbol</td><td>FOSB</td></tr>
    <tr><td>Web</td><td><a href="https://www.craiovadrilling.ro">www.craiovadrilling.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.0085 |
| 2019 | 0.00699 |
| 2018 | 0.0092 |
| 2017 | 0.0062 |
| 2016 | 0.0062 |
| 2015 | 0.0203 |
| 2014 | 0.0141 |
| 2013 | 0.0278 |
| 2012 | 0.0084 |
| 2009 | 0.0046 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
