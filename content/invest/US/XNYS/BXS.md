---
title: "BANCORPSOUTH BANK (BXS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BANCORPSOUTH BANK</td></tr>
    <tr><td>Symbol</td><td>BXS</td></tr>
    <tr><td>Web</td><td><a href="https://www.bancorpsouth.com">www.bancorpsouth.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.38 |
| 2020 | 0.745 |
| 2019 | 0.71 |
| 2018 | 0.62 |
| 2017 | 0.53 |
| 2016 | 0.45 |
| 2015 | 0.35 |
| 2014 | 0.25 |
| 2013 | 0.11 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
