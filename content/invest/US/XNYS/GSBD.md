---
title: "GOLDMAN SACHS BDC INC (GSBD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GOLDMAN SACHS BDC INC</td></tr>
    <tr><td>Symbol</td><td>GSBD</td></tr>
    <tr><td>Web</td><td><a href="https://www.goldmansachsbdc.com">www.goldmansachsbdc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.55 |
| 2020 | 1.8 |
| 2019 | 1.8 |
| 2018 | 1.8 |
| 2017 | 1.8 |
| 2016 | 1.8 |
| 2015 | 1.35 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
