---
title: "LINDE PLC (LIN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>LINDE PLC</td></tr>
    <tr><td>Symbol</td><td>LIN</td></tr>
    <tr><td>Web</td><td><a href="https://www.linde.com">www.linde.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.12 |
| 2020 | 2.889 |
| 2018 | 0.825 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
