---
title: "BONSO ELECTRONICS INTERNATIONAL (BNSO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BONSO ELECTRONICS INTERNATIONAL</td></tr>
    <tr><td>Symbol</td><td>BNSO</td></tr>
    <tr><td>Web</td><td><a href="https://www.bonso.com">www.bonso.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2006 | 0.05 |
| 2005 | 0.1 |
| 2004 | 0.1 |
| 2003 | 0.05 |
| 2001 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
