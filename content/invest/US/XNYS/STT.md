---
title: "STATE STREET CORPORATION (STT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>STATE STREET CORPORATION</td></tr>
    <tr><td>Symbol</td><td>STT</td></tr>
    <tr><td>Web</td><td><a href="https://www.statestreet.com">www.statestreet.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.52 |
| 2020 | 2.08 |
| 2019 | 1.98 |
| 2018 | 1.78 |
| 2017 | 1.6 |
| 2016 | 1.44 |
| 2015 | 1.32 |
| 2014 | 1.16 |
| 2013 | 0.52 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
