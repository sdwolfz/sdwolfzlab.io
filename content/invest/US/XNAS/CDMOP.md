---
title: "AVID BIOSERVICES INC 10.50% CONV PFD SER'E' (CDMOP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AVID BIOSERVICES INC 10.50% CONV PFD SER'E'</td></tr>
    <tr><td>Symbol</td><td>CDMOP</td></tr>
    <tr><td>Web</td><td><a href="http://www.avidbio.com">www.avidbio.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.625 |
| 2019 | 2.625 |
| 2018 | 2.625 |
| 2017 | 2.625 |
| 2016 | 2.625 |
| 2015 | 2.625 |
| 2014 | 2.268 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
