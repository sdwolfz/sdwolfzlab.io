---
title: "Galliford Try (GFRD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Galliford Try</td></tr>
    <tr><td>Symbol</td><td>GFRD</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 58.0 |
| 2018 | 77.0 |
| 2017 | 86.37 |
| 2016 | 73.78 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
