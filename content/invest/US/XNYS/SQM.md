---
title: "SOCIEDAD QUIMICA Y MINERA DE CHILE ADR-EACH REPR 1 PRF SER'B' (SQM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SOCIEDAD QUIMICA Y MINERA DE CHILE ADR-EACH REPR 1 PRF SER'B'</td></tr>
    <tr><td>Symbol</td><td>SQM</td></tr>
    <tr><td>Web</td><td><a href="http://www.sqm.com">www.sqm.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.523 |
| 2019 | 0.799 |
| 2018 | 1.424 |
| 2017 | 1.168 |
| 2016 | 1.106 |
| 2015 | 0.38 |
| 2014 | 1.124 |
| 2013 | 0.83 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
