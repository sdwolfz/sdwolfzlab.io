---
title: " (BFRA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BFRA</td></tr>
    <tr><td>Web</td><td><a href="https://www.biofrontera.com">www.biofrontera.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.052 |
| 2020 | 0.049 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
