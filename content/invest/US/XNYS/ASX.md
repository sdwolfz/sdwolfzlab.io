---
title: " (ASX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ASX</td></tr>
    <tr><td>Web</td><td><a href="https://www.aseglobal.com">www.aseglobal.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.088 |
| 2019 | 0.106 |
| 2017 | 0.13 |
| 2016 | 0.183 |
| 2015 | 0.227 |
| 2014 | 0.151 |
| 2013 | 0.121 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
