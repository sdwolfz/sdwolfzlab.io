---
title: " (IEP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>IEP</td></tr>
    <tr><td>Web</td><td><a href="https://www.ielp.com">www.ielp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.0 |
| 2020 | 8.0 |
| 2019 | 8.0 |
| 2018 | 7.0 |
| 2017 | 6.0 |
| 2016 | 6.0 |
| 2015 | 6.0 |
| 2014 | 6.0 |
| 2013 | 4.5 |
| 2011 | 0.95 |
| 2010 | 1.0 |
| 2009 | 1.0 |
| 2008 | 1.0 |
| 2007 | 0.55 |
| 2006 | 0.4 |
| 2005 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
