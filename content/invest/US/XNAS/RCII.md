---
title: "RENT A CENTER INC (RCII)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>RENT A CENTER INC</td></tr>
    <tr><td>Symbol</td><td>RCII</td></tr>
    <tr><td>Web</td><td><a href="https://www.rentacenter.com">www.rentacenter.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.31 |
| 2020 | 1.47 |
| 2019 | 0.25 |
| 2017 | 0.24 |
| 2016 | 0.24 |
| 2015 | 0.96 |
| 2014 | 0.93 |
| 2013 | 0.86 |
| 2012 | 0.69 |
| 2011 | 0.6 |
| 2010 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
