---
title: " (HIMX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>HIMX</td></tr>
    <tr><td>Web</td><td><a href="https://www.himax.com.tw">www.himax.com.tw</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.1 |
| 2017 | 0.24 |
| 2016 | 0.13 |
| 2015 | 0.3 |
| 2014 | 0.27 |
| 2013 | 0.25 |
| 2012 | 0.063 |
| 2011 | 0.12 |
| 2010 | 0.24 |
| 2009 | 0.28 |
| 2008 | 0.33 |
| 2007 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
