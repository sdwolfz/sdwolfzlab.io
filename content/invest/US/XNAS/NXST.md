---
title: "NEXSTAR MEDIA GROUP INC (NXST)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NEXSTAR MEDIA GROUP INC</td></tr>
    <tr><td>Symbol</td><td>NXST</td></tr>
    <tr><td>Web</td><td><a href="https://www.nexstar.tv">www.nexstar.tv</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.4 |
| 2020 | 2.24 |
| 2019 | 1.8 |
| 2018 | 1.5 |
| 2017 | 1.2 |
| 2016 | 0.96 |
| 2015 | 0.76 |
| 2014 | 0.6 |
| 2013 | 0.48 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
