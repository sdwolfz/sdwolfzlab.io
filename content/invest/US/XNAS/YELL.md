---
title: "YELLOW CORPORATION (YELL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>YELLOW CORPORATION</td></tr>
    <tr><td>Symbol</td><td>YELL</td></tr>
    <tr><td>Web</td><td><a href="https://www.myyellow.com">www.myyellow.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2002 | 3.975 |
| 1995 | 0.47 |
| 1994 | 0.94 |
| 1993 | 0.94 |
| 1992 | 0.94 |
| 1991 | 0.94 |
| 1990 | 0.82 |
| 1989 | 0.73 |
| 1988 | 0.175 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
