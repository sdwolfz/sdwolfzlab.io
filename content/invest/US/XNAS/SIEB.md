---
title: "SIEBERT FINANCIAL CORP (SIEB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SIEBERT FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>SIEB</td></tr>
    <tr><td>Web</td><td><a href="https://www.siebert.com">www.siebert.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.2 |
| 2008 | 0.1 |
| 2007 | 0.12 |
| 2006 | 0.08 |
| 2000 | 0.04 |
| 1999 | 0.16 |
| 1998 | 0.15 |
| 1997 | 0.09 |
| 1996 | 6.453 |
| 1995 | 0.36 |
| 1994 | 0.36 |
| 1993 | 0.36 |
| 1992 | 0.36 |
| 1991 | 0.36 |
| 1990 | 0.36 |
| 1989 | 0.3 |
| 1988 | 0.09 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
