---
title: "MOBEX SA TG.MURES (MOBG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>MOBEX SA TG.MURES</td></tr>
    <tr><td>Symbol</td><td>MOBG</td></tr>
    <tr><td>Web</td><td><a href="https://www.mobex.ro">www.mobex.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 1.51 |
| 2012 | 0.976 |
| 2009 | 0.7747 |
| 2008 | 0.4385 |
| 2006 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
