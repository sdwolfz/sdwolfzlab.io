---
title: " (TEDU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>TEDU</td></tr>
    <tr><td>Web</td><td><a href="https://www.tedu.cn">www.tedu.cn</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.12 |
| 2017 | 0.16 |
| 2016 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
