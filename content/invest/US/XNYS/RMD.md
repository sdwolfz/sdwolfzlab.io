---
title: "RESMED INC (RMD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>RESMED INC</td></tr>
    <tr><td>Symbol</td><td>RMD</td></tr>
    <tr><td>Web</td><td><a href="https://www.resmed.com">www.resmed.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.78 |
| 2020 | 1.56 |
| 2019 | 1.52 |
| 2018 | 1.44 |
| 2017 | 1.36 |
| 2016 | 1.26 |
| 2015 | 1.16 |
| 2014 | 1.06 |
| 2013 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
