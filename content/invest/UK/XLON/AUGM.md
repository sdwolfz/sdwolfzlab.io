---
title: "Augmentum Fint. (AUGM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Augmentum Fint.</td></tr>
    <tr><td>Symbol</td><td>AUGM</td></tr>
    <tr><td>Web</td><td><a href="https://www.augmentum.vc">www.augmentum.vc</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
