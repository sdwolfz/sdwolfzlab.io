---
title: "Capital & Counties (CAPC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Capital & Counties</td></tr>
    <tr><td>Symbol</td><td>CAPC</td></tr>
    <tr><td>Web</td><td><a href="https://www.capitalandcounties.com">www.capitalandcounties.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 1.5 |
| 2018 | 1.5 |
| 2017 | 1.5 |
| 2016 | 1.5 |
| 2015 | 1.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
