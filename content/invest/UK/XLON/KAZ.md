---
title: "KAZ MINERALS PLC ORD 20P (KAZ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>KAZ MINERALS PLC ORD 20P</td></tr>
    <tr><td>Symbol</td><td>KAZ</td></tr>
    <tr><td>Web</td><td><a href="http://www.kazminerals.com">www.kazminerals.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 12.0 |
| 2018 | 12.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
