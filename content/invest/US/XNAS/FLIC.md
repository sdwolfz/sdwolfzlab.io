---
title: "FIRST OF LONG ISLAND CORP (FLIC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST OF LONG ISLAND CORP</td></tr>
    <tr><td>Symbol</td><td>FLIC</td></tr>
    <tr><td>Web</td><td><a href="https://www.fnbli.com">www.fnbli.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.38 |
| 2020 | 0.73 |
| 2019 | 0.69 |
| 2018 | 0.47 |
| 2017 | 0.58 |
| 2016 | 0.75 |
| 2015 | 0.97 |
| 2014 | 0.8 |
| 2013 | 1.02 |
| 2012 | 0.96 |
| 2011 | 0.9 |
| 2010 | 0.84 |
| 2009 | 0.76 |
| 2008 | 0.66 |
| 2007 | 1.27 |
| 2006 | 0.9 |
| 2005 | 0.42 |
| 2004 | 0.78 |
| 2003 | 0.7 |
| 2002 | 1.2 |
| 2001 | 0.76 |
| 2000 | 0.68 |
| 1999 | 0.3 |
| 1998 | 0.57 |
| 1997 | 0.74 |
| 1996 | 0.64 |
| 1995 | 0.84 |
| 1994 | 0.76 |
| 1993 | 0.69 |
| 1992 | 0.63 |
| 1991 | 0.57 |
| 1990 | 0.54 |
| 1989 | 0.47 |
| 1988 | 0.47 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
