---
title: "COLUMBIA BANKING SYSTEMS INC (COLB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COLUMBIA BANKING SYSTEMS INC</td></tr>
    <tr><td>Symbol</td><td>COLB</td></tr>
    <tr><td>Web</td><td><a href="https://www.columbiabank.com">www.columbiabank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.56 |
| 2020 | 1.34 |
| 2019 | 1.4 |
| 2018 | 1.14 |
| 2017 | 0.88 |
| 2016 | 1.53 |
| 2015 | 1.34 |
| 2014 | 0.94 |
| 2013 | 0.41 |
| 2012 | 0.98 |
| 2011 | 0.27 |
| 2010 | 0.04 |
| 2009 | 0.07 |
| 2008 | 0.58 |
| 2007 | 0.66 |
| 2006 | 0.57 |
| 2005 | 0.39 |
| 2004 | 0.19 |
| 2003 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
