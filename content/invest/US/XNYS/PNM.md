---
title: "PNM RESOURCES INC (PNM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PNM RESOURCES INC</td></tr>
    <tr><td>Symbol</td><td>PNM</td></tr>
    <tr><td>Web</td><td><a href="https://www.pnmresources.com">www.pnmresources.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.654 |
| 2020 | 1.232 |
| 2019 | 1.16 |
| 2018 | 1.06 |
| 2017 | 0.972 |
| 2016 | 0.88 |
| 2015 | 0.8 |
| 2014 | 0.74 |
| 2013 | 0.33 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
