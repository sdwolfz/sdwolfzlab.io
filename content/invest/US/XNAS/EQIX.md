---
title: " (EQIX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>EQIX</td></tr>
    <tr><td>Web</td><td><a href="https://www.equinix.com">www.equinix.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 5.74 |
| 2020 | 10.64 |
| 2019 | 9.84 |
| 2018 | 9.12 |
| 2017 | 8.0 |
| 2016 | 7.0 |
| 2015 | 17.71 |
| 2014 | 7.57 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
