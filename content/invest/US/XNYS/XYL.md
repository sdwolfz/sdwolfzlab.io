---
title: "XYLEM INC (XYL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>XYLEM INC</td></tr>
    <tr><td>Symbol</td><td>XYL</td></tr>
    <tr><td>Web</td><td><a href="https://www.xylem.com">www.xylem.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 1.04 |
| 2019 | 0.96 |
| 2018 | 0.84 |
| 2017 | 0.72 |
| 2016 | 0.62 |
| 2015 | 0.564 |
| 2014 | 0.512 |
| 2013 | 0.348 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
