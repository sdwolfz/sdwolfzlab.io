---
title: "VERITEX HLDGS INC (VBTX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>VERITEX HLDGS INC</td></tr>
    <tr><td>Symbol</td><td>VBTX</td></tr>
    <tr><td>Web</td><td><a href="https://www.veritexbank.com">www.veritexbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.34 |
| 2020 | 0.68 |
| 2019 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
