---
title: "FAM SA GALATI (FAMZ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>FAM SA GALATI</td></tr>
    <tr><td>Symbol</td><td>FAMZ</td></tr>
    <tr><td>Web</td><td><a href="https://www.fam-galati.ro">www.fam-galati.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.5 |
| 2014 | 2.1578 |
| 2013 | 0.069 |
| 2010 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
