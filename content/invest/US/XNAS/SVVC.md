---
title: "FIRSTHAND TECHNOLOGY VALUE FD INC (SVVC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRSTHAND TECHNOLOGY VALUE FD INC</td></tr>
    <tr><td>Symbol</td><td>SVVC</td></tr>
    <tr><td>Web</td><td><a href="https://www.firsthandtvf.com">www.firsthandtvf.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.034 |
| 2014 | 5.86 |
| 2013 | 0.317 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
