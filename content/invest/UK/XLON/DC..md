---
title: "Dixons Carphone (DC.)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Dixons Carphone</td></tr>
    <tr><td>Symbol</td><td>DC.</td></tr>
    <tr><td>Web</td><td><a href="https://www.dixonscarphone.com">www.dixonscarphone.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.25 |
| 2019 | 6.75 |
| 2018 | 11.25 |
| 2017 | 11.25 |
| 2016 | 9.75 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
