---
title: " (SBAC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SBAC</td></tr>
    <tr><td>Web</td><td><a href="https://www.sbasite.com">www.sbasite.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.16 |
| 2020 | 1.86 |
| 2019 | 0.74 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
