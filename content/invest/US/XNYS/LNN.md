---
title: "LINDSAY CORPORATION (LNN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>LINDSAY CORPORATION</td></tr>
    <tr><td>Symbol</td><td>LNN</td></tr>
    <tr><td>Web</td><td><a href="https://www.lindsay.com">www.lindsay.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.65 |
| 2020 | 1.27 |
| 2019 | 1.24 |
| 2018 | 1.22 |
| 2017 | 1.18 |
| 2016 | 1.14 |
| 2015 | 1.1 |
| 2014 | 1.06 |
| 2013 | 0.26 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
