---
title: "AT HOME GROUP INC (HOME)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AT HOME GROUP INC</td></tr>
    <tr><td>Symbol</td><td>HOME</td></tr>
    <tr><td>Web</td><td><a href="https://www.athome.com">www.athome.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 0.06 |
| 2013 | 0.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
