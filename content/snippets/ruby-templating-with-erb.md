---
title: "Ruby templating with ERB"
date: 2020-11-16T13:40:26Z
type: posts
slug: ruby-templating-with-erb
tags: ["ruby", "development"]
---

ERB comes by default with ruby, you only need to require it:

```ruby
require 'erb'
```

Create a template file `./template/steak.erb`:

```erb
A large steak, <%= @how_done %>, please!
<%- 3.times do -%>
And another!
<%- end -%>
```

And read it into a variable:

```ruby
tempalte = File.read('./template/steak.erb')
```

Now you can bind instance variables and pass them to the renderer:

```ruby
renderer = ERB.new(template, nil, '-')

@how_done = 'medium rare'
puts renderer.result(binding)
```
