---
title: " (UHT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>UHT</td></tr>
    <tr><td>Web</td><td><a href="https://www.uhrit.com">www.uhrit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.695 |
| 2020 | 2.76 |
| 2019 | 2.72 |
| 2018 | 2.68 |
| 2017 | 2.64 |
| 2016 | 2.6 |
| 2015 | 2.56 |
| 2014 | 2.52 |
| 2013 | 2.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
