---
title: " (INN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>INN</td></tr>
    <tr><td>Web</td><td><a href="https://www.shpreit.com">www.shpreit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.18 |
| 2019 | 0.72 |
| 2018 | 0.72 |
| 2017 | 0.672 |
| 2016 | 0.543 |
| 2015 | 0.468 |
| 2014 | 0.458 |
| 2013 | 0.224 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
