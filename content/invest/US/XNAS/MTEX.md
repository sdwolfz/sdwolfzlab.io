---
title: "MANNATECH INC (MTEX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MANNATECH INC</td></tr>
    <tr><td>Symbol</td><td>MTEX</td></tr>
    <tr><td>Web</td><td><a href="https://www.mannatech.com">www.mannatech.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.16 |
| 2020 | 1.57 |
| 2019 | 0.5 |
| 2018 | 1.25 |
| 2017 | 0.5 |
| 2016 | 0.25 |
| 2009 | 0.04 |
| 2008 | 0.22 |
| 2007 | 0.36 |
| 2006 | 0.32 |
| 2005 | 0.29 |
| 2004 | 0.27 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
