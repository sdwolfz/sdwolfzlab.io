---
title: "SYNALLOY CORP (SYNL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SYNALLOY CORP</td></tr>
    <tr><td>Symbol</td><td>SYNL</td></tr>
    <tr><td>Web</td><td><a href="https://www.synalloy.com">www.synalloy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.25 |
| 2017 | 0.13 |
| 2015 | 0.3 |
| 2014 | 0.3 |
| 2013 | 0.26 |
| 2012 | 0.25 |
| 2011 | 0.25 |
| 2010 | 0.5 |
| 2009 | 0.1 |
| 2008 | 0.25 |
| 2007 | 0.15 |
| 2001 | 0.15 |
| 2000 | 0.2 |
| 1999 | 0.2 |
| 1998 | 0.4 |
| 1997 | 0.37 |
| 1996 | 0.34 |
| 1995 | 0.36 |
| 1994 | 0.29 |
| 1993 | 0.34 |
| 1992 | 0.42 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
