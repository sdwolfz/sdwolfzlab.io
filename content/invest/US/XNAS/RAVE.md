---
title: "RAVE RESTAURANT GROUP INC (RAVE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>RAVE RESTAURANT GROUP INC</td></tr>
    <tr><td>Symbol</td><td>RAVE</td></tr>
    <tr><td>Web</td><td><a href="https://www.raverg.com">www.raverg.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2000 | 0.24 |
| 1999 | 0.24 |
| 1998 | 0.24 |
| 1997 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
