---
title: "POLYPIPE GROUP PLC ORD GBP0.001 (PLP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>POLYPIPE GROUP PLC ORD GBP0.001</td></tr>
    <tr><td>Symbol</td><td>PLP</td></tr>
    <tr><td>Web</td><td><a href="http://ir.polypipe.com">ir.polypipe.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 12.1 |
| 2018 | 11.6 |
| 2017 | 11.1 |
| 2016 | 10.1 |
| 2015 | 7.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
