---
title: "BROOKFIELD INFRASTRUCTURE CORP (BIPC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BROOKFIELD INFRASTRUCTURE CORP</td></tr>
    <tr><td>Symbol</td><td>BIPC</td></tr>
    <tr><td>Web</td><td><a href="https://bip.brookfield.com">bip.brookfield.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.51 |
| 2020 | 1.455 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
