---
title: "CONSTRUCTII FEROVIARE SA GALATI (CORO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>CONSTRUCTII FEROVIARE SA GALATI</td></tr>
    <tr><td>Symbol</td><td>CORO</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 0.2484 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
