---
title: "SIF TRANSILVANIA S.A. (SIF3)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>SIF TRANSILVANIA S.A.</td></tr>
    <tr><td>Symbol</td><td>SIF3</td></tr>
    <tr><td>Web</td><td><a href="https://www.siftransilvania.ro">www.siftransilvania.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.0355 |
| 2018 | 0.0121 |
| 2017 | 0.01 |
| 2016 | 0.02 |
| 2015 | 0.02653 |
| 2014 | 0.0125 |
| 2012 | 0.175 |
| 2011 | 0.1712 |
| 2010 | 0.03 |
| 2009 | 0.03 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
