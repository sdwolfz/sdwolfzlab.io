---
title: "GRIDSUM HOLDING INC SPON ADS EACH REP 1 ORD SHS CL B (GSUM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GRIDSUM HOLDING INC SPON ADS EACH REP 1 ORD SHS CL B</td></tr>
    <tr><td>Symbol</td><td>GSUM</td></tr>
    <tr><td>Web</td><td><a href="http://www.gridsum.com">www.gridsum.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
