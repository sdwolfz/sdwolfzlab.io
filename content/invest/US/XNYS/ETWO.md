---
title: "E2OPEN PARENT HOLDINGS INC (ETWO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>E2OPEN PARENT HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>ETWO</td></tr>
    <tr><td>Web</td><td><a href="https://www.e2open.com">www.e2open.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
