---
title: "PENNYMAC FINANCIAL SERVICES INC (PFSI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PENNYMAC FINANCIAL SERVICES INC</td></tr>
    <tr><td>Symbol</td><td>PFSI</td></tr>
    <tr><td>Web</td><td><a href="https://www.pennymacfinancial.com">www.pennymacfinancial.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.54 |
| 2019 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
