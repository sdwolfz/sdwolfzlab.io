---
title: "OSISKO GOLD ROYALTIES LTD (OR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>OSISKO GOLD ROYALTIES LTD</td></tr>
    <tr><td>Symbol</td><td>OR</td></tr>
    <tr><td>Web</td><td><a href="https://www.osiskogr.com">www.osiskogr.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.149 |
| 2019 | 0.151 |
| 2018 | 0.155 |
| 2017 | 0.14 |
| 2016 | 0.092 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
