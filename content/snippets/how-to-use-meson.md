---
title: "How to Use: meson"
date: 2020-12-20T11:43:11Z
type: posts
slug: how-to-use-meson
series: ["How to Use"]
tags: ["linux", "c", "development"]
---

## Install

```sh
sudo pacman -S gcc meson ninja
```

## Develop

Write a sample program inside `src/main.c`:

```c
#include <stdio.h>

int main(int argc, char **argv) {
  printf("Hello World!\n");

  return 0;
}
```

## Configure

Create a `meson.build` file containing:

```python
project(
  'meson-sample',
  'c',
  version: '0.0.1'
)

executable('hello', 'src/main.c')
```

Set up your compile target config files:

* `compile/x86_64-generic.ini`:

```ini
[target_machine]
system     = 'linux'
cpu_family = 'x86_64'
cpu        = 'x86'
endian     = 'little'

[binaries]
strip = 'strip'

[built-in options]
c_std  = 'c17'
c_args = '-march=x86-64 -mtune=generic'
```

* `compile/x86_64-znver1.ini`:

```ini
[target_machine]
system     = 'linux'
cpu_family = 'x86_64'
cpu        = 'znver1'
endian     = 'little'

[binaries]
strip = 'strip'

[built-in options]
c_std  = 'c17'
c_args = '-march=znver1 -mtune=znver1'
```

## Compile

1. First set up your build targets:

```sh
meson setup build/x86_64-generic \
  --buildtype=release            \
  --cross-file ./compile/x86_64-generic.ini

meson setup build/x86_64-znver1 \
  --buildtype=release           \
  --cross-file ./compile/x86_64-znver1.ini
```

2. Then you can compile:

```sh
meson compile -C build/x86_64-generic
meson compile -C build/x86_64-znver1
```

3. Now your binaries are ready to run:

```sh
time ./build/x86_64-generic/hello
# Hello World!
#
# real    0m0.002s
# user    0m0.001s
# sys     0m0.001s

time ./build/x86_64-znver1/hello
# Hello World!
#
# real    0m0.002s
# user    0m0.002s
# sys     0m0.000s
```

## Aarch64

Cross compile for ARM with these extra steps:

1. Install a cross-compiler:

```sh
sudo pacman -S aarch64-linux-gnu-gcc
```

2. Set up your compile target config file:

* `compile/aarch64-generic.ini`:

```ini
[target_machine]
system     = 'linux'
cpu_family = 'aarch64'
cpu        = 'aarch64'
endian     = 'little'

[binaries]
c     = 'aarch64-linux-gnu-gcc'
strip = 'strip'

[built-in options]
c_std  = 'c17'
c_args = '-march=armv8-a -mtune=cortex-a72'
```

3. Set up your build target:

```sh
meson setup build/aarch64-generic \
  --buildtype=release             \
  --cross-file ./compile/aarch64-generic.ini
```

4. Compile:

```sh
meson compile -C build/aarch64-generic
```

5. Now you can run it, given you have, or transfer it to, a ARM CPU device:

```sh
time ./build/aarch64-generic/hello
# Hello World!
#
# real    0m0.002s
# user    0m0.002s
# sys     0m0.000s
```
