---
title: "One Liners: Edit hosts file"
date: 2020-12-08T12:46:23Z
type: posts
slug: one-liners-edit-hosts-files
series: ["One Liners"]
tags: ["linux", "productivity"]
---

## How

Add `mydomain.test` pointing to `127.0.0.1` to your `/etc/hosts` file use:

```sh
grep -q '127.0.0.1 mydomain.test' /etc/hosts ||
  sudo sed -i '$ a 127.0.0.1 mydomain.test' /etc/hosts
```

To remove it do:

```sh
grep -q '127.0.0.1 mydomain.test' /etc/hosts &&
  sudo sed -i '/127.0.0.1 mydomain.test/d' /etc/hosts
```

Comment it out with:

```sh
grep -q '127.0.0.1 mydomain.test' /etc/hosts &&
  sudo sed -i 's/127.0.0.1 mydomain.test/# &/' /etc/hosts
```

And uncomment it by executing:

```sh
grep -q '# 127.0.0.1 mydomain.test' /etc/hosts &&
  sudo sed -i '/# 127.0.0.1 mydomain.test/s/^# //g' /etc/hosts
```

Add them to your `Makefile` for ultimate productivity:

```Makefile
IP     := 127.0.0.1
DOMAIN := mydomain.test

.PHONY: domain/add
domain/add:
	@grep -q '$(IP) $(DOMAIN)' /etc/hosts ||
		sudo sed -i '$ a $(IP) $(DOMAIN)' /etc/hosts

.PHONY: domain/remove
domain/add:
	@grep -q '$(IP) $(DOMAIN)' /etc/hosts &&
		sudo sed -i '/$(IP) $(DOMAIN)/d' /etc/hosts

.PHONY: domain/comment
domain/add:
	@grep -q '$(IP) $(DOMAIN)' /etc/hosts &&
		sudo sed -i 's/$(IP) $(DOMAIN)/# &/' /etc/hosts

.PHONY: domain/uncomment
domain/add:
	@grep -q '# $(IP) $(DOMAIN)' /etc/hosts &&
		sudo sed -i '/# $(IP) $(DOMAIN)/s/^# //g' /etc/hosts
```

So you can quickly use:

```sh
make domain/add
make domain/remove
make domain/comment
make domain/uncomment
```

Or in general with:

```sh
make domain/add       IP=127.0.0.1 DOMAIN=mydomain.test
make domain/remove    IP=127.0.0.1 DOMAIN=mydomain.test
make domain/comment   IP=127.0.0.1 DOMAIN=mydomain.test
make domain/uncomment IP=127.0.0.1 DOMAIN=mydomain.test
```

## Why

Using `grep` before `sudo sed` means you only need your `sudo` password if you
mutate the file. Clever trick to save you time!

Domains pointing to localhost have to be `.test` and not something else, more
about it in the official RFC:

* https://tools.ietf.org/html/rfc2606
* https://en.wikipedia.org/wiki/.test
