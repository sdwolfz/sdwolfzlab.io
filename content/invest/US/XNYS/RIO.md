---
title: " (RIO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>RIO</td></tr>
    <tr><td>Web</td><td><a href="https://www.riotinto.com">www.riotinto.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 4.02 |
| 2020 | 3.86 |
| 2019 | 6.35 |
| 2018 | 3.063 |
| 2017 | 2.366 |
| 2016 | 1.513 |
| 2015 | 2.227 |
| 2014 | 2.025 |
| 2013 | 0.846 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
