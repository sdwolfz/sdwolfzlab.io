---
title: "FULL HOUSE RESORTS INC (FLL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FULL HOUSE RESORTS INC</td></tr>
    <tr><td>Symbol</td><td>FLL</td></tr>
    <tr><td>Web</td><td><a href="https://www.fullhouseresorts.com">www.fullhouseresorts.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
