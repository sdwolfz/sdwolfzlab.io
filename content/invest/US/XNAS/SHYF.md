---
title: "SHYFT GROUP INC (SHYF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SHYFT GROUP INC</td></tr>
    <tr><td>Symbol</td><td>SHYF</td></tr>
    <tr><td>Web</td><td><a href="https://www.theshyftgroup.com">www.theshyftgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.05 |
| 2020 | 0.1 |
| 2019 | 0.1 |
| 2018 | 0.1 |
| 2017 | 0.1 |
| 2016 | 0.1 |
| 2015 | 0.1 |
| 2014 | 0.1 |
| 2013 | 0.1 |
| 2012 | 0.1 |
| 2011 | 0.1 |
| 2010 | 0.1 |
| 2009 | 0.13 |
| 2008 | 0.1 |
| 2007 | 0.16 |
| 2006 | 0.27 |
| 2005 | 0.26 |
| 2004 | 0.22 |
| 2003 | 0.2 |
| 2002 | 0.16 |
| 2001 | 0.07 |
| 2000 | 0.07 |
| 1999 | 0.07 |
| 1998 | 0.07 |
| 1997 | 0.14 |
| 1996 | 0.05 |
| 1995 | 0.05 |
| 1994 | 0.05 |
| 1993 | 0.05 |
| 1992 | 0.05 |
| 1991 | 0.05 |
| 1990 | 0.05 |
| 1989 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
