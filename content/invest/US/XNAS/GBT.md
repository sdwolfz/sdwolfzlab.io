---
title: "GLOBAL BLOOD THERAPEUTICS INC (GBT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GLOBAL BLOOD THERAPEUTICS INC</td></tr>
    <tr><td>Symbol</td><td>GBT</td></tr>
    <tr><td>Web</td><td><a href="https://www.gbt.com">www.gbt.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
