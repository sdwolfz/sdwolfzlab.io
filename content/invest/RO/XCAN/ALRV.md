---
title: "ALIMENTARA SA Slatina (ALRV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>ALIMENTARA SA Slatina</td></tr>
    <tr><td>Symbol</td><td>ALRV</td></tr>
    <tr><td>Web</td><td><a href="https://www.alimentara-slatina.ro">www.alimentara-slatina.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2009 | 2.0589 |
| 2008 | 0.4711 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
