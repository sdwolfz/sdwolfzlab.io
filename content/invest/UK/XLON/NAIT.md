---
title: "North American (NAIT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>North American</td></tr>
    <tr><td>Symbol</td><td>NAIT</td></tr>
    <tr><td>Web</td><td><a href="https://www.northamericanincome.co.uk">www.northamericanincome.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 9.5 |
| 2019 | 8.5 |
| 2018 | 7.8 |
| 2017 | 7.2 |
| 2016 | 6.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
