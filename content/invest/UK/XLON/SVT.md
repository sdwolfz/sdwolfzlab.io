---
title: "Severn Trent (SVT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Severn Trent</td></tr>
    <tr><td>Symbol</td><td>SVT</td></tr>
    <tr><td>Web</td><td><a href="https://www.severntrent.com">www.severntrent.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 100.08 |
| 2019 | 93.37 |
| 2018 | 86.55 |
| 2017 | 81.5 |
| 2016 | 80.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
