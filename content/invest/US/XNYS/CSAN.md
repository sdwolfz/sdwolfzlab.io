---
title: " (CSAN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CSAN</td></tr>
    <tr><td>Web</td><td><a href="https://www.cosan.com.br">www.cosan.com.br</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.171 |
| 2020 | 1.023 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
