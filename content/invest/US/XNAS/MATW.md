---
title: "MATTHEWS INTL CORP (MATW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MATTHEWS INTL CORP</td></tr>
    <tr><td>Symbol</td><td>MATW</td></tr>
    <tr><td>Web</td><td><a href="https://www.matw.com">www.matw.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.43 |
| 2020 | 0.845 |
| 2019 | 0.81 |
| 2018 | 0.77 |
| 2017 | 0.7 |
| 2016 | 0.62 |
| 2015 | 0.54 |
| 2014 | 0.46 |
| 2013 | 0.41 |
| 2012 | 0.37 |
| 2011 | 0.33 |
| 2010 | 0.29 |
| 2009 | 0.265 |
| 2008 | 0.245 |
| 2007 | 0.225 |
| 2006 | 0.205 |
| 2005 | 0.185 |
| 2004 | 0.165 |
| 2003 | 0.123 |
| 2002 | 0.106 |
| 2001 | 0.176 |
| 2000 | 0.193 |
| 1999 | 0.183 |
| 1998 | 0.258 |
| 1997 | 0.245 |
| 1996 | 0.3 |
| 1995 | 0.26 |
| 1994 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
