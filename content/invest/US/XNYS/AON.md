---
title: "AON PLC (IE) (AON)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AON PLC (IE)</td></tr>
    <tr><td>Symbol</td><td>AON</td></tr>
    <tr><td>Web</td><td><a href="https://www.aon.com">www.aon.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.97 |
| 2020 | 1.78 |
| 2019 | 1.72 |
| 2018 | 1.96 |
| 2017 | 1.41 |
| 2016 | 1.29 |
| 2015 | 1.15 |
| 2014 | 0.925 |
| 2013 | 0.35 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
