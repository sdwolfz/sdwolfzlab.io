---
title: "CONCIFOR SA BUZAU (COBU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>CONCIFOR SA BUZAU</td></tr>
    <tr><td>Symbol</td><td>COBU</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2004 | 0.018 |
| 2003 | 0.019 |
| 2002 | 0.0057 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
