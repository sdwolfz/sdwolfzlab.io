---
title: "MIDLAND STS BANCORP INC (MSBI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MIDLAND STS BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>MSBI</td></tr>
    <tr><td>Web</td><td><a href="https://www.midlandsb.com">www.midlandsb.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.56 |
| 2020 | 1.07 |
| 2019 | 0.97 |
| 2018 | 0.88 |
| 2017 | 0.8 |
| 2016 | 0.36 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
