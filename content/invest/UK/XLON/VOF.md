---
title: "VinaCapital vietnam Opportunity Fund (VOF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>VinaCapital vietnam Opportunity Fund</td></tr>
    <tr><td>Symbol</td><td>VOF</td></tr>
    <tr><td>Web</td><td><a href="https://www.vinacapital.com">www.vinacapital.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 11.0 |
| 2019 | 11.0 |
| 2018 | 15.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
