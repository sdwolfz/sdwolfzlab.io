---
title: "ORASURE TECHNOLOGIES INC (OSUR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ORASURE TECHNOLOGIES INC</td></tr>
    <tr><td>Symbol</td><td>OSUR</td></tr>
    <tr><td>Web</td><td><a href="https://www.orasure.com">www.orasure.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
