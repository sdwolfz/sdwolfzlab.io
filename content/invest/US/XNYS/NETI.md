---
title: "ENETI INC (NETI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ENETI INC</td></tr>
    <tr><td>Symbol</td><td>NETI</td></tr>
    <tr><td>Web</td><td><a href="https://www.eneti-inc.com">www.eneti-inc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
