---
title: "TORONTO-DOMINION BANK (TD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TORONTO-DOMINION BANK</td></tr>
    <tr><td>Symbol</td><td>TD</td></tr>
    <tr><td>Web</td><td><a href="https://www.td.com">www.td.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.62 |
| 2020 | 2.309 |
| 2019 | 2.169 |
| 2018 | 2.036 |
| 2017 | 1.787 |
| 2016 | 1.637 |
| 2015 | 1.873 |
| 2014 | 1.84 |
| 2013 | 1.66 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
