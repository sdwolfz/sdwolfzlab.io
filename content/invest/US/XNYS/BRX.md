---
title: " (BRX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BRX</td></tr>
    <tr><td>Web</td><td><a href="https://www.brixmor.com">www.brixmor.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.645 |
| 2020 | 0.57 |
| 2019 | 1.12 |
| 2018 | 1.1 |
| 2017 | 1.04 |
| 2016 | 0.98 |
| 2015 | 0.9 |
| 2014 | 0.727 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
