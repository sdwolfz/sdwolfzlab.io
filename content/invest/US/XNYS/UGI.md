---
title: "UGI CORP (UGI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>UGI CORP</td></tr>
    <tr><td>Symbol</td><td>UGI</td></tr>
    <tr><td>Web</td><td><a href="https://www.ugicorp.com">www.ugicorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.675 |
| 2020 | 1.315 |
| 2019 | 1.21 |
| 2018 | 1.03 |
| 2017 | 0.987 |
| 2016 | 0.939 |
| 2015 | 0.902 |
| 2014 | 1.013 |
| 2013 | 0.564 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
