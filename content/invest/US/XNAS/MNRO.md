---
title: "MONRO INC (MNRO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MONRO INC</td></tr>
    <tr><td>Symbol</td><td>MNRO</td></tr>
    <tr><td>Web</td><td><a href="https://www.monro.com">www.monro.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.22 |
| 2020 | 0.88 |
| 2019 | 0.86 |
| 2018 | 0.78 |
| 2017 | 0.71 |
| 2016 | 0.66 |
| 2015 | 0.58 |
| 2014 | 0.5 |
| 2013 | 0.33 |
| 2012 | 0.49 |
| 2011 | 0.34 |
| 2010 | 0.37 |
| 2009 | 0.33 |
| 2008 | 0.24 |
| 2007 | 0.29 |
| 2006 | 0.24 |
| 2005 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
