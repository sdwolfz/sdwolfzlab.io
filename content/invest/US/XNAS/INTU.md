---
title: "INTUIT INC (INTU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>INTUIT INC</td></tr>
    <tr><td>Symbol</td><td>INTU</td></tr>
    <tr><td>Web</td><td><a href="https://www.intuit.com">www.intuit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.18 |
| 2020 | 2.18 |
| 2019 | 1.94 |
| 2018 | 1.64 |
| 2017 | 1.41 |
| 2016 | 1.24 |
| 2015 | 1.05 |
| 2014 | 0.82 |
| 2013 | 0.7 |
| 2012 | 0.62 |
| 2011 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
