---
title: "Switch between Pacman branches"
date: 2021-03-31T15:39:51Z
type: posts
slug: switch-between-pacman-branches
tags: ["linux", "pacman", "arch", "manjaro"]
---

For the `testing` branch:

```sh
sudo pacman-mirrors -aS testing
```

Stable can be set back with:

```sh
sudo pacman-mirrors -aS stable
```

And so on...
