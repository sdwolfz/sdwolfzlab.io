---
title: "MILLENDO THERAPEUTICS INC (MLND)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MILLENDO THERAPEUTICS INC</td></tr>
    <tr><td>Symbol</td><td>MLND</td></tr>
    <tr><td>Web</td><td><a href="https://www.millendo.com">www.millendo.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
