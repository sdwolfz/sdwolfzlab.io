---
title: "CENTRAIS ELETR BRAS SA - ELETROBRAS ADR-EACH REPR 1 COM SHS NPV (EBR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CENTRAIS ELETR BRAS SA - ELETROBRAS ADR-EACH REPR 1 COM SHS NPV</td></tr>
    <tr><td>Symbol</td><td>EBR</td></tr>
    <tr><td>Web</td><td><a href="http://www.eletrobras.com">www.eletrobras.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.285 |
| 2019 | 0.202 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
