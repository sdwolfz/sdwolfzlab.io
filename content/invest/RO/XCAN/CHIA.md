---
title: "CONSTRUCTII HIDROTEHNICE SA IASI (CHIA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>CONSTRUCTII HIDROTEHNICE SA IASI</td></tr>
    <tr><td>Symbol</td><td>CHIA</td></tr>
    <tr><td>Web</td><td><a href="https://www.conhidro.ro">www.conhidro.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 1.0 |
| 2015 | 1.0 |
| 2012 | 1.0 |
| 2010 | 9.524 |
| 2009 | 3.0 |
| 2007 | 0.65 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
