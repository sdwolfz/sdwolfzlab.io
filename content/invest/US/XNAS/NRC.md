---
title: "NATIONAL RESEARCH CORP (NRC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NATIONAL RESEARCH CORP</td></tr>
    <tr><td>Symbol</td><td>NRC</td></tr>
    <tr><td>Web</td><td><a href="https://www.nrchealth.com">www.nrchealth.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 0.21 |
| 2019 | 0.78 |
| 2018 | 1.13 |
| 2017 | 0.4 |
| 2016 | 0.34 |
| 2015 | 0.62 |
| 2014 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
