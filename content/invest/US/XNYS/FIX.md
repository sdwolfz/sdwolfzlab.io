---
title: "COMFORT SYSTEMS USA (FIX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>COMFORT SYSTEMS USA</td></tr>
    <tr><td>Symbol</td><td>FIX</td></tr>
    <tr><td>Web</td><td><a href="https://www.comfortsystemsusa.com">www.comfortsystemsusa.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.23 |
| 2020 | 0.425 |
| 2019 | 0.395 |
| 2018 | 0.33 |
| 2017 | 0.295 |
| 2016 | 0.275 |
| 2015 | 0.25 |
| 2014 | 0.225 |
| 2013 | 0.11 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
