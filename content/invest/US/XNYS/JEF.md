---
title: "JEFFERIES FINANCIAL GROUP INC (JEF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>JEFFERIES FINANCIAL GROUP INC</td></tr>
    <tr><td>Symbol</td><td>JEF</td></tr>
    <tr><td>Web</td><td><a href="https://www.jefferies.com">www.jefferies.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.4 |
| 2020 | 0.6 |
| 2019 | 1.773 |
| 2018 | 0.45 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
