---
title: "Stock Spirit (STCK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Stock Spirit</td></tr>
    <tr><td>Symbol</td><td>STCK</td></tr>
    <tr><td>Web</td><td><a href="https://www.stockspirits.com">www.stockspirits.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 8.94 |
| 2018 | 8.51 |
| 2017 | 8.1 |
| 2016 | 19.62 |
| 2015 | 5.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
