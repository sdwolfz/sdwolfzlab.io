---
title: "Reckitt Ben. Gp (RKT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Reckitt Ben. Gp</td></tr>
    <tr><td>Symbol</td><td>RKT</td></tr>
    <tr><td>Web</td><td><a href="https://www.reckittbenckiser.com">www.reckittbenckiser.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
