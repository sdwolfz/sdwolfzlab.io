---
title: " (MBNKP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>MBNKP</td></tr>
    <tr><td>Web</td><td><a href="https://www.medallionbank.com">www.medallionbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.0 |
| 2020 | 2.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
