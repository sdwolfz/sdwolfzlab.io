---
title: "QAD INC (QADA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>QAD INC</td></tr>
    <tr><td>Symbol</td><td>QADA</td></tr>
    <tr><td>Web</td><td><a href="https://www.qad.com">www.qad.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.072 |
| 2020 | 0.288 |
| 2019 | 0.288 |
| 2018 | 0.288 |
| 2017 | 0.288 |
| 2016 | 0.288 |
| 2015 | 0.288 |
| 2014 | 0.288 |
| 2013 | 0.288 |
| 2012 | 0.648 |
| 2011 | 0.252 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
