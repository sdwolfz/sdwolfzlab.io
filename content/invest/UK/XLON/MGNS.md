---
title: "Morgan Sindall Group (MGNS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Morgan Sindall Group</td></tr>
    <tr><td>Symbol</td><td>MGNS</td></tr>
    <tr><td>Web</td><td><a href="https://www.morgansindall.com">www.morgansindall.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 59.0 |
| 2018 | 53.0 |
| 2017 | 45.0 |
| 2016 | 35.0 |
| 2015 | 29.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
