---
title: "Pz Cussons (PZC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Pz Cussons</td></tr>
    <tr><td>Symbol</td><td>PZC</td></tr>
    <tr><td>Web</td><td><a href="https://www.pzcussons.com">www.pzcussons.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 5.8 |
| 2019 | 8.28 |
| 2018 | 8.28 |
| 2017 | 8.28 |
| 2016 | 8.11 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
