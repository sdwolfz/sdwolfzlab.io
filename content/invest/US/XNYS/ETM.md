---
title: "ENTERCOM COMMUNICATIONS (ETM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ENTERCOM COMMUNICATIONS</td></tr>
    <tr><td>Symbol</td><td>ETM</td></tr>
    <tr><td>Web</td><td><a href="http://www.entercom.com">www.entercom.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.02 |
| 2019 | 0.22 |
| 2018 | 0.27 |
| 2017 | 0.515 |
| 2016 | 0.225 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
