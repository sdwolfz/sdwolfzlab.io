---
title: "CONPET SA (COTE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>CONPET SA</td></tr>
    <tr><td>Symbol</td><td>COTE</td></tr>
    <tr><td>Web</td><td><a href="https://www.conpet.ro">www.conpet.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.896592 |
| 2019 | 7.052311 |
| 2018 | 7.467827 |
| 2017 | 8.041683 |
| 2016 | 16.582363 |
| 2016 | 9.818045 |
| 2015 | 7.299888 |
| 2014 | 5.940972 |
| 2013 | 3.411867 |
| 2012 | 3.2678 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
