---
title: " (NTES)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>NTES</td></tr>
    <tr><td>Web</td><td><a href="https://ir.netease.com">ir.netease.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.06 |
| 2020 | 3.86 |
| 2019 | 9.8 |
| 2018 | 1.67 |
| 2017 | 3.64 |
| 2016 | 2.93 |
| 2015 | 1.78 |
| 2014 | 2.48 |
| 2013 | 1.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
