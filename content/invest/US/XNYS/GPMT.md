---
title: " (GPMT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>GPMT</td></tr>
    <tr><td>Web</td><td><a href="https://www.gpmtreit.com">www.gpmtreit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 0.65 |
| 2019 | 1.68 |
| 2018 | 1.62 |
| 2017 | 0.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
