---
title: "Unite (UTG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Unite</td></tr>
    <tr><td>Symbol</td><td>UTG</td></tr>
    <tr><td>Web</td><td><a href="https://www.unite-group.co.uk">www.unite-group.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 33.2 |
| 2018 | 29.0 |
| 2017 | 22.7 |
| 2016 | 18.0 |
| 2015 | 15.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
