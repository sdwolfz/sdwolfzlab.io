---
title: "MACY'S INC (M)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MACY'S INC</td></tr>
    <tr><td>Symbol</td><td>M</td></tr>
    <tr><td>Web</td><td><a href="https://www.macysinc.com">www.macysinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.377 |
| 2019 | 1.508 |
| 2018 | 1.508 |
| 2017 | 1.508 |
| 2016 | 1.491 |
| 2015 | 1.393 |
| 2014 | 1.189 |
| 2013 | 0.75 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
