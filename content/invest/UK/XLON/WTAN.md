---
title: "Witan (WTAN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Witan</td></tr>
    <tr><td>Symbol</td><td>WTAN</td></tr>
    <tr><td>Web</td><td><a href="https://www.witan.co.uk">www.witan.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 5.45 |
| 2019 | 5.35 |
| 2018 | 4.7 |
| 2017 | 4.2 |
| 2016 | 3.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
