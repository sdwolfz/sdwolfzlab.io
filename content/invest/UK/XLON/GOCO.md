---
title: "GOCO GROUP PLC ORD GBP0.0002 (GOCO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>GOCO GROUP PLC ORD GBP0.0002</td></tr>
    <tr><td>Symbol</td><td>GOCO</td></tr>
    <tr><td>Web</td><td><a href="http://www.gocogroup.com">www.gocogroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 9.0 |
| 2018 | 1.6 |
| 2017 | 1.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
