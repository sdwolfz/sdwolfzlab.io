---
title: "ADAPTHEALTH CORP (AHCO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ADAPTHEALTH CORP</td></tr>
    <tr><td>Symbol</td><td>AHCO</td></tr>
    <tr><td>Web</td><td><a href="https://www.adapthealth.com">www.adapthealth.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
