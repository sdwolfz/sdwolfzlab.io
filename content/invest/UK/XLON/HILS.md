---
title: "Hill & Smith (HILS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Hill & Smith</td></tr>
    <tr><td>Symbol</td><td>HILS</td></tr>
    <tr><td>Web</td><td><a href="https://www.hsholdings.com">www.hsholdings.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 33.6 |
| 2018 | 31.8 |
| 2017 | 30.0 |
| 2016 | 26.4 |
| 2015 | 20.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
