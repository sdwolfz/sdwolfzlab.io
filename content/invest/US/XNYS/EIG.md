---
title: "EMPLOYERS HOLDINGS INC (EIG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>EMPLOYERS HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>EIG</td></tr>
    <tr><td>Web</td><td><a href="https://www.employers.com">www.employers.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.5 |
| 2020 | 1.0 |
| 2019 | 0.88 |
| 2018 | 0.8 |
| 2017 | 0.6 |
| 2016 | 0.36 |
| 2015 | 0.24 |
| 2014 | 0.24 |
| 2013 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
