---
title: "ROMPETROL RAFINARE S.A. (RRC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>ROMPETROL RAFINARE S.A.</td></tr>
    <tr><td>Symbol</td><td>RRC</td></tr>
    <tr><td>Web</td><td><a href="https://www.rompetrol-rafinare.ro">www.rompetrol-rafinare.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
