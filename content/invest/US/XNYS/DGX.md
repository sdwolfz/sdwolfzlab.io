---
title: "QUEST DIAGNOSTICS INC (DGX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>QUEST DIAGNOSTICS INC</td></tr>
    <tr><td>Symbol</td><td>DGX</td></tr>
    <tr><td>Web</td><td><a href="https://www.questdiagnostics.com">www.questdiagnostics.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.18 |
| 2020 | 2.21 |
| 2019 | 2.12 |
| 2018 | 1.95 |
| 2017 | 1.8 |
| 2016 | 1.58 |
| 2015 | 1.47 |
| 2014 | 1.29 |
| 2013 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
