---
title: "ANTECO SA PLOIESTI (ANTE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>ANTECO SA PLOIESTI</td></tr>
    <tr><td>Symbol</td><td>ANTE</td></tr>
    <tr><td>Web</td><td><a href="https://www.anteco.ro">www.anteco.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2003 | 0.4041 |
| 2002 | 1.198 |
| 2001 | 1.3568 |
| 2000 | 0.7137 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
