---
title: "PROGENITY INC (PROG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PROGENITY INC</td></tr>
    <tr><td>Symbol</td><td>PROG</td></tr>
    <tr><td>Web</td><td><a href="https://www.progenity.com">www.progenity.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
