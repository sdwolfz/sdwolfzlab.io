---
title: "NORFOLK SOUTHERN CORP (NSC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NORFOLK SOUTHERN CORP</td></tr>
    <tr><td>Symbol</td><td>NSC</td></tr>
    <tr><td>Web</td><td><a href="https://www.norfolksouthern.com">www.norfolksouthern.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.98 |
| 2020 | 3.76 |
| 2019 | 3.6 |
| 2018 | 3.04 |
| 2017 | 2.44 |
| 2016 | 2.36 |
| 2015 | 2.36 |
| 2014 | 2.22 |
| 2013 | 1.04 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
