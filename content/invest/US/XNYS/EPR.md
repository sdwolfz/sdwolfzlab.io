---
title: " (EPR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>EPR</td></tr>
    <tr><td>Web</td><td><a href="https://www.eprkc.com">www.eprkc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.514 |
| 2019 | 4.5 |
| 2018 | 4.32 |
| 2017 | 4.08 |
| 2016 | 3.84 |
| 2015 | 3.636 |
| 2014 | 3.42 |
| 2013 | 1.578 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
