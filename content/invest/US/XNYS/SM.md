---
title: "SM ENERGY COMPANY (SM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SM ENERGY COMPANY</td></tr>
    <tr><td>Symbol</td><td>SM</td></tr>
    <tr><td>Web</td><td><a href="https://www.sm-energy.com">www.sm-energy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.01 |
| 2020 | 0.02 |
| 2019 | 0.1 |
| 2018 | 0.1 |
| 2017 | 0.1 |
| 2016 | 0.1 |
| 2015 | 0.1 |
| 2014 | 0.1 |
| 2013 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
