---
title: "LORAL SPACE & COMMUNICATIONS INC (LORL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LORAL SPACE & COMMUNICATIONS INC</td></tr>
    <tr><td>Symbol</td><td>LORL</td></tr>
    <tr><td>Web</td><td><a href="https://www.loral.com">www.loral.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 7.0 |
| 2012 | 42.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
