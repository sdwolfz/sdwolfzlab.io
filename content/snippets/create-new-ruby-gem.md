---
title: "Create a new Ruby gem"
date: 2023-07-29T05:09:02Z
type: posts
slug: create-new-ruby-gem
tags: ["ruby", "development"]
---

## Setup

Add this to your `~/.bundle/config`

```yml
---

BUNDLE_GEM__TEST: "minitest"
BUNDLE_GEM__CI: "gitlab"
BUNDLE_GEM__MIT: "false"
BUNDLE_GEM__COC: "false"
BUNDLE_GEM__CHANGELOG: "false"
BUNDLE_GEM__LINTER: "rubocop"
```

## Gem Name

Use `_` for CamelCase and `-` for Name::Spaces.

```sh
# Library gem
bundle gem <NAME>

# CLI gem
bundle gem --exe <NAME>
```

## Permissions

Only if you have a CLI gem:

```sh
chmod +x exe/*
```

## Cleanup

Commit an initial empty commit by unstaging everything:

```sh
git rm -r --cached .
git commit -m 'Initial commit' --allow-empty
```

Delete gems from `Gemfile` add add them as development dependencies in your
`*.gemspec`:

``` ruby
spec.add_development_dependency 'debug',            '~> 1.7'
spec.add_development_dependency 'minitest',         '~> 5.17'
spec.add_development_dependency 'rake',             '~> 13.0'
spec.add_development_dependency 'rubocop',          '~> 1.42'
spec.add_development_dependency 'rubocop-minitest', '~> 0.25'
spec.add_development_dependency 'rubocop-rake',     '~> 0.6'
spec.add_development_dependency 'simplecov',        '~> 0.22'
spec.add_development_dependency 'yard',             '~> 0.9'
```

Edit your `version.rb` file to say `-pre1` at the end:

```ruby
VERSION = "0.1.0-pre1"
```

Add a BSD-3-Clause license in your `LICENSE` file:
* https://opensource.org/license/BSD-3-clause/

```
Copyright (c) 2023, Codruț Constantin Gușoi
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the software nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
```

## Edit

Make sure to edit your `*.gemspec` file and add in your gem details.

## Rubygems

Commit and push your first `-pre1` version to rubygems to secure your gem name:

```sh
git add .
git commit -m 'Initial -pre1 version'

gem build <NAME>.gemspec --output=release/<NAME>.gem
envchain rubygems gem push release/<NAME>.gem
```

## Development

Before starting development, add any extra setup files you need from:
* https://gitlab.com/sdwolfz/docker-projects/-/tree/master/ruby?ref_type=heads

## Documentation

https://bundler.io/guides/creating_gem.html
