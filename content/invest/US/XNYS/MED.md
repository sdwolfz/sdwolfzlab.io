---
title: "MEDIFAST INC (MED)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MEDIFAST INC</td></tr>
    <tr><td>Symbol</td><td>MED</td></tr>
    <tr><td>Web</td><td><a href="https://www.medifastinc.com">www.medifastinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.42 |
| 2020 | 4.52 |
| 2019 | 3.38 |
| 2018 | 2.19 |
| 2017 | 1.44 |
| 2016 | 1.07 |
| 2015 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
