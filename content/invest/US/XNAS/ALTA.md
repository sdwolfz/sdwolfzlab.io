---
title: "ALTABANCORP (ALTA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ALTABANCORP</td></tr>
    <tr><td>Symbol</td><td>ALTA</td></tr>
    <tr><td>Web</td><td><a href="https://www.altabancorp.com">www.altabancorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 0.56 |
| 2019 | 0.49 |
| 2018 | 0.41 |
| 2017 | 0.34 |
| 2016 | 0.29 |
| 2015 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
