---
title: "LAS VEGAS SANDS CORP (LVS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>LAS VEGAS SANDS CORP</td></tr>
    <tr><td>Symbol</td><td>LVS</td></tr>
    <tr><td>Web</td><td><a href="https://www.sands.com">www.sands.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.79 |
| 2019 | 3.08 |
| 2018 | 3.0 |
| 2017 | 2.92 |
| 2016 | 2.88 |
| 2015 | 2.6 |
| 2014 | 2.0 |
| 2013 | 0.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
