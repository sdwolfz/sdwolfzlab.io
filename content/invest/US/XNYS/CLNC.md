---
title: " (CLNC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CLNC</td></tr>
    <tr><td>Web</td><td><a href="https://www.clncredit.com">www.clncredit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.24 |
| 2020 | 0.3 |
| 2019 | 1.65 |
| 2018 | 1.45 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
