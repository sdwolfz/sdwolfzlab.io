---
title: "Spirent (SPT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Spirent</td></tr>
    <tr><td>Symbol</td><td>SPT</td></tr>
    <tr><td>Web</td><td><a href="https://www.spirent.com">www.spirent.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 5.39 |
| 2018 | 4.49 |
| 2017 | 9.08 |
| 2016 | 3.89 |
| 2015 | 3.89 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
