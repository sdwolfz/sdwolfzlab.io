---
title: "TRICO BANCSHARES (TCBK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TRICO BANCSHARES</td></tr>
    <tr><td>Symbol</td><td>TCBK</td></tr>
    <tr><td>Web</td><td><a href="https://www.tcbk.com">www.tcbk.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 0.88 |
| 2019 | 0.82 |
| 2018 | 0.7 |
| 2017 | 0.66 |
| 2016 | 0.6 |
| 2015 | 0.52 |
| 2014 | 0.44 |
| 2013 | 0.42 |
| 2012 | 0.36 |
| 2011 | 0.36 |
| 2010 | 0.4 |
| 2009 | 0.52 |
| 2008 | 0.52 |
| 2007 | 0.52 |
| 2006 | 0.48 |
| 2005 | 0.45 |
| 2004 | 0.53 |
| 2003 | 0.8 |
| 2002 | 0.8 |
| 2001 | 0.8 |
| 2000 | 0.79 |
| 1999 | 0.7 |
| 1998 | 0.64 |
| 1997 | 0.64 |
| 1996 | 0.58 |
| 1995 | 0.43 |
| 1994 | 0.4 |
| 1993 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
