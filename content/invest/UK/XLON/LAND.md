---
title: "Land Securities (LAND)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Land Securities</td></tr>
    <tr><td>Symbol</td><td>LAND</td></tr>
    <tr><td>Web</td><td><a href="https://www.landsec.com">www.landsec.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 23.2 |
| 2019 | 45.55 |
| 2018 | 44.2 |
| 2017 | 38.55 |
| 2016 | 35.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
