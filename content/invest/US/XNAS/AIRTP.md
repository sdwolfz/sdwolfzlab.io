---
title: " (AIRTP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>AIRTP</td></tr>
    <tr><td>Web</td><td><a href="https://www.airt.net">www.airt.net</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.0 |
| 2020 | 2.0 |
| 2019 | 0.087 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
