---
title: "CAPITAL CITY BANK GROUP INC (CCBG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CAPITAL CITY BANK GROUP INC</td></tr>
    <tr><td>Symbol</td><td>CCBG</td></tr>
    <tr><td>Web</td><td><a href="https://www.ccbg.com">www.ccbg.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.15 |
| 2020 | 0.57 |
| 2019 | 0.48 |
| 2018 | 0.32 |
| 2017 | 0.24 |
| 2016 | 0.17 |
| 2015 | 0.13 |
| 2014 | 0.09 |
| 2011 | 0.3 |
| 2010 | 0.49 |
| 2009 | 0.76 |
| 2008 | 0.745 |
| 2007 | 0.71 |
| 2006 | 0.663 |
| 2005 | 0.694 |
| 2004 | 0.73 |
| 2003 | 0.69 |
| 2002 | 0.628 |
| 2001 | 0.595 |
| 2000 | 0.545 |
| 1999 | 0.493 |
| 1998 | 0.505 |
| 1997 | 0.765 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
