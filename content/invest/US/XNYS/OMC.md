---
title: "OMNICOM GROUP INC (OMC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>OMNICOM GROUP INC</td></tr>
    <tr><td>Symbol</td><td>OMC</td></tr>
    <tr><td>Web</td><td><a href="https://www.omnicomgroup.com">www.omnicomgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.4 |
| 2020 | 2.6 |
| 2019 | 2.6 |
| 2018 | 2.4 |
| 2017 | 2.25 |
| 2016 | 2.15 |
| 2015 | 2.0 |
| 2014 | 1.9 |
| 2013 | 0.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
