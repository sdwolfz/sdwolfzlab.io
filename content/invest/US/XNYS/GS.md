---
title: "GOLDMAN SACHS GROUP INC (GS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GOLDMAN SACHS GROUP INC</td></tr>
    <tr><td>Symbol</td><td>GS</td></tr>
    <tr><td>Web</td><td><a href="https://www.gs.com">www.gs.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.5 |
| 2020 | 5.0 |
| 2019 | 4.15 |
| 2018 | 3.15 |
| 2017 | 2.9 |
| 2016 | 2.6 |
| 2015 | 2.55 |
| 2014 | 2.25 |
| 2013 | 1.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
