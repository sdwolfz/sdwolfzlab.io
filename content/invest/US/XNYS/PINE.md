---
title: "ALPINE INCOME PROPERTY TRUST INC (PINE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ALPINE INCOME PROPERTY TRUST INC</td></tr>
    <tr><td>Symbol</td><td>PINE</td></tr>
    <tr><td>Web</td><td><a href="https://www.alpinereit.com">www.alpinereit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.49 |
| 2020 | 0.82 |
| 2019 | 0.058 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
