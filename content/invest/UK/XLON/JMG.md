---
title: "JPMorgan Emerging Markets Investment Trust (JMG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>JPMorgan Emerging Markets Investment Trust</td></tr>
    <tr><td>Symbol</td><td>JMG</td></tr>
    <tr><td>Web</td><td><a href="https://www.jpmfemergingmarkets.com">www.jpmfemergingmarkets.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 14.2 |
| 2019 | 14.0 |
| 2018 | 12.5 |
| 2017 | 11.0 |
| 2016 | 9.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
