---
title: "Jpmorgan Glob (JEMI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Jpmorgan Glob</td></tr>
    <tr><td>Symbol</td><td>JEMI</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 5.1 |
| 2019 | 5.1 |
| 2018 | 5.0 |
| 2017 | 4.9 |
| 2016 | 4.9 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
