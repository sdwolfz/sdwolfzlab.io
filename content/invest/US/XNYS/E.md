---
title: " (E)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>E</td></tr>
    <tr><td>Web</td><td><a href="https://www.eni.com">www.eni.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.4 |
| 2020 | 0.854 |
| 2019 | 1.333 |
| 2018 | 1.372 |
| 2017 | 1.315 |
| 2016 | 1.284 |
| 2015 | 1.533 |
| 2014 | 2.241 |
| 2013 | 1.19 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
