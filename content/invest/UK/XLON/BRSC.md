---
title: "BlackRock Smaller Companies Trust PLC (BRSC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>BlackRock Smaller Companies Trust PLC</td></tr>
    <tr><td>Symbol</td><td>BRSC</td></tr>
    <tr><td>Web</td><td><a href="https://www.blackrock.com">www.blackrock.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 32.5 |
| 2019 | 31.2 |
| 2018 | 26.0 |
| 2017 | 21.0 |
| 2016 | 17.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
