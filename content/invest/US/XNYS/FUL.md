---
title: "FULLER(H.B.)CO (FUL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FULLER(H.B.)CO</td></tr>
    <tr><td>Symbol</td><td>FUL</td></tr>
    <tr><td>Web</td><td><a href="https://www.hbfuller.com">www.hbfuller.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.33 |
| 2020 | 0.646 |
| 2019 | 0.635 |
| 2018 | 0.615 |
| 2017 | 0.59 |
| 2016 | 0.55 |
| 2015 | 0.51 |
| 2014 | 0.46 |
| 2013 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
