---
title: "DAWSON GEOPHYSICAL CO (DWSN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>DAWSON GEOPHYSICAL CO</td></tr>
    <tr><td>Symbol</td><td>DWSN</td></tr>
    <tr><td>Web</td><td><a href="https://www.dawson3d.com">www.dawson3d.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2012 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
