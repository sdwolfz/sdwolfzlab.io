---
title: " (OXSQ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>OXSQ</td></tr>
    <tr><td>Web</td><td><a href="https://www.oxfordsquarecapital.com">www.oxfordsquarecapital.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.21 |
| 2020 | 0.612 |
| 2019 | 0.803 |
| 2018 | 0.8 |
| 2017 | 0.8 |
| 2016 | 1.16 |
| 2015 | 1.14 |
| 2014 | 1.16 |
| 2013 | 1.16 |
| 2012 | 1.12 |
| 2011 | 0.99 |
| 2010 | 0.81 |
| 2009 | 0.6 |
| 2008 | 1.06 |
| 2007 | 1.44 |
| 2006 | 1.38 |
| 2005 | 1.01 |
| 2004 | 0.43 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
