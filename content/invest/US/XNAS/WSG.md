---
title: "WANDA SPORTS GROUP CO LTD SPON ADS EA REP 1.5 CL A ORD SHS (WSG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>WANDA SPORTS GROUP CO LTD SPON ADS EA REP 1.5 CL A ORD SHS</td></tr>
    <tr><td>Symbol</td><td>WSG</td></tr>
    <tr><td>Web</td><td><a href="http://www.wsg.cn">www.wsg.cn</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
