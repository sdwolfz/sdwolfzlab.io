---
title: "Docker Images: basic entrypoint"
date: 2020-07-14T21:24:54Z
type: posts
slug: docker-images-basic-entrypoint
series: ["Docker Images"]
tags: ["docker", "shell"]
---

This is a docker entrypoint script that loads environment variables from a
mounted file and executes the given command; most of the time it's all you need.

```sh
#!/bin/sh

set -e

# Load environment variables.
if [ -f /environment/secrets.env ];
then
  set -a
  echo 'Sourcing: /environment/secrets.env'
  . /environment/secrets.env
  set +a
fi

# Executes the given command.
exec "$@"
```

Give this file a short and simple name like `entry` and copy it in your
`Dockerfile`:

```Dockerfile
COPY ./entry /

RUN set -exo pipefail                      && \
                                              \
    echo 'Ensure entrypoint is executable' && \
    chmod +x /entry

ENTRYPOINT ["/entry"]
```

In an image with an existing entrypoint, like `postgres`, you need to chain the
existing entrypoint, and redefine the command, since `CMD` is always unset when
an `ENTRYPOINT` is defined:

```Dockerfile
ENTRYPOINT ["/entry", "docker-entrypoint.sh"]
CMD ["postgres"]
```

Make sure to `docker run` with that `secrets.env` file mounted:

```sh
docker run --rm -it -v "$PWD"/secrets.env:/environment/secrets.env alpine env
```

Or if using `docker-compose` do this:

```yml
services:
  postgres:
    build:
      context: .
      dockerfile: ./docker/postgres/Dockerfile
    volumes:
      - ./environment/secrets.env:/environment/secrets.env
```
