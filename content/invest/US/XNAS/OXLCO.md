---
title: "OXFORD LANE CAPITAL CORP 7.5% PRF SER'2023' (OXLCO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>OXFORD LANE CAPITAL CORP 7.5% PRF SER'2023'</td></tr>
    <tr><td>Symbol</td><td>OXLCO</td></tr>
    <tr><td>Web</td><td><a href="http://www.oxfordlanecapital.com">www.oxfordlanecapital.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.313 |
| 2020 | 1.875 |
| 2019 | 1.875 |
| 2018 | 1.875 |
| 2017 | 1.875 |
| 2016 | 1.875 |
| 2015 | 1.875 |
| 2014 | 1.875 |
| 2013 | 0.99 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
