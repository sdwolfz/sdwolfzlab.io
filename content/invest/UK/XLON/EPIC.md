---
title: "Ediston Prprty (EPIC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Ediston Prprty</td></tr>
    <tr><td>Symbol</td><td>EPIC</td></tr>
    <tr><td>Web</td><td><a href="https://www.epic-REIT.com">www.epic-REIT.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 4.88 |
| 2019 | 5.75 |
| 2018 | 5.75 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
