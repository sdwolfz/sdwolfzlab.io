---
title: " (MPLX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>MPLX</td></tr>
    <tr><td>Web</td><td><a href="https://www.mplx.com">www.mplx.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.376 |
| 2020 | 2.752 |
| 2019 | 2.651 |
| 2018 | 2.49 |
| 2017 | 2.21 |
| 2016 | 2.03 |
| 2015 | 1.702 |
| 2014 | 1.34 |
| 2013 | 0.583 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
