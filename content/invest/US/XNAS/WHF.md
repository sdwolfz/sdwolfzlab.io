---
title: "WHITEHORSE FIN INC (WHF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>WHITEHORSE FIN INC</td></tr>
    <tr><td>Symbol</td><td>WHF</td></tr>
    <tr><td>Web</td><td><a href="https://www.whitehorsefinance.com">www.whitehorsefinance.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.355 |
| 2020 | 1.545 |
| 2019 | 1.615 |
| 2018 | 1.42 |
| 2017 | 1.42 |
| 2016 | 1.42 |
| 2015 | 1.42 |
| 2014 | 1.42 |
| 2013 | 1.42 |
| 2012 | 0.108 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
