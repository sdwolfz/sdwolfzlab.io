---
title: "Git refined diff output"
date: 2022-09-09T07:29:17Z
type: posts
slug: git-refined-diff-output
tags: ["git", "terminal", "linux"]
---

## Package

Find the location of your `diff-highlight` package:

{{< tabgroup align="right" style="code" >}}
  {{< tab name="Manjaro/Arch" >}}
  ```
  pacman -Ql git | grep diff-highlight
  ```
  {{< /tab >}}

  {{< tab name="Debian/Ubuntu" >}}
  ```
  dpkg -L git | grep diff-highlight
  ```
  {{< /tab >}}

  {{< tab name="Fedora" >}}
  ```
  rpm -ql git | grep diff-highlight
  ```
  {{< /tab >}}
{{< /tabgroup >}}

## Configure

Edit your `~/.gitconfig` to contain this section:

```conf
[pager]
  log  = perl /usr/share/git/diff-highlight/diff-highlight | less
  show = perl /usr/share/git/diff-highlight/diff-highlight | less
  diff = perl /usr/share/git/diff-highlight/diff-highlight | less
```

## Usage

Now your `git diff` will highlight individual changes.

## Future

All we have to do now is wait until git will have the diff highlight properly
implemented as a flag or config option instead of having it written in an
obsolete and slow programming language.
