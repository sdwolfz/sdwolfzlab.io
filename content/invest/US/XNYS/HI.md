---
title: "HILLENBRAND INC (HI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HILLENBRAND INC</td></tr>
    <tr><td>Symbol</td><td>HI</td></tr>
    <tr><td>Web</td><td><a href="https://www.hillenbrand.com">www.hillenbrand.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.215 |
| 2020 | 0.854 |
| 2019 | 0.843 |
| 2018 | 0.831 |
| 2017 | 0.822 |
| 2016 | 0.814 |
| 2015 | 0.803 |
| 2014 | 0.794 |
| 2013 | 0.588 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
