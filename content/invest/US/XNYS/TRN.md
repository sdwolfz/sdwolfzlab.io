---
title: "TRINITY INDUSTRIES INC (TRN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TRINITY INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>TRN</td></tr>
    <tr><td>Web</td><td><a href="https://www.trin.net">www.trin.net</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.63 |
| 2020 | 0.76 |
| 2019 | 0.64 |
| 2018 | 8.52 |
| 2017 | 0.48 |
| 2016 | 0.44 |
| 2015 | 0.42 |
| 2014 | 0.5 |
| 2013 | 0.28 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
