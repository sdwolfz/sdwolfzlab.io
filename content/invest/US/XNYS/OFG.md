---
title: "OFG BANCORP (OFG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>OFG BANCORP</td></tr>
    <tr><td>Symbol</td><td>OFG</td></tr>
    <tr><td>Web</td><td><a href="https://www.ofgbancorp.com">www.ofgbancorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.16 |
| 2020 | 0.28 |
| 2019 | 0.28 |
| 2018 | 0.25 |
| 2017 | 0.24 |
| 2016 | 0.24 |
| 2015 | 0.36 |
| 2014 | 0.34 |
| 2013 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
