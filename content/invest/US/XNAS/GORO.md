---
title: "GOLD RESOURCE CORPORATION (GORO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GOLD RESOURCE CORPORATION</td></tr>
    <tr><td>Symbol</td><td>GORO</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.023 |
| 2020 | 0.04 |
| 2019 | 0.023 |
| 2018 | 0.02 |
| 2017 | 0.02 |
| 2016 | 0.03 |
| 2015 | 0.12 |
| 2014 | 0.12 |
| 2013 | 0.48 |
| 2012 | 0.68 |
| 2011 | 0.48 |
| 2010 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
