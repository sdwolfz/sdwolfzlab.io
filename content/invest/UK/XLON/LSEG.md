---
title: "Lon.stk.exch (LSEG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Lon.stk.exch</td></tr>
    <tr><td>Symbol</td><td>LSEG</td></tr>
    <tr><td>Web</td><td><a href="https://www.lseg.com">www.lseg.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 75.0 |
| 2019 | 70.0 |
| 2018 | 60.4 |
| 2017 | 51.6 |
| 2016 | 43.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
