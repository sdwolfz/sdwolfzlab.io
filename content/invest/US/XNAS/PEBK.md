---
title: "PEOPLES BANCORP OF NORTH CAROLINA (PEBK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PEOPLES BANCORP OF NORTH CAROLINA</td></tr>
    <tr><td>Symbol</td><td>PEBK</td></tr>
    <tr><td>Web</td><td><a href="https://www.peoplesbanknc.com">www.peoplesbanknc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.16 |
| 2020 | 0.75 |
| 2019 | 0.66 |
| 2018 | 0.52 |
| 2017 | 0.36 |
| 2016 | 0.38 |
| 2015 | 0.28 |
| 2014 | 0.18 |
| 2013 | 0.12 |
| 2012 | 0.18 |
| 2011 | 0.08 |
| 2010 | 0.08 |
| 2009 | 0.26 |
| 2008 | 0.48 |
| 2007 | 0.555 |
| 2006 | 0.34 |
| 2005 | 0.31 |
| 2004 | 0.4 |
| 2003 | 0.4 |
| 2002 | 0.4 |
| 2001 | 0.4 |
| 2000 | 0.4 |
| 1999 | 0.37 |
| 1998 | 0.48 |
| 1997 | 0.48 |
| 1996 | 0.48 |
| 1995 | 0.36 |
| 1994 | 0.48 |
| 1993 | 0.36 |
| 1992 | 0.36 |
| 1991 | 0.48 |
| 1990 | 0.48 |
| 1989 | 0.36 |
| 1988 | 0.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
