---
title: "MONEYGRAM INTERNATIONAL INC (MGI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MONEYGRAM INTERNATIONAL INC</td></tr>
    <tr><td>Symbol</td><td>MGI</td></tr>
    <tr><td>Web</td><td><a href="https://global.moneygram.com">global.moneygram.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2007 | 0.2 |
| 2006 | 0.17 |
| 2005 | 0.07 |
| 2004 | 0.02 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
