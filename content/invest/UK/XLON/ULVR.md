---
title: "Unilever (ULVR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Unilever</td></tr>
    <tr><td>Symbol</td><td>ULVR</td></tr>
    <tr><td>Web</td><td><a href="https://www.unilever.com">www.unilever.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 312.0 |
| 2019 | 304.0 |
| 2018 | 285.0 |
| 2017 | 122.0 |
| 2016 | 104.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
