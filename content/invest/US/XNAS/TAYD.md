---
title: "TAYLOR DEVICES (TAYD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TAYLOR DEVICES</td></tr>
    <tr><td>Symbol</td><td>TAYD</td></tr>
    <tr><td>Web</td><td><a href="https://www.taylordevices.com">www.taylordevices.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 1989 | 0.02 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
