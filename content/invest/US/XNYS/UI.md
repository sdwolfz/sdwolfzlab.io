---
title: "UBIQUITI INC (UI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>UBIQUITI INC</td></tr>
    <tr><td>Symbol</td><td>UI</td></tr>
    <tr><td>Web</td><td><a href="https://www.ui.com">www.ui.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.8 |
| 2020 | 1.4 |
| 2019 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
