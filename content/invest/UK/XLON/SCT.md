---
title: "Softcat (SCT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Softcat</td></tr>
    <tr><td>Symbol</td><td>SCT</td></tr>
    <tr><td>Web</td><td><a href="https://www.softcat.com">www.softcat.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 26.4 |
| 2019 | 28.4 |
| 2018 | 27.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
