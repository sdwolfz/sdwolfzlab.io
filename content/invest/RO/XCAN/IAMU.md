---
title: "IAMU SA BLAJ (IAMU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>IAMU SA BLAJ</td></tr>
    <tr><td>Symbol</td><td>IAMU</td></tr>
    <tr><td>Web</td><td><a href="https://www.iamu.ro">www.iamu.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.421046 |
| 2019 | 0.421046 |
| 2018 | 0.31578 |
| 2017 | 0.29 |
| 2016 | 0.17 |
| 2015 | 0.1662 |
| 2014 | 0.14 |
| 2004 | 0.8772 |
| 2003 | 0.3773 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
