---
title: "UNITED PARCEL SERVICE INC (UPS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>UNITED PARCEL SERVICE INC</td></tr>
    <tr><td>Symbol</td><td>UPS</td></tr>
    <tr><td>Web</td><td><a href="https://www.ups.com">www.ups.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.02 |
| 2020 | 4.04 |
| 2019 | 3.84 |
| 2018 | 3.64 |
| 2017 | 3.32 |
| 2016 | 3.12 |
| 2015 | 2.92 |
| 2014 | 2.68 |
| 2013 | 1.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
