---
title: "NORTH AMERICAN CONSTRUCTION GRP LTD (NOA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NORTH AMERICAN CONSTRUCTION GRP LTD</td></tr>
    <tr><td>Symbol</td><td>NOA</td></tr>
    <tr><td>Web</td><td><a href="https://www.nacg.ca">www.nacg.ca</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.119 |
| 2019 | 0.09 |
| 2018 | 0.062 |
| 2017 | 0.063 |
| 2016 | 0.061 |
| 2015 | 0.055 |
| 2014 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
