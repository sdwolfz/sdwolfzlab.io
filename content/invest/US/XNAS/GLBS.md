---
title: "GLOBUS MARITIME LTD (GLBS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GLOBUS MARITIME LTD</td></tr>
    <tr><td>Symbol</td><td>GLBS</td></tr>
    <tr><td>Web</td><td><a href="https://www.globusmaritime.gr">www.globusmaritime.gr</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2012 | 0.245 |
| 2011 | 0.64 |
| 2010 | 0.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
