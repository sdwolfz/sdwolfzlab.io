---
title: "TFI INTERNATIONAL INC (TFII)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TFI INTERNATIONAL INC</td></tr>
    <tr><td>Symbol</td><td>TFII</td></tr>
    <tr><td>Web</td><td><a href="https://www.tfiintl.com">www.tfiintl.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.23 |
| 2020 | 0.805 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
