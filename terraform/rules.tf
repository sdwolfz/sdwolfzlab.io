# ------------------------------------------------------------------------------
# Firewall Rules

resource "cloudflare_ruleset" "block_abusive_countries_ruleset" {
  zone_id     = cloudflare_zone.domain.id
  name        = "Block Abusive Countries"
  description = "Block Abusive Countries"
  kind        = "zone"
  phase       = "http_request_firewall_custom"

  rules {
    action      = "block"
    expression  = <<-EXP
                    (ip.geoip.country eq "AF") or
                    (ip.geoip.country eq "AM") or
                    (ip.geoip.country eq "AZ") or
                    (ip.geoip.country eq "BF") or
                    (ip.geoip.country eq "BI") or
                    (ip.geoip.country eq "BO") or
                    (ip.geoip.country eq "BY") or
                    (ip.geoip.country eq "CD") or
                    (ip.geoip.country eq "CF") or
                    (ip.geoip.country eq "CG") or
                    (ip.geoip.country eq "CN") or
                    (ip.geoip.country eq "CU") or
                    (ip.geoip.country eq "DJ") or
                    (ip.geoip.country eq "DZ") or
                    (ip.geoip.country eq "ER") or
                    (ip.geoip.country eq "ET") or
                    (ip.geoip.country eq "GN") or
                    (ip.geoip.country eq "GQ") or
                    (ip.geoip.country eq "HN") or
                    (ip.geoip.country eq "IN") or
                    (ip.geoip.country eq "IR") or
                    (ip.geoip.country eq "KG") or
                    (ip.geoip.country eq "KP") or
                    (ip.geoip.country eq "KZ") or
                    (ip.geoip.country eq "LA") or
                    (ip.geoip.country eq "LK") or
                    (ip.geoip.country eq "LS") or
                    (ip.geoip.country eq "ML") or
                    (ip.geoip.country eq "MN") or
                    (ip.geoip.country eq "MZ") or
                    (ip.geoip.country eq "NA") or
                    (ip.geoip.country eq "NI") or
                    (ip.geoip.country eq "PK") or
                    (ip.geoip.country eq "QA") or
                    (ip.geoip.country eq "RU") or
                    (ip.geoip.country eq "SD") or
                    (ip.geoip.country eq "SS") or
                    (ip.geoip.country eq "ST") or
                    (ip.geoip.country eq "SV") or
                    (ip.geoip.country eq "SY") or
                    (ip.geoip.country eq "SZ") or
                    (ip.geoip.country eq "TG") or
                    (ip.geoip.country eq "TH") or
                    (ip.geoip.country eq "TJ") or
                    (ip.geoip.country eq "TM") or
                    (ip.geoip.country eq "TZ") or
                    (ip.geoip.country eq "UG") or
                    (ip.geoip.country eq "UZ") or
                    (ip.geoip.country eq "VE") or
                    (ip.geoip.country eq "VN") or
                    (ip.geoip.country eq "ZA") or
                    (ip.geoip.country eq "ZW")
                  EXP
    description = "Block Abusive Countries"
    enabled     = true
  }
}

# ------------------------------------------------------------------------------
# Page Rules

resource "cloudflare_page_rule" "root_rule" {
  zone_id  = cloudflare_zone.domain.id
  target   = format("%s/", local.domain)
  priority = 1
  status   = "active"

  actions {
    forwarding_url {
      status_code = 301
      url         = format("https://www.%s", local.domain)
    }
  }
}

resource "cloudflare_page_rule" "args_rule" {
  zone_id  = cloudflare_zone.domain.id
  target   = format("%s/*", local.domain)
  priority = 2
  status   = "active"

  actions {
    forwarding_url {
      status_code = 301
      url         = format("https://www.%s/$1", local.domain)
    }
  }
}
