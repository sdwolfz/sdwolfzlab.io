---
title: "ACTIVISION BLIZZARD INC (ATVI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ACTIVISION BLIZZARD INC</td></tr>
    <tr><td>Symbol</td><td>ATVI</td></tr>
    <tr><td>Web</td><td><a href="https://www.activisionblizzard.com">www.activisionblizzard.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.47 |
| 2020 | 0.41 |
| 2019 | 0.37 |
| 2018 | 0.34 |
| 2017 | 0.3 |
| 2016 | 0.26 |
| 2015 | 0.23 |
| 2014 | 0.2 |
| 2013 | 0.19 |
| 2012 | 0.18 |
| 2011 | 0.165 |
| 2010 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
