---
title: "BANK OF MARIN BANCORP (BMRC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BANK OF MARIN BANCORP</td></tr>
    <tr><td>Symbol</td><td>BMRC</td></tr>
    <tr><td>Web</td><td><a href="https://www.bankofmarin.com">www.bankofmarin.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.46 |
| 2020 | 0.92 |
| 2019 | 0.8 |
| 2018 | 1.27 |
| 2017 | 1.12 |
| 2016 | 1.02 |
| 2015 | 0.9 |
| 2014 | 0.8 |
| 2013 | 0.73 |
| 2012 | 0.7 |
| 2011 | 0.65 |
| 2010 | 0.61 |
| 2009 | 0.57 |
| 2008 | 0.56 |
| 2007 | 0.511 |
| 2006 | 0.46 |
| 2005 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
