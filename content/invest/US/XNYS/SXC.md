---
title: "SUNCOKE ENERGY INC (SXC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SUNCOKE ENERGY INC</td></tr>
    <tr><td>Symbol</td><td>SXC</td></tr>
    <tr><td>Web</td><td><a href="https://www.suncoke.com">www.suncoke.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 0.24 |
| 2019 | 0.06 |
| 2015 | 0.433 |
| 2014 | 0.058 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
