---
title: "COHERENT INC (COHR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COHERENT INC</td></tr>
    <tr><td>Symbol</td><td>COHR</td></tr>
    <tr><td>Web</td><td><a href="https://www.coherent.com">www.coherent.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2012 | 1.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
