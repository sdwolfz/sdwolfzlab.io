---
title: "SIX FLAGS ENT CORP NEW (SIX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SIX FLAGS ENT CORP NEW</td></tr>
    <tr><td>Symbol</td><td>SIX</td></tr>
    <tr><td>Web</td><td><a href="https://investors.sixflags.com">investors.sixflags.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.25 |
| 2019 | 3.29 |
| 2018 | 3.16 |
| 2017 | 2.62 |
| 2016 | 2.38 |
| 2015 | 2.14 |
| 2014 | 1.93 |
| 2013 | 1.36 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
