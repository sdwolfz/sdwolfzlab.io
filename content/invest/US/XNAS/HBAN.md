---
title: "HUNTINGTON BANCSHARES INC (HBAN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HUNTINGTON BANCSHARES INC</td></tr>
    <tr><td>Symbol</td><td>HBAN</td></tr>
    <tr><td>Web</td><td><a href="https://www.huntington.com">www.huntington.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 0.6 |
| 2019 | 0.58 |
| 2018 | 0.5 |
| 2017 | 0.35 |
| 2016 | 0.29 |
| 2015 | 0.25 |
| 2014 | 0.21 |
| 2013 | 0.19 |
| 2012 | 0.16 |
| 2011 | 0.1 |
| 2010 | 0.04 |
| 2009 | 0.04 |
| 2008 | 0.663 |
| 2007 | 1.06 |
| 2006 | 1.0 |
| 2005 | 0.845 |
| 2004 | 0.75 |
| 2003 | 0.67 |
| 2002 | 0.64 |
| 2001 | 0.72 |
| 2000 | 0.8 |
| 1999 | 0.8 |
| 1998 | 0.8 |
| 1997 | 0.8 |
| 1996 | 0.8 |
| 1995 | 0.8 |
| 1994 | 0.8 |
| 1993 | 0.78 |
| 1992 | 0.76 |
| 1991 | 0.8 |
| 1990 | 0.585 |
| 1989 | 0.74 |
| 1988 | 0.185 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
