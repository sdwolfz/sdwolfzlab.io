---
title: "ACCENTURE PLC (ACN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ACCENTURE PLC</td></tr>
    <tr><td>Symbol</td><td>ACN</td></tr>
    <tr><td>Web</td><td><a href="https://www.accenture.com">www.accenture.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.76 |
| 2020 | 3.28 |
| 2019 | 2.26 |
| 2018 | 2.79 |
| 2017 | 2.54 |
| 2016 | 2.31 |
| 2015 | 2.12 |
| 2014 | 1.95 |
| 2013 | 1.74 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
