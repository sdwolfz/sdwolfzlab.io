---
title: "STAR BULK CARRIERS CORP (SBLK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>STAR BULK CARRIERS CORP</td></tr>
    <tr><td>Symbol</td><td>SBLK</td></tr>
    <tr><td>Web</td><td><a href="https://www.starbulk.com">www.starbulk.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.05 |
| 2019 | 0.05 |
| 2012 | 0.045 |
| 2011 | 0.2 |
| 2010 | 0.2 |
| 2009 | 0.1 |
| 2008 | 0.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
