---
title: "SALEM MEDIA GROUP INC (SALM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SALEM MEDIA GROUP INC</td></tr>
    <tr><td>Symbol</td><td>SALM</td></tr>
    <tr><td>Web</td><td><a href="https://www.salemmedia.com">www.salemmedia.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.025 |
| 2019 | 0.22 |
| 2018 | 0.26 |
| 2017 | 0.26 |
| 2016 | 0.26 |
| 2015 | 0.26 |
| 2014 | 0.245 |
| 2013 | 0.208 |
| 2012 | 0.14 |
| 2010 | 0.2 |
| 2007 | 0.42 |
| 2006 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
