---
title: " (NNN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>NNN</td></tr>
    <tr><td>Web</td><td><a href="https://www.nnnreit.com">www.nnnreit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.04 |
| 2020 | 2.07 |
| 2019 | 2.03 |
| 2018 | 1.95 |
| 2017 | 1.86 |
| 2016 | 1.78 |
| 2015 | 1.71 |
| 2014 | 1.65 |
| 2013 | 0.81 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
