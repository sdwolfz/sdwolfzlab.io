---
title: "Bypass screenshot security policy"
date: 2020-10-23T00:12:05Z
type: posts
slug: bypass-screenshot-security-policy
series: ["Android"]
tags: ["android", "linux"]
---

1. [Enable 'Developer options' on your phone](/snippets/enable-developer-options-on-your-phone/)

2. Install `scrcpy`:

```sh
sudo pacman -S scrcpy
```

3. Plug in your phone and open the app.

4. Start recording:

```sh
scrcpy -r file.mp4
```
