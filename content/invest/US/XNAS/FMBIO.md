---
title: " (FMBIO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>FMBIO</td></tr>
    <tr><td>Web</td><td><a href="https://www.firstmidwest.com">www.firstmidwest.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.875 |
| 2020 | 0.715 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
