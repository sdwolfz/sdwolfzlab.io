---
title: "FLUOR CORP (FLR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FLUOR CORP</td></tr>
    <tr><td>Symbol</td><td>FLR</td></tr>
    <tr><td>Web</td><td><a href="https://www.fluor.com">www.fluor.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.1 |
| 2019 | 0.73 |
| 2018 | 0.84 |
| 2017 | 0.84 |
| 2016 | 0.84 |
| 2015 | 0.84 |
| 2014 | 0.84 |
| 2013 | 0.32 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
