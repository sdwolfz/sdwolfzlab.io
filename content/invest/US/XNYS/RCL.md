---
title: "ROYAL CARIBBEAN GROUP (RCL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ROYAL CARIBBEAN GROUP</td></tr>
    <tr><td>Symbol</td><td>RCL</td></tr>
    <tr><td>Web</td><td><a href="https://www.rclcorporate.com">www.rclcorporate.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.78 |
| 2019 | 2.96 |
| 2018 | 2.6 |
| 2017 | 2.16 |
| 2016 | 1.71 |
| 2015 | 1.35 |
| 2014 | 1.1 |
| 2013 | 0.62 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
