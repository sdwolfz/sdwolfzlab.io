---
title: "GENPACT LIMITED (G)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GENPACT LIMITED</td></tr>
    <tr><td>Symbol</td><td>G</td></tr>
    <tr><td>Web</td><td><a href="https://www.genpact.com">www.genpact.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.216 |
| 2020 | 0.388 |
| 2019 | 0.34 |
| 2018 | 0.3 |
| 2017 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
