---
title: "ALAMO GROUP INC (ALG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ALAMO GROUP INC</td></tr>
    <tr><td>Symbol</td><td>ALG</td></tr>
    <tr><td>Web</td><td><a href="https://www.alamo-group.com">www.alamo-group.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 0.52 |
| 2019 | 0.48 |
| 2018 | 0.44 |
| 2017 | 0.4 |
| 2016 | 0.36 |
| 2015 | 0.32 |
| 2014 | 0.28 |
| 2013 | 0.14 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
