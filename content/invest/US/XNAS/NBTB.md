---
title: "NBT BANCORP INC (NBTB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NBT BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>NBTB</td></tr>
    <tr><td>Web</td><td><a href="https://www.nbtbancorp.com">www.nbtbancorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.54 |
| 2020 | 1.08 |
| 2019 | 1.05 |
| 2018 | 0.99 |
| 2017 | 0.92 |
| 2016 | 0.9 |
| 2015 | 0.87 |
| 2014 | 0.84 |
| 2013 | 0.81 |
| 2012 | 0.8 |
| 2011 | 0.8 |
| 2010 | 0.8 |
| 2009 | 0.8 |
| 2008 | 0.8 |
| 2007 | 0.79 |
| 2006 | 0.76 |
| 2005 | 0.76 |
| 2004 | 0.74 |
| 2003 | 0.68 |
| 2002 | 0.68 |
| 2001 | 0.68 |
| 2000 | 0.68 |
| 1999 | 0.51 |
| 1998 | 0.567 |
| 1997 | 0.47 |
| 1996 | 0.39 |
| 1995 | 0.36 |
| 1994 | 0.345 |
| 1993 | 0.335 |
| 1992 | 0.213 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
