---
title: "CAPITOL FEDERAL FINANCIAL INC NEW (CFFN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CAPITOL FEDERAL FINANCIAL INC NEW</td></tr>
    <tr><td>Symbol</td><td>CFFN</td></tr>
    <tr><td>Web</td><td><a href="https://www.capfed.com">www.capfed.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.17 |
| 2020 | 0.47 |
| 2019 | 0.93 |
| 2018 | 0.98 |
| 2017 | 0.88 |
| 2016 | 0.88 |
| 2015 | 0.84 |
| 2014 | 0.81 |
| 2013 | 0.73 |
| 2012 | 1.0 |
| 2011 | 1.0 |
| 2010 | 2.3 |
| 2009 | 2.29 |
| 2008 | 2.11 |
| 2007 | 2.0 |
| 2006 | 2.09 |
| 2005 | 2.3 |
| 2004 | 2.0 |
| 2003 | 2.0 |
| 2002 | 2.0 |
| 2001 | 0.62 |
| 2000 | 0.46 |
| 1999 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
