---
title: "Renewables Infrastructure Group (TRIG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Renewables Infrastructure Group</td></tr>
    <tr><td>Symbol</td><td>TRIG</td></tr>
    <tr><td>Web</td><td><a href="https://www.trig-ltd.com">www.trig-ltd.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.76 |
| 2019 | 6.61 |
| 2018 | 6.64 |
| 2017 | 6.4 |
| 2016 | 6.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
