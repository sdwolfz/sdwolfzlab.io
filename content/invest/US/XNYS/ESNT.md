---
title: "ESSENT GROUP LTD (ESNT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ESSENT GROUP LTD</td></tr>
    <tr><td>Symbol</td><td>ESNT</td></tr>
    <tr><td>Web</td><td><a href="https://www.essentgroup.com">www.essentgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.16 |
| 2020 | 0.64 |
| 2019 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
