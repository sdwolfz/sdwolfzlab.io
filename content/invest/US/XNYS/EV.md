---
title: "EATON VANCE CORP (EV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>EATON VANCE CORP</td></tr>
    <tr><td>Symbol</td><td>EV</td></tr>
    <tr><td>Web</td><td><a href="http://www.eatonvance.com">www.eatonvance.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 5.75 |
| 2019 | 1.425 |
| 2018 | 1.28 |
| 2017 | 1.43 |
| 2016 | 1.075 |
| 2015 | 1.015 |
| 2014 | 0.91 |
| 2013 | 0.42 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
