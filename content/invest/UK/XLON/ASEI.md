---
title: "Abstd Equityinc (ASEI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Abstd Equityinc</td></tr>
    <tr><td>Symbol</td><td>ASEI</td></tr>
    <tr><td>Web</td><td><a href="https://www.sli.co.uk">www.sli.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 20.6 |
| 2019 | 20.5 |
| 2018 | 19.2 |
| 2017 | 17.1 |
| 2016 | 15.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
