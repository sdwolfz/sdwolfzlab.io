---
title: "Test for Emacs compiler warnings"
date: 2022-09-10T10:55:37Z
type: posts
slug: test-for-emacs-compiler-warnings
tags: ["emacs", "linux", "make", "git", "CI"]
---

## Setup

Your `.gitignore`:

```gitignore
*.elc
```

A `Makefile` containing:

```makefile
.PHONY: compile
compile:
	@emacs -q -batch \
	  --eval '(setq byte-compile-error-on-warn t)' \
	  -f batch-byte-compile \
	  *.el
```

> The first indentation is a tab, the following two are spaces. Makefiles just
> work this way for reasons beyond human comprehension.

## Run

Executing `make compile` will compile all your `.el` files and error in case
there are any warnings.

## CI

{{< tabgroup align="right" style="code" >}}
  {{< tab name="GitLab" >}}
  ```yml
  compile:
    image: silex/emacs:${EMACS_VERSION}-alpine-ci
    parallel:
      matrix:
        - EMACS_VERSION:
            - 26.1
            - 26.2
            - 26.3
            - 27.1
            - 27.2
            - 28.1
            - master
    script:
      - make compile
  ```
  {{< /tab >}}

  {{< tab name="GitHub" >}}
  ```yml
  name: CI

  on: [push, pull_request]

  jobs:
    compile:
      name: 'Compile code and check for warnings'
      runs-on: debian-latest
      strategy:
        matrix:
          emacs_version:
            - 26.1
            - 26.2
            - 26.3
            - 27.1
            - 27.2
            - 28.1
            - snapshot
      steps:
        - name: 'Install Emacs'
          uses: purcell/setup-emacs@master
          with:
            version: ${{ matrix.emacs_version }}
        - name: 'Checkout code'
          uses: actions/checkout@v2
        - name: 'Byte compile and check for warnings'
          run: make compile
  ```
  {{< /tab >}}
{{< /tabgroup >}}
