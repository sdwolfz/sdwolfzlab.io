---
title: "PEOPLES FINANCIAL SERVICES GROUP (PFIS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PEOPLES FINANCIAL SERVICES GROUP</td></tr>
    <tr><td>Symbol</td><td>PFIS</td></tr>
    <tr><td>Web</td><td><a href="https://www.psbt.com">www.psbt.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.74 |
| 2020 | 1.44 |
| 2019 | 1.37 |
| 2018 | 1.31 |
| 2017 | 1.26 |
| 2016 | 1.24 |
| 2015 | 1.24 |
| 2014 | 1.24 |
| 2013 | 0.92 |
| 2012 | 0.86 |
| 2011 | 0.8 |
| 2010 | 0.79 |
| 2009 | 0.76 |
| 2008 | 0.76 |
| 2007 | 0.76 |
| 2006 | 0.76 |
| 2005 | 1.76 |
| 2004 | 0.73 |
| 2003 | 0.81 |
| 2002 | 0.88 |
| 2001 | 0.72 |
| 2000 | 0.62 |
| 1999 | 3.52 |
| 1998 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
