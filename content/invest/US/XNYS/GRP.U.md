---
title: "GRANITE REAL ESTATE INVESTMENT TR STAPLED UNIT (1 ORD & 1 UNIT) (GRP.U)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GRANITE REAL ESTATE INVESTMENT TR STAPLED UNIT (1 ORD & 1 UNIT)</td></tr>
    <tr><td>Symbol</td><td>GRP.U</td></tr>
    <tr><td>Web</td><td><a href="http://www.graniterealestate.com">www.graniterealestate.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.098 |
| 2019 | 2.112 |
| 2018 | 3.017 |
| 2017 | 2.359 |
| 2016 | 1.812 |
| 2015 | 2.019 |
| 2014 | 2.205 |
| 2013 | 1.058 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
