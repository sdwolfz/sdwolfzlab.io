---
title: "RUHNN HOLDING LIMITED SPON ADS EACH REP 5 ORD SHS CL A (RUHN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>RUHNN HOLDING LIMITED SPON ADS EACH REP 5 ORD SHS CL A</td></tr>
    <tr><td>Symbol</td><td>RUHN</td></tr>
    <tr><td>Web</td><td><a href="http://www.ruhnn.com">www.ruhnn.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
