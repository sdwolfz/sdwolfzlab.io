---
title: " (BUD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BUD</td></tr>
    <tr><td>Web</td><td><a href="https://www.ab-inbev.com">www.ab-inbev.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.447 |
| 2020 | 0.418 |
| 2019 | 1.491 |
| 2018 | 2.528 |
| 2017 | 3.212 |
| 2016 | 3.188 |
| 2015 | 3.224 |
| 2014 | 2.673 |
| 2013 | 2.512 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
