---
title: "GRAHAM HOLDINGS CO (GHC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GRAHAM HOLDINGS CO</td></tr>
    <tr><td>Symbol</td><td>GHC</td></tr>
    <tr><td>Web</td><td><a href="https://www.ghco.com">www.ghco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 3.02 |
| 2020 | 5.8 |
| 2019 | 5.56 |
| 2018 | 5.32 |
| 2017 | 5.08 |
| 2016 | 4.84 |
| 2015 | 9.1 |
| 2014 | 10.2 |
| 2012 | 2.45 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
