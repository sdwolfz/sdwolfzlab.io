---
title: "Git init without branch hint"
date: 2023-01-14T13:16:34Z
type: posts
slug: git-init-without-branch-hint
tags: ["git", "shell"]
---

If you run `git init` you will get this yellow hint:

```
hint: Using 'master' as the name for the initial branch. This default branch name
hint: is subject to change. To configure the initial branch name to use in all
hint: of your new repositories, which will suppress this warning, call:
hint:
hint:   git config --global init.defaultBranch <name>
hint:
hint: Names commonly chosen instead of 'master' are 'main', 'trunk' and
hint: 'development'. The just-created branch can be renamed via this command:
hint:
hint:   git branch -m <name>
```

This is annoying, pointless, and a complete waste of the git core developer's
time as well as ours.

## Solution

Remove the hint with:

```sh
git config --global init.defaultBranch master
```
