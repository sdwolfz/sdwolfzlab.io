---
title: "MOLDOVA SA VASLUI (MODY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>MOLDOVA SA VASLUI</td></tr>
    <tr><td>Symbol</td><td>MODY</td></tr>
    <tr><td>Web</td><td><a href="https://www.moldovasa.ro">www.moldovasa.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.0353 |
| 2019 | 0.0419 |
| 2018 | 0.0461 |
| 2015 | 0.034975 |
| 2014 | 0.042657 |
| 2013 | 0.0462 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
