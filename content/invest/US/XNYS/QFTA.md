---
title: "QUANTUM FINTECH ACQUISITION CORP (QFTA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>QUANTUM FINTECH ACQUISITION CORP</td></tr>
    <tr><td>Symbol</td><td>QFTA</td></tr>
    <tr><td>Web</td><td><a href="https://www.qftacorp.com">www.qftacorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
