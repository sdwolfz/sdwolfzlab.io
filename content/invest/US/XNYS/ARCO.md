---
title: "ARCOS DORADOS HOLDINGS INC (ARCO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ARCOS DORADOS HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>ARCO</td></tr>
    <tr><td>Web</td><td><a href="https://www.arcosdorados.com">www.arcosdorados.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.11 |
| 2019 | 0.11 |
| 2018 | 0.1 |
| 2014 | 0.24 |
| 2013 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
