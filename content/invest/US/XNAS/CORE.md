---
title: "CORE-MARK HLDG CO INC (CORE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CORE-MARK HLDG CO INC</td></tr>
    <tr><td>Symbol</td><td>CORE</td></tr>
    <tr><td>Web</td><td><a href="https://www.core-mark.com">www.core-mark.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.26 |
| 2020 | 0.49 |
| 2019 | 0.45 |
| 2018 | 0.41 |
| 2017 | 0.37 |
| 2016 | 0.49 |
| 2015 | 0.55 |
| 2014 | 0.68 |
| 2013 | 0.6 |
| 2012 | 0.89 |
| 2011 | 0.17 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
