---
title: "OVERSEAS SHIPHOLDING GROUP INC (OSG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>OVERSEAS SHIPHOLDING GROUP INC</td></tr>
    <tr><td>Symbol</td><td>OSG</td></tr>
    <tr><td>Web</td><td><a href="https://www.osg.com">www.osg.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.48 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
