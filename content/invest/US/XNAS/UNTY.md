---
title: "UNITY BANCORP INC (UNTY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>UNITY BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>UNTY</td></tr>
    <tr><td>Web</td><td><a href="https://www.unitybank.com">www.unitybank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.08 |
| 2020 | 0.32 |
| 2019 | 0.31 |
| 2018 | 0.27 |
| 2017 | 0.23 |
| 2016 | 0.18 |
| 2015 | 0.14 |
| 2014 | 0.1 |
| 2013 | 0.03 |
| 2008 | 0.15 |
| 2007 | 0.2 |
| 2006 | 0.2 |
| 2005 | 0.18 |
| 2004 | 0.15 |
| 2003 | 0.03 |
| 1999 | 0.23 |
| 1998 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
