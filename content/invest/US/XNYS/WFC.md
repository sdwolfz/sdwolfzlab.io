---
title: "WELLS FARGO & COMPANY (WFC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>WELLS FARGO & COMPANY</td></tr>
    <tr><td>Symbol</td><td>WFC</td></tr>
    <tr><td>Web</td><td><a href="https://www.wellsfargo.com">www.wellsfargo.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 1.22 |
| 2019 | 1.92 |
| 2018 | 1.64 |
| 2017 | 1.54 |
| 2016 | 1.515 |
| 2015 | 1.475 |
| 2014 | 1.35 |
| 2013 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
