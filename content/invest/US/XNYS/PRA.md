---
title: "PROASSURANCE CORP (PRA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PROASSURANCE CORP</td></tr>
    <tr><td>Symbol</td><td>PRA</td></tr>
    <tr><td>Web</td><td><a href="https://www.proassurance.com">www.proassurance.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.05 |
| 2020 | 0.46 |
| 2019 | 1.24 |
| 2018 | 1.74 |
| 2017 | 5.93 |
| 2016 | 5.93 |
| 2015 | 2.24 |
| 2014 | 0.9 |
| 2013 | 0.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
