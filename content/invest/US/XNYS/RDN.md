---
title: "RADIAN GROUP INC (RDN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>RADIAN GROUP INC</td></tr>
    <tr><td>Symbol</td><td>RDN</td></tr>
    <tr><td>Web</td><td><a href="https://www.radian.com">www.radian.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.265 |
| 2020 | 0.5 |
| 2019 | 0.008 |
| 2018 | 0.008 |
| 2017 | 0.008 |
| 2016 | 0.008 |
| 2015 | 0.008 |
| 2014 | 0.008 |
| 2013 | 0.004 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
