---
title: "SANFILIPPO(JOHN B)& SON INC (JBSS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SANFILIPPO(JOHN B)& SON INC</td></tr>
    <tr><td>Symbol</td><td>JBSS</td></tr>
    <tr><td>Web</td><td><a href="https://www.jbssinc.com">www.jbssinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.5 |
| 2020 | 3.5 |
| 2019 | 5.0 |
| 2018 | 2.55 |
| 2017 | 2.5 |
| 2016 | 5.0 |
| 2015 | 2.0 |
| 2014 | 1.5 |
| 2013 | 1.5 |
| 2012 | 1.0 |
| 1993 | 0.05 |
| 1992 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
