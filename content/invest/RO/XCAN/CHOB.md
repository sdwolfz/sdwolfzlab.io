---
title: "CHIMCOMPLEX BORZESTI SA ONESTI (CHOB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>CHIMCOMPLEX BORZESTI SA ONESTI</td></tr>
    <tr><td>Symbol</td><td>CHOB</td></tr>
    <tr><td>Web</td><td><a href="https://www.chimcomplex.ro">www.chimcomplex.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.005099 |
| 2016 | 0.044487 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
