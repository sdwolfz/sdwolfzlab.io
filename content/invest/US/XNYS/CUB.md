---
title: "CUBIC CORP (CUB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CUBIC CORP</td></tr>
    <tr><td>Symbol</td><td>CUB</td></tr>
    <tr><td>Web</td><td><a href="https://www.cubic.com">www.cubic.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.135 |
| 2020 | 0.27 |
| 2019 | 0.27 |
| 2018 | 0.27 |
| 2017 | 0.27 |
| 2016 | 0.27 |
| 2015 | 0.27 |
| 2014 | 0.24 |
| 2013 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
