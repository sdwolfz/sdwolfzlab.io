---
title: "Ep Global (EPG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Ep Global</td></tr>
    <tr><td>Symbol</td><td>EPG</td></tr>
    <tr><td>Web</td><td><a href="https://www.edinburghpartners.com">www.edinburghpartners.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.0 |
| 2019 | 7.5 |
| 2018 | 6.5 |
| 2017 | 5.3 |
| 2016 | 5.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
