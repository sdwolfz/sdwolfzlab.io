---
title: "Bh Global (BHGG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Bh Global</td></tr>
    <tr><td>Symbol</td><td>BHGG</td></tr>
    <tr><td>Web</td><td><a href="https://www.bhglobal.com">www.bhglobal.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
