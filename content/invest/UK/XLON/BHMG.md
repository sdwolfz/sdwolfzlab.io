---
title: "Bh Macro Gbp (BHMG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Bh Macro Gbp</td></tr>
    <tr><td>Symbol</td><td>BHMG</td></tr>
    <tr><td>Web</td><td><a href="https://www.bhmacro.com">www.bhmacro.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
