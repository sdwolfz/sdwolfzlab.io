---
title: " (MFGP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>MFGP</td></tr>
    <tr><td>Web</td><td><a href="https://www.microfocus.com">www.microfocus.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.135 |
| 2020 | 0.563 |
| 2019 | 1.241 |
| 2018 | 0.88 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
