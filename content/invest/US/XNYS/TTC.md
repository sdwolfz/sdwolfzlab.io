---
title: "TORO CO (TTC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TORO CO</td></tr>
    <tr><td>Symbol</td><td>TTC</td></tr>
    <tr><td>Web</td><td><a href="https://www.thetorocompany.com">www.thetorocompany.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.262 |
| 2020 | 1.012 |
| 2019 | 0.925 |
| 2018 | 0.825 |
| 2017 | 0.725 |
| 2016 | 0.775 |
| 2015 | 1.05 |
| 2014 | 0.85 |
| 2013 | 0.48 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
