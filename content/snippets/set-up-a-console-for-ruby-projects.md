---
title: "Set up a console for Ruby projects"
date: 2020-10-13T22:41:29Z
type: posts
slug: set-up-a-console-for-ruby-projects
tags: ["ruby", "development", "productivity"]
---

## With IRB

1. Add `irb` to your `Gemfile` (yes you need to do this)!

```ruby
# frozen_string_literal: true

gem 'irb'
```

2. Run `bundle install` of course!

3. Create a `./bin/console` file containing:

```ruby
#!/usr/bin/env ruby
# frozen_string_literal: true

require 'rubygems'
require 'bundler/setup'
require 'irb'

require_relative '../config/environment.rb'

IRB.start
```

4. Be sure you require all your project files in `config/environment.rb`.

5. Make your file executable:

```sh
chmod +x ./bin/console
```

6. Now you can run it like any other program:

```sh
./bin/console

# irb(main):001:0>
```

7. **Optional** but fancy: create a `Makefile` with this content so you can
   execute `make console` instead.

```Makefile
.PHONY: console
console:
	@./bin/console
```

8. **Pro Tip**: use `load 'path/to/file.rb'` to reload file instead of
   restarting the shell on every edit.

## With Pry

Step 1. becomes:

```ruby
# frozen_string_literal: true

gem 'irb'
gem 'pry-byebug'
```

Yes, you always need `irb` in your `Gemfile`!

Step 3. becomes:

```ruby
#!/usr/bin/env ruby
# frozen_string_literal: true

require 'rubygems'
require 'bundler/setup'
require 'pry'

require_relative '../config/environment.rb'

Pry.start
```

See also: [How to Use: pry-byebug](/snippets/how-to-use-pry-byebug/)
