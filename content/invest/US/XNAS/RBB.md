---
title: "RBB BANCORP (RBB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>RBB BANCORP</td></tr>
    <tr><td>Symbol</td><td>RBB</td></tr>
    <tr><td>Web</td><td><a href="https://www.royalbusinessbankusa.com">www.royalbusinessbankusa.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 0.33 |
| 2019 | 0.4 |
| 2018 | 0.35 |
| 2017 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
