---
title: " (UDR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>UDR</td></tr>
    <tr><td>Web</td><td><a href="https://www.udr.com">www.udr.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.723 |
| 2020 | 1.423 |
| 2019 | 1.351 |
| 2018 | 1.276 |
| 2017 | 1.225 |
| 2016 | 1.163 |
| 2015 | 1.094 |
| 2014 | 1.015 |
| 2013 | 0.47 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
