---
title: "GENCOR INDS INC (GENC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GENCOR INDS INC</td></tr>
    <tr><td>Symbol</td><td>GENC</td></tr>
    <tr><td>Web</td><td><a href="https://www.gencor.com">www.gencor.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 1995 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
