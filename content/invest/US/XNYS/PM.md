---
title: "PHILIP MORRIS INTERNATIONAL INC (PM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PHILIP MORRIS INTERNATIONAL INC</td></tr>
    <tr><td>Symbol</td><td>PM</td></tr>
    <tr><td>Web</td><td><a href="https://www.pmi.com">www.pmi.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.2 |
| 2020 | 4.74 |
| 2019 | 4.62 |
| 2018 | 4.49 |
| 2017 | 4.22 |
| 2016 | 4.12 |
| 2015 | 4.04 |
| 2014 | 3.88 |
| 2013 | 2.73 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
