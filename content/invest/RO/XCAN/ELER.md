---
title: "ELEROM SA ROMAN (ELER)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>ELEROM SA ROMAN</td></tr>
    <tr><td>Symbol</td><td>ELER</td></tr>
    <tr><td>Web</td><td><a href="https://www.elerom.ro">www.elerom.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
