---
title: "CONEX PRAHOVA SA BUCURESTI (COLK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>CONEX PRAHOVA SA BUCURESTI</td></tr>
    <tr><td>Symbol</td><td>COLK</td></tr>
    <tr><td>Web</td><td><a href="https://sites.google.com">sites.google.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2007 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
