---
title: " (IRCP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>IRCP</td></tr>
    <tr><td>Web</td><td><a href="https://www.irsacp.com.ar">www.irsacp.com.ar</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 3.861 |
| 2019 | 0.316 |
| 2018 | 0.469 |
| 2017 | 1.874 |
| 2016 | 0.766 |
| 2015 | 2.018 |
| 2014 | 0.939 |
| 2013 | 1.577 |
| 2012 | 2.08 |
| 2011 | 1.899 |
| 2010 | 0.909 |
| 2009 | 0.731 |
| 2008 | 0.892 |
| 2007 | 0.89 |
| 2006 | 0.762 |
| 2005 | 0.475 |
| 2004 | 0.292 |
| 2003 | 0.179 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
