---
title: "FARMER BROS CO (FARM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FARMER BROS CO</td></tr>
    <tr><td>Symbol</td><td>FARM</td></tr>
    <tr><td>Web</td><td><a href="https://www.farmerbros.com">www.farmerbros.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2011 | 0.06 |
| 2010 | 0.46 |
| 2009 | 0.46 |
| 2008 | 0.46 |
| 2007 | 0.445 |
| 2006 | 0.425 |
| 2005 | 0.405 |
| 2004 | 2.095 |
| 2003 | 3.65 |
| 2002 | 3.45 |
| 2001 | 3.25 |
| 2000 | 3.05 |
| 1999 | 2.85 |
| 1998 | 2.65 |
| 1997 | 3.0 |
| 1996 | 2.25 |
| 1995 | 2.0 |
| 1994 | 2.0 |
| 1993 | 1.85 |
| 1992 | 1.65 |
| 1991 | 1.45 |
| 1990 | 1.3 |
| 1989 | 1.15 |
| 1988 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
