---
title: "HERMAN MILLER INC (MLHR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HERMAN MILLER INC</td></tr>
    <tr><td>Symbol</td><td>MLHR</td></tr>
    <tr><td>Web</td><td><a href="https://www.hermanmiller.com">www.hermanmiller.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.375 |
| 2020 | 0.397 |
| 2019 | 0.815 |
| 2018 | 0.755 |
| 2017 | 0.7 |
| 2016 | 0.635 |
| 2015 | 0.575 |
| 2014 | 0.56 |
| 2013 | 0.5 |
| 2012 | 0.224 |
| 2011 | 0.088 |
| 2010 | 0.088 |
| 2009 | 0.154 |
| 2008 | 0.352 |
| 2007 | 0.344 |
| 2006 | 0.32 |
| 2005 | 0.29 |
| 2004 | 0.254 |
| 2003 | 0.145 |
| 2002 | 0.145 |
| 2001 | 0.145 |
| 2000 | 0.145 |
| 1999 | 0.145 |
| 1998 | 0.181 |
| 1997 | 0.348 |
| 1996 | 0.52 |
| 1995 | 0.52 |
| 1994 | 0.52 |
| 1993 | 0.52 |
| 1992 | 0.39 |
| 1991 | 0.52 |
| 1990 | 0.52 |
| 1989 | 0.52 |
| 1988 | 0.13 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
