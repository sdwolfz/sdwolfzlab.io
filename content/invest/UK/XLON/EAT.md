---
title: "European Assets Trust (EAT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>European Assets Trust</td></tr>
    <tr><td>Symbol</td><td>EAT</td></tr>
    <tr><td>Web</td><td><a href="https://www.bmogam.com">www.bmogam.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 7.02 |
| 2019 | 6.8 |
| 2018 | 9.3 |
| 2017 | 8.22 |
| 2016 | 9.43 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
