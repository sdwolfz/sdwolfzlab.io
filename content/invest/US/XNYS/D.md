---
title: "DOMINION ENERGY INC (D)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DOMINION ENERGY INC</td></tr>
    <tr><td>Symbol</td><td>D</td></tr>
    <tr><td>Web</td><td><a href="https://www.dominionenergy.com">www.dominionenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.26 |
| 2020 | 3.45 |
| 2019 | 3.672 |
| 2018 | 3.34 |
| 2017 | 3.035 |
| 2016 | 2.8 |
| 2015 | 2.588 |
| 2014 | 2.4 |
| 2013 | 1.126 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
