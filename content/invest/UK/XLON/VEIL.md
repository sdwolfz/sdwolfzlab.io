---
title: "Vietnam Enterprise Investments (VEIL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Vietnam Enterprise Investments</td></tr>
    <tr><td>Symbol</td><td>VEIL</td></tr>
    <tr><td>Web</td><td><a href="https://www.veil-dragoncapital.com">www.veil-dragoncapital.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
