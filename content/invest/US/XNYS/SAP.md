---
title: " (SAP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SAP</td></tr>
    <tr><td>Web</td><td><a href="https://www.sap.com">www.sap.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.582 |
| 2020 | 1.246 |
| 2019 | 1.203 |
| 2018 | 1.188 |
| 2017 | 0.98 |
| 2016 | 0.931 |
| 2015 | 1.221 |
| 2014 | 1.98 |
| 2013 | 0.798 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
