---
title: "DELUXE CORP (DLX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DELUXE CORP</td></tr>
    <tr><td>Symbol</td><td>DLX</td></tr>
    <tr><td>Web</td><td><a href="https://www.deluxe.com">www.deluxe.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.6 |
| 2020 | 1.2 |
| 2019 | 1.2 |
| 2018 | 1.2 |
| 2017 | 1.2 |
| 2016 | 1.2 |
| 2015 | 1.2 |
| 2014 | 1.15 |
| 2013 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
