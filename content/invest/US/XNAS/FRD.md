---
title: "FRIEDMAN INDUSTRIES INC (FRD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FRIEDMAN INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>FRD</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.04 |
| 2020 | 0.08 |
| 2019 | 0.16 |
| 2018 | 0.12 |
| 2017 | 0.04 |
| 2016 | 0.04 |
| 2015 | 0.05 |
| 2014 | 0.08 |
| 2013 | 0.37 |
| 2012 | 1.02 |
| 2011 | 0.48 |
| 2010 | 0.64 |
| 2009 | 0.21 |
| 2008 | 0.31 |
| 2007 | 0.34 |
| 2006 | 0.32 |
| 2005 | 0.32 |
| 2004 | 0.17 |
| 2003 | 0.11 |
| 2002 | 0.08 |
| 2001 | 0.15 |
| 2000 | 0.13 |
| 1999 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
