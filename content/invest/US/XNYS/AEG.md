---
title: "AEGON NV (AEG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AEGON NV</td></tr>
    <tr><td>Symbol</td><td>AEG</td></tr>
    <tr><td>Web</td><td><a href="https://www.aegon.com">www.aegon.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.062 |
| 2020 | 0.218 |
| 2019 | 0.283 |
| 2018 | 0.278 |
| 2017 | 0.247 |
| 2016 | 0.247 |
| 2015 | 0.229 |
| 2014 | 0.253 |
| 2013 | 0.125 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
