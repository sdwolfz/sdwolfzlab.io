---
title: "ARTECA SA JILAVA (ARJI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>ARTECA SA JILAVA</td></tr>
    <tr><td>Symbol</td><td>ARJI</td></tr>
    <tr><td>Web</td><td><a href="https://www.arteca.ro">www.arteca.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.315 |
| 2019 | 0.21 |
| 2018 | 0.21 |
| 2017 | 0.28 |
| 2016 | 0.11 |
| 2015 | 0.0696 |
| 2014 | 0.1 |
| 2013 | 0.116 |
| 2012 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
