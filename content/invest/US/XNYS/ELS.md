---
title: " (ELS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ELS</td></tr>
    <tr><td>Web</td><td><a href="https://www.equitylifestyleproperties.com">www.equitylifestyleproperties.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.363 |
| 2020 | 1.372 |
| 2019 | 1.838 |
| 2018 | 2.2 |
| 2017 | 1.952 |
| 2016 | 1.7 |
| 2015 | 1.5 |
| 2014 | 1.3 |
| 2013 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
