---
title: " (MTR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>MTR</td></tr>
    <tr><td>Web</td><td><a href="https://mtr.investorhq.businesswire.com">mtr.investorhq.businesswire.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.293 |
| 2019 | 0.909 |
| 2018 | 1.191 |
| 2017 | 1.512 |
| 2016 | 0.646 |
| 2015 | 1.031 |
| 2014 | 3.507 |
| 2013 | 0.977 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
