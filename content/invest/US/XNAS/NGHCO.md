---
title: "NATIONAL GENERAL HLDGS CO DEP SHS REP 1/40 SHS NON CUM PFD SER B (NGHCO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NATIONAL GENERAL HLDGS CO DEP SHS REP 1/40 SHS NON CUM PFD SER B</td></tr>
    <tr><td>Symbol</td><td>NGHCO</td></tr>
    <tr><td>Web</td><td><a href="http://www.nationalgeneral.com">www.nationalgeneral.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.875 |
| 2019 | 1.875 |
| 2018 | 1.875 |
| 2017 | 1.875 |
| 2016 | 1.875 |
| 2015 | 1.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
