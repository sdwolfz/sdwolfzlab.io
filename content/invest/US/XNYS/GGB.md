---
title: " (GGB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>GGB</td></tr>
    <tr><td>Web</td><td><a href="https://www.gerdau.com">www.gerdau.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.018 |
| 2020 | 0.053 |
| 2019 | 0.067 |
| 2018 | 0.072 |
| 2017 | 0.012 |
| 2016 | 0.012 |
| 2015 | 0.036 |
| 2014 | 0.052 |
| 2013 | 0.019 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
