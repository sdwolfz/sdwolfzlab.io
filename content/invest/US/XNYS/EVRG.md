---
title: "EVERGY INC (EVRG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>EVERGY INC</td></tr>
    <tr><td>Symbol</td><td>EVRG</td></tr>
    <tr><td>Web</td><td><a href="https://investors.evergy.com">investors.evergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.07 |
| 2020 | 2.05 |
| 2019 | 1.93 |
| 2018 | 1.335 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
