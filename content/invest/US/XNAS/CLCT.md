---
title: "COLLECTORS UNIVERSE INC (CLCT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COLLECTORS UNIVERSE INC</td></tr>
    <tr><td>Symbol</td><td>CLCT</td></tr>
    <tr><td>Web</td><td><a href="http://www.collectorsuniverse.com">www.collectorsuniverse.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.7 |
| 2019 | 0.7 |
| 2018 | 0.7 |
| 2017 | 1.4 |
| 2016 | 1.4 |
| 2015 | 1.4 |
| 2014 | 1.3 |
| 2013 | 1.3 |
| 2012 | 1.3 |
| 2011 | 1.3 |
| 2010 | 1.175 |
| 2009 | 0.25 |
| 2008 | 0.75 |
| 2007 | 0.62 |
| 2006 | 0.36 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
