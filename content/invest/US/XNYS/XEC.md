---
title: "CIMAREX ENERGY CO (XEC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CIMAREX ENERGY CO</td></tr>
    <tr><td>Symbol</td><td>XEC</td></tr>
    <tr><td>Web</td><td><a href="https://www.cimarex.com">www.cimarex.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.49 |
| 2020 | 0.86 |
| 2019 | 0.78 |
| 2018 | 0.58 |
| 2017 | 0.32 |
| 2016 | 0.4 |
| 2015 | 0.64 |
| 2014 | 0.62 |
| 2013 | 0.28 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
