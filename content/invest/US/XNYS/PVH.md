---
title: "PVH CORPORATION (PVH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PVH CORPORATION</td></tr>
    <tr><td>Symbol</td><td>PVH</td></tr>
    <tr><td>Web</td><td><a href="https://www.pvh.com">www.pvh.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.038 |
| 2019 | 0.152 |
| 2018 | 0.152 |
| 2017 | 0.152 |
| 2016 | 0.152 |
| 2015 | 0.152 |
| 2014 | 0.152 |
| 2013 | 0.076 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
