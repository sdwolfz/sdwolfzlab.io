---
title: "FORWARD AIR CORPORATION (FWRD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FORWARD AIR CORPORATION</td></tr>
    <tr><td>Symbol</td><td>FWRD</td></tr>
    <tr><td>Web</td><td><a href="https://www.forwardaircorp.com">www.forwardaircorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.42 |
| 2020 | 0.75 |
| 2019 | 0.72 |
| 2018 | 0.63 |
| 2017 | 0.6 |
| 2016 | 0.51 |
| 2015 | 0.48 |
| 2014 | 0.48 |
| 2013 | 0.4 |
| 2012 | 0.34 |
| 2011 | 0.28 |
| 2010 | 0.28 |
| 2009 | 0.28 |
| 2008 | 0.28 |
| 2007 | 0.28 |
| 2006 | 0.28 |
| 2005 | 0.27 |
| 1998 | 7.219 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
