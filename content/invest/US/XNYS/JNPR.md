---
title: "JUNIPER NETWORKS (JNPR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>JUNIPER NETWORKS</td></tr>
    <tr><td>Symbol</td><td>JNPR</td></tr>
    <tr><td>Web</td><td><a href="https://www.juniper.net">www.juniper.net</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.4 |
| 2020 | 0.8 |
| 2019 | 0.76 |
| 2018 | 0.72 |
| 2017 | 0.4 |
| 2016 | 0.4 |
| 2015 | 0.4 |
| 2014 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
