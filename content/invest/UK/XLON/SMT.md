---
title: "Scottish Mortgage (SMT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Scottish Mortgage</td></tr>
    <tr><td>Symbol</td><td>SMT</td></tr>
    <tr><td>Web</td><td><a href="https://www.bailliegifford.co.uk">www.bailliegifford.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 3.25 |
| 2019 | 3.13 |
| 2018 | 3.07 |
| 2017 | 3.0 |
| 2016 | 2.96 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
