---
title: "SPECTRUM PHARMACEUTICALS INC (SPPI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SPECTRUM PHARMACEUTICALS INC</td></tr>
    <tr><td>Symbol</td><td>SPPI</td></tr>
    <tr><td>Web</td><td><a href="https://www.sppirx.com">www.sppirx.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2012 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
