---
title: "CEREALCOM SA ALEXANDRIA (CCOM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>CEREALCOM SA ALEXANDRIA</td></tr>
    <tr><td>Symbol</td><td>CCOM</td></tr>
    <tr><td>Web</td><td><a href="https://www.cerealcomteleorman.ro">www.cerealcomteleorman.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2002 | 0.1027 |
| 2001 | 1.3636 |
| 2000 | 0.6509 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
