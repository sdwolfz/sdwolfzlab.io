---
title: " (BHP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BHP</td></tr>
    <tr><td>Web</td><td><a href="https://www.bhp.com">www.bhp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.02 |
| 2020 | 2.4 |
| 2019 | 4.7 |
| 2018 | 2.36 |
| 2017 | 1.66 |
| 2016 | 0.6 |
| 2015 | 2.48 |
| 2014 | 2.42 |
| 2013 | 2.32 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
