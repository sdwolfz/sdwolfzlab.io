---
title: "ASSURED GUARANTY LTD (AGO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ASSURED GUARANTY LTD</td></tr>
    <tr><td>Symbol</td><td>AGO</td></tr>
    <tr><td>Web</td><td><a href="https://www.assuredguaranty.com">www.assuredguaranty.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.44 |
| 2020 | 0.8 |
| 2019 | 0.72 |
| 2018 | 0.64 |
| 2017 | 0.568 |
| 2016 | 0.52 |
| 2015 | 0.48 |
| 2014 | 0.44 |
| 2013 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
