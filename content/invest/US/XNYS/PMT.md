---
title: " (PMT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>PMT</td></tr>
    <tr><td>Web</td><td><a href="https://www.pennymacmortgageinvestmenttrust.com">www.pennymacmortgageinvestmenttrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.47 |
| 2020 | 1.52 |
| 2019 | 1.88 |
| 2018 | 1.88 |
| 2017 | 1.88 |
| 2016 | 1.88 |
| 2015 | 2.16 |
| 2014 | 2.99 |
| 2013 | 1.14 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
