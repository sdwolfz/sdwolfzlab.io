---
title: " (PBCTP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>PBCTP</td></tr>
    <tr><td>Web</td><td><a href="https://www.peoples.com">www.peoples.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.703 |
| 2020 | 1.406 |
| 2019 | 1.406 |
| 2018 | 1.406 |
| 2017 | 1.406 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
