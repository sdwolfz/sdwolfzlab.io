---
title: "RED RIVER BANCSHARES INC (RRBI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>RED RIVER BANCSHARES INC</td></tr>
    <tr><td>Symbol</td><td>RRBI</td></tr>
    <tr><td>Web</td><td><a href="https://www.redriverbank.net">www.redriverbank.net</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.07 |
| 2020 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
