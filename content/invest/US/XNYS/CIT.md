---
title: "CIT GROUP INC (CIT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CIT GROUP INC</td></tr>
    <tr><td>Symbol</td><td>CIT</td></tr>
    <tr><td>Web</td><td><a href="https://www.cit.com">www.cit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.7 |
| 2020 | 1.4 |
| 2019 | 1.3 |
| 2018 | 0.82 |
| 2017 | 0.785 |
| 2016 | 0.6 |
| 2015 | 0.6 |
| 2014 | 0.5 |
| 2013 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
