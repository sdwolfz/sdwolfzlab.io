---
title: "Fidelity Asian Values (FAS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Fidelity Asian Values</td></tr>
    <tr><td>Symbol</td><td>FAS</td></tr>
    <tr><td>Web</td><td><a href="https://www.fidelityinvestmenttrusts.com">www.fidelityinvestmenttrusts.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 8.5 |
| 2019 | 8.8 |
| 2018 | 5.5 |
| 2017 | 5.0 |
| 2016 | 4.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
