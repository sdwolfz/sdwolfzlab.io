---
title: "CATALYST BIOSCIENCES INC (CBIO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CATALYST BIOSCIENCES INC</td></tr>
    <tr><td>Symbol</td><td>CBIO</td></tr>
    <tr><td>Web</td><td><a href="https://www.catalystbiosciences.com">www.catalystbiosciences.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 1.649 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
