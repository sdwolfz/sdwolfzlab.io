---
title: "GREAT LAKES DREDGE & DOCK CORP (GLDD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GREAT LAKES DREDGE & DOCK CORP</td></tr>
    <tr><td>Symbol</td><td>GLDD</td></tr>
    <tr><td>Web</td><td><a href="https://www.gldd.com">www.gldd.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2012 | 0.313 |
| 2011 | 0.08 |
| 2010 | 0.068 |
| 2009 | 0.068 |
| 2008 | 0.068 |
| 2007 | 0.017 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
