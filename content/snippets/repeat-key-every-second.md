---
title: "Repeat a key every second"
date: 2021-03-30T21:04:03Z
type: posts
slug: repeat-key-every-second
tags: ["linux"]
---

## Install

```sh
sudo pacman -S xdotool
```

## Use

```sh
while true; do xdotool key ctrl+r; sleep 1; done
```
