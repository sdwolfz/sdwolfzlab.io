---
title: "Aggreko (AGK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Aggreko</td></tr>
    <tr><td>Symbol</td><td>AGK</td></tr>
    <tr><td>Web</td><td><a href="https://www.aggreko.com">www.aggreko.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 15.0 |
| 2019 | 27.65 |
| 2018 | 27.12 |
| 2017 | 27.12 |
| 2016 | 27.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
