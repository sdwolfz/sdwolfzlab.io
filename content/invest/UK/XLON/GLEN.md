---
title: "Glencore (GLEN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Glencore</td></tr>
    <tr><td>Symbol</td><td>GLEN</td></tr>
    <tr><td>Web</td><td><a href="https://www.glencore.com">www.glencore.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 12.0 |
| 2019 | 20.0 |
| 2018 | 20.0 |
| 2017 | 20.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
