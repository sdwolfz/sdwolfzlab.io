---
title: "Prudential (PRU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Prudential</td></tr>
    <tr><td>Symbol</td><td>PRU</td></tr>
    <tr><td>Web</td><td><a href="https://www.prudentialplc.com">www.prudentialplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 31.34 |
| 2019 | 63.18 |
| 2018 | 49.35 |
| 2017 | 45.07 |
| 2016 | 49.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
