---
title: "ALGONQUIN POWER & UTILITIES CORP (AQN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ALGONQUIN POWER & UTILITIES CORP</td></tr>
    <tr><td>Symbol</td><td>AQN</td></tr>
    <tr><td>Web</td><td><a href="https://www.algonquinpowerandutilities.com">www.algonquinpowerandutilities.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.606 |
| 2019 | 0.551 |
| 2018 | 0.5 |
| 2017 | 0.464 |
| 2016 | 0.106 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
