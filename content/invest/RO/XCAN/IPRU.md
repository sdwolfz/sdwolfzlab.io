---
title: "IPROEB SA Bistrita (IPRU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>IPROEB SA Bistrita</td></tr>
    <tr><td>Symbol</td><td>IPRU</td></tr>
    <tr><td>Web</td><td><a href="https://www.iproeb.ro">www.iproeb.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.04204 |
| 2018 | 0.08408 |
| 2017 | 0.12612 |
| 2016 | 0.07357 |
| 2015 | 0.084 |
| 2013 | 0.03153 |
| 2009 | 0.084 |
| 2008 | 0.126 |
| 2007 | 0.245 |
| 2006 | 0.098 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
