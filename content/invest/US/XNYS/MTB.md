---
title: "M & T BANK CORP (MTB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>M & T BANK CORP</td></tr>
    <tr><td>Symbol</td><td>MTB</td></tr>
    <tr><td>Web</td><td><a href="https://www.mtb.com">www.mtb.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.1 |
| 2020 | 4.4 |
| 2019 | 4.1 |
| 2018 | 3.55 |
| 2017 | 3.0 |
| 2016 | 2.8 |
| 2015 | 2.8 |
| 2014 | 2.8 |
| 2013 | 2.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
