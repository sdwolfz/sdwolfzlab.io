---
title: "MAGNA INTERNATIONAL INC (MGA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MAGNA INTERNATIONAL INC</td></tr>
    <tr><td>Symbol</td><td>MGA</td></tr>
    <tr><td>Web</td><td><a href="https://www.magna.com">www.magna.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.6 |
| 2019 | 1.46 |
| 2018 | 1.32 |
| 2017 | 1.1 |
| 2016 | 1.0 |
| 2015 | 0.88 |
| 2014 | 1.52 |
| 2013 | 0.64 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
