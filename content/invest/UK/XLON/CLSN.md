---
title: "CALISEN PLC ORD GBP0.01 (CLSN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>CALISEN PLC ORD GBP0.01</td></tr>
    <tr><td>Symbol</td><td>CLSN</td></tr>
    <tr><td>Web</td><td><a href="http://www.calisen.com">www.calisen.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
