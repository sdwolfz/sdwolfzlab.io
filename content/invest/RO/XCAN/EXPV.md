---
title: "EXPLOATARE PORTUARA DROBETA SA ORSOVA (EXPV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>EXPLOATARE PORTUARA DROBETA SA ORSOVA</td></tr>
    <tr><td>Symbol</td><td>EXPV</td></tr>
    <tr><td>Web</td><td><a href="https://www.port-orsova.ro">www.port-orsova.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
