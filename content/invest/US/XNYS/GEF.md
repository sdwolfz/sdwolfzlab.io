---
title: "GREIF INC (GEF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GREIF INC</td></tr>
    <tr><td>Symbol</td><td>GEF</td></tr>
    <tr><td>Web</td><td><a href="https://www.greif.com">www.greif.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.44 |
| 2020 | 1.76 |
| 2019 | 1.76 |
| 2018 | 1.72 |
| 2017 | 1.68 |
| 2016 | 1.68 |
| 2015 | 1.68 |
| 2014 | 1.68 |
| 2013 | 1.26 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
