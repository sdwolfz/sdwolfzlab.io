---
title: "Mediclinic International (MDC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Mediclinic International</td></tr>
    <tr><td>Symbol</td><td>MDC</td></tr>
    <tr><td>Web</td><td><a href="https://www.mediclinic.com">www.mediclinic.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 7.16 |
| 2019 | 7.9 |
| 2018 | 58.0 |
| 2017 | 7.9 |
| 2016 | 337.34 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
