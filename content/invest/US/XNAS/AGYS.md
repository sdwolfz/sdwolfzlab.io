---
title: "AGILYSYS INC (AGYS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AGILYSYS INC</td></tr>
    <tr><td>Symbol</td><td>AGYS</td></tr>
    <tr><td>Web</td><td><a href="https://www.agilysys.com">www.agilysys.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2009 | 0.09 |
| 2008 | 0.12 |
| 2007 | 0.12 |
| 2006 | 0.12 |
| 2005 | 0.12 |
| 2004 | 0.12 |
| 2003 | 0.12 |
| 2002 | 0.12 |
| 2001 | 0.12 |
| 2000 | 0.12 |
| 1999 | 0.12 |
| 1998 | 0.12 |
| 1997 | 0.12 |
| 1996 | 0.09 |
| 1995 | 0.13 |
| 1994 | 0.125 |
| 1993 | 0.13 |
| 1992 | 0.205 |
| 1991 | 0.16 |
| 1990 | 0.145 |
| 1989 | 0.14 |
| 1988 | 0.035 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
