---
title: "WABASH NATIONAL CORP (WNC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>WABASH NATIONAL CORP</td></tr>
    <tr><td>Symbol</td><td>WNC</td></tr>
    <tr><td>Web</td><td><a href="https://ir.wabashnational.com">ir.wabashnational.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.16 |
| 2020 | 0.24 |
| 2019 | 0.4 |
| 2018 | 0.3 |
| 2017 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
