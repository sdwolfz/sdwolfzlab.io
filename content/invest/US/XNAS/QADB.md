---
title: "QAD INC (QADB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>QAD INC</td></tr>
    <tr><td>Symbol</td><td>QADB</td></tr>
    <tr><td>Web</td><td><a href="https://www.qad.com">www.qad.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.06 |
| 2020 | 0.24 |
| 2019 | 0.24 |
| 2018 | 0.24 |
| 2017 | 0.24 |
| 2016 | 0.24 |
| 2015 | 0.24 |
| 2014 | 0.24 |
| 2013 | 0.24 |
| 2012 | 0.54 |
| 2011 | 0.21 |
| 2010 | 0.1 |
| 2009 | 0.1 |
| 2008 | 0.1 |
| 2007 | 0.1 |
| 2006 | 0.1 |
| 2005 | 0.1 |
| 2004 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
