---
title: "THOR INDUSTRIES (THO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>THOR INDUSTRIES</td></tr>
    <tr><td>Symbol</td><td>THO</td></tr>
    <tr><td>Web</td><td><a href="https://www.thorindustries.com">www.thorindustries.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.41 |
| 2020 | 1.62 |
| 2019 | 1.58 |
| 2018 | 1.52 |
| 2017 | 1.4 |
| 2016 | 1.26 |
| 2015 | 1.14 |
| 2014 | 1.0 |
| 2013 | 0.64 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
