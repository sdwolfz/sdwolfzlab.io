---
title: "SCT SA BUCURESTI (SCTB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>SCT SA BUCURESTI</td></tr>
    <tr><td>Symbol</td><td>SCTB</td></tr>
    <tr><td>Web</td><td><a href="https://www.sctb.ro">www.sctb.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
