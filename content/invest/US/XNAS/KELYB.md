---
title: "KELLY SERVICES INC (KELYB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>KELLY SERVICES INC</td></tr>
    <tr><td>Symbol</td><td>KELYB</td></tr>
    <tr><td>Web</td><td><a href="https://www.kellyservices.com">www.kellyservices.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.075 |
| 2019 | 0.3 |
| 2018 | 0.3 |
| 2017 | 0.3 |
| 2016 | 0.275 |
| 2015 | 0.2 |
| 2014 | 0.2 |
| 2013 | 0.2 |
| 2012 | 0.2 |
| 2011 | 0.1 |
| 2008 | 0.54 |
| 2007 | 0.52 |
| 2006 | 0.45 |
| 2005 | 0.4 |
| 2004 | 0.4 |
| 2003 | 0.4 |
| 2002 | 0.4 |
| 2001 | 0.85 |
| 2000 | 0.99 |
| 1999 | 0.95 |
| 1998 | 0.91 |
| 1997 | 0.87 |
| 1996 | 0.83 |
| 1995 | 0.78 |
| 1994 | 0.7 |
| 1993 | 0.71 |
| 1992 | 0.73 |
| 1991 | 0.72 |
| 1990 | 0.66 |
| 1989 | 0.648 |
| 1988 | 0.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
