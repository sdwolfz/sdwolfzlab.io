---
title: "Meggitt (MGGT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Meggitt</td></tr>
    <tr><td>Symbol</td><td>MGGT</td></tr>
    <tr><td>Web</td><td><a href="https://www.meggitt.com">www.meggitt.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 17.5 |
| 2018 | 16.65 |
| 2017 | 15.85 |
| 2016 | 15.1 |
| 2015 | 14.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
