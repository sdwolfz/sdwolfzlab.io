---
title: "SMUCKER(J.M.)CO (SJM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SMUCKER(J.M.)CO</td></tr>
    <tr><td>Symbol</td><td>SJM</td></tr>
    <tr><td>Web</td><td><a href="https://www.jmsmucker.com">www.jmsmucker.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.8 |
| 2020 | 3.56 |
| 2019 | 3.46 |
| 2018 | 3.26 |
| 2017 | 3.06 |
| 2016 | 2.84 |
| 2015 | 2.62 |
| 2014 | 2.44 |
| 2013 | 1.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
