---
title: "F&C Investment Trust (FCIT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>F&C Investment Trust</td></tr>
    <tr><td>Symbol</td><td>FCIT</td></tr>
    <tr><td>Web</td><td><a href="https://www.fandcit.com">www.fandcit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 12.1 |
| 2019 | 11.6 |
| 2018 | 11.0 |
| 2017 | 10.4 |
| 2016 | 9.85 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
