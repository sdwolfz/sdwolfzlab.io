---
title: "TEEKAY TANKERS LTD (TNK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TEEKAY TANKERS LTD</td></tr>
    <tr><td>Symbol</td><td>TNK</td></tr>
    <tr><td>Web</td><td><a href="https://www.teekay.com">www.teekay.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.03 |
| 2017 | 0.12 |
| 2016 | 0.3 |
| 2015 | 0.12 |
| 2014 | 0.12 |
| 2013 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
