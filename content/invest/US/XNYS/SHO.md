---
title: " (SHO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SHO</td></tr>
    <tr><td>Web</td><td><a href="https://www.sunstonehotels.com">www.sunstonehotels.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.05 |
| 2019 | 0.74 |
| 2018 | 0.69 |
| 2017 | 0.73 |
| 2016 | 0.68 |
| 2015 | 1.41 |
| 2014 | 0.15 |
| 2013 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
