---
title: "FEDNAT HOLDING COMPANY (FNHC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FEDNAT HOLDING COMPANY</td></tr>
    <tr><td>Symbol</td><td>FNHC</td></tr>
    <tr><td>Web</td><td><a href="https://www.fednat.com">www.fednat.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.36 |
| 2019 | 0.33 |
| 2018 | 0.32 |
| 2017 | 0.32 |
| 2016 | 0.25 |
| 2015 | 0.17 |
| 2014 | 0.12 |
| 2013 | 0.11 |
| 2012 | 0.02 |
| 2010 | 0.12 |
| 2009 | 0.36 |
| 2008 | 0.72 |
| 2007 | 0.72 |
| 2006 | 0.48 |
| 2005 | 0.32 |
| 2004 | 0.44 |
| 2003 | 0.32 |
| 2002 | 0.11 |
| 2001 | 0.08 |
| 2000 | 0.02 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
