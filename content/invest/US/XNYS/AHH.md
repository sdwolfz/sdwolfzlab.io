---
title: " (AHH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>AHH</td></tr>
    <tr><td>Web</td><td><a href="https://www.armadahoffler.com">www.armadahoffler.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.31 |
| 2020 | 0.44 |
| 2019 | 0.84 |
| 2018 | 0.8 |
| 2017 | 0.76 |
| 2016 | 0.72 |
| 2015 | 0.68 |
| 2014 | 0.64 |
| 2013 | 0.32 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
