---
title: " (HT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>HT</td></tr>
    <tr><td>Web</td><td><a href="https://www.hersha.com">www.hersha.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.56 |
| 2019 | 1.12 |
| 2018 | 1.12 |
| 2017 | 1.32 |
| 2016 | 0.84 |
| 2015 | 0.7 |
| 2014 | 0.26 |
| 2013 | 0.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
