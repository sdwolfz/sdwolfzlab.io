---
title: "Martin Currie Global Portfolio Trust (MNP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Martin Currie Global Portfolio Trust</td></tr>
    <tr><td>Symbol</td><td>MNP</td></tr>
    <tr><td>Web</td><td><a href="https://www.martincurrieglobal.com">www.martincurrieglobal.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 4.2 |
| 2018 | 4.2 |
| 2017 | 4.2 |
| 2016 | 4.15 |
| 2015 | 4.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
