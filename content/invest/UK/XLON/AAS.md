---
title: "Abstd Asiafocus (AAS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Abstd Asiafocus</td></tr>
    <tr><td>Symbol</td><td>AAS</td></tr>
    <tr><td>Web</td><td><a href="https://www.asian-smaller.co.uk">www.asian-smaller.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 19.0 |
| 2019 | 19.0 |
| 2018 | 17.0 |
| 2017 | 16.0 |
| 2016 | 10.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
