---
title: "SDL PLC ORD 1P (SDL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>SDL PLC ORD 1P</td></tr>
    <tr><td>Symbol</td><td>SDL</td></tr>
    <tr><td>Web</td><td><a href="http://www.sdl.com">www.sdl.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 7.0 |
| 2017 | 6.2 |
| 2016 | 6.2 |
| 2015 | 3.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
