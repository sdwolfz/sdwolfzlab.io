---
title: "Troy Income (TIGT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Troy Income</td></tr>
    <tr><td>Symbol</td><td>TIGT</td></tr>
    <tr><td>Web</td><td><a href="https://www.tigt.co.uk">www.tigt.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.78 |
| 2019 | 2.75 |
| 2018 | 2.67 |
| 2017 | 2.56 |
| 2016 | 2.43 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
