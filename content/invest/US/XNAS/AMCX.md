---
title: "AMC NETWORKS INC (AMCX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AMC NETWORKS INC</td></tr>
    <tr><td>Symbol</td><td>AMCX</td></tr>
    <tr><td>Web</td><td><a href="https://www.amcnetworks.com">www.amcnetworks.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
