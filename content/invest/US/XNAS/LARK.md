---
title: "LANDMARK BANCORP INC(KANSAS) (LARK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LANDMARK BANCORP INC(KANSAS)</td></tr>
    <tr><td>Symbol</td><td>LARK</td></tr>
    <tr><td>Web</td><td><a href="https://www.banklandmark.com">www.banklandmark.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.4 |
| 2020 | 0.8 |
| 2019 | 0.8 |
| 2018 | 0.8 |
| 2017 | 0.8 |
| 2016 | 0.8 |
| 2015 | 0.76 |
| 2014 | 0.76 |
| 2013 | 0.76 |
| 2012 | 0.76 |
| 2011 | 0.76 |
| 2010 | 0.76 |
| 2009 | 0.76 |
| 2008 | 0.76 |
| 2007 | 0.76 |
| 2006 | 0.7 |
| 2005 | 0.68 |
| 2004 | 0.68 |
| 2003 | 0.66 |
| 2002 | 0.61 |
| 2001 | 0.6 |
| 2000 | 0.6 |
| 1999 | 0.7 |
| 1998 | 0.65 |
| 1997 | 0.3 |
| 1996 | 0.4 |
| 1995 | 0.3 |
| 1994 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
