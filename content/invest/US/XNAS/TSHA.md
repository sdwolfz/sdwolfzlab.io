---
title: "TAYSHA GENE THERAPIES INC (TSHA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TAYSHA GENE THERAPIES INC</td></tr>
    <tr><td>Symbol</td><td>TSHA</td></tr>
    <tr><td>Web</td><td><a href="https://www.tayshagtx.com">www.tayshagtx.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
