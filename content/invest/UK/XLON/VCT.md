---
title: "Victrex (VCT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Victrex</td></tr>
    <tr><td>Symbol</td><td>VCT</td></tr>
    <tr><td>Web</td><td><a href="https://www.victrex.com">www.victrex.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 46.14 |
| 2019 | 59.56 |
| 2018 | 142.24 |
| 2017 | 121.8 |
| 2016 | 46.82 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
