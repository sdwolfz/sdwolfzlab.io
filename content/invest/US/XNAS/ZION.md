---
title: "ZIONS BANCORPORATION N A. (ZION)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ZIONS BANCORPORATION N A.</td></tr>
    <tr><td>Symbol</td><td>ZION</td></tr>
    <tr><td>Web</td><td><a href="https://www.zionsbancorp.com">www.zionsbancorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.68 |
| 2020 | 1.36 |
| 2019 | 1.28 |
| 2018 | 1.04 |
| 2017 | 0.44 |
| 2016 | 0.28 |
| 2015 | 0.22 |
| 2014 | 0.16 |
| 2013 | 0.13 |
| 2012 | 0.04 |
| 2011 | 0.04 |
| 2010 | 0.04 |
| 2009 | 0.1 |
| 2008 | 1.61 |
| 2007 | 1.68 |
| 2006 | 1.47 |
| 2005 | 1.44 |
| 2004 | 1.26 |
| 2003 | 1.02 |
| 2002 | 0.8 |
| 2001 | 0.8 |
| 2000 | 0.89 |
| 1999 | 0.72 |
| 1998 | 0.54 |
| 1997 | 1.24 |
| 1996 | 1.26 |
| 1995 | 1.41 |
| 1994 | 1.16 |
| 1993 | 0.98 |
| 1992 | 1.395 |
| 1991 | 1.44 |
| 1990 | 1.44 |
| 1989 | 1.44 |
| 1988 | 0.36 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
