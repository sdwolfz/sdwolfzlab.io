---
title: "EBAY INC (EBAY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>EBAY INC</td></tr>
    <tr><td>Symbol</td><td>EBAY</td></tr>
    <tr><td>Web</td><td><a href="https://www.ebay.com">www.ebay.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.36 |
| 2020 | 0.64 |
| 2019 | 0.56 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
