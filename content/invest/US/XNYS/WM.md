---
title: "WASTE MANAGEMENT INC (WM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>WASTE MANAGEMENT INC</td></tr>
    <tr><td>Symbol</td><td>WM</td></tr>
    <tr><td>Web</td><td><a href="https://www.wm.com">www.wm.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.575 |
| 2020 | 2.18 |
| 2019 | 2.048 |
| 2018 | 1.86 |
| 2017 | 1.7 |
| 2016 | 1.64 |
| 2015 | 1.54 |
| 2014 | 1.5 |
| 2013 | 1.095 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
