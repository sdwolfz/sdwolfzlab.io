---
title: "CEPROCIM SA BUCURESTI (CEPO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>CEPROCIM SA BUCURESTI</td></tr>
    <tr><td>Symbol</td><td>CEPO</td></tr>
    <tr><td>Web</td><td><a href="https://www.ceprocim.ro">www.ceprocim.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 1.0 |
| 2007 | 5.35 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
