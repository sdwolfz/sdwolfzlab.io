---
title: "UNITED STATES LIME & MINERALS INC (USLM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>UNITED STATES LIME & MINERALS INC</td></tr>
    <tr><td>Symbol</td><td>USLM</td></tr>
    <tr><td>Web</td><td><a href="https://www.uslm.com">www.uslm.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.32 |
| 2020 | 0.64 |
| 2019 | 5.89 |
| 2018 | 0.54 |
| 2017 | 0.54 |
| 2016 | 0.5 |
| 2015 | 0.5 |
| 2014 | 0.5 |
| 2003 | 0.05 |
| 2002 | 0.1 |
| 2001 | 0.1 |
| 2000 | 0.1 |
| 1999 | 0.1 |
| 1998 | 0.1 |
| 1997 | 0.1 |
| 1996 | 0.1 |
| 1995 | 0.075 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
