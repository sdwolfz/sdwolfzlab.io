---
title: " (MIXT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>MIXT</td></tr>
    <tr><td>Web</td><td><a href="https://www.mixtelematics.com">www.mixtelematics.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.051 |
| 2020 | 0.186 |
| 2019 | 0.193 |
| 2018 | 0.165 |
| 2017 | 0.128 |
| 2016 | 0.103 |
| 2015 | 0.026 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
