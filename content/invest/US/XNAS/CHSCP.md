---
title: " (CHSCP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CHSCP</td></tr>
    <tr><td>Web</td><td><a href="https://www.chsinc.com">www.chsinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.5 |
| 2020 | 2.0 |
| 2019 | 2.0 |
| 2018 | 2.0 |
| 2017 | 2.0 |
| 2016 | 2.0 |
| 2015 | 2.0 |
| 2014 | 2.0 |
| 2013 | 2.0 |
| 2012 | 2.0 |
| 2011 | 2.0 |
| 2010 | 2.0 |
| 2009 | 2.0 |
| 2008 | 2.0 |
| 2007 | 2.0 |
| 2006 | 2.0 |
| 2005 | 2.0 |
| 2004 | 2.0 |
| 2003 | 1.84 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
