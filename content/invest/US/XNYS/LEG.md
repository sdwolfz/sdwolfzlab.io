---
title: "LEGGETT & PLATT INC (LEG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>LEGGETT & PLATT INC</td></tr>
    <tr><td>Symbol</td><td>LEG</td></tr>
    <tr><td>Web</td><td><a href="https://www.leggett.com">www.leggett.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.82 |
| 2020 | 1.6 |
| 2019 | 1.58 |
| 2018 | 1.5 |
| 2017 | 1.42 |
| 2016 | 1.34 |
| 2015 | 1.26 |
| 2014 | 1.22 |
| 2013 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
