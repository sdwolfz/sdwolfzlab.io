---
title: "EBAY INC 6% NTS 01/02/56 USD25 (EBAYL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>EBAY INC 6% NTS 01/02/56 USD25</td></tr>
    <tr><td>Symbol</td><td>EBAYL</td></tr>
    <tr><td>Web</td><td><a href="http://www.ebayinc.com">www.ebayinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.5 |
| 2019 | 1.5 |
| 2018 | 1.5 |
| 2017 | 1.5 |
| 2016 | 1.008 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
