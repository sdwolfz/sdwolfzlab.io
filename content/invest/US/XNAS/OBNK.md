---
title: "ORIGIN BANCORP INC (OBNK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ORIGIN BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>OBNK</td></tr>
    <tr><td>Web</td><td><a href="https://www.origin.bank">www.origin.bank</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.23 |
| 2020 | 0.378 |
| 2019 | 0.25 |
| 2018 | 0.098 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
