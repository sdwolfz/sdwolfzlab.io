---
title: "STEWART INFORMATION SERVICES CORP (STC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>STEWART INFORMATION SERVICES CORP</td></tr>
    <tr><td>Symbol</td><td>STC</td></tr>
    <tr><td>Web</td><td><a href="https://www.stewart.com">www.stewart.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.33 |
| 2020 | 1.2 |
| 2019 | 1.2 |
| 2018 | 1.2 |
| 2017 | 1.2 |
| 2016 | 1.2 |
| 2015 | 0.8 |
| 2014 | 0.1 |
| 2013 | 0.1 |
| 2012 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
