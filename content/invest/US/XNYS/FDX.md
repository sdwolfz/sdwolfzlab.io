---
title: "FEDEX CORP (FDX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FEDEX CORP</td></tr>
    <tr><td>Symbol</td><td>FDX</td></tr>
    <tr><td>Web</td><td><a href="https://www.fedex.com">www.fedex.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.65 |
| 2020 | 2.6 |
| 2019 | 2.6 |
| 2018 | 2.45 |
| 2017 | 1.9 |
| 2016 | 1.45 |
| 2015 | 0.95 |
| 2014 | 0.75 |
| 2013 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
