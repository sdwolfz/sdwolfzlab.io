---
title: "SPOK HLDGS INC (SPOK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SPOK HLDGS INC</td></tr>
    <tr><td>Symbol</td><td>SPOK</td></tr>
    <tr><td>Web</td><td><a href="https://www.spok.com">www.spok.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 0.5 |
| 2019 | 0.5 |
| 2018 | 0.5 |
| 2017 | 0.5 |
| 2016 | 0.75 |
| 2015 | 0.625 |
| 2014 | 0.5 |
| 2013 | 0.5 |
| 2012 | 0.75 |
| 2011 | 1.0 |
| 2010 | 2.0 |
| 2009 | 2.0 |
| 2008 | 1.4 |
| 2007 | 3.6 |
| 2006 | 3.65 |
| 2005 | 1.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
