---
title: "GUESS INC (GES)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GUESS INC</td></tr>
    <tr><td>Symbol</td><td>GES</td></tr>
    <tr><td>Web</td><td><a href="https://investors.guess.com">investors.guess.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.112 |
| 2020 | 0.224 |
| 2019 | 0.561 |
| 2018 | 0.9 |
| 2017 | 0.9 |
| 2016 | 0.9 |
| 2015 | 0.9 |
| 2014 | 0.9 |
| 2013 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
