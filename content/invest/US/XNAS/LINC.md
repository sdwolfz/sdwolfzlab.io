---
title: "LINCOLN EDUCATIONAL SERVICES CORP (LINC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LINCOLN EDUCATIONAL SERVICES CORP</td></tr>
    <tr><td>Symbol</td><td>LINC</td></tr>
    <tr><td>Web</td><td><a href="https://www.lincolntech.edu">www.lincolntech.edu</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 0.18 |
| 2013 | 0.28 |
| 2012 | 0.28 |
| 2011 | 0.82 |
| 2010 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
