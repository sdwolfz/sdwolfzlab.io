---
title: "CALAMOS L/S EQTY & DYNMC INCOME TR (CPZ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CALAMOS L/S EQTY & DYNMC INCOME TR</td></tr>
    <tr><td>Symbol</td><td>CPZ</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.52 |
| 2020 | 1.34 |
| 2019 | 0.042 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
