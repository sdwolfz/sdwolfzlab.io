---
title: "Dunelm (DNLM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Dunelm</td></tr>
    <tr><td>Symbol</td><td>DNLM</td></tr>
    <tr><td>Web</td><td><a href="https://www.dunelm.com">www.dunelm.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 52.5 |
| 2019 | 28.0 |
| 2018 | 26.5 |
| 2017 | 26.0 |
| 2016 | 25.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
