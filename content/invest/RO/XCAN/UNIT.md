---
title: "SIFI UNITEH SA BUCURESTI (UNIT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>SIFI UNITEH SA BUCURESTI</td></tr>
    <tr><td>Symbol</td><td>UNIT</td></tr>
    <tr><td>Web</td><td><a href="https://www.uniteh-timisoara.ro">www.uniteh-timisoara.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 49.0 |
| 2018 | 0.61 |
| 2017 | 0.56 |
| 2009 | 0.34 |
| 2008 | 0.4 |
| 2006 | 0.21 |
| 2005 | 0.0883 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
