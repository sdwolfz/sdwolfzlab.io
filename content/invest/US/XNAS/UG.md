---
title: "UNITED GUARDIAN (UG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>UNITED GUARDIAN</td></tr>
    <tr><td>Symbol</td><td>UG</td></tr>
    <tr><td>Web</td><td><a href="https://www.u-g.com">www.u-g.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.78 |
| 2019 | 1.1 |
| 2018 | 1.05 |
| 2017 | 1.42 |
| 2016 | 0.75 |
| 2015 | 1.0 |
| 2014 | 0.8 |
| 2013 | 0.97 |
| 2012 | 1.36 |
| 2011 | 0.8 |
| 2010 | 0.63 |
| 2009 | 0.6 |
| 2008 | 0.55 |
| 2007 | 0.55 |
| 2006 | 0.47 |
| 2005 | 0.47 |
| 2004 | 0.43 |
| 2003 | 0.15 |
| 2002 | 0.1 |
| 2001 | 0.1 |
| 2000 | 0.1 |
| 1999 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
