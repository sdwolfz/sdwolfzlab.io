---
title: "Sme Credit Real (SCRF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Sme Credit Real</td></tr>
    <tr><td>Symbol</td><td>SCRF</td></tr>
    <tr><td>Web</td><td><a href="https://www.smecreditrealisation.com">www.smecreditrealisation.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 5.25 |
| 2019 | 4.25 |
| 2018 | 6.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
