---
title: "NEWMARK GROUP INC (NMRK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NEWMARK GROUP INC</td></tr>
    <tr><td>Symbol</td><td>NMRK</td></tr>
    <tr><td>Web</td><td><a href="https://www.nmrk.com">www.nmrk.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.02 |
| 2020 | 0.13 |
| 2019 | 0.39 |
| 2018 | 0.27 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
