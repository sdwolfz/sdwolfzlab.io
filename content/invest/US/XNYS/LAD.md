---
title: "LITHIA MOTORS INC (LAD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>LITHIA MOTORS INC</td></tr>
    <tr><td>Symbol</td><td>LAD</td></tr>
    <tr><td>Web</td><td><a href="https://www.lithiainvestorrelations.com">www.lithiainvestorrelations.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.66 |
| 2020 | 1.22 |
| 2019 | 1.19 |
| 2018 | 1.14 |
| 2017 | 1.06 |
| 2016 | 0.95 |
| 2015 | 0.76 |
| 2014 | 0.61 |
| 2013 | 0.26 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
