---
title: "TENARIS S.A. SPONS ADS EACH REP 2 ORD SHS (TS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TENARIS S.A. SPONS ADS EACH REP 2 ORD SHS</td></tr>
    <tr><td>Symbol</td><td>TS</td></tr>
    <tr><td>Web</td><td><a href="http://www.tenaris.com">www.tenaris.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.7 |
| 2019 | 0.82 |
| 2018 | 0.82 |
| 2017 | 0.82 |
| 2016 | 0.86 |
| 2015 | 0.9 |
| 2014 | 0.9 |
| 2013 | 0.86 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
