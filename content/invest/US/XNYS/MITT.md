---
title: " (MITT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>MITT</td></tr>
    <tr><td>Web</td><td><a href="https://www.agmortgageinvestmenttrust.com">www.agmortgageinvestmenttrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.06 |
| 2020 | 0.03 |
| 2019 | 1.9 |
| 2018 | 1.975 |
| 2017 | 2.0 |
| 2016 | 1.9 |
| 2015 | 2.275 |
| 2014 | 2.4 |
| 2013 | 2.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
