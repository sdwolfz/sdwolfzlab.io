---
title: "WESTERN MIDSTREAM PARTNERS LP (WES)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>WESTERN MIDSTREAM PARTNERS LP</td></tr>
    <tr><td>Symbol</td><td>WES</td></tr>
    <tr><td>Web</td><td><a href="https://www.westernmidstream.com">www.westernmidstream.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.626 |
| 2020 | 1.555 |
| 2019 | 2.451 |
| 2018 | 3.77 |
| 2017 | 3.53 |
| 2016 | 3.29 |
| 2015 | 2.95 |
| 2014 | 2.55 |
| 2013 | 1.14 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
