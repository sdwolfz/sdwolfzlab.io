---
title: "WEST PHARMACEUTICAL SERVICES INC (WST)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>WEST PHARMACEUTICAL SERVICES INC</td></tr>
    <tr><td>Symbol</td><td>WST</td></tr>
    <tr><td>Web</td><td><a href="https://www.westpharma.com">www.westpharma.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.51 |
| 2020 | 0.65 |
| 2019 | 0.61 |
| 2018 | 0.57 |
| 2017 | 0.53 |
| 2016 | 0.49 |
| 2015 | 0.45 |
| 2014 | 0.41 |
| 2013 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
