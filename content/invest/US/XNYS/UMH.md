---
title: " (UMH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>UMH</td></tr>
    <tr><td>Web</td><td><a href="https://www.umh.reit">www.umh.reit</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.38 |
| 2020 | 0.72 |
| 2019 | 0.72 |
| 2018 | 0.72 |
| 2017 | 0.72 |
| 2016 | 0.72 |
| 2015 | 0.72 |
| 2014 | 0.72 |
| 2013 | 0.36 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
