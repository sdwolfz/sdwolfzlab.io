---
title: "SENSIENT TECHNOLOGIES CORP (SXT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SENSIENT TECHNOLOGIES CORP</td></tr>
    <tr><td>Symbol</td><td>SXT</td></tr>
    <tr><td>Web</td><td><a href="https://www.sensient.com">www.sensient.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.78 |
| 2020 | 1.56 |
| 2019 | 1.47 |
| 2018 | 1.35 |
| 2017 | 1.23 |
| 2016 | 1.11 |
| 2015 | 1.04 |
| 2014 | 0.98 |
| 2013 | 0.46 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
