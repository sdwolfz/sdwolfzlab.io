---
title: " (BSMX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BSMX</td></tr>
    <tr><td>Web</td><td><a href="https://www.santander.com.mx">www.santander.com.mx</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.185 |
| 2018 | 0.347 |
| 2017 | 0.341 |
| 2016 | 0.508 |
| 2015 | 0.277 |
| 2014 | 0.163 |
| 2013 | 1.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
