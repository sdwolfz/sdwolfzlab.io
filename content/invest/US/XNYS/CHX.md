---
title: "CHAMPIONX CORPORATION (CHX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CHAMPIONX CORPORATION</td></tr>
    <tr><td>Symbol</td><td>CHX</td></tr>
    <tr><td>Web</td><td><a href="http://www.championx.com">www.championx.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
