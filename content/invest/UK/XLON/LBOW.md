---
title: "Icg-longbow (LBOW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Icg-longbow</td></tr>
    <tr><td>Symbol</td><td>LBOW</td></tr>
    <tr><td>Web</td><td><a href="https://www.ICG-Longbow-SSUP.com">www.ICG-Longbow-SSUP.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.0 |
| 2019 | 6.0 |
| 2018 | 6.0 |
| 2017 | 8.5 |
| 2016 | 6.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
