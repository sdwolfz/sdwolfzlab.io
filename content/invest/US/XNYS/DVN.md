---
title: "DEVON ENERGY CORP (DVN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DEVON ENERGY CORP</td></tr>
    <tr><td>Symbol</td><td>DVN</td></tr>
    <tr><td>Web</td><td><a href="https://www.devonenergy.com">www.devonenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.98 |
| 2020 | 0.68 |
| 2019 | 0.35 |
| 2018 | 0.3 |
| 2017 | 0.24 |
| 2016 | 0.42 |
| 2015 | 0.96 |
| 2014 | 0.94 |
| 2013 | 0.44 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
