---
title: "INSTITUTUL DE CERCETARI PENTRU ACOPERIRI AVANSATE ICAA SA BUCURESTI (ICEV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>INSTITUTUL DE CERCETARI PENTRU ACOPERIRI AVANSATE ICAA SA BUCURESTI</td></tr>
    <tr><td>Symbol</td><td>ICEV</td></tr>
    <tr><td>Web</td><td><a href="https://www.icaaro.com">www.icaaro.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2003 | 0.0071 |
| 2002 | 0.0263 |
| 2001 | 0.0157 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
