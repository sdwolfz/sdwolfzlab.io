---
title: "INFOSYS LTD SPON ADR EACH REP 1 ORD SHS (INFY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>INFOSYS LTD SPON ADR EACH REP 1 ORD SHS</td></tr>
    <tr><td>Symbol</td><td>INFY</td></tr>
    <tr><td>Web</td><td><a href="http://www.infosys.com">www.infosys.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.236 |
| 2019 | 0.299 |
| 2018 | 0.303 |
| 2017 | 0.389 |
| 2016 | 0.335 |
| 2015 | 0.576 |
| 2014 | 0.922 |
| 2013 | 0.475 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
