---
title: "HMN FINANCIAL INC (HMNF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HMN FINANCIAL INC</td></tr>
    <tr><td>Symbol</td><td>HMNF</td></tr>
    <tr><td>Web</td><td><a href="https://www.hmnf.com">www.hmnf.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2008 | 0.75 |
| 2007 | 1.0 |
| 2006 | 0.98 |
| 2005 | 0.92 |
| 2004 | 0.84 |
| 2003 | 0.76 |
| 2002 | 0.68 |
| 2001 | 0.52 |
| 2000 | 0.44 |
| 1999 | 0.32 |
| 1998 | 0.21 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
