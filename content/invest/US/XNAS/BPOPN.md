---
title: " (BPOPN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BPOPN</td></tr>
    <tr><td>Web</td><td><a href="https://www.popular.com">www.popular.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.698 |
| 2020 | 1.675 |
| 2019 | 1.675 |
| 2018 | 1.675 |
| 2017 | 1.675 |
| 2016 | 1.675 |
| 2015 | 1.675 |
| 2014 | 1.675 |
| 2013 | 1.675 |
| 2012 | 1.675 |
| 2011 | 1.675 |
| 2010 | 1.675 |
| 2009 | 1.675 |
| 2008 | 1.675 |
| 2007 | 1.675 |
| 2006 | 1.675 |
| 2005 | 1.675 |
| 2004 | 1.675 |
| 2003 | 0.14 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
