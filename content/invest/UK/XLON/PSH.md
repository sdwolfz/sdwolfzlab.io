---
title: "Pershing Square Holdings (PSH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Pershing Square Holdings</td></tr>
    <tr><td>Symbol</td><td>PSH</td></tr>
    <tr><td>Web</td><td><a href="https://www.pershingsquareholdings.com">www.pershingsquareholdings.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 40.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
