---
title: "RSpec Instance Double"
date: 2023-07-11T19:49:39Z
type: posts
slug: rspec-instance-double
tags: ["rspec", "ruby", "testing"]
---

Here is how you can make an assertion on a nested call to another class:

```ruby
let(:double) { instance_double('something') }

before do
  allow(SomeClass).to receive(:some_method).and_return(double)
  allow(double).to receive(:another).and_return(true)
end

it 'does something useful' do
  subject.call

  expect(SomeClass).to have_received(:some_method).with(args)
  expect(double).to have_received(:another).with(other_args)
end
```
