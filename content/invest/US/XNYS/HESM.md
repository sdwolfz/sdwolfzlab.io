---
title: " (HESM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>HESM</td></tr>
    <tr><td>Web</td><td><a href="https://www.hessmidstream.com">www.hessmidstream.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.9 |
| 2020 | 1.735 |
| 2019 | 1.561 |
| 2018 | 1.357 |
| 2017 | 0.581 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
