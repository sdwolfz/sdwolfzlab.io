---
title: "METALICA SA ORADEA (MEOR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>METALICA SA ORADEA</td></tr>
    <tr><td>Symbol</td><td>MEOR</td></tr>
    <tr><td>Web</td><td><a href="https://www.metalicaoradea.ro">www.metalicaoradea.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2004 | 2.0 |
| 2003 | 1.5 |
| 2002 | 0.1833 |
| 2001 | 1.5645 |
| 2000 | 0.315 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
