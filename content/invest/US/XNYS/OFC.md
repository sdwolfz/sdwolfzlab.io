---
title: " (OFC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>OFC</td></tr>
    <tr><td>Web</td><td><a href="https://www.copt.com">www.copt.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.275 |
| 2020 | 1.1 |
| 2019 | 1.1 |
| 2018 | 1.1 |
| 2017 | 1.1 |
| 2016 | 1.1 |
| 2015 | 1.1 |
| 2014 | 1.1 |
| 2013 | 0.825 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
