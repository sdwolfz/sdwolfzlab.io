---
title: "LOUISIANA-PACIFIC CORP (LPX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>LOUISIANA-PACIFIC CORP</td></tr>
    <tr><td>Symbol</td><td>LPX</td></tr>
    <tr><td>Web</td><td><a href="https://www.lpcorp.com">www.lpcorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.32 |
| 2020 | 0.58 |
| 2019 | 0.54 |
| 2018 | 0.52 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
