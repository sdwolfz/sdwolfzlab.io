---
title: "AMCON DISTRIBUTING CO (DIT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AMCON DISTRIBUTING CO</td></tr>
    <tr><td>Symbol</td><td>DIT</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.36 |
| 2020 | 6.0 |
| 2019 | 1.0 |
| 2018 | 1.0 |
| 2017 | 1.0 |
| 2016 | 1.0 |
| 2015 | 0.72 |
| 2014 | 0.72 |
| 2013 | 0.72 |
| 2012 | 0.72 |
| 2011 | 0.72 |
| 2010 | 0.72 |
| 2009 | 0.48 |
| 2008 | 0.26 |
| 2004 | 0.39 |
| 2003 | 0.12 |
| 2002 | 0.12 |
| 2001 | 0.12 |
| 2000 | 0.12 |
| 1999 | 0.09 |
| 1998 | 0.02 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
