---
title: "Schroder Japan (SJG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Schroder Japan</td></tr>
    <tr><td>Symbol</td><td>SJG</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 4.9 |
| 2019 | 4.7 |
| 2018 | 4.0 |
| 2017 | 3.5 |
| 2016 | 2.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
