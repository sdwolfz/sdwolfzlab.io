---
title: "SHARPSPRING INC (SHSP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SHARPSPRING INC</td></tr>
    <tr><td>Symbol</td><td>SHSP</td></tr>
    <tr><td>Web</td><td><a href="https://www.sharpspring.com">www.sharpspring.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 0.48 |
| 2013 | 0.083 |
| 2012 | 0.176 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
