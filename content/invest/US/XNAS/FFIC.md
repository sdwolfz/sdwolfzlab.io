---
title: "FLUSHING FINANCIAL CORP (FFIC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FLUSHING FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>FFIC</td></tr>
    <tr><td>Web</td><td><a href="https://www.flushingbank.com">www.flushingbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.21 |
| 2020 | 0.84 |
| 2019 | 0.84 |
| 2018 | 0.8 |
| 2017 | 0.72 |
| 2016 | 0.68 |
| 2015 | 0.64 |
| 2014 | 0.6 |
| 2013 | 0.52 |
| 2012 | 0.52 |
| 2011 | 0.52 |
| 2010 | 0.52 |
| 2009 | 0.52 |
| 2008 | 0.52 |
| 2007 | 0.48 |
| 2006 | 0.44 |
| 2005 | 0.4 |
| 2004 | 0.35 |
| 2003 | 0.42 |
| 2002 | 0.36 |
| 2001 | 0.38 |
| 2000 | 0.4 |
| 1999 | 0.32 |
| 1998 | 0.31 |
| 1997 | 0.22 |
| 1996 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
