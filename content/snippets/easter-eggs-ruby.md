---
title: "Easter Eggs: ruby"
date: 2020-07-12T12:55:20Z
type: posts
slug: easter-eggs-ruby
series: ["Easter Eggs"]
tags: ["ruby", "docker"]
---

Ruby is amazing! Here's one easter egg:

```sh
ruby -r irb -e 'IRB.send(:easter_egg)'
```

Or start a ruby shell, type `RubyVM` and press `Tab` twice.

It also works in `Docker`, sort of, you need to provide the `:dancing` argument.

```sh
docker run --rm -it ruby:2.7.1-alpine ruby -r irb -e 'IRB.send(:easter_egg, :dancing)'
```
