---
title: " (GTY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>GTY</td></tr>
    <tr><td>Web</td><td><a href="https://www.gettyrealty.com">www.gettyrealty.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.78 |
| 2020 | 1.5 |
| 2019 | 1.42 |
| 2018 | 1.31 |
| 2017 | 1.16 |
| 2016 | 1.03 |
| 2015 | 0.93 |
| 2014 | 0.82 |
| 2013 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
