---
title: " (MLCO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>MLCO</td></tr>
    <tr><td>Web</td><td><a href="https://www.melco-resorts.com">www.melco-resorts.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.165 |
| 2019 | 0.64 |
| 2018 | 0.56 |
| 2017 | 1.681 |
| 2016 | 0.722 |
| 2015 | 0.117 |
| 2014 | 0.623 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
