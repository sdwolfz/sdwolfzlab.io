---
title: "ALBEMARLE CORP (ALB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ALBEMARLE CORP</td></tr>
    <tr><td>Symbol</td><td>ALB</td></tr>
    <tr><td>Web</td><td><a href="https://www.albemarle.com">www.albemarle.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.78 |
| 2020 | 1.54 |
| 2019 | 1.472 |
| 2018 | 1.34 |
| 2017 | 1.28 |
| 2016 | 1.22 |
| 2015 | 1.16 |
| 2014 | 1.1 |
| 2013 | 0.48 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
