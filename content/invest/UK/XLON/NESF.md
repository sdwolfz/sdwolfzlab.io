---
title: "NextEnergy Solar (NESF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>NextEnergy Solar</td></tr>
    <tr><td>Symbol</td><td>NESF</td></tr>
    <tr><td>Web</td><td><a href="https://nextenergysolarfund.com">nextenergysolarfund.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.87 |
| 2019 | 6.65 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
