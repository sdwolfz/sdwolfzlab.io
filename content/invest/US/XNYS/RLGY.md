---
title: "REALOGY HLDGS CORP (RLGY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>REALOGY HLDGS CORP</td></tr>
    <tr><td>Symbol</td><td>RLGY</td></tr>
    <tr><td>Web</td><td><a href="https://www.realogy.com">www.realogy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.27 |
| 2018 | 0.36 |
| 2017 | 0.36 |
| 2016 | 0.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
