---
title: "LENNAR CORP (LEN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>LENNAR CORP</td></tr>
    <tr><td>Symbol</td><td>LEN</td></tr>
    <tr><td>Web</td><td><a href="https://www.lennar.com">www.lennar.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.5 |
| 2020 | 0.625 |
| 2019 | 0.16 |
| 2018 | 0.16 |
| 2017 | 0.16 |
| 2016 | 0.16 |
| 2015 | 0.16 |
| 2014 | 0.16 |
| 2013 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
