---
title: "ASSURANT INC (AIZ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ASSURANT INC</td></tr>
    <tr><td>Symbol</td><td>AIZ</td></tr>
    <tr><td>Web</td><td><a href="https://www.assurant.com">www.assurant.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.66 |
| 2020 | 2.55 |
| 2019 | 2.43 |
| 2018 | 2.28 |
| 2017 | 2.15 |
| 2016 | 2.03 |
| 2015 | 1.37 |
| 2014 | 1.06 |
| 2013 | 0.75 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
