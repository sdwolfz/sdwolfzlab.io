---
title: "Murray International (MYI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Murray International</td></tr>
    <tr><td>Symbol</td><td>MYI</td></tr>
    <tr><td>Web</td><td><a href="https://www.murray-intl.co.uk">www.murray-intl.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 54.5 |
| 2019 | 53.5 |
| 2018 | 51.5 |
| 2017 | 50.0 |
| 2016 | 47.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
