---
title: "Quilter (QLT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Quilter</td></tr>
    <tr><td>Symbol</td><td>QLT</td></tr>
    <tr><td>Web</td><td><a href="https://www.quilter.com">www.quilter.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 5.2 |
| 2018 | 12.0 |
| 2017 | 161.47 |
| 2015 | 18.46 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
