---
title: "ANALOG DEVICES INC (ADI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ANALOG DEVICES INC</td></tr>
    <tr><td>Symbol</td><td>ADI</td></tr>
    <tr><td>Web</td><td><a href="https://www.analog.com">www.analog.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.69 |
| 2020 | 2.48 |
| 2019 | 2.16 |
| 2018 | 1.92 |
| 2017 | 1.8 |
| 2016 | 1.68 |
| 2015 | 1.6 |
| 2014 | 1.48 |
| 2013 | 1.36 |
| 2012 | 1.2 |
| 2011 | 0.97 |
| 2010 | 0.86 |
| 2009 | 0.6 |
| 2008 | 0.78 |
| 2007 | 0.72 |
| 2006 | 0.601 |
| 2005 | 0.38 |
| 2004 | 0.22 |
| 2003 | 0.04 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
