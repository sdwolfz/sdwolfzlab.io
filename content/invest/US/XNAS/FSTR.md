---
title: "FOSTER(L.B.)& CO (FSTR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FOSTER(L.B.)& CO</td></tr>
    <tr><td>Symbol</td><td>FSTR</td></tr>
    <tr><td>Web</td><td><a href="https://www.lbfoster.com">www.lbfoster.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.12 |
| 2015 | 0.16 |
| 2014 | 0.13 |
| 2013 | 0.12 |
| 2012 | 0.1 |
| 2011 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
