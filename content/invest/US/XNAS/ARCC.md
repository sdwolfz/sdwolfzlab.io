---
title: " (ARCC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ARCC</td></tr>
    <tr><td>Web</td><td><a href="https://www.arescapitalcorp.com">www.arescapitalcorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.8 |
| 2020 | 1.6 |
| 2019 | 1.68 |
| 2018 | 1.54 |
| 2017 | 1.52 |
| 2016 | 1.52 |
| 2015 | 1.57 |
| 2014 | 1.57 |
| 2013 | 1.57 |
| 2012 | 1.6 |
| 2011 | 1.41 |
| 2010 | 1.4 |
| 2009 | 1.47 |
| 2008 | 1.68 |
| 2007 | 1.66 |
| 2006 | 1.64 |
| 2005 | 1.3 |
| 2004 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
