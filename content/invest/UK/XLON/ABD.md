---
title: "Abdn.nw.dwn (ABD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Abdn.nw.dwn</td></tr>
    <tr><td>Symbol</td><td>ABD</td></tr>
    <tr><td>Web</td><td><a href="https://www.newdawn-trust.co.uk">www.newdawn-trust.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 4.3 |
| 2019 | 4.3 |
| 2018 | 4.3 |
| 2017 | 4.0 |
| 2016 | 3.9 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
