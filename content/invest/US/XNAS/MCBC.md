---
title: "MACATAWA BANK CORP (MCBC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MACATAWA BANK CORP</td></tr>
    <tr><td>Symbol</td><td>MCBC</td></tr>
    <tr><td>Web</td><td><a href="https://www.macatawabank.com">www.macatawabank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.16 |
| 2020 | 0.32 |
| 2019 | 0.28 |
| 2018 | 0.25 |
| 2017 | 0.18 |
| 2016 | 0.12 |
| 2015 | 0.11 |
| 2014 | 0.08 |
| 2008 | 0.26 |
| 2007 | 0.52 |
| 2006 | 0.61 |
| 2005 | 0.63 |
| 2004 | 0.51 |
| 2003 | 0.42 |
| 2002 | 0.34 |
| 2001 | 0.29 |
| 2000 | 0.07 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
