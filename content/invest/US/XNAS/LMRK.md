---
title: " (LMRK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>LMRK</td></tr>
    <tr><td>Web</td><td><a href="https://www.landmarkmlp.com">www.landmarkmlp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.4 |
| 2020 | 0.968 |
| 2019 | 1.47 |
| 2018 | 1.47 |
| 2017 | 1.415 |
| 2016 | 1.325 |
| 2015 | 1.057 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
