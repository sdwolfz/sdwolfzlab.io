---
title: "Schroder Asian (ATR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Schroder Asian</td></tr>
    <tr><td>Symbol</td><td>ATR</td></tr>
    <tr><td>Web</td><td><a href="https://www.schroders.co.uk">www.schroders.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 6.5 |
| 2018 | 6.2 |
| 2017 | 4.8 |
| 2016 | 4.5 |
| 2015 | 4.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
