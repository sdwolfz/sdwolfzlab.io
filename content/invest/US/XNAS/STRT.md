---
title: "STRATTEC SECURITY CORP (STRT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>STRATTEC SECURITY CORP</td></tr>
    <tr><td>Symbol</td><td>STRT</td></tr>
    <tr><td>Web</td><td><a href="https://www.strattec.com">www.strattec.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.14 |
| 2019 | 0.56 |
| 2018 | 0.56 |
| 2017 | 0.56 |
| 2016 | 0.54 |
| 2015 | 0.5 |
| 2014 | 0.46 |
| 2013 | 0.22 |
| 2012 | 0.6 |
| 2011 | 0.2 |
| 2010 | 1.2 |
| 2008 | 0.6 |
| 2007 | 1.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
