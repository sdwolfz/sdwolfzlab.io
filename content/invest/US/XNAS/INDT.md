---
title: "INDUS REALTY TRUST INC (INDT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>INDUS REALTY TRUST INC</td></tr>
    <tr><td>Symbol</td><td>INDT</td></tr>
    <tr><td>Web</td><td><a href="https://www.indusrt.com">www.indusrt.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.99 |
| 2019 | 0.5 |
| 2018 | 0.45 |
| 2017 | 0.4 |
| 2016 | 0.3 |
| 2015 | 0.3 |
| 2014 | 0.2 |
| 2013 | 0.2 |
| 2012 | 0.2 |
| 2011 | 0.4 |
| 2010 | 0.4 |
| 2009 | 0.4 |
| 2008 | 0.4 |
| 2007 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
