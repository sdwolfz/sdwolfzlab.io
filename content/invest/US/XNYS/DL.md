---
title: "CHINA DISTANCE EDUCATIN HLDGS LTD SPON ADR EACH REP 4 ORD SHS (DL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CHINA DISTANCE EDUCATIN HLDGS LTD SPON ADR EACH REP 4 ORD SHS</td></tr>
    <tr><td>Symbol</td><td>DL</td></tr>
    <tr><td>Web</td><td><a href="http://www.cdeledu.com">www.cdeledu.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.56 |
| 2017 | 0.45 |
| 2016 | 0.9 |
| 2015 | 0.8 |
| 2014 | 0.58 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
