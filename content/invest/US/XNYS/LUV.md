---
title: "SOUTHWEST AIRLINES CO (LUV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SOUTHWEST AIRLINES CO</td></tr>
    <tr><td>Symbol</td><td>LUV</td></tr>
    <tr><td>Web</td><td><a href="https://www.southwest.com">www.southwest.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.18 |
| 2019 | 0.7 |
| 2018 | 0.605 |
| 2017 | 0.475 |
| 2016 | 0.375 |
| 2015 | 0.285 |
| 2014 | 0.22 |
| 2013 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
