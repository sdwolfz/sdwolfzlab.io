---
title: "BECTON DICKINSON & CO (BDX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BECTON DICKINSON & CO</td></tr>
    <tr><td>Symbol</td><td>BDX</td></tr>
    <tr><td>Web</td><td><a href="https://www.bd.com">www.bd.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.66 |
| 2020 | 3.2 |
| 2019 | 3.1 |
| 2018 | 3.02 |
| 2017 | 2.94 |
| 2016 | 2.71 |
| 2015 | 2.46 |
| 2014 | 2.235 |
| 2013 | 1.04 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
