---
title: "OFS CAPITAL CORPORATION (OFS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>OFS CAPITAL CORPORATION</td></tr>
    <tr><td>Symbol</td><td>OFS</td></tr>
    <tr><td>Web</td><td><a href="https://www.ofscapital.com">www.ofscapital.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.86 |
| 2019 | 1.36 |
| 2018 | 1.73 |
| 2017 | 1.36 |
| 2016 | 1.36 |
| 2015 | 1.36 |
| 2014 | 1.36 |
| 2013 | 1.19 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
