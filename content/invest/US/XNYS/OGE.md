---
title: "OGE ENERGY CORP (OGE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>OGE ENERGY CORP</td></tr>
    <tr><td>Symbol</td><td>OGE</td></tr>
    <tr><td>Web</td><td><a href="https://www.ogeenergy.com">www.ogeenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.806 |
| 2020 | 1.564 |
| 2019 | 1.482 |
| 2018 | 1.364 |
| 2017 | 1.242 |
| 2016 | 1.128 |
| 2015 | 1.025 |
| 2014 | 0.925 |
| 2013 | 0.418 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
