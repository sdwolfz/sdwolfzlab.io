---
title: "AMERICAN ELECTRIC POWER CO INC (AEP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AMERICAN ELECTRIC POWER CO INC</td></tr>
    <tr><td>Symbol</td><td>AEP</td></tr>
    <tr><td>Web</td><td><a href="https://www.aep.com">www.aep.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.48 |
| 2020 | 2.84 |
| 2019 | 2.71 |
| 2018 | 2.53 |
| 2017 | 2.39 |
| 2016 | 2.27 |
| 2015 | 2.15 |
| 2014 | 2.03 |
| 2013 | 1.95 |
| 2012 | 1.88 |
| 2011 | 1.85 |
| 2010 | 1.71 |
| 2009 | 1.64 |
| 2008 | 1.64 |
| 2007 | 1.58 |
| 2006 | 1.5 |
| 2005 | 1.42 |
| 2004 | 1.05 |
| 2003 | 1.65 |
| 2002 | 2.4 |
| 2001 | 2.4 |
| 2000 | 1.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
