---
title: "MASTERCRAFT BOAT HOLDINGS INC (MCFT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MASTERCRAFT BOAT HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>MCFT</td></tr>
    <tr><td>Web</td><td><a href="https://www.mastercraft.com">www.mastercraft.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 4.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
