---
title: "Tesco (TSCO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Tesco</td></tr>
    <tr><td>Symbol</td><td>TSCO</td></tr>
    <tr><td>Web</td><td><a href="https://www.tescoplc.com">www.tescoplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 11.59 |
| 2019 | 7.31 |
| 2018 | 3.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
