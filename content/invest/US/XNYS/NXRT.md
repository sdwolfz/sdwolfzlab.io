---
title: " (NXRT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>NXRT</td></tr>
    <tr><td>Web</td><td><a href="https://www.nexpointliving.com">www.nexpointliving.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.682 |
| 2020 | 1.28 |
| 2019 | 1.138 |
| 2018 | 1.025 |
| 2017 | 0.91 |
| 2016 | 0.838 |
| 2015 | 0.618 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
