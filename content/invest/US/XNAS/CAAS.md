---
title: "CHINA AUTOMOTIVE SYSTEMS INC (CAAS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CHINA AUTOMOTIVE SYSTEMS INC</td></tr>
    <tr><td>Symbol</td><td>CAAS</td></tr>
    <tr><td>Web</td><td><a href="https://www.caasauto.com">www.caasauto.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 0.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
