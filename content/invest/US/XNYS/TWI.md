---
title: "TITAN INTL INC (TWI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TITAN INTL INC</td></tr>
    <tr><td>Symbol</td><td>TWI</td></tr>
    <tr><td>Web</td><td><a href="https://www.titan-intl.com">www.titan-intl.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.005 |
| 2019 | 0.02 |
| 2018 | 0.02 |
| 2017 | 0.02 |
| 2016 | 0.02 |
| 2015 | 0.02 |
| 2014 | 0.02 |
| 2013 | 0.015 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
