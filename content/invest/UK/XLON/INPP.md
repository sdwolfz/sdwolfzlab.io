---
title: "International Public Partnerships (INPP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>International Public Partnerships</td></tr>
    <tr><td>Symbol</td><td>INPP</td></tr>
    <tr><td>Web</td><td><a href="https://www.internationalpublicpartnerships.com">www.internationalpublicpartnerships.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 7.18 |
| 2018 | 7.0 |
| 2017 | 6.82 |
| 2016 | 6.65 |
| 2015 | 6.45 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
