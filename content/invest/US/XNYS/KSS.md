---
title: "KOHLS CORPORATION (KSS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>KOHLS CORPORATION</td></tr>
    <tr><td>Symbol</td><td>KSS</td></tr>
    <tr><td>Web</td><td><a href="https://www.kohlscorporation.com">www.kohlscorporation.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 0.704 |
| 2019 | 2.68 |
| 2018 | 2.44 |
| 2017 | 2.2 |
| 2016 | 2.0 |
| 2015 | 1.8 |
| 2014 | 1.56 |
| 2013 | 0.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
