---
title: "DOUBLELINE YIELD OPPORTUNITIES FUND (DLY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DOUBLELINE YIELD OPPORTUNITIES FUND</td></tr>
    <tr><td>Symbol</td><td>DLY</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.585 |
| 2020 | 1.053 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
