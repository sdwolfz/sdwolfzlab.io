---
title: "COMSTOCK RESOURCES INC (CRK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>COMSTOCK RESOURCES INC</td></tr>
    <tr><td>Symbol</td><td>CRK</td></tr>
    <tr><td>Web</td><td><a href="https://www.comstockresources.com">www.comstockresources.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 0.5 |
| 2013 | 0.375 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
