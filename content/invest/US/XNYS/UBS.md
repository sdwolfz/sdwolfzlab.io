---
title: "UBS GROUP AG (UBS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>UBS GROUP AG</td></tr>
    <tr><td>Symbol</td><td>UBS</td></tr>
    <tr><td>Web</td><td><a href="https://www.ubs.com">www.ubs.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 0.119 |
| 2018 | 0.69 |
| 2017 | 0.6 |
| 2016 | 0.888 |
| 2015 | 1.04 |
| 2014 | 0.282 |
| 2013 | 0.161 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
