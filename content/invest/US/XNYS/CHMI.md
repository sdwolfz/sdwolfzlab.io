---
title: " (CHMI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CHMI</td></tr>
    <tr><td>Web</td><td><a href="https://www.chmireit.com">www.chmireit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.27 |
| 2020 | 1.21 |
| 2019 | 1.78 |
| 2018 | 1.96 |
| 2017 | 1.96 |
| 2016 | 2.11 |
| 2015 | 1.98 |
| 2014 | 2.54 |
| 2013 | 0.45 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
