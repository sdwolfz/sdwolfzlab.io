---
title: "ZIX CORPORATION (ZIXI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ZIX CORPORATION</td></tr>
    <tr><td>Symbol</td><td>ZIXI</td></tr>
    <tr><td>Web</td><td><a href="https://www.zix.com">www.zix.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 1995 | 0.02 |
| 1994 | 0.08 |
| 1993 | 0.063 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
