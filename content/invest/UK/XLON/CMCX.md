---
title: "Cmc Mkts (CMCX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Cmc Mkts</td></tr>
    <tr><td>Symbol</td><td>CMCX</td></tr>
    <tr><td>Web</td><td><a href="https://www.cmcmarkets.com">www.cmcmarkets.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 15.03 |
| 2019 | 2.03 |
| 2018 | 8.93 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
