---
title: "KIRKLAND'S INC (KIRK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>KIRKLAND'S INC</td></tr>
    <tr><td>Symbol</td><td>KIRK</td></tr>
    <tr><td>Web</td><td><a href="https://www.kirklands.com">www.kirklands.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 1.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
