---
title: "JPMorgan Mid Cap (JMF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>JPMorgan Mid Cap</td></tr>
    <tr><td>Symbol</td><td>JMF</td></tr>
    <tr><td>Web</td><td><a href="https://www.jpmmidcap.com">www.jpmmidcap.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 29.0 |
| 2019 | 29.5 |
| 2018 | 28.0 |
| 2017 | 26.0 |
| 2016 | 25.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
