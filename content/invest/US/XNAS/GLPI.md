---
title: " (GLPI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>GLPI</td></tr>
    <tr><td>Web</td><td><a href="https://www.glpropinc.com">www.glpropinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.65 |
| 2020 | 2.5 |
| 2019 | 2.74 |
| 2018 | 2.57 |
| 2017 | 2.5 |
| 2016 | 2.32 |
| 2015 | 2.18 |
| 2014 | 14.33 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
