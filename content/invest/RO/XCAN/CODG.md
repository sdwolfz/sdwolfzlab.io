---
title: "COMTRAM SA SIBIU (CODG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/bvb/">BVB</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/bvb/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>COMTRAM SA SIBIU</td></tr>
    <tr><td>Symbol</td><td>CODG</td></tr>
    <tr><td>Web</td><td><a href="http://www.comtram.ro">www.comtram.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
