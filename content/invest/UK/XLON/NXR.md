---
title: "Norcros (NXR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Norcros</td></tr>
    <tr><td>Symbol</td><td>NXR</td></tr>
    <tr><td>Web</td><td><a href="https://www.norcros.com">www.norcros.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 3.1 |
| 2019 | 8.4 |
| 2018 | 7.8 |
| 2017 | 7.2 |
| 2016 | 6.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
