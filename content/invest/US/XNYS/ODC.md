---
title: "OIL DRI CORP OF AMERICA (ODC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>OIL DRI CORP OF AMERICA</td></tr>
    <tr><td>Symbol</td><td>ODC</td></tr>
    <tr><td>Web</td><td><a href="https://www.oildri.com">www.oildri.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.52 |
| 2020 | 1.02 |
| 2019 | 0.98 |
| 2018 | 0.94 |
| 2017 | 0.9 |
| 2016 | 0.86 |
| 2015 | 0.82 |
| 2014 | 0.78 |
| 2013 | 0.38 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
