---
title: "Next (NXT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Next</td></tr>
    <tr><td>Symbol</td><td>NXT</td></tr>
    <tr><td>Web</td><td><a href="https://www.next.co.uk">www.next.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 174.0 |
| 2019 | 165.0 |
| 2018 | 338.0 |
| 2017 | 158.0 |
| 2016 | 398.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
