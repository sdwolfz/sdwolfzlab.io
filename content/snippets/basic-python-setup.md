---
title: "Basic python setup"
date: 2020-12-21T11:38:59Z
type: posts
slug: basic-python-setup
series: ["Basics"]
tags: ["python", "linux", "development"]
---

## Install

Since you undoubtedly use linux, `python` should come pre-installed, but here is
the install command anyway:

```sh
sudo pacman -S python python-pip
```

## Configure

Add this to your `~/.bash_profile`:

```sh
export PATH=~/.local/bin:$PATH
```

## Packages

```sh
pip install --user -U package-name
```

## Cleanup

When a new python verson is released, clean up the old versions from
`~/.local/lib`:

```sh
ls -la ~/.local/lib
```
