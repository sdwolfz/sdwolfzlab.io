---
title: "GLOBAL BLUE GROUP HOLDING AG (GB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GLOBAL BLUE GROUP HOLDING AG</td></tr>
    <tr><td>Symbol</td><td>GB</td></tr>
    <tr><td>Web</td><td><a href="https://www.globalblue.com">www.globalblue.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
