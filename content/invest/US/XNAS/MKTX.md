---
title: "MARKETAXESS HLDGS INC (MKTX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MARKETAXESS HLDGS INC</td></tr>
    <tr><td>Symbol</td><td>MKTX</td></tr>
    <tr><td>Web</td><td><a href="https://www.marketaxess.com">www.marketaxess.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.32 |
| 2020 | 2.4 |
| 2019 | 2.04 |
| 2018 | 1.68 |
| 2017 | 1.32 |
| 2016 | 1.04 |
| 2015 | 0.8 |
| 2014 | 0.64 |
| 2013 | 0.52 |
| 2012 | 1.74 |
| 2011 | 0.36 |
| 2010 | 0.28 |
| 2009 | 0.07 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
