---
title: "Wh Smith (SMWH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Wh Smith</td></tr>
    <tr><td>Symbol</td><td>SMWH</td></tr>
    <tr><td>Web</td><td><a href="https://www.whsmithplc.co.uk">www.whsmithplc.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 58.2 |
| 2018 | 54.1 |
| 2017 | 48.2 |
| 2016 | 43.9 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
