---
title: "GREENE COUNTY BANCORP INC (GCBC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GREENE COUNTY BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>GCBC</td></tr>
    <tr><td>Web</td><td><a href="https://www.tbogc.com">www.tbogc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.24 |
| 2020 | 0.46 |
| 2019 | 0.42 |
| 2018 | 0.395 |
| 2017 | 0.385 |
| 2016 | 0.467 |
| 2015 | 0.73 |
| 2014 | 0.71 |
| 2013 | 0.7 |
| 2012 | 0.7 |
| 2011 | 0.7 |
| 2010 | 0.895 |
| 2009 | 0.68 |
| 2008 | 0.65 |
| 2007 | 0.64 |
| 2006 | 0.46 |
| 2005 | 0.66 |
| 2004 | 0.82 |
| 2003 | 0.7 |
| 2002 | 0.6 |
| 2001 | 0.37 |
| 2000 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
