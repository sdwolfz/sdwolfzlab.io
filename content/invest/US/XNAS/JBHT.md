---
title: "HUNT(J.B.)TRANSPORT SERVICES INC (JBHT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HUNT(J.B.)TRANSPORT SERVICES INC</td></tr>
    <tr><td>Symbol</td><td>JBHT</td></tr>
    <tr><td>Web</td><td><a href="https://www.jbhunt.com">www.jbhunt.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.58 |
| 2020 | 1.08 |
| 2019 | 1.04 |
| 2018 | 0.96 |
| 2017 | 0.92 |
| 2016 | 0.88 |
| 2015 | 0.84 |
| 2014 | 0.8 |
| 2013 | 0.45 |
| 2012 | 0.71 |
| 2011 | 0.52 |
| 2010 | 0.48 |
| 2009 | 0.44 |
| 2008 | 0.4 |
| 2007 | 0.36 |
| 2006 | 0.32 |
| 2005 | 0.36 |
| 2004 | 0.09 |
| 2000 | 0.05 |
| 1999 | 0.2 |
| 1998 | 0.2 |
| 1997 | 0.2 |
| 1996 | 0.2 |
| 1995 | 0.2 |
| 1994 | 0.2 |
| 1993 | 0.2 |
| 1992 | 0.22 |
| 1991 | 0.28 |
| 1990 | 0.24 |
| 1989 | 0.24 |
| 1988 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
