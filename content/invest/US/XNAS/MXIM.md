---
title: "MAXIM INTEGRATED PRODUCTS (MXIM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MAXIM INTEGRATED PRODUCTS</td></tr>
    <tr><td>Symbol</td><td>MXIM</td></tr>
    <tr><td>Web</td><td><a href="https://www.maximintegrated.com">www.maximintegrated.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.44 |
| 2019 | 1.88 |
| 2018 | 1.76 |
| 2017 | 1.38 |
| 2016 | 1.26 |
| 2015 | 1.16 |
| 2014 | 1.08 |
| 2013 | 1.0 |
| 2012 | 0.92 |
| 2011 | 0.86 |
| 2010 | 0.82 |
| 2009 | 0.8 |
| 2008 | 0.775 |
| 2007 | 0.687 |
| 2006 | 0.562 |
| 2005 | 0.425 |
| 2004 | 0.34 |
| 2003 | 0.22 |
| 2002 | 0.02 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
