---
title: "TOP SHIPS INC (TOPS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TOP SHIPS INC</td></tr>
    <tr><td>Symbol</td><td>TOPS</td></tr>
    <tr><td>Web</td><td><a href="https://www.topships.org">www.topships.org</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2006 | 7.71 |
| 2005 | 0.88 |
| 2004 | 0.21 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
