---
title: "Natwest Grp (NWG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Natwest Grp</td></tr>
    <tr><td>Symbol</td><td>NWG</td></tr>
    <tr><td>Web</td><td><a href="https://www.natwestgroup.com">www.natwestgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 3.0 |
| 2019 | 22.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
