---
title: "AMERICAN EAGLE OUTFITTERS INC (AEO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AMERICAN EAGLE OUTFITTERS INC</td></tr>
    <tr><td>Symbol</td><td>AEO</td></tr>
    <tr><td>Web</td><td><a href="https://www.ae.com">www.ae.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.276 |
| 2020 | 0.414 |
| 2019 | 0.414 |
| 2018 | 0.552 |
| 2017 | 0.5 |
| 2016 | 0.5 |
| 2015 | 0.5 |
| 2014 | 0.5 |
| 2013 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
