---
title: "HERITAGE FINANCIAL CORP (HFWA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HERITAGE FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>HFWA</td></tr>
    <tr><td>Web</td><td><a href="https://www.hf-wa.com">www.hf-wa.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.4 |
| 2020 | 0.8 |
| 2019 | 0.84 |
| 2018 | 0.72 |
| 2017 | 0.61 |
| 2016 | 0.72 |
| 2015 | 0.53 |
| 2014 | 0.5 |
| 2013 | 0.42 |
| 2012 | 0.8 |
| 2011 | 0.38 |
| 2009 | 0.1 |
| 2008 | 0.77 |
| 2007 | 0.84 |
| 2006 | 0.79 |
| 2005 | 0.71 |
| 2004 | 0.63 |
| 2003 | 0.55 |
| 2002 | 0.47 |
| 2001 | 0.39 |
| 2000 | 0.31 |
| 1999 | 0.23 |
| 1998 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
