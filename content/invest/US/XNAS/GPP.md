---
title: " (GPP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>GPP</td></tr>
    <tr><td>Web</td><td><a href="https://www.greenplainspartners.com">www.greenplainspartners.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.24 |
| 2020 | 0.835 |
| 2019 | 1.9 |
| 2018 | 1.895 |
| 2017 | 1.78 |
| 2016 | 1.638 |
| 2015 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
