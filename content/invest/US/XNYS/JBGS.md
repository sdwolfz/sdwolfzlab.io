---
title: " (JBGS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>JBGS</td></tr>
    <tr><td>Web</td><td><a href="https://www.jbgsmith.com">www.jbgsmith.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.225 |
| 2020 | 0.9 |
| 2019 | 0.9 |
| 2018 | 1.0 |
| 2017 | 0.45 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
