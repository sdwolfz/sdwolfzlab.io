---
title: "Picture in Picture on all workspaces"
date: 2020-12-23T10:55:36Z
type: posts
slug: picture-in-picture-on-all-workspaces
series: ["Hacks"]
tags: ["linux", "xfce4", "desktop", "firefox"]
---

## Prerequisites

```sh
sudo pacman -S wmctrl
```

## How

1. Make the `Firefox` window `Always on Top` via right click on the title bar.

2. Open a video and activate `Picture in Picture` in `Firefox`.

3. Execute:

```sh
wmctrl -r Picture-in-Picture -b add,sticky,above
```

Now you can view the `Picture in Picture` window on all workspaces.
