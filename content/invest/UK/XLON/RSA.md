---
title: "RSA Insurance (RSA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>RSA Insurance</td></tr>
    <tr><td>Symbol</td><td>RSA</td></tr>
    <tr><td>Web</td><td><a href="https://www.rsagroup.com">www.rsagroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 23.1 |
| 2018 | 21.0 |
| 2017 | 19.6 |
| 2016 | 61.0 |
| 2015 | 10.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
