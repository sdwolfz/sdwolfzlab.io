---
title: "Clipper (CLG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Clipper</td></tr>
    <tr><td>Symbol</td><td>CLG</td></tr>
    <tr><td>Web</td><td><a href="https://www.clippergroup.co.uk">www.clippergroup.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 9.7 |
| 2019 | 9.7 |
| 2018 | 8.4 |
| 2017 | 7.2 |
| 2016 | 6.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
