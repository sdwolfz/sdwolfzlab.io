---
title: "Robert Walters (RWA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Robert Walters</td></tr>
    <tr><td>Symbol</td><td>RWA</td></tr>
    <tr><td>Web</td><td><a href="https://www.robertwaltersgroup.com">www.robertwaltersgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 15.5 |
| 2018 | 14.7 |
| 2017 | 12.05 |
| 2016 | 8.5 |
| 2015 | 7.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
