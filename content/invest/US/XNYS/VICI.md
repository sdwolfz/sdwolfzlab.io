---
title: " (VICI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>VICI</td></tr>
    <tr><td>Web</td><td><a href="https://www.viciproperties.com">www.viciproperties.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.33 |
| 2020 | 1.256 |
| 2019 | 1.17 |
| 2018 | 0.996 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
