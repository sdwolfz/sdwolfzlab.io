---
title: "DANA INCORPORATED (DAN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DANA INCORPORATED</td></tr>
    <tr><td>Symbol</td><td>DAN</td></tr>
    <tr><td>Web</td><td><a href="https://www.dana.com">www.dana.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.1 |
| 2019 | 0.4 |
| 2018 | 0.4 |
| 2017 | 0.24 |
| 2016 | 0.24 |
| 2015 | 0.23 |
| 2014 | 0.2 |
| 2013 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
