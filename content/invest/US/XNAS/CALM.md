---
title: "CAL MAINE FOODS INC (CALM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CAL MAINE FOODS INC</td></tr>
    <tr><td>Symbol</td><td>CALM</td></tr>
    <tr><td>Web</td><td><a href="https://www.calmainefoods.com">www.calmainefoods.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.034 |
| 2019 | 0.421 |
| 2018 | 0.436 |
| 2016 | 1.192 |
| 2015 | 1.902 |
| 2014 | 1.767 |
| 2013 | 0.69 |
| 2012 | 1.34 |
| 2011 | 0.828 |
| 2010 | 1.016 |
| 2009 | 0.956 |
| 2008 | 1.48 |
| 2007 | 0.05 |
| 2006 | 0.05 |
| 2005 | 0.05 |
| 2004 | 0.05 |
| 2003 | 0.05 |
| 2002 | 0.05 |
| 2001 | 0.05 |
| 2000 | 0.05 |
| 1999 | 0.05 |
| 1998 | 0.04 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
