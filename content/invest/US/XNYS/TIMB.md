---
title: " (TIMB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>TIMB</td></tr>
    <tr><td>Web</td><td><a href="https://ri.tim.com.br">ri.tim.com.br</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.541 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
