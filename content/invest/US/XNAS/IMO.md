---
title: "IMPERIAL OIL (IMO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>IMPERIAL OIL</td></tr>
    <tr><td>Symbol</td><td>IMO</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.175 |
| 2020 | 0.661 |
| 2019 | 0.638 |
| 2018 | 0.561 |
| 2017 | 0.486 |
| 2016 | 0.446 |
| 2015 | 0.425 |
| 2014 | 0.472 |
| 2013 | 0.47 |
| 2012 | 0.481 |
| 2011 | 0.441 |
| 2010 | 0.305 |
| 2009 | 1.221 |
| 2008 | 0.089 |
| 2007 | 0.329 |
| 2006 | 0.211 |
| 2005 | 1.105 |
| 2004 | 0.677 |
| 2003 | 0.63 |
| 2002 | 0.53 |
| 2001 | 0.62 |
| 2000 | 0.52 |
| 1999 | 0.195 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
