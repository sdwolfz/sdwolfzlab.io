---
title: "RELIANT BANCORP INC (RBNC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>RELIANT BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>RBNC</td></tr>
    <tr><td>Web</td><td><a href="https://www.reliantbank.com">www.reliantbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.24 |
| 2020 | 0.4 |
| 2019 | 0.36 |
| 2018 | 0.3 |
| 2017 | 0.4 |
| 2016 | 0.2 |
| 2013 | 0.2 |
| 2012 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
