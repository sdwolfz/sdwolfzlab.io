---
title: "AAGES S.A. (AAG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>AAGES S.A.</td></tr>
    <tr><td>Symbol</td><td>AAG</td></tr>
    <tr><td>Web</td><td><a href="https://www.aages.ro">www.aages.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.1 |
| 2019 | 0.21 |
| 2018 | 0.15 |
| 2017 | 0.09 |

### Reports

<style>
  .reports-table {
    width: 100%;
    display: block;
    overflow-x: auto;
    overflow-y: scroll;
    scrollbar-width: none;
    max-height: 50rem;
    border: 0.5rem solid #D8D8D8;
  }
  .reports-table table {
    border: none;
    padding: 0;
  }
  .reports-table th {
    border: none;
    padding: 0;
  }
  .reports-table td {
    border: none
    padding: 0;
  }

  table.reports {
    border-collapse: collapse;
  }

  table.reports thead th, table.reports tbody td {
    padding: 3px 5px;
  }

  table.reports td {
    border: 0px solid #FFFFFF;
  }

  table.reports thead .header-top {
    background-color: #000000;
    color: #FFFFFF;
  }

  table.reports thead .header-assets {
    background-color: #3D85C6;
    color: #FFFFFF;
  }

  table.reports thead .header-assets-non {
    background-color: #A2C4C9;
  }

  table.reports thead .header-assets-cur {
    background-color: #9FC5E8;
  }

  table.reports thead .header-assets-tot {
    background-color: #3D85C6;
    color: #FFFFFF;
  }

  table.reports thead .header-equity {
    background-color: #6AA84F;
    color: #FFFFFF;
  }

  table.reports thead .header-lia-non {
    background-color: #D5A6BD;
  }

  table.reports thead .header-lia-cur {
    background-color: #EA9999;
  }

  table.reports thead .header-lia-tot {
    background-color: #A64D79;
    color: #FFFFFF;
  }

  table.reports thead .header-equity {
    background-color: #6AA84F;
    color: #FFFFFF;
  }

  table.reports thead .header-liabilities {
    background-color: #A64D79;
    color: #FFFFFF;
  }

  table.reports thead .header-revenue {
    background-color: #674EA6;
    color: #FFFFFF;
  }

  table.reports thead .header-cashflow {
    background-color: #E69138;
    color: #FFFFFF;
  }

  table.reports tbody td.number {
    text-align: right;
  }

  table.reports tbody td.year-even, table.reports tbody td.quarter-even {
    text-align: center;
    background-color: #B7B7B7;
  }

  table.reports tbody td.year-odd, table.reports tbody td.quarter-odd {
    text-align: center;
    background-color: #D9D9D9;
  }

  table.reports tbody td.assets-non-h2 {
    background-color: #D0E0E3;
  }
  table.reports tbody td.assets-cur-h2 {
    background-color: #CFE2F3;
  }
  table.reports tbody td.assets-tot-h2 {
    background-color: #CFE2F3;
  }
  table.reports tbody td.assets-non-t3 {
    background-color: #A2C4C9;
  }
  table.reports tbody td.assets-cur-t3 {
    background-color: #9FC5E8;
  }
  table.reports tbody td.assets-tot-t3 {
    background-color: #9FC5E8;
  }
  table.reports tbody td.assets-non-a4 {
    background-color: #76A5AF;
  }
  table.reports tbody td.assets-cur-a4 {
    background-color: #6FA8DC;
  }
  table.reports tbody td.assets-tot-a4 {
    background-color: #6FA8DC;
  }

  table.reports tbody td.equity-h2 {
    background-color: #D9EAD3;
  }
  table.reports tbody td.equity-t3 {
    background-color: #B6D7A8;
  }
  table.reports tbody td.equity-a4 {
    background-color: #93C47D;
  }

  table.reports tbody td.liabilities-non-h2 {
    background-color: #EAD1DC;
  }
  table.reports tbody td.liabilities-cur-h2 {
    background-color: #F4CCCC;
  }
  table.reports tbody td.liabilities-tot-h2 {
    background-color: #F4CCCC;
  }
  table.reports tbody td.liabilities-non-t3 {
    background-color: #D5A6BD;
  }
  table.reports tbody td.liabilities-cur-t3 {
    background-color: #EA9999;
  }
  table.reports tbody td.liabilities-tot-t3 {
    background-color: #EA9999;
  }
  table.reports tbody td.liabilities-non-a4 {
    background-color: #C27BA0;
  }
  table.reports tbody td.liabilities-cur-a4 {
    background-color: #E06666;
  }
  table.reports tbody td.liabilities-tot-a4 {
    background-color: #E06666;
  }

  table.reports tbody td.revenue-h2 {
    background-color: #D9D2E9;
  }
  table.reports tbody td.revenue-t3 {
    background-color: #B4A7D6;
  }
  table.reports tbody td.revenue-a4 {
    background-color: #8E7CC3;
  }

  table.reports tbody td.cashflow-h2 {
    background-color: #FCE5CD;
  }
  table.reports tbody td.cashflow-t3 {
    background-color: #F9CB9C;
  }
  table.reports tbody td.cashflow-a4 {
    background-color: #F6B26B;
  }
</style>

<div class="reports-table">
  <table class="reports">
    <thead>
      <tr>
        <th rowspan="3" class="header-top">Year</th>
        <th rowspan="3" class="header-top">Quarter</th>
        <th colspan="7" class="header-top">Balance</th>
        <th colspan="9" class="header-top">Revenue</th>
        <th colspan="8" class="header-top">Cashflow</th>
      </tr>
      <tr>
        <th colspan="3" class="header-assets">Assets</th>
        <th             class="header-equity">Equity</th>
        <th colspan="3" class="header-liabilities">Liabilities</th>
        <th rowspan="2" class="header-revenue">Sales</th>
        <th rowspan="2" class="header-revenue">Operating Expenses</th>
        <th rowspan="2" class="header-revenue">Operating Profit</th>
        <th rowspan="2" class="header-revenue">Financial Resut</th>
        <th rowspan="2" class="header-revenue">Profit Before Taxes</th>
        <th rowspan="2" class="header-revenue">Tax On Gains</th>
        <th rowspan="2" class="header-revenue">Net Profit</th>
        <th rowspan="2" class="header-revenue">Other</th>
        <th rowspan="2" class="header-revenue">Total Result</th>
        <th rowspan="2" class="header-cashflow">Operating</th>
        <th rowspan="2" class="header-cashflow">Investment</th>
        <th rowspan="2" class="header-cashflow">Financing</th>
        <th rowspan="2" class="header-cashflow">Total</th>
        <th rowspan="2" class="header-cashflow">Cash at Start</th>
        <th rowspan="2" class="header-cashflow">Cash at End</th>
        <th rowspan="2" class="header-cashflow">Capex</th>
        <th rowspan="2" class="header-cashflow">Free Cash Flow</th>
      </tr>
      <tr>
        <th class="header-assets-non">Noncurrent</th>
        <th class="header-assets-cur">Current</th>
        <th class="header-assets-tot">Total</th>
        <th class="header-equity">Total</th>
        <th class="header-lia-non">Noncurrent</th>
        <th class="header-lia-cur">Current</th>
        <th class="header-lia-tot">Total</th>
      <tr>
    </thead>
    <tbody>
      <tr class="row-q1">
        <td rowspan="1" class="year-even">2021</td>
        <td class="quarter-even">Q1</td>
        <td class="number assets-non-q1">14,963,611</td>
        <td class="number assets-cur-q1">28,997,073</td>
        <td class="number assets-tot-q1">43,960,684</td>
        <td class="number equity-q1">30,735,895</td>
        <td class="number liabilities-non-q1">1,188,610</td>
        <td class="number liabilities-cur-q1">12,036,179</td>
        <td class="number liabilities-tot-q1">13,224,789</td>
        <td class="number revenue-q1">9,356,981</td>
        <td class="number revenue-q1">-7,420,646</td>
        <td class="number revenue-q1">1,936,335</td>
        <td class="number revenue-q1">14,181</td>
        <td class="number revenue-q1">1,950,516</td>
        <td class="number revenue-q1">-290,542</td>
        <td class="number revenue-q1">1,659,974</td>
        <td class="number revenue-q1">-2,464</td>
        <td class="number revenue-q1">1,657,510</td>
        <td class="number cashflow-q1">1,368,531</td>
        <td class="number cashflow-q1">-28,730</td>
        <td class="number cashflow-q1">-2,357,690</td>
        <td class="number cashflow-q1">-1,017,889</td>
        <td class="number cashflow-q1">7,131,371</td>
        <td class="number cashflow-q1">6,113,482</td>
        <td class="number cashflow-q1">-13,645</td>
        <td class="number cashflow-q1">1,354,886</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-odd">2020</td>
        <td class="quarter-odd">A4</td>
        <td class="number assets-non-a4">15,136,781</td>
        <td class="number assets-cur-a4">28,042,237</td>
        <td class="number assets-tot-a4">43,179,018</td>
        <td class="number equity-a4">29,074,356</td>
        <td class="number liabilities-non-a4">1,294,448</td>
        <td class="number liabilities-cur-a4">12,810,214</td>
        <td class="number liabilities-tot-a4">14,104,662</td>
        <td class="number revenue-a4">29,373,935</td>
        <td class="number revenue-a4">-26,952,275</td>
        <td class="number revenue-a4">2,421,660</td>
        <td class="number revenue-a4">-317,674</td>
        <td class="number revenue-a4">2,103,986</td>
        <td class="number revenue-a4">-184,690</td>
        <td class="number revenue-a4">1,919,296</td>
        <td class="number revenue-a4">-9,859</td>
        <td class="number revenue-a4">1,909,437</td>
        <td class="number cashflow-a4">4,318,280</td>
        <td class="number cashflow-a4">-359,614</td>
        <td class="number cashflow-a4">-3,295,533</td>
        <td class="number cashflow-a4">663,133</td>
        <td class="number cashflow-a4">6,468,238</td>
        <td class="number cashflow-a4">7,131,371</td>
        <td class="number cashflow-a4">-179,659</td>
        <td class="number cashflow-a4">4,138,621</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-odd">T3</td>
        <td class="number assets-non-t3">15,241,268</td>
        <td class="number assets-cur-t3">29,132,911</td>
        <td class="number assets-tot-t3">44,374,179</td>
        <td class="number equity-t3">29,306,192</td>
        <td class="number liabilities-non-t3">1,429,648</td>
        <td class="number liabilities-cur-t3">13,638,339</td>
        <td class="number liabilities-tot-t3">15,067,987</td>
        <td class="number revenue-t3">23,441,363</td>
        <td class="number revenue-t3">-20,913,614</td>
        <td class="number revenue-t3">2,527,749</td>
        <td class="number revenue-t3">-231,232</td>
        <td class="number revenue-t3">2,296,517</td>
        <td class="number revenue-t3">-265,453</td>
        <td class="number revenue-t3">2,031,064</td>
        <td class="number revenue-t3">-7,394</td>
        <td class="number revenue-t3">2,023,670</td>
        <td class="number cashflow-t3">6,589,883</td>
        <td class="number cashflow-t3">-284,104</td>
        <td class="number cashflow-t3">-5,308,937</td>
        <td class="number cashflow-t3">996,842</td>
        <td class="number cashflow-t3">6,468,238</td>
        <td class="number cashflow-t3">7,465,080</td>
        <td class="number cashflow-t3">-134,145</td>
        <td class="number cashflow-t3">6,455,738</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-odd">H2</td>
        <td class="number assets-non-h2">0</td>
        <td class="number assets-cur-h2">0</td>
        <td class="number assets-tot-h2">0</td>
        <td class="number equity-h2">0</td>
        <td class="number liabilities-non-h2">0</td>
        <td class="number liabilities-cur-h2">0</td>
        <td class="number liabilities-tot-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-odd">Q1</td>
        <td class="number assets-non-q1">0</td>
        <td class="number assets-cur-q1">0</td>
        <td class="number assets-tot-q1">0</td>
        <td class="number equity-q1">0</td>
        <td class="number liabilities-non-q1">0</td>
        <td class="number liabilities-cur-q1">0</td>
        <td class="number liabilities-tot-q1">0</td>
        <td class="number revenue-q1">7,785,335</td>
        <td class="number revenue-q1">-7,492,756</td>
        <td class="number revenue-q1">292,579</td>
        <td class="number revenue-q1">2,434</td>
        <td class="number revenue-q1">295,013</td>
        <td class="number revenue-q1">-29,579</td>
        <td class="number revenue-q1">265,434</td>
        <td class="number revenue-q1">-1,784</td>
        <td class="number revenue-q1">263,650</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-even">2019</td>
        <td class="quarter-even">A4</td>
        <td class="number assets-non-a4">15,747,161</td>
        <td class="number assets-cur-a4">29,686,958</td>
        <td class="number assets-tot-a4">45,434,119</td>
        <td class="number equity-a4">29,609,464</td>
        <td class="number liabilities-non-a4">1,795,919</td>
        <td class="number liabilities-cur-a4">14,028,736</td>
        <td class="number liabilities-tot-a4">15,824,655</td>
        <td class="number revenue-a4">36,722,003</td>
        <td class="number revenue-a4">-30,529,368</td>
        <td class="number revenue-a4">6,192,635</td>
        <td class="number revenue-a4">-165,652</td>
        <td class="number revenue-a4">6,026,983</td>
        <td class="number revenue-a4">-973,274</td>
        <td class="number revenue-a4">5,053,709</td>
        <td class="number revenue-a4">-7,139</td>
        <td class="number revenue-a4">5,046,570</td>
        <td class="number cashflow-a4">4,626,452</td>
        <td class="number cashflow-a4">-1,237,030</td>
        <td class="number cashflow-a4">-2,126,905</td>
        <td class="number cashflow-a4">1,262,517</td>
        <td class="number cashflow-a4">5,205,721</td>
        <td class="number cashflow-a4">6,468,238</td>
        <td class="number cashflow-a4">-122,112</td>
        <td class="number cashflow-a4">4,504,340</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-even">T3</td>
        <td class="number assets-non-t3">0</td>
        <td class="number assets-cur-t3">0</td>
        <td class="number assets-tot-t3">0</td>
        <td class="number equity-t3">0</td>
        <td class="number liabilities-non-t3">0</td>
        <td class="number liabilities-cur-t3">0</td>
        <td class="number liabilities-tot-t3">0</td>
        <td class="number revenue-t3">24,226,481</td>
        <td class="number revenue-t3">-21,570,368</td>
        <td class="number revenue-t3">2,656,113</td>
        <td class="number revenue-t3">-94,431</td>
        <td class="number revenue-t3">2,561,682</td>
        <td class="number revenue-t3">-338,602</td>
        <td class="number revenue-t3">2,223,080</td>
        <td class="number revenue-t3">-7,394</td>
        <td class="number revenue-t3">2,215,686</td>
        <td class="number cashflow-t3">4,626,452</td>
        <td class="number cashflow-t3">-1,237,030</td>
        <td class="number cashflow-t3">-2,126,905</td>
        <td class="number cashflow-t3">1,262,517</td>
        <td class="number cashflow-t3">5,205,721</td>
        <td class="number cashflow-t3">6,468,238</td>
        <td class="number cashflow-t3">-1,222,112</td>
        <td class="number cashflow-t3">3,404,340</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-even">H2</td>
        <td class="number assets-non-h2">0</td>
        <td class="number assets-cur-h2">0</td>
        <td class="number assets-tot-h2">0</td>
        <td class="number equity-h2">0</td>
        <td class="number liabilities-non-h2">0</td>
        <td class="number liabilities-cur-h2">0</td>
        <td class="number liabilities-tot-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-even">Q1</td>
        <td class="number assets-non-q1">0</td>
        <td class="number assets-cur-q1">0</td>
        <td class="number assets-tot-q1">0</td>
        <td class="number equity-q1">0</td>
        <td class="number liabilities-non-q1">0</td>
        <td class="number liabilities-cur-q1">0</td>
        <td class="number liabilities-tot-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-odd">2018</td>
        <td class="quarter-odd">A4</td>
        <td class="number assets-non-a4">0</td>
        <td class="number assets-cur-a4">0</td>
        <td class="number assets-tot-a4">0</td>
        <td class="number equity-a4">0</td>
        <td class="number liabilities-non-a4">0</td>
        <td class="number liabilities-cur-a4">0</td>
        <td class="number liabilities-tot-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-odd">T3</td>
        <td class="number assets-non-t3">0</td>
        <td class="number assets-cur-t3">0</td>
        <td class="number assets-tot-t3">0</td>
        <td class="number equity-t3">0</td>
        <td class="number liabilities-non-t3">0</td>
        <td class="number liabilities-cur-t3">0</td>
        <td class="number liabilities-tot-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-odd">H2</td>
        <td class="number assets-non-h2">0</td>
        <td class="number assets-cur-h2">0</td>
        <td class="number assets-tot-h2">0</td>
        <td class="number equity-h2">0</td>
        <td class="number liabilities-non-h2">0</td>
        <td class="number liabilities-cur-h2">0</td>
        <td class="number liabilities-tot-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-odd">Q1</td>
        <td class="number assets-non-q1">10,959,389</td>
        <td class="number assets-cur-q1">24,716,822</td>
        <td class="number assets-tot-q1">35,676,211</td>
        <td class="number equity-q1">21,527,738</td>
        <td class="number liabilities-non-q1">1,189,567</td>
        <td class="number liabilities-cur-q1">12,958,905</td>
        <td class="number liabilities-tot-q1">14,148,472</td>
        <td class="number revenue-q1">7,714,682</td>
        <td class="number revenue-q1">-7,217,898</td>
        <td class="number revenue-q1">496,784</td>
        <td class="number revenue-q1">-89,638</td>
        <td class="number revenue-q1">407,146</td>
        <td class="number revenue-q1">-106,847</td>
        <td class="number revenue-q1">300,299</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">300,299</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-even">2017</td>
        <td class="quarter-even">A4</td>
        <td class="number assets-non-a4">0</td>
        <td class="number assets-cur-a4">0</td>
        <td class="number assets-tot-a4">0</td>
        <td class="number equity-a4">0</td>
        <td class="number liabilities-non-a4">0</td>
        <td class="number liabilities-cur-a4">0</td>
        <td class="number liabilities-tot-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-even">T3</td>
        <td class="number assets-non-t3">0</td>
        <td class="number assets-cur-t3">0</td>
        <td class="number assets-tot-t3">0</td>
        <td class="number equity-t3">0</td>
        <td class="number liabilities-non-t3">0</td>
        <td class="number liabilities-cur-t3">0</td>
        <td class="number liabilities-tot-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-even">H2</td>
        <td class="number assets-non-h2">0</td>
        <td class="number assets-cur-h2">0</td>
        <td class="number assets-tot-h2">0</td>
        <td class="number equity-h2">0</td>
        <td class="number liabilities-non-h2">0</td>
        <td class="number liabilities-cur-h2">0</td>
        <td class="number liabilities-tot-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-even">Q1</td>
        <td class="number assets-non-q1">0</td>
        <td class="number assets-cur-q1">0</td>
        <td class="number assets-tot-q1">0</td>
        <td class="number equity-q1">0</td>
        <td class="number liabilities-non-q1">0</td>
        <td class="number liabilities-cur-q1">0</td>
        <td class="number liabilities-tot-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
      <tr class="row-a4">
        <td rowspan="4" class="year-odd">2016</td>
        <td class="quarter-odd">A4</td>
        <td class="number assets-non-a4">0</td>
        <td class="number assets-cur-a4">0</td>
        <td class="number assets-tot-a4">0</td>
        <td class="number equity-a4">0</td>
        <td class="number liabilities-non-a4">0</td>
        <td class="number liabilities-cur-a4">0</td>
        <td class="number liabilities-tot-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number revenue-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
        <td class="number cashflow-a4">0</td>
      </tr>
      <tr class="row-t3">
        <td class="quarter-odd">T3</td>
        <td class="number assets-non-t3">0</td>
        <td class="number assets-cur-t3">0</td>
        <td class="number assets-tot-t3">0</td>
        <td class="number equity-t3">0</td>
        <td class="number liabilities-non-t3">0</td>
        <td class="number liabilities-cur-t3">0</td>
        <td class="number liabilities-tot-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number revenue-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
        <td class="number cashflow-t3">0</td>
      </tr>
      <tr class="row-h2">
        <td class="quarter-odd">H2</td>
        <td class="number assets-non-h2">0</td>
        <td class="number assets-cur-h2">0</td>
        <td class="number assets-tot-h2">0</td>
        <td class="number equity-h2">0</td>
        <td class="number liabilities-non-h2">0</td>
        <td class="number liabilities-cur-h2">0</td>
        <td class="number liabilities-tot-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number revenue-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
        <td class="number cashflow-h2">0</td>
      </tr>
      <tr class="row-q1">
        <td class="quarter-odd">Q1</td>
        <td class="number assets-non-q1">0</td>
        <td class="number assets-cur-q1">0</td>
        <td class="number assets-tot-q1">0</td>
        <td class="number equity-q1">0</td>
        <td class="number liabilities-non-q1">0</td>
        <td class="number liabilities-cur-q1">0</td>
        <td class="number liabilities-tot-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number revenue-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
        <td class="number cashflow-q1">0</td>
      </tr>
    </tbody>
  </table>
</div>

## Valuation

**Coming soon...**
