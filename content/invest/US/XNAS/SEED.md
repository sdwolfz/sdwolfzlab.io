---
title: "ORIGIN AGRITECH (SEED)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ORIGIN AGRITECH</td></tr>
    <tr><td>Symbol</td><td>SEED</td></tr>
    <tr><td>Web</td><td><a href="https://www.originseed.com.cn">www.originseed.com.cn</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
