---
title: "CENTRAL VALLEY COMMUNITY (CVCY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CENTRAL VALLEY COMMUNITY</td></tr>
    <tr><td>Symbol</td><td>CVCY</td></tr>
    <tr><td>Web</td><td><a href="https://www.cvcb.com">www.cvcb.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.23 |
| 2020 | 0.44 |
| 2019 | 0.43 |
| 2018 | 0.31 |
| 2017 | 0.24 |
| 2016 | 0.24 |
| 2015 | 0.18 |
| 2014 | 0.2 |
| 2013 | 0.2 |
| 2012 | 0.05 |
| 2008 | 0.1 |
| 2007 | 0.1 |
| 2004 | 0.1 |
| 2003 | 0.1 |
| 2002 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
