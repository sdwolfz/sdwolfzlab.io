---
title: "CANADIAN IMPERIAL BANK OF COMMERCE (CM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CANADIAN IMPERIAL BANK OF COMMERCE</td></tr>
    <tr><td>Symbol</td><td>CM</td></tr>
    <tr><td>Web</td><td><a href="https://www.cibc.com">www.cibc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 4.393 |
| 2019 | 4.265 |
| 2018 | 4.139 |
| 2017 | 4.984 |
| 2016 | 3.641 |
| 2015 | 3.646 |
| 2014 | 4.01 |
| 2013 | 2.88 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
