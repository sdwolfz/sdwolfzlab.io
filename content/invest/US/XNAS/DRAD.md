---
title: "DIGIRAD CORP (DRAD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>DIGIRAD CORP</td></tr>
    <tr><td>Symbol</td><td>DRAD</td></tr>
    <tr><td>Web</td><td><a href="http://www.digirad.com">www.digirad.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.165 |
| 2017 | 0.21 |
| 2016 | 0.2 |
| 2015 | 0.2 |
| 2014 | 0.2 |
| 2013 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
