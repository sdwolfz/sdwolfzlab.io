---
title: "FARMERS & MERCHANTS BANCORP INC (FMAO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FARMERS & MERCHANTS BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>FMAO</td></tr>
    <tr><td>Web</td><td><a href="https://www.fm.bank">www.fm.bank</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.17 |
| 2020 | 0.66 |
| 2019 | 0.61 |
| 2018 | 0.56 |
| 2017 | 0.51 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
