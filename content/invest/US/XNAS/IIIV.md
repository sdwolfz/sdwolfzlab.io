---
title: "I3 VERTICALS INC (IIIV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>I3 VERTICALS INC</td></tr>
    <tr><td>Symbol</td><td>IIIV</td></tr>
    <tr><td>Web</td><td><a href="https://www.i3verticals.com">www.i3verticals.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
