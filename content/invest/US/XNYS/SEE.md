---
title: "SEALED AIR CORP (SEE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SEALED AIR CORP</td></tr>
    <tr><td>Symbol</td><td>SEE</td></tr>
    <tr><td>Web</td><td><a href="https://www.sealedair.com">www.sealedair.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.16 |
| 2020 | 0.64 |
| 2019 | 0.64 |
| 2018 | 0.64 |
| 2017 | 0.64 |
| 2016 | 0.61 |
| 2015 | 0.52 |
| 2014 | 0.52 |
| 2013 | 0.26 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
