---
title: " (CEA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CEA</td></tr>
    <tr><td>Web</td><td><a href="https://www.ceair.com">www.ceair.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.319 |
| 2018 | 0.361 |
| 2017 | 0.322 |
| 2016 | 0.339 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
