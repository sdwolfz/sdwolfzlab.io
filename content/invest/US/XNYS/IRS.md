---
title: " (IRS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>IRS</td></tr>
    <tr><td>Web</td><td><a href="https://www.irsa.com.ar">www.irsa.com.ar</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 1.362 |
| 2014 | 0.044 |
| 2013 | 0.472 |
| 2012 | 0.589 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
