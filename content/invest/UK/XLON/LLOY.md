---
title: "Lloyds (LLOY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Lloyds</td></tr>
    <tr><td>Symbol</td><td>LLOY</td></tr>
    <tr><td>Web</td><td><a href="https://www.lloydsbankinggroup.com">www.lloydsbankinggroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.57 |
| 2019 | 3.37 |
| 2018 | 3.21 |
| 2017 | 3.05 |
| 2016 | 3.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
