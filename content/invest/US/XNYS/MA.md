---
title: "MASTERCARD INCORPORATED (MA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MASTERCARD INCORPORATED</td></tr>
    <tr><td>Symbol</td><td>MA</td></tr>
    <tr><td>Web</td><td><a href="https://www.mastercard.com">www.mastercard.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.88 |
| 2020 | 1.6 |
| 2019 | 1.32 |
| 2018 | 1.0 |
| 2017 | 0.88 |
| 2016 | 0.76 |
| 2015 | 0.64 |
| 2014 | 0.39 |
| 2013 | 1.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
