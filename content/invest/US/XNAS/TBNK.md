---
title: "TERRITORIAL BANCOR (TBNK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TERRITORIAL BANCOR</td></tr>
    <tr><td>Symbol</td><td>TBNK</td></tr>
    <tr><td>Web</td><td><a href="https://www.territorialsavings.net">www.territorialsavings.net</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.56 |
| 2020 | 0.92 |
| 2019 | 1.49 |
| 2018 | 1.14 |
| 2017 | 1.2 |
| 2016 | 0.92 |
| 2015 | 0.76 |
| 2014 | 0.7 |
| 2013 | 0.62 |
| 2012 | 0.54 |
| 2011 | 0.34 |
| 2010 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
