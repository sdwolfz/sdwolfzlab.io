---
title: "HORMEL FOODS CORP (HRL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HORMEL FOODS CORP</td></tr>
    <tr><td>Symbol</td><td>HRL</td></tr>
    <tr><td>Web</td><td><a href="https://www.hormelfoods.com">www.hormelfoods.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.49 |
| 2020 | 0.932 |
| 2019 | 0.84 |
| 2018 | 0.752 |
| 2017 | 0.68 |
| 2016 | 0.58 |
| 2015 | 1.0 |
| 2014 | 0.8 |
| 2013 | 0.34 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
