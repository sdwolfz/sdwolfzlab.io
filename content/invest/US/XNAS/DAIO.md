---
title: "DATA I/O CORP (DAIO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>DATA I/O CORP</td></tr>
    <tr><td>Symbol</td><td>DAIO</td></tr>
    <tr><td>Web</td><td><a href="https://www.dataio.com">www.dataio.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 1989 | 4.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
