---
title: " (ESS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ESS</td></tr>
    <tr><td>Web</td><td><a href="https://www.essex.com">www.essex.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.09 |
| 2020 | 8.312 |
| 2019 | 7.8 |
| 2018 | 7.44 |
| 2017 | 7.0 |
| 2016 | 6.4 |
| 2015 | 5.76 |
| 2014 | 5.11 |
| 2013 | 3.63 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
