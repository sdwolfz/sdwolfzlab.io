---
title: "Regional Reit (RGL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Regional Reit</td></tr>
    <tr><td>Symbol</td><td>RGL</td></tr>
    <tr><td>Web</td><td><a href="https://www.regionalreit.com">www.regionalreit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 8.25 |
| 2018 | 8.05 |
| 2017 | 7.85 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
