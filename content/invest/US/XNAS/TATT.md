---
title: "TAT TECHNOLOGIES (TATT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TAT TECHNOLOGIES</td></tr>
    <tr><td>Symbol</td><td>TATT</td></tr>
    <tr><td>Web</td><td><a href="https://www.tat-technologies.com">www.tat-technologies.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 0.34 |
| 2016 | 0.34 |
| 2014 | 0.227 |
| 2012 | 0.283 |
| 2009 | 0.85 |
| 2007 | 0.4 |
| 2006 | 0.2 |
| 2005 | 0.18 |
| 2004 | 1.18 |
| 2003 | 0.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
