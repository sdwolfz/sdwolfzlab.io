---
title: " (ASRVP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ASRVP</td></tr>
    <tr><td>Web</td><td><a href="https://www.ameriserv.com">www.ameriserv.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.528 |
| 2020 | 2.113 |
| 2019 | 2.113 |
| 2018 | 2.113 |
| 2017 | 2.113 |
| 2016 | 2.113 |
| 2015 | 2.113 |
| 2014 | 2.113 |
| 2013 | 2.113 |
| 2012 | 2.113 |
| 2011 | 2.113 |
| 2010 | 2.113 |
| 2009 | 2.113 |
| 2008 | 2.112 |
| 2007 | 2.113 |
| 2006 | 2.112 |
| 2005 | 2.112 |
| 2004 | 2.112 |
| 2003 | 2.112 |
| 2002 | 2.112 |
| 2001 | 2.112 |
| 2000 | 2.112 |
| 1999 | 1.056 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
