---
title: " (NCTY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>NCTY</td></tr>
    <tr><td>Web</td><td><a href="https://www.the9.com">www.the9.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2009 | 1.07 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
