---
title: "ARCHER-DANIELS-MIDLAND CO (ADM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ARCHER-DANIELS-MIDLAND CO</td></tr>
    <tr><td>Symbol</td><td>ADM</td></tr>
    <tr><td>Web</td><td><a href="https://www.adm.com">www.adm.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.74 |
| 2020 | 1.44 |
| 2019 | 1.4 |
| 2018 | 1.34 |
| 2017 | 1.28 |
| 2016 | 1.2 |
| 2015 | 1.12 |
| 2014 | 0.96 |
| 2013 | 0.38 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
