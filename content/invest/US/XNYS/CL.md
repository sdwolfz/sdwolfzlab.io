---
title: "COLGATE-PALMOLIVE CO (CL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>COLGATE-PALMOLIVE CO</td></tr>
    <tr><td>Symbol</td><td>CL</td></tr>
    <tr><td>Web</td><td><a href="https://www.colgatepalmolive.com">www.colgatepalmolive.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.89 |
| 2020 | 1.75 |
| 2019 | 1.71 |
| 2018 | 1.66 |
| 2017 | 1.59 |
| 2016 | 1.55 |
| 2015 | 1.5 |
| 2014 | 1.42 |
| 2013 | 0.68 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
