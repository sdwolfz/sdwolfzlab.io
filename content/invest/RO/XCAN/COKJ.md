---
title: "CONCIVIA SA BRAILA (COKJ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>CONCIVIA SA BRAILA</td></tr>
    <tr><td>Symbol</td><td>COKJ</td></tr>
    <tr><td>Web</td><td><a href="https://www.concivia.ro">www.concivia.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2004 | 1.7292 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
