---
title: "PERPETUAL INCOME&GROWTH INV TST PLC ORD 10P (PLI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>PERPETUAL INCOME&GROWTH INV TST PLC ORD 10P</td></tr>
    <tr><td>Symbol</td><td>PLI</td></tr>
    <tr><td>Web</td><td><a href="http://www.invesco.co.uk">www.invesco.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 15.0 |
| 2019 | 14.5 |
| 2018 | 14.7 |
| 2017 | 14.05 |
| 2016 | 14.9 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
