---
title: "CATO CORP (CATO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CATO CORP</td></tr>
    <tr><td>Symbol</td><td>CATO</td></tr>
    <tr><td>Web</td><td><a href="https://www.catofashions.com">www.catofashions.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.33 |
| 2019 | 1.32 |
| 2018 | 1.32 |
| 2017 | 1.32 |
| 2016 | 1.29 |
| 2015 | 1.2 |
| 2014 | 1.2 |
| 2013 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
