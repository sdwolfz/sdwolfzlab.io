---
title: "FIRSTCASH INC (FCFS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRSTCASH INC</td></tr>
    <tr><td>Symbol</td><td>FCFS</td></tr>
    <tr><td>Web</td><td><a href="https://www.firstcash.com">www.firstcash.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.57 |
| 2020 | 1.08 |
| 2019 | 1.02 |
| 2018 | 0.91 |
| 2017 | 0.77 |
| 2016 | 0.565 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
