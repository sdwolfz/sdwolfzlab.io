---
title: "How to Use: sha256sum"
date: 2023-04-02T16:54:33Z
type: posts
slug: how-to-use-sha256sum
series: ["How to Use"]
tags: ["sha256", "linux", "shell", "security"]
---

## Prerequisites

Create a basic file:

```sh
echo "abc123" > test123.txt
```

## Digest

Find the `sha256sum` of a file with:

```sh
sha256sum test123.txt
```

You will get the following result:

```txt
5ecf8d2cc410094e8b82dd0bc178a57f3aa1e80916689beb00fe56148b1b1256  test123.txt
```

> NOTE: there are 2 spaces between the digest and file name. TWO&nbsp;&nbsp;SPACES!!!

Save that result into a file for easy checking:

```sh
sha256sum test123.txt > test123.txt.sha256
```

## Check

If there's a `sha256` file available, you can do:

```sh
sha256sum --check test123.txt.sha256
```

### Success

```txt
test123.txt: OK
```

### Failure

Change the file contents:

```sh
echo "zzz987" > test123.txt
```

Now you get a different message and exit code `1`:

```txt
test123.txt: FAILED
sha256sum: WARNING: 1 computed checksum did NOT match
```

## Scripting

In scripts, like Dockerfile RUN instructions you might wan to check the
sha256sum inline:

```sh
SHA=61bfc547a623d7305256611a81ecd24e6bf9dac555529ed6baeafcf8160900da
PACKAGE=your-file.tar.gz
echo "$SHA  $PACKAGE" | sha256sum -c -
```

> NOTE: there are 2 spaces between the digest and file name. TWO&nbsp;&nbsp;SPACES!!!
