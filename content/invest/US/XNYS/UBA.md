---
title: " (UBA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>UBA</td></tr>
    <tr><td>Web</td><td><a href="https://www.ubproperties.com">www.ubproperties.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 0.77 |
| 2019 | 1.1 |
| 2018 | 1.08 |
| 2017 | 1.325 |
| 2016 | 0.78 |
| 2015 | 1.025 |
| 2014 | 1.011 |
| 2013 | 0.752 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
