---
title: "St.Modwen Properties (SMP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>St.Modwen Properties</td></tr>
    <tr><td>Symbol</td><td>SMP</td></tr>
    <tr><td>Web</td><td><a href="https://www.stmodwen.co.uk">www.stmodwen.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.1 |
| 2019 | 8.7 |
| 2018 | 7.1 |
| 2017 | 6.28 |
| 2016 | 6.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
