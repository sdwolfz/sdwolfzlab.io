---
title: "CARVER BANCORP INC (CARV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CARVER BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>CARV</td></tr>
    <tr><td>Web</td><td><a href="https://www.carverbank.com">www.carverbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2010 | 0.15 |
| 2009 | 0.4 |
| 2008 | 0.4 |
| 2007 | 0.38 |
| 2006 | 0.34 |
| 2005 | 0.3 |
| 2004 | 0.24 |
| 2003 | 0.2 |
| 2002 | 0.05 |
| 2001 | 0.05 |
| 2000 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
