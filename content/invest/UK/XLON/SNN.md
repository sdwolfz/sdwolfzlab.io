---
title: "Sanne Group (SNN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Sanne Group</td></tr>
    <tr><td>Symbol</td><td>SNN</td></tr>
    <tr><td>Web</td><td><a href="https://www.sannegroup.com">www.sannegroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 13.8 |
| 2017 | 12.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
