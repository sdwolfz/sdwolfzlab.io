---
title: " (TEF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>TEF</td></tr>
    <tr><td>Web</td><td><a href="https://www.telefonica.com">www.telefonica.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.319 |
| 2019 | 0.338 |
| 2018 | 0.359 |
| 2017 | 0.352 |
| 2016 | 0.765 |
| 2015 | 1.153 |
| 2014 | 1.064 |
| 2013 | 0.362 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
