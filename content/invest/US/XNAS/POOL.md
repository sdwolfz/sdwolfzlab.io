---
title: "POOL CORPORATION (POOL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>POOL CORPORATION</td></tr>
    <tr><td>Symbol</td><td>POOL</td></tr>
    <tr><td>Web</td><td><a href="https://www.poolcorp.com">www.poolcorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.38 |
| 2020 | 2.29 |
| 2019 | 2.1 |
| 2018 | 1.72 |
| 2017 | 1.42 |
| 2016 | 1.19 |
| 2015 | 1.0 |
| 2014 | 0.85 |
| 2013 | 0.73 |
| 2012 | 0.62 |
| 2011 | 0.55 |
| 2010 | 0.52 |
| 2009 | 0.52 |
| 2008 | 0.51 |
| 2007 | 0.465 |
| 2006 | 0.405 |
| 2005 | 0.34 |
| 2004 | 0.27 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
