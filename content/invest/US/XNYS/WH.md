---
title: "WYNDHAM HOTELS & RESORTS INC (WH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>WYNDHAM HOTELS & RESORTS INC</td></tr>
    <tr><td>Symbol</td><td>WH</td></tr>
    <tr><td>Web</td><td><a href="https://www.wyndhamhotels.com">www.wyndhamhotels.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.16 |
| 2020 | 0.56 |
| 2019 | 1.16 |
| 2018 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
