---
title: " (NHI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>NHI</td></tr>
    <tr><td>Web</td><td><a href="https://www.nhireit.com">www.nhireit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.102 |
| 2020 | 4.408 |
| 2019 | 4.2 |
| 2018 | 4.0 |
| 2017 | 3.8 |
| 2016 | 3.6 |
| 2015 | 3.4 |
| 2014 | 3.08 |
| 2013 | 2.205 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
