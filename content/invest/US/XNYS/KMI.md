---
title: "KINDER MORGAN INC (KMI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>KINDER MORGAN INC</td></tr>
    <tr><td>Symbol</td><td>KMI</td></tr>
    <tr><td>Web</td><td><a href="https://www.kindermorgan.com">www.kindermorgan.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.532 |
| 2020 | 1.036 |
| 2019 | 0.95 |
| 2018 | 0.725 |
| 2017 | 0.5 |
| 2016 | 0.5 |
| 2015 | 1.93 |
| 2014 | 1.7 |
| 2013 | 0.81 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
