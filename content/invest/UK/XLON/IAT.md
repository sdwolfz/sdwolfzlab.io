---
title: "Invesco Asia (IAT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Invesco Asia</td></tr>
    <tr><td>Symbol</td><td>IAT</td></tr>
    <tr><td>Web</td><td><a href="https://www.invesco.co.uk">www.invesco.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 7.0 |
| 2019 | 5.7 |
| 2018 | 5.5 |
| 2017 | 4.3 |
| 2016 | 3.65 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
