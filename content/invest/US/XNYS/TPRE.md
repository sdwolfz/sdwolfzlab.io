---
title: "THIRD POINT REINSURANCE LTD (TPRE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>THIRD POINT REINSURANCE LTD</td></tr>
    <tr><td>Symbol</td><td>TPRE</td></tr>
    <tr><td>Web</td><td><a href="http://www.thirdpointre.bm">www.thirdpointre.bm</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
