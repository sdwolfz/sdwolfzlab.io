---
title: " (IHG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>IHG</td></tr>
    <tr><td>Web</td><td><a href="https://www.ihgplc.com">www.ihgplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.839 |
| 2019 | 3.761 |
| 2018 | 1.053 |
| 2017 | 0.978 |
| 2016 | 0.967 |
| 2015 | 0.775 |
| 2014 | 0.738 |
| 2013 | 0.225 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
