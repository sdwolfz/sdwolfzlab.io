---
title: "AMERIS BANCORP (ABCB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AMERIS BANCORP</td></tr>
    <tr><td>Symbol</td><td>ABCB</td></tr>
    <tr><td>Web</td><td><a href="https://www.amerisbank.com">www.amerisbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.15 |
| 2020 | 0.6 |
| 2019 | 0.5 |
| 2018 | 0.4 |
| 2017 | 0.4 |
| 2016 | 0.3 |
| 2015 | 0.2 |
| 2014 | 0.15 |
| 2009 | 0.1 |
| 2008 | 0.38 |
| 2007 | 0.56 |
| 2006 | 0.56 |
| 2005 | 0.56 |
| 2004 | 0.56 |
| 2003 | 0.52 |
| 2002 | 0.48 |
| 2001 | 0.48 |
| 2000 | 0.46 |
| 1999 | 0.4 |
| 1998 | 0.3 |
| 1997 | 0.4 |
| 1996 | 0.4 |
| 1995 | 0.4 |
| 1994 | 0.285 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
