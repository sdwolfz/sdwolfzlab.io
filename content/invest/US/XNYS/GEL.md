---
title: " (GEL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>GEL</td></tr>
    <tr><td>Web</td><td><a href="https://www.genesisenergy.com">www.genesisenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 1.0 |
| 2019 | 2.2 |
| 2018 | 2.1 |
| 2017 | 2.653 |
| 2016 | 2.718 |
| 2015 | 2.47 |
| 2014 | 2.23 |
| 2013 | 1.032 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
