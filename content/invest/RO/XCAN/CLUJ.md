---
title: "CLUJANA SA CLUJ NAPOCA (CLUJ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>CLUJANA SA CLUJ NAPOCA</td></tr>
    <tr><td>Symbol</td><td>CLUJ</td></tr>
    <tr><td>Web</td><td><a href="https://www.clujana.com">www.clujana.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
