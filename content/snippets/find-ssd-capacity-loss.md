---
title: "Find SSD capacity loss"
date: 2021-03-16T15:12:51Z
type: posts
slug: find-ssd-capacity-loss
tags: ["ssd", "linux"]
---

SSDs degrade over time with usage, here's how you find out how much you lost so
far...

# Install

```sh
sudo pacman -S smartmontools
```

# Run

```sh
sudo smartctl --all /dev/nvme0
```

# Result

One of the lines will look like this:

```sh
Percentage Used:                    28%
```

That means you lost over a quarter of your storage. Buy better hardware next
time!
