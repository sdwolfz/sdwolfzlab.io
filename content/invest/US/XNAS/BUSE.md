---
title: "FIRST BUSEY CORP (BUSE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST BUSEY CORP</td></tr>
    <tr><td>Symbol</td><td>BUSE</td></tr>
    <tr><td>Web</td><td><a href="https://www.busey.com">www.busey.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.46 |
| 2020 | 0.88 |
| 2019 | 0.84 |
| 2018 | 0.8 |
| 2017 | 0.72 |
| 2016 | 0.68 |
| 2015 | 0.32 |
| 2014 | 0.19 |
| 2013 | 0.12 |
| 2012 | 0.24 |
| 2011 | 0.16 |
| 2010 | 0.16 |
| 2009 | 0.4 |
| 2008 | 0.8 |
| 2007 | 0.77 |
| 2006 | 0.64 |
| 2005 | 0.56 |
| 2004 | 0.7 |
| 2003 | 0.68 |
| 2002 | 0.6 |
| 2001 | 0.52 |
| 2000 | 0.48 |
| 1999 | 0.44 |
| 1998 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
