---
title: "COSTAMARE INC (CMRE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>COSTAMARE INC</td></tr>
    <tr><td>Symbol</td><td>CMRE</td></tr>
    <tr><td>Web</td><td><a href="https://www.costamare.com">www.costamare.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.4 |
| 2019 | 0.4 |
| 2018 | 0.4 |
| 2017 | 0.4 |
| 2016 | 0.97 |
| 2015 | 1.15 |
| 2014 | 1.11 |
| 2013 | 0.54 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
