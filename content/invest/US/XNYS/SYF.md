---
title: "SYNCHRONY FINANCIAL (SYF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SYNCHRONY FINANCIAL</td></tr>
    <tr><td>Symbol</td><td>SYF</td></tr>
    <tr><td>Web</td><td><a href="https://www.synchronyfinancial.com">www.synchronyfinancial.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.44 |
| 2020 | 0.88 |
| 2019 | 0.86 |
| 2018 | 0.72 |
| 2017 | 0.56 |
| 2016 | 0.39 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
