---
title: "UTZ BRANDS INC (UTZ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>UTZ BRANDS INC</td></tr>
    <tr><td>Symbol</td><td>UTZ</td></tr>
    <tr><td>Web</td><td><a href="https://www.utzsnacks.com">www.utzsnacks.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.05 |
| 2020 | 0.11 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
