---
title: "ARDAGH GROUP S A (ARD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ARDAGH GROUP S A</td></tr>
    <tr><td>Symbol</td><td>ARD</td></tr>
    <tr><td>Web</td><td><a href="https://www.ardaghgroup.com">www.ardaghgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 0.15 |
| 2018 | 0.14 |
| 2017 | 0.42 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
