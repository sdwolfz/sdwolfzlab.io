---
title: " (SNY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SNY</td></tr>
    <tr><td>Web</td><td><a href="https://www.sanofi.com">www.sanofi.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.906 |
| 2020 | 1.699 |
| 2019 | 1.722 |
| 2018 | 1.861 |
| 2017 | 1.577 |
| 2016 | 1.63 |
| 2015 | 1.618 |
| 2014 | 1.91 |
| 2013 | 1.862 |
| 2012 | 1.756 |
| 2011 | 1.822 |
| 2010 | 1.627 |
| 2009 | 1.419 |
| 2008 | 1.51 |
| 2007 | 1.145 |
| 2006 | 0.912 |
| 2005 | 0.799 |
| 2004 | 0.61 |
| 2003 | 0.49 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
