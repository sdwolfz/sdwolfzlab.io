---
title: "Liontrust Asset Management (LIO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Liontrust Asset Management</td></tr>
    <tr><td>Symbol</td><td>LIO</td></tr>
    <tr><td>Web</td><td><a href="https://www.liontrust.co.uk">www.liontrust.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 33.0 |
| 2019 | 27.0 |
| 2018 | 21.0 |
| 2017 | 15.0 |
| 2016 | 12.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
