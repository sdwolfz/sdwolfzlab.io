# Personal Website

Setup to build and publish my personal website.

## Requirements

You need the following packages installed:

| Name          | Link                                             |
|---------------|--------------------------------------------------|
| `bash`        | https://www.gnu.org/software/bash/               |
| `docker`      | https://docs.docker.com/engine/install/          |
| `docker-here` | https://gitlab.com/sdwolfz/docker-here           |
| `git`         | https://git-scm.com/                             |
| `make`        | https://www.gnu.org/software/make/               |
| `xdg-utils`   | https://freedesktop.org/wiki/Software/xdg-utils/ |

## Setup

### Docker Image

The docker image for this repository is being managed here:
https://gitlab.com/sdwolfz/docker-images

### Theme

```sh
make theme link
```

### Pull

```
docker pull sdwolfz/hugo:latest
```

### Build

```sh
docker-here sdwolfz/hugo:latest hugo
```

### Run

```sh
make shell/server
```

### Shell

```sh
make shell
```

### Open

```sh
make open
```

### Generate

```sh
docker-here sdwolfz/hugo:latest hugo new
```

## Development

### Terraform

Generate a `CLOUDFLARE_API_TOKEN` and save it in envchain:
* Permissions:
  - Account Settings:Read
  - DNS:Edit
  - Firewall Services:Edit
  - Page Rules:Edit
  - Zone WAF:Edit
* Resource: include - Codrut Gusoi
* Zones: codrut.pro

```sh
envchain -s -n website CLOUDFLARE_API_TOKEN
```

Start the terraform shell:

```sh
make shell/terraform
```

#### Initialize

```sh
terraform init
```

#### Prepare

```sh
terraform fmt
terraform validate
terraform plan
```

#### Run

```sh
terraform apply
```

#### Upgrade

Run this from a new terraform version:

```sh
terraform init -upgrade
```

### Snippets

To create a new snippet add the `my-new-snippet.md` filename to the end of:
```sh
docker-here sdwolfz/hugo:latest hugo new snippets/
```
