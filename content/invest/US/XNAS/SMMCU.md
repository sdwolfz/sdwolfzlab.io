---
title: "SOUTH MOUNTAIN MERGER CORP UNIT (1 CL A & 1/2 WT EXP ) (SMMCU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SOUTH MOUNTAIN MERGER CORP UNIT (1 CL A & 1/2 WT EXP )</td></tr>
    <tr><td>Symbol</td><td>SMMCU</td></tr>
    <tr><td>Web</td><td><a href="http://www.smmergercorp.com">www.smmergercorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
