---
title: "SIERRA BANCORP (BSRR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SIERRA BANCORP</td></tr>
    <tr><td>Symbol</td><td>BSRR</td></tr>
    <tr><td>Web</td><td><a href="https://www.sierrabancorp.com">www.sierrabancorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.42 |
| 2020 | 0.8 |
| 2019 | 0.74 |
| 2018 | 0.64 |
| 2017 | 0.56 |
| 2016 | 0.48 |
| 2015 | 0.42 |
| 2014 | 0.34 |
| 2013 | 0.26 |
| 2012 | 0.24 |
| 2011 | 0.24 |
| 2010 | 0.24 |
| 2009 | 0.4 |
| 2008 | 0.68 |
| 2007 | 0.62 |
| 2006 | 0.54 |
| 2005 | 0.45 |
| 2004 | 0.37 |
| 2003 | 0.36 |
| 2002 | 0.28 |
| 2001 | 0.18 |
| 2000 | 0.23 |
| 1999 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
