---
title: "INNOVATIVE SOLUTIONS & SUPPORT (ISSC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>INNOVATIVE SOLUTIONS & SUPPORT</td></tr>
    <tr><td>Symbol</td><td>ISSC</td></tr>
    <tr><td>Web</td><td><a href="https://www.innovative-ss.com">www.innovative-ss.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.15 |
| 2012 | 1.5 |
| 2008 | 1.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
