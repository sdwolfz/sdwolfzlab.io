---
title: "Bodycote (BOY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Bodycote</td></tr>
    <tr><td>Symbol</td><td>BOY</td></tr>
    <tr><td>Web</td><td><a href="https://www.bodycote.com">www.bodycote.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 19.4 |
| 2019 | 20.0 |
| 2018 | 39.0 |
| 2017 | 42.4 |
| 2016 | 15.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
