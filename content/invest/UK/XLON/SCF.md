---
title: "Schroder Income Growth Fund (SCF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Schroder Income Growth Fund</td></tr>
    <tr><td>Symbol</td><td>SCF</td></tr>
    <tr><td>Web</td><td><a href="https://www.schroders.co.uk">www.schroders.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 12.6 |
| 2019 | 12.4 |
| 2018 | 11.8 |
| 2017 | 11.2 |
| 2016 | 10.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
