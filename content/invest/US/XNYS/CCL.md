---
title: "CARNIVAL CORP (CCL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CARNIVAL CORP</td></tr>
    <tr><td>Symbol</td><td>CCL</td></tr>
    <tr><td>Web</td><td><a href="https://www.carnivalcorp.com">www.carnivalcorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.5 |
| 2019 | 2.0 |
| 2018 | 1.95 |
| 2017 | 1.15 |
| 2016 | 1.35 |
| 2015 | 1.1 |
| 2014 | 1.0 |
| 2013 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
