---
title: "Crest Nicholson (CRST)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Crest Nicholson</td></tr>
    <tr><td>Symbol</td><td>CRST</td></tr>
    <tr><td>Web</td><td><a href="https://www.crestnicholson.com">www.crestnicholson.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 84.7 |
| 2018 | 33.0 |
| 2017 | 33.0 |
| 2016 | 27.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
