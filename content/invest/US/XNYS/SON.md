---
title: "SONOCO PRODUCTS CO (SON)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SONOCO PRODUCTS CO</td></tr>
    <tr><td>Symbol</td><td>SON</td></tr>
    <tr><td>Web</td><td><a href="https://www.sonoco.com">www.sonoco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.9 |
| 2020 | 1.72 |
| 2019 | 1.7 |
| 2018 | 1.62 |
| 2017 | 1.54 |
| 2016 | 1.46 |
| 2015 | 1.37 |
| 2014 | 1.27 |
| 2013 | 0.62 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
