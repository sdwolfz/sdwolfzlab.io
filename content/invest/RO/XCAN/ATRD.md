---
title: "ATELIERELE CFR GRIVITA SA BUCURESTI (ATRD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>ATELIERELE CFR GRIVITA SA BUCURESTI</td></tr>
    <tr><td>Symbol</td><td>ATRD</td></tr>
    <tr><td>Web</td><td><a href="https://www.grivita.ro">www.grivita.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2007 | 1.7014 |
| 2003 | 0.7763 |
| 2002 | 1.6409 |
| 2001 | 1.5781 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
