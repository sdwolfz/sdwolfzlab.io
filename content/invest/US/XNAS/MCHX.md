---
title: "MARCHEX INC (MCHX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MARCHEX INC</td></tr>
    <tr><td>Symbol</td><td>MCHX</td></tr>
    <tr><td>Web</td><td><a href="https://www.marchex.com">www.marchex.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.5 |
| 2015 | 0.04 |
| 2014 | 0.08 |
| 2012 | 0.25 |
| 2011 | 0.08 |
| 2010 | 0.08 |
| 2009 | 0.08 |
| 2008 | 0.08 |
| 2007 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
