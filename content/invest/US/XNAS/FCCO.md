---
title: "FIRST COMMUNITY CORP(SC) (FCCO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST COMMUNITY CORP(SC)</td></tr>
    <tr><td>Symbol</td><td>FCCO</td></tr>
    <tr><td>Web</td><td><a href="https://www.firstcommunitysc.com">www.firstcommunitysc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.24 |
| 2020 | 0.48 |
| 2019 | 0.44 |
| 2018 | 0.4 |
| 2017 | 0.36 |
| 2016 | 0.32 |
| 2015 | 0.28 |
| 2014 | 0.24 |
| 2013 | 0.22 |
| 2012 | 0.16 |
| 2011 | 0.16 |
| 2010 | 0.16 |
| 2009 | 0.24 |
| 2008 | 0.32 |
| 2007 | 0.27 |
| 2006 | 0.23 |
| 2005 | 0.2 |
| 2004 | 0.2 |
| 2003 | 0.19 |
| 2002 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
