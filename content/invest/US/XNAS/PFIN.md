---
title: "P & F INDUSTRIES INC (PFIN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>P & F INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>PFIN</td></tr>
    <tr><td>Web</td><td><a href="https://www.pfina.com">www.pfina.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.05 |
| 2019 | 0.2 |
| 2018 | 0.2 |
| 2017 | 0.2 |
| 2016 | 0.65 |
| 1995 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
