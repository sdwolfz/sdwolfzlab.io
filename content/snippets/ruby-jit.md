---
title: "Ruby JIT"
date: 2021-04-13T10:45:39Z
type: posts
slug: ruby-jit
tags: ["ruby"]
---

First, make sure you have at least ruby version `2.6`, then enable it with a flag:

```sh
ruby --jit ./my-ruby-script.rb
```

Or with an environment vairable:

```sh
RUBYOPT="--jit"
```

If you want to see JIT output in your console do:

```sh
RUBYOPT="--jit --jit-verbose=1"
```

And that's it, now let it do it's thing!
