---
title: "TRAVELERS COMPANIES INC (TRV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TRAVELERS COMPANIES INC</td></tr>
    <tr><td>Symbol</td><td>TRV</td></tr>
    <tr><td>Web</td><td><a href="https://www.travelers.com">www.travelers.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.73 |
| 2020 | 3.37 |
| 2019 | 3.23 |
| 2018 | 3.03 |
| 2017 | 2.83 |
| 2016 | 2.62 |
| 2015 | 2.38 |
| 2014 | 2.15 |
| 2013 | 1.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
