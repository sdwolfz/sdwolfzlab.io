---
title: " (GRFS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>GRFS</td></tr>
    <tr><td>Web</td><td><a href="https://www.grifols.com">www.grifols.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.201 |
| 2019 | 0.398 |
| 2018 | 0.479 |
| 2017 | 0.377 |
| 2016 | 0.356 |
| 2015 | 0.727 |
| 2014 | 0.6 |
| 2013 | 0.271 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
