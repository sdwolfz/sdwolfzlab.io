---
title: "PARATEK PHARMACEUTICALS INC (PRTK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PARATEK PHARMACEUTICALS INC</td></tr>
    <tr><td>Symbol</td><td>PRTK</td></tr>
    <tr><td>Web</td><td><a href="https://www.paratekpharma.com">www.paratekpharma.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 1.997 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
