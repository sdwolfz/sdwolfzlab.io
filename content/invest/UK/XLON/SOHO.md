---
title: "Triple Pnt Soc (SOHO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Triple Pnt Soc</td></tr>
    <tr><td>Symbol</td><td>SOHO</td></tr>
    <tr><td>Web</td><td><a href="https://www.triplepointreit.com">www.triplepointreit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 5.18 |
| 2019 | 5.1 |
| 2018 | 5.0 |
| 2017 | 1.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
