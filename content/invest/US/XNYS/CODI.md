---
title: " (CODI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CODI</td></tr>
    <tr><td>Web</td><td><a href="https://www.compasstrust.com">www.compasstrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.72 |
| 2020 | 1.44 |
| 2019 | 1.44 |
| 2018 | 1.44 |
| 2017 | 1.44 |
| 2016 | 1.44 |
| 2015 | 1.44 |
| 2014 | 1.44 |
| 2013 | 0.72 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
