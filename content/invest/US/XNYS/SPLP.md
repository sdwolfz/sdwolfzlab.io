---
title: " (SPLP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SPLP</td></tr>
    <tr><td>Web</td><td><a href="https://www.steelpartners.com">www.steelpartners.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.375 |
| 2018 | 0.75 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
