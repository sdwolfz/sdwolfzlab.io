---
title: "CENTERPOINT ENERGY INC (CNP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CENTERPOINT ENERGY INC</td></tr>
    <tr><td>Symbol</td><td>CNP</td></tr>
    <tr><td>Web</td><td><a href="https://www.centerpointenergy.com">www.centerpointenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.32 |
| 2020 | 0.74 |
| 2019 | 1.148 |
| 2018 | 1.112 |
| 2017 | 1.072 |
| 2016 | 1.028 |
| 2015 | 0.992 |
| 2014 | 0.948 |
| 2013 | 0.414 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
