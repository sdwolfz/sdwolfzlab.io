---
title: " (DEA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>DEA</td></tr>
    <tr><td>Web</td><td><a href="https://www.easterlyreit.com">www.easterlyreit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.52 |
| 2020 | 1.04 |
| 2019 | 1.04 |
| 2018 | 1.04 |
| 2017 | 1.0 |
| 2016 | 0.92 |
| 2015 | 0.54 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
