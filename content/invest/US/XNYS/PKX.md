---
title: " (PKX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>PKX</td></tr>
    <tr><td>Web</td><td><a href="https://www.posco.com">www.posco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.423 |
| 2019 | 1.685 |
| 2018 | 1.817 |
| 2017 | 1.226 |
| 2016 | 0.402 |
| 2015 | 0.345 |
| 2014 | 0.406 |
| 2013 | 1.531 |
| 2012 | 1.102 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
