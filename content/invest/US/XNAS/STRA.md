---
title: "STRATEGIC EDUCATION INC (STRA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>STRATEGIC EDUCATION INC</td></tr>
    <tr><td>Symbol</td><td>STRA</td></tr>
    <tr><td>Web</td><td><a href="https://www.strategiceducation.com">www.strategiceducation.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.2 |
| 2020 | 2.4 |
| 2019 | 2.1 |
| 2018 | 1.5 |
| 2017 | 1.0 |
| 2012 | 4.0 |
| 2011 | 4.0 |
| 2010 | 3.25 |
| 2009 | 2.25 |
| 2008 | 1.625 |
| 2007 | 3.313 |
| 2006 | 1.063 |
| 2005 | 0.625 |
| 2004 | 0.405 |
| 2003 | 0.26 |
| 2002 | 0.26 |
| 2001 | 0.26 |
| 2000 | 0.245 |
| 1999 | 0.21 |
| 1998 | 0.18 |
| 1997 | 0.19 |
| 1996 | 0.125 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
