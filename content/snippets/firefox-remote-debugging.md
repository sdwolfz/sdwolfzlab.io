---
title: "Firefox remote debugging"
date: 2021-04-24T12:32:59Z
type: posts
slug: firefox-remote-debugging
tags: ["firefox", "debugging"]
---

1. If you haven't done already, then
   [Enable 'Developer options' on your phone](/snippets/enable-developer-options-on-your-phone/).
2. Enable `Remote debugging via USB` in your mobile browser's settings.
3. Make sure your devices are properly connected via USB.
4. Visit `about:debugging` in your laptop browser.
5. Press `Connect` next to your device.
6. Select your device and press `Inspect` on your desired tab.
