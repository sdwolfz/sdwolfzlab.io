---
title: "CENTURY BANCORP INC (CNBKA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CENTURY BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>CNBKA</td></tr>
    <tr><td>Web</td><td><a href="https://www.centurybank.com">www.centurybank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.36 |
| 2020 | 0.54 |
| 2019 | 0.48 |
| 2018 | 0.48 |
| 2017 | 0.48 |
| 2016 | 0.48 |
| 2015 | 0.48 |
| 2014 | 0.48 |
| 2013 | 0.48 |
| 2012 | 0.48 |
| 2011 | 0.48 |
| 2010 | 0.48 |
| 2009 | 0.48 |
| 2008 | 0.48 |
| 2007 | 0.48 |
| 2006 | 0.48 |
| 2005 | 0.48 |
| 2004 | 0.48 |
| 2003 | 0.45 |
| 2002 | 0.42 |
| 2001 | 0.37 |
| 2000 | 0.33 |
| 1999 | 0.3 |
| 1998 | 0.21 |
| 1997 | 0.2 |
| 1996 | 0.16 |
| 1995 | 0.42 |
| 1994 | 0.1 |
| 1993 | 0.1 |
| 1992 | 0.1 |
| 1991 | 0.1 |
| 1990 | 0.25 |
| 1989 | 0.296 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
