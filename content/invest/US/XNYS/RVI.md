---
title: "RETAIL VALUE INC (RVI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>RETAIL VALUE INC</td></tr>
    <tr><td>Symbol</td><td>RVI</td></tr>
    <tr><td>Web</td><td><a href="https://www.retailvalueinc.com">www.retailvalueinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.16 |
| 2019 | 2.05 |
| 2018 | 1.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
