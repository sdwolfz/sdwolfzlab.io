---
title: "MGIC INVESTMENT CORP (MTG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MGIC INVESTMENT CORP</td></tr>
    <tr><td>Symbol</td><td>MTG</td></tr>
    <tr><td>Web</td><td><a href="https://www.mgic.com">www.mgic.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 0.24 |
| 2019 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
