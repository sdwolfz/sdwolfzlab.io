---
title: "LAKE SHORE BANCORP (LSBK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LAKE SHORE BANCORP</td></tr>
    <tr><td>Symbol</td><td>LSBK</td></tr>
    <tr><td>Web</td><td><a href="https://www.lakeshoresavings.com">www.lakeshoresavings.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.26 |
| 2020 | 0.49 |
| 2019 | 0.48 |
| 2018 | 0.4 |
| 2017 | 0.32 |
| 2016 | 0.28 |
| 2015 | 0.28 |
| 2014 | 0.28 |
| 2013 | 0.28 |
| 2012 | 0.25 |
| 2011 | 0.28 |
| 2010 | 0.24 |
| 2009 | 0.2 |
| 2008 | 0.19 |
| 2007 | 0.13 |
| 2006 | 0.03 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
