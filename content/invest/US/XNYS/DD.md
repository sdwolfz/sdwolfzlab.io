---
title: "DUPONT DE NEMOURS INC (DD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DUPONT DE NEMOURS INC</td></tr>
    <tr><td>Symbol</td><td>DD</td></tr>
    <tr><td>Web</td><td><a href="https://www.dupont.com">www.dupont.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.6 |
| 2020 | 1.2 |
| 2019 | 27.99 |
| 2017 | 1.14 |
| 2016 | 1.52 |
| 2015 | 1.72 |
| 2014 | 1.84 |
| 2013 | 0.9 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
