---
title: " (SE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SE</td></tr>
    <tr><td>Web</td><td><a href="https://www.sea.com">www.sea.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 0.44 |
| 2016 | 1.62 |
| 2015 | 1.48 |
| 2014 | 1.375 |
| 2013 | 0.61 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
