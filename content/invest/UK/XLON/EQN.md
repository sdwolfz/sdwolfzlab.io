---
title: "Equiniti (EQN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Equiniti</td></tr>
    <tr><td>Symbol</td><td>EQN</td></tr>
    <tr><td>Web</td><td><a href="https://www.equiniti.com">www.equiniti.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 5.49 |
| 2018 | 5.32 |
| 2017 | 4.48 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
