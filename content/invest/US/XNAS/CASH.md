---
title: "META FINANCIAL GROUP (CASH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>META FINANCIAL GROUP</td></tr>
    <tr><td>Symbol</td><td>CASH</td></tr>
    <tr><td>Web</td><td><a href="https://www.metafinancialgroup.com">www.metafinancialgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.05 |
| 2020 | 0.2 |
| 2019 | 0.2 |
| 2018 | 0.46 |
| 2017 | 0.52 |
| 2016 | 0.52 |
| 2015 | 0.52 |
| 2014 | 0.52 |
| 2013 | 0.52 |
| 2012 | 0.52 |
| 2011 | 0.52 |
| 2010 | 0.52 |
| 2009 | 0.52 |
| 2008 | 0.52 |
| 2007 | 0.52 |
| 2006 | 0.52 |
| 2005 | 0.52 |
| 2004 | 0.52 |
| 2003 | 0.52 |
| 2002 | 0.52 |
| 2001 | 0.52 |
| 2000 | 0.52 |
| 1999 | 0.52 |
| 1998 | 0.49 |
| 1997 | 0.39 |
| 1996 | 0.465 |
| 1995 | 0.335 |
| 1994 | 0.075 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
