---
title: "COMMUNITY TRUST BANCORP INC (CTBI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COMMUNITY TRUST BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>CTBI</td></tr>
    <tr><td>Web</td><td><a href="https://www.ctbi.com">www.ctbi.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.77 |
| 2020 | 1.53 |
| 2019 | 1.48 |
| 2018 | 1.38 |
| 2017 | 1.3 |
| 2016 | 1.26 |
| 2015 | 1.22 |
| 2014 | 1.21 |
| 2013 | 1.27 |
| 2012 | 1.25 |
| 2011 | 1.23 |
| 2010 | 1.21 |
| 2009 | 1.2 |
| 2008 | 1.17 |
| 2007 | 1.1 |
| 2006 | 1.05 |
| 2005 | 0.98 |
| 2004 | 0.93 |
| 2003 | 0.88 |
| 2002 | 0.84 |
| 2001 | 0.81 |
| 2000 | 0.78 |
| 1999 | 0.81 |
| 1998 | 0.81 |
| 1997 | 0.56 |
| 1996 | 0.74 |
| 1995 | 0.66 |
| 1994 | 0.45 |
| 1993 | 0.825 |
| 1992 | 0.865 |
| 1991 | 0.825 |
| 1990 | 1.02 |
| 1989 | 0.94 |
| 1988 | 0.23 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
