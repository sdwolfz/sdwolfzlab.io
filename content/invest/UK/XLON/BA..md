---
title: "BAE Systems (BA.)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>BAE Systems</td></tr>
    <tr><td>Symbol</td><td>BA.</td></tr>
    <tr><td>Web</td><td><a href="https://www.baesystems.com">www.baesystems.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 23.2 |
| 2018 | 22.2 |
| 2017 | 21.8 |
| 2016 | 21.3 |
| 2015 | 20.9 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
