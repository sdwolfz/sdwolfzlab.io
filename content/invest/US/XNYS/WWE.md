---
title: "WORLD WRESTLING ENTERTAINMENT INC (WWE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>WORLD WRESTLING ENTERTAINMENT INC</td></tr>
    <tr><td>Symbol</td><td>WWE</td></tr>
    <tr><td>Web</td><td><a href="https://www.wwe.com">www.wwe.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.24 |
| 2020 | 0.48 |
| 2019 | 0.48 |
| 2018 | 0.48 |
| 2017 | 0.48 |
| 2016 | 0.48 |
| 2015 | 0.48 |
| 2014 | 0.48 |
| 2013 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
