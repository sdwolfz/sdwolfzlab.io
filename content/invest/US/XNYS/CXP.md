---
title: " (CXP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CXP</td></tr>
    <tr><td>Web</td><td><a href="https://www.columbia.reit">www.columbia.reit</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.21 |
| 2020 | 0.84 |
| 2019 | 0.81 |
| 2018 | 0.8 |
| 2017 | 0.8 |
| 2016 | 1.2 |
| 2015 | 1.2 |
| 2014 | 1.2 |
| 2013 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
