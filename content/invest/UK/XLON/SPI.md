---
title: "Spire Healthcare (SPI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Spire Healthcare</td></tr>
    <tr><td>Symbol</td><td>SPI</td></tr>
    <tr><td>Web</td><td><a href="https://www.spirehealthcare.com">www.spirehealthcare.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 3.8 |
| 2018 | 3.5 |
| 2017 | 3.8 |
| 2016 | 0.87 |
| 2015 | 0.88 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
