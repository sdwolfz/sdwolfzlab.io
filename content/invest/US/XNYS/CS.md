---
title: " (CS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CS</td></tr>
    <tr><td>Web</td><td><a href="https://www.credit-suisse.com">www.credit-suisse.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.035 |
| 2020 | 0.097 |
| 2019 | 0.257 |
| 2018 | 0.249 |
| 2017 | 1.222 |
| 2016 | 0.719 |
| 2015 | 0.719 |
| 2014 | 0.786 |
| 2013 | 0.106 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
