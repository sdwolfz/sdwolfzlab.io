---
title: "STERLING BANCORP D (STL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>STERLING BANCORP D</td></tr>
    <tr><td>Symbol</td><td>STL</td></tr>
    <tr><td>Web</td><td><a href="https://www.sterlingbancorp.com">www.sterlingbancorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.14 |
| 2020 | 0.28 |
| 2019 | 0.28 |
| 2018 | 0.28 |
| 2017 | 0.28 |
| 2016 | 0.28 |
| 2015 | 0.28 |
| 2014 | 0.28 |
| 2013 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
