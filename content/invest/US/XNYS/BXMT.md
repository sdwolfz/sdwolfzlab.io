---
title: " (BXMT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BXMT</td></tr>
    <tr><td>Web</td><td><a href="https://www.blackstonemortgagetrust.com">www.blackstonemortgagetrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.62 |
| 2020 | 2.48 |
| 2019 | 2.48 |
| 2018 | 2.48 |
| 2017 | 2.48 |
| 2016 | 2.48 |
| 2015 | 2.28 |
| 2014 | 1.98 |
| 2013 | 0.72 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
