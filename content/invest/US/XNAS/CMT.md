---
title: "CORE MOLDING TECHNOLOGIES INC (CMT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CORE MOLDING TECHNOLOGIES INC</td></tr>
    <tr><td>Symbol</td><td>CMT</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.1 |
| 2017 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
