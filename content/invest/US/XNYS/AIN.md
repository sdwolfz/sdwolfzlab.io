---
title: "ALBANY INTERNATIONAL CORP (AIN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ALBANY INTERNATIONAL CORP</td></tr>
    <tr><td>Symbol</td><td>AIN</td></tr>
    <tr><td>Web</td><td><a href="https://www.albint.com">www.albint.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.77 |
| 2019 | 0.73 |
| 2018 | 0.69 |
| 2017 | 0.68 |
| 2016 | 0.68 |
| 2015 | 0.67 |
| 2014 | 0.63 |
| 2013 | 0.45 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
