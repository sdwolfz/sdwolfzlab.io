---
title: "Bunzl (BNZL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Bunzl</td></tr>
    <tr><td>Symbol</td><td>BNZL</td></tr>
    <tr><td>Web</td><td><a href="https://www.bunzl.com">www.bunzl.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 54.1 |
| 2019 | 51.3 |
| 2018 | 50.2 |
| 2017 | 46.0 |
| 2016 | 42.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
