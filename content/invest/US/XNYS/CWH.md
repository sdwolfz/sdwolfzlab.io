---
title: "CAMPING WORLD HOLDINGS INC (CWH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CAMPING WORLD HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>CWH</td></tr>
    <tr><td>Web</td><td><a href="https://www.campingworld.com">www.campingworld.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.23 |
| 2020 | 1.476 |
| 2019 | 0.612 |
| 2018 | 0.612 |
| 2017 | 0.742 |
| 2016 | 0.08 |
| 2014 | 0.25 |
| 2013 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
