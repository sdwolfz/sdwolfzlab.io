---
title: "Ascential (ASCL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Ascential</td></tr>
    <tr><td>Symbol</td><td>ASCL</td></tr>
    <tr><td>Web</td><td><a href="https://www.ascential.com">www.ascential.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 5.8 |
| 2018 | 5.8 |
| 2017 | 5.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
