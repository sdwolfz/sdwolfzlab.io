---
title: "APPLE INC (AAPL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>APPLE INC</td></tr>
    <tr><td>Symbol</td><td>AAPL</td></tr>
    <tr><td>Web</td><td><a href="https://www.apple.com">www.apple.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.425 |
| 2020 | 2.615 |
| 2019 | 3.04 |
| 2018 | 2.82 |
| 2017 | 2.46 |
| 2016 | 2.23 |
| 2015 | 2.03 |
| 2014 | 7.28 |
| 2013 | 11.8 |
| 2012 | 5.3 |
| 1995 | 0.48 |
| 1994 | 0.48 |
| 1993 | 0.48 |
| 1992 | 0.48 |
| 1991 | 0.48 |
| 1990 | 0.45 |
| 1989 | 0.41 |
| 1988 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
