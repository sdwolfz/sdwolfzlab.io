## Spacemacs

* https://github.com/syl20bnr/spacemacs

Role: `Maintainer`

## Redmine Tags

* https://github.com/ixti/redmine_tags

Role: `Collaborator`

## Matrex

* https://github.com/versilov/matrex

Role: `Initiator`
