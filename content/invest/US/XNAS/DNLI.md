---
title: "DENALI THERAPEUTICS INC (DNLI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>DENALI THERAPEUTICS INC</td></tr>
    <tr><td>Symbol</td><td>DNLI</td></tr>
    <tr><td>Web</td><td><a href="https://www.denalitherapeutics.com">www.denalitherapeutics.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
