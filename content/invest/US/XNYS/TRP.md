---
title: "TC ENERGY CORPORATION (TRP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TC ENERGY CORPORATION</td></tr>
    <tr><td>Symbol</td><td>TRP</td></tr>
    <tr><td>Web</td><td><a href="https://www.tcenergy.com">www.tcenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.995 |
| 2019 | 2.842 |
| 2018 | 1.626 |
| 2017 | 1.926 |
| 2016 | 1.699 |
| 2015 | 1.753 |
| 2014 | 1.92 |
| 2013 | 0.92 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
