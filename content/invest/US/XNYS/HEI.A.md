---
title: "HEICO CORP (HEI.A)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HEICO CORP</td></tr>
    <tr><td>Symbol</td><td>HEI.A</td></tr>
    <tr><td>Web</td><td><a href="https://www.heico.com">www.heico.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.08 |
| 2020 | 0.16 |
| 2019 | 0.14 |
| 2018 | 0.116 |
| 2017 | 0.08 |
| 2016 | 0.152 |
| 2015 | 0.15 |
| 2014 | 0.13 |
| 2013 | 0.116 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
