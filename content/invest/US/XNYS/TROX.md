---
title: "TRONOX HOLDINGS PLC (TROX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TRONOX HOLDINGS PLC</td></tr>
    <tr><td>Symbol</td><td>TROX</td></tr>
    <tr><td>Web</td><td><a href="https://www.tronox.com">www.tronox.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.16 |
| 2020 | 0.28 |
| 2019 | 0.18 |
| 2018 | 0.18 |
| 2017 | 0.18 |
| 2016 | 0.385 |
| 2015 | 1.0 |
| 2014 | 1.0 |
| 2013 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
