---
title: "MOOG INC (MOG.B)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MOOG INC</td></tr>
    <tr><td>Symbol</td><td>MOG.B</td></tr>
    <tr><td>Web</td><td><a href="https://www.moog.com">www.moog.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.5 |
| 2020 | 0.75 |
| 2019 | 1.0 |
| 2018 | 0.75 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
