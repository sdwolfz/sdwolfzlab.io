---
title: "KEYCORP (KEY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>KEYCORP</td></tr>
    <tr><td>Symbol</td><td>KEY</td></tr>
    <tr><td>Web</td><td><a href="https://www.key.com">www.key.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.185 |
| 2020 | 0.74 |
| 2019 | 0.71 |
| 2018 | 0.565 |
| 2017 | 0.38 |
| 2016 | 0.33 |
| 2015 | 0.29 |
| 2014 | 0.25 |
| 2013 | 0.11 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
