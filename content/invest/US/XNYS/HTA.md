---
title: " (HTA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>HTA</td></tr>
    <tr><td>Web</td><td><a href="https://www.htareit.com">www.htareit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.64 |
| 2020 | 0.95 |
| 2019 | 1.25 |
| 2018 | 1.23 |
| 2017 | 1.51 |
| 2016 | 1.19 |
| 2015 | 1.17 |
| 2014 | 0.578 |
| 2013 | 0.288 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
