---
title: "ELLINGTON FINANCIAL INC (EFC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ELLINGTON FINANCIAL INC</td></tr>
    <tr><td>Symbol</td><td>EFC</td></tr>
    <tr><td>Web</td><td><a href="https://www.ellingtonfinancial.com">www.ellingtonfinancial.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.44 |
| 2020 | 1.26 |
| 2019 | 1.81 |
| 2018 | 1.64 |
| 2017 | 1.76 |
| 2016 | 1.95 |
| 2015 | 2.45 |
| 2014 | 3.08 |
| 2013 | 1.54 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
