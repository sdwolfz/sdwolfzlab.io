---
title: " (GAINL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>GAINL</td></tr>
    <tr><td>Web</td><td><a href="https://www.gladstoneinvestment.com">www.gladstoneinvestment.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.797 |
| 2020 | 1.594 |
| 2019 | 1.594 |
| 2018 | 0.571 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
