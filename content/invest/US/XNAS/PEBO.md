---
title: "PEOPLES BANCORP INC (PEBO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PEOPLES BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>PEBO</td></tr>
    <tr><td>Web</td><td><a href="https://www.peoplesbancorp.com">www.peoplesbancorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.71 |
| 2020 | 1.37 |
| 2019 | 1.32 |
| 2018 | 1.12 |
| 2017 | 0.84 |
| 2016 | 0.64 |
| 2015 | 0.6 |
| 2014 | 0.6 |
| 2013 | 0.54 |
| 2012 | 0.45 |
| 2011 | 0.3 |
| 2010 | 0.4 |
| 2009 | 0.66 |
| 2008 | 0.91 |
| 2007 | 0.88 |
| 2006 | 0.83 |
| 2005 | 0.78 |
| 2004 | 0.72 |
| 2003 | 0.66 |
| 2002 | 0.6 |
| 2001 | 0.59 |
| 2000 | 0.56 |
| 1999 | 0.56 |
| 1998 | 0.59 |
| 1997 | 0.74 |
| 1996 | 0.68 |
| 1995 | 0.66 |
| 1994 | 0.72 |
| 1993 | 1.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
