---
title: "SITE CENTERS CORP (SITC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SITE CENTERS CORP</td></tr>
    <tr><td>Symbol</td><td>SITC</td></tr>
    <tr><td>Web</td><td><a href="https://www.sitecenters.com">www.sitecenters.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.11 |
| 2020 | 0.25 |
| 2019 | 0.8 |
| 2018 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
