---
title: "COMMUNITY FIRST BANCSHARES INC D C (CFBI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COMMUNITY FIRST BANCSHARES INC D C</td></tr>
    <tr><td>Symbol</td><td>CFBI</td></tr>
    <tr><td>Web</td><td><a href="http://www.newtonfederal.com">www.newtonfederal.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
