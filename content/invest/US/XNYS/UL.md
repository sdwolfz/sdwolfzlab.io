---
title: " (UL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>UL</td></tr>
    <tr><td>Web</td><td><a href="https://www.unilever.com">www.unilever.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.02 |
| 2020 | 1.83 |
| 2019 | 1.797 |
| 2018 | 1.806 |
| 2017 | 1.543 |
| 2016 | 1.388 |
| 2015 | 1.645 |
| 2014 | 1.881 |
| 2013 | 0.722 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
