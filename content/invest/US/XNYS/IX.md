---
title: " (IX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>IX</td></tr>
    <tr><td>Web</td><td><a href="https://www.orix.co.jp">www.orix.co.jp</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.624 |
| 2019 | 2.972 |
| 2018 | 2.832 |
| 2017 | 2.264 |
| 2016 | 1.87 |
| 2014 | 1.003 |
| 2013 | 0.597 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
