---
title: "SALISBURY BANCORP INC (SAL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SALISBURY BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>SAL</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.59 |
| 2020 | 1.16 |
| 2019 | 1.12 |
| 2018 | 1.12 |
| 2017 | 1.12 |
| 2016 | 1.12 |
| 2015 | 1.12 |
| 2014 | 1.12 |
| 2013 | 1.12 |
| 2012 | 1.12 |
| 2011 | 1.12 |
| 2010 | 1.12 |
| 2009 | 0.84 |
| 2008 | 1.12 |
| 2007 | 1.08 |
| 2006 | 1.04 |
| 2005 | 1.0 |
| 2004 | 0.96 |
| 2003 | 0.92 |
| 2002 | 0.88 |
| 2001 | 0.84 |
| 2000 | 0.77 |
| 1999 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
