---
title: "GRAFTECH INTERNATIONAL LTD (EAF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GRAFTECH INTERNATIONAL LTD</td></tr>
    <tr><td>Symbol</td><td>EAF</td></tr>
    <tr><td>Web</td><td><a href="https://www.graftech.com">www.graftech.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.02 |
| 2020 | 0.115 |
| 2019 | 0.34 |
| 2018 | 0.934 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
