---
title: "KAR AUCTION SERVICES INC (KAR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>KAR AUCTION SERVICES INC</td></tr>
    <tr><td>Symbol</td><td>KAR</td></tr>
    <tr><td>Web</td><td><a href="https://www.karglobal.com">www.karglobal.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.19 |
| 2019 | 39.67 |
| 2018 | 1.4 |
| 2017 | 1.31 |
| 2016 | 1.19 |
| 2015 | 1.08 |
| 2014 | 1.02 |
| 2013 | 0.44 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
