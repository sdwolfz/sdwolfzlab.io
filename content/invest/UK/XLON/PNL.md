---
title: "Personal Assets Trust (PNL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Personal Assets Trust</td></tr>
    <tr><td>Symbol</td><td>PNL</td></tr>
    <tr><td>Web</td><td><a href="https://www.patplc.co.uk">www.patplc.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 560.0 |
| 2019 | 560.0 |
| 2018 | 560.0 |
| 2017 | 560.0 |
| 2016 | 560.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
