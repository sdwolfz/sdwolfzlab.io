---
title: "NET 1 UEPS TECHNOLOGIES INC (UEPS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NET 1 UEPS TECHNOLOGIES INC</td></tr>
    <tr><td>Symbol</td><td>UEPS</td></tr>
    <tr><td>Web</td><td><a href="https://www.net1.com">www.net1.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
