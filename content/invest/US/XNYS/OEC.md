---
title: "ORION ENGINEERED CARBONS SA (OEC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ORION ENGINEERED CARBONS SA</td></tr>
    <tr><td>Symbol</td><td>OEC</td></tr>
    <tr><td>Web</td><td><a href="https://www.orioncarbons.com">www.orioncarbons.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.2 |
| 2019 | 0.8 |
| 2018 | 0.8 |
| 2017 | 0.758 |
| 2016 | 0.72 |
| 2015 | 0.752 |
| 2014 | 0.67 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
