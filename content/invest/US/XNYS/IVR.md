---
title: " (IVR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>IVR</td></tr>
    <tr><td>Web</td><td><a href="https://www.invescomortgagecapital.com">www.invescomortgagecapital.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.17 |
| 2020 | 1.07 |
| 2019 | 1.85 |
| 2018 | 1.68 |
| 2017 | 1.63 |
| 2016 | 1.6 |
| 2015 | 1.7 |
| 2014 | 2.0 |
| 2013 | 1.65 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
