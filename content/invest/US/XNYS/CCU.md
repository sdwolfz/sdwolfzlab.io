---
title: " (CCU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CCU</td></tr>
    <tr><td>Web</td><td><a href="https://www.ccu.cl">www.ccu.cl</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.247 |
| 2020 | 0.363 |
| 2019 | 0.821 |
| 2018 | 0.535 |
| 2017 | 0.345 |
| 2016 | 0.297 |
| 2015 | 0.307 |
| 2014 | 0.355 |
| 2013 | 0.452 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
