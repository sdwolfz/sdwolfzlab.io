---
title: "TECNOGLASS INC (TGLS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TECNOGLASS INC</td></tr>
    <tr><td>Symbol</td><td>TGLS</td></tr>
    <tr><td>Web</td><td><a href="https://www.tecnoglass.com">www.tecnoglass.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.028 |
| 2020 | 0.11 |
| 2019 | 0.56 |
| 2018 | 0.42 |
| 2017 | 0.53 |
| 2016 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
