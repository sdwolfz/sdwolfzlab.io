---
title: "VIRCO MANUFACTURING CORP (VIRC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>VIRCO MANUFACTURING CORP</td></tr>
    <tr><td>Symbol</td><td>VIRC</td></tr>
    <tr><td>Web</td><td><a href="https://www.virco.com">www.virco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.06 |
| 2017 | 0.015 |
| 2011 | 0.05 |
| 2010 | 0.1 |
| 2009 | 0.1 |
| 2008 | 0.1 |
| 2007 | 0.025 |
| 2003 | 0.04 |
| 2002 | 0.08 |
| 2001 | 0.08 |
| 2000 | 0.08 |
| 1999 | 0.04 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
