---
title: "DOMTAR CORPORATION (UFS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DOMTAR CORPORATION</td></tr>
    <tr><td>Symbol</td><td>UFS</td></tr>
    <tr><td>Web</td><td><a href="https://www.domtar.com">www.domtar.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.455 |
| 2019 | 1.8 |
| 2018 | 2.175 |
| 2017 | 1.66 |
| 2016 | 1.645 |
| 2015 | 1.6 |
| 2014 | 1.675 |
| 2013 | 1.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
