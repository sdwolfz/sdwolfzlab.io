---
title: "NAVIOS MARITIME ACQUISITION CORP (NNA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NAVIOS MARITIME ACQUISITION CORP</td></tr>
    <tr><td>Symbol</td><td>NNA</td></tr>
    <tr><td>Web</td><td><a href="https://www.navios-acquisition.com">www.navios-acquisition.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.05 |
| 2020 | 0.9 |
| 2019 | 1.2 |
| 2018 | 0.08 |
| 2017 | 0.2 |
| 2016 | 0.2 |
| 2015 | 0.2 |
| 2014 | 0.2 |
| 2013 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
