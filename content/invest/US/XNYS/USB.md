---
title: "US BANCORP (USB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>US BANCORP</td></tr>
    <tr><td>Symbol</td><td>USB</td></tr>
    <tr><td>Web</td><td><a href="https://www.usbank.com">www.usbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.42 |
| 2020 | 1.68 |
| 2019 | 1.58 |
| 2018 | 1.34 |
| 2017 | 1.16 |
| 2016 | 1.07 |
| 2015 | 1.01 |
| 2014 | 0.965 |
| 2013 | 0.69 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
