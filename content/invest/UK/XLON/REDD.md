---
title: "Redde Northgate (REDD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Redde Northgate</td></tr>
    <tr><td>Symbol</td><td>REDD</td></tr>
    <tr><td>Web</td><td><a href="https://www.reddenorthgate.co.uk">www.reddenorthgate.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 13.1 |
| 2019 | 18.3 |
| 2018 | 11.65 |
| 2018 | 17.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
