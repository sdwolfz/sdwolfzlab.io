---
title: "ELI LILLY AND COMPANY (LLY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ELI LILLY AND COMPANY</td></tr>
    <tr><td>Symbol</td><td>LLY</td></tr>
    <tr><td>Web</td><td><a href="https://www.lilly.com">www.lilly.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.7 |
| 2020 | 2.96 |
| 2019 | 2.58 |
| 2018 | 2.252 |
| 2017 | 2.08 |
| 2016 | 2.04 |
| 2015 | 2.0 |
| 2014 | 1.96 |
| 2013 | 0.98 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
