---
title: " (TGP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>TGP</td></tr>
    <tr><td>Web</td><td><a href="https://www.teekay.com">www.teekay.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.537 |
| 2020 | 0.94 |
| 2019 | 0.71 |
| 2018 | 0.56 |
| 2017 | 0.56 |
| 2016 | 0.56 |
| 2015 | 2.8 |
| 2014 | 2.768 |
| 2013 | 1.35 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
