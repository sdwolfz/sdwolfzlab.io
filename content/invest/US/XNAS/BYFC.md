---
title: "BROADWAY FINANCIAL CORP DELAWARE (BYFC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BROADWAY FINANCIAL CORP DELAWARE</td></tr>
    <tr><td>Symbol</td><td>BYFC</td></tr>
    <tr><td>Web</td><td><a href="https://www.broadwayfederalbank.com">www.broadwayfederalbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2010 | 0.06 |
| 2009 | 0.2 |
| 2008 | 0.2 |
| 2007 | 0.2 |
| 2006 | 0.2 |
| 2005 | 0.2 |
| 2004 | 0.175 |
| 2003 | 0.112 |
| 2002 | 0.238 |
| 2001 | 0.15 |
| 2000 | 0.2 |
| 1999 | 0.2 |
| 1998 | 0.2 |
| 1997 | 0.2 |
| 1996 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
