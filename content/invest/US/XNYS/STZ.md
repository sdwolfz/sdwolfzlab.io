---
title: "CONSTELLATION BRANDS INC (STZ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CONSTELLATION BRANDS INC</td></tr>
    <tr><td>Symbol</td><td>STZ</td></tr>
    <tr><td>Web</td><td><a href="https://www.cbrands.com">www.cbrands.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.51 |
| 2020 | 3.0 |
| 2019 | 2.99 |
| 2018 | 2.74 |
| 2017 | 1.96 |
| 2016 | 1.51 |
| 2015 | 0.93 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
