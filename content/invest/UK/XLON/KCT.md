---
title: "Kin and Carta (KCT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Kin and Carta</td></tr>
    <tr><td>Symbol</td><td>KCT</td></tr>
    <tr><td>Web</td><td><a href="https://www.kinandcarta.com">www.kinandcarta.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 1.95 |
| 2018 | 1.95 |
| 2017 | 1.95 |
| 2016 | 7.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
