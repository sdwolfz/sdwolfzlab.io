---
title: "Abstd Euro Log. (ASLI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Abstd Euro Log.</td></tr>
    <tr><td>Symbol</td><td>ASLI</td></tr>
    <tr><td>Web</td><td><a href="https://www.eurologisticsincome.co.uk">www.eurologisticsincome.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 5.08 |
| 2018 | 3.0 |
| 2018 | 0.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
