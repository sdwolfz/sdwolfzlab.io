---
title: "Terminal 24-bit color and italics"
date: 2020-12-15T09:17:01Z
type: posts
slug: terminal-24-bit-color-and-italics
tags: ["terminal", "linux"]
---

1. Create a `xterm-24bit.terminfo` file in your home directory containing:

```sh
xterm-24bit|xterm with 24-bit direct color mode and italic text,
    use=xterm-256color,
    sitm=\E[3m,
    ritm=\E[23m,
    setb24=\E[48;2;%p1%{65536}%/%d;%p1%{256}%/%{255}%&%d;%p1%{255}%&%dm,
    setf24=\E[38;2;%p1%{65536}%/%d;%p1%{256}%/%{255}%&%d;%p1%{255}%&%dm,

```

> **Note:** make sure to have a newline (**LF**) at the end of file otherwise it will error!

2. Run the following command to compile it:

```sh
tic -x ~/xterm-24bit.terminfo
```

3. Start using it by enabling it in your `~/.bash_profile`:

```bash
export TERM=xterm-24bit
```

4. Fix your ssh issues with:

```bash
alias ssh="TERM=xterm ssh"
```

5. Generally speaking, it's a good idea to set your language env variables to
   `.UTF-8`, so all characters display correctly:

```bash
export LC_ALL=en_GB.UTF-8
export LANG=en_GB.UTF-8
export LANGUAGE=en_GB.UTF-8
```

6. To use with `tmux`, configure it via:

```sh
set -g default-terminal "xterm-24bit"
set -g terminal-overrides ',xterm-24bit:Tc'
```

Now, if only this was available by default... that would be great!
