---
title: "GLOBAL INDEMNITY GROUP LLC (GBLI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GLOBAL INDEMNITY GROUP LLC</td></tr>
    <tr><td>Symbol</td><td>GBLI</td></tr>
    <tr><td>Web</td><td><a href="https://www.global-indemnity.com">www.global-indemnity.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 1.0 |
| 2019 | 1.0 |
| 2018 | 1.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
