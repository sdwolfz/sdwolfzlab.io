---
title: "Reuse YAML sections with anchors"
date: 2020-06-21T12:58:39Z
type: posts
slug: reuse-yaml-sections-with-anchors
tags: ["yaml", "docker", "docker-compose", "tutorial", "productivity"]
---

Define an anchor using the `&` character and reference it everywhere using the
`<<: *` alias:

```yml
version: '2.4'

x-default-logging: &logging
  logging:
    options:
      max-size: '12m'
      max-file: '5'
    driver: json-file

services:
  web:
    image: alpine:latest
    command: /bin/true
    <<: *logging

  postgres:
    image: postgres:alpine
    <<: *logging

  redis:
    image: redis:alpine
    <<: *logging
```

If you run `docker-compose config` you'll see `x-default-logging` is gone and
aliases are replaced.
