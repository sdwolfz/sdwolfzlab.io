---
title: "WATTS WATER TECHNOLOGIES INC (WTS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>WATTS WATER TECHNOLOGIES INC</td></tr>
    <tr><td>Symbol</td><td>WTS</td></tr>
    <tr><td>Web</td><td><a href="https://www.wattswater.com">www.wattswater.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.49 |
| 2020 | 0.92 |
| 2019 | 0.9 |
| 2018 | 0.82 |
| 2017 | 0.75 |
| 2016 | 0.71 |
| 2015 | 0.66 |
| 2014 | 0.58 |
| 2013 | 0.26 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
