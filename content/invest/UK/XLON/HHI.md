---
title: "Henderson High Income Trust (HHI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Henderson High Income Trust</td></tr>
    <tr><td>Symbol</td><td>HHI</td></tr>
    <tr><td>Web</td><td><a href="https://www.hendersonhighincome.com">www.hendersonhighincome.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 9.8 |
| 2018 | 9.6 |
| 2017 | 9.4 |
| 2016 | 9.15 |
| 2015 | 8.9 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
