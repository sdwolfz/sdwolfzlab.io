---
title: "RUSH ENTERPRISES (RUSHB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>RUSH ENTERPRISES</td></tr>
    <tr><td>Symbol</td><td>RUSHB</td></tr>
    <tr><td>Web</td><td><a href="https://www.rushenterprises.com">www.rushenterprises.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.36 |
| 2020 | 0.54 |
| 2019 | 0.5 |
| 2018 | 0.24 |
| 2002 | 5.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
