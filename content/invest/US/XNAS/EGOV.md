---
title: "NIC INC (EGOV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NIC INC</td></tr>
    <tr><td>Symbol</td><td>EGOV</td></tr>
    <tr><td>Web</td><td><a href="http://www.egov.com">www.egov.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.36 |
| 2019 | 0.32 |
| 2018 | 0.32 |
| 2017 | 0.32 |
| 2016 | 0.65 |
| 2015 | 0.55 |
| 2014 | 0.5 |
| 2013 | 0.35 |
| 2012 | 0.25 |
| 2011 | 0.25 |
| 2010 | 0.55 |
| 2009 | 0.3 |
| 2008 | 0.25 |
| 2007 | 0.75 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
