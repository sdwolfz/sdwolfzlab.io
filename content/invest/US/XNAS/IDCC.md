---
title: "INTERDIGITAL INC (IDCC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>INTERDIGITAL INC</td></tr>
    <tr><td>Symbol</td><td>IDCC</td></tr>
    <tr><td>Web</td><td><a href="https://www.interdigital.com">www.interdigital.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.7 |
| 2020 | 1.4 |
| 2019 | 1.4 |
| 2018 | 1.4 |
| 2017 | 1.25 |
| 2016 | 0.9 |
| 2015 | 0.8 |
| 2014 | 0.6 |
| 2013 | 0.3 |
| 2012 | 1.9 |
| 2011 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
