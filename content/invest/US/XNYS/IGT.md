---
title: "INTERNATIONAL GAME TECHNOLOGY PLC (IGT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>INTERNATIONAL GAME TECHNOLOGY PLC</td></tr>
    <tr><td>Symbol</td><td>IGT</td></tr>
    <tr><td>Web</td><td><a href="https://www.igt.com">www.igt.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.2 |
| 2019 | 0.8 |
| 2018 | 0.8 |
| 2017 | 0.8 |
| 2016 | 0.8 |
| 2015 | 0.51 |
| 2014 | 0.55 |
| 2013 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
