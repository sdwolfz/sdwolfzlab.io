---
title: "CNX RESOURCES CORPORATION (CNX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CNX RESOURCES CORPORATION</td></tr>
    <tr><td>Symbol</td><td>CNX</td></tr>
    <tr><td>Web</td><td><a href="https://www.cnx.com">www.cnx.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 2.806 |
| 2016 | 0.01 |
| 2015 | 0.146 |
| 2014 | 0.252 |
| 2013 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
