---
title: "MAXEON SOLAR TECHNOLOGIES LIMITED (MAXN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MAXEON SOLAR TECHNOLOGIES LIMITED</td></tr>
    <tr><td>Symbol</td><td>MAXN</td></tr>
    <tr><td>Web</td><td><a href="https://www.maxeon.com">www.maxeon.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
