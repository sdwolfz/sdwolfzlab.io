---
title: "CORTEVA INC (CTVA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CORTEVA INC</td></tr>
    <tr><td>Symbol</td><td>CTVA</td></tr>
    <tr><td>Web</td><td><a href="https://www.corteva.com">www.corteva.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.26 |
| 2020 | 0.52 |
| 2019 | 0.26 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
