---
title: "HAWKINS INC (HWKN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HAWKINS INC</td></tr>
    <tr><td>Symbol</td><td>HWKN</td></tr>
    <tr><td>Web</td><td><a href="https://www.hawkinsinc.com">www.hawkinsinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.245 |
| 2020 | 0.93 |
| 2019 | 0.92 |
| 2018 | 0.89 |
| 2017 | 0.86 |
| 2016 | 0.82 |
| 2015 | 0.78 |
| 2014 | 0.74 |
| 2013 | 0.7 |
| 2012 | 0.66 |
| 2011 | 0.62 |
| 2010 | 0.68 |
| 2009 | 0.64 |
| 2008 | 0.5 |
| 2007 | 0.46 |
| 2006 | 0.42 |
| 2005 | 0.38 |
| 2004 | 0.36 |
| 2003 | 0.36 |
| 2002 | 0.3 |
| 2001 | 0.45 |
| 2000 | 0.32 |
| 1999 | 0.27 |
| 1998 | 0.2 |
| 1997 | 0.09 |
| 1996 | 0.08 |
| 1995 | 0.07 |
| 1989 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
