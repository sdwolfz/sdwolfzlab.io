---
title: "BRISTOL-MYERS SQUIBB CO (BMY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BRISTOL-MYERS SQUIBB CO</td></tr>
    <tr><td>Symbol</td><td>BMY</td></tr>
    <tr><td>Web</td><td><a href="https://www.bms.com">www.bms.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.49 |
| 2020 | 2.29 |
| 2019 | 1.64 |
| 2018 | 1.6 |
| 2017 | 1.56 |
| 2016 | 1.14 |
| 2015 | 1.49 |
| 2014 | 1.45 |
| 2013 | 1.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
