---
title: "FLAROS SA BUCURESTI (FLAO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>FLAROS SA BUCURESTI</td></tr>
    <tr><td>Symbol</td><td>FLAO</td></tr>
    <tr><td>Web</td><td><a href="https://www.flaros.com">www.flaros.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.8 |
| 2019 | 1.52 |
| 2018 | 0.77 |
| 2017 | 0.6573 |
| 2016 | 0.38592 |
| 2014 | 0.42 |
| 2013 | 0.3541 |
| 2012 | 0.1849 |
| 2004 | 0.3975 |
| 2003 | 0.7372 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
