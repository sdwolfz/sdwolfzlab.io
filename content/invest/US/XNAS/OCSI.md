---
title: "OAKTREE STRATEGIC INCOME CORP (OCSI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>OAKTREE STRATEGIC INCOME CORP</td></tr>
    <tr><td>Symbol</td><td>OCSI</td></tr>
    <tr><td>Web</td><td><a href="http://www.oaktreestrategicincome.com">www.oaktreestrategicincome.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.55 |
| 2019 | 0.62 |
| 2018 | 0.595 |
| 2017 | 0.76 |
| 2016 | 0.825 |
| 2015 | 1.075 |
| 2014 | 1.1 |
| 2013 | 0.21 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
