---
title: "DOLLAR GENERAL CORP (DG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DOLLAR GENERAL CORP</td></tr>
    <tr><td>Symbol</td><td>DG</td></tr>
    <tr><td>Web</td><td><a href="https://www.dollargeneral.com">www.dollargeneral.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.78 |
| 2020 | 1.4 |
| 2019 | 1.25 |
| 2018 | 1.13 |
| 2017 | 0.78 |
| 2016 | 1.0 |
| 2015 | 0.88 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
