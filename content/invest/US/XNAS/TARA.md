---
title: "PROTARA THERAPEUTICS INC (TARA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PROTARA THERAPEUTICS INC</td></tr>
    <tr><td>Symbol</td><td>TARA</td></tr>
    <tr><td>Web</td><td><a href="https://www.protaratx.com">www.protaratx.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
