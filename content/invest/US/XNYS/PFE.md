---
title: "PFIZER INC (PFE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PFIZER INC</td></tr>
    <tr><td>Symbol</td><td>PFE</td></tr>
    <tr><td>Web</td><td><a href="https://www.pfizer.com">www.pfizer.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.78 |
| 2020 | 3.107 |
| 2019 | 1.44 |
| 2018 | 1.36 |
| 2017 | 1.28 |
| 2016 | 1.2 |
| 2015 | 1.12 |
| 2014 | 1.04 |
| 2013 | 0.48 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
