---
title: "CANTEL MEDICAL CORP (CMD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CANTEL MEDICAL CORP</td></tr>
    <tr><td>Symbol</td><td>CMD</td></tr>
    <tr><td>Web</td><td><a href="https://www.cantelmedical.com">www.cantelmedical.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.105 |
| 2019 | 0.2 |
| 2018 | 0.17 |
| 2017 | 0.14 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
