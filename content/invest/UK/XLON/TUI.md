---
title: "TUI AG (TUI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>TUI AG</td></tr>
    <tr><td>Symbol</td><td>TUI</td></tr>
    <tr><td>Web</td><td><a href="https://www.tui-group.com">www.tui-group.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 24.08 |
| 2019 | 54.0 |
| 2018 | 72.0 |
| 2017 | 65.0 |
| 2016 | 63.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
