---
title: "CHESAPEAKE UTILITIES CORP (CPK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CHESAPEAKE UTILITIES CORP</td></tr>
    <tr><td>Symbol</td><td>CPK</td></tr>
    <tr><td>Web</td><td><a href="https://www.chpk.com">www.chpk.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.92 |
| 2020 | 1.725 |
| 2019 | 1.585 |
| 2018 | 1.435 |
| 2017 | 1.28 |
| 2016 | 1.202 |
| 2015 | 1.131 |
| 2014 | 1.33 |
| 2013 | 0.77 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
