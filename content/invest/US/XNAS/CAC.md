---
title: "CAMDEN NATIONAL CORPORATION (CAC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CAMDEN NATIONAL CORPORATION</td></tr>
    <tr><td>Symbol</td><td>CAC</td></tr>
    <tr><td>Web</td><td><a href="https://www.camdennational.com">www.camdennational.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.69 |
| 2020 | 1.32 |
| 2019 | 1.2 |
| 2018 | 1.1 |
| 2017 | 0.92 |
| 2016 | 1.1 |
| 2015 | 1.2 |
| 2014 | 1.08 |
| 2013 | 1.06 |
| 2012 | 1.0 |
| 2011 | 1.5 |
| 2010 | 1.0 |
| 2009 | 1.0 |
| 2008 | 0.75 |
| 2007 | 1.2 |
| 2006 | 0.88 |
| 2005 | 1.3 |
| 2004 | 0.8 |
| 2003 | 0.72 |
| 2002 | 0.68 |
| 2001 | 0.64 |
| 2000 | 0.63 |
| 1999 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
