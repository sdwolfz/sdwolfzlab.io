# ------------------------------------------------------------------------------
# Upstream

resource "cloudflare_record" "CNAME_upstream" {
  zone_id = cloudflare_zone.domain.id
  name    = local.domain
  content = local.upstream
  proxied = "true"
  type    = "CNAME"
  ttl     = "1"
}

resource "cloudflare_record" "CNAME_www" {
  zone_id = cloudflare_zone.domain.id
  name    = "www"
  content = local.upstream
  proxied = "true"
  type    = "CNAME"
  ttl     = "1"
}

# ------------------------------------------------------------------------------
# Mail

resource "cloudflare_record" "CNAME_DKIM_1" {
  zone_id = cloudflare_zone.domain.id
  name    = "protonmail._domainkey"
  content = "protonmail.domainkey.dgws5jztstay7xjaowsnwhjua6wa26zuu7hf6woocqdh2nyj7isvq.domains.proton.ch"
  proxied = "false"
  type    = "CNAME"
  ttl     = local.ttl
}

resource "cloudflare_record" "CNAME_DKIM_2" {
  zone_id = cloudflare_zone.domain.id
  name    = "protonmail2._domainkey"
  content = "protonmail2.domainkey.dgws5jztstay7xjaowsnwhjua6wa26zuu7hf6woocqdh2nyj7isvq.domains.proton.ch"
  proxied = "false"
  type    = "CNAME"
  ttl     = local.ttl
}


resource "cloudflare_record" "CNAME_DKIM_3" {
  zone_id = cloudflare_zone.domain.id
  name    = "protonmail3._domainkey"
  content = "protonmail3.domainkey.dgws5jztstay7xjaowsnwhjua6wa26zuu7hf6woocqdh2nyj7isvq.domains.proton.ch"
  proxied = "false"
  type    = "CNAME"
  ttl     = local.ttl
}

resource "cloudflare_record" "MX_1" {
  zone_id  = cloudflare_zone.domain.id
  name     = local.domain
  content  = "mail.protonmail.ch"
  type     = "MX"
  ttl      = local.ttl
  priority = "10"
  proxied  = "false"
}

resource "cloudflare_record" "MX_2" {
  zone_id  = cloudflare_zone.domain.id
  name     = local.domain
  content  = "mailsec.protonmail.ch"
  type     = "MX"
  ttl      = local.ttl
  priority = "20"
  proxied  = "false"
}

resource "cloudflare_record" "TXT_DMARC" {
  zone_id = cloudflare_zone.domain.id
  name    = "_dmarc"
  content = "v=DMARC1; p=quarantine; rua=mailto:alert+dmarc@codrut.pro"
  type    = "TXT"
  ttl     = local.ttl
  proxied = "false"
}

resource "cloudflare_record" "TXT_SPF" {
  zone_id = cloudflare_zone.domain.id
  name    = local.domain
  content = "v=spf1 include:_spf.protonmail.ch mx ~all"
  type    = "TXT"
  ttl     = local.ttl
  proxied = "false"
}

# ------------------------------------------------------------------------------
# Verify

resource "cloudflare_record" "TXT_google_verification" {
  zone_id = cloudflare_zone.domain.id
  name    = local.domain
  content = "google-site-verification=MJLcSGRVgpaXeoC0xDh0SQ8eFEpX7KAmLSlYv0Z0RsM"
  type    = "TXT"
  ttl     = local.ttl
  proxied = "false"
}

resource "cloudflare_record" "TXT_protonmail_verification" {
  zone_id = cloudflare_zone.domain.id
  name    = local.domain
  content = "protonmail-verification=b89221ea3ff4632b52d370d0b55bdc1000b42c0c"
  type    = "TXT"
  ttl     = local.ttl
  proxied = "false"
}

resource "cloudflare_record" "TXT_gitlab_root_verification" {
  zone_id = cloudflare_zone.domain.id
  name    = "_gitlab-pages-verification-code"
  content = "gitlab-pages-verification-code=1f4febc042cc66ce229ad66b5a753a1f"
  type    = "TXT"
  ttl     = local.ttl
  proxied = "false"
}

resource "cloudflare_record" "TXT_gitlab_www_verification" {
  zone_id = cloudflare_zone.domain.id
  name    = "_gitlab-pages-verification-code.www"
  content = "gitlab-pages-verification-code=f9a2e9639fad7d59dccfbb01e2fca2d5"
  type    = "TXT"
  ttl     = local.ttl
  proxied = "false"
}
