---
title: "HNI CORP (HNI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HNI CORP</td></tr>
    <tr><td>Symbol</td><td>HNI</td></tr>
    <tr><td>Web</td><td><a href="https://www.hnicorp.com">www.hnicorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.305 |
| 2020 | 1.22 |
| 2019 | 1.21 |
| 2018 | 1.17 |
| 2017 | 1.13 |
| 2016 | 1.09 |
| 2015 | 1.045 |
| 2014 | 0.99 |
| 2013 | 0.48 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
