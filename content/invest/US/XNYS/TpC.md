---
title: "AT&T INC DEP SHS EACH REP 1/1000TH PFD SER C (TpC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AT&T INC DEP SHS EACH REP 1/1000TH PFD SER C</td></tr>
    <tr><td>Symbol</td><td>TpC</td></tr>
    <tr><td>Web</td><td><a href="http://www.att.com">www.att.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
