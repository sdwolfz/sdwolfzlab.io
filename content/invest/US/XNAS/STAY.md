---
title: " (STAY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>STAY</td></tr>
    <tr><td>Web</td><td><a href="https://www.esa.com">www.esa.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.44 |
| 2020 | 0.26 |
| 2019 | 0.91 |
| 2018 | 0.87 |
| 2017 | 0.82 |
| 2016 | 0.74 |
| 2015 | 0.91 |
| 2014 | 0.53 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
