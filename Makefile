#-------------------------------------------------------------------------------
# Settings
#-------------------------------------------------------------------------------

# By default the `help` goal is executed.
.DEFAULT_GLOBAL := help

# Location of this Makefile.
HERE := $(realpath $(dir $(realpath $(firstword $(MAKEFILE_LIST)))))

# Running a docker container in the current directory.
DOCKER_HERE := docker run --rm -it -u `id -u`:`id -g` -v "$(PWD)":/here -w /here

# Docker Image Versions
HUGO_VERSION      := "latest"
TERRAFORM_VERSION := "1.9.5"

#-------------------------------------------------------------------------------
# Utility goals
#-------------------------------------------------------------------------------

# # help
#
# Displays a help message containing usage instructions and a list of available
# goals.
#
# Usage:
#
# To display the help message run:
# ```bash
# make help
# ```
#
# This is also the default goal so you can run it as following:
# ```bash
# make
# ```
.PHONY: help
help:
	@echo 'Usage: make [<GOAL_1>, ...] [<VARIABLE_1>=value_1, ...]'
	@echo ''
	@echo 'Examples:'
	@echo '  make'
	@echo '  make help'
	@echo '  make css/webfonts'
	@echo ''
	@echo 'Utility goals:'
	@echo '  - help:   Displays this help message.'
	@echo '  - server: Start the hugo server locally.'
	@echo '  - open:   Open the main webpage in a browser.'
	@echo '  - shield'
	@echo '  - build'
	@echo '  - link'
	@echo '  - theme'
	@echo ''
	@echo 'Shell goals:'
	@echo '  - shell/port'
	@echo '  - shell/server'
	@echo '  - shell/terraform'
	@echo '  - shell/css'
	@echo ''
	@echo 'Project goals:'
	@echo '  - css:           Update all static resources'
	@echo '  - css/normalize: Update normalize.css'
	@echo '  - css/webfonts:  Update font awesome'
	@echo ''
	@echo 'CI goals:'
	@echo '  - ci'
	@echo '  - ci/editorconfig'
	@echo '  - ci/terraform'
	@echo '  - ci/yamllint'
	@echo ''
	@echo 'Lint goals:'
	@echo '  - lint/editorconfig'
	@echo '  - lint/terraform'
	@echo '  - lint/yamllint'
	@echo ''
	@echo 'Default goal: help'

#-------------------------------------------------------------------------------
# Shield

.PHONY: shield
shield:
	@git init terraform/.terraform

#-------------------------------------------------------------------------------
# Shell

.PHONY: shell
shell:
	@$(DOCKER_HERE) sdwolfz/hugo:$(HUGO_VERSION) sh

.PHONY: shell/port
shell/port:
	@$(DOCKER_HERE) -p 1313:1313 sdwolfz/hugo:$(HUGO_VERSION) sh

# # server
#
# Start the hugo server locally.
#
# Usage:
#
# ```bash
# make server
# ```
.PHONY: server
server:
	@hugo server --bind 0.0.0.0 --disableFastRender -F

.PHONY: shell/server
shell/server:
	@$(DOCKER_HERE) -p 1313:1313 sdwolfz/hugo:$(HUGO_VERSION) make server

.PHONY: shell/terraform
shell/terraform:
	@envchain website                             \
		docker run                                  \
			--rm -it                                  \
			-u `id -u`:`id -g`                        \
			-v ~/.secrets/Terraform/website:/.tfstate \
			-v $(PWD):/work -w /work/terraform        \
			-e CLOUDFLARE_API_TOKEN                   \
			sdwolfz/terraform:$(TERRAFORM_VERSION)

# # shell/css
#
# Open a shell in the `csso-cli` docker image.
#
# Usage:
#
# ```sh
# make shell/css
# ```
.PHONY: shell/css
shell/css:
	@$(DOCKER_HERE) sdwolfz/csso-cli:latest sh

#-------------------------------------------------------------------------------
# Open

# # open
#
# Open the main webpage in a browser.
#
# Usage:
#
# ```bash
# make open
# ```
.PHONY: open
open:
	@echo 'Opening: http://localhost:1313'
	@xdg-open http://localhost:1313

#-------------------------------------------------------------------------------
# Project goals
#-------------------------------------------------------------------------------

.PHONY: css
css: css/fontawesome css/roboto

.PHONY: css/fontawesome
css/fontawesome:
	@sh ./scripts/fontawesome.sh

.PHONY: css/roboto
css/roboto:
	@sh ./scripts/roboto.sh

.PHONY: clean
clean:
	@git clean -fdx

.PHONY: build
build:
	@hugo -F

.PHONY: link
link:
	@ln -s $(PWD)/themes/hugo-pro-coder ../hugo-pro-coder

.PHONY: theme
theme:
	@git clone https://gitlab.com/sdwolfz/hugo-pro-coder.git ./themes/hugo-pro-coder

# ------------------------------------------------------------------------------
# CI
# ------------------------------------------------------------------------------

DOCKER_HERE := docker run --rm -it -u `id -u`:`id -g` -v "$(PWD)":/here -w /here

.PHONY: ci
ci: ci/editorconfig ci/terraform ci/yamllint

.PHONY: ci/editorconfig
ci/editorconfig:
	@$(DOCKER_HERE) sdwolfz/editorconfig-checker:latest make lint/editorconfig

.PHONY: ci/terraform
ci/terraform:
	@$(DOCKER_HERE) sdwolfz/terraform:$(TERRAFORM_VERSION) make lint/terraform

.PHONY: ci/yamllint
ci/yamllint:
	@$(DOCKER_HERE) sdwolfz/yamllint:latest make lint/yamllint

# ------------------------------------------------------------------------------
# Lint

.PHONY: lint/editorconfig
lint/editorconfig:
	@editorconfig-checker $$(git ls-files)

.PHONY: lint/terraform
lint/terraform:
	@make -C terraform --no-print-directory init fmt validate
	@git diff --exit-code

.PHONY: lint/yamllint
lint/yamllint:
	@yamllint .
