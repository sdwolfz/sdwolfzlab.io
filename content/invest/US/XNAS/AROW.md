---
title: "ARROW FINANCIAL CORP (AROW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ARROW FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>AROW</td></tr>
    <tr><td>Web</td><td><a href="https://www.arrowfinancial.com">www.arrowfinancial.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.52 |
| 2020 | 1.04 |
| 2019 | 1.04 |
| 2018 | 1.02 |
| 2017 | 1.0 |
| 2016 | 1.0 |
| 2015 | 1.0 |
| 2014 | 1.0 |
| 2013 | 1.0 |
| 2012 | 1.0 |
| 2011 | 1.0 |
| 2010 | 1.0 |
| 2009 | 1.0 |
| 2008 | 0.98 |
| 2007 | 0.96 |
| 2006 | 0.96 |
| 2005 | 0.94 |
| 2004 | 0.91 |
| 2003 | 0.99 |
| 2002 | 0.98 |
| 2001 | 0.9 |
| 2000 | 0.8 |
| 1999 | 0.86 |
| 1998 | 0.85 |
| 1997 | 0.81 |
| 1996 | 0.71 |
| 1995 | 0.58 |
| 1994 | 0.38 |
| 1993 | 0.11 |
| 1991 | 0.28 |
| 1990 | 0.7 |
| 1989 | 0.64 |
| 1988 | 0.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
