---
title: "SEMPRA ENERGY (SRE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SEMPRA ENERGY</td></tr>
    <tr><td>Symbol</td><td>SRE</td></tr>
    <tr><td>Web</td><td><a href="https://www.sempra.com">www.sempra.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.1 |
| 2020 | 4.18 |
| 2019 | 3.868 |
| 2018 | 3.58 |
| 2017 | 3.288 |
| 2016 | 3.395 |
| 2015 | 2.8 |
| 2014 | 2.64 |
| 2013 | 1.89 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
