---
title: "SIFI CLUJ RETAIL SA Bucuresti (ARCU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>SIFI CLUJ RETAIL SA Bucuresti</td></tr>
    <tr><td>Symbol</td><td>ARCU</td></tr>
    <tr><td>Web</td><td><a href="https://www.artaculinaracluj.ro">www.artaculinaracluj.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.16 |
| 2019 | 0.138 |
| 2018 | 0.12 |
| 2017 | 0.09 |
| 2016 | 0.11 |
| 2010 | 0.018 |
| 2009 | 0.0515 |
| 2007 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
