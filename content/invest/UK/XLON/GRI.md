---
title: "Grainger plc (GRI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Grainger plc</td></tr>
    <tr><td>Symbol</td><td>GRI</td></tr>
    <tr><td>Web</td><td><a href="https://www.graingerplc.co.uk">www.graingerplc.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 5.47 |
| 2019 | 5.19 |
| 2018 | 5.26 |
| 2017 | 4.9 |
| 2016 | 4.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
