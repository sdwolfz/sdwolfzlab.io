---
title: "C3.AI INC (AI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>C3.AI INC</td></tr>
    <tr><td>Symbol</td><td>AI</td></tr>
    <tr><td>Web</td><td><a href="https://www.c3.ai">www.c3.ai</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 1.05 |
| 2018 | 1.675 |
| 2017 | 2.275 |
| 2016 | 2.5 |
| 2015 | 3.0 |
| 2014 | 3.5 |
| 2013 | 2.625 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
