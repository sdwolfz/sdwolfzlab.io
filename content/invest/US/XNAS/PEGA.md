---
title: "PEGASYSTEMS INC (PEGA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PEGASYSTEMS INC</td></tr>
    <tr><td>Symbol</td><td>PEGA</td></tr>
    <tr><td>Web</td><td><a href="https://www.pega.com">www.pega.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.03 |
| 2020 | 0.12 |
| 2019 | 0.12 |
| 2018 | 0.12 |
| 2017 | 0.12 |
| 2016 | 0.12 |
| 2015 | 0.12 |
| 2014 | 0.105 |
| 2013 | 0.12 |
| 2012 | 0.12 |
| 2011 | 0.12 |
| 2010 | 0.12 |
| 2009 | 0.12 |
| 2008 | 0.12 |
| 2007 | 0.15 |
| 2006 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
