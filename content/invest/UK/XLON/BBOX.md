---
title: "Tritax Big Box (BBOX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Tritax Big Box</td></tr>
    <tr><td>Symbol</td><td>BBOX</td></tr>
    <tr><td>Web</td><td><a href="https://www.tritaxbigbox.co.uk">www.tritaxbigbox.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 6.85 |
| 2018 | 6.7 |
| 2017 | 6.4 |
| 2016 | 6.2 |
| 2015 | 6.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
