---
title: "PHILLIPS 66 (PSX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PHILLIPS 66</td></tr>
    <tr><td>Symbol</td><td>PSX</td></tr>
    <tr><td>Web</td><td><a href="https://www.phillips66.com">www.phillips66.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.9 |
| 2020 | 3.6 |
| 2019 | 3.5 |
| 2018 | 3.1 |
| 2017 | 2.73 |
| 2016 | 2.45 |
| 2015 | 2.18 |
| 2014 | 1.89 |
| 2013 | 0.703 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
