---
title: " (SUPV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SUPV</td></tr>
    <tr><td>Web</td><td><a href="https://www.gruposupervielle.com">www.gruposupervielle.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.013 |
| 2020 | 0.03 |
| 2019 | 0.072 |
| 2018 | 0.107 |
| 2017 | 0.051 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
