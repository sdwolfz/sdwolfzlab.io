---
title: "James Fisher and Sons (FSJ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>James Fisher and Sons</td></tr>
    <tr><td>Symbol</td><td>FSJ</td></tr>
    <tr><td>Web</td><td><a href="https://www.james-fisher.co.uk">www.james-fisher.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 32.6 |
| 2018 | 29.6 |
| 2017 | 28.7 |
| 2016 | 26.1 |
| 2015 | 23.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
