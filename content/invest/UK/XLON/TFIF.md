---
title: "Twentyfour Inc (TFIF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Twentyfour Inc</td></tr>
    <tr><td>Symbol</td><td>TFIF</td></tr>
    <tr><td>Web</td><td><a href="https://www.twentyfouram.com">www.twentyfouram.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.4 |
| 2019 | 6.45 |
| 2018 | 7.23 |
| 2017 | 6.99 |
| 2016 | 7.14 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
