---
title: "DOLBY LABORATORIES INC (DLB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DOLBY LABORATORIES INC</td></tr>
    <tr><td>Symbol</td><td>DLB</td></tr>
    <tr><td>Web</td><td><a href="https://www.dolby.com">www.dolby.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.44 |
| 2020 | 0.88 |
| 2019 | 0.79 |
| 2018 | 0.67 |
| 2017 | 0.58 |
| 2016 | 0.5 |
| 2015 | 0.42 |
| 2014 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
