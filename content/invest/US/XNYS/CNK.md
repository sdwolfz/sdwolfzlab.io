---
title: "CINEMARK HOLDINGS INC (CNK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CINEMARK HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>CNK</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.36 |
| 2019 | 1.36 |
| 2018 | 1.28 |
| 2017 | 1.16 |
| 2016 | 1.08 |
| 2015 | 1.0 |
| 2014 | 1.0 |
| 2013 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
