---
title: "HTML Tabs without Javascript"
date: 2022-05-12T18:46:07Z
type: posts
slug: html-tabs-without-javascript
tags: ["html", "css"]
---

## HTML

```html
<div class="tabs tab-right">
  <style>
    .tabs input#tab1:checked ~ .tab-content-1,
    .tabs input#tab2:checked ~ .tab-content-2,
    .tabs input#tab3:checked ~ .tab-content-3 {
      display: block;
    }
  </style>

  <input type="radio" name="tab-select" id="tab1" checked />
  <label for="tab1">Ruby</label>
  <div class="tab-content tab-content-1">

~~~ruby
puts 'Hello'
~~~

  </div>

  <input type="radio" name="tab-select" id="tab2" />
  <label for="tab2">Python</label>
  <div class="tab-content tab-content-2">

~~~python
print('Hello')
~~~

  </div>

  <input type="radio" name="tab-select" id="tab3" />
  <label for="tab3">Javascript</label>
  <div class="tab-content tab-content-3">

~~~js
console.log('Hello')
~~~

  </div>
</div>
```

## CSS

```css
<style>
  /* Parent */
  .tabs {
    margin: 2rem 0 2rem 0;
    display: flex;
    flex-wrap: wrap;
    position: relative;
  }
  .tabs.tab-left {
    justify-content: flex-start;
  }
  .tabs.tab-right {
    justify-content: flex-end;
  }

  /* Floating */
  .tabs.tab-left label {
    margin-right: 0.5rem;
  }
  .tabs.tab-right label {
    margin-left: 0.5rem;
  }

  /* Input */
  .tabs input {
    display: none;
  }

  /* Label */
  .tabs label {
    background-color: #e0e0e0;
    border-width: 1px;
    border-style: solid;
    border-color: #ccc;
    border-bottom-style: hidden;
    border-radius: 4px 4px 0px 0px;
    position: relative;
    cursor: pointer;
    display: inline-block;
    padding: 0.3rem 0.6rem;
    user-select: none;
    order: 1;
    position: relative;
    top: 1px;
  }
  .tabs input:checked + label {
    background-color: #fafafa;
  }

  /* Content */
  .tabs .tab-content {
    background-color: #fafafa;
    display: none;
    padding: 1rem;
    border-width: 1px;
    border-style: solid;
    border-color: #ccc;
    width: 100%;
    order: 2;
  }
  .tabs.tab-left .tab-content {
    border-radius: 0px 4px 4px 4px;
  }
  .tabs.tab-right .tab-content {
    border-radius: 4px 0px 4px 4px;
  }
</style>
```

## Result

Some text at the top to see it's positioned properly!

<style>
  /* Parent */
  .tabs {
    margin: 2rem 0 2rem 0;
    display: flex;
    flex-wrap: wrap;
    position: relative;
  }
  .tabs.tab-left {
    justify-content: flex-start;
  }
  .tabs.tab-right {
    justify-content: flex-end;
  }

  /* Floating */
  .tabs.tab-left label {
    margin-right: 0.5rem;
  }
  .tabs.tab-right label {
    margin-left: 0.5rem;
  }

  /* Input */
  .tabs input {
    display: none;
  }

  /* Label */
  .tabs label {
    background-color: #e0e0e0;
    border-width: 1px;
    border-style: solid;
    border-color: #ccc;
    border-bottom-style: hidden;
    border-radius: 4px 4px 0px 0px;
    position: relative;
    cursor: pointer;
    display: inline-block;
    padding: 0.3rem 0.6rem;
    user-select: none;
    order: 1;
    position: relative;
    top: 1px;
  }
  .tabs input:checked + label {
    background-color: #fafafa;
  }

  /* Content */
  .tabs .tab-content {
    background-color: #fafafa;
    display: none;
    padding: 1rem;
    border-width: 1px;
    border-style: solid;
    border-color: #ccc;
    width: 100%;
    order: 2;
  }
  .tabs.tab-left .tab-content {
    border-radius: 0px 4px 4px 4px;
  }
  .tabs.tab-right .tab-content {
    border-radius: 4px 0px 4px 4px;
  }
</style>

<div class="tabs tab-right">
  <style>
    .tabs input#tab1:checked ~ .tab-content-1,
    .tabs input#tab2:checked ~ .tab-content-2,
    .tabs input#tab3:checked ~ .tab-content-3 {
      display: block;
    }
  </style>

  <input type="radio" name="tab-select" id="tab1" checked />
  <label for="tab1">Ruby</label>
  <div class="tab-content tab-content-1">

```ruby
puts 'Hello'
```

  </div>

  <input type="radio" name="tab-select" id="tab2" />
  <label for="tab2">Python</label>
  <div class="tab-content tab-content-2">

```python
print('Hello')
```

  </div>

  <input type="radio" name="tab-select" id="tab3" />
  <label for="tab3">Javascript</label>
  <div class="tab-content tab-content-3">

```js
console.log('Hello')
```
</div>

</div>

And also at the bottom for the same reason.
