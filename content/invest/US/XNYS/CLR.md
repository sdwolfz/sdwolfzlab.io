---
title: "CONTINENTAL RESOURCES INC (CLR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CONTINENTAL RESOURCES INC</td></tr>
    <tr><td>Symbol</td><td>CLR</td></tr>
    <tr><td>Web</td><td><a href="https://www.contres.com">www.contres.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.11 |
| 2020 | 0.05 |
| 2019 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
