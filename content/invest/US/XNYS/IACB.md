---
title: "ION ACQUISITION CORP 2 LTD (IACB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ION ACQUISITION CORP 2 LTD</td></tr>
    <tr><td>Symbol</td><td>IACB</td></tr>
    <tr><td>Web</td><td><a href="https://www.ion-am.com">www.ion-am.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
