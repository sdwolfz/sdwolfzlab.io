---
title: "Melrose (MRO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Melrose</td></tr>
    <tr><td>Symbol</td><td>MRO</td></tr>
    <tr><td>Web</td><td><a href="https://www.melroseplc.net">www.melroseplc.net</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 5.1 |
| 2018 | 4.6 |
| 2017 | 4.2 |
| 2016 | 3.3 |
| 2015 | 5.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
