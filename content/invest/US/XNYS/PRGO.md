---
title: "PERRIGO COMPANY PLC (PRGO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PERRIGO COMPANY PLC</td></tr>
    <tr><td>Symbol</td><td>PRGO</td></tr>
    <tr><td>Web</td><td><a href="https://www.perrigo.com">www.perrigo.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.48 |
| 2020 | 0.9 |
| 2019 | 0.82 |
| 2018 | 0.76 |
| 2017 | 0.64 |
| 2016 | 0.58 |
| 2015 | 0.5 |
| 2014 | 0.42 |
| 2013 | 0.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
