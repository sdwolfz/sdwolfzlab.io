---
title: "PRIMERICA INC (PRI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PRIMERICA INC</td></tr>
    <tr><td>Symbol</td><td>PRI</td></tr>
    <tr><td>Web</td><td><a href="https://www.primerica.com">www.primerica.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.94 |
| 2020 | 1.6 |
| 2019 | 1.36 |
| 2018 | 1.0 |
| 2017 | 0.78 |
| 2016 | 0.7 |
| 2015 | 0.64 |
| 2014 | 0.48 |
| 2013 | 0.22 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
