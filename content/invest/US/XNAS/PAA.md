---
title: " (PAA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>PAA</td></tr>
    <tr><td>Web</td><td><a href="https://www.plainsallamerican.com">www.plainsallamerican.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.36 |
| 2020 | 0.9 |
| 2019 | 1.38 |
| 2018 | 1.2 |
| 2017 | 1.95 |
| 2016 | 2.65 |
| 2015 | 2.755 |
| 2014 | 2.55 |
| 2013 | 2.325 |
| 2012 | 3.677 |
| 2011 | 3.905 |
| 2010 | 3.755 |
| 2009 | 3.623 |
| 2008 | 3.495 |
| 2007 | 3.282 |
| 2006 | 2.87 |
| 2005 | 2.575 |
| 2004 | 1.74 |
| 2003 | 2.188 |
| 2002 | 2.113 |
| 2001 | 1.95 |
| 2000 | 0.925 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
