---
title: " (KT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>KT</td></tr>
    <tr><td>Web</td><td><a href="https://www.kt.com">www.kt.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.51 |
| 2019 | 0.373 |
| 2018 | 0.398 |
| 2017 | 0.392 |
| 2013 | 0.3 |
| 2012 | 0.689 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
