---
title: "AMERICAN FINANCIAL GROUP INC OHIO (AFG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AMERICAN FINANCIAL GROUP INC OHIO</td></tr>
    <tr><td>Symbol</td><td>AFG</td></tr>
    <tr><td>Web</td><td><a href="https://www.afginc.com">www.afginc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.0 |
| 2020 | 3.85 |
| 2019 | 4.95 |
| 2018 | 4.45 |
| 2017 | 6.288 |
| 2016 | 1.153 |
| 2015 | 1.03 |
| 2014 | 0.91 |
| 2013 | 0.415 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
