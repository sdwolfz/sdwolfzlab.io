---
title: "AMALGAMATED FINL CORP (AMAL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AMALGAMATED FINL CORP</td></tr>
    <tr><td>Symbol</td><td>AMAL</td></tr>
    <tr><td>Web</td><td><a href="https://www.amalgamatedbank.com">www.amalgamatedbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.16 |
| 2020 | 0.32 |
| 2019 | 0.26 |
| 2018 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
