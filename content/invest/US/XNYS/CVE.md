---
title: "CENOVUS ENERGY INC (CVE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CENOVUS ENERGY INC</td></tr>
    <tr><td>Symbol</td><td>CVE</td></tr>
    <tr><td>Web</td><td><a href="https://www.cenovus.com">www.cenovus.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.047 |
| 2019 | 0.16 |
| 2018 | 0.155 |
| 2017 | 0.155 |
| 2016 | 0.15 |
| 2015 | 0.812 |
| 2014 | 1.064 |
| 2013 | 0.484 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
