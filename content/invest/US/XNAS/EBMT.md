---
title: "EAGLE BANCORP MONTANA INC (EBMT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>EAGLE BANCORP MONTANA INC</td></tr>
    <tr><td>Symbol</td><td>EBMT</td></tr>
    <tr><td>Web</td><td><a href="https://www.opportunitybank.com">www.opportunitybank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.195 |
| 2020 | 0.385 |
| 2019 | 0.375 |
| 2018 | 0.365 |
| 2017 | 0.34 |
| 2016 | 0.315 |
| 2015 | 0.305 |
| 2014 | 0.295 |
| 2013 | 0.289 |
| 2012 | 0.285 |
| 2011 | 0.283 |
| 2010 | 0.468 |
| 2009 | 1.03 |
| 2008 | 0.99 |
| 2007 | 0.92 |
| 2006 | 0.84 |
| 2005 | 0.76 |
| 2004 | 0.68 |
| 2003 | 0.58 |
| 2002 | 0.46 |
| 2001 | 0.34 |
| 2000 | 0.14 |
| 1996 | 0.14 |
| 1995 | 1.0 |
| 1994 | 1.01 |
| 1993 | 1.14 |
| 1992 | 0.82 |
| 1991 | 0.66 |
| 1990 | 0.46 |
| 1989 | 0.28 |
| 1988 | 0.07 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
