---
title: "PLANET GREEN HOLDINGS CORP (PLAG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PLANET GREEN HOLDINGS CORP</td></tr>
    <tr><td>Symbol</td><td>PLAG</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2007 | 0.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
