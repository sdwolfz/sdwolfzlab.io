---
title: "IF BANCORP INC (IROQ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>IF BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>IROQ</td></tr>
    <tr><td>Web</td><td><a href="https://www.iroquoisfed.com">www.iroquoisfed.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.15 |
| 2020 | 0.3 |
| 2019 | 0.275 |
| 2018 | 0.225 |
| 2017 | 0.18 |
| 2016 | 0.16 |
| 2015 | 0.1 |
| 2014 | 0.1 |
| 2013 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
