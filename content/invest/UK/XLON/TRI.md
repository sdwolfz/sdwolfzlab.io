---
title: "Trifast (TRI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Trifast</td></tr>
    <tr><td>Symbol</td><td>TRI</td></tr>
    <tr><td>Web</td><td><a href="https://www.trifast.com">www.trifast.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.2 |
| 2019 | 4.25 |
| 2018 | 3.85 |
| 2017 | 3.5 |
| 2016 | 2.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
