---
title: "St James Place (STJ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>St James Place</td></tr>
    <tr><td>Symbol</td><td>STJ</td></tr>
    <tr><td>Web</td><td><a href="https://www.sjp.co.uk">www.sjp.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 49.71 |
| 2018 | 48.22 |
| 2017 | 42.86 |
| 2016 | 33.0 |
| 2015 | 27.96 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
