---
title: "ACRES COMMERCIAL REALTY CORP (ACR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ACRES COMMERCIAL REALTY CORP</td></tr>
    <tr><td>Symbol</td><td>ACR</td></tr>
    <tr><td>Web</td><td><a href="https://www.acresreit.com">www.acresreit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
