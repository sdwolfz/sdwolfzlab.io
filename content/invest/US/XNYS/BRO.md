---
title: "BROWN & BROWN INC (BRO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BROWN & BROWN INC</td></tr>
    <tr><td>Symbol</td><td>BRO</td></tr>
    <tr><td>Web</td><td><a href="https://www.bbinsurance.com">www.bbinsurance.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.186 |
| 2020 | 0.348 |
| 2019 | 0.325 |
| 2018 | 0.305 |
| 2017 | 0.555 |
| 2016 | 0.504 |
| 2015 | 0.453 |
| 2014 | 0.41 |
| 2013 | 0.19 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
