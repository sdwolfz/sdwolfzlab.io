---
title: "CASSAVA SCIENCES INC (SAVA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CASSAVA SCIENCES INC</td></tr>
    <tr><td>Symbol</td><td>SAVA</td></tr>
    <tr><td>Web</td><td><a href="https://www.cassavasciences.com">www.cassavasciences.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2012 | 0.75 |
| 2010 | 2.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
