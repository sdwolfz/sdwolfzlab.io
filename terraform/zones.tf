resource "cloudflare_zone" "domain" {
  account_id = cloudflare_account.account.id
  zone       = local.domain
  paused     = false
  plan       = "free"
  type       = "full"
}

resource "cloudflare_zone_dnssec" "domain_DNSSEC" {
  zone_id = cloudflare_zone.domain.id
}
