---
title: "RIVERNORTH FLEXIBLE MUNI INM FD INC (RFM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>RIVERNORTH FLEXIBLE MUNI INM FD INC</td></tr>
    <tr><td>Symbol</td><td>RFM</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.52 |
| 2020 | 0.832 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
