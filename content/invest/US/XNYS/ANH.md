---
title: "ANWORTH MORTGAGE ASSET CORP (ANH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ANWORTH MORTGAGE ASSET CORP</td></tr>
    <tr><td>Symbol</td><td>ANH</td></tr>
    <tr><td>Web</td><td><a href="http://www.anworth.com">www.anworth.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.2 |
| 2019 | 0.43 |
| 2018 | 0.56 |
| 2017 | 0.6 |
| 2016 | 0.6 |
| 2015 | 0.6 |
| 2014 | 0.56 |
| 2013 | 0.35 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
