---
title: "CONFECTII VASLUI SA Vaslui (COVB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>CONFECTII VASLUI SA Vaslui</td></tr>
    <tr><td>Symbol</td><td>COVB</td></tr>
    <tr><td>Web</td><td><a href="https://www.confectiivaslui.com">www.confectiivaslui.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.00286 |
| 2019 | 0.00286 |
| 2018 | 0.00286 |
| 2009 | 0.0108 |
| 2008 | 0.0103 |
| 2007 | 0.0135 |
| 2005 | 1.0153 |
| 2004 | 0.0281 |
| 2003 | 0.396 |
| 2002 | 0.3734 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
