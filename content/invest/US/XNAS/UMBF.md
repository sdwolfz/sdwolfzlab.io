---
title: "UMB FINANCIAL CORP (UMBF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>UMB FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>UMBF</td></tr>
    <tr><td>Web</td><td><a href="https://www.umb.com">www.umb.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.64 |
| 2020 | 1.25 |
| 2019 | 1.21 |
| 2018 | 1.17 |
| 2017 | 1.04 |
| 2016 | 0.99 |
| 2015 | 0.95 |
| 2014 | 0.91 |
| 2013 | 0.87 |
| 2012 | 0.83 |
| 2011 | 0.79 |
| 2010 | 0.75 |
| 2009 | 0.71 |
| 2008 | 0.655 |
| 2007 | 0.57 |
| 2006 | 0.64 |
| 2005 | 0.91 |
| 2004 | 0.85 |
| 2003 | 0.81 |
| 2002 | 0.8 |
| 2001 | 0.6 |
| 2000 | 0.8 |
| 1999 | 0.6 |
| 1998 | 0.8 |
| 1997 | 0.6 |
| 1996 | 0.6 |
| 1995 | 0.8 |
| 1994 | 0.6 |
| 1993 | 0.8 |
| 1992 | 0.8 |
| 1991 | 0.6 |
| 1990 | 0.8 |
| 1989 | 0.87 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
