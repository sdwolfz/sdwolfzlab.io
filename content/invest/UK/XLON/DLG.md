---
title: "Direct Line (DLG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Direct Line</td></tr>
    <tr><td>Symbol</td><td>DLG</td></tr>
    <tr><td>Web</td><td><a href="https://www.directlinegroup.com">www.directlinegroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 22.1 |
| 2019 | 21.6 |
| 2018 | 29.3 |
| 2017 | 35.4 |
| 2016 | 24.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
