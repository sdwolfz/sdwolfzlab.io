---
title: " (ARLP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ARLP</td></tr>
    <tr><td>Web</td><td><a href="https://www.arlp.com">www.arlp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.4 |
| 2019 | 2.145 |
| 2018 | 2.07 |
| 2017 | 1.88 |
| 2016 | 1.988 |
| 2015 | 2.663 |
| 2014 | 3.683 |
| 2013 | 4.565 |
| 2012 | 4.163 |
| 2011 | 3.628 |
| 2010 | 3.205 |
| 2009 | 2.95 |
| 2008 | 2.53 |
| 2007 | 2.2 |
| 2006 | 1.92 |
| 2005 | 2.738 |
| 2004 | 2.488 |
| 2003 | 2.1 |
| 2002 | 2.0 |
| 2001 | 2.0 |
| 2000 | 2.0 |
| 1999 | 0.23 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
