---
title: "MOBEST SA BUCURESTI (MOBE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>MOBEST SA BUCURESTI</td></tr>
    <tr><td>Symbol</td><td>MOBE</td></tr>
    <tr><td>Web</td><td><a href="https://www.mobest.ro">www.mobest.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.11 |
| 2019 | 0.09 |
| 2018 | 0.1722 |
| 2017 | 0.26715 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
