---
title: "FIRST FOUNDATION INC (FFWM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST FOUNDATION INC</td></tr>
    <tr><td>Symbol</td><td>FFWM</td></tr>
    <tr><td>Web</td><td><a href="https://www.ff-inc.com">www.ff-inc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.18 |
| 2020 | 0.28 |
| 2019 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
