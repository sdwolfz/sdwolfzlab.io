---
title: " (JHX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>JHX</td></tr>
    <tr><td>Web</td><td><a href="https://www.jameshardie.com">www.jameshardie.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.298 |
| 2018 | 0.4 |
| 2017 | 0.284 |
| 2016 | 0.302 |
| 2015 | 0.455 |
| 2014 | 1.06 |
| 2013 | 1.575 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
