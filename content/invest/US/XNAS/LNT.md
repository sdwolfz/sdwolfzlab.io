---
title: "ALLIANT ENERGY CORP (LNT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ALLIANT ENERGY CORP</td></tr>
    <tr><td>Symbol</td><td>LNT</td></tr>
    <tr><td>Web</td><td><a href="https://www.alliantenergy.com">www.alliantenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.805 |
| 2020 | 1.52 |
| 2019 | 1.42 |
| 2018 | 1.341 |
| 2017 | 1.26 |
| 2016 | 1.763 |
| 2015 | 2.2 |
| 2014 | 2.04 |
| 2013 | 1.88 |
| 2012 | 1.8 |
| 2011 | 1.7 |
| 2010 | 1.58 |
| 2009 | 1.5 |
| 2008 | 1.4 |
| 2007 | 1.27 |
| 2006 | 1.15 |
| 2005 | 1.05 |
| 2004 | 1.013 |
| 2003 | 1.0 |
| 2002 | 2.0 |
| 2001 | 2.0 |
| 2000 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
