---
title: "SPIRIT AEROSYSTEMS HOLDINGS INC (SPR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SPIRIT AEROSYSTEMS HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>SPR</td></tr>
    <tr><td>Web</td><td><a href="https://www.spiritaero.com">www.spiritaero.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.02 |
| 2020 | 0.04 |
| 2019 | 0.48 |
| 2018 | 0.46 |
| 2017 | 0.4 |
| 2016 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
