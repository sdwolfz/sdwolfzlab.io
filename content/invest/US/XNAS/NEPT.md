---
title: "NEPTUNE WELLNESS SOLUTIONS INC (NEPT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NEPTUNE WELLNESS SOLUTIONS INC</td></tr>
    <tr><td>Symbol</td><td>NEPT</td></tr>
    <tr><td>Web</td><td><a href="https://www.neptunecorp.com">www.neptunecorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2012 | 0.003 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
