---
title: " (IBA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>IBA</td></tr>
    <tr><td>Web</td><td><a href="https://www.bachoco.com.mx">www.bachoco.com.mx</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.401 |
| 2020 | 0.657 |
| 2019 | 0.856 |
| 2018 | 0.861 |
| 2017 | 0.824 |
| 2016 | 0.83 |
| 2015 | 1.145 |
| 2013 | 0.263 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
