---
title: "MECANOENERGETICA SA Dr. Tr. Severin (MEGU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>MECANOENERGETICA SA Dr. Tr. Severin</td></tr>
    <tr><td>Symbol</td><td>MEGU</td></tr>
    <tr><td>Web</td><td><a href="https://www.mecanoenergetica.ro">www.mecanoenergetica.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
