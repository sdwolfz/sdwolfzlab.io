---
title: "Keller (KLR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Keller</td></tr>
    <tr><td>Symbol</td><td>KLR</td></tr>
    <tr><td>Web</td><td><a href="https://www.keller.com">www.keller.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 35.9 |
| 2017 | 34.2 |
| 2016 | 28.5 |
| 2015 | 27.1 |
| 2014 | 25.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
