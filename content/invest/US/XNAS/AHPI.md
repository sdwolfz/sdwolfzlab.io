---
title: "ALLIED HEALTHCARE PRODUCTS INC (AHPI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ALLIED HEALTHCARE PRODUCTS INC</td></tr>
    <tr><td>Symbol</td><td>AHPI</td></tr>
    <tr><td>Web</td><td><a href="https://www.alliedhpi.com">www.alliedhpi.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 1996 | 0.14 |
| 1995 | 0.21 |
| 1994 | 0.25 |
| 1993 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
