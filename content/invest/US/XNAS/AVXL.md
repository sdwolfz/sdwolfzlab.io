---
title: "ANAVEX LIFE SCIENCES CORPORATION (AVXL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ANAVEX LIFE SCIENCES CORPORATION</td></tr>
    <tr><td>Symbol</td><td>AVXL</td></tr>
    <tr><td>Web</td><td><a href="https://www.anavex.com">www.anavex.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
