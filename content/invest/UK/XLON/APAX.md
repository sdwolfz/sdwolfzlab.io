---
title: "Apax Glb (APAX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Apax Glb</td></tr>
    <tr><td>Symbol</td><td>APAX</td></tr>
    <tr><td>Web</td><td><a href="https://www.apaxglobalalpha.com">www.apaxglobalalpha.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 11.15 |
| 2019 | 10.86 |
| 2018 | 9.56 |
| 2017 | 8.41 |
| 2016 | 8.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
