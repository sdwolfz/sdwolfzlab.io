---
title: "Barratt Developments (BDEV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Barratt Developments</td></tr>
    <tr><td>Symbol</td><td>BDEV</td></tr>
    <tr><td>Web</td><td><a href="https://www.barrattdevelopments.co.uk">www.barrattdevelopments.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 36.67 |
| 2019 | 46.4 |
| 2018 | 43.8 |
| 2017 | 41.7 |
| 2016 | 30.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
