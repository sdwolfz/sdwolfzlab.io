---
title: "CRA INTERNATIONAL INC (CRAI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CRA INTERNATIONAL INC</td></tr>
    <tr><td>Symbol</td><td>CRAI</td></tr>
    <tr><td>Web</td><td><a href="https://www.crai.com">www.crai.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.52 |
| 2020 | 0.95 |
| 2019 | 0.83 |
| 2018 | 0.71 |
| 2017 | 0.59 |
| 2016 | 0.14 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
