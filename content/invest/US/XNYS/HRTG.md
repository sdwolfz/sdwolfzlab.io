---
title: "HERITAGE INSURANCE HLDGS INC (HRTG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HERITAGE INSURANCE HLDGS INC</td></tr>
    <tr><td>Symbol</td><td>HRTG</td></tr>
    <tr><td>Web</td><td><a href="https://investors.heritagepci.com">investors.heritagepci.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 0.24 |
| 2019 | 0.24 |
| 2018 | 0.24 |
| 2017 | 0.24 |
| 2016 | 0.23 |
| 2015 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
