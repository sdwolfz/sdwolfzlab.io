---
title: "CABOT CORP (CBT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CABOT CORP</td></tr>
    <tr><td>Symbol</td><td>CBT</td></tr>
    <tr><td>Web</td><td><a href="https://www.cabotcorp.com">www.cabotcorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.35 |
| 2020 | 1.4 |
| 2019 | 1.38 |
| 2018 | 1.305 |
| 2017 | 1.245 |
| 2016 | 1.12 |
| 2015 | 0.88 |
| 2014 | 0.86 |
| 2013 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
