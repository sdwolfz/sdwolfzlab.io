---
title: "INVACARE CORP (IVC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>INVACARE CORP</td></tr>
    <tr><td>Symbol</td><td>IVC</td></tr>
    <tr><td>Web</td><td><a href="https://www.invacare.com">www.invacare.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.026 |
| 2019 | 0.052 |
| 2018 | 0.052 |
| 2017 | 0.039 |
| 2016 | 0.052 |
| 2015 | 0.052 |
| 2014 | 0.052 |
| 2013 | 0.026 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
