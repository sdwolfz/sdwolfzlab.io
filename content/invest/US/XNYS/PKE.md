---
title: "PARK AEROSPACE CORP (PKE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PARK AEROSPACE CORP</td></tr>
    <tr><td>Symbol</td><td>PKE</td></tr>
    <tr><td>Web</td><td><a href="https://www.parkaerospace.com">www.parkaerospace.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 1.4 |
| 2019 | 4.65 |
| 2018 | 3.4 |
| 2017 | 0.4 |
| 2016 | 0.4 |
| 2015 | 0.4 |
| 2014 | 0.4 |
| 2013 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
