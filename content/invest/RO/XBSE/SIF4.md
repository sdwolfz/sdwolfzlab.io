---
title: "SIF MUNTENIA S.A. (SIF4)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>SIF MUNTENIA S.A.</td></tr>
    <tr><td>Symbol</td><td>SIF4</td></tr>
    <tr><td>Web</td><td><a href="https://www.sifmuntenia.ro">www.sifmuntenia.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 0.0347 |
| 2016 | 0.04 |
| 2015 | 0.045 |
| 2014 | 0.0715 |
| 2012 | 0.134 |
| 2011 | 0.081 |
| 2010 | 0.081 |
| 2009 | 0.04 |
| 2008 | 0.04 |
| 2007 | 0.07 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
