---
title: " (CIB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CIB</td></tr>
    <tr><td>Web</td><td><a href="https://www.grupobancolombia.com">www.grupobancolombia.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.064 |
| 2020 | 1.519 |
| 2019 | 1.222 |
| 2018 | 1.348 |
| 2017 | 1.272 |
| 2016 | 0.878 |
| 2015 | 1.173 |
| 2014 | 1.506 |
| 2013 | 0.776 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
