---
title: " (CMCT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CMCT</td></tr>
    <tr><td>Web</td><td><a href="https://www.cimcommercial.com">www.cimcommercial.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.075 |
| 2020 | 0.3 |
| 2019 | 14.4 |
| 2018 | 0.5 |
| 2017 | 3.584 |
| 2016 | 0.875 |
| 2015 | 0.875 |
| 2014 | 6.261 |
| 2013 | 0.5 |
| 2012 | 0.6 |
| 2011 | 0.64 |
| 2010 | 0.64 |
| 2009 | 0.705 |
| 2008 | 1.015 |
| 2007 | 1.2 |
| 2006 | 1.3 |
| 2005 | 1.55 |
| 2004 | 1.4 |
| 2003 | 1.54 |
| 2002 | 1.62 |
| 2001 | 1.52 |
| 2000 | 1.745 |
| 1999 | 0.92 |
| 1994 | 1.02 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
