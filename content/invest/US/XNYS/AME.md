---
title: "AMETEK INC (AME)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AMETEK INC</td></tr>
    <tr><td>Symbol</td><td>AME</td></tr>
    <tr><td>Web</td><td><a href="https://www.ametek.com">www.ametek.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.72 |
| 2019 | 0.56 |
| 2018 | 0.56 |
| 2017 | 0.36 |
| 2016 | 0.36 |
| 2015 | 0.36 |
| 2014 | 0.33 |
| 2013 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
