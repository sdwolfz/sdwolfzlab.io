---
title: "Smith (DS) (SMDS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Smith (DS)</td></tr>
    <tr><td>Symbol</td><td>SMDS</td></tr>
    <tr><td>Web</td><td><a href="https://www.dssmith.com">www.dssmith.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 16.2 |
| 2018 | 14.7 |
| 2017 | 15.2 |
| 2016 | 12.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
