---
title: "GW PHARMACEUTICALS ADS EA REPR 12 ORD GBP0.00 (GWPH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GW PHARMACEUTICALS ADS EA REPR 12 ORD GBP0.00</td></tr>
    <tr><td>Symbol</td><td>GWPH</td></tr>
    <tr><td>Web</td><td><a href="http://www.gwpharm.com">www.gwpharm.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
