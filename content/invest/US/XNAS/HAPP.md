---
title: "HAPPINESS BIOTECH GROUP LTD (HAPP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HAPPINESS BIOTECH GROUP LTD</td></tr>
    <tr><td>Symbol</td><td>HAPP</td></tr>
    <tr><td>Web</td><td><a href="https://www.fjxfl.com">www.fjxfl.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.015 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
