---
title: " (AINV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>AINV</td></tr>
    <tr><td>Web</td><td><a href="https://www.apolloic.com">www.apolloic.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.36 |
| 2020 | 1.62 |
| 2019 | 1.8 |
| 2018 | 0.9 |
| 2017 | 0.6 |
| 2016 | 0.7 |
| 2015 | 0.8 |
| 2014 | 0.8 |
| 2013 | 0.8 |
| 2012 | 0.8 |
| 2011 | 1.12 |
| 2010 | 1.12 |
| 2009 | 1.08 |
| 2008 | 2.08 |
| 2007 | 2.06 |
| 2006 | 1.87 |
| 2005 | 1.44 |
| 2004 | 0.225 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
