---
title: "DENTSPLY SIRONA INC (XRAY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>DENTSPLY SIRONA INC</td></tr>
    <tr><td>Symbol</td><td>XRAY</td></tr>
    <tr><td>Web</td><td><a href="https://www.dentsplysirona.com">www.dentsplysirona.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.4 |
| 2019 | 0.375 |
| 2018 | 0.35 |
| 2017 | 0.35 |
| 2016 | 0.31 |
| 2015 | 0.29 |
| 2014 | 0.265 |
| 2013 | 0.25 |
| 2012 | 0.22 |
| 2011 | 0.205 |
| 2010 | 0.2 |
| 2009 | 0.2 |
| 2008 | 0.185 |
| 2007 | 0.165 |
| 2006 | 0.215 |
| 2005 | 0.25 |
| 2004 | 0.217 |
| 2003 | 0.197 |
| 2002 | 0.184 |
| 2001 | 0.275 |
| 2000 | 0.256 |
| 1999 | 0.231 |
| 1998 | 0.21 |
| 1997 | 0.339 |
| 1996 | 0.34 |
| 1995 | 0.308 |
| 1994 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
