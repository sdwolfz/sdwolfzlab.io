---
title: "PRODLACTA SA Brasov (PRAE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>PRODLACTA SA Brasov</td></tr>
    <tr><td>Symbol</td><td>PRAE</td></tr>
    <tr><td>Web</td><td><a href="https://www.prodlacta.ro">www.prodlacta.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2003 | 0.0085 |
| 2002 | 0.0118 |
| 2001 | 0.0125 |
| 2000 | 0.0131 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
