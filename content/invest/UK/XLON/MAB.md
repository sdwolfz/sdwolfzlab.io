---
title: "Mitchells & Butlers (MAB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Mitchells & Butlers</td></tr>
    <tr><td>Symbol</td><td>MAB</td></tr>
    <tr><td>Web</td><td><a href="https://www.mbplc.com">www.mbplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 7.5 |
| 2017 | 7.5 |
| 2016 | 7.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
