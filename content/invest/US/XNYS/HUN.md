---
title: "HUNTSMAN CORP (HUN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HUNTSMAN CORP</td></tr>
    <tr><td>Symbol</td><td>HUN</td></tr>
    <tr><td>Web</td><td><a href="https://www.huntsman.com">www.huntsman.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.35 |
| 2020 | 0.648 |
| 2019 | 0.648 |
| 2018 | 0.648 |
| 2017 | 0.5 |
| 2016 | 0.5 |
| 2015 | 0.5 |
| 2014 | 0.5 |
| 2013 | 0.375 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
