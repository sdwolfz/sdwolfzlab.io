---
title: "FREEPORT-MCMORAN INC (FCX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FREEPORT-MCMORAN INC</td></tr>
    <tr><td>Symbol</td><td>FCX</td></tr>
    <tr><td>Web</td><td><a href="https://www.fcx.com">www.fcx.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.075 |
| 2020 | 0.05 |
| 2019 | 0.2 |
| 2018 | 0.15 |
| 2015 | 0.463 |
| 2014 | 1.252 |
| 2013 | 0.626 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
