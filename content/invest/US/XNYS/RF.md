---
title: "REGIONS FINANCIAL CORP (RF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>REGIONS FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>RF</td></tr>
    <tr><td>Web</td><td><a href="https://www.regions.com">www.regions.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.31 |
| 2020 | 0.62 |
| 2019 | 0.59 |
| 2018 | 0.46 |
| 2017 | 0.315 |
| 2016 | 0.255 |
| 2015 | 0.23 |
| 2014 | 0.18 |
| 2013 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
