---
title: "PHX MINERALS INC (PHX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PHX MINERALS INC</td></tr>
    <tr><td>Symbol</td><td>PHX</td></tr>
    <tr><td>Web</td><td><a href="https://www.phxmin.com">www.phxmin.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.02 |
| 2020 | 0.07 |
| 2019 | 0.16 |
| 2018 | 0.16 |
| 2017 | 0.2 |
| 2016 | 0.16 |
| 2015 | 0.16 |
| 2014 | 0.24 |
| 2013 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
