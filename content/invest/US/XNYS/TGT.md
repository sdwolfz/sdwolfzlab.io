---
title: "TARGET CORP (TGT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TARGET CORP</td></tr>
    <tr><td>Symbol</td><td>TGT</td></tr>
    <tr><td>Web</td><td><a href="https://investors.target.com">investors.target.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.36 |
| 2020 | 2.68 |
| 2019 | 2.6 |
| 2018 | 2.52 |
| 2017 | 2.44 |
| 2016 | 2.32 |
| 2015 | 2.16 |
| 2014 | 1.9 |
| 2013 | 0.86 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
