---
title: " (FEDU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>FEDU</td></tr>
    <tr><td>Web</td><td><a href="https://www.sijiedu.com">www.sijiedu.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
