---
title: "UMPQUA HOLDINGS CORP (UMPQ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>UMPQUA HOLDINGS CORP</td></tr>
    <tr><td>Symbol</td><td>UMPQ</td></tr>
    <tr><td>Web</td><td><a href="https://www.umpquabank.com">www.umpquabank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.42 |
| 2020 | 0.63 |
| 2019 | 0.84 |
| 2018 | 0.82 |
| 2017 | 0.68 |
| 2016 | 0.64 |
| 2015 | 0.62 |
| 2014 | 0.6 |
| 2013 | 0.6 |
| 2012 | 0.34 |
| 2011 | 0.24 |
| 2010 | 0.2 |
| 2009 | 0.2 |
| 2008 | 0.62 |
| 2007 | 0.74 |
| 2006 | 0.6 |
| 2005 | 0.32 |
| 2004 | 0.22 |
| 2003 | 0.16 |
| 2002 | 0.16 |
| 2001 | 0.16 |
| 2000 | 0.16 |
| 1999 | 0.16 |
| 1998 | 0.09 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
