---
title: "Paragon Group (PAG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Paragon Group</td></tr>
    <tr><td>Symbol</td><td>PAG</td></tr>
    <tr><td>Web</td><td><a href="https://www.paragonbankinggroup.co.uk">www.paragonbankinggroup.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 14.4 |
| 2019 | 21.2 |
| 2018 | 19.4 |
| 2017 | 15.7 |
| 2016 | 13.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
