---
title: "Easter Eggs: Pacman"
date: 2023-04-08T13:28:45Z
type: posts
slug: easter-eggs-pacman
series: ["Easter Eggs"]
tags: ["linux"]
---

1. Open your `pacman.cfg` file:

```sh
sudo vim /etc/pacman.conf
```
2. Uncomment or add these instructions:

```conf
Color
ILoveCandy
```

3. Update your system with `sudo pacman -Syyu`.
