---
title: "BAKER HUGHES COMPANY (BKR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BAKER HUGHES COMPANY</td></tr>
    <tr><td>Symbol</td><td>BKR</td></tr>
    <tr><td>Web</td><td><a href="https://www.bakerhughes.com">www.bakerhughes.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.18 |
| 2020 | 0.72 |
| 2019 | 0.36 |
| 2013 | 0.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
