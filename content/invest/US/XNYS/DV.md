---
title: "DOUBLEVERIFY HLDGS INC (DV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DOUBLEVERIFY HLDGS INC</td></tr>
    <tr><td>Symbol</td><td>DV</td></tr>
    <tr><td>Web</td><td><a href="https://www.doubleverify.com">www.doubleverify.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.36 |
| 2015 | 0.36 |
| 2014 | 0.35 |
| 2013 | 0.34 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
