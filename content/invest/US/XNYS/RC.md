---
title: "READY CAPITAL CORPORATION (RC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>READY CAPITAL CORPORATION</td></tr>
    <tr><td>Symbol</td><td>RC</td></tr>
    <tr><td>Web</td><td><a href="https://www.readycapital.com">www.readycapital.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.4 |
| 2020 | 1.3 |
| 2019 | 1.6 |
| 2018 | 0.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
