---
title: "BANKFINANCIAL CORPORATION (BFIN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BANKFINANCIAL CORPORATION</td></tr>
    <tr><td>Symbol</td><td>BFIN</td></tr>
    <tr><td>Web</td><td><a href="https://www.bankfinancial.com">www.bankfinancial.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.4 |
| 2019 | 0.4 |
| 2018 | 0.37 |
| 2017 | 0.28 |
| 2016 | 0.21 |
| 2015 | 0.2 |
| 2014 | 0.08 |
| 2013 | 0.04 |
| 2012 | 0.03 |
| 2011 | 0.22 |
| 2010 | 0.28 |
| 2009 | 0.28 |
| 2008 | 0.28 |
| 2007 | 0.28 |
| 2006 | 0.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
