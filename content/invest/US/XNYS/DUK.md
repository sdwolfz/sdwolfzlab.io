---
title: "DUKE ENERGY CORP (DUK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DUKE ENERGY CORP</td></tr>
    <tr><td>Symbol</td><td>DUK</td></tr>
    <tr><td>Web</td><td><a href="https://www.duke-energy.com">www.duke-energy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.965 |
| 2020 | 3.82 |
| 2019 | 3.746 |
| 2018 | 3.636 |
| 2017 | 3.49 |
| 2016 | 3.36 |
| 2015 | 3.24 |
| 2014 | 3.15 |
| 2013 | 1.56 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
