---
title: "ITERIS INC (ITI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ITERIS INC</td></tr>
    <tr><td>Symbol</td><td>ITI</td></tr>
    <tr><td>Web</td><td><a href="https://www.iteris.com">www.iteris.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 1997 | 11.275 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
