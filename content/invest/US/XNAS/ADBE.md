---
title: "ADOBE INC (ADBE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ADOBE INC</td></tr>
    <tr><td>Symbol</td><td>ADBE</td></tr>
    <tr><td>Web</td><td><a href="https://www.adobe.com">www.adobe.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2005 | 0.013 |
| 2004 | 0.063 |
| 2003 | 0.05 |
| 2002 | 0.038 |
| 2001 | 0.063 |
| 2000 | 0.075 |
| 1999 | 0.175 |
| 1998 | 0.2 |
| 1997 | 0.25 |
| 1996 | 0.2 |
| 1995 | 0.2 |
| 1994 | 0.2 |
| 1993 | 0.33 |
| 1992 | 0.32 |
| 1991 | 0.3 |
| 1990 | 0.23 |
| 1989 | 0.19 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
