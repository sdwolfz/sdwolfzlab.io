---
title: "HERSHEY COMPANY (HSY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HERSHEY COMPANY</td></tr>
    <tr><td>Symbol</td><td>HSY</td></tr>
    <tr><td>Web</td><td><a href="https://www.thehersheycompany.com">www.thehersheycompany.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.608 |
| 2020 | 3.154 |
| 2019 | 2.99 |
| 2018 | 2.756 |
| 2017 | 2.548 |
| 2016 | 2.402 |
| 2015 | 2.236 |
| 2014 | 2.04 |
| 2013 | 0.97 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
