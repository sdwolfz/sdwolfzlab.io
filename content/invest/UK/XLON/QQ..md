---
title: "Qinetiq (QQ.)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Qinetiq</td></tr>
    <tr><td>Symbol</td><td>QQ.</td></tr>
    <tr><td>Web</td><td><a href="https://www.qinetiq.com">www.qinetiq.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.2 |
| 2019 | 6.6 |
| 2018 | 6.3 |
| 2017 | 6.0 |
| 2016 | 5.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
