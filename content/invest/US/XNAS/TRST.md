---
title: "TRUSTCO BANK CORP NY (TRST)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TRUSTCO BANK CORP NY</td></tr>
    <tr><td>Symbol</td><td>TRST</td></tr>
    <tr><td>Web</td><td><a href="https://www.trustcobank.com">www.trustcobank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.068 |
| 2020 | 0.273 |
| 2019 | 0.273 |
| 2018 | 0.268 |
| 2017 | 0.263 |
| 2016 | 0.263 |
| 2015 | 0.263 |
| 2014 | 0.263 |
| 2013 | 0.263 |
| 2012 | 0.263 |
| 2011 | 0.263 |
| 2010 | 0.256 |
| 2009 | 0.298 |
| 2008 | 0.44 |
| 2007 | 0.64 |
| 2006 | 0.64 |
| 2005 | 0.61 |
| 2004 | 0.6 |
| 2003 | 0.6 |
| 2002 | 0.6 |
| 2001 | 0.6 |
| 2000 | 0.6 |
| 1999 | 0.975 |
| 1998 | 1.1 |
| 1997 | 1.1 |
| 1996 | 1.1 |
| 1995 | 1.1 |
| 1994 | 1.05 |
| 1993 | 1.5 |
| 1992 | 1.75 |
| 1991 | 1.52 |
| 1990 | 1.32 |
| 1989 | 1.15 |
| 1988 | 0.275 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
