---
title: "Rentokil Initial (RTO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Rentokil Initial</td></tr>
    <tr><td>Symbol</td><td>RTO</td></tr>
    <tr><td>Web</td><td><a href="https://www.rentokil-initial.com">www.rentokil-initial.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 5.15 |
| 2018 | 4.47 |
| 2017 | 3.88 |
| 2016 | 3.37 |
| 2015 | 2.93 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
