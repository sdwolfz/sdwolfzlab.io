---
title: "ATLANTIC AMERICAN CORP (AAME)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ATLANTIC AMERICAN CORP</td></tr>
    <tr><td>Symbol</td><td>AAME</td></tr>
    <tr><td>Web</td><td><a href="https://www.atlam.com">www.atlam.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.02 |
| 2019 | 0.02 |
| 2018 | 0.02 |
| 2017 | 0.02 |
| 2016 | 0.02 |
| 2015 | 0.02 |
| 2014 | 0.04 |
| 2013 | 0.02 |
| 2012 | 0.07 |
| 2011 | 0.02 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
