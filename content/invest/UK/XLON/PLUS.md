---
title: "Plus500 (PLUS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Plus500</td></tr>
    <tr><td>Symbol</td><td>PLUS</td></tr>
    <tr><td>Web</td><td><a href="https://www.plus500.com">www.plus500.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 65.01 |
| 2018 | 199.77 |
| 2017 | 168.67 |
| 2016 | 107.3 |
| 2015 | 56.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
