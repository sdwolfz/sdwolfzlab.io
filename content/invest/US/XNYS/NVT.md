---
title: "NVENT ELECTRIC PLC (NVT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NVENT ELECTRIC PLC</td></tr>
    <tr><td>Symbol</td><td>NVT</td></tr>
    <tr><td>Web</td><td><a href="https://www.nvent.com">www.nvent.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.35 |
| 2020 | 0.7 |
| 2019 | 0.7 |
| 2018 | 0.35 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
