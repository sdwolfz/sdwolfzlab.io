---
title: " (VOD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>VOD</td></tr>
    <tr><td>Web</td><td><a href="https://www.vodafone.com">www.vodafone.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.076 |
| 2019 | 0.96 |
| 2018 | 1.745 |
| 2017 | 1.769 |
| 2016 | 1.532 |
| 2015 | 1.713 |
| 2014 | 19.177 |
| 2013 | 1.648 |
| 2012 | 1.529 |
| 2011 | 2.082 |
| 2010 | 1.247 |
| 2009 | 1.227 |
| 2008 | 1.376 |
| 2007 | 1.387 |
| 2006 | 1.172 |
| 2005 | 0.777 |
| 2004 | 0.549 |
| 2003 | 0.306 |
| 2002 | 0.262 |
| 2001 | 0.217 |
| 2000 | 0.109 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
