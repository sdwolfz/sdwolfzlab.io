---
title: "BARRICK GOLD CORPORATION (GOLD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BARRICK GOLD CORPORATION</td></tr>
    <tr><td>Symbol</td><td>GOLD</td></tr>
    <tr><td>Web</td><td><a href="https://www.barrick.com">www.barrick.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.31 |
| 2019 | 2.8 |
| 2018 | 2.05 |
| 2017 | 0.98 |
| 2016 | 1.28 |
| 2015 | 1.16 |
| 2014 | 0.48 |
| 2013 | 0.48 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
