---
title: " (WPC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>WPC</td></tr>
    <tr><td>Web</td><td><a href="https://www.wpcarey.com">www.wpcarey.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.048 |
| 2020 | 4.172 |
| 2019 | 4.14 |
| 2018 | 4.09 |
| 2017 | 4.01 |
| 2016 | 3.929 |
| 2015 | 3.826 |
| 2014 | 3.685 |
| 2013 | 2.57 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
