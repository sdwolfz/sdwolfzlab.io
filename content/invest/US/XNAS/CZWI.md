---
title: "CITIZENS COMMUNITY BANCORP INC MD (CZWI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CITIZENS COMMUNITY BANCORP INC MD</td></tr>
    <tr><td>Symbol</td><td>CZWI</td></tr>
    <tr><td>Web</td><td><a href="https://www.ccf.us">www.ccf.us</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.23 |
| 2020 | 0.21 |
| 2019 | 0.2 |
| 2018 | 0.2 |
| 2017 | 0.16 |
| 2016 | 0.12 |
| 2015 | 0.08 |
| 2014 | 0.04 |
| 2013 | 0.02 |
| 2009 | 0.15 |
| 2008 | 0.2 |
| 2007 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
