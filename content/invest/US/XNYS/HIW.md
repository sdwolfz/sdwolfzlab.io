---
title: " (HIW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>HIW</td></tr>
    <tr><td>Web</td><td><a href="https://www.highwoods.com">www.highwoods.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.96 |
| 2020 | 1.92 |
| 2019 | 1.9 |
| 2018 | 1.852 |
| 2017 | 2.2 |
| 2016 | 1.7 |
| 2015 | 1.7 |
| 2014 | 1.7 |
| 2013 | 0.85 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
