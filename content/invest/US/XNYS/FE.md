---
title: "FIRSTENERGY CORP (FE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FIRSTENERGY CORP</td></tr>
    <tr><td>Symbol</td><td>FE</td></tr>
    <tr><td>Web</td><td><a href="https://www.firstenergycorp.com">www.firstenergycorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.78 |
| 2020 | 1.56 |
| 2019 | 1.52 |
| 2018 | 1.44 |
| 2017 | 1.44 |
| 2016 | 1.44 |
| 2015 | 1.44 |
| 2014 | 1.44 |
| 2013 | 1.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
