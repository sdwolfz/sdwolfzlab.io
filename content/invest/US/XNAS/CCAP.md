---
title: "CRESCENT CAPITAL BDC INC (CCAP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CRESCENT CAPITAL BDC INC</td></tr>
    <tr><td>Symbol</td><td>CCAP</td></tr>
    <tr><td>Web</td><td><a href="https://www.crescentbdc.com">www.crescentbdc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.41 |
| 2020 | 1.64 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
