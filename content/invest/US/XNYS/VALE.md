---
title: " (VALE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>VALE</td></tr>
    <tr><td>Web</td><td><a href="https://www.vale.com">www.vale.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.736 |
| 2020 | 0.25 |
| 2019 | 0.222 |
| 2018 | 0.428 |
| 2017 | 0.336 |
| 2016 | 0.041 |
| 2015 | 0.27 |
| 2014 | 0.719 |
| 2013 | 0.131 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
