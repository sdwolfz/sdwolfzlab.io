---
title: "Secs.scotland (STS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Secs.scotland</td></tr>
    <tr><td>Symbol</td><td>STS</td></tr>
    <tr><td>Web</td><td><a href="https://www.securitiestrust.com">www.securitiestrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.41 |
| 2019 | 6.25 |
| 2018 | 6.1 |
| 2017 | 5.95 |
| 2016 | 5.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
