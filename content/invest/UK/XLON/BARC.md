---
title: "Barclays (BARC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Barclays</td></tr>
    <tr><td>Symbol</td><td>BARC</td></tr>
    <tr><td>Web</td><td><a href="https://www.barclays.com">www.barclays.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.0 |
| 2019 | 9.0 |
| 2018 | 6.5 |
| 2017 | 3.0 |
| 2016 | 3.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
