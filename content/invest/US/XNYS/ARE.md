---
title: " (ARE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ARE</td></tr>
    <tr><td>Web</td><td><a href="https://www.are.com">www.are.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.09 |
| 2020 | 4.24 |
| 2019 | 4.0 |
| 2018 | 3.73 |
| 2017 | 3.45 |
| 2016 | 3.23 |
| 2015 | 3.05 |
| 2014 | 2.88 |
| 2013 | 2.01 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
