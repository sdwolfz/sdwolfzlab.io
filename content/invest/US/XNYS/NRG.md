---
title: "NRG ENERGY INC (NRG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NRG ENERGY INC</td></tr>
    <tr><td>Symbol</td><td>NRG</td></tr>
    <tr><td>Web</td><td><a href="https://www.nrg.com">www.nrg.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.65 |
| 2020 | 1.2 |
| 2019 | 0.12 |
| 2018 | 0.12 |
| 2017 | 0.12 |
| 2016 | 0.235 |
| 2015 | 0.58 |
| 2014 | 0.54 |
| 2013 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
