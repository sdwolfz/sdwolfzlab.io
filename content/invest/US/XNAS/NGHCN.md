---
title: "NATIONAL GENERAL HLDGS CO DEP SHS REP 1/40TH PFD SER C 7.50% (NGHCN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NATIONAL GENERAL HLDGS CO DEP SHS REP 1/40TH PFD SER C 7.50%</td></tr>
    <tr><td>Symbol</td><td>NGHCN</td></tr>
    <tr><td>Web</td><td><a href="http://www.nationalgeneral.com">www.nationalgeneral.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.875 |
| 2019 | 1.875 |
| 2018 | 1.875 |
| 2017 | 1.875 |
| 2016 | 0.979 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
