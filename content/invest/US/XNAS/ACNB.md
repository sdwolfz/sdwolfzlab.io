---
title: "ACNB CORP (ACNB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ACNB CORP</td></tr>
    <tr><td>Symbol</td><td>ACNB</td></tr>
    <tr><td>Web</td><td><a href="https://www.acnb.com">www.acnb.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.52 |
| 2020 | 1.0 |
| 2019 | 0.98 |
| 2018 | 0.89 |
| 2017 | 0.8 |
| 2016 | 0.8 |
| 2015 | 0.8 |
| 2014 | 0.77 |
| 2013 | 0.76 |
| 2012 | 0.76 |
| 2011 | 0.76 |
| 2010 | 0.76 |
| 2009 | 0.76 |
| 2008 | 0.76 |
| 2007 | 0.6 |
| 2006 | 0.63 |
| 2005 | 0.91 |
| 2004 | 0.9 |
| 2003 | 0.89 |
| 2002 | 1.08 |
| 2001 | 0.88 |
| 2000 | 0.87 |
| 1999 | 0.85 |
| 1998 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
