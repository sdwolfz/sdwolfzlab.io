---
title: "PROVIDENT FINANCIAL SERVICES INC (PFS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PROVIDENT FINANCIAL SERVICES INC</td></tr>
    <tr><td>Symbol</td><td>PFS</td></tr>
    <tr><td>Web</td><td><a href="https://www.provident.bank">www.provident.bank</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.46 |
| 2020 | 0.92 |
| 2019 | 1.12 |
| 2018 | 0.82 |
| 2017 | 0.93 |
| 2016 | 0.71 |
| 2015 | 0.65 |
| 2014 | 0.6 |
| 2013 | 0.29 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
