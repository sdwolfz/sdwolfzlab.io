---
title: "WVS FINANCIAL CORP (WVFC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>WVS FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>WVFC</td></tr>
    <tr><td>Web</td><td><a href="https://www.wvsbank.com">www.wvsbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.4 |
| 2019 | 0.48 |
| 2018 | 0.36 |
| 2017 | 0.28 |
| 2016 | 0.2 |
| 2015 | 0.2 |
| 2014 | 0.16 |
| 2013 | 0.16 |
| 2012 | 0.16 |
| 2011 | 0.16 |
| 2010 | 0.52 |
| 2009 | 0.64 |
| 2008 | 0.64 |
| 2007 | 0.64 |
| 2006 | 0.64 |
| 2005 | 0.64 |
| 2004 | 0.64 |
| 2003 | 0.64 |
| 2002 | 0.64 |
| 2001 | 0.64 |
| 2000 | 0.64 |
| 1999 | 0.64 |
| 1998 | 2.81 |
| 1997 | 3.2 |
| 1996 | 2.2 |
| 1995 | 0.48 |
| 1994 | 0.14 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
