## Shell Scripts Package Manager

A package manager for projects still stuck in the stone age.

* https://gitlab.com/sdwolfz/sspm

Status: `Feature Complate`

## FooTool

It does almost nothing... except demonstrating how to create a package manager
out of shell scripts, for projects still stuck in the stone age.

* https://gitlab.com/sdwolfz/footool

Status: `Feature Complete`
