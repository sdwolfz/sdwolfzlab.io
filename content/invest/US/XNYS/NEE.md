---
title: "NEXTERA ENERGY INC (NEE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NEXTERA ENERGY INC</td></tr>
    <tr><td>Symbol</td><td>NEE</td></tr>
    <tr><td>Web</td><td><a href="https://www.nexteraenergy.com">www.nexteraenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.385 |
| 2020 | 4.55 |
| 2019 | 5.0 |
| 2018 | 4.44 |
| 2017 | 3.932 |
| 2016 | 3.48 |
| 2015 | 3.08 |
| 2014 | 2.9 |
| 2013 | 1.32 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
