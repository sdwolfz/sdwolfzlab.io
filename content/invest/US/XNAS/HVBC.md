---
title: "HV BANCORP INC (HVBC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HV BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>HVBC</td></tr>
    <tr><td>Web</td><td><a href="https://www.myhvb.com">www.myhvb.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
