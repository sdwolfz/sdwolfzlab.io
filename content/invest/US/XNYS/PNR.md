---
title: "PENTAIR PLC (PNR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PENTAIR PLC</td></tr>
    <tr><td>Symbol</td><td>PNR</td></tr>
    <tr><td>Web</td><td><a href="https://www.pentair.com">www.pentair.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.6 |
| 2020 | 0.76 |
| 2019 | 0.72 |
| 2018 | 23.15 |
| 2017 | 1.38 |
| 2016 | 1.33 |
| 2015 | 1.28 |
| 2014 | 1.1 |
| 2013 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
