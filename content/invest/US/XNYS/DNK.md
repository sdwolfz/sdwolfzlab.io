---
title: "PHOENIX TREE HOLDINGS LIMITED SPON ADS EACH REP 10 ORD SHA CLASS A (DNK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PHOENIX TREE HOLDINGS LIMITED SPON ADS EACH REP 10 ORD SHA CLASS A</td></tr>
    <tr><td>Symbol</td><td>DNK</td></tr>
    <tr><td>Web</td><td><a href="http://www.danke.com">www.danke.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
