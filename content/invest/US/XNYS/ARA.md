---
title: "AMERICAN RENAL ASSOCS HLDGS INC (ARA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AMERICAN RENAL ASSOCS HLDGS INC</td></tr>
    <tr><td>Symbol</td><td>ARA</td></tr>
    <tr><td>Web</td><td><a href="http://www.americanrenal.com">www.americanrenal.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
