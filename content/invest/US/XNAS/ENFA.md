---
title: "890 5TH AVENUE PARTNERS INC (ENFA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>890 5TH AVENUE PARTNERS INC</td></tr>
    <tr><td>Symbol</td><td>ENFA</td></tr>
    <tr><td>Web</td><td><a href="https://www.890fifthavenue.com">www.890fifthavenue.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
