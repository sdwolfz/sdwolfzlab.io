---
title: "Ig Group Holdings (IGG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Ig Group Holdings</td></tr>
    <tr><td>Symbol</td><td>IGG</td></tr>
    <tr><td>Web</td><td><a href="https://www.iggroup.com">www.iggroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 43.2 |
| 2019 | 43.2 |
| 2018 | 43.2 |
| 2017 | 32.3 |
| 2016 | 31.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
