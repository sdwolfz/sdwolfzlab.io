---
title: "WASHINGTON TRUST BANCORP INC (WASH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>WASHINGTON TRUST BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>WASH</td></tr>
    <tr><td>Web</td><td><a href="https://www.washtrust.com">www.washtrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.52 |
| 2020 | 2.05 |
| 2019 | 2.0 |
| 2018 | 1.76 |
| 2017 | 1.54 |
| 2016 | 1.46 |
| 2015 | 1.36 |
| 2014 | 1.22 |
| 2013 | 1.03 |
| 2012 | 0.94 |
| 2011 | 0.88 |
| 2010 | 0.84 |
| 2009 | 0.84 |
| 2008 | 0.83 |
| 2007 | 1.0 |
| 2006 | 0.76 |
| 2005 | 0.72 |
| 2004 | 0.68 |
| 2003 | 0.76 |
| 2002 | 0.55 |
| 2001 | 0.51 |
| 2000 | 0.36 |
| 1999 | 0.44 |
| 1998 | 0.5 |
| 1997 | 0.72 |
| 1996 | 0.97 |
| 1995 | 0.92 |
| 1994 | 0.9 |
| 1993 | 0.88 |
| 1992 | 0.8 |
| 1991 | 0.8 |
| 1990 | 1.32 |
| 1989 | 1.2 |
| 1988 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
