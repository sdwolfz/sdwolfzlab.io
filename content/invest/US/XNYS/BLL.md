---
title: "BALL CORP (BLL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BALL CORP</td></tr>
    <tr><td>Symbol</td><td>BLL</td></tr>
    <tr><td>Web</td><td><a href="https://www.ball.com">www.ball.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 0.6 |
| 2019 | 0.55 |
| 2018 | 0.4 |
| 2017 | 0.43 |
| 2016 | 0.52 |
| 2015 | 0.52 |
| 2014 | 0.52 |
| 2013 | 0.26 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
