---
title: "OLD SECOND BANCORP INC (OSBC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>OLD SECOND BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>OSBC</td></tr>
    <tr><td>Web</td><td><a href="https://www.oldsecond.com">www.oldsecond.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.06 |
| 2020 | 0.04 |
| 2019 | 0.04 |
| 2018 | 0.04 |
| 2017 | 0.04 |
| 2016 | 0.03 |
| 2010 | 0.02 |
| 2009 | 0.1 |
| 2008 | 0.63 |
| 2007 | 0.59 |
| 2006 | 0.55 |
| 2005 | 0.51 |
| 2004 | 0.68 |
| 2003 | 0.8 |
| 2002 | 0.8 |
| 2001 | 0.8 |
| 2000 | 0.6 |
| 1999 | 0.7 |
| 1998 | 0.9 |
| 1997 | 0.9 |
| 1996 | 0.9 |
| 1995 | 0.95 |
| 1994 | 0.9 |
| 1993 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
