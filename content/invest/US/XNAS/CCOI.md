---
title: "COGENT COMMUNICATIONS HOLDINGS INC (CCOI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COGENT COMMUNICATIONS HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>CCOI</td></tr>
    <tr><td>Web</td><td><a href="https://www.cogentco.com">www.cogentco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.535 |
| 2020 | 2.775 |
| 2019 | 2.44 |
| 2018 | 2.12 |
| 2017 | 1.8 |
| 2016 | 1.51 |
| 2015 | 1.46 |
| 2014 | 1.17 |
| 2013 | 0.76 |
| 2012 | 0.21 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
