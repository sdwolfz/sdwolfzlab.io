---
title: "PILGRIMS PRIDE CORP (PPC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PILGRIMS PRIDE CORP</td></tr>
    <tr><td>Symbol</td><td>PPC</td></tr>
    <tr><td>Web</td><td><a href="https://www.pilgrims.com">www.pilgrims.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 2.75 |
| 2015 | 5.77 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
