---
title: "ALTIMMUNE INC (ALT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ALTIMMUNE INC</td></tr>
    <tr><td>Symbol</td><td>ALT</td></tr>
    <tr><td>Web</td><td><a href="https://www.altimmune.com">www.altimmune.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 2.91 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
