---
title: "STAMPS.COM INC (STMP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>STAMPS.COM INC</td></tr>
    <tr><td>Symbol</td><td>STMP</td></tr>
    <tr><td>Web</td><td><a href="https://www.stamps.com">www.stamps.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2010 | 2.0 |
| 2004 | 1.75 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
