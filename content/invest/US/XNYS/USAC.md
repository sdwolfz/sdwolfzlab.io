---
title: " (USAC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>USAC</td></tr>
    <tr><td>Web</td><td><a href="https://www.usacompression.com">www.usacompression.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.05 |
| 2020 | 2.1 |
| 2019 | 2.1 |
| 2018 | 2.1 |
| 2017 | 2.1 |
| 2016 | 2.1 |
| 2015 | 2.075 |
| 2014 | 1.975 |
| 2013 | 0.9 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
