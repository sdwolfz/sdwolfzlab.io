---
title: "KADANT INC (KAI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>KADANT INC</td></tr>
    <tr><td>Symbol</td><td>KAI</td></tr>
    <tr><td>Web</td><td><a href="https://www.kadant.com">www.kadant.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.49 |
| 2020 | 0.95 |
| 2019 | 0.91 |
| 2018 | 0.87 |
| 2017 | 0.82 |
| 2016 | 0.74 |
| 2015 | 0.66 |
| 2014 | 0.575 |
| 2013 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
