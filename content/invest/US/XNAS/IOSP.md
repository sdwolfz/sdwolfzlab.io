---
title: "INNOSPEC INC (IOSP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>INNOSPEC INC</td></tr>
    <tr><td>Symbol</td><td>IOSP</td></tr>
    <tr><td>Web</td><td><a href="https://www.innospecinc.com">www.innospecinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.57 |
| 2020 | 1.04 |
| 2019 | 1.02 |
| 2018 | 0.89 |
| 2017 | 0.77 |
| 2016 | 0.67 |
| 2015 | 0.61 |
| 2014 | 0.55 |
| 2013 | 0.5 |
| 2012 | 2.0 |
| 2009 | 0.05 |
| 2008 | 0.1 |
| 2007 | 0.135 |
| 2006 | 0.08 |
| 2005 | 0.14 |
| 2004 | 0.12 |
| 2003 | 0.05 |
| 2002 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
