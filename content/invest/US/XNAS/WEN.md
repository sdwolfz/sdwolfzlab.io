---
title: "WENDYS COMPANY (THE) (WEN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>WENDYS COMPANY (THE)</td></tr>
    <tr><td>Symbol</td><td>WEN</td></tr>
    <tr><td>Web</td><td><a href="https://www.wendys.com">www.wendys.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.09 |
| 2020 | 0.29 |
| 2019 | 0.42 |
| 2018 | 0.34 |
| 2017 | 0.28 |
| 2016 | 0.245 |
| 2015 | 0.225 |
| 2014 | 0.205 |
| 2013 | 0.18 |
| 2012 | 0.1 |
| 2011 | 0.08 |
| 2010 | 0.065 |
| 2009 | 0.06 |
| 2008 | 0.39 |
| 2007 | 0.46 |
| 2006 | 0.595 |
| 2005 | 0.44 |
| 2004 | 0.48 |
| 2003 | 0.24 |
| 2002 | 0.24 |
| 2001 | 0.24 |
| 2000 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
