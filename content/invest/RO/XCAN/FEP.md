---
title: "FEPER (FEP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>FEPER</td></tr>
    <tr><td>Symbol</td><td>FEP</td></tr>
    <tr><td>Web</td><td><a href="https://www.feper.ro">www.feper.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.009 |
| 2018 | 0.0056 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
