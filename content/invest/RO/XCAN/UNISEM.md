---
title: "UNISEM SA BUCURESTI (UNISEM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>UNISEM SA BUCURESTI</td></tr>
    <tr><td>Symbol</td><td>UNISEM</td></tr>
    <tr><td>Web</td><td><a href="https://www.unisemromania.ro">www.unisemromania.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.1053 |
| 2016 | 0.0067 |
| 2004 | 0.0005 |
| 2002 | 0.0007 |
| 2001 | 0.0022 |
| 2000 | 0.0014 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
