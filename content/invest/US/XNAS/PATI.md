---
title: "PATRIOT TRANSPORTATION HOLDING INC (PATI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PATRIOT TRANSPORTATION HOLDING INC</td></tr>
    <tr><td>Symbol</td><td>PATI</td></tr>
    <tr><td>Web</td><td><a href="https://www.patriottrans.com">www.patriottrans.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
