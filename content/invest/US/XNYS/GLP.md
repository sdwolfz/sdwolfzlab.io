---
title: " (GLP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>GLP</td></tr>
    <tr><td>Web</td><td><a href="https://www.globalp.com">www.globalp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.734 |
| 2020 | 2.272 |
| 2019 | 2.045 |
| 2018 | 1.876 |
| 2017 | 1.852 |
| 2016 | 1.852 |
| 2015 | 2.734 |
| 2014 | 2.527 |
| 2013 | 1.187 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
