---
title: "Baillie Gifford (BGUK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Baillie Gifford</td></tr>
    <tr><td>Symbol</td><td>BGUK</td></tr>
    <tr><td>Web</td><td><a href="https://www.bailliegifford.com">www.bailliegifford.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 3.1 |
| 2019 | 4.45 |
| 2018 | 6.0 |
| 2017 | 5.4 |
| 2016 | 5.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
