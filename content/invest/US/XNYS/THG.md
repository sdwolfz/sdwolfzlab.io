---
title: "HANOVER INSURANCE GROUP (THG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HANOVER INSURANCE GROUP</td></tr>
    <tr><td>Symbol</td><td>THG</td></tr>
    <tr><td>Web</td><td><a href="https://www.hanover.com">www.hanover.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.7 |
| 2020 | 2.65 |
| 2019 | 9.7 |
| 2018 | 2.22 |
| 2017 | 2.04 |
| 2016 | 1.88 |
| 2015 | 1.69 |
| 2014 | 1.52 |
| 2013 | 1.03 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
