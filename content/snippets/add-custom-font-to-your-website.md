---
title: "Add a custom font to your website"
date: 2020-12-11T13:20:15Z
type: posts
slug: add-custom-font-to-your-website
tags: ["design", "font", "css", "style"]
---

1. Download and extract your font (I use `Roboto`):

```sh
#!/usr/bin/env sh

set -eux

ROBOTO_SHA=017a959dbef25cf37cb3cd407b174799b09b8b28ad72494be389ee3b563e3751
ROBOTO_NAME=Roboto
ROBOTO_ZIP="$ROBOTO_NAME".zip
ROBOTO_URL=https://fonts.google.com/download?family=Roboto
ROBOTO_TMP=./tmp

rm -rf "$ROBOTO_TMP"
mkdir -p "$ROBOTO_TMP"
wget "$ROBOTO_URL" -O "$ROBOTO_TMP"/"$ROBOTO_ZIP"
sha256sum "$ROBOTO_TMP"/"$ROBOTO_ZIP"
echo "$ROBOTO_SHA  $ROBOTO_TMP/$ROBOTO_ZIP" | sha256sum -c -
unzip "$ROBOTO_TMP"/"$ROBOTO_ZIP" -d "$ROBOTO_TMP"

cp -r "$ROBOTO_TMP"/Roboto-*.ttf webfonts/
```

2. Declare the needed font faces in your CSS:

```css
@font-face {
  font-family: "Roboto";
  src: url("webfonts/Roboto-Regular.ttf");
}
@font-face {
  font-family: "Roboto";
  font-weight: bold;
  src: url("webfonts/Roboto-Bold.ttf");
}
```

3. Start using them:

```css
body {
  font-family: "Roboto";
}
```
