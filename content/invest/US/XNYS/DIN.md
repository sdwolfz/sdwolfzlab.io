---
title: "DINEEQUITY INC (DIN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DINEEQUITY INC</td></tr>
    <tr><td>Symbol</td><td>DIN</td></tr>
    <tr><td>Web</td><td><a href="https://www.dinebrands.com">www.dinebrands.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.76 |
| 2019 | 2.76 |
| 2018 | 2.52 |
| 2017 | 3.88 |
| 2016 | 3.73 |
| 2015 | 3.545 |
| 2014 | 3.125 |
| 2013 | 1.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
