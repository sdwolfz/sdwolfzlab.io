---
title: "MURPHY OIL CORP (MUR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MURPHY OIL CORP</td></tr>
    <tr><td>Symbol</td><td>MUR</td></tr>
    <tr><td>Web</td><td><a href="https://www.murphyoilcorp.com">www.murphyoilcorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 0.625 |
| 2019 | 1.0 |
| 2018 | 1.0 |
| 2017 | 1.0 |
| 2016 | 1.2 |
| 2015 | 1.4 |
| 2014 | 1.326 |
| 2013 | 0.626 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
