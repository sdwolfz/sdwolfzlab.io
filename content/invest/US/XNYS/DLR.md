---
title: "DIGITAL REALTY TRUST INC (DLR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DIGITAL REALTY TRUST INC</td></tr>
    <tr><td>Symbol</td><td>DLR</td></tr>
    <tr><td>Web</td><td><a href="http://www.digitalrealty.com">www.digitalrealty.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 4.48 |
| 2019 | 4.32 |
| 2018 | 4.04 |
| 2017 | 3.72 |
| 2016 | 3.52 |
| 2015 | 3.4 |
| 2014 | 3.32 |
| 2013 | 1.56 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
