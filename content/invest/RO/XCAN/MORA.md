---
title: "MORARIT PANIFICATIE SA ROMAN (MORA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>MORARIT PANIFICATIE SA ROMAN</td></tr>
    <tr><td>Symbol</td><td>MORA</td></tr>
    <tr><td>Web</td><td><a href="https://www.panificatieroman.ro">www.panificatieroman.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2010 | 0.0721 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
