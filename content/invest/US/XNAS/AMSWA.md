---
title: "AMERICAN SOFTWARE INC (AMSWA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AMERICAN SOFTWARE INC</td></tr>
    <tr><td>Symbol</td><td>AMSWA</td></tr>
    <tr><td>Web</td><td><a href="https://www.amsoftware.com">www.amsoftware.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.22 |
| 2020 | 0.44 |
| 2019 | 0.44 |
| 2018 | 0.44 |
| 2017 | 0.44 |
| 2016 | 0.42 |
| 2015 | 0.4 |
| 2014 | 0.4 |
| 2013 | 0.1 |
| 2012 | 0.66 |
| 2011 | 0.27 |
| 2010 | 0.45 |
| 2009 | 0.36 |
| 2008 | 0.36 |
| 2007 | 0.33 |
| 2006 | 0.29 |
| 2005 | 0.28 |
| 2004 | 0.26 |
| 2003 | 0.12 |
| 1994 | 0.24 |
| 1993 | 0.32 |
| 1992 | 0.3 |
| 1991 | 0.26 |
| 1990 | 0.28 |
| 1989 | 0.32 |
| 1988 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
