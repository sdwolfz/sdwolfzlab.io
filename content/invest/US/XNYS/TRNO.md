---
title: " (TRNO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>TRNO</td></tr>
    <tr><td>Web</td><td><a href="https://www.terreno.com">www.terreno.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.58 |
| 2020 | 1.12 |
| 2019 | 1.02 |
| 2018 | 0.92 |
| 2017 | 0.84 |
| 2016 | 0.76 |
| 2015 | 0.66 |
| 2014 | 0.57 |
| 2013 | 0.26 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
