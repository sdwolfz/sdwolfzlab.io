---
title: "AVNET INC (AVT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AVNET INC</td></tr>
    <tr><td>Symbol</td><td>AVT</td></tr>
    <tr><td>Web</td><td><a href="https://www.avnet.com">www.avnet.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.21 |
| 2020 | 0.84 |
| 2019 | 0.82 |
| 2018 | 0.78 |
| 2017 | 0.72 |
| 2016 | 0.68 |
| 2015 | 0.66 |
| 2014 | 0.62 |
| 2013 | 0.3 |
| 2001 | 0.225 |
| 2000 | 0.225 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
