---
title: "How to Use: esbuild"
date: 2022-04-28T21:38:03Z
type: posts
slug: how-to-use-esbuild
series: ["How to Use"]
tags: ["javascript", "node", "esbuild"]
---

## Install

```sh
npm init
npm install --save-dev esbuild
```

## Setup

Add this to your `esbuild.js` file:

```js
const esbuild = require('esbuild')

const targets = ['chrome58', 'firefox57', 'safari11']

esbuild.build({
  entryPoints: ['src/main.js'],
  outfile:     'dist/bundle.min.js',

  bundle:    true,
  minify:    true,
  sourcemap: true,

  target:   targets,
  platform: 'browser',
  format:   'iife',

  globalName: 'UsefulPackage'
}).catch(() => process.exit(1))
```

## Code

Write your code in js files like `src/some-class.js`:

```js
class SomeClass {
  constructor() {
    console.log('SomeClass instantiated!')
  }
}

export default SomeClass
```

Import it into your `src/main.js` file:

```js
import SomeClass from './some-class'

export default SomeClass
```

## Compile

```sh
node esbuild.js
```

## Browser

Unfortunately you can't have `SomeClass` at the top level. It has to be within
the `UsefulPackage` global name defined in `esbuild.js`.

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Trying out UsefulPackage containing SomeClass</title>
  </head>
  <body>
    <script type="text/javascript" src="../dist/bundle.min.js"></script>
    <script>
      document.addEventListener('DOMContentLoaded', (_event) => {
        var SomeClass = UsefulPackage.default

        new SomeClass()
      })
    </script>
  </body>
</html>
```

## Node

Nothing special here, just like in `src/main.js`:

```sh
import SomeClass from './some-class'
```
