---
title: "UNION PACIFIC CORP (UNP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>UNION PACIFIC CORP</td></tr>
    <tr><td>Symbol</td><td>UNP</td></tr>
    <tr><td>Web</td><td><a href="https://www.up.com">www.up.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.97 |
| 2020 | 3.88 |
| 2019 | 3.7 |
| 2018 | 3.06 |
| 2017 | 2.48 |
| 2016 | 2.255 |
| 2015 | 2.2 |
| 2014 | 2.365 |
| 2013 | 1.58 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
