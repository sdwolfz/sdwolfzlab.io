---
title: "GREAT SOUTHERN BANCORPORATION INC (GSBC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GREAT SOUTHERN BANCORPORATION INC</td></tr>
    <tr><td>Symbol</td><td>GSBC</td></tr>
    <tr><td>Web</td><td><a href="https://www.greatsouthernbank.com">www.greatsouthernbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.34 |
| 2020 | 2.36 |
| 2019 | 2.07 |
| 2018 | 1.2 |
| 2017 | 0.94 |
| 2016 | 0.88 |
| 2015 | 0.86 |
| 2014 | 0.8 |
| 2013 | 0.72 |
| 2012 | 0.72 |
| 2011 | 0.72 |
| 2010 | 0.72 |
| 2009 | 0.72 |
| 2008 | 0.72 |
| 2007 | 0.68 |
| 2006 | 0.6 |
| 2005 | 0.52 |
| 2004 | 0.54 |
| 2003 | 0.71 |
| 2002 | 0.695 |
| 2001 | 0.5 |
| 2000 | 0.5 |
| 1999 | 0.5 |
| 1998 | 0.455 |
| 1997 | 0.41 |
| 1996 | 0.725 |
| 1995 | 0.65 |
| 1994 | 1.152 |
| 1993 | 0.36 |
| 1992 | 0.36 |
| 1991 | 0.36 |
| 1990 | 0.27 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
