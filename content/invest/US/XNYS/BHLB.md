---
title: "BERKSHIRE HILLS BANCORP INC (BHLB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BERKSHIRE HILLS BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>BHLB</td></tr>
    <tr><td>Web</td><td><a href="https://www.berkshirebank.com">www.berkshirebank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 0.72 |
| 2019 | 0.92 |
| 2018 | 0.88 |
| 2017 | 0.84 |
| 2016 | 0.8 |
| 2015 | 0.76 |
| 2014 | 0.72 |
| 2013 | 0.36 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
