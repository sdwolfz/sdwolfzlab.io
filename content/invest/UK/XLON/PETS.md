---
title: "Pets At Home (PETS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Pets At Home</td></tr>
    <tr><td>Symbol</td><td>PETS</td></tr>
    <tr><td>Web</td><td><a href="https://investors.petsathome.com">investors.petsathome.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 7.5 |
| 2019 | 7.5 |
| 2018 | 7.5 |
| 2017 | 7.5 |
| 2016 | 7.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
