---
title: "Pennon (PNN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Pennon</td></tr>
    <tr><td>Symbol</td><td>PNN</td></tr>
    <tr><td>Web</td><td><a href="https://www.pennon-group.co.uk">www.pennon-group.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 41.06 |
| 2018 | 38.59 |
| 2017 | 35.96 |
| 2016 | 33.58 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
