---
title: "Phoenix Spree D (PSDL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Phoenix Spree D</td></tr>
    <tr><td>Symbol</td><td>PSDL</td></tr>
    <tr><td>Web</td><td><a href="https://www.phoenixspree.com">www.phoenixspree.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 7.5 |
| 2018 | 7.5 |
| 2017 | 6.9 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
