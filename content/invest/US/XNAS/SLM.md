---
title: "SLM CORP (SLM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SLM CORP</td></tr>
    <tr><td>Symbol</td><td>SLM</td></tr>
    <tr><td>Web</td><td><a href="https://www.salliemae.com">www.salliemae.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.06 |
| 2020 | 0.12 |
| 2019 | 0.12 |
| 2014 | 0.15 |
| 2013 | 0.6 |
| 2012 | 0.5 |
| 2011 | 0.3 |
| 2007 | 0.25 |
| 2006 | 0.97 |
| 2005 | 0.85 |
| 2004 | 0.74 |
| 2003 | 1.1 |
| 2002 | 0.85 |
| 2001 | 0.925 |
| 2000 | 0.335 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
