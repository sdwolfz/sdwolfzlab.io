---
title: "Pacific Assets (PAC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Pacific Assets</td></tr>
    <tr><td>Symbol</td><td>PAC</td></tr>
    <tr><td>Web</td><td><a href="https://www.pacific-assets.co.uk">www.pacific-assets.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
