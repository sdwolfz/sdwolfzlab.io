---
title: "ROCKWELL AUTOMATION INC (ROK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ROCKWELL AUTOMATION INC</td></tr>
    <tr><td>Symbol</td><td>ROK</td></tr>
    <tr><td>Web</td><td><a href="https://www.rockwellautomation.com">www.rockwellautomation.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.14 |
| 2020 | 4.13 |
| 2019 | 3.93 |
| 2018 | 3.645 |
| 2017 | 3.115 |
| 2016 | 2.935 |
| 2015 | 2.675 |
| 2014 | 2.39 |
| 2013 | 1.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
