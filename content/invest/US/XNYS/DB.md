---
title: "DEUTSCHE BANK AG (DB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DEUTSCHE BANK AG</td></tr>
    <tr><td>Symbol</td><td>DB</td></tr>
    <tr><td>Web</td><td><a href="https://www.db.com">www.db.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 0.211 |
| 2015 | 0.807 |
| 2014 | 1.032 |
| 2013 | 0.965 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
