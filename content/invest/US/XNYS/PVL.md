---
title: " (PVL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>PVL</td></tr>
    <tr><td>Web</td><td><a href="https://www.permianvilleroyaltytrust.com">www.permianvilleroyaltytrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.134 |
| 2019 | 0.307 |
| 2018 | 0.128 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
