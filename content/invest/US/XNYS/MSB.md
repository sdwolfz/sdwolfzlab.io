---
title: " (MSB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>MSB</td></tr>
    <tr><td>Web</td><td><a href="https://www.mesabi-trust.com">www.mesabi-trust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.35 |
| 2020 | 1.67 |
| 2019 | 3.36 |
| 2018 | 2.79 |
| 2017 | 1.49 |
| 2016 | 0.55 |
| 2015 | 0.68 |
| 2014 | 1.77 |
| 2013 | 0.97 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
