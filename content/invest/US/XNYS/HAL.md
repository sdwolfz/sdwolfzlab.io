---
title: "HALLIBURTON CO (HAL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HALLIBURTON CO</td></tr>
    <tr><td>Symbol</td><td>HAL</td></tr>
    <tr><td>Web</td><td><a href="https://www.halliburton.com">www.halliburton.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.045 |
| 2020 | 0.315 |
| 2019 | 0.72 |
| 2018 | 0.72 |
| 2017 | 0.72 |
| 2016 | 0.72 |
| 2015 | 0.72 |
| 2014 | 0.63 |
| 2013 | 0.275 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
