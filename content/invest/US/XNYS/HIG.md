---
title: "HARTFORD FINANCIAL SERVICES GRP INC (HIG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HARTFORD FINANCIAL SERVICES GRP INC</td></tr>
    <tr><td>Symbol</td><td>HIG</td></tr>
    <tr><td>Web</td><td><a href="https://www.thehartford.com">www.thehartford.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.35 |
| 2020 | 1.3 |
| 2019 | 1.2 |
| 2018 | 1.1 |
| 2017 | 0.94 |
| 2016 | 0.86 |
| 2015 | 0.78 |
| 2014 | 0.66 |
| 2013 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
