---
title: "Esken Ltd (ESKN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Esken Ltd</td></tr>
    <tr><td>Symbol</td><td>ESKN</td></tr>
    <tr><td>Web</td><td><a href="https://www.stobartgroup.co.uk">www.stobartgroup.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 3.0 |
| 2019 | 9.0 |
| 2018 | 18.0 |
| 2017 | 13.5 |
| 2016 | 6.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
