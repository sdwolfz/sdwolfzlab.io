---
title: "LUTHER BURBANK CORPORATION (LBC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LUTHER BURBANK CORPORATION</td></tr>
    <tr><td>Symbol</td><td>LBC</td></tr>
    <tr><td>Web</td><td><a href="https://www.lutherburbanksavings.com">www.lutherburbanksavings.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.115 |
| 2020 | 0.23 |
| 2019 | 0.23 |
| 2018 | 0.188 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
