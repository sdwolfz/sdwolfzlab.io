---
title: " (PSB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>PSB</td></tr>
    <tr><td>Web</td><td><a href="https://www.psbusinessparks.com">www.psbusinessparks.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.1 |
| 2020 | 4.2 |
| 2019 | 4.2 |
| 2018 | 3.8 |
| 2017 | 3.4 |
| 2016 | 3.0 |
| 2015 | 2.2 |
| 2014 | 2.0 |
| 2013 | 0.88 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
