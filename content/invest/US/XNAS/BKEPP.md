---
title: " (BKEPP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BKEPP</td></tr>
    <tr><td>Web</td><td><a href="https://www.bkep.com">www.bkep.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.358 |
| 2020 | 0.715 |
| 2019 | 0.715 |
| 2018 | 0.715 |
| 2017 | 0.715 |
| 2016 | 0.715 |
| 2015 | 0.715 |
| 2014 | 0.715 |
| 2013 | 0.715 |
| 2012 | 0.706 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
