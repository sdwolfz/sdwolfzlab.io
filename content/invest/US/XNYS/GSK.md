---
title: " (GSK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>GSK</td></tr>
    <tr><td>Web</td><td><a href="https://www.gsk.com">www.gsk.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.149 |
| 2020 | 2.019 |
| 2019 | 2.01 |
| 2018 | 2.142 |
| 2017 | 2.056 |
| 2016 | 2.647 |
| 2015 | 2.392 |
| 2014 | 2.651 |
| 2013 | 1.169 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
