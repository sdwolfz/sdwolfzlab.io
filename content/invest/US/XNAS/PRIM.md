---
title: "PRIMORIS SERVICES CORPORATION (PRIM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PRIMORIS SERVICES CORPORATION</td></tr>
    <tr><td>Symbol</td><td>PRIM</td></tr>
    <tr><td>Web</td><td><a href="https://www.prim.com">www.prim.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 0.24 |
| 2019 | 0.24 |
| 2018 | 0.24 |
| 2017 | 0.225 |
| 2016 | 0.22 |
| 2015 | 0.205 |
| 2014 | 0.15 |
| 2013 | 0.135 |
| 2012 | 0.12 |
| 2011 | 0.11 |
| 2010 | 0.1 |
| 2009 | 0.1 |
| 2008 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
