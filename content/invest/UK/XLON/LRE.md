---
title: "Lancashire Holdings (LRE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Lancashire Holdings</td></tr>
    <tr><td>Symbol</td><td>LRE</td></tr>
    <tr><td>Web</td><td><a href="https://www.lancashiregroup.com">www.lancashiregroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 15.0 |
| 2019 | 15.0 |
| 2018 | 15.0 |
| 2017 | 15.0 |
| 2016 | 90.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
