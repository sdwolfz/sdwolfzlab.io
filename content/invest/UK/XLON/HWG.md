---
title: "Harworth Gp (HWG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Harworth Gp</td></tr>
    <tr><td>Symbol</td><td>HWG</td></tr>
    <tr><td>Web</td><td><a href="https://www.HarworthGroup.com">www.HarworthGroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.3 |
| 2018 | 0.91 |
| 2017 | 0.83 |
| 2016 | 0.23 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
