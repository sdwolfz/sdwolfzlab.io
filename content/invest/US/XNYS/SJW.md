---
title: "SJW GROUP (SJW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SJW GROUP</td></tr>
    <tr><td>Symbol</td><td>SJW</td></tr>
    <tr><td>Web</td><td><a href="https://www.sjwgroup.com">www.sjwgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.68 |
| 2020 | 1.28 |
| 2019 | 1.2 |
| 2018 | 1.12 |
| 2017 | 0.824 |
| 2016 | 0.812 |
| 2015 | 0.78 |
| 2014 | 0.752 |
| 2013 | 0.366 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
