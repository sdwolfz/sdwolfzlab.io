---
title: "CHASE CORP(MASS) (CCF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CHASE CORP(MASS)</td></tr>
    <tr><td>Symbol</td><td>CCF</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.8 |
| 2019 | 0.8 |
| 2018 | 0.8 |
| 2017 | 0.8 |
| 2016 | 0.7 |
| 2015 | 0.65 |
| 2014 | 0.6 |
| 2013 | 0.45 |
| 2012 | 0.4 |
| 2011 | 0.35 |
| 2010 | 0.35 |
| 2009 | 0.2 |
| 2008 | 0.35 |
| 2007 | 0.25 |
| 2006 | 0.4 |
| 2005 | 0.35 |
| 2004 | 0.35 |
| 2003 | 0.31 |
| 2002 | 0.27 |
| 2001 | 0.36 |
| 2000 | 0.36 |
| 1999 | 0.32 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
