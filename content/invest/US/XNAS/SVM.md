---
title: "SILVERCORP METALS INC (SVM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SILVERCORP METALS INC</td></tr>
    <tr><td>Symbol</td><td>SVM</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.025 |
| 2019 | 0.025 |
| 2018 | 0.025 |
| 2017 | 0.02 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
