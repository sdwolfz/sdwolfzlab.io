---
title: "MAMMOTH ENERGY SERVICES INC (TUSK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MAMMOTH ENERGY SERVICES INC</td></tr>
    <tr><td>Symbol</td><td>TUSK</td></tr>
    <tr><td>Web</td><td><a href="https://www.mammothenergy.com">www.mammothenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.25 |
| 2018 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
