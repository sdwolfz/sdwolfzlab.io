---
title: "Mercantile Investment Trust PLC (MRC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Mercantile Investment Trust PLC</td></tr>
    <tr><td>Symbol</td><td>MRC</td></tr>
    <tr><td>Web</td><td><a href="https://www.mercantileit.co.uk">www.mercantileit.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.6 |
| 2019 | 6.3 |
| 2018 | 5.3 |
| 2017 | 4.6 |
| 2016 | 4.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
