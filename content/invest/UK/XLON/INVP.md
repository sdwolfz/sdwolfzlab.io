---
title: "Investec (INVP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Investec</td></tr>
    <tr><td>Symbol</td><td>INVP</td></tr>
    <tr><td>Web</td><td><a href="https://www.investec.com">www.investec.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 21.94 |
| 2018 | 24.0 |
| 2017 | 23.0 |
| 2016 | 21.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
