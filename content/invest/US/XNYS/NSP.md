---
title: "INSPERITY INC (NSP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>INSPERITY INC</td></tr>
    <tr><td>Symbol</td><td>NSP</td></tr>
    <tr><td>Web</td><td><a href="https://www.insperity.com">www.insperity.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.4 |
| 2020 | 1.6 |
| 2019 | 1.2 |
| 2018 | 0.8 |
| 2017 | 2.0 |
| 2016 | 0.97 |
| 2015 | 0.85 |
| 2014 | 0.74 |
| 2013 | 0.51 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
