---
title: " (CUZ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CUZ</td></tr>
    <tr><td>Web</td><td><a href="https://www.cousins.com">www.cousins.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.31 |
| 2020 | 1.49 |
| 2019 | 0.935 |
| 2018 | 0.195 |
| 2017 | 0.3 |
| 2016 | 0.24 |
| 2015 | 0.32 |
| 2014 | 0.3 |
| 2013 | 0.09 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
