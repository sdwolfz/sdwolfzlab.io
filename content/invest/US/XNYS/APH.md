---
title: "AMPHENOL CORP (APH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AMPHENOL CORP</td></tr>
    <tr><td>Symbol</td><td>APH</td></tr>
    <tr><td>Web</td><td><a href="https://www.amphenol.com">www.amphenol.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.29 |
| 2020 | 1.04 |
| 2019 | 0.96 |
| 2018 | 0.88 |
| 2017 | 0.7 |
| 2016 | 0.58 |
| 2015 | 0.53 |
| 2014 | 0.65 |
| 2013 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
