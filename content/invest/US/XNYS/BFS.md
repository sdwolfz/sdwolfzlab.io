---
title: " (BFS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BFS</td></tr>
    <tr><td>Web</td><td><a href="https://www.saulcenters.com">www.saulcenters.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.06 |
| 2020 | 2.12 |
| 2019 | 2.12 |
| 2018 | 2.08 |
| 2017 | 2.04 |
| 2016 | 1.84 |
| 2015 | 1.69 |
| 2014 | 1.56 |
| 2013 | 0.72 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
