---
title: "GRAINGER W W INC (GWW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GRAINGER W W INC</td></tr>
    <tr><td>Symbol</td><td>GWW</td></tr>
    <tr><td>Web</td><td><a href="https://www.grainger.com">www.grainger.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 3.15 |
| 2020 | 5.94 |
| 2019 | 5.68 |
| 2018 | 5.36 |
| 2017 | 5.06 |
| 2016 | 4.83 |
| 2015 | 4.59 |
| 2014 | 4.17 |
| 2013 | 1.86 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
