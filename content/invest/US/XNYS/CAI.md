---
title: "CAI INTERNATIONAL INC (CAI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CAI INTERNATIONAL INC</td></tr>
    <tr><td>Symbol</td><td>CAI</td></tr>
    <tr><td>Web</td><td><a href="https://www.capps.com">www.capps.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.6 |
| 2020 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
