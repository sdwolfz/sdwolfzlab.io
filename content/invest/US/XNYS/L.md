---
title: "LOEWS CORP (L)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>LOEWS CORP</td></tr>
    <tr><td>Symbol</td><td>L</td></tr>
    <tr><td>Web</td><td><a href="https://www.loews.com">www.loews.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.063 |
| 2020 | 0.252 |
| 2019 | 0.252 |
| 2018 | 0.252 |
| 2017 | 0.252 |
| 2016 | 0.252 |
| 2015 | 0.252 |
| 2014 | 0.252 |
| 2013 | 0.126 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
