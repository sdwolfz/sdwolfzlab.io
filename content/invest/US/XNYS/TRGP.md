---
title: "TARGA RESOURCES CORPORATION (TRGP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TARGA RESOURCES CORPORATION</td></tr>
    <tr><td>Symbol</td><td>TRGP</td></tr>
    <tr><td>Web</td><td><a href="https://www.targaresources.com">www.targaresources.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 1.21 |
| 2019 | 3.64 |
| 2018 | 3.64 |
| 2017 | 3.64 |
| 2016 | 3.64 |
| 2015 | 3.39 |
| 2014 | 2.678 |
| 2013 | 1.103 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
