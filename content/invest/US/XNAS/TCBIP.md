---
title: " (TCBIP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>TCBIP</td></tr>
    <tr><td>Web</td><td><a href="https://www.texascapitalbank.com">www.texascapitalbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.813 |
| 2020 | 1.625 |
| 2019 | 1.625 |
| 2018 | 1.625 |
| 2017 | 1.625 |
| 2016 | 1.625 |
| 2015 | 1.625 |
| 2014 | 1.625 |
| 2013 | 1.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
