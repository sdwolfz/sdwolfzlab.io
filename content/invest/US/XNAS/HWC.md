---
title: "HANCOCK WHITNEY CORPORATION (HWC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HANCOCK WHITNEY CORPORATION</td></tr>
    <tr><td>Symbol</td><td>HWC</td></tr>
    <tr><td>Web</td><td><a href="https://www.hancockwhitney.com">www.hancockwhitney.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.27 |
| 2020 | 1.08 |
| 2019 | 1.08 |
| 2018 | 1.02 |
| 2017 | 0.96 |
| 2016 | 0.96 |
| 2015 | 0.96 |
| 2014 | 0.96 |
| 2013 | 0.96 |
| 2012 | 0.96 |
| 2011 | 0.96 |
| 2010 | 0.96 |
| 2009 | 0.96 |
| 2008 | 0.96 |
| 2007 | 0.96 |
| 2006 | 0.895 |
| 2005 | 0.72 |
| 2004 | 0.705 |
| 2003 | 0.88 |
| 2002 | 1.0 |
| 2001 | 1.12 |
| 2000 | 1.25 |
| 1999 | 1.0 |
| 1998 | 1.0 |
| 1997 | 1.0 |
| 1996 | 0.75 |
| 1995 | 0.96 |
| 1994 | 0.92 |
| 1993 | 0.51 |
| 1992 | 0.68 |
| 1991 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
