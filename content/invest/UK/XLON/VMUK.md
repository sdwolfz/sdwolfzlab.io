---
title: "Virgin Money Uk (VMUK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Virgin Money Uk</td></tr>
    <tr><td>Symbol</td><td>VMUK</td></tr>
    <tr><td>Web</td><td><a href="https://www.virginmoneyukplc.com">www.virginmoneyukplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 3.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
