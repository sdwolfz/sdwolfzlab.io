---
title: "LANDSTAR SYSTEMS INC (LSTR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LANDSTAR SYSTEMS INC</td></tr>
    <tr><td>Symbol</td><td>LSTR</td></tr>
    <tr><td>Web</td><td><a href="https://www.landstar.com">www.landstar.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.42 |
| 2020 | 2.79 |
| 2019 | 0.7 |
| 2018 | 2.13 |
| 2017 | 0.38 |
| 2016 | 0.34 |
| 2015 | 1.3 |
| 2014 | 0.26 |
| 2013 | 0.35 |
| 2012 | 0.73 |
| 2011 | 0.21 |
| 2010 | 0.19 |
| 2009 | 0.17 |
| 2008 | 0.155 |
| 2007 | 0.135 |
| 2006 | 0.11 |
| 2005 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
