---
title: "How to Use: guard"
date: 2020-10-15T18:25:25Z
type: posts
slug: how-to-use-guard
series: ["How to Use"]
tags: ["ruby", "development", "productivity"]
---

1. Add these gems to your `Gemfile`:

```ruby
group :development do
  gem 'guard'
  gem 'guard-shell'
end
```

2. Install it by running `bundle install`.

3. Create a `Guardfile` containing:

```ruby
# frozen_string_literal: true

clearing :on

guard :shell do
  watch(%r{(?:lib|test)/(.*)\.rb}) { |_m| `bundle exec rake test` }
end
```

4. Create the following entry in a `Makefile`:

```makefile
.PHONY: guard
guard:
	@bundle exec guard
```

5. Run it when doing development:

```sh
make guard
```

6. Enjoy auto-executing tests and tasks!
