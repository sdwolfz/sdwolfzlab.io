---
title: "National Express (NEX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>National Express</td></tr>
    <tr><td>Symbol</td><td>NEX</td></tr>
    <tr><td>Web</td><td><a href="https://www.nationalexpressgroup.com">www.nationalexpressgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 16.35 |
| 2018 | 14.86 |
| 2017 | 13.51 |
| 2016 | 12.28 |
| 2015 | 11.33 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
