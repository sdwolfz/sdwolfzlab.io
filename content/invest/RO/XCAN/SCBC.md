---
title: "SCUT SA BACAU (SCBC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>SCUT SA BACAU</td></tr>
    <tr><td>Symbol</td><td>SCBC</td></tr>
    <tr><td>Web</td><td><a href="https://www.sut.ro">www.sut.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.0 |
| 2019 | 1.0 |
| 2018 | 1.0 |
| 2017 | 0.5 |
| 2016 | 0.75 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
