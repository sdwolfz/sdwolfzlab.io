---
title: "GREAT ELM CAPITAL CORP (GECC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GREAT ELM CAPITAL CORP</td></tr>
    <tr><td>Symbol</td><td>GECC</td></tr>
    <tr><td>Web</td><td><a href="https://www.greatelmcc.com">www.greatelmcc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.996 |
| 2019 | 1.046 |
| 2018 | 1.236 |
| 2017 | 1.196 |
| 2016 | 0.166 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
