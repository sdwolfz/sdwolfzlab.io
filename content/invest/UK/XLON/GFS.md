---
title: "G4S PLC ORD 25P (GFS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>G4S PLC ORD 25P</td></tr>
    <tr><td>Symbol</td><td>GFS</td></tr>
    <tr><td>Web</td><td><a href="http://www.g4s.com">www.g4s.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 3.59 |
| 2018 | 9.7 |
| 2017 | 9.7 |
| 2016 | 9.41 |
| 2015 | 9.41 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
