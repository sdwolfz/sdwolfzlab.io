---
title: "MTS SYSTEMS CORP (MTSC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MTS SYSTEMS CORP</td></tr>
    <tr><td>Symbol</td><td>MTSC</td></tr>
    <tr><td>Web</td><td><a href="http://www.mts.com">www.mts.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.3 |
| 2019 | 1.2 |
| 2018 | 1.2 |
| 2017 | 1.2 |
| 2016 | 1.2 |
| 2015 | 1.2 |
| 2014 | 1.2 |
| 2013 | 1.2 |
| 2012 | 1.1 |
| 2011 | 0.9 |
| 2010 | 0.65 |
| 2009 | 0.6 |
| 2008 | 0.6 |
| 2007 | 0.52 |
| 2006 | 0.42 |
| 2005 | 0.36 |
| 2004 | 0.28 |
| 2003 | 0.24 |
| 2002 | 0.24 |
| 2001 | 0.24 |
| 2000 | 0.24 |
| 1999 | 0.24 |
| 1998 | 0.24 |
| 1997 | 0.42 |
| 1996 | 0.42 |
| 1995 | 0.58 |
| 1994 | 0.56 |
| 1993 | 0.5 |
| 1992 | 0.48 |
| 1991 | 0.42 |
| 1990 | 0.4 |
| 1989 | 0.31 |
| 1988 | 0.07 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
