---
title: " (FITBO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>FITBO</td></tr>
    <tr><td>Web</td><td><a href="https://www.53.com">www.53.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.309 |
| 2020 | 1.238 |
| 2019 | 0.358 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
