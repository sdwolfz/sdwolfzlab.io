---
title: "International Airlines (IAG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>International Airlines</td></tr>
    <tr><td>Symbol</td><td>IAG</td></tr>
    <tr><td>Web</td><td><a href="https://www.iairgroup.com">www.iairgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 10.32 |
| 2019 | 83.0 |
| 2018 | 66.0 |
| 2017 | 27.0 |
| 2016 | 23.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
