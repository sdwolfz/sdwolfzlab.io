---
title: " (HBANN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>HBANN</td></tr>
    <tr><td>Web</td><td><a href="https://www.huntington.com">www.huntington.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.735 |
| 2020 | 1.469 |
| 2019 | 1.469 |
| 2018 | 1.469 |
| 2017 | 1.469 |
| 2016 | 0.657 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
