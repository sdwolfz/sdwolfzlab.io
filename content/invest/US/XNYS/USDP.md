---
title: " (USDP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>USDP</td></tr>
    <tr><td>Web</td><td><a href="https://www.usdpartners.com">www.usdpartners.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.224 |
| 2020 | 0.703 |
| 2019 | 1.456 |
| 2018 | 1.414 |
| 2017 | 1.35 |
| 2016 | 1.245 |
| 2015 | 1.113 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
