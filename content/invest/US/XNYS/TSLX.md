---
title: "SIXTH STREET SPECIALTY LENDING INC (TSLX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SIXTH STREET SPECIALTY LENDING INC</td></tr>
    <tr><td>Symbol</td><td>TSLX</td></tr>
    <tr><td>Web</td><td><a href="https://www.sixthstreetspecialtylending.com">www.sixthstreetspecialtylending.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.36 |
| 2020 | 2.3 |
| 2019 | 1.81 |
| 2018 | 1.78 |
| 2017 | 1.64 |
| 2016 | 1.56 |
| 2015 | 1.56 |
| 2014 | 1.53 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
