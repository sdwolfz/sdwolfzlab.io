---
title: "NEXA RESOURCES S A (NEXA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NEXA RESOURCES S A</td></tr>
    <tr><td>Symbol</td><td>NEXA</td></tr>
    <tr><td>Web</td><td><a href="https://www.nexaresources.com">www.nexaresources.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.264 |
| 2020 | 0.321 |
| 2019 | 0.525 |
| 2018 | 0.468 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
