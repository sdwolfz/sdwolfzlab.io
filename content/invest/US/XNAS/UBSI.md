---
title: "UNITED BANKSHARES INC (UBSI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>UNITED BANKSHARES INC</td></tr>
    <tr><td>Symbol</td><td>UBSI</td></tr>
    <tr><td>Web</td><td><a href="https://www.ubsi-inc.com">www.ubsi-inc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.35 |
| 2020 | 1.4 |
| 2019 | 1.37 |
| 2018 | 1.36 |
| 2017 | 1.33 |
| 2016 | 1.32 |
| 2015 | 1.29 |
| 2014 | 1.28 |
| 2013 | 1.25 |
| 2012 | 1.24 |
| 2011 | 1.21 |
| 2010 | 1.2 |
| 2009 | 1.17 |
| 2008 | 1.16 |
| 2007 | 1.13 |
| 2006 | 1.09 |
| 2005 | 1.05 |
| 2004 | 1.02 |
| 2003 | 1.0 |
| 2002 | 0.95 |
| 2001 | 0.91 |
| 2000 | 0.84 |
| 1999 | 0.82 |
| 1998 | 0.92 |
| 1997 | 1.35 |
| 1996 | 1.24 |
| 1995 | 1.17 |
| 1994 | 1.06 |
| 1993 | 0.95 |
| 1992 | 0.85 |
| 1991 | 0.81 |
| 1990 | 0.75 |
| 1989 | 0.64 |
| 1988 | 0.14 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
