---
title: "HTML datalist"
date: 2021-05-06T11:51:19Z
type: posts
slug: html-datalist
tags: ["html"]
---

## Markup

```html
<label for="input-ID">Search for a word:</label>
<input list="datalist-ID" id="input-ID" name="input-NAME" />

<datalist id="datalist-ID">
  <option value="some">
  <option value="random">
  <option value="words">
  <option value="to">
  <option value="search">
  <option value="lol">
</datalist>
```

## Result

<label for="input-ID">Search for a word:</label>
<input list="datalist-ID" id="input-ID" name="input-NAME" />

<datalist id="datalist-ID">
  <option value="some">
  <option value="random">
  <option value="words">
  <option value="to">
  <option value="search">
  <option value="lol">
</datalist>

## Caveats

It doesen't work on Firefox Mobile on Android, of course...
* https://bugzilla.mozilla.org/show_bug.cgi?id=1535985
