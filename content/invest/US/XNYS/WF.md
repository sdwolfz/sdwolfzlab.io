---
title: " (WF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>WF</td></tr>
    <tr><td>Web</td><td><a href="https://www.woorifg.com">www.woorifg.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.788 |
| 2019 | 1.442 |
| 2018 | 1.431 |
| 2017 | 1.169 |
| 2016 | 0.8 |
| 2015 | 0.487 |
| 2014 | 1.046 |
| 2012 | 0.501 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
