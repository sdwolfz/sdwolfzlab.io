---
title: " (PHI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>PHI</td></tr>
    <tr><td>Web</td><td><a href="https://www.pldt.com">www.pldt.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.555 |
| 2020 | 1.044 |
| 2019 | 0.926 |
| 2018 | 0.803 |
| 2017 | 1.231 |
| 2016 | 1.042 |
| 2015 | 2.427 |
| 2014 | 2.14 |
| 2013 | 1.064 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
