---
title: "ADIT EDTECH ACQUISITION CORP (ADEX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ADIT EDTECH ACQUISITION CORP</td></tr>
    <tr><td>Symbol</td><td>ADEX</td></tr>
    <tr><td>Web</td><td><a href="https://www.aditedtech.com">www.aditedtech.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
