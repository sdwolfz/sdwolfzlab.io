---
title: "BARINGS BDC INC (BBDC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BARINGS BDC INC</td></tr>
    <tr><td>Symbol</td><td>BBDC</td></tr>
    <tr><td>Web</td><td><a href="https://www.barings.com">www.barings.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.19 |
| 2020 | 0.65 |
| 2019 | 0.54 |
| 2018 | 1.91 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
