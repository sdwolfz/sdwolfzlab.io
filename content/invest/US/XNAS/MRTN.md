---
title: "MARTEN TRANSPORT (MRTN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MARTEN TRANSPORT</td></tr>
    <tr><td>Symbol</td><td>MRTN</td></tr>
    <tr><td>Web</td><td><a href="https://www.marten.com">www.marten.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.08 |
| 2020 | 0.66 |
| 2019 | 0.77 |
| 2018 | 0.1 |
| 2017 | 0.1 |
| 2016 | 0.1 |
| 2015 | 0.1 |
| 2014 | 0.1 |
| 2013 | 0.1 |
| 2012 | 0.845 |
| 2011 | 0.08 |
| 2010 | 0.04 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
