---
title: "HELMERICH & PAYNE INC (HP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HELMERICH & PAYNE INC</td></tr>
    <tr><td>Symbol</td><td>HP</td></tr>
    <tr><td>Web</td><td><a href="https://www.hpinc.com">www.hpinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.5 |
| 2020 | 1.92 |
| 2019 | 2.84 |
| 2018 | 2.82 |
| 2017 | 2.8 |
| 2016 | 2.776 |
| 2015 | 2.752 |
| 2014 | 2.626 |
| 2013 | 1.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
