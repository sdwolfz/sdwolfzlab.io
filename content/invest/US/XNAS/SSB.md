---
title: "SOUTH STATE CORP (SSB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SOUTH STATE CORP</td></tr>
    <tr><td>Symbol</td><td>SSB</td></tr>
    <tr><td>Web</td><td><a href="https://www.southstatebank.com">www.southstatebank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.94 |
| 2020 | 1.88 |
| 2019 | 1.67 |
| 2018 | 1.38 |
| 2017 | 1.32 |
| 2016 | 1.21 |
| 2015 | 0.98 |
| 2014 | 0.82 |
| 2013 | 0.74 |
| 2012 | 0.69 |
| 2011 | 0.68 |
| 2010 | 0.68 |
| 2009 | 0.68 |
| 2008 | 0.68 |
| 2007 | 0.68 |
| 2006 | 0.68 |
| 2005 | 0.68 |
| 2004 | 0.68 |
| 2003 | 0.66 |
| 2002 | 0.63 |
| 2001 | 0.57 |
| 2000 | 0.54 |
| 1999 | 0.13 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
