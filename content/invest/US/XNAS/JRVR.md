---
title: "JAMES RIVER GROUP HOLDINGS (JRVR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>JAMES RIVER GROUP HOLDINGS</td></tr>
    <tr><td>Symbol</td><td>JRVR</td></tr>
    <tr><td>Web</td><td><a href="https://www.jrgh.net">www.jrgh.net</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.6 |
| 2020 | 1.2 |
| 2019 | 1.2 |
| 2018 | 1.2 |
| 2017 | 1.7 |
| 2016 | 2.25 |
| 2015 | 1.64 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
