---
title: "DIME COMMUNITY BANCSHARES INC. (DCOM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>DIME COMMUNITY BANCSHARES INC.</td></tr>
    <tr><td>Symbol</td><td>DCOM</td></tr>
    <tr><td>Web</td><td><a href="https://www.dime.com">www.dime.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.48 |
| 2020 | 0.96 |
| 2019 | 0.92 |
| 2018 | 0.92 |
| 2017 | 0.92 |
| 2016 | 0.92 |
| 2015 | 0.92 |
| 2014 | 0.92 |
| 2013 | 0.69 |
| 2012 | 1.15 |
| 2011 | 0.69 |
| 2010 | 0.92 |
| 2009 | 0.92 |
| 2008 | 0.92 |
| 2007 | 0.92 |
| 2006 | 0.92 |
| 2005 | 0.91 |
| 2004 | 0.88 |
| 2003 | 1.18 |
| 2002 | 0.61 |
| 2001 | 0.55 |
| 2000 | 0.49 |
| 1999 | 0.42 |
| 1998 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
