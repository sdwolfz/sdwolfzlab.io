---
title: "LIMESTONE BANCORP INC (LMST)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LIMESTONE BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>LMST</td></tr>
    <tr><td>Web</td><td><a href="https://www.limestonebank.com">www.limestonebank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2011 | 0.02 |
| 2010 | 0.51 |
| 2009 | 0.83 |
| 2008 | 0.84 |
| 2007 | 0.81 |
| 2006 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
