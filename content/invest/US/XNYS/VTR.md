---
title: "VENTAS INC (VTR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>VENTAS INC</td></tr>
    <tr><td>Symbol</td><td>VTR</td></tr>
    <tr><td>Web</td><td><a href="http://www.ventasreit.com">www.ventasreit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.143 |
| 2019 | 3.172 |
| 2018 | 3.163 |
| 2017 | 3.115 |
| 2016 | 2.965 |
| 2015 | 3.04 |
| 2014 | 2.965 |
| 2013 | 2.065 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
