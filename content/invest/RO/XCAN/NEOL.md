---
title: "NEPTUN OLIMP SA NEPTUN (NEOL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>NEPTUN OLIMP SA NEPTUN</td></tr>
    <tr><td>Symbol</td><td>NEOL</td></tr>
    <tr><td>Web</td><td><a href="https://www.neptunolimp.com">www.neptunolimp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
