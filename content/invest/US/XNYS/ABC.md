---
title: "AMERISOURCEBERGEN CORPORATION (ABC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AMERISOURCEBERGEN CORPORATION</td></tr>
    <tr><td>Symbol</td><td>ABC</td></tr>
    <tr><td>Web</td><td><a href="https://www.amerisourcebergen.com">www.amerisourcebergen.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.88 |
| 2020 | 1.7 |
| 2019 | 1.6 |
| 2018 | 1.54 |
| 2017 | 1.475 |
| 2016 | 1.385 |
| 2015 | 1.21 |
| 2014 | 0.995 |
| 2013 | 0.445 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
