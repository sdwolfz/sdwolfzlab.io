---
title: "Rotork (ROR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Rotork</td></tr>
    <tr><td>Symbol</td><td>ROR</td></tr>
    <tr><td>Web</td><td><a href="https://www.rotork.com">www.rotork.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 6.2 |
| 2018 | 5.9 |
| 2017 | 5.4 |
| 2016 | 5.1 |
| 2015 | 5.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
