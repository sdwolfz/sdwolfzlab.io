---
title: "HILL-ROM HOLDINGS INC (HRC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HILL-ROM HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>HRC</td></tr>
    <tr><td>Web</td><td><a href="https://www.hill-rom.com">www.hill-rom.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.48 |
| 2020 | 0.88 |
| 2019 | 0.84 |
| 2018 | 0.8 |
| 2017 | 0.72 |
| 2016 | 0.68 |
| 2015 | 0.64 |
| 2014 | 0.612 |
| 2013 | 0.414 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
