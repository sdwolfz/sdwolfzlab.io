---
title: "FINCH THERAPEUTICS GROUP INC (FNCH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FINCH THERAPEUTICS GROUP INC</td></tr>
    <tr><td>Symbol</td><td>FNCH</td></tr>
    <tr><td>Web</td><td><a href="https://www.finchtherapeutics.com">www.finchtherapeutics.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
