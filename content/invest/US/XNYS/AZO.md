---
title: "AUTOZONE INC (AZO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AUTOZONE INC</td></tr>
    <tr><td>Symbol</td><td>AZO</td></tr>
    <tr><td>Web</td><td><a href="https://www.autozone.com">www.autozone.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
