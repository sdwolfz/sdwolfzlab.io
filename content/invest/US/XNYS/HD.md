---
title: "HOME DEPOT INC (HD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HOME DEPOT INC</td></tr>
    <tr><td>Symbol</td><td>HD</td></tr>
    <tr><td>Web</td><td><a href="https://www.homedepot.com">www.homedepot.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.65 |
| 2020 | 6.0 |
| 2019 | 5.44 |
| 2018 | 4.12 |
| 2017 | 3.56 |
| 2016 | 2.76 |
| 2015 | 2.36 |
| 2014 | 1.88 |
| 2013 | 1.17 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
