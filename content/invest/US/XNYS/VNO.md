---
title: " (VNO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>VNO</td></tr>
    <tr><td>Web</td><td><a href="https://www.vno.com">www.vno.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.06 |
| 2020 | 2.38 |
| 2019 | 4.59 |
| 2018 | 2.52 |
| 2017 | 2.62 |
| 2016 | 2.52 |
| 2015 | 2.52 |
| 2014 | 2.92 |
| 2013 | 1.46 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
