---
title: "QUALCOMM INC (QCOM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>QUALCOMM INC</td></tr>
    <tr><td>Symbol</td><td>QCOM</td></tr>
    <tr><td>Web</td><td><a href="https://www.qualcomm.com">www.qualcomm.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.33 |
| 2020 | 2.57 |
| 2019 | 2.48 |
| 2018 | 2.43 |
| 2017 | 2.24 |
| 2016 | 2.07 |
| 2015 | 1.86 |
| 2014 | 1.61 |
| 2013 | 1.3 |
| 2012 | 0.965 |
| 2011 | 0.835 |
| 2010 | 0.74 |
| 2009 | 0.67 |
| 2008 | 0.62 |
| 2007 | 0.54 |
| 2006 | 0.45 |
| 2005 | 0.34 |
| 2004 | 0.31 |
| 2003 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
