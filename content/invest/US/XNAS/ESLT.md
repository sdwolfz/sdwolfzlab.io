---
title: "ELBIT SYSTEMS LTD (ESLT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ELBIT SYSTEMS LTD</td></tr>
    <tr><td>Symbol</td><td>ESLT</td></tr>
    <tr><td>Web</td><td><a href="https://www.elbitsystems.com">www.elbitsystems.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.44 |
| 2020 | 1.67 |
| 2019 | 1.76 |
| 2018 | 1.76 |
| 2017 | 1.76 |
| 2016 | 1.6 |
| 2015 | 1.44 |
| 2014 | 1.26 |
| 2013 | 1.2 |
| 2012 | 1.2 |
| 2011 | 1.44 |
| 2010 | 1.44 |
| 2009 | 1.82 |
| 2008 | 0.8 |
| 2007 | 0.65 |
| 2006 | 0.59 |
| 2005 | 0.53 |
| 2004 | 2.04 |
| 2003 | 0.2 |
| 2002 | 0.33 |
| 2001 | 0.16 |
| 2000 | 0.41 |
| 1999 | 0.24 |
| 1998 | 0.2 |
| 1997 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
