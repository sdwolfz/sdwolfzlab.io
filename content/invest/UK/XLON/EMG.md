---
title: "Man (EMG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Man</td></tr>
    <tr><td>Symbol</td><td>EMG</td></tr>
    <tr><td>Web</td><td><a href="https://www.man.com">www.man.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 10.0 |
| 2019 | 9.8 |
| 2018 | 12.2 |
| 2017 | 10.8 |
| 2016 | 9.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
