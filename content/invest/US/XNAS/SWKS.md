---
title: "SKYWORKS SOLUTIONS INC (SWKS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SKYWORKS SOLUTIONS INC</td></tr>
    <tr><td>Symbol</td><td>SWKS</td></tr>
    <tr><td>Web</td><td><a href="https://www.skyworksinc.com">www.skyworksinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.0 |
| 2020 | 1.88 |
| 2019 | 1.64 |
| 2018 | 1.4 |
| 2017 | 1.2 |
| 2016 | 1.08 |
| 2015 | 0.78 |
| 2014 | 0.35 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
