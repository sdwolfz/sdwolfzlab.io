---
title: "MEDALLION FINANCIAL CORP (MFIN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MEDALLION FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>MFIN</td></tr>
    <tr><td>Web</td><td><a href="https://www.medallion.com">www.medallion.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.6 |
| 2015 | 0.99 |
| 2014 | 0.95 |
| 2013 | 0.89 |
| 2012 | 0.83 |
| 2011 | 0.7 |
| 2010 | 0.6 |
| 2009 | 0.76 |
| 2008 | 0.76 |
| 2007 | 0.76 |
| 2006 | 0.66 |
| 2005 | 0.5 |
| 2004 | 0.33 |
| 2003 | 0.09 |
| 2002 | 0.03 |
| 2001 | 0.38 |
| 2000 | 1.22 |
| 1999 | 1.31 |
| 1998 | 1.22 |
| 1997 | 0.95 |
| 1996 | 0.41 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
