---
title: "NATURAL HEALTH TRENDS CORP (NHTC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NATURAL HEALTH TRENDS CORP</td></tr>
    <tr><td>Symbol</td><td>NHTC</td></tr>
    <tr><td>Web</td><td><a href="https://www.naturalhealthtrendscorp.com">www.naturalhealthtrendscorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.4 |
| 2020 | 0.8 |
| 2019 | 0.64 |
| 2018 | 2.77 |
| 2017 | 1.52 |
| 2016 | 0.61 |
| 2015 | 0.14 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
