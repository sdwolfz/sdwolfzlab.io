---
title: "SYSCO CORP (SYY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SYSCO CORP</td></tr>
    <tr><td>Symbol</td><td>SYY</td></tr>
    <tr><td>Web</td><td><a href="https://www.sysco.com">www.sysco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.9 |
| 2020 | 1.8 |
| 2019 | 1.56 |
| 2018 | 1.44 |
| 2017 | 1.32 |
| 2016 | 1.24 |
| 2015 | 0.9 |
| 2014 | 1.17 |
| 2013 | 0.85 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
