---
title: "Schroder UK Mid & Small Cap Fund (SCP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Schroder UK Mid & Small Cap Fund</td></tr>
    <tr><td>Symbol</td><td>SCP</td></tr>
    <tr><td>Web</td><td><a href="https://www.schroders.co.uk">www.schroders.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 16.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
