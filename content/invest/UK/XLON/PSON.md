---
title: "Pearson (PSON)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Pearson</td></tr>
    <tr><td>Symbol</td><td>PSON</td></tr>
    <tr><td>Web</td><td><a href="https://www.pearson.com">www.pearson.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 13.5 |
| 2019 | 19.5 |
| 2018 | 18.5 |
| 2017 | 17.0 |
| 2016 | 52.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
