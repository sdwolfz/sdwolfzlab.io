---
title: "ENERPLUS CORPORATION (ERF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ENERPLUS CORPORATION</td></tr>
    <tr><td>Symbol</td><td>ERF</td></tr>
    <tr><td>Web</td><td><a href="https://www.enerplus.com">www.enerplus.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.09 |
| 2019 | 0.092 |
| 2018 | 0.096 |
| 2017 | 0.109 |
| 2016 | 0.127 |
| 2015 | 0.619 |
| 2014 | 1.17 |
| 2013 | 0.45 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
