---
title: "Electra Private Equity (ELTA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Electra Private Equity</td></tr>
    <tr><td>Symbol</td><td>ELTA</td></tr>
    <tr><td>Web</td><td><a href="https://www.electraequity.com">www.electraequity.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 31.0 |
| 2019 | 419.0 |
| 2018 | 939.0 |
| 2017 | 440.0 |
| 2016 | 154.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
