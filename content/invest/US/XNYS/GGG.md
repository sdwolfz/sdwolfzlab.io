---
title: "GRACO INC (GGG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GRACO INC</td></tr>
    <tr><td>Symbol</td><td>GGG</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.376 |
| 2020 | 0.7 |
| 2019 | 0.64 |
| 2018 | 0.528 |
| 2017 | 1.44 |
| 2016 | 1.32 |
| 2015 | 1.2 |
| 2014 | 1.1 |
| 2013 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
