---
title: "PETROCART SA PIATRA NEAMT (PTRC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>PETROCART SA PIATRA NEAMT</td></tr>
    <tr><td>Symbol</td><td>PTRC</td></tr>
    <tr><td>Web</td><td><a href="https://www.petrocart.ro">www.petrocart.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2001 | 0.7307 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
