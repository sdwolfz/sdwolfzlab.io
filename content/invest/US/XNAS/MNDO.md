---
title: "MIND C.T.I (MNDO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MIND C.T.I</td></tr>
    <tr><td>Symbol</td><td>MNDO</td></tr>
    <tr><td>Web</td><td><a href="https://www.mindcti.com">www.mindcti.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.26 |
| 2020 | 0.24 |
| 2019 | 0.26 |
| 2018 | 0.3 |
| 2017 | 0.32 |
| 2016 | 0.27 |
| 2015 | 0.3 |
| 2014 | 0.24 |
| 2013 | 0.24 |
| 2012 | 0.24 |
| 2011 | 0.32 |
| 2010 | 0.2 |
| 2009 | 0.8 |
| 2008 | 0.2 |
| 2007 | 0.2 |
| 2006 | 0.14 |
| 2005 | 0.24 |
| 2004 | 0.13 |
| 2003 | 0.14 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
