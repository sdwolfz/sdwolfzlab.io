---
title: "FULTON FINANCIAL CORP (FULT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FULTON FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>FULT</td></tr>
    <tr><td>Web</td><td><a href="https://www.fult.com">www.fult.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.14 |
| 2020 | 0.56 |
| 2019 | 0.56 |
| 2018 | 0.52 |
| 2017 | 0.47 |
| 2016 | 0.41 |
| 2015 | 0.38 |
| 2014 | 0.34 |
| 2013 | 0.32 |
| 2012 | 0.3 |
| 2011 | 0.2 |
| 2010 | 0.12 |
| 2009 | 0.12 |
| 2008 | 0.6 |
| 2007 | 0.597 |
| 2006 | 0.588 |
| 2005 | 0.6 |
| 2004 | 0.655 |
| 2003 | 0.63 |
| 2002 | 0.64 |
| 2001 | 0.67 |
| 2000 | 0.63 |
| 1999 | 0.6 |
| 1998 | 0.62 |
| 1997 | 0.68 |
| 1996 | 0.68 |
| 1995 | 0.68 |
| 1994 | 0.71 |
| 1993 | 0.8 |
| 1992 | 0.8 |
| 1991 | 0.8 |
| 1990 | 0.8 |
| 1989 | 0.8 |
| 1988 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
