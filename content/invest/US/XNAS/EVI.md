---
title: "EVI INDUSTRIES INC (EVI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>EVI INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>EVI</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.13 |
| 2017 | 0.12 |
| 2016 | 0.1 |
| 2015 | 0.2 |
| 2014 | 0.2 |
| 2013 | 0.4 |
| 2012 | 0.6 |
| 2011 | 0.05 |
| 2007 | 0.08 |
| 2006 | 0.08 |
| 2005 | 0.075 |
| 2004 | 0.06 |
| 2003 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
