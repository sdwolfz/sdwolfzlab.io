---
title: "Phoenix Group Holdings (PHNX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Phoenix Group Holdings</td></tr>
    <tr><td>Symbol</td><td>PHNX</td></tr>
    <tr><td>Web</td><td><a href="https://www.thephoenixgroup.com">www.thephoenixgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 46.8 |
| 2018 | 46.0 |
| 2017 | 50.2 |
| 2016 | 50.6 |
| 2015 | 53.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
