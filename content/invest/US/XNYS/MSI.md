---
title: "MOTOROLA SOLUTIONS INC (MSI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MOTOROLA SOLUTIONS INC</td></tr>
    <tr><td>Symbol</td><td>MSI</td></tr>
    <tr><td>Web</td><td><a href="https://www.motorolasolutions.com">www.motorolasolutions.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.71 |
| 2020 | 2.63 |
| 2019 | 2.35 |
| 2018 | 2.13 |
| 2017 | 1.93 |
| 2016 | 1.7 |
| 2015 | 1.43 |
| 2014 | 1.3 |
| 2013 | 0.62 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
