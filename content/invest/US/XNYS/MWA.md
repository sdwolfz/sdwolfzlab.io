---
title: "MUELLER WATER PRODUCTS INC (MWA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MUELLER WATER PRODUCTS INC</td></tr>
    <tr><td>Symbol</td><td>MWA</td></tr>
    <tr><td>Web</td><td><a href="https://www.muellerwaterproducts.com">www.muellerwaterproducts.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.11 |
| 2020 | 0.211 |
| 2019 | 0.204 |
| 2018 | 0.2 |
| 2017 | 0.16 |
| 2016 | 0.11 |
| 2015 | 0.078 |
| 2014 | 0.072 |
| 2013 | 0.036 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
