---
title: "HINGHAM INSTITUTION FOR SAVINGS (HIFS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HINGHAM INSTITUTION FOR SAVINGS</td></tr>
    <tr><td>Symbol</td><td>HIFS</td></tr>
    <tr><td>Web</td><td><a href="https://www.hinghamsavings.com">www.hinghamsavings.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.49 |
| 2020 | 3.48 |
| 2019 | 2.04 |
| 2018 | 1.73 |
| 2017 | 1.62 |
| 2016 | 1.52 |
| 2015 | 2.14 |
| 2014 | 1.37 |
| 2013 | 1.32 |
| 2012 | 1.28 |
| 2011 | 1.23 |
| 2010 | 1.15 |
| 2009 | 1.07 |
| 2008 | 1.02 |
| 2007 | 1.0 |
| 2006 | 0.8 |
| 2005 | 0.98 |
| 2004 | 0.92 |
| 2003 | 0.88 |
| 2002 | 0.8 |
| 2001 | 0.73 |
| 2000 | 0.64 |
| 1999 | 0.55 |
| 1998 | 0.62 |
| 1997 | 0.53 |
| 1996 | 0.42 |
| 1995 | 0.24 |
| 1994 | 0.12 |
| 1990 | 0.28 |
| 1989 | 0.23 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
