---
title: "CVR ENERGY INC (CVI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CVR ENERGY INC</td></tr>
    <tr><td>Symbol</td><td>CVI</td></tr>
    <tr><td>Web</td><td><a href="https://www.cvrenergy.com">www.cvrenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.2 |
| 2019 | 3.05 |
| 2018 | 2.5 |
| 2017 | 2.0 |
| 2016 | 2.0 |
| 2015 | 2.0 |
| 2014 | 3.0 |
| 2013 | 1.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
