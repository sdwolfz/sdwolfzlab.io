---
title: "Disable annoying Linux bell sound"
date: 2022-06-23T23:59:06Z
type: posts
slug: disable-annoying-linux-bell-sound
tags: ["linux"]
---

## Basic

```sh
sudo echo 'blacklist pcspkr' > /etc/modprobe.d/nobeep.conf
```

And restart your machine!

## Automated

Add this to an ansible role `roles/os/configure/nobeep.yml`:

```yml
# -------------------------------------------------------------------------------
# Bell Sound
# -------------------------------------------------------------------------------

- name: Blacklist the pcspkr module to silence horrendous beeps
  copy:
    dest: /etc/modprobe.d/nobeep.conf
    content: |
      blacklist pcspkr
  become: true
```

Reference it in a playbook `playbooks/os/configure.yml`:

```yml
---

- hosts: localhost
  roles:
    - { role: ../../roles/os/configure/nobeep }
```

And invoke that playbook:

```makefile
.PHONY: os/configure
os/configure:
	@ansible-playbook playbooks/os/configure.yml --ask-become-pass
```

Using make:

```sh
make os/configure
```
