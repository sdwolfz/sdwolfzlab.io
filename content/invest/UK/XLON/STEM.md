---
title: "Sthree (STEM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Sthree</td></tr>
    <tr><td>Symbol</td><td>STEM</td></tr>
    <tr><td>Web</td><td><a href="https://www.sthree.com">www.sthree.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 5.0 |
| 2019 | 5.1 |
| 2018 | 14.5 |
| 2017 | 14.0 |
| 2016 | 14.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
