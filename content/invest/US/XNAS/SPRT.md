---
title: "SUPPORT COM INC (SPRT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SUPPORT COM INC</td></tr>
    <tr><td>Symbol</td><td>SPRT</td></tr>
    <tr><td>Web</td><td><a href="https://www.support.com">www.support.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 1.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
