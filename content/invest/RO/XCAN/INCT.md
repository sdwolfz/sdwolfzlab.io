---
title: "INCERTRANS SA BUCURESTI (INCT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>INCERTRANS SA BUCURESTI</td></tr>
    <tr><td>Symbol</td><td>INCT</td></tr>
    <tr><td>Web</td><td><a href="https://www.incertrans.ro">www.incertrans.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.01 |
| 2018 | 0.02 |
| 2017 | 0.031 |
| 2017 | 0.019 |
| 2007 | 0.06725 |
| 2006 | 0.0687 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
