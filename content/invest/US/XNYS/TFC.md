---
title: "TRUIST FINANCIAL CORPORATION (TFC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TRUIST FINANCIAL CORPORATION</td></tr>
    <tr><td>Symbol</td><td>TFC</td></tr>
    <tr><td>Web</td><td><a href="https://www.truist.com">www.truist.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.9 |
| 2020 | 1.8 |
| 2019 | 1.71 |
| 2018 | 1.56 |
| 2017 | 1.26 |
| 2016 | 1.15 |
| 2015 | 1.05 |
| 2014 | 0.95 |
| 2013 | 0.46 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
