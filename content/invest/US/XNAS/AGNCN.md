---
title: " (AGNCN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>AGNCN</td></tr>
    <tr><td>Web</td><td><a href="https://www.agnc.com">www.agnc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.438 |
| 2020 | 1.75 |
| 2019 | 1.75 |
| 2018 | 1.75 |
| 2017 | 0.695 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
