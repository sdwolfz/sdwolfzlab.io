---
title: "COFFEE HLDG CO INC (JVA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COFFEE HLDG CO INC</td></tr>
    <tr><td>Symbol</td><td>JVA</td></tr>
    <tr><td>Web</td><td><a href="https://www.coffeeholding.com">www.coffeeholding.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2012 | 0.18 |
| 2011 | 0.12 |
| 2010 | 0.06 |
| 2008 | 0.28 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
