---
title: " (SPG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SPG</td></tr>
    <tr><td>Web</td><td><a href="https://www.simon.com">www.simon.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.3 |
| 2020 | 6.0 |
| 2019 | 8.3 |
| 2018 | 7.9 |
| 2017 | 7.15 |
| 2016 | 6.5 |
| 2015 | 6.05 |
| 2014 | 5.15 |
| 2013 | 2.35 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
