---
title: "HASTINGS GROUP HOLDINGS PLC ORD GBP0.02 (HSTG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>HASTINGS GROUP HOLDINGS PLC ORD GBP0.02</td></tr>
    <tr><td>Symbol</td><td>HSTG</td></tr>
    <tr><td>Web</td><td><a href="http://www.hastingsplc.com">www.hastingsplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 13.5 |
| 2017 | 12.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
