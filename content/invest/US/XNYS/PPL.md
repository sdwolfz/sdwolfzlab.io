---
title: "PPL CORP (PPL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PPL CORP</td></tr>
    <tr><td>Symbol</td><td>PPL</td></tr>
    <tr><td>Web</td><td><a href="https://www.pplweb.com">www.pplweb.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.415 |
| 2020 | 1.66 |
| 2019 | 1.648 |
| 2018 | 1.64 |
| 2017 | 1.58 |
| 2016 | 1.52 |
| 2015 | 1.5 |
| 2014 | 1.492 |
| 2013 | 1.104 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
