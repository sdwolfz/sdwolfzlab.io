---
title: "PRIMIS FINANCIAL CORP (FRST)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PRIMIS FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>FRST</td></tr>
    <tr><td>Web</td><td><a href="https://www.primisbank.com">www.primisbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.4 |
| 2019 | 0.36 |
| 2018 | 0.32 |
| 2017 | 0.32 |
| 2016 | 0.32 |
| 2015 | 0.52 |
| 2014 | 0.6 |
| 2013 | 0.25 |
| 2012 | 0.245 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
