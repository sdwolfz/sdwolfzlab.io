---
title: "URBAN&CIVIC PLC ORD 20P (UANC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>URBAN&CIVIC PLC ORD 20P</td></tr>
    <tr><td>Symbol</td><td>UANC</td></tr>
    <tr><td>Web</td><td><a href="http://www.urbanandcivic.com">www.urbanandcivic.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 3.5 |
| 2017 | 3.2 |
| 2016 | 2.9 |
| 2015 | 26.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
