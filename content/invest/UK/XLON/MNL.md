---
title: "Manchester&lon. (MNL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Manchester&lon.</td></tr>
    <tr><td>Symbol</td><td>MNL</td></tr>
    <tr><td>Web</td><td><a href="https://www.mlcapman.com">www.mlcapman.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 14.0 |
| 2019 | 14.0 |
| 2018 | 12.0 |
| 2017 | 9.0 |
| 2016 | 13.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
