---
title: "VOYA FINANCIAL INC (VOYA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>VOYA FINANCIAL INC</td></tr>
    <tr><td>Symbol</td><td>VOYA</td></tr>
    <tr><td>Web</td><td><a href="https://www.voya.com">www.voya.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.33 |
| 2020 | 0.6 |
| 2019 | 0.32 |
| 2018 | 0.04 |
| 2017 | 0.04 |
| 2016 | 0.04 |
| 2015 | 0.04 |
| 2014 | 0.04 |
| 2013 | 0.02 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
