---
title: "GRAPHIC PACKAGING HOLDING COMPANY (GPK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GRAPHIC PACKAGING HOLDING COMPANY</td></tr>
    <tr><td>Symbol</td><td>GPK</td></tr>
    <tr><td>Web</td><td><a href="https://www.graphicpkg.com">www.graphicpkg.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.075 |
| 2020 | 0.3 |
| 2019 | 0.3 |
| 2018 | 0.3 |
| 2017 | 0.375 |
| 2016 | 0.225 |
| 2015 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
