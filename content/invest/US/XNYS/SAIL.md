---
title: "SAILPOINT TECHNOLOGIES HOLDINGS INC (SAIL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SAILPOINT TECHNOLOGIES HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>SAIL</td></tr>
    <tr><td>Web</td><td><a href="https://www.sailpoint.com">www.sailpoint.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
