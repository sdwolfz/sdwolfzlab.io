---
title: "UNIVERSAL CORP (UVV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>UNIVERSAL CORP</td></tr>
    <tr><td>Symbol</td><td>UVV</td></tr>
    <tr><td>Web</td><td><a href="https://www.universalcorp.com">www.universalcorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.54 |
| 2020 | 3.06 |
| 2019 | 3.02 |
| 2018 | 2.6 |
| 2017 | 2.16 |
| 2016 | 2.12 |
| 2015 | 2.08 |
| 2014 | 2.04 |
| 2013 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
