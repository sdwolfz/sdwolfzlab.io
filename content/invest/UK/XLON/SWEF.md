---
title: "Starwood Eur (SWEF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Starwood Eur</td></tr>
    <tr><td>Symbol</td><td>SWEF</td></tr>
    <tr><td>Web</td><td><a href="https://www.starwoodeuropeanfinance.com">www.starwoodeuropeanfinance.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 6.5 |
| 2018 | 6.5 |
| 2017 | 6.5 |
| 2016 | 6.5 |
| 2015 | 7.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
