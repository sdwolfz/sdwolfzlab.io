---
title: "TRACTOR SUPPLY CO (TSCO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TRACTOR SUPPLY CO</td></tr>
    <tr><td>Symbol</td><td>TSCO</td></tr>
    <tr><td>Web</td><td><a href="https://www.tractorsupply.com">www.tractorsupply.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.04 |
| 2020 | 1.5 |
| 2019 | 1.36 |
| 2018 | 1.2 |
| 2017 | 1.05 |
| 2016 | 0.92 |
| 2015 | 0.76 |
| 2014 | 0.61 |
| 2013 | 0.85 |
| 2012 | 0.72 |
| 2011 | 0.43 |
| 2010 | 0.49 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
