---
title: "FEDERATED HERMES INC (FHI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FEDERATED HERMES INC</td></tr>
    <tr><td>Symbol</td><td>FHI</td></tr>
    <tr><td>Web</td><td><a href="https://www.federatedhermes.com">www.federatedhermes.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.54 |
| 2020 | 2.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
