---
title: " (GEO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>GEO</td></tr>
    <tr><td>Web</td><td><a href="https://www.geogroup.com">www.geogroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 1.78 |
| 2019 | 1.92 |
| 2018 | 1.88 |
| 2017 | 1.877 |
| 2016 | 2.6 |
| 2015 | 2.51 |
| 2014 | 2.33 |
| 2013 | 1.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
