---
title: "INTERNATIONAL PAPER CO (IP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>INTERNATIONAL PAPER CO</td></tr>
    <tr><td>Symbol</td><td>IP</td></tr>
    <tr><td>Web</td><td><a href="https://www.internationalpaper.com">www.internationalpaper.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.512 |
| 2020 | 2.048 |
| 2019 | 2.012 |
| 2018 | 1.925 |
| 2017 | 1.864 |
| 2016 | 1.783 |
| 2015 | 1.64 |
| 2014 | 1.45 |
| 2013 | 0.65 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
