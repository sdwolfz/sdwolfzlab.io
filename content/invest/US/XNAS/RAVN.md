---
title: "RAVEN INDUSTRIES INC (RAVN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>RAVEN INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>RAVN</td></tr>
    <tr><td>Web</td><td><a href="https://www.ravenind.com">www.ravenind.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.39 |
| 2019 | 0.52 |
| 2018 | 0.52 |
| 2017 | 0.52 |
| 2016 | 0.52 |
| 2015 | 0.52 |
| 2014 | 0.49 |
| 2013 | 0.36 |
| 2012 | 0.63 |
| 2011 | 0.72 |
| 2010 | 1.89 |
| 2009 | 0.55 |
| 2008 | 1.77 |
| 2007 | 0.44 |
| 2006 | 0.36 |
| 2005 | 0.28 |
| 2004 | 1.635 |
| 2003 | 0.34 |
| 2002 | 0.56 |
| 2001 | 0.635 |
| 2000 | 0.7 |
| 1999 | 0.66 |
| 1998 | 0.62 |
| 1997 | 0.56 |
| 1996 | 0.5 |
| 1995 | 0.45 |
| 1994 | 0.39 |
| 1993 | 0.33 |
| 1992 | 0.075 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
