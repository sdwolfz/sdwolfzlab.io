---
title: "EQUITRANS MIDSTREAM CORPORATION (ETRN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>EQUITRANS MIDSTREAM CORPORATION</td></tr>
    <tr><td>Symbol</td><td>ETRN</td></tr>
    <tr><td>Web</td><td><a href="https://www.equitransmidstream.com">www.equitransmidstream.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 0.9 |
| 2019 | 1.76 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
