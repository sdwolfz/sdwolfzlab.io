---
title: " (JP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>JP</td></tr>
    <tr><td>Web</td><td><a href="https://www.jupai.investorroom.com">www.jupai.investorroom.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.58 |
| 2017 | 0.478 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
