---
title: "METEN EDTECHX EDUCATION GROUP LTD (METX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>METEN EDTECHX EDUCATION GROUP LTD</td></tr>
    <tr><td>Symbol</td><td>METX</td></tr>
    <tr><td>Web</td><td><a href="https://www.meten.com">www.meten.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
