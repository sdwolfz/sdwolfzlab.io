---
title: "NACCO INDUSTRIES INC (NC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NACCO INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>NC</td></tr>
    <tr><td>Web</td><td><a href="https://www.nacco.com">www.nacco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.192 |
| 2020 | 0.766 |
| 2019 | 0.735 |
| 2018 | 0.66 |
| 2017 | 68.179 |
| 2016 | 1.066 |
| 2015 | 1.043 |
| 2014 | 1.021 |
| 2013 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
