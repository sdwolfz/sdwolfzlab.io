---
title: "How to Use: docker-here"
date: 2020-07-06T22:19:44Z
type: posts
slug: how-to-use-docker-here
series: ["How to Use"]
tags: ["docker", "linux", "productivity"]
---

Source: https://gitlab.com/sdwolfz/docker-here

## Install

```sh
git clone https://gitlab.com/sdwolfz/docker-here.git
cd docker-here
sudo make install
```

## Usage

Instead of doing this:

```sh
docker run --rm -it -u "$(id -u)":"$(id -g)" -v "$PWD":/here -w /here alpine ls
```

Now you can do this:

```sh
docker-here alpine ls
```

You can change both the working directory and the mount path with `-w`:

```sh
docker-here -w /work alpine pwd
```

Run as root with the `-r` flag:

```sh
docker-here -r alpine whoami
```

Or do a dry-run using `-V`:

```sh
docker-here -V alpine ash

# docker run --rm -it -u 1000:1000 -v /home/user:/here -w /here alpine ash
```

And more importantly, it can be used with `envchain`, whereas bash aliases could
not.
