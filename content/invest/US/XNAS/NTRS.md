---
title: "NORTHERN TRUST CORP (NTRS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NORTHERN TRUST CORP</td></tr>
    <tr><td>Symbol</td><td>NTRS</td></tr>
    <tr><td>Web</td><td><a href="https://www.northerntrust.com">www.northerntrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.4 |
| 2020 | 2.8 |
| 2019 | 2.6 |
| 2018 | 1.94 |
| 2017 | 1.6 |
| 2016 | 1.48 |
| 2015 | 1.41 |
| 2014 | 1.3 |
| 2013 | 1.23 |
| 2012 | 1.18 |
| 2011 | 1.12 |
| 2010 | 1.12 |
| 2009 | 1.12 |
| 2008 | 1.12 |
| 2007 | 1.03 |
| 2006 | 0.94 |
| 2005 | 0.86 |
| 2004 | 0.78 |
| 2003 | 0.7 |
| 2002 | 0.68 |
| 2001 | 0.635 |
| 2000 | 0.56 |
| 1999 | 0.855 |
| 1998 | 0.87 |
| 1997 | 0.75 |
| 1996 | 1.29 |
| 1995 | 1.09 |
| 1994 | 0.92 |
| 1993 | 0.775 |
| 1992 | 0.998 |
| 1991 | 0.87 |
| 1990 | 0.97 |
| 1989 | 1.31 |
| 1988 | 0.31 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
