---
title: "QEP RESOURCES INC (QEP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>QEP RESOURCES INC</td></tr>
    <tr><td>Symbol</td><td>QEP</td></tr>
    <tr><td>Web</td><td><a href="http://www.qepres.com">www.qepres.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.02 |
| 2019 | 0.04 |
| 2015 | 0.08 |
| 2014 | 0.08 |
| 2013 | 0.04 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
