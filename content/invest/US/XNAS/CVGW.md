---
title: "CALAVO GROWERS INC (CVGW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CALAVO GROWERS INC</td></tr>
    <tr><td>Symbol</td><td>CVGW</td></tr>
    <tr><td>Web</td><td><a href="https://www.calavo.com">www.calavo.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.15 |
| 2019 | 1.1 |
| 2018 | 1.0 |
| 2017 | 0.95 |
| 2016 | 0.9 |
| 2015 | 0.8 |
| 2014 | 0.75 |
| 2013 | 0.7 |
| 2012 | 0.65 |
| 2011 | 0.55 |
| 2010 | 0.55 |
| 2009 | 0.5 |
| 2008 | 0.35 |
| 2007 | 0.35 |
| 2006 | 0.32 |
| 2005 | 0.32 |
| 2004 | 0.3 |
| 2003 | 0.25 |
| 2002 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
