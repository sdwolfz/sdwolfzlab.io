---
title: "BWX TECHNOLOGIES INC (BWXT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BWX TECHNOLOGIES INC</td></tr>
    <tr><td>Symbol</td><td>BWXT</td></tr>
    <tr><td>Web</td><td><a href="https://www.bwxt.com">www.bwxt.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.42 |
| 2020 | 0.76 |
| 2019 | 0.68 |
| 2018 | 0.64 |
| 2017 | 0.42 |
| 2016 | 0.36 |
| 2015 | 0.22 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
