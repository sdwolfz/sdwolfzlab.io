---
title: "GFL ENVIRONMENTAL INC. (GFL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GFL ENVIRONMENTAL INC.</td></tr>
    <tr><td>Symbol</td><td>GFL</td></tr>
    <tr><td>Web</td><td><a href="https://gflenv.com">gflenv.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.01 |
| 2020 | 0.03 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
