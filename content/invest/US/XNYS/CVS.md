---
title: "CVS HEALTH CORPORATION (CVS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CVS HEALTH CORPORATION</td></tr>
    <tr><td>Symbol</td><td>CVS</td></tr>
    <tr><td>Web</td><td><a href="https://www.cvshealth.com">www.cvshealth.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.0 |
| 2020 | 2.0 |
| 2019 | 2.0 |
| 2018 | 2.0 |
| 2017 | 2.0 |
| 2016 | 1.7 |
| 2015 | 1.4 |
| 2014 | 1.1 |
| 2013 | 0.45 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
