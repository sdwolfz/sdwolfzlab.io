---
title: " (BSAC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BSAC</td></tr>
    <tr><td>Web</td><td><a href="https://www.santander.cl">www.santander.cl</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.687 |
| 2020 | 0.652 |
| 2019 | 0.832 |
| 2018 | 1.123 |
| 2017 | 0.854 |
| 2016 | 0.862 |
| 2015 | 0.903 |
| 2014 | 0.768 |
| 2013 | 0.807 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
