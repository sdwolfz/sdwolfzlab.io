---
title: "NAVIENT CORP (NAVI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NAVIENT CORP</td></tr>
    <tr><td>Symbol</td><td>NAVI</td></tr>
    <tr><td>Web</td><td><a href="https://www.navient.com">www.navient.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.16 |
| 2020 | 0.64 |
| 2019 | 0.64 |
| 2018 | 0.64 |
| 2017 | 0.64 |
| 2016 | 0.64 |
| 2015 | 0.64 |
| 2014 | 0.45 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
