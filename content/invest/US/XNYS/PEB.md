---
title: " (PEB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>PEB</td></tr>
    <tr><td>Web</td><td><a href="https://www.pebblebrookhotels.com">www.pebblebrookhotels.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.01 |
| 2020 | 0.04 |
| 2019 | 1.52 |
| 2018 | 1.52 |
| 2017 | 1.52 |
| 2016 | 1.52 |
| 2015 | 1.24 |
| 2014 | 0.92 |
| 2013 | 0.48 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
