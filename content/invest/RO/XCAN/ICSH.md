---
title: "ICSH SA HUNEDOARA (ICSH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>ICSH SA HUNEDOARA</td></tr>
    <tr><td>Symbol</td><td>ICSH</td></tr>
    <tr><td>Web</td><td><a href="https://www.icsh.ro">www.icsh.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
