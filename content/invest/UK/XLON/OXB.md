---
title: "Oxford Biomedica (OXB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Oxford Biomedica</td></tr>
    <tr><td>Symbol</td><td>OXB</td></tr>
    <tr><td>Web</td><td><a href="https://www.oxb.com">www.oxb.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
