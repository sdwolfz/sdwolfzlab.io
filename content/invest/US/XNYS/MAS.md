---
title: "MASCO CORP (MAS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MASCO CORP</td></tr>
    <tr><td>Symbol</td><td>MAS</td></tr>
    <tr><td>Web</td><td><a href="https://www.masco.com">www.masco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.14 |
| 2020 | 0.545 |
| 2019 | 0.495 |
| 2018 | 0.435 |
| 2017 | 0.405 |
| 2016 | 0.385 |
| 2015 | 0.365 |
| 2014 | 0.33 |
| 2013 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
