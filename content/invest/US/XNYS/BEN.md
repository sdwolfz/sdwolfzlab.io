---
title: "FRANKLIN RESOURCES INC (BEN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FRANKLIN RESOURCES INC</td></tr>
    <tr><td>Symbol</td><td>BEN</td></tr>
    <tr><td>Web</td><td><a href="https://www.franklinresources.com">www.franklinresources.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 1.09 |
| 2019 | 1.05 |
| 2018 | 3.95 |
| 2017 | 0.83 |
| 2016 | 0.74 |
| 2015 | 0.63 |
| 2014 | 0.51 |
| 2013 | 0.317 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
