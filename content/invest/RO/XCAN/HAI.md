---
title: "Holde Agri Invest S.A. - Clasa A (HAI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>Holde Agri Invest S.A. - Clasa A</td></tr>
    <tr><td>Symbol</td><td>HAI</td></tr>
    <tr><td>Web</td><td><a href="https://www.holde.eu">www.holde.eu</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
