---
title: "ACACIA RESEARCH CORP (ACTG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ACACIA RESEARCH CORP</td></tr>
    <tr><td>Symbol</td><td>ACTG</td></tr>
    <tr><td>Web</td><td><a href="https://www.acaciaresearch.com">www.acaciaresearch.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 0.5 |
| 2014 | 0.5 |
| 2013 | 0.375 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
