---
title: "PROTECTIVE INSURANCE CORPORATION (PTVCA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PROTECTIVE INSURANCE CORPORATION</td></tr>
    <tr><td>Symbol</td><td>PTVCA</td></tr>
    <tr><td>Web</td><td><a href="https://www.protectiveinsurance.com">www.protectiveinsurance.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.4 |
| 2019 | 0.4 |
| 2018 | 1.12 |
| 2017 | 1.08 |
| 2016 | 1.04 |
| 2015 | 1.0 |
| 2014 | 1.0 |
| 2013 | 1.0 |
| 2012 | 1.0 |
| 2011 | 1.0 |
| 2010 | 2.25 |
| 2009 | 1.0 |
| 2008 | 1.0 |
| 2007 | 1.65 |
| 2006 | 2.55 |
| 2005 | 0.95 |
| 2004 | 2.05 |
| 2003 | 0.65 |
| 2002 | 0.4 |
| 2001 | 0.4 |
| 2000 | 0.4 |
| 1999 | 0.4 |
| 1998 | 0.4 |
| 1997 | 0.4 |
| 1996 | 0.36 |
| 1995 | 0.3 |
| 1994 | 0.33 |
| 1993 | 0.55 |
| 1992 | 0.525 |
| 1991 | 0.3 |
| 1990 | 0.3 |
| 1989 | 0.225 |
| 1988 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
