---
title: "CALIFORNIA WATER SERVICE GROUP (CWT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CALIFORNIA WATER SERVICE GROUP</td></tr>
    <tr><td>Symbol</td><td>CWT</td></tr>
    <tr><td>Web</td><td><a href="https://www.calwatergroup.com">www.calwatergroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.46 |
| 2020 | 0.852 |
| 2019 | 0.792 |
| 2018 | 0.752 |
| 2017 | 0.72 |
| 2016 | 0.688 |
| 2015 | 0.672 |
| 2014 | 0.648 |
| 2013 | 0.32 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
