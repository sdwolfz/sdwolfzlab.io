---
title: "TR Property Investment Trust (TRY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>TR Property Investment Trust</td></tr>
    <tr><td>Symbol</td><td>TRY</td></tr>
    <tr><td>Web</td><td><a href="https://www.trproperty.com">www.trproperty.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 14.0 |
| 2019 | 13.5 |
| 2018 | 12.2 |
| 2017 | 10.5 |
| 2016 | 8.35 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
