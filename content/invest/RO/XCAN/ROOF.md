---
title: "ROMANOFIR SA TALMACIU (ROOF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>ROMANOFIR SA TALMACIU</td></tr>
    <tr><td>Symbol</td><td>ROOF</td></tr>
    <tr><td>Web</td><td><a href="https://www.romanofir.ro">www.romanofir.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.5 |
| 2019 | 0.75 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
