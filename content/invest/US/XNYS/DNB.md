---
title: "DUN & BRADSTREET HLDGS INC (DNB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DUN & BRADSTREET HLDGS INC</td></tr>
    <tr><td>Symbol</td><td>DNB</td></tr>
    <tr><td>Web</td><td><a href="https://www.dnb.com">www.dnb.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 1.566 |
| 2017 | 2.008 |
| 2016 | 1.928 |
| 2015 | 1.852 |
| 2014 | 1.76 |
| 2013 | 0.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
