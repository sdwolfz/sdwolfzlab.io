---
title: "BC LATINOAMERICANO COM EX SA BLADEX (BLX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BC LATINOAMERICANO COM EX SA BLADEX</td></tr>
    <tr><td>Symbol</td><td>BLX</td></tr>
    <tr><td>Web</td><td><a href="https://www.bladex.com">www.bladex.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.5 |
| 2020 | 1.135 |
| 2019 | 1.54 |
| 2018 | 1.54 |
| 2017 | 1.54 |
| 2016 | 1.54 |
| 2015 | 1.155 |
| 2014 | 1.785 |
| 2013 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
