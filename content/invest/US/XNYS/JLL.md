---
title: "JONES LANG LASALLE INC (JLL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>JONES LANG LASALLE INC</td></tr>
    <tr><td>Symbol</td><td>JLL</td></tr>
    <tr><td>Web</td><td><a href="https://www.jll.com">www.jll.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.86 |
| 2018 | 0.82 |
| 2017 | 0.72 |
| 2016 | 0.64 |
| 2015 | 0.56 |
| 2014 | 0.48 |
| 2013 | 0.44 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
