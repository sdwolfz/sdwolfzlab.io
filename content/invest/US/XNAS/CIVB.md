---
title: "CIVISTA BANCSHARES INC (CIVB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CIVISTA BANCSHARES INC</td></tr>
    <tr><td>Symbol</td><td>CIVB</td></tr>
    <tr><td>Web</td><td><a href="https://www.civb.com">www.civb.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.24 |
| 2020 | 0.44 |
| 2019 | 0.42 |
| 2018 | 0.32 |
| 2017 | 0.25 |
| 2016 | 0.22 |
| 2015 | 0.2 |
| 2014 | 0.19 |
| 2013 | 0.15 |
| 2012 | 0.12 |
| 2011 | 0.03 |
| 2009 | 0.25 |
| 2008 | 0.91 |
| 2007 | 1.12 |
| 2006 | 1.12 |
| 2005 | 1.12 |
| 2004 | 1.08 |
| 2003 | 1.3 |
| 2002 | 1.3 |
| 2001 | 1.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
