---
title: "MIDDLEFIELD BANC CORP(US) (MBCN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MIDDLEFIELD BANC CORP(US)</td></tr>
    <tr><td>Symbol</td><td>MBCN</td></tr>
    <tr><td>Web</td><td><a href="https://www.middlefieldbank.bank">www.middlefieldbank.bank</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.16 |
| 2020 | 0.6 |
| 2019 | 0.99 |
| 2018 | 1.17 |
| 2017 | 1.08 |
| 2016 | 1.08 |
| 2015 | 1.07 |
| 2014 | 1.04 |
| 2013 | 1.04 |
| 2012 | 1.04 |
| 2011 | 1.04 |
| 2010 | 1.04 |
| 2009 | 1.04 |
| 2008 | 1.03 |
| 2007 | 0.97 |
| 2006 | 0.71 |
| 2005 | 0.675 |
| 2004 | 0.64 |
| 2003 | 0.61 |
| 2002 | 0.57 |
| 2001 | 0.42 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
