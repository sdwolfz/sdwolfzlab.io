---
title: "BRINKER INTERNATIONAL (EAT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BRINKER INTERNATIONAL</td></tr>
    <tr><td>Symbol</td><td>EAT</td></tr>
    <tr><td>Web</td><td><a href="https://www.brinker.com">www.brinker.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.38 |
| 2019 | 1.52 |
| 2018 | 1.52 |
| 2017 | 1.44 |
| 2016 | 1.32 |
| 2015 | 1.2 |
| 2014 | 1.04 |
| 2013 | 0.68 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
