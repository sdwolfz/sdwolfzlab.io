---
title: "DIVERSIFIED GAS & OIL PLC ORD 1P (DGOC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>DIVERSIFIED GAS & OIL PLC ORD 1P</td></tr>
    <tr><td>Symbol</td><td>DGOC</td></tr>
    <tr><td>Web</td><td><a href="http://www.dgoc.com">www.dgoc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 13.92 |
| 2018 | 11.23 |
| 2017 | 5.44 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
