---
title: "GENUINE PARTS CO (GPC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GENUINE PARTS CO</td></tr>
    <tr><td>Symbol</td><td>GPC</td></tr>
    <tr><td>Web</td><td><a href="https://www.genpt.com">www.genpt.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.63 |
| 2020 | 3.16 |
| 2019 | 3.048 |
| 2018 | 2.88 |
| 2017 | 2.7 |
| 2016 | 2.632 |
| 2015 | 2.46 |
| 2014 | 2.3 |
| 2013 | 1.614 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
