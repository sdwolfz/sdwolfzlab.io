---
title: " (WMC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>WMC</td></tr>
    <tr><td>Web</td><td><a href="https://www.westernassetmcc.com">www.westernassetmcc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.06 |
| 2020 | 0.11 |
| 2019 | 1.24 |
| 2018 | 1.24 |
| 2017 | 1.24 |
| 2016 | 1.38 |
| 2015 | 2.49 |
| 2014 | 2.74 |
| 2013 | 1.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
