---
title: "Chemring (CHG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Chemring</td></tr>
    <tr><td>Symbol</td><td>CHG</td></tr>
    <tr><td>Web</td><td><a href="https://www.chemring.co.uk">www.chemring.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 3.9 |
| 2019 | 3.6 |
| 2018 | 3.3 |
| 2017 | 3.0 |
| 2016 | 1.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
