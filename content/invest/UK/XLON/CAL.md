---
title: "Capital & Regional (CAL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Capital & Regional</td></tr>
    <tr><td>Symbol</td><td>CAL</td></tr>
    <tr><td>Web</td><td><a href="https://www.capreg.com">www.capreg.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 21.0 |
| 2018 | 24.2 |
| 2017 | 36.4 |
| 2016 | 33.9 |
| 2015 | 31.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
