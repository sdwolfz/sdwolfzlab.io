---
title: "BIG 5 SPORTING GOODS CORP (BGFV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BIG 5 SPORTING GOODS CORP</td></tr>
    <tr><td>Symbol</td><td>BGFV</td></tr>
    <tr><td>Web</td><td><a href="https://www.big5sportinggoods.com">www.big5sportinggoods.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.33 |
| 2020 | 0.25 |
| 2019 | 0.2 |
| 2018 | 0.5 |
| 2017 | 0.6 |
| 2016 | 0.525 |
| 2015 | 0.4 |
| 2014 | 0.4 |
| 2013 | 0.4 |
| 2012 | 0.3 |
| 2011 | 0.3 |
| 2010 | 0.2 |
| 2009 | 0.2 |
| 2008 | 0.36 |
| 2007 | 0.36 |
| 2006 | 0.34 |
| 2005 | 0.28 |
| 2004 | 0.07 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
