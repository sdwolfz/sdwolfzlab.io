---
title: "MSC INDUSTRIAL DIRECT CO (MSM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MSC INDUSTRIAL DIRECT CO</td></tr>
    <tr><td>Symbol</td><td>MSM</td></tr>
    <tr><td>Web</td><td><a href="https://www.mscdirect.com">www.mscdirect.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.5 |
| 2020 | 10.75 |
| 2019 | 2.76 |
| 2018 | 2.37 |
| 2017 | 1.83 |
| 2016 | 1.74 |
| 2015 | 1.63 |
| 2014 | 1.39 |
| 2013 | 0.63 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
