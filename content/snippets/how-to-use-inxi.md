---
title: "How to Use: inxi"
date: 2020-07-21T18:11:59Z
type: posts
slug: how-to-use-inxi
series: ["How to Use"]
tags: ["linux", "hardware", "terminal"]
---

Want to show somebody your hardware/OS specs? execute:

```sh
inxi -Fxxxz --no-host
```

And paste the output somehere they can see it.
