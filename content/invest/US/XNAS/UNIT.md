---
title: " (UNIT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>UNIT</td></tr>
    <tr><td>Web</td><td><a href="https://www.uniti.com">www.uniti.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 0.6 |
| 2019 | 0.37 |
| 2018 | 2.4 |
| 2017 | 2.4 |
| 2016 | 2.4 |
| 2015 | 1.642 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
