---
title: "Syncona (SYNC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Syncona</td></tr>
    <tr><td>Symbol</td><td>SYNC</td></tr>
    <tr><td>Web</td><td><a href="https://www.synconaltd.com">www.synconaltd.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 2.3 |
| 2018 | 2.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
