---
title: "METHODE ELECTRONICS INC (MEI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>METHODE ELECTRONICS INC</td></tr>
    <tr><td>Symbol</td><td>MEI</td></tr>
    <tr><td>Web</td><td><a href="https://www.methode.com">www.methode.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.22 |
| 2020 | 0.44 |
| 2019 | 0.44 |
| 2018 | 0.44 |
| 2017 | 0.36 |
| 2016 | 0.36 |
| 2015 | 0.36 |
| 2014 | 0.34 |
| 2013 | 0.14 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
