---
title: "Card Factory (CARD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Card Factory</td></tr>
    <tr><td>Symbol</td><td>CARD</td></tr>
    <tr><td>Web</td><td><a href="https://www.cardfactory.eu.com">www.cardfactory.eu.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 7.9 |
| 2019 | 14.3 |
| 2018 | 24.3 |
| 2017 | 24.1 |
| 2016 | 23.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
