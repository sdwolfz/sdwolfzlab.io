---
title: "MCKESSON CORPORATION (MCK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MCKESSON CORPORATION</td></tr>
    <tr><td>Symbol</td><td>MCK</td></tr>
    <tr><td>Web</td><td><a href="https://www.mckesson.com">www.mckesson.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.84 |
| 2020 | 1.66 |
| 2019 | 1.6 |
| 2018 | 1.46 |
| 2017 | 1.24 |
| 2016 | 1.12 |
| 2015 | 1.04 |
| 2014 | 0.96 |
| 2013 | 0.48 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
