---
title: " (SIFY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SIFY</td></tr>
    <tr><td>Web</td><td><a href="https://www.sifytechnologies.com">www.sifytechnologies.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.017 |
| 2018 | 0.017 |
| 2017 | 0.019 |
| 2016 | 0.015 |
| 2015 | 0.016 |
| 2014 | 0.017 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
