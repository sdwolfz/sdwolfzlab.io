---
title: "BGC PARTNERS INC (BGCP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BGC PARTNERS INC</td></tr>
    <tr><td>Symbol</td><td>BGCP</td></tr>
    <tr><td>Web</td><td><a href="https://www.bgcpartners.com">www.bgcpartners.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.02 |
| 2020 | 0.17 |
| 2019 | 0.56 |
| 2018 | 0.72 |
| 2017 | 0.7 |
| 2016 | 0.62 |
| 2015 | 0.54 |
| 2014 | 0.48 |
| 2013 | 0.48 |
| 2012 | 0.63 |
| 2011 | 0.65 |
| 2010 | 0.48 |
| 2009 | 0.3 |
| 2008 | 0.23 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
