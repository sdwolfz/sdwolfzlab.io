---
title: "AXIS CAPITAL HOLDINGS LTD (AXS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AXIS CAPITAL HOLDINGS LTD</td></tr>
    <tr><td>Symbol</td><td>AXS</td></tr>
    <tr><td>Web</td><td><a href="https://www.axiscapital.com">www.axiscapital.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.42 |
| 2020 | 1.65 |
| 2019 | 1.61 |
| 2018 | 1.57 |
| 2017 | 1.53 |
| 2016 | 1.43 |
| 2015 | 1.22 |
| 2014 | 1.1 |
| 2013 | 0.77 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
