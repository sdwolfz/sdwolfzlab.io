---
title: "MAGIC SOFTWARE ENTERPRISES (MGIC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MAGIC SOFTWARE ENTERPRISES</td></tr>
    <tr><td>Symbol</td><td>MGIC</td></tr>
    <tr><td>Web</td><td><a href="https://www.magicsoftware.com">www.magicsoftware.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.21 |
| 2020 | 0.255 |
| 2019 | 0.306 |
| 2018 | 0.285 |
| 2017 | 0.215 |
| 2016 | 0.175 |
| 2015 | 0.176 |
| 2014 | 0.215 |
| 2013 | 0.21 |
| 2012 | 0.1 |
| 2010 | 0.5 |
| 2003 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
