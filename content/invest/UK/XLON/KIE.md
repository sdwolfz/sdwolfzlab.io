---
title: "Kier (KIE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Kier</td></tr>
    <tr><td>Symbol</td><td>KIE</td></tr>
    <tr><td>Web</td><td><a href="https://www.kier.co.uk">www.kier.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 4.9 |
| 2018 | 45.54 |
| 2017 | 44.55 |
| 2016 | 42.57 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
