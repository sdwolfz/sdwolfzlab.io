---
title: "Docker Compose project starter"
date: 2020-12-23T14:39:29Z
type: posts
slug: docker-compose-project-starter
tags: ["docker"]
draft: true
---

## Prerequisites

Use the docker-compose nginx proxy setup.

## Project

Set up your networks, volumes, and logging like this:

```yml
#-------------------------------------------------------------------------------
# Version
#-------------------------------------------------------------------------------

version: '2.4'

#-------------------------------------------------------------------------------
# Networks
#-------------------------------------------------------------------------------

networks:
  backend_network:
    driver: bridge
  frontend_network:
    external: true
    name: nginx-reverse-proxy-network

#-------------------------------------------------------------------------------
# Volumes
#-------------------------------------------------------------------------------

volumes:
  web_files:
    driver: local
  web_log:
    driver: local

  postgres_data:
    driver: local
  postgres_upgrade:
    driver: local

#-------------------------------------------------------------------------------
# Logging
#-------------------------------------------------------------------------------

x-default-logging: &logging
  logging:
    options:
      max-size: '12m'
      max-file: '5'
    driver: json-file
```

2. Create your env files:

```sh
mkdir -p ./config/environment
touch ./config/environment/{vhost,web,postgres}.env
```

3. Create your Dockerfiles:

```sh
mkdir -p ./docker/{web,postgres}
touch ./docker/{web,postgres}/{Dockerfile,entry}
```

4. And now you can define your services!

```yml
#-------------------------------------------------------------------------------
# Services
#-------------------------------------------------------------------------------

services:

  #-----------------------------------------------------------------------------
  # Web

  web:
    build:
      args:
        HOST_USER_UID: ${HOST_USER_UID}
        HOST_USER_GID: ${HOST_USER_GID}
      context: .
      dockerfile: ./docker/web/Dockerfile
    command: bundle exec rails s
    depends_on:
      postgres:
        condition: service_healthy
    env_file: ./config/environment/vhost.env
    expose:
      - "9292"
    <<: *logging
    mem_limit: '800m'
    memswap_limit: '1g'
    networks:
      - backend_network
      - frontend_network
    restart: on-failure
    ulimits:
      nproc: 10000
      nofile: 90000
    user: notroot
    volumes:
      - web_files:/work/files
      - web_log:/work/log
      - ./config/environment/web.env:/environment/web.env
    working_dir: /work

  #-----------------------------------------------------------------------------
  # Postgres

  postgres:
    build:
      args:
        HOST_USER_UID: ${HOST_USER_UID}
        HOST_USER_GID: ${HOST_USER_GID}
      context: .
      dockerfile: ./docker/postgres/Dockerfile
    healthcheck:
      test: pg_isready -U notroot -h localhost -p 5432 -d notroot || exit 1
      interval: "10s"
      retries: 20
      start_period: "10s"
      timeout: "7s"
    <<: *logging
    mem_limit: '500m'
    memswap_limit: '1g'
    networks:
      - backend_network
    restart: on-failure
    ulimits:
      nproc: 10000
      nofile: 50000
    volumes:
      - postgres_data:/var/lib/postgresql/data
      - postgres_upgrade:/var/lib/postgresql/upgrade
      - ./config/environment/postgres.env:/environment/postgres.env
```

5. See how to set up individual services here:
* ruby
* postgres
