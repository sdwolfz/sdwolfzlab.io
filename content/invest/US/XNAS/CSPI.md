---
title: "CSP INC (CSPI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CSP INC</td></tr>
    <tr><td>Symbol</td><td>CSPI</td></tr>
    <tr><td>Web</td><td><a href="https://www.cspi.com">www.cspi.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.15 |
| 2019 | 0.75 |
| 2018 | 0.37 |
| 2017 | 0.55 |
| 2016 | 0.33 |
| 2015 | 0.44 |
| 2014 | 0.44 |
| 2013 | 0.3 |
| 2012 | 0.42 |
| 2000 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
