---
title: "SELECT MEDICAL HOLDINGS CORP (SEM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SELECT MEDICAL HOLDINGS CORP</td></tr>
    <tr><td>Symbol</td><td>SEM</td></tr>
    <tr><td>Web</td><td><a href="https://www.selectmedicalholdings.com">www.selectmedicalholdings.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 0.1 |
| 2014 | 0.4 |
| 2013 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
