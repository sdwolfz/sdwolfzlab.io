---
title: "MECANICA CODLEA SA (MEOY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>MECANICA CODLEA SA</td></tr>
    <tr><td>Symbol</td><td>MEOY</td></tr>
    <tr><td>Web</td><td><a href="https://www.mecod.ro">www.mecod.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.007175 |
| 2019 | 0.00523 |
| 2018 | 0.00508 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
