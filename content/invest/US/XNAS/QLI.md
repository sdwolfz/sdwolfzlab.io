---
title: "QILIAN INTERNATIONAL HLDG GROUP LTD (QLI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>QILIAN INTERNATIONAL HLDG GROUP LTD</td></tr>
    <tr><td>Symbol</td><td>QLI</td></tr>
    <tr><td>Web</td><td><a href="https://www.qlsyy.net">www.qlsyy.net</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
