---
title: "WORTHINGTON INDUSTRIES INC (WOR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>WORTHINGTON INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>WOR</td></tr>
    <tr><td>Web</td><td><a href="https://www.worthingtonindustries.com">www.worthingtonindustries.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.53 |
| 2020 | 0.98 |
| 2019 | 0.94 |
| 2018 | 0.88 |
| 2017 | 0.82 |
| 2016 | 0.78 |
| 2015 | 0.74 |
| 2014 | 0.84 |
| 2013 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
