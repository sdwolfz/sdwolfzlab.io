---
title: "WEST BANCORPORATION INC (WTBA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>WEST BANCORPORATION INC</td></tr>
    <tr><td>Symbol</td><td>WTBA</td></tr>
    <tr><td>Web</td><td><a href="https://www.westbankstrong.com">www.westbankstrong.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.46 |
| 2020 | 0.84 |
| 2019 | 0.83 |
| 2018 | 0.78 |
| 2017 | 0.71 |
| 2016 | 0.67 |
| 2015 | 0.62 |
| 2014 | 0.49 |
| 2013 | 0.42 |
| 2012 | 0.36 |
| 2011 | 0.17 |
| 2010 | 0.05 |
| 2009 | 0.09 |
| 2008 | 0.64 |
| 2007 | 0.64 |
| 2006 | 0.64 |
| 2005 | 0.64 |
| 2004 | 0.64 |
| 2003 | 0.64 |
| 2002 | 0.62 |
| 2001 | 0.15 |
| 2000 | 0.29 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
