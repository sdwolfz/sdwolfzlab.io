---
title: "BADGER METER INC (BMI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BADGER METER INC</td></tr>
    <tr><td>Symbol</td><td>BMI</td></tr>
    <tr><td>Web</td><td><a href="https://www.badgermeter.com">www.badgermeter.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.36 |
| 2020 | 0.7 |
| 2019 | 0.64 |
| 2018 | 0.56 |
| 2017 | 0.49 |
| 2016 | 0.63 |
| 2015 | 0.78 |
| 2014 | 0.74 |
| 2013 | 0.36 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
