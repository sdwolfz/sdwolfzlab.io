---
title: "PIPER SANDLER COMPANIES (PIPR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PIPER SANDLER COMPANIES</td></tr>
    <tr><td>Symbol</td><td>PIPR</td></tr>
    <tr><td>Web</td><td><a href="https://www.pipersandler.com">www.pipersandler.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.7 |
| 2020 | 2.0 |
| 2019 | 0.375 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
