---
title: "MUDRICK CAPITAL ACQUISITION CORP II (MUDS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MUDRICK CAPITAL ACQUISITION CORP II</td></tr>
    <tr><td>Symbol</td><td>MUDS</td></tr>
    <tr><td>Web</td><td><a href="https://www.mudrickcapital.com">www.mudrickcapital.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
