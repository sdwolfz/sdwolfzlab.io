---
title: "COHEN & COMPANY INC (COHN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COHEN & COMPANY INC</td></tr>
    <tr><td>Symbol</td><td>COHN</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.4 |
| 2018 | 0.8 |
| 2017 | 0.26 |
| 2016 | 0.08 |
| 2015 | 0.08 |
| 2014 | 0.08 |
| 2013 | 0.08 |
| 2012 | 0.08 |
| 2011 | 0.2 |
| 2010 | 0.1 |
| 2008 | 0.5 |
| 2007 | 1.23 |
| 2006 | 0.37 |
| 2005 | 0.44 |
| 2004 | 0.065 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
