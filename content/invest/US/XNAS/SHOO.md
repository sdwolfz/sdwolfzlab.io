---
title: "MADDEN(STEVEN) (SHOO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MADDEN(STEVEN)</td></tr>
    <tr><td>Symbol</td><td>SHOO</td></tr>
    <tr><td>Web</td><td><a href="https://www.stevemadden.com">www.stevemadden.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 0.15 |
| 2019 | 0.57 |
| 2018 | 0.74 |
| 2006 | 1.0 |
| 2005 | 1.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
