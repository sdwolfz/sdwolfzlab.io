---
title: "TEXAS INSTRUMENTS INC (TXN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TEXAS INSTRUMENTS INC</td></tr>
    <tr><td>Symbol</td><td>TXN</td></tr>
    <tr><td>Web</td><td><a href="https://www.ti.com">www.ti.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.04 |
| 2020 | 3.72 |
| 2019 | 3.21 |
| 2018 | 2.63 |
| 2017 | 2.12 |
| 2016 | 1.64 |
| 2015 | 1.4 |
| 2014 | 1.24 |
| 2013 | 1.07 |
| 2012 | 0.72 |
| 2011 | 0.56 |
| 2010 | 0.49 |
| 2009 | 0.45 |
| 2008 | 0.41 |
| 2007 | 0.3 |
| 2006 | 0.13 |
| 2005 | 0.105 |
| 2004 | 0.089 |
| 2003 | 0.085 |
| 2002 | 0.085 |
| 2001 | 0.085 |
| 2000 | 0.021 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
