---
title: " (SLG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SLG</td></tr>
    <tr><td>Web</td><td><a href="https://www.slgreen.com">www.slgreen.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.212 |
| 2020 | 4.725 |
| 2019 | 3.435 |
| 2018 | 3.289 |
| 2017 | 3.138 |
| 2016 | 2.935 |
| 2015 | 2.52 |
| 2014 | 2.1 |
| 2013 | 1.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
