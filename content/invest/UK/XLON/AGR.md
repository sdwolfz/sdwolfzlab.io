---
title: "Assura (AGR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Assura</td></tr>
    <tr><td>Symbol</td><td>AGR</td></tr>
    <tr><td>Web</td><td><a href="https://www.assuraplc.com">www.assuraplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.75 |
| 2019 | 2.65 |
| 2018 | 2.46 |
| 2017 | 2.25 |
| 2016 | 2.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
