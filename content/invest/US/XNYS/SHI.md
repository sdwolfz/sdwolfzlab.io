---
title: " (SHI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SHI</td></tr>
    <tr><td>Web</td><td><a href="https://www.spc.com.cn">www.spc.com.cn</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.325 |
| 2020 | 1.471 |
| 2019 | 3.221 |
| 2018 | 4.158 |
| 2017 | 3.257 |
| 2016 | 1.319 |
| 2014 | 0.711 |
| 2013 | 0.445 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
