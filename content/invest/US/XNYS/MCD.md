---
title: "MCDONALD'S CORPORATION (MCD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MCDONALD'S CORPORATION</td></tr>
    <tr><td>Symbol</td><td>MCD</td></tr>
    <tr><td>Web</td><td><a href="https://www.investor.mcdonalds.com">www.investor.mcdonalds.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.29 |
| 2020 | 5.04 |
| 2019 | 4.73 |
| 2018 | 4.19 |
| 2017 | 3.83 |
| 2016 | 3.61 |
| 2015 | 3.44 |
| 2014 | 3.28 |
| 2013 | 1.58 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
