---
title: "WESTLAKE CHEMICAL CORPORATION (WLK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>WESTLAKE CHEMICAL CORPORATION</td></tr>
    <tr><td>Symbol</td><td>WLK</td></tr>
    <tr><td>Web</td><td><a href="https://www.westlake.com">www.westlake.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.27 |
| 2020 | 1.064 |
| 2019 | 1.024 |
| 2018 | 0.92 |
| 2017 | 0.802 |
| 2016 | 0.746 |
| 2015 | 0.694 |
| 2014 | 0.582 |
| 2013 | 0.638 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
