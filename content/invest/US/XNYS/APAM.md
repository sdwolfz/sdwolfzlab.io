---
title: "ARTISAN PARTNERS ASSET MGMT INC (APAM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ARTISAN PARTNERS ASSET MGMT INC</td></tr>
    <tr><td>Symbol</td><td>APAM</td></tr>
    <tr><td>Web</td><td><a href="https://www.artisanpartners.com">www.artisanpartners.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.16 |
| 2020 | 3.39 |
| 2019 | 3.39 |
| 2018 | 3.19 |
| 2017 | 2.76 |
| 2016 | 2.8 |
| 2015 | 2.4 |
| 2014 | 2.2 |
| 2013 | 0.86 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
