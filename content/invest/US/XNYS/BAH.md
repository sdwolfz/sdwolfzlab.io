---
title: "BOOZ ALLEN HAMILTON HLDG CORP (BAH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BOOZ ALLEN HAMILTON HLDG CORP</td></tr>
    <tr><td>Symbol</td><td>BAH</td></tr>
    <tr><td>Web</td><td><a href="https://www.boozallen.com">www.boozallen.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.37 |
| 2020 | 1.24 |
| 2019 | 0.96 |
| 2018 | 0.76 |
| 2017 | 0.68 |
| 2016 | 0.6 |
| 2015 | 0.52 |
| 2014 | 0.43 |
| 2013 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
