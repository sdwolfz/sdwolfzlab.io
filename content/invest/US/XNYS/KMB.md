---
title: "KIMBERLY CLARK CORP (KMB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>KIMBERLY CLARK CORP</td></tr>
    <tr><td>Symbol</td><td>KMB</td></tr>
    <tr><td>Web</td><td><a href="https://www.kimberly-clark.com">www.kimberly-clark.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.28 |
| 2020 | 4.28 |
| 2019 | 4.12 |
| 2018 | 4.0 |
| 2017 | 3.88 |
| 2016 | 3.68 |
| 2015 | 3.52 |
| 2014 | 3.36 |
| 2013 | 1.62 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
