---
title: "KEURIG DR PEPPER INC (KDP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>KEURIG DR PEPPER INC</td></tr>
    <tr><td>Symbol</td><td>KDP</td></tr>
    <tr><td>Web</td><td><a href="https://www.keurigdrpepper.com">www.keurigdrpepper.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 0.6 |
| 2019 | 0.6 |
| 2018 | 104.48 |
| 2017 | 2.32 |
| 2016 | 2.12 |
| 2015 | 1.92 |
| 2014 | 1.64 |
| 2013 | 1.52 |
| 2012 | 1.36 |
| 2011 | 1.21 |
| 2010 | 0.9 |
| 2009 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
