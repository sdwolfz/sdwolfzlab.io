---
title: "MOVADO GROUP INC (MOV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MOVADO GROUP INC</td></tr>
    <tr><td>Symbol</td><td>MOV</td></tr>
    <tr><td>Web</td><td><a href="https://www.movadogroup.com">www.movadogroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2019 | 0.8 |
| 2018 | 0.8 |
| 2017 | 0.52 |
| 2016 | 0.52 |
| 2015 | 0.44 |
| 2014 | 0.4 |
| 2013 | 0.21 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
