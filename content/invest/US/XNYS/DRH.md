---
title: " (DRH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>DRH</td></tr>
    <tr><td>Web</td><td><a href="https://www.drhc.com">www.drhc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.625 |
| 2018 | 0.375 |
| 2017 | 0.5 |
| 2016 | 0.5 |
| 2015 | 0.5 |
| 2014 | 0.408 |
| 2013 | 0.255 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
