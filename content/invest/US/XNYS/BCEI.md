---
title: "BONANZA CREEK ENERGY INC (BCEI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BONANZA CREEK ENERGY INC</td></tr>
    <tr><td>Symbol</td><td>BCEI</td></tr>
    <tr><td>Web</td><td><a href="https://www.bonanzacrk.com">www.bonanzacrk.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.35 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
