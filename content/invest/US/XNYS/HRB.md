---
title: "BLOCK(H & R) INC (HRB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BLOCK(H & R) INC</td></tr>
    <tr><td>Symbol</td><td>HRB</td></tr>
    <tr><td>Web</td><td><a href="https://www.hrblock.com">www.hrblock.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.26 |
| 2020 | 1.04 |
| 2019 | 1.03 |
| 2018 | 0.99 |
| 2017 | 0.94 |
| 2016 | 0.86 |
| 2015 | 0.8 |
| 2014 | 0.8 |
| 2013 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
