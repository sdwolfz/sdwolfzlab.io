---
title: "TIPTREE INC (TIPT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TIPTREE INC</td></tr>
    <tr><td>Symbol</td><td>TIPT</td></tr>
    <tr><td>Web</td><td><a href="https://www.tiptreeinc.com">www.tiptreeinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.08 |
| 2020 | 0.16 |
| 2019 | 0.155 |
| 2018 | 0.135 |
| 2017 | 0.12 |
| 2016 | 0.1 |
| 2015 | 0.1 |
| 2013 | 0.175 |
| 2012 | 0.54 |
| 2011 | 0.405 |
| 2009 | 0.68 |
| 2008 | 0.68 |
| 2007 | 0.17 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
