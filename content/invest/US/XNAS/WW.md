---
title: "WW INTERNATIONAL INC (WW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>WW INTERNATIONAL INC</td></tr>
    <tr><td>Symbol</td><td>WW</td></tr>
    <tr><td>Web</td><td><a href="https://corporate.ww.com">corporate.ww.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2013 | 0.525 |
| 2012 | 0.7 |
| 2011 | 0.7 |
| 2010 | 0.7 |
| 2009 | 0.7 |
| 2008 | 0.7 |
| 2007 | 0.7 |
| 2006 | 0.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
