---
title: "Allianz Technology Trust (ATT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Allianz Technology Trust</td></tr>
    <tr><td>Symbol</td><td>ATT</td></tr>
    <tr><td>Web</td><td><a href="https://www.allianztechnologytrust.com">www.allianztechnologytrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
