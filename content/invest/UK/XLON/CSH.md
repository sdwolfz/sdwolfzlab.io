---
title: "Civitas Social Housing (CSH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Civitas Social Housing</td></tr>
    <tr><td>Symbol</td><td>CSH</td></tr>
    <tr><td>Web</td><td><a href="https://civitassocialhousing.com">civitassocialhousing.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 5.3 |
| 2019 | 5.08 |
| 2018 | 3.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
