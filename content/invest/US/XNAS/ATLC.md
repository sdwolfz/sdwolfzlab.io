---
title: "ATLANTICUS HLDGS CORP (ATLC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ATLANTICUS HLDGS CORP</td></tr>
    <tr><td>Symbol</td><td>ATLC</td></tr>
    <tr><td>Web</td><td><a href="https://www.atlanticus.com">www.atlanticus.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2009 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
