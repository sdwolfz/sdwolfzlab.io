---
title: "MARATHON PETROLEUM CORP (MPC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MARATHON PETROLEUM CORP</td></tr>
    <tr><td>Symbol</td><td>MPC</td></tr>
    <tr><td>Web</td><td><a href="https://www.marathonpetroleum.com">www.marathonpetroleum.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.16 |
| 2020 | 2.32 |
| 2019 | 2.12 |
| 2018 | 1.84 |
| 2017 | 1.52 |
| 2016 | 1.36 |
| 2015 | 1.39 |
| 2014 | 1.84 |
| 2013 | 0.84 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
