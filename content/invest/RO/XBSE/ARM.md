---
title: "ARMATURA S.A. (ARM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>ARMATURA S.A.</td></tr>
    <tr><td>Symbol</td><td>ARM</td></tr>
    <tr><td>Web</td><td><a href="https://www.armatura.ro">www.armatura.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2004 | 0.007 |
| 2003 | 0.014 |
| 2002 | 0.014 |
| 2001 | 0.023 |
| 2000 | 0.016 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
