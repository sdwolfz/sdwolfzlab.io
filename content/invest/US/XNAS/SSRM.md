---
title: "SSR MINING INC (SSRM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SSR MINING INC</td></tr>
    <tr><td>Symbol</td><td>SSRM</td></tr>
    <tr><td>Web</td><td><a href="https://www.ssrmining.com">www.ssrmining.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
