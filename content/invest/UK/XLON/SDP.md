---
title: "Schroder AsiaPacific Fund (SDP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Schroder AsiaPacific Fund</td></tr>
    <tr><td>Symbol</td><td>SDP</td></tr>
    <tr><td>Web</td><td><a href="https://www.schroders.com">www.schroders.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 8.0 |
| 2019 | 9.7 |
| 2018 | 9.5 |
| 2017 | 5.6 |
| 2016 | 4.75 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
