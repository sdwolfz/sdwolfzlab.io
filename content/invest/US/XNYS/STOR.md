---
title: " (STOR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>STOR</td></tr>
    <tr><td>Web</td><td><a href="https://www.storecapital.com">www.storecapital.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.36 |
| 2020 | 1.42 |
| 2019 | 1.36 |
| 2018 | 1.28 |
| 2017 | 1.2 |
| 2016 | 1.12 |
| 2015 | 1.04 |
| 2014 | 0.114 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
