---
title: "GCM GROSVENOR INC (GCMG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GCM GROSVENOR INC</td></tr>
    <tr><td>Symbol</td><td>GCMG</td></tr>
    <tr><td>Web</td><td><a href="https://www.gcmgrosvenor.com">www.gcmgrosvenor.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.14 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
