---
title: "ALLEGION PLC (ALLE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ALLEGION PLC</td></tr>
    <tr><td>Symbol</td><td>ALLE</td></tr>
    <tr><td>Web</td><td><a href="https://www.allegion.com">www.allegion.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.72 |
| 2020 | 1.28 |
| 2019 | 1.08 |
| 2018 | 0.84 |
| 2017 | 0.64 |
| 2016 | 0.48 |
| 2015 | 0.4 |
| 2014 | 0.32 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
