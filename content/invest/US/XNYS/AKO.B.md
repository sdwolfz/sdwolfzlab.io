---
title: " (AKO.B)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>AKO.B</td></tr>
    <tr><td>Web</td><td><a href="https://www.koandina.com">www.koandina.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.153 |
| 2020 | 0.554 |
| 2019 | 0.528 |
| 2018 | 0.616 |
| 2017 | 0.741 |
| 2016 | 0.527 |
| 2015 | 0.447 |
| 2014 | 0.367 |
| 2013 | 0.624 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
