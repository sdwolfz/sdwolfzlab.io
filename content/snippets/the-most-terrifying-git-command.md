---
title: "The most terrifying Git command"
date: 2020-11-29T16:52:21Z
type: posts
slug: the-most-terrifying-git-command
tags: ["git", "linux"]
---

```sh
git clean -fdx
```

It removes all files not tracked or staged by git. Useful for cleanup and
reverting your project to "factory settings".

But you might want to keep some directories around, like editor specific caches.
You can do this:

1. First, add the directory you want to keep in `.gitignore`; it has to be the
   entire directory, with none of it's contents tracked:

```sh
# Good

/.idea/
/env/
/secrets/

# Bad (Don't do this!)

/secrets/*
!/secrets/.keep
```

2. Now you need to do a `git init` in each of those directories. Add a `shiled`
   goal to your `Makefile`:

```make
.PHONY: shield
shield:
	@git init .idea
	@git init env
	@git init secrets
```

3. Make sure to execute `make shield` after cloning the repository!

4. Now you can run `git clean -fdx` all you want! Add it to your `Makefile` too:

```make
.PHONY: clean
clean:
	@git clean -fdx
```

## Pro Tip

If your repository and development tools are not `git clean -fdx` proof, you
failed at life!
