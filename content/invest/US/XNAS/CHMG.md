---
title: "CHEMUNG FINANCIAL CORP (CHMG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CHEMUNG FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>CHMG</td></tr>
    <tr><td>Web</td><td><a href="https://www.chemungcanal.com">www.chemungcanal.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.26 |
| 2020 | 1.04 |
| 2019 | 1.04 |
| 2018 | 1.04 |
| 2017 | 1.04 |
| 2016 | 1.04 |
| 2015 | 1.04 |
| 2014 | 1.04 |
| 2013 | 1.04 |
| 2012 | 1.0 |
| 2011 | 1.0 |
| 2010 | 1.0 |
| 2009 | 1.25 |
| 2008 | 0.75 |
| 2007 | 0.97 |
| 2006 | 0.96 |
| 2005 | 0.96 |
| 2004 | 0.93 |
| 2003 | 0.92 |
| 2002 | 0.92 |
| 2001 | 0.9 |
| 2000 | 0.86 |
| 1999 | 0.76 |
| 1998 | 0.17 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
