---
title: "EVERTEC INC (EVTC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>EVERTEC INC</td></tr>
    <tr><td>Symbol</td><td>EVTC</td></tr>
    <tr><td>Web</td><td><a href="https://www.evertecinc.com">www.evertecinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.2 |
| 2019 | 0.2 |
| 2018 | 0.1 |
| 2017 | 0.3 |
| 2016 | 0.4 |
| 2015 | 0.4 |
| 2014 | 0.4 |
| 2013 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
