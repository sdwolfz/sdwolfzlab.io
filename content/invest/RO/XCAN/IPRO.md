---
title: "IPROLAM SA BUCURESTI (IPRO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>IPROLAM SA BUCURESTI</td></tr>
    <tr><td>Symbol</td><td>IPRO</td></tr>
    <tr><td>Web</td><td><a href="https://www.iprolam.ro">www.iprolam.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2013 | 0.2 |
| 2012 | 0.2 |
| 2009 | 0.1 |
| 2008 | 0.1 |
| 2007 | 0.1 |
| 2006 | 0.0927 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
