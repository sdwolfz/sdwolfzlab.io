---
title: " (WIT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>WIT</td></tr>
    <tr><td>Web</td><td><a href="https://www.wipro.com">www.wipro.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.012 |
| 2020 | 0.014 |
| 2019 | 0.01 |
| 2018 | 0.016 |
| 2017 | 0.015 |
| 2016 | 0.088 |
| 2015 | 0.19 |
| 2014 | 0.376 |
| 2013 | 0.082 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
