---
title: "GSX TECHEDU INC SPON ADS EACH REP 0.666 ORD SHS CL A (GSX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GSX TECHEDU INC SPON ADS EACH REP 0.666 ORD SHS CL A</td></tr>
    <tr><td>Symbol</td><td>GSX</td></tr>
    <tr><td>Web</td><td><a href="http://www.genshuixue.com">www.genshuixue.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
