---
title: "SLR SENIOR INVESTMENT CORP (SUNS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SLR SENIOR INVESTMENT CORP</td></tr>
    <tr><td>Symbol</td><td>SUNS</td></tr>
    <tr><td>Web</td><td><a href="https://slrseniorinvestmentcorp.com">slrseniorinvestmentcorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.5 |
| 2020 | 1.27 |
| 2019 | 1.41 |
| 2018 | 1.41 |
| 2017 | 1.41 |
| 2016 | 1.41 |
| 2015 | 1.41 |
| 2014 | 1.41 |
| 2013 | 1.41 |
| 2012 | 1.29 |
| 2011 | 0.55 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
