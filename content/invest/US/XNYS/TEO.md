---
title: " (TEO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>TEO</td></tr>
    <tr><td>Web</td><td><a href="https://institucional.telecom.com.ar">institucional.telecom.com.ar</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.346 |
| 2019 | 1.243 |
| 2018 | 2.296 |
| 2017 | 1.137 |
| 2016 | 0.649 |
| 2015 | 0.378 |
| 2014 | 0.95 |
| 2013 | 0.685 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
