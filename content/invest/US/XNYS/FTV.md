---
title: "FORTIVE CORP (FTV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FORTIVE CORP</td></tr>
    <tr><td>Symbol</td><td>FTV</td></tr>
    <tr><td>Web</td><td><a href="https://www.fortive.com">www.fortive.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.14 |
| 2020 | 13.68 |
| 2019 | 0.28 |
| 2018 | 0.28 |
| 2017 | 0.28 |
| 2016 | 0.14 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
