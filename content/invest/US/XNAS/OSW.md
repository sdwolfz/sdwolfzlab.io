---
title: "ONESPAWORLD HOLDINGS LIMITED (OSW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ONESPAWORLD HOLDINGS LIMITED</td></tr>
    <tr><td>Symbol</td><td>OSW</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.04 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
