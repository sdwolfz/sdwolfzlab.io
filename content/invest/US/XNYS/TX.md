---
title: " (TX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>TX</td></tr>
    <tr><td>Web</td><td><a href="https://www.ternium.com">www.ternium.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.1 |
| 2020 | 1.2 |
| 2019 | 1.2 |
| 2018 | 1.1 |
| 2017 | 1.0 |
| 2016 | 0.9 |
| 2015 | 0.9 |
| 2014 | 0.75 |
| 2013 | 0.65 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
