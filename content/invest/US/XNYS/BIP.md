---
title: " (BIP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BIP</td></tr>
    <tr><td>Web</td><td><a href="https://www.bip.brookfield.com">www.bip.brookfield.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.285 |
| 2019 | 2.511 |
| 2018 | 1.88 |
| 2017 | 1.74 |
| 2016 | 1.923 |
| 2015 | 2.12 |
| 2014 | 1.92 |
| 2013 | 0.86 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
