---
title: "NATIONAL BANKSHARES INC (NKSH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NATIONAL BANKSHARES INC</td></tr>
    <tr><td>Symbol</td><td>NKSH</td></tr>
    <tr><td>Web</td><td><a href="https://www.nationalbankshares.com">www.nationalbankshares.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.39 |
| 2019 | 1.39 |
| 2018 | 1.21 |
| 2017 | 1.17 |
| 2016 | 1.16 |
| 2015 | 1.14 |
| 2014 | 1.13 |
| 2013 | 1.12 |
| 2012 | 1.1 |
| 2011 | 1.0 |
| 2010 | 0.91 |
| 2009 | 0.84 |
| 2008 | 0.8 |
| 2007 | 0.76 |
| 2006 | 0.73 |
| 2005 | 1.42 |
| 2004 | 1.28 |
| 2003 | 1.13 |
| 2002 | 0.97 |
| 2001 | 0.86 |
| 2000 | 0.85 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
