---
title: "CHICAGO RIVET & MACHINE CO (CVR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CHICAGO RIVET & MACHINE CO</td></tr>
    <tr><td>Symbol</td><td>CVR</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.22 |
| 2020 | 0.52 |
| 2019 | 1.18 |
| 2018 | 1.14 |
| 2017 | 1.15 |
| 2016 | 0.99 |
| 2015 | 0.97 |
| 2014 | 1.12 |
| 2013 | 0.63 |
| 2012 | 0.9 |
| 2011 | 0.51 |
| 2010 | 0.32 |
| 2009 | 0.48 |
| 2008 | 0.87 |
| 2007 | 0.72 |
| 2006 | 0.72 |
| 2005 | 0.87 |
| 2004 | 0.72 |
| 2003 | 0.97 |
| 2002 | 0.87 |
| 2001 | 0.97 |
| 2000 | 1.07 |
| 1999 | 0.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
