---
title: " (NVO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>NVO</td></tr>
    <tr><td>Web</td><td><a href="https://www.novonordisk.com">www.novonordisk.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.663 |
| 2020 | 0.929 |
| 2019 | 0.875 |
| 2018 | 0.905 |
| 2017 | 0.815 |
| 2016 | 1.03 |
| 2015 | 0.533 |
| 2014 | 0.606 |
| 2013 | 0.453 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
