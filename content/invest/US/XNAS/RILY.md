---
title: "B RILEY FINANCIAL INC (RILY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>B RILEY FINANCIAL INC</td></tr>
    <tr><td>Symbol</td><td>RILY</td></tr>
    <tr><td>Web</td><td><a href="https://www.brileyfin.com">www.brileyfin.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 6.5 |
| 2020 | 1.325 |
| 2019 | 1.49 |
| 2018 | 0.74 |
| 2017 | 0.67 |
| 2016 | 0.28 |
| 2015 | 0.32 |
| 2014 | 0.03 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
