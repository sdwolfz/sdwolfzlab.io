---
title: " (FUN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>FUN</td></tr>
    <tr><td>Web</td><td><a href="https://www.cedarfair.com">www.cedarfair.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.935 |
| 2019 | 3.71 |
| 2018 | 3.595 |
| 2017 | 3.455 |
| 2016 | 3.33 |
| 2015 | 3.075 |
| 2014 | 2.85 |
| 2013 | 1.325 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
