---
title: "CARBOCHIM S.A. (CBC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>CARBOCHIM S.A.</td></tr>
    <tr><td>Symbol</td><td>CBC</td></tr>
    <tr><td>Web</td><td><a href="https://www.carbochim.ro">www.carbochim.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.3 |
| 2017 | 0.25 |
| 2015 | 0.4 |
| 2014 | 0.19 |
| 2013 | 0.08 |
| 2012 | 0.02 |
| 2011 | 0.55 |
| 2010 | 0.31 |
| 2008 | 0.18 |
| 2006 | 0.17 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
