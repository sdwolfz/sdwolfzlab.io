---
title: " (VEDL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>VEDL</td></tr>
    <tr><td>Web</td><td><a href="https://www.vedantalimited.com">www.vedantalimited.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.625 |
| 2019 | 0.088 |
| 2018 | 2.226 |
| 2017 | 1.078 |
| 2016 | 0.088 |
| 2015 | 0.322 |
| 2014 | 0.097 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
