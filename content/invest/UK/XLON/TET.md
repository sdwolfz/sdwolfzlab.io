---
title: "Treatt (TET)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Treatt</td></tr>
    <tr><td>Symbol</td><td>TET</td></tr>
    <tr><td>Web</td><td><a href="https://www.treatt.com">www.treatt.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.0 |
| 2019 | 5.5 |
| 2018 | 5.1 |
| 2017 | 4.8 |
| 2016 | 4.35 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
