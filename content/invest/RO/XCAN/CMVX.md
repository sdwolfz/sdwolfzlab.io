---
title: "COMVEX SA CONSTANTA (CMVX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>COMVEX SA CONSTANTA</td></tr>
    <tr><td>Symbol</td><td>CMVX</td></tr>
    <tr><td>Web</td><td><a href="https://www.comvex.ro">www.comvex.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2012 | 2.0951 |
| 2007 | 3.3956 |
| 2006 | 1.8189 |
| 2005 | 3.6016 |
| 2004 | 1.1979 |
| 2003 | 2.0995 |
| 2002 | 1.0925 |
| 2001 | 0.2342 |
| 2000 | 0.4321 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
