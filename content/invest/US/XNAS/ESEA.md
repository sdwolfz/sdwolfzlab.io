---
title: "EUROSEAS LTD (ESEA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>EUROSEAS LTD</td></tr>
    <tr><td>Symbol</td><td>ESEA</td></tr>
    <tr><td>Web</td><td><a href="https://www.euroseas.gr">www.euroseas.gr</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2013 | 0.045 |
| 2012 | 0.125 |
| 2011 | 0.27 |
| 2010 | 0.22 |
| 2009 | 0.35 |
| 2008 | 1.13 |
| 2007 | 1.0 |
| 2006 | 0.33 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
