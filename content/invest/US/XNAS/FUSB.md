---
title: "FIRST US BANCSHARES INC (FUSB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST US BANCSHARES INC</td></tr>
    <tr><td>Symbol</td><td>FUSB</td></tr>
    <tr><td>Web</td><td><a href="https://www.firstusbank.com">www.firstusbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.03 |
| 2020 | 0.12 |
| 2019 | 0.09 |
| 2018 | 0.08 |
| 2017 | 0.08 |
| 2016 | 0.08 |
| 2015 | 0.08 |
| 2014 | 0.03 |
| 2011 | 0.04 |
| 2010 | 0.44 |
| 2009 | 0.6 |
| 2008 | 1.08 |
| 2007 | 1.19 |
| 2006 | 1.07 |
| 2005 | 0.95 |
| 2004 | 0.72 |
| 2003 | 1.0 |
| 2002 | 1.2 |
| 2001 | 1.02 |
| 2000 | 0.46 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
