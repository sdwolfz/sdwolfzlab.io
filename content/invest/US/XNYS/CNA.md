---
title: "CNA FINANCIAL CORP (CNA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CNA FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>CNA</td></tr>
    <tr><td>Web</td><td><a href="https://www.cna.com">www.cna.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.51 |
| 2020 | 3.48 |
| 2019 | 3.4 |
| 2018 | 3.3 |
| 2017 | 3.1 |
| 2016 | 3.0 |
| 2015 | 1.0 |
| 2014 | 1.0 |
| 2013 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
