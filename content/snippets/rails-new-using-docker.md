---
title: "Create new Rails app through Docker"
date: 2024-01-21T09:43:30Z
type: posts
slug: create-new-rails-app-through-Docker
tags: ["development", "rails", "ruby"]
---

## Prerequisites

Install [Docker Here](/projects/docker-here/).

## Shell

You need the bigger `Debian` based docker image for installing Rails as it needs
to have `gcc` and `make` for building naitve extensions.

```sh
docker-here ruby:3.3.0 bash
```

## Rails

```sh
gem install -N rails
```

## New

```sh
rails new "name-me"     \
  --skip-bundle         \
  --skip-docker         \
  --skip-jbuilder       \
  --database=postgresql \
  --javascript=esbuild  \
  --css=tailwind
```

Once this is done you can `Ctrl-C` to exit the docker shell.

## Git

```sh
git status

git init
git commit -m 'Initial commit' --allow-empty

git add .
git commit -m 'Rails New'
```

## Assets

```sh
bundle exec rails javascript:install:esbuild
bundle exec rails css:install:tailwind
```

## Next Steps

Continue the setup using templates from
[Docker Projects](/projects/docker-projects/).
