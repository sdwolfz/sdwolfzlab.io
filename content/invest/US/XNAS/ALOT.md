---
title: "ASTRONOVA INC (ALOT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ASTRONOVA INC</td></tr>
    <tr><td>Symbol</td><td>ALOT</td></tr>
    <tr><td>Web</td><td><a href="https://www.astronovainc.com">www.astronovainc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.07 |
| 2019 | 0.28 |
| 2018 | 0.28 |
| 2017 | 0.28 |
| 2016 | 0.28 |
| 2015 | 0.28 |
| 2014 | 0.28 |
| 2013 | 0.28 |
| 2012 | 0.35 |
| 2011 | 0.28 |
| 2010 | 0.28 |
| 2009 | 0.24 |
| 2008 | 0.23 |
| 2007 | 0.2 |
| 2006 | 0.225 |
| 2005 | 0.16 |
| 2004 | 0.16 |
| 2003 | 0.16 |
| 2002 | 0.16 |
| 2001 | 0.16 |
| 2000 | 0.16 |
| 1999 | 0.16 |
| 1998 | 0.16 |
| 1997 | 0.15 |
| 1996 | 0.12 |
| 1995 | 0.12 |
| 1994 | 0.12 |
| 1993 | 0.12 |
| 1992 | 0.12 |
| 1991 | 0.09 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
