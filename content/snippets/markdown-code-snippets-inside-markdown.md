---
title: "Markdown code snippets inside Markdown"
date: 2020-12-14T15:25:28Z
type: posts
slug: markdown-code-snippets-inside-markdown
tags: ["markdown", "web"]
---

Wondering how to write a markdown code snippet inside a markdown document?

````markdown
```sh
ls -la | grep my-file
```
````

You need to use four backticks:

~~~markdown
````markdown
```sh
ls -la | grep my-file
```
````
~~~

Or tildas:

````markdown
~~~markdown
```sh
ls -la | grep my-file
```
~~~
````

The choice is yours but be consistent...
