---
title: "ERIE INDEMNITY CO (ERIE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ERIE INDEMNITY CO</td></tr>
    <tr><td>Symbol</td><td>ERIE</td></tr>
    <tr><td>Web</td><td><a href="https://www.erieinsurance.com">www.erieinsurance.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 3.105 |
| 2020 | 5.86 |
| 2019 | 3.6 |
| 2018 | 3.36 |
| 2017 | 3.13 |
| 2016 | 2.19 |
| 2015 | 3.454 |
| 2014 | 2.54 |
| 2013 | 1.778 |
| 2012 | 4.803 |
| 2011 | 2.06 |
| 2010 | 1.44 |
| 2009 | 1.83 |
| 2008 | 2.21 |
| 2007 | 1.6 |
| 2006 | 1.44 |
| 2005 | 1.3 |
| 2004 | 0.645 |
| 2003 | 0.975 |
| 2002 | 0.68 |
| 2001 | 0.61 |
| 2000 | 0.54 |
| 1999 | 0.48 |
| 1998 | 0.43 |
| 1997 | 0.38 |
| 1996 | 0.667 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
