---
title: " (BPOPM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BPOPM</td></tr>
    <tr><td>Web</td><td><a href="https://www.popular.com">www.popular.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.638 |
| 2020 | 1.531 |
| 2019 | 1.531 |
| 2018 | 1.531 |
| 2017 | 1.531 |
| 2016 | 1.531 |
| 2015 | 1.531 |
| 2014 | 1.531 |
| 2013 | 1.531 |
| 2012 | 1.531 |
| 2011 | 1.531 |
| 2010 | 1.531 |
| 2009 | 1.531 |
| 2008 | 1.531 |
| 2007 | 1.531 |
| 2006 | 1.404 |
| 2005 | 1.408 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
