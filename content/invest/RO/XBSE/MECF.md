---
title: "MECANICA CEAHLAU (MECF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>MECANICA CEAHLAU</td></tr>
    <tr><td>Symbol</td><td>MECF</td></tr>
    <tr><td>Web</td><td><a href="https://www.mecanicaceahlau.ro">www.mecanicaceahlau.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.04585 |
| 2016 | 0.0049 |
| 2015 | 0.005 |
| 2014 | 0.006 |
| 2012 | 0.006 |
| 2003 | 0.1441 |
| 2002 | 0.5525 |
| 2001 | 0.2517 |
| 2000 | 0.1376 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
