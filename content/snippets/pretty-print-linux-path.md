---
title: "Pretty print Linux $PATH"
date: 2022-09-25T10:19:10Z
type: posts
slug: pretty-print-linux-path
series: ["Bash Tricks"]
tags: ["linux", "shell", "bash", "sed"]
---

{{< tabgroup align="right" style="code" >}}
  {{< tab name="Bash" >}}
  ```sh
  echo $PATH | sed "s/:/\n/g"
  ```
  {{< /tab >}}
{{< /tabgroup >}}
