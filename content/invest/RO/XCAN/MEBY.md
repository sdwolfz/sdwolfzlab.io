---
title: "LUCEAFARUL SA BACAU (MEBY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>LUCEAFARUL SA BACAU</td></tr>
    <tr><td>Symbol</td><td>MEBY</td></tr>
    <tr><td>Web</td><td><a href="https://www.luceafarul.ro">www.luceafarul.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2012 | 0.1726 |
| 2006 | 0.32 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
