---
title: "GERMINA AGRIBUSINESS SA BUCURESTI (SEOM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>GERMINA AGRIBUSINESS SA BUCURESTI</td></tr>
    <tr><td>Symbol</td><td>SEOM</td></tr>
    <tr><td>Web</td><td><a href="https://www.germina.ro">www.germina.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.37 |
| 2009 | 0.7 |
| 2008 | 0.13 |
| 2006 | 0.126 |
| 2004 | 0.099 |
| 2002 | 0.2033 |
| 2001 | 0.1748 |
| 2000 | 0.1121 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
