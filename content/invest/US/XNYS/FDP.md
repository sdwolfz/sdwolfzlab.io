---
title: "FRESH DEL MONTE PRODUCE INC (FDP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FRESH DEL MONTE PRODUCE INC</td></tr>
    <tr><td>Symbol</td><td>FDP</td></tr>
    <tr><td>Web</td><td><a href="https://www.freshdelmonte.com">www.freshdelmonte.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.3 |
| 2019 | 0.14 |
| 2018 | 0.6 |
| 2017 | 0.6 |
| 2016 | 0.55 |
| 2015 | 0.5 |
| 2014 | 0.5 |
| 2013 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
