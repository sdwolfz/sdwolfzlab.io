---
title: "Temporary files and directories in Ruby"
date: 2020-07-23T20:13:45Z
type: posts
slug: temporary-files-and-directories-in-ruby
tags: ["ruby", "tutorial", "development"]
---

## File

Create a temporary file in your current directory:

```ruby
require 'tempfile'

Tempfile.create('', Dir.pwd) do |file|
  puts File.path(file)

  File.write(file, 'I am temporary!')
  puts File.read(file)
end
```

## Directory

For directory it's similar, but no `require` needed:

```ruby
Dir.mktmpdir(nil, Dir.pwd) do |dir|
  puts File.path(dir)
end
```
