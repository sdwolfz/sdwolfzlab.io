---
title: "ROMAERO SA BUCURESTI (RORX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>ROMAERO SA BUCURESTI</td></tr>
    <tr><td>Symbol</td><td>RORX</td></tr>
    <tr><td>Web</td><td><a href="https://www.romaero.com">www.romaero.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
