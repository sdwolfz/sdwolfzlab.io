---
title: " (REG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>REG</td></tr>
    <tr><td>Web</td><td><a href="https://www.regencycenters.com">www.regencycenters.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.19 |
| 2020 | 2.38 |
| 2019 | 2.34 |
| 2018 | 2.22 |
| 2017 | 2.1 |
| 2016 | 2.0 |
| 2015 | 1.94 |
| 2014 | 1.88 |
| 2013 | 1.85 |
| 2012 | 1.85 |
| 2011 | 1.85 |
| 2010 | 1.85 |
| 2009 | 2.113 |
| 2008 | 2.9 |
| 2007 | 2.64 |
| 2006 | 2.38 |
| 2005 | 2.2 |
| 2004 | 2.12 |
| 2003 | 2.08 |
| 2002 | 2.04 |
| 2001 | 2.0 |
| 2000 | 0.96 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
