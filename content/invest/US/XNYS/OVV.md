---
title: "OVINTIV INC (OVV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>OVINTIV INC</td></tr>
    <tr><td>Symbol</td><td>OVV</td></tr>
    <tr><td>Web</td><td><a href="https://www.ovintiv.com">www.ovintiv.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.376 |
| 2019 | 0.094 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
