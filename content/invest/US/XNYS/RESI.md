---
title: "FRONT YARD RESIDENTIAL CORP (RESI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FRONT YARD RESIDENTIAL CORP</td></tr>
    <tr><td>Symbol</td><td>RESI</td></tr>
    <tr><td>Web</td><td><a href="http://www.frontyardresidential.com">www.frontyardresidential.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.15 |
| 2019 | 0.45 |
| 2018 | 0.6 |
| 2017 | 0.6 |
| 2016 | 0.6 |
| 2015 | 1.75 |
| 2014 | 1.95 |
| 2013 | 0.35 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
