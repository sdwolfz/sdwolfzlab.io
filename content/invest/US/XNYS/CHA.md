---
title: "CHINA TELECOM CORPORATION SPONS ADR EACH REPR 100'H'COM CNY1 (CHA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CHINA TELECOM CORPORATION SPONS ADR EACH REPR 100'H'COM CNY1</td></tr>
    <tr><td>Symbol</td><td>CHA</td></tr>
    <tr><td>Web</td><td><a href="http://www.chinatelecom-h.com">www.chinatelecom-h.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.451 |
| 2019 | 1.439 |
| 2018 | 1.319 |
| 2017 | 1.21 |
| 2016 | 1.102 |
| 2015 | 1.103 |
| 2014 | 1.103 |
| 2013 | 0.986 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
