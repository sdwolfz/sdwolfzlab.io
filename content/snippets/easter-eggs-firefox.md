---
title: "Easter Eggs: Firefox"
date: 2020-07-24T18:28:29Z
type: posts
slug: easter-eggs-firefox
series: ["Easter Eggs"]
tags: ["firefox", "web", "development"]
---

1. Start the console with `Ctrl + Shift + K`
2. Run:

```javascript
document.designMode = 'on'
```

3. Close the console with `Ctrl + Shift + I`

And now you can edit any text directiny into the page.
