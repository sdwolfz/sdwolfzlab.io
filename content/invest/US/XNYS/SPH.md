---
title: " (SPH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SPH</td></tr>
    <tr><td>Web</td><td><a href="https://www.suburbanpropane.com">www.suburbanpropane.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.6 |
| 2020 | 1.8 |
| 2019 | 2.4 |
| 2018 | 2.4 |
| 2017 | 3.261 |
| 2016 | 3.548 |
| 2015 | 3.536 |
| 2014 | 3.5 |
| 2013 | 1.75 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
