---
title: "HONEYWELL INTERNATIONAL INC (HON)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HONEYWELL INTERNATIONAL INC</td></tr>
    <tr><td>Symbol</td><td>HON</td></tr>
    <tr><td>Web</td><td><a href="https://www.honeywell.com">www.honeywell.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.86 |
| 2020 | 3.63 |
| 2019 | 3.36 |
| 2018 | 4.905 |
| 2017 | 2.74 |
| 2016 | 2.45 |
| 2015 | 2.146 |
| 2014 | 1.867 |
| 2013 | 0.86 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
