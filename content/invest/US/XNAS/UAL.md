---
title: "UNITED AIRLINES HOLDINGS INC (UAL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>UNITED AIRLINES HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>UAL</td></tr>
    <tr><td>Web</td><td><a href="https://www.unitedcontinentalholdings.com">www.unitedcontinentalholdings.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2008 | 2.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
