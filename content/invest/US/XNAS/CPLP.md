---
title: " (CPLP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CPLP</td></tr>
    <tr><td>Web</td><td><a href="https://www.capitalpplp.com">www.capitalpplp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.9 |
| 2019 | 0.99 |
| 2018 | 0.32 |
| 2017 | 0.32 |
| 2016 | 0.463 |
| 2015 | 0.942 |
| 2014 | 0.93 |
| 2013 | 0.93 |
| 2012 | 0.93 |
| 2011 | 0.93 |
| 2010 | 1.093 |
| 2009 | 2.28 |
| 2008 | 1.615 |
| 2007 | 0.748 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
