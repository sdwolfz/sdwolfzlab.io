---
title: "WPP (WPP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>WPP</td></tr>
    <tr><td>Symbol</td><td>WPP</td></tr>
    <tr><td>Web</td><td><a href="https://www.wpp.com">www.wpp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 10.0 |
| 2019 | 60.0 |
| 2018 | 60.0 |
| 2017 | 59.75 |
| 2016 | 56.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
