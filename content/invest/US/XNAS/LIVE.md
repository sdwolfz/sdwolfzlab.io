---
title: "LIVE VENTURES INCORPORATED (LIVE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LIVE VENTURES INCORPORATED</td></tr>
    <tr><td>Symbol</td><td>LIVE</td></tr>
    <tr><td>Web</td><td><a href="https://www.liveventures.com">www.liveventures.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2005 | 0.03 |
| 2004 | 0.01 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
