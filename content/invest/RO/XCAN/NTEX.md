---
title: "SIFI CJ STORAGE SA (NTEX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>SIFI CJ STORAGE SA</td></tr>
    <tr><td>Symbol</td><td>NTEX</td></tr>
    <tr><td>Web</td><td><a href="https://www.napotex.ro">www.napotex.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 5.46 |
| 2019 | 2.7 |
| 2018 | 1.85 |
| 2017 | 1.2 |
| 2016 | 1.0 |
| 2015 | 0.8 |
| 2014 | 0.48 |
| 2011 | 0.61 |
| 2010 | 0.5 |
| 2009 | 0.84 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
