---
title: "IAMBA ARAD S.A. ARAD (FERO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>IAMBA ARAD S.A. ARAD</td></tr>
    <tr><td>Symbol</td><td>FERO</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.0591 |
| 2015 | 0.336 |
| 2012 | 0.0877 |
| 2009 | 0.0244 |
| 2008 | 0.03 |
| 2007 | 0.0673 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
