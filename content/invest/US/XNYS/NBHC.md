---
title: "NATIONAL BANK HOLDINGS CORP (NBHC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NATIONAL BANK HOLDINGS CORP</td></tr>
    <tr><td>Symbol</td><td>NBHC</td></tr>
    <tr><td>Web</td><td><a href="https://www.nationalbankholdings.com">www.nationalbankholdings.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.43 |
| 2020 | 0.8 |
| 2019 | 0.75 |
| 2018 | 0.54 |
| 2017 | 0.34 |
| 2016 | 0.22 |
| 2015 | 0.2 |
| 2014 | 0.2 |
| 2013 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
