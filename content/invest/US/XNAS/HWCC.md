---
title: "HOUSTON WIRE & CABLE CO (HWCC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HOUSTON WIRE & CABLE CO</td></tr>
    <tr><td>Symbol</td><td>HWCC</td></tr>
    <tr><td>Web</td><td><a href="https://www.houwire.com">www.houwire.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.15 |
| 2015 | 0.42 |
| 2014 | 0.47 |
| 2013 | 0.42 |
| 2012 | 0.36 |
| 2011 | 0.355 |
| 2010 | 0.34 |
| 2009 | 0.34 |
| 2008 | 0.34 |
| 2007 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
