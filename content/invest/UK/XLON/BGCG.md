---
title: "Baillie Gif. Ch (BGCG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Baillie Gif. Ch</td></tr>
    <tr><td>Symbol</td><td>BGCG</td></tr>
    <tr><td>Web</td><td><a href="https://www.bailliegifford.co.uk">www.bailliegifford.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
