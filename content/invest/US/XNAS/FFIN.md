---
title: "FIRST FINANCIAL BANKSHARES INC (FFIN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST FINANCIAL BANKSHARES INC</td></tr>
    <tr><td>Symbol</td><td>FFIN</td></tr>
    <tr><td>Web</td><td><a href="https://www.ffin.com">www.ffin.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 0.51 |
| 2019 | 0.57 |
| 2018 | 0.82 |
| 2017 | 0.75 |
| 2016 | 0.7 |
| 2015 | 0.62 |
| 2014 | 0.68 |
| 2013 | 1.03 |
| 2012 | 0.99 |
| 2011 | 1.06 |
| 2010 | 1.36 |
| 2009 | 1.36 |
| 2008 | 1.34 |
| 2007 | 1.26 |
| 2006 | 1.18 |
| 2005 | 1.18 |
| 2004 | 1.33 |
| 2003 | 1.28 |
| 2002 | 1.35 |
| 2001 | 1.23 |
| 2000 | 1.29 |
| 1999 | 1.125 |
| 1998 | 1.075 |
| 1997 | 1.03 |
| 1996 | 1.15 |
| 1995 | 1.21 |
| 1994 | 1.16 |
| 1993 | 0.32 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
