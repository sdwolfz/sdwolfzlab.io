---
title: "INTRICOM CORP (IIN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>INTRICOM CORP</td></tr>
    <tr><td>Symbol</td><td>IIN</td></tr>
    <tr><td>Web</td><td><a href="https://www.intricon.com">www.intricon.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2001 | 0.135 |
| 2000 | 0.18 |
| 1999 | 0.045 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
