---
title: "CMS ENERGY CORP (CMS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CMS ENERGY CORP</td></tr>
    <tr><td>Symbol</td><td>CMS</td></tr>
    <tr><td>Web</td><td><a href="https://www.cmsenergy.com">www.cmsenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.87 |
| 2020 | 1.628 |
| 2019 | 1.528 |
| 2018 | 1.428 |
| 2017 | 1.332 |
| 2016 | 1.24 |
| 2015 | 1.16 |
| 2014 | 1.08 |
| 2013 | 0.51 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
