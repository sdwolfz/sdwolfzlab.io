---
title: "W & T OFFSHORE INC (WTI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>W & T OFFSHORE INC</td></tr>
    <tr><td>Symbol</td><td>WTI</td></tr>
    <tr><td>Web</td><td><a href="https://www.wtoffshore.com">www.wtoffshore.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 0.4 |
| 2013 | 0.19 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
