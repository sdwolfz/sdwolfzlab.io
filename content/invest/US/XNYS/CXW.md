---
title: " (CXW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CXW</td></tr>
    <tr><td>Web</td><td><a href="https://www.corecivic.com">www.corecivic.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.88 |
| 2019 | 1.32 |
| 2018 | 1.72 |
| 2017 | 1.68 |
| 2016 | 2.04 |
| 2015 | 2.16 |
| 2014 | 2.04 |
| 2013 | 0.96 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
