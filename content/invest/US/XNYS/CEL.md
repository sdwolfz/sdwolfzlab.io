---
title: "CELLCOM ISRAEL LTD (CEL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CELLCOM ISRAEL LTD</td></tr>
    <tr><td>Symbol</td><td>CEL</td></tr>
    <tr><td>Web</td><td><a href="http://www.cellcom.co.il">www.cellcom.co.il</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2013 | 0.24 |
| 2012 | 0.34 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
