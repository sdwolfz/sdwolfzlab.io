---
title: "Ruby: pass around a block"
date: 2021-10-25T11:12:19Z
type: posts
slug: ruby-pass-around-a-block
tags: ["ruby"]
---

You have the following:

```ruby
callable = proc { puts 'Hello' }
```

Pass it to a function like this:

```ruby
def pass_me_something
  yield if block_given?
end

pass_me_something(&callable)
```

Or be explicit:

```ruby
def pass_me_something(&block)
  block.call if block_given?
end

pass_me_something(&callable)
```

You can also be weird:

```ruby
def pass_me_something(block)
  block.call if block&.is_a?(Proc)
end

pass_me_something(callable)
```
