---
title: "COMPUTER PROGRAMS & SYSTEMS INC (CPSI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COMPUTER PROGRAMS & SYSTEMS INC</td></tr>
    <tr><td>Symbol</td><td>CPSI</td></tr>
    <tr><td>Web</td><td><a href="https://www.cpsi.com">www.cpsi.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.3 |
| 2019 | 0.4 |
| 2018 | 0.4 |
| 2017 | 0.85 |
| 2016 | 1.86 |
| 2015 | 2.56 |
| 2014 | 2.28 |
| 2013 | 2.04 |
| 2012 | 2.84 |
| 2011 | 1.44 |
| 2010 | 1.44 |
| 2009 | 1.44 |
| 2008 | 1.44 |
| 2007 | 1.44 |
| 2006 | 1.44 |
| 2005 | 0.88 |
| 2004 | 0.48 |
| 2003 | 0.255 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
