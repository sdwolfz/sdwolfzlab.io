---
title: "MOELIS & COMPANY (MC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MOELIS & COMPANY</td></tr>
    <tr><td>Symbol</td><td>MC</td></tr>
    <tr><td>Web</td><td><a href="https://www.moelis.com">www.moelis.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 3.1 |
| 2020 | 4.152 |
| 2019 | 3.25 |
| 2018 | 4.88 |
| 2017 | 1.48 |
| 2016 | 2.04 |
| 2015 | 1.0 |
| 2014 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
