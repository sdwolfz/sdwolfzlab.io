---
title: "GAMESTOP CORPORATION (GME)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GAMESTOP CORPORATION</td></tr>
    <tr><td>Symbol</td><td>GME</td></tr>
    <tr><td>Web</td><td><a href="https://www.gamestop.com">www.gamestop.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.38 |
| 2018 | 1.52 |
| 2017 | 1.52 |
| 2016 | 1.48 |
| 2015 | 1.44 |
| 2014 | 1.32 |
| 2013 | 0.825 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
