---
title: "POTIS CAPITAL SA CLUJ-NAPOCA (POTI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>POTIS CAPITAL SA CLUJ-NAPOCA</td></tr>
    <tr><td>Symbol</td><td>POTI</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.78 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
