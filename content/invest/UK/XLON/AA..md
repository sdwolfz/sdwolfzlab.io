---
title: "AA PLC ORD 0.1P (AA.)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>AA PLC ORD 0.1P</td></tr>
    <tr><td>Symbol</td><td>AA.</td></tr>
    <tr><td>Web</td><td><a href="http://www.theaa.com">www.theaa.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.6 |
| 2019 | 2.0 |
| 2018 | 5.0 |
| 2017 | 9.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
