---
title: "Royal Dutch Shell B (RDSB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Royal Dutch Shell B</td></tr>
    <tr><td>Symbol</td><td>RDSB</td></tr>
    <tr><td>Web</td><td><a href="https://www.shell.com">www.shell.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 65.0 |
| 2019 | 188.0 |
| 2018 | 188.0 |
| 2017 | 188.0 |
| 2016 | 188.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
