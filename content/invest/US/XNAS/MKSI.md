---
title: "MKS INSTRUMENTS INC (MKSI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MKS INSTRUMENTS INC</td></tr>
    <tr><td>Symbol</td><td>MKSI</td></tr>
    <tr><td>Web</td><td><a href="https://www.mksinst.com">www.mksinst.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.8 |
| 2019 | 0.8 |
| 2018 | 0.78 |
| 2017 | 0.705 |
| 2016 | 0.68 |
| 2015 | 0.675 |
| 2014 | 0.655 |
| 2013 | 0.64 |
| 2012 | 0.62 |
| 2011 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
