---
title: "AT&T INC (T)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AT&T INC</td></tr>
    <tr><td>Symbol</td><td>T</td></tr>
    <tr><td>Web</td><td><a href="https://www.att.com">www.att.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.04 |
| 2020 | 2.08 |
| 2019 | 2.04 |
| 2018 | 2.0 |
| 2017 | 1.96 |
| 2016 | 1.92 |
| 2015 | 1.88 |
| 2014 | 1.84 |
| 2013 | 0.9 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
