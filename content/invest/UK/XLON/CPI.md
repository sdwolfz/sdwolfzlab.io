---
title: "Capita (CPI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Capita</td></tr>
    <tr><td>Symbol</td><td>CPI</td></tr>
    <tr><td>Web</td><td><a href="https://www.capita.co.uk">www.capita.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 85.76 |
| 2017 | 11.1 |
| 2016 | 31.7 |
| 2015 | 31.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
