---
title: "FINANCIAL INSTITUTIONS INC (FISI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FINANCIAL INSTITUTIONS INC</td></tr>
    <tr><td>Symbol</td><td>FISI</td></tr>
    <tr><td>Web</td><td><a href="https://www.fiiwarsaw.com">www.fiiwarsaw.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.27 |
| 2020 | 1.04 |
| 2019 | 1.0 |
| 2018 | 0.96 |
| 2017 | 0.85 |
| 2016 | 0.81 |
| 2015 | 0.8 |
| 2014 | 0.77 |
| 2013 | 0.74 |
| 2012 | 0.57 |
| 2011 | 0.47 |
| 2010 | 0.4 |
| 2009 | 0.4 |
| 2008 | 0.54 |
| 2007 | 0.46 |
| 2006 | 0.34 |
| 2005 | 0.4 |
| 2004 | 0.64 |
| 2003 | 0.64 |
| 2002 | 0.58 |
| 2001 | 0.48 |
| 2000 | 0.42 |
| 1999 | 0.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
