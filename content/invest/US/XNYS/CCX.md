---
title: "CHURCHILL CAPITAL CORP II (CCX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CHURCHILL CAPITAL CORP II</td></tr>
    <tr><td>Symbol</td><td>CCX</td></tr>
    <tr><td>Web</td><td><a href="https://www.churchillcapitalcorp.com">www.churchillcapitalcorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
