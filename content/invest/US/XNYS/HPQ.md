---
title: "HP INC (HPQ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HP INC</td></tr>
    <tr><td>Symbol</td><td>HPQ</td></tr>
    <tr><td>Web</td><td><a href="https://www8.hp.com">www8.hp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.194 |
| 2020 | 0.722 |
| 2019 | 0.656 |
| 2018 | 0.577 |
| 2017 | 0.538 |
| 2016 | 0.505 |
| 2015 | 0.636 |
| 2014 | 0.625 |
| 2013 | 0.29 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
