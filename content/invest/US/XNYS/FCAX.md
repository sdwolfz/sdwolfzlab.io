---
title: "FORTRESS CAPITAL ACQUISITION CORP (FCAX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FORTRESS CAPITAL ACQUISITION CORP</td></tr>
    <tr><td>Symbol</td><td>FCAX</td></tr>
    <tr><td>Web</td><td><a href="https://www.fortresscapitalac.com">www.fortresscapitalac.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
