---
title: "ELECTROCONSTRUCTIA ELCO SA SUCEAVA (ELEL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>ELECTROCONSTRUCTIA ELCO SA SUCEAVA</td></tr>
    <tr><td>Symbol</td><td>ELEL</td></tr>
    <tr><td>Web</td><td><a href="https://www.elcosuceava.ro">www.elcosuceava.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.05 |
| 2019 | 0.05 |
| 2018 | 0.04 |
| 2017 | 0.06 |
| 2016 | 0.06 |
| 2015 | 0.05 |
| 2014 | 0.5 |
| 2013 | 0.05 |
| 2012 | 0.07 |
| 2010 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
