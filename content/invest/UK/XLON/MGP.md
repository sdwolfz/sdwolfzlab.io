---
title: "Medica Group P. (MGP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Medica Group P.</td></tr>
    <tr><td>Symbol</td><td>MGP</td></tr>
    <tr><td>Web</td><td><a href="https://www.medicagroup.co.uk">www.medicagroup.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 2.25 |
| 2017 | 1.65 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
