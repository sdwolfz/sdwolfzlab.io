---
title: "NORTHWEST BANCSHARES INC (NWBI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NORTHWEST BANCSHARES INC</td></tr>
    <tr><td>Symbol</td><td>NWBI</td></tr>
    <tr><td>Web</td><td><a href="https://www.northwest.bank">www.northwest.bank</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.39 |
| 2020 | 0.76 |
| 2019 | 0.72 |
| 2018 | 0.68 |
| 2017 | 0.64 |
| 2016 | 0.6 |
| 2015 | 0.56 |
| 2014 | 1.62 |
| 2013 | 0.5 |
| 2012 | 0.6 |
| 2011 | 0.43 |
| 2010 | 0.4 |
| 2009 | 0.88 |
| 2008 | 0.88 |
| 2007 | 0.84 |
| 2006 | 0.7 |
| 2005 | 0.54 |
| 2004 | 0.44 |
| 2003 | 0.36 |
| 2002 | 0.28 |
| 2001 | 0.2 |
| 2000 | 0.16 |
| 1999 | 0.16 |
| 1998 | 0.16 |
| 1997 | 0.32 |
| 1996 | 0.46 |
| 1995 | 0.55 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
