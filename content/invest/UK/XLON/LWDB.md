---
title: "Law Debenture (LWDB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Law Debenture</td></tr>
    <tr><td>Symbol</td><td>LWDB</td></tr>
    <tr><td>Web</td><td><a href="https://www.lawdebenture.com">www.lawdebenture.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 27.5 |
| 2019 | 26.0 |
| 2018 | 18.9 |
| 2017 | 17.3 |
| 2016 | 16.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
