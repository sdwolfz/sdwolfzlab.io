---
title: "TEXAS PACIFIC LAND CORPORATION (TPL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TEXAS PACIFIC LAND CORPORATION</td></tr>
    <tr><td>Symbol</td><td>TPL</td></tr>
    <tr><td>Web</td><td><a href="https://www.texaspacific.com">www.texaspacific.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 5.5 |
| 2020 | 26.0 |
| 2019 | 6.0 |
| 2018 | 4.05 |
| 2017 | 1.35 |
| 2016 | 0.31 |
| 2015 | 0.29 |
| 2014 | 0.27 |
| 2012 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
