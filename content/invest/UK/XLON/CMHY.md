---
title: "City Merch.high (CMHY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>City Merch.high</td></tr>
    <tr><td>Symbol</td><td>CMHY</td></tr>
    <tr><td>Web</td><td><a href="https://www.invesco.co.uk">www.invesco.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 10.0 |
| 2015 | 10.0 |
| 2014 | 10.0 |
| 2013 | 10.0 |
| 2012 | 10.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
