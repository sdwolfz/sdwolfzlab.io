---
title: "Georgia Capital (CGEO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Georgia Capital</td></tr>
    <tr><td>Symbol</td><td>CGEO</td></tr>
    <tr><td>Web</td><td><a href="https://georgiacapital.ge">georgiacapital.ge</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
