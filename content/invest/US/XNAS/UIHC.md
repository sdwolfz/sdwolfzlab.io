---
title: "UNITED INSURANCE HOLDINGS CORP (UIHC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>UNITED INSURANCE HOLDINGS CORP</td></tr>
    <tr><td>Symbol</td><td>UIHC</td></tr>
    <tr><td>Web</td><td><a href="https://www.upcinsurance.com">www.upcinsurance.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 0.24 |
| 2019 | 0.24 |
| 2018 | 0.24 |
| 2017 | 0.24 |
| 2016 | 0.23 |
| 2015 | 0.2 |
| 2014 | 0.16 |
| 2013 | 0.12 |
| 2012 | 0.08 |
| 2011 | 0.05 |
| 2010 | 0.05 |
| 2009 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
