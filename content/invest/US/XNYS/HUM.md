---
title: "HUMANA INC (HUM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HUMANA INC</td></tr>
    <tr><td>Symbol</td><td>HUM</td></tr>
    <tr><td>Web</td><td><a href="https://www.humana.com">www.humana.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.4 |
| 2020 | 2.5 |
| 2019 | 2.2 |
| 2018 | 1.0 |
| 2017 | 1.89 |
| 2016 | 0.87 |
| 2015 | 1.15 |
| 2014 | 1.11 |
| 2013 | 0.81 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
