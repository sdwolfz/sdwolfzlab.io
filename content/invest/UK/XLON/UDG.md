---
title: "UDG Healthcare (UDG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>UDG Healthcare</td></tr>
    <tr><td>Symbol</td><td>UDG</td></tr>
    <tr><td>Web</td><td><a href="https://www.udghealthcare.com">www.udghealthcare.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 17.0 |
| 2019 | 16.8 |
| 2018 | 16.0 |
| 2017 | 13.3 |
| 2016 | 11.55 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
