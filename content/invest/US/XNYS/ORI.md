---
title: "OLD REPUBLIC INTERNATIONAL CORP (ORI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>OLD REPUBLIC INTERNATIONAL CORP</td></tr>
    <tr><td>Symbol</td><td>ORI</td></tr>
    <tr><td>Web</td><td><a href="https://www.oldrepublic.com">www.oldrepublic.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.22 |
| 2020 | 0.84 |
| 2019 | 1.8 |
| 2018 | 1.78 |
| 2017 | 0.76 |
| 2016 | 0.752 |
| 2015 | 0.74 |
| 2014 | 0.732 |
| 2013 | 0.54 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
