---
title: "STONEX GROUP INC (SNEX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>STONEX GROUP INC</td></tr>
    <tr><td>Symbol</td><td>SNEX</td></tr>
    <tr><td>Web</td><td><a href="https://www.stonex.com">www.stonex.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
