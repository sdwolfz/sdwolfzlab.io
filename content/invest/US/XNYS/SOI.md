---
title: "SOLARIS OILFIELD INFRASTRUCTURE INC (SOI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SOLARIS OILFIELD INFRASTRUCTURE INC</td></tr>
    <tr><td>Symbol</td><td>SOI</td></tr>
    <tr><td>Web</td><td><a href="https://www.solarisoilfield.com">www.solarisoilfield.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.105 |
| 2020 | 0.42 |
| 2019 | 0.405 |
| 2018 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
