---
title: "UZINEXPORT SA BUCURESTI (UZIN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>UZINEXPORT SA BUCURESTI</td></tr>
    <tr><td>Symbol</td><td>UZIN</td></tr>
    <tr><td>Web</td><td><a href="https://www.uzinexport.ro">www.uzinexport.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.1 |
| 2009 | 0.068 |
| 2008 | 0.0457 |
| 2003 | 0.105 |
| 2002 | 0.2111 |
| 2000 | 0.1113 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
