---
title: " (BP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BP</td></tr>
    <tr><td>Web</td><td><a href="https://www.bp.com">www.bp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.62 |
| 2020 | 1.87 |
| 2019 | 2.44 |
| 2018 | 1.815 |
| 2017 | 2.38 |
| 2016 | 2.38 |
| 2015 | 2.385 |
| 2014 | 2.34 |
| 2013 | 1.11 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
