---
title: " (CLMT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CLMT</td></tr>
    <tr><td>Web</td><td><a href="https://www.calumetspecialty.com">www.calumetspecialty.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.685 |
| 2015 | 2.74 |
| 2014 | 2.74 |
| 2013 | 2.7 |
| 2012 | 2.3 |
| 2011 | 1.94 |
| 2010 | 1.825 |
| 2009 | 1.8 |
| 2008 | 1.98 |
| 2007 | 2.46 |
| 2006 | 1.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
