---
title: "GATX CORP (GATX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GATX CORP</td></tr>
    <tr><td>Symbol</td><td>GATX</td></tr>
    <tr><td>Web</td><td><a href="https://www.gatx.com">www.gatx.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.0 |
| 2020 | 1.92 |
| 2019 | 1.84 |
| 2018 | 1.76 |
| 2017 | 1.68 |
| 2016 | 1.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
