---
title: "ATLANTICA SUSTAINABLE INFR PLC (AY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ATLANTICA SUSTAINABLE INFR PLC</td></tr>
    <tr><td>Symbol</td><td>AY</td></tr>
    <tr><td>Web</td><td><a href="https://www.atlantica.com">www.atlantica.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.85 |
| 2020 | 1.66 |
| 2019 | 1.57 |
| 2018 | 1.33 |
| 2017 | 1.05 |
| 2016 | 0.453 |
| 2015 | 1.429 |
| 2014 | 0.296 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
