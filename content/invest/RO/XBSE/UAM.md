---
title: "UAMT S.A. (UAM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>UAMT S.A.</td></tr>
    <tr><td>Symbol</td><td>UAM</td></tr>
    <tr><td>Web</td><td><a href="https://www.uamt.ro">www.uamt.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2002 | 0.0061 |
| 2001 | 0.0008 |
| 2000 | 0.0206 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
