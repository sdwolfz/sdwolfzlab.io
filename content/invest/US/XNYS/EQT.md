---
title: "EQT CORPORATION (EQT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>EQT CORPORATION</td></tr>
    <tr><td>Symbol</td><td>EQT</td></tr>
    <tr><td>Web</td><td><a href="https://www.eqt.com">www.eqt.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.03 |
| 2019 | 0.12 |
| 2018 | 0.12 |
| 2017 | 0.12 |
| 2016 | 0.12 |
| 2015 | 0.12 |
| 2014 | 0.12 |
| 2013 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
