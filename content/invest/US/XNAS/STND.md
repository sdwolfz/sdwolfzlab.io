---
title: "STANDARD AVB FINANCIAL CORP (STND)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>STANDARD AVB FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>STND</td></tr>
    <tr><td>Web</td><td><a href="https://www.standardbankpa.com">www.standardbankpa.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.442 |
| 2020 | 0.884 |
| 2019 | 0.884 |
| 2018 | 0.442 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
