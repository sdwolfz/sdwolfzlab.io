---
title: "Cineworld (CINE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Cineworld</td></tr>
    <tr><td>Symbol</td><td>CINE</td></tr>
    <tr><td>Web</td><td><a href="https://www.cineworld.co.uk">www.cineworld.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 35.77 |
| 2018 | 15.0 |
| 2017 | 21.4 |
| 2016 | 19.0 |
| 2015 | 17.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
