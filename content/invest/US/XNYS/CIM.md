---
title: " (CIM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CIM</td></tr>
    <tr><td>Web</td><td><a href="https://www.chimerareit.com">www.chimerareit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.63 |
| 2020 | 1.4 |
| 2019 | 2.0 |
| 2018 | 2.0 |
| 2017 | 2.0 |
| 2016 | 1.94 |
| 2015 | 1.92 |
| 2014 | 0.36 |
| 2013 | 0.27 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
