---
title: "EXPONENT INC (EXPO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>EXPONENT INC</td></tr>
    <tr><td>Symbol</td><td>EXPO</td></tr>
    <tr><td>Web</td><td><a href="https://www.exponent.com">www.exponent.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.4 |
| 2020 | 0.76 |
| 2019 | 0.64 |
| 2018 | 0.78 |
| 2017 | 0.84 |
| 2016 | 0.72 |
| 2015 | 0.75 |
| 2014 | 1.0 |
| 2013 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
