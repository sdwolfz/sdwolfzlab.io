---
title: "ROYAL BANK OF CANADA (RY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ROYAL BANK OF CANADA</td></tr>
    <tr><td>Symbol</td><td>RY</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.848 |
| 2020 | 3.185 |
| 2019 | 3.055 |
| 2018 | 2.925 |
| 2017 | 3.317 |
| 2016 | 2.453 |
| 2015 | 2.883 |
| 2014 | 2.84 |
| 2013 | 1.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
