---
title: " (SHG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SHG</td></tr>
    <tr><td>Web</td><td><a href="https://www.shinhangroup.com">www.shinhangroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.018 |
| 2019 | 1.156 |
| 2018 | 1.063 |
| 2017 | 1.031 |
| 2015 | 0.793 |
| 2014 | 0.655 |
| 2013 | 0.47 |
| 2012 | 0.462 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
