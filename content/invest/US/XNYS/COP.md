---
title: "CONOCOPHILLIPS (COP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CONOCOPHILLIPS</td></tr>
    <tr><td>Symbol</td><td>COP</td></tr>
    <tr><td>Web</td><td><a href="https://www.conocophillips.com">www.conocophillips.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.86 |
| 2020 | 1.69 |
| 2019 | 1.335 |
| 2018 | 1.16 |
| 2017 | 1.06 |
| 2016 | 1.0 |
| 2015 | 2.94 |
| 2014 | 2.84 |
| 2013 | 1.38 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
