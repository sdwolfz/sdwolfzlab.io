---
title: "Icg Ent Trst (ICGT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Icg Ent Trst</td></tr>
    <tr><td>Symbol</td><td>ICGT</td></tr>
    <tr><td>Web</td><td><a href="https://www.icg-enterprise.co.uk">www.icg-enterprise.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 23.0 |
| 2019 | 22.0 |
| 2018 | 21.0 |
| 2017 | 20.0 |
| 2016 | 11.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
