---
title: "PACCAR INC (PCAR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PACCAR INC</td></tr>
    <tr><td>Symbol</td><td>PCAR</td></tr>
    <tr><td>Web</td><td><a href="https://www.paccar.com">www.paccar.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.66 |
| 2020 | 1.98 |
| 2019 | 3.58 |
| 2018 | 3.09 |
| 2017 | 2.19 |
| 2016 | 1.56 |
| 2015 | 2.32 |
| 2014 | 1.86 |
| 2013 | 1.7 |
| 2012 | 1.58 |
| 2011 | 1.3 |
| 2010 | 0.69 |
| 2009 | 0.54 |
| 2008 | 0.82 |
| 2007 | 1.88 |
| 2006 | 2.95 |
| 2005 | 2.87 |
| 2004 | 2.75 |
| 2003 | 2.06 |
| 2002 | 1.6 |
| 2001 | 1.45 |
| 2000 | 2.2 |
| 1999 | 2.4 |
| 1998 | 2.2 |
| 1997 | 2.2 |
| 1996 | 2.5 |
| 1995 | 4.0 |
| 1994 | 3.0 |
| 1993 | 2.0 |
| 1992 | 1.3 |
| 1991 | 1.1 |
| 1990 | 1.0 |
| 1989 | 2.5 |
| 1988 | 1.75 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
