---
title: "NAVIOS MARITIME PARTNERS L.P. (NMM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NAVIOS MARITIME PARTNERS L.P.</td></tr>
    <tr><td>Symbol</td><td>NMM</td></tr>
    <tr><td>Web</td><td><a href="https://www.navios-mlp.com">www.navios-mlp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.7 |
| 2019 | 0.92 |
| 2018 | 0.06 |
| 2015 | 1.539 |
| 2014 | 1.768 |
| 2013 | 0.884 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
