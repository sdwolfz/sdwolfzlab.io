---
title: "EASTERN BANKSHARES INC (EBC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>EASTERN BANKSHARES INC</td></tr>
    <tr><td>Symbol</td><td>EBC</td></tr>
    <tr><td>Web</td><td><a href="https://www.easternbank.com">www.easternbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.14 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
