---
title: "COSTCO WHOLESALE CORP (COST)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COSTCO WHOLESALE CORP</td></tr>
    <tr><td>Symbol</td><td>COST</td></tr>
    <tr><td>Web</td><td><a href="https://www.costco.com">www.costco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.49 |
| 2020 | 12.75 |
| 2019 | 2.52 |
| 2018 | 2.21 |
| 2017 | 8.95 |
| 2016 | 1.75 |
| 2015 | 6.555 |
| 2014 | 1.375 |
| 2013 | 1.205 |
| 2012 | 8.065 |
| 2011 | 0.925 |
| 2010 | 0.795 |
| 2009 | 0.7 |
| 2008 | 0.625 |
| 2007 | 0.565 |
| 2006 | 0.505 |
| 2005 | 0.445 |
| 2004 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
