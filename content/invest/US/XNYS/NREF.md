---
title: "NEXPOINT REAL ESTATE FINANCE INC (NREF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NEXPOINT REAL ESTATE FINANCE INC</td></tr>
    <tr><td>Symbol</td><td>NREF</td></tr>
    <tr><td>Web</td><td><a href="https://www.nexpointfinance.com">www.nexpointfinance.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.95 |
| 2020 | 1.42 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
