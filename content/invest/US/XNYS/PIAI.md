---
title: "PRIME IMPACT ACQUISITION I (PIAI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PRIME IMPACT ACQUISITION I</td></tr>
    <tr><td>Symbol</td><td>PIAI</td></tr>
    <tr><td>Web</td><td><a href="https://www.primeimpactcapital.com">www.primeimpactcapital.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
