---
title: "RESTAURANT BRANDS INTL INC (QSR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>RESTAURANT BRANDS INTL INC</td></tr>
    <tr><td>Symbol</td><td>QSR</td></tr>
    <tr><td>Web</td><td><a href="https://www.rbi.com">www.rbi.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.53 |
| 2020 | 2.08 |
| 2019 | 2.0 |
| 2018 | 1.8 |
| 2017 | 0.78 |
| 2016 | 0.62 |
| 2015 | 0.44 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
