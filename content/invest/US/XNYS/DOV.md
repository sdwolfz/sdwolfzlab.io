---
title: "DOVER CORP (DOV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DOVER CORP</td></tr>
    <tr><td>Symbol</td><td>DOV</td></tr>
    <tr><td>Web</td><td><a href="https://www.dovercorporation.com">www.dovercorporation.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.495 |
| 2020 | 1.97 |
| 2019 | 1.94 |
| 2018 | 1.9 |
| 2017 | 1.82 |
| 2016 | 1.72 |
| 2015 | 1.64 |
| 2014 | 1.55 |
| 2013 | 0.75 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
