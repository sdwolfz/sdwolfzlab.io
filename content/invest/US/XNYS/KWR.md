---
title: "QUAKER CHEMICAL CORP (KWR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>QUAKER CHEMICAL CORP</td></tr>
    <tr><td>Symbol</td><td>KWR</td></tr>
    <tr><td>Web</td><td><a href="https://www.quakerchem.com">www.quakerchem.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.79 |
| 2020 | 1.55 |
| 2019 | 1.51 |
| 2018 | 1.45 |
| 2017 | 1.4 |
| 2016 | 1.33 |
| 2015 | 1.24 |
| 2014 | 1.1 |
| 2013 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
