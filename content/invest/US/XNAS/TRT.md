---
title: "TRIO-TECH INTERNATIONAL (TRT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TRIO-TECH INTERNATIONAL</td></tr>
    <tr><td>Symbol</td><td>TRT</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2008 | 0.11 |
| 2006 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
