---
title: " (GFNCP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>GFNCP</td></tr>
    <tr><td>Web</td><td><a href="https://www.generalfinance.com">www.generalfinance.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 4.525 |
| 2020 | 9.15 |
| 2019 | 9.125 |
| 2018 | 9.125 |
| 2017 | 9.125 |
| 2016 | 9.15 |
| 2015 | 9.125 |
| 2014 | 9.125 |
| 2013 | 4.175 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
