---
title: "DIGIMARC CORP (NEW) (DMRC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>DIGIMARC CORP (NEW)</td></tr>
    <tr><td>Symbol</td><td>DMRC</td></tr>
    <tr><td>Web</td><td><a href="https://www.digimarc.com">www.digimarc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 0.22 |
| 2013 | 0.44 |
| 2012 | 0.33 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
