---
title: "WINTRUST FINANCIAL CORP (WTFC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>WINTRUST FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>WTFC</td></tr>
    <tr><td>Web</td><td><a href="https://www.wintrust.com">www.wintrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.62 |
| 2020 | 1.12 |
| 2019 | 1.0 |
| 2018 | 0.76 |
| 2017 | 0.56 |
| 2016 | 0.48 |
| 2015 | 0.44 |
| 2014 | 0.4 |
| 2013 | 0.18 |
| 2012 | 0.18 |
| 2011 | 0.18 |
| 2010 | 0.18 |
| 2009 | 0.27 |
| 2008 | 0.36 |
| 2007 | 0.32 |
| 2006 | 0.28 |
| 2005 | 0.24 |
| 2004 | 0.2 |
| 2003 | 0.16 |
| 2002 | 0.15 |
| 2001 | 0.14 |
| 2000 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
