---
title: "Centamin PLC (CEY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Centamin PLC</td></tr>
    <tr><td>Symbol</td><td>CEY</td></tr>
    <tr><td>Web</td><td><a href="https://www.centamin.com">www.centamin.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 9.0 |
| 2019 | 10.0 |
| 2018 | 5.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
