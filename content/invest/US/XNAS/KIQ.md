---
title: "KELSO TECHNOLOGIES (KIQ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>KELSO TECHNOLOGIES</td></tr>
    <tr><td>Symbol</td><td>KIQ</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 0.03 |
| 2014 | 0.01 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
