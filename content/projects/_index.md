---
title: "Projects"
---

## Games

That's what life is all about!

* [Triangulate](/games/triangulate/)

## Personal

These are projects I've started in my spare time, to scratch an itch or automate
manual labour.

* [CacheBox](/projects/cache-box/)
* [Docker Here](/projects/docker-here/)
* [Docker Images](/projects/docker-images/)
* [Docker Projects](/projects/docker-projects/)
* [Dotfiles](/projects/dotfiles/)
* [Envcheck](/projects/envcheck/)
* [PKGBUILDS](/projects/pkgbuilds/)
* [Shell Scripts Package Manager](/projects/sspm/)
* [Spyglass](/projects/spyglass/)
* [Stonks ETL](/projects/stonks-etl/)

## Forks

There's good software out there, but sometimes you need something a little
different, or the authors won't update it. Can't let it go to waste!

* [Forks](/projects/forks/)

## Collaborations

Sometimes it's nice to help out upstream directly, or simply let somebody else
carry out your legacy!

* [Collaborations](/projects/collaborations/)

## Experimental

All my experiments are conducted in this mono-repository:

* https://gitlab.com/sdwolfz/experimental

## Other

Visit my GitLab page to see other bits and pieces:

* https://gitlab.com/users/sdwolfz/projects
