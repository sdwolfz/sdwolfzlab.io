---
title: "Computacenter (CCC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Computacenter</td></tr>
    <tr><td>Symbol</td><td>CCC</td></tr>
    <tr><td>Web</td><td><a href="https://www.computacenter.com">www.computacenter.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 37.0 |
| 2018 | 30.3 |
| 2017 | 26.1 |
| 2016 | 22.2 |
| 2015 | 21.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
