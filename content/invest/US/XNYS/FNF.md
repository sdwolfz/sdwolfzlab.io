---
title: "FIDELITY NATIONAL FINANCIAL (FNF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FIDELITY NATIONAL FINANCIAL</td></tr>
    <tr><td>Symbol</td><td>FNF</td></tr>
    <tr><td>Web</td><td><a href="https://www.fnf.com">www.fnf.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.36 |
| 2020 | 1.35 |
| 2019 | 1.26 |
| 2018 | 1.2 |
| 2017 | 14.521 |
| 2016 | 0.88 |
| 2015 | 0.8 |
| 2014 | 0.73 |
| 2013 | 0.34 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
