---
title: "CITIGROUP INC DEP SHS REPSTG 1/1000 PFD K (CpK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CITIGROUP INC DEP SHS REPSTG 1/1000 PFD K</td></tr>
    <tr><td>Symbol</td><td>CpK</td></tr>
    <tr><td>Web</td><td><a href="http://www.citigroup.com">www.citigroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.725 |
| 2019 | 1.585 |
| 2018 | 1.435 |
| 2017 | 1.28 |
| 2016 | 1.202 |
| 2015 | 1.131 |
| 2014 | 1.33 |
| 2013 | 0.77 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
