---
title: "LANCASTER COLONY CORP (LANC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LANCASTER COLONY CORP</td></tr>
    <tr><td>Symbol</td><td>LANC</td></tr>
    <tr><td>Web</td><td><a href="https://www.lancastercolony.com">www.lancastercolony.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.75 |
| 2020 | 2.85 |
| 2019 | 2.65 |
| 2018 | 2.45 |
| 2017 | 2.25 |
| 2016 | 2.05 |
| 2015 | 6.88 |
| 2014 | 1.78 |
| 2013 | 1.62 |
| 2012 | 6.46 |
| 2011 | 1.35 |
| 2010 | 1.23 |
| 2009 | 1.155 |
| 2008 | 1.125 |
| 2007 | 1.09 |
| 2006 | 1.05 |
| 2005 | 3.01 |
| 2004 | 0.94 |
| 2003 | 0.83 |
| 2002 | 0.74 |
| 2001 | 0.69 |
| 2000 | 0.65 |
| 1999 | 0.61 |
| 1998 | 0.57 |
| 1997 | 0.76 |
| 1996 | 0.69 |
| 1995 | 0.61 |
| 1994 | 0.57 |
| 1993 | 0.58 |
| 1992 | 0.69 |
| 1991 | 0.82 |
| 1990 | 0.77 |
| 1989 | 0.73 |
| 1988 | 0.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
