---
title: "H&E EQUIPMENT SERVICES LLC (HEES)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>H&E EQUIPMENT SERVICES LLC</td></tr>
    <tr><td>Symbol</td><td>HEES</td></tr>
    <tr><td>Web</td><td><a href="https://www.he-equipment.com">www.he-equipment.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.275 |
| 2020 | 1.1 |
| 2019 | 1.1 |
| 2018 | 1.1 |
| 2017 | 1.1 |
| 2016 | 1.1 |
| 2015 | 1.05 |
| 2014 | 0.5 |
| 2012 | 7.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
