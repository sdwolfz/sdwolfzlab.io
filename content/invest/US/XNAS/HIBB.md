---
title: "HIBBETT SPORTS INC (HIBB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HIBBETT SPORTS INC</td></tr>
    <tr><td>Symbol</td><td>HIBB</td></tr>
    <tr><td>Web</td><td><a href="https://www.hibbett.com">www.hibbett.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
