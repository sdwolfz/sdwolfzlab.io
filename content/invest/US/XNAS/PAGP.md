---
title: " (PAGP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>PAGP</td></tr>
    <tr><td>Web</td><td><a href="https://www.plainsallamerican.com">www.plainsallamerican.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.36 |
| 2020 | 0.9 |
| 2019 | 1.38 |
| 2018 | 1.2 |
| 2017 | 1.95 |
| 2016 | 0.9 |
| 2015 | 0.883 |
| 2014 | 0.67 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
