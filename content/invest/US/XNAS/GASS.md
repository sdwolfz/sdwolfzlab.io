---
title: "STEALTHGAS INC (GASS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>STEALTHGAS INC</td></tr>
    <tr><td>Symbol</td><td>GASS</td></tr>
    <tr><td>Web</td><td><a href="https://www.stealthgas.com">www.stealthgas.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2009 | 0.188 |
| 2008 | 0.75 |
| 2007 | 0.75 |
| 2006 | 0.75 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
