---
title: "NIELSEN HLDGS PLC (NLSN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NIELSEN HLDGS PLC</td></tr>
    <tr><td>Symbol</td><td>NLSN</td></tr>
    <tr><td>Web</td><td><a href="https://www.nielsen.com">www.nielsen.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 0.24 |
| 2019 | 1.11 |
| 2018 | 1.39 |
| 2017 | 1.33 |
| 2016 | 1.21 |
| 2015 | 1.09 |
| 2014 | 0.95 |
| 2013 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
