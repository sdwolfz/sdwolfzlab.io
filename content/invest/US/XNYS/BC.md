---
title: "BRUNSWICK CORP (BC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BRUNSWICK CORP</td></tr>
    <tr><td>Symbol</td><td>BC</td></tr>
    <tr><td>Web</td><td><a href="https://www.brunswick.com">www.brunswick.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.605 |
| 2020 | 0.99 |
| 2019 | 0.87 |
| 2018 | 0.78 |
| 2017 | 0.685 |
| 2016 | 0.615 |
| 2015 | 0.525 |
| 2014 | 0.45 |
| 2013 | 0.1 |
| 2012 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
