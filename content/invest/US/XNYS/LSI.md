---
title: " (LSI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>LSI</td></tr>
    <tr><td>Web</td><td><a href="https://www.lifestorage.com">www.lifestorage.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.48 |
| 2020 | 4.28 |
| 2019 | 4.0 |
| 2018 | 4.0 |
| 2017 | 3.95 |
| 2016 | 1.9 |
| 2013 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
