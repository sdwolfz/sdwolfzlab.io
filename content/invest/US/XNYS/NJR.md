---
title: "NEW JERSEY RESOURCES CORP (NJR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NEW JERSEY RESOURCES CORP</td></tr>
    <tr><td>Symbol</td><td>NJR</td></tr>
    <tr><td>Web</td><td><a href="https://www.njresources.com">www.njresources.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.666 |
| 2020 | 1.292 |
| 2019 | 1.21 |
| 2018 | 1.13 |
| 2017 | 1.056 |
| 2016 | 0.975 |
| 2015 | 0.915 |
| 2014 | 2.16 |
| 2013 | 1.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
