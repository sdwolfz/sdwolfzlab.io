---
title: "RALPH LAUREN CORP (RL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>RALPH LAUREN CORP</td></tr>
    <tr><td>Symbol</td><td>RL</td></tr>
    <tr><td>Web</td><td><a href="https://www.ralphlauren.com">www.ralphlauren.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.688 |
| 2019 | 2.689 |
| 2018 | 2.375 |
| 2017 | 2.0 |
| 2016 | 2.0 |
| 2015 | 2.0 |
| 2014 | 1.8 |
| 2013 | 1.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
