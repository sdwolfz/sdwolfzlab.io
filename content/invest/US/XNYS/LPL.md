---
title: " (LPL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>LPL</td></tr>
    <tr><td>Web</td><td><a href="https://www.lgdisplay.com">www.lgdisplay.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 0.174 |
| 2014 | 0.158 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
