---
title: "COMERICA INC (CMA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>COMERICA INC</td></tr>
    <tr><td>Symbol</td><td>CMA</td></tr>
    <tr><td>Web</td><td><a href="https://www.comerica.com">www.comerica.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.36 |
| 2020 | 2.72 |
| 2019 | 2.68 |
| 2018 | 1.84 |
| 2017 | 1.09 |
| 2016 | 0.89 |
| 2015 | 0.83 |
| 2014 | 0.79 |
| 2013 | 0.34 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
