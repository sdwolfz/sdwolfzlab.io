---
title: " (HTHT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>HTHT</td></tr>
    <tr><td>Web</td><td><a href="https://www.huazhu.com">www.huazhu.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.34 |
| 2018 | 0.34 |
| 2017 | 0.64 |
| 2015 | 0.68 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
