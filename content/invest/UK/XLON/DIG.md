---
title: "Dunedin Inc. (DIG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Dunedin Inc.</td></tr>
    <tr><td>Symbol</td><td>DIG</td></tr>
    <tr><td>Web</td><td><a href="https://www.dunedinincomegrowth.co.uk">www.dunedinincomegrowth.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 12.7 |
| 2019 | 12.45 |
| 2018 | 12.1 |
| 2017 | 11.7 |
| 2016 | 11.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
