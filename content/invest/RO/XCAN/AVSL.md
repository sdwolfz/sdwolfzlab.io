---
title: "AVICOLA SA SLOBOZIA (AVSL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>AVICOLA SA SLOBOZIA</td></tr>
    <tr><td>Symbol</td><td>AVSL</td></tr>
    <tr><td>Web</td><td><a href="https://www.avicola-slobozia.ro">www.avicola-slobozia.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.069 |
| 2018 | 0.96 |
| 2017 | 0.96 |
| 2015 | 0.25 |
| 2003 | 0.003 |
| 2002 | 0.004 |
| 2001 | 0.6392 |
| 2000 | 0.1429 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
