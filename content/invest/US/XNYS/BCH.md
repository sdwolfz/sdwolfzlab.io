---
title: " (BCH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BCH</td></tr>
    <tr><td>Web</td><td><a href="https://www.bancochile.cl">www.bancochile.cl</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.374 |
| 2020 | 0.625 |
| 2019 | 0.793 |
| 2018 | 0.773 |
| 2017 | 2.124 |
| 2016 | 2.338 |
| 2015 | 2.584 |
| 2014 | 6.562 |
| 2013 | 3.32 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
