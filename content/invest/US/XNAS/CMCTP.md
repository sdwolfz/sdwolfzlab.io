---
title: " (CMCTP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CMCTP</td></tr>
    <tr><td>Web</td><td><a href="https://www.cimcommercial.com">www.cimcommercial.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.56 |
| 2019 | 1.56 |
| 2018 | 1.738 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
