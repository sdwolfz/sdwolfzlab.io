---
title: "HEXCEL CORPORATION (HXL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HEXCEL CORPORATION</td></tr>
    <tr><td>Symbol</td><td>HXL</td></tr>
    <tr><td>Web</td><td><a href="https://www.hexcel.com">www.hexcel.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.17 |
| 2019 | 0.64 |
| 2018 | 0.55 |
| 2017 | 0.47 |
| 2016 | 0.43 |
| 2015 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
