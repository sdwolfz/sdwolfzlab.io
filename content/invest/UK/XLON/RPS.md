---
title: "RPS Group (RPS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>RPS Group</td></tr>
    <tr><td>Symbol</td><td>RPS</td></tr>
    <tr><td>Web</td><td><a href="https://www.rpsgroup.com">www.rpsgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 4.42 |
| 2018 | 9.88 |
| 2016 | 9.74 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
