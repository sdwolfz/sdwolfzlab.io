---
title: "ENERSYS (ENS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ENERSYS</td></tr>
    <tr><td>Symbol</td><td>ENS</td></tr>
    <tr><td>Web</td><td><a href="https://www.enersys.com">www.enersys.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.175 |
| 2020 | 0.7 |
| 2019 | 0.7 |
| 2018 | 0.7 |
| 2017 | 0.7 |
| 2016 | 0.7 |
| 2015 | 0.7 |
| 2014 | 0.65 |
| 2013 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
