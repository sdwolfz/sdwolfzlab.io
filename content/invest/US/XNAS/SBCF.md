---
title: "SEACOAST BANKING OF FLORIDA (SBCF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SEACOAST BANKING OF FLORIDA</td></tr>
    <tr><td>Symbol</td><td>SBCF</td></tr>
    <tr><td>Web</td><td><a href="https://www.seacoastbanking.com">www.seacoastbanking.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.13 |
| 2009 | 0.01 |
| 2008 | 0.34 |
| 2007 | 0.64 |
| 2006 | 0.61 |
| 2005 | 0.58 |
| 2004 | 0.54 |
| 2003 | 0.48 |
| 2002 | 0.81 |
| 2001 | 1.14 |
| 2000 | 1.06 |
| 1999 | 0.98 |
| 1998 | 0.9 |
| 1997 | 0.82 |
| 1996 | 0.65 |
| 1995 | 0.54 |
| 1994 | 0.49 |
| 1993 | 0.45 |
| 1992 | 0.41 |
| 1991 | 0.4 |
| 1990 | 0.4 |
| 1989 | 0.37 |
| 1988 | 0.09 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
