---
title: "SHUTTERSTOCK INC (SSTK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SHUTTERSTOCK INC</td></tr>
    <tr><td>Symbol</td><td>SSTK</td></tr>
    <tr><td>Web</td><td><a href="https://www.shutterstock.com">www.shutterstock.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.42 |
| 2020 | 0.68 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
