---
title: "FIRST NATIONAL CORPORATION(VA) (FXNC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST NATIONAL CORPORATION(VA)</td></tr>
    <tr><td>Symbol</td><td>FXNC</td></tr>
    <tr><td>Web</td><td><a href="https://www.fbvirginia.com">www.fbvirginia.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 0.44 |
| 2019 | 0.27 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
