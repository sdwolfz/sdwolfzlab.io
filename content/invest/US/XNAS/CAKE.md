---
title: "CHEESECAKE FACTORY (CAKE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CHEESECAKE FACTORY</td></tr>
    <tr><td>Symbol</td><td>CAKE</td></tr>
    <tr><td>Web</td><td><a href="https://www.thecheesecakefactory.com">www.thecheesecakefactory.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.36 |
| 2019 | 1.38 |
| 2018 | 1.24 |
| 2017 | 1.06 |
| 2016 | 0.88 |
| 2015 | 0.73 |
| 2014 | 0.61 |
| 2013 | 0.52 |
| 2012 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
