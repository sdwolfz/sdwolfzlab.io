---
title: "Serco (SRP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Serco</td></tr>
    <tr><td>Symbol</td><td>SRP</td></tr>
    <tr><td>Web</td><td><a href="https://www.serco.com">www.serco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.4 |
| 2019 | 1.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
