---
title: "STAFFING 360 SOLUTIONS INC (STAF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>STAFFING 360 SOLUTIONS INC</td></tr>
    <tr><td>Symbol</td><td>STAF</td></tr>
    <tr><td>Web</td><td><a href="https://www.staffing360solutions.com">www.staffing360solutions.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.04 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
