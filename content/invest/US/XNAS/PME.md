---
title: "PINGTAN MARINE ENTERPRISE LTD (PME)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PINGTAN MARINE ENTERPRISE LTD</td></tr>
    <tr><td>Symbol</td><td>PME</td></tr>
    <tr><td>Web</td><td><a href="https://www.ptmarine.com">www.ptmarine.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.03 |
| 2017 | 0.04 |
| 2016 | 0.04 |
| 2015 | 0.03 |
| 2014 | 0.01 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
