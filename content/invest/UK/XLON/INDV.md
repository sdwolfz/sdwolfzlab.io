---
title: "Indivior (INDV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Indivior</td></tr>
    <tr><td>Symbol</td><td>INDV</td></tr>
    <tr><td>Web</td><td><a href="https://www.indivior.com">www.indivior.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 9.5 |
| 2015 | 12.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
