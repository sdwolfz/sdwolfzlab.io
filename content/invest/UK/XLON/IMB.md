---
title: "Imperial Brands (IMB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Imperial Brands</td></tr>
    <tr><td>Symbol</td><td>IMB</td></tr>
    <tr><td>Web</td><td><a href="https://www.imperial-tobacco.com">www.imperial-tobacco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 137.71 |
| 2019 | 206.57 |
| 2018 | 187.79 |
| 2017 | 170.7 |
| 2016 | 155.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
