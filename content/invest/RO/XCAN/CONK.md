---
title: "CONCAS SA BUZAU (CONK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>CONCAS SA BUZAU</td></tr>
    <tr><td>Symbol</td><td>CONK</td></tr>
    <tr><td>Web</td><td><a href="https://www.concas.ro">www.concas.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.85 |
| 2018 | 0.6 |
| 2017 | 0.6 |
| 2016 | 0.6 |
| 2005 | 0.047 |
| 2004 | 0.1 |
| 2002 | 0.13 |
| 2001 | 0.066 |
| 2000 | 0.13 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
