---
title: "PRACTIC SA Bucuresti (PRBU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>PRACTIC SA Bucuresti</td></tr>
    <tr><td>Symbol</td><td>PRBU</td></tr>
    <tr><td>Web</td><td><a href="https://www.practicsa.ro">www.practicsa.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 17.678 |
| 2019 | 16.84 |
| 2019 | 19.2855 |
| 2019 | 29.8 |
| 2018 | 46.1131 |
| 2017 | 50.1 |
| 2016 | 27.56 |
| 2015 | 10.943516 |
| 2015 | 19.193244 |
| 2014 | 16.83 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
