---
title: "LHC GROUP INC (LHCG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LHC GROUP INC</td></tr>
    <tr><td>Symbol</td><td>LHCG</td></tr>
    <tr><td>Web</td><td><a href="https://www.lhcgroup.com">www.lhcgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
