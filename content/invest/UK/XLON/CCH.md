---
title: "Coca-Cola HBC (CCH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Coca-Cola HBC</td></tr>
    <tr><td>Symbol</td><td>CCH</td></tr>
    <tr><td>Web</td><td><a href="https://www.coca-colahellenic.com">www.coca-colahellenic.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 62.0 |
| 2018 | 57.0 |
| 2017 | 54.0 |
| 2016 | 44.0 |
| 2015 | 40.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
