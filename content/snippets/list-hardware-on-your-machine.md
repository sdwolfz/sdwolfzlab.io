---
title: "List hardware on your machine"
date: 2020-11-18T19:33:10Z
type: posts
slug: list-hardware-on-your-machine
tags: ["linux"]
---

## Install

```sh
sudp pacman -S lshw
```

## Run

To get everything just execute:

```sh
sudo lshw -sanitize
```

But you probably want to narrow it down, so here are some common ones:

```sh
# Storage (SSD)
sudo lshw -class disk -class storage -sanitize

# Processor
sudo lshw -class processor -sanitize

# Battery
sudo lshw -class power -sanitize

# RAM
sudo lshw -class memory -sanitize

# WiFi
sudo lshw -class network -sanitize

# Camera and Microphone
sudo lshw -class multimedia -sanitize
```

Get a summary and browse for yourself:

```sh
sudo lshw -businfo
```

Now you can take the product info, search for it online and do whatever; watch
reviews, find it's price, purchase another, never purchase again, etc.
