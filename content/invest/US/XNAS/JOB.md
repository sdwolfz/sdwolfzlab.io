---
title: "GEE GROUP INC (JOB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GEE GROUP INC</td></tr>
    <tr><td>Symbol</td><td>JOB</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2007 | 0.1 |
| 2006 | 0.1 |
| 2000 | 0.25 |
| 1999 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
