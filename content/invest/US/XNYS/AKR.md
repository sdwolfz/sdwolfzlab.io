---
title: " (AKR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>AKR</td></tr>
    <tr><td>Web</td><td><a href="https://www.acadiarealty.com">www.acadiarealty.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 0.29 |
| 2019 | 1.13 |
| 2018 | 1.09 |
| 2017 | 1.05 |
| 2016 | 1.16 |
| 2015 | 1.22 |
| 2014 | 0.93 |
| 2013 | 0.44 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
