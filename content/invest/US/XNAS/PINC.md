---
title: "PREMIER INC (PINC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PREMIER INC</td></tr>
    <tr><td>Symbol</td><td>PINC</td></tr>
    <tr><td>Web</td><td><a href="https://www.premierinc.com">www.premierinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.38 |
| 2020 | 0.38 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
