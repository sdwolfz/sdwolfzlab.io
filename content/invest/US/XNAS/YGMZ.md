---
title: "MINGZHU LOGISTICS HLDGS LTD (YGMZ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MINGZHU LOGISTICS HLDGS LTD</td></tr>
    <tr><td>Symbol</td><td>YGMZ</td></tr>
    <tr><td>Web</td><td><a href="https://www.szygmz.com">www.szygmz.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
