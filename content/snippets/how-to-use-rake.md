---
title: "How to Use: rake"
date: 2020-10-16T18:15:11Z
type: posts
slug: how-to-use-rake
series: ["How to Use"]
tags: ["ruby", "development", "productivity"]
---

1. Add the `rake` gem to your `Gemfile`:

```ruby
gem 'rake'
```

2. Install it by running `bundle install`.

3. Create a `Rakefile` containing:

```ruby
# frozen_string_literal: true

Rake.add_rakelib(File.join('lib', 'tasks'))
```

4. Create a `hello.rake` file in `./lib/tasks` containing your code:

```ruby
# frozen_string_literal: true

# Require your code here:
# require_relative '../../config/environment'

namespace :hello do
  task :world do
    puts 'Hello World!'
  end

  task chain: :world do
    puts 'Chain!'
  end

  task :name, [:who] do |_task, args|
    puts "Hello #{args[:who]}!"
  end
end
```

5. Run it:

```sh
bundle exec rake hello:world
# Hello World!

bundle exec rake hello:chain
# Hello World!
# Chain!

bundle exec rake hello:name[Rake]
# Hello Rake!
```
