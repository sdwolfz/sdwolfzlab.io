---
title: " (CPAC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CPAC</td></tr>
    <tr><td>Web</td><td><a href="https://www.cementospacasmayo.com.pe">www.cementospacasmayo.com.pe</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.297 |
| 2019 | 0.488 |
| 2018 | 0.514 |
| 2017 | 0.498 |
| 2016 | 0.38 |
| 2015 | 0.392 |
| 2014 | 0.307 |
| 2013 | 0.151 |
| 2012 | 0.145 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
