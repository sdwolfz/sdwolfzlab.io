---
title: "VERIZON COMMUNICATIONS (VZ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>VERIZON COMMUNICATIONS</td></tr>
    <tr><td>Symbol</td><td>VZ</td></tr>
    <tr><td>Web</td><td><a href="https://www.verizon.com">www.verizon.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.254 |
| 2020 | 2.472 |
| 2019 | 2.424 |
| 2018 | 2.373 |
| 2017 | 2.321 |
| 2016 | 2.272 |
| 2015 | 2.765 |
| 2014 | 2.14 |
| 2013 | 1.045 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
