---
title: "CABLE ONE INC (CABO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CABLE ONE INC</td></tr>
    <tr><td>Symbol</td><td>CABO</td></tr>
    <tr><td>Web</td><td><a href="https://ir.cableone.net">ir.cableone.net</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.5 |
| 2020 | 9.5 |
| 2019 | 8.5 |
| 2018 | 7.5 |
| 2017 | 6.5 |
| 2016 | 6.0 |
| 2015 | 1.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
