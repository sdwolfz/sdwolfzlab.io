---
title: "CENTRAIS ELETR BRAS SA - ELETROBRAS ADR-EACH REPR 1 PRF'B'SHS NPV (EBR.B)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CENTRAIS ELETR BRAS SA - ELETROBRAS ADR-EACH REPR 1 PRF'B'SHS NPV</td></tr>
    <tr><td>Symbol</td><td>EBR.B</td></tr>
    <tr><td>Web</td><td><a href="http://www.eletrobras.com">www.eletrobras.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.316 |
| 2019 | 0.676 |
| 2017 | 0.463 |
| 2015 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
