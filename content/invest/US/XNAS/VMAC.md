---
title: "VISTAS MEDIA ACQUISITION COM INC (VMAC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>VISTAS MEDIA ACQUISITION COM INC</td></tr>
    <tr><td>Symbol</td><td>VMAC</td></tr>
    <tr><td>Web</td><td><a href="https://www.vmac.media">www.vmac.media</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
