---
title: "COMCM SA CONSTANTA (CMCM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>COMCM SA CONSTANTA</td></tr>
    <tr><td>Symbol</td><td>CMCM</td></tr>
    <tr><td>Web</td><td><a href="https://www.comcm.ro">www.comcm.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 0.0024 |
| 2013 | 0.004 |
| 2008 | 0.0163 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
