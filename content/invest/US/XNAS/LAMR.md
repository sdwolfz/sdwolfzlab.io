---
title: " (LAMR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>LAMR</td></tr>
    <tr><td>Web</td><td><a href="https://www.lamar.com">www.lamar.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.75 |
| 2020 | 2.5 |
| 2019 | 3.84 |
| 2018 | 3.65 |
| 2017 | 3.32 |
| 2016 | 3.02 |
| 2015 | 2.75 |
| 2014 | 2.5 |
| 2007 | 3.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
