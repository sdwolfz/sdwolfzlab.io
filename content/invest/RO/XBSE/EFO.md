---
title: "TURISM, HOTELURI, RESTAURANTE MAREA NEAGRA S.A. (EFO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>TURISM, HOTELURI, RESTAURANTE MAREA NEAGRA S.A.</td></tr>
    <tr><td>Symbol</td><td>EFO</td></tr>
    <tr><td>Web</td><td><a href="https://www.thrmareaneagra.ro">www.thrmareaneagra.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.007 |
| 2019 | 0.0055 |
| 2019 | 0.018 |
| 2018 | 0.00848 |
| 2017 | 0.00145 |
| 2016 | 0.0068 |
| 2015 | 0.0054 |
| 2014 | 0.0035 |
| 2012 | 0.0018 |
| 2003 | 0.0148 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
