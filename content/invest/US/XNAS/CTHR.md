---
title: "CHARLES & COLVARD (CTHR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CHARLES & COLVARD</td></tr>
    <tr><td>Symbol</td><td>CTHR</td></tr>
    <tr><td>Web</td><td><a href="https://www.charlesandcolvard.com">www.charlesandcolvard.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2007 | 0.08 |
| 2006 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
