---
title: "ICERP SA PLOIESTI (ICER)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/bvb/">BVB</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/bvb/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>ICERP SA PLOIESTI</td></tr>
    <tr><td>Symbol</td><td>ICER</td></tr>
    <tr><td>Web</td><td><a href="http://www.icerp.ro">www.icerp.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2006 | 1.9481 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
