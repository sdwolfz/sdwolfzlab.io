---
title: "MOLSON COORS BEVERAGE COMPANY (TAP.A)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MOLSON COORS BEVERAGE COMPANY</td></tr>
    <tr><td>Symbol</td><td>TAP.A</td></tr>
    <tr><td>Web</td><td><a href="https://www.molsoncoors.com">www.molsoncoors.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.57 |
| 2019 | 1.96 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
