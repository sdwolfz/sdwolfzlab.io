---
title: "MERIAN CHRYSALIS INVESTMENT COMPANY ORD NPV (MERI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>MERIAN CHRYSALIS INVESTMENT COMPANY ORD NPV</td></tr>
    <tr><td>Symbol</td><td>MERI</td></tr>
    <tr><td>Web</td><td><a href="http://www.merian.com">www.merian.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
