---
title: "Big Yellow (BYG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Big Yellow</td></tr>
    <tr><td>Symbol</td><td>BYG</td></tr>
    <tr><td>Web</td><td><a href="https://www.bigyellow.co.uk">www.bigyellow.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 33.8 |
| 2019 | 33.2 |
| 2018 | 30.8 |
| 2017 | 27.6 |
| 2016 | 24.9 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
