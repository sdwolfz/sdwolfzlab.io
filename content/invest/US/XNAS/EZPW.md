---
title: "EZCORP INC (EZPW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>EZCORP INC</td></tr>
    <tr><td>Symbol</td><td>EZPW</td></tr>
    <tr><td>Web</td><td><a href="https://www.ezcorp.com">www.ezcorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2000 | 0.013 |
| 1999 | 0.05 |
| 1998 | 0.025 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
