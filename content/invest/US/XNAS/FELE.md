---
title: "FRANKLIN ELECTRIC CO INC (FELE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FRANKLIN ELECTRIC CO INC</td></tr>
    <tr><td>Symbol</td><td>FELE</td></tr>
    <tr><td>Web</td><td><a href="https://www.franklin-electric.com">www.franklin-electric.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.35 |
| 2020 | 0.62 |
| 2019 | 0.58 |
| 2018 | 0.467 |
| 2017 | 0.423 |
| 2016 | 0.398 |
| 2015 | 0.382 |
| 2014 | 0.348 |
| 2013 | 0.377 |
| 2012 | 0.57 |
| 2011 | 0.535 |
| 2010 | 0.515 |
| 2009 | 0.5 |
| 2008 | 0.495 |
| 2007 | 0.47 |
| 2006 | 0.43 |
| 2005 | 0.38 |
| 2004 | 0.46 |
| 2003 | 0.55 |
| 2002 | 0.63 |
| 2001 | 0.94 |
| 2000 | 0.86 |
| 1999 | 0.77 |
| 1998 | 0.66 |
| 1997 | 0.57 |
| 1996 | 0.46 |
| 1995 | 0.38 |
| 1994 | 0.29 |
| 1993 | 0.15 |
| 1989 | 25.48 |
| 1988 | 0.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
