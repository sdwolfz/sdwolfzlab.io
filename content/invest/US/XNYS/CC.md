---
title: "THE CHEMOURS COMPANY LLC (CC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>THE CHEMOURS COMPANY LLC</td></tr>
    <tr><td>Symbol</td><td>CC</td></tr>
    <tr><td>Web</td><td><a href="https://www.chemours.com">www.chemours.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.5 |
| 2020 | 1.0 |
| 2019 | 1.0 |
| 2018 | 0.84 |
| 2017 | 0.12 |
| 2016 | 0.12 |
| 2015 | 0.58 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
