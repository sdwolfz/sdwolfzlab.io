---
title: "AZZ INC (AZZ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AZZ INC</td></tr>
    <tr><td>Symbol</td><td>AZZ</td></tr>
    <tr><td>Web</td><td><a href="https://www.azz.com">www.azz.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.34 |
| 2020 | 0.68 |
| 2019 | 0.68 |
| 2018 | 0.68 |
| 2017 | 0.68 |
| 2016 | 0.62 |
| 2015 | 0.6 |
| 2014 | 0.57 |
| 2013 | 0.28 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
