---
title: "FOX CORPORATION (FOXA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FOX CORPORATION</td></tr>
    <tr><td>Symbol</td><td>FOXA</td></tr>
    <tr><td>Web</td><td><a href="https://www.foxcorporation.com">www.foxcorporation.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.23 |
| 2020 | 0.46 |
| 2019 | 0.46 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
