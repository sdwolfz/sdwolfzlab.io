---
title: " (LYG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>LYG</td></tr>
    <tr><td>Web</td><td><a href="https://www.lloydsbankinggroup.com">www.lloydsbankinggroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.032 |
| 2020 | 0.105 |
| 2019 | 0.164 |
| 2018 | 0.165 |
| 2017 | 0.166 |
| 2016 | 0.16 |
| 2015 | 0.092 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
