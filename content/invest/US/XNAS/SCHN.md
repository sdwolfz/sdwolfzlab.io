---
title: "SCHNITZER STEEL (SCHN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SCHNITZER STEEL</td></tr>
    <tr><td>Symbol</td><td>SCHN</td></tr>
    <tr><td>Web</td><td><a href="https://www.schnitzersteel.com">www.schnitzersteel.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.375 |
| 2020 | 0.75 |
| 2019 | 0.75 |
| 2018 | 0.75 |
| 2017 | 0.75 |
| 2016 | 0.75 |
| 2015 | 0.75 |
| 2014 | 0.75 |
| 2013 | 0.75 |
| 2012 | 0.58 |
| 2011 | 0.068 |
| 2010 | 0.068 |
| 2009 | 0.068 |
| 2008 | 0.068 |
| 2007 | 0.068 |
| 2006 | 0.068 |
| 2005 | 0.068 |
| 2004 | 0.076 |
| 2003 | 0.175 |
| 2002 | 0.2 |
| 2001 | 0.2 |
| 2000 | 0.2 |
| 1999 | 0.2 |
| 1998 | 0.2 |
| 1997 | 0.2 |
| 1996 | 0.2 |
| 1995 | 0.2 |
| 1994 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
