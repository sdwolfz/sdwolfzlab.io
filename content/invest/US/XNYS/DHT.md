---
title: "DHT HOLDINGS INC (DHT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DHT HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>DHT</td></tr>
    <tr><td>Web</td><td><a href="https://www.dhtankers.com">www.dhtankers.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.09 |
| 2020 | 1.35 |
| 2019 | 0.2 |
| 2018 | 0.08 |
| 2017 | 0.2 |
| 2016 | 0.71 |
| 2015 | 0.53 |
| 2014 | 0.08 |
| 2013 | 0.04 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
