---
title: "Schroders (SDR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Schroders</td></tr>
    <tr><td>Symbol</td><td>SDR</td></tr>
    <tr><td>Web</td><td><a href="https://www.schroders.com">www.schroders.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 114.0 |
| 2018 | 114.0 |
| 2017 | 98.0 |
| 2016 | 87.0 |
| 2015 | 83.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
