---
title: "Gcp Asset Bckd (GABI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Gcp Asset Bckd</td></tr>
    <tr><td>Symbol</td><td>GABI</td></tr>
    <tr><td>Web</td><td><a href="https://www.graviscapital.com">www.graviscapital.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 6.45 |
| 2018 | 6.35 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
