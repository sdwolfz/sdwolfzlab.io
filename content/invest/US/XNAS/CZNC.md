---
title: "CITIZENS AND NORTHERN CORP (CZNC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CITIZENS AND NORTHERN CORP</td></tr>
    <tr><td>Symbol</td><td>CZNC</td></tr>
    <tr><td>Web</td><td><a href="https://www.cnbankpa.com">www.cnbankpa.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.55 |
| 2020 | 1.08 |
| 2019 | 1.18 |
| 2018 | 1.08 |
| 2017 | 1.04 |
| 2016 | 1.04 |
| 2015 | 1.04 |
| 2014 | 1.04 |
| 2013 | 1.0 |
| 2012 | 0.84 |
| 2011 | 0.58 |
| 2010 | 0.39 |
| 2009 | 0.72 |
| 2008 | 0.96 |
| 2007 | 0.72 |
| 2006 | 0.72 |
| 2005 | 0.69 |
| 2004 | 0.66 |
| 2003 | 0.735 |
| 2002 | 0.86 |
| 2001 | 1.06 |
| 2000 | 0.72 |
| 1999 | 0.66 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
