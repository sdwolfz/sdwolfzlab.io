---
title: "Invesco Sel. Uk (IVPU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Invesco Sel. Uk</td></tr>
    <tr><td>Symbol</td><td>IVPU</td></tr>
    <tr><td>Web</td><td><a href="https://www.invescoperpetual.co.uk">www.invescoperpetual.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 13.65 |
| 2019 | 13.5 |
| 2018 | 13.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
