---
title: "HENRY JACK & ASSOCIATES INC (JKHY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HENRY JACK & ASSOCIATES INC</td></tr>
    <tr><td>Symbol</td><td>JKHY</td></tr>
    <tr><td>Web</td><td><a href="https://www.jackhenry.com">www.jackhenry.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.46 |
| 2020 | 1.72 |
| 2019 | 1.6 |
| 2018 | 1.48 |
| 2017 | 1.24 |
| 2016 | 1.12 |
| 2015 | 1.0 |
| 2014 | 0.88 |
| 2013 | 0.73 |
| 2012 | 0.46 |
| 2011 | 0.42 |
| 2010 | 0.38 |
| 2009 | 0.34 |
| 2008 | 0.3 |
| 2007 | 0.26 |
| 2006 | 0.22 |
| 2005 | 0.18 |
| 2004 | 0.16 |
| 2003 | 0.14 |
| 2002 | 0.14 |
| 2001 | 0.15 |
| 2000 | 0.25 |
| 1999 | 0.32 |
| 1998 | 0.26 |
| 1997 | 0.245 |
| 1996 | 0.28 |
| 1995 | 0.23 |
| 1994 | 0.21 |
| 1993 | 0.26 |
| 1992 | 0.33 |
| 1991 | 0.36 |
| 1990 | 0.28 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
