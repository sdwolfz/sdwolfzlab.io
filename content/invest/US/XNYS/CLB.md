---
title: "CORE LABORATORIES NV (CLB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CORE LABORATORIES NV</td></tr>
    <tr><td>Symbol</td><td>CLB</td></tr>
    <tr><td>Web</td><td><a href="https://www.corelab.com">www.corelab.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.02 |
| 2020 | 0.28 |
| 2019 | 2.2 |
| 2018 | 2.2 |
| 2017 | 2.2 |
| 2016 | 2.2 |
| 2015 | 2.2 |
| 2014 | 2.0 |
| 2013 | 0.64 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
