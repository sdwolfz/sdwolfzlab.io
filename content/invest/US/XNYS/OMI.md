---
title: "OWENS & MINOR INC (OMI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>OWENS & MINOR INC</td></tr>
    <tr><td>Symbol</td><td>OMI</td></tr>
    <tr><td>Web</td><td><a href="https://www.owens-minor.com">www.owens-minor.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.004 |
| 2020 | 0.008 |
| 2019 | 0.008 |
| 2018 | 0.855 |
| 2017 | 1.028 |
| 2016 | 1.02 |
| 2015 | 1.008 |
| 2014 | 1.0 |
| 2013 | 0.48 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
