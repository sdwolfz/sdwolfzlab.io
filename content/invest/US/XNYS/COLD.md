---
title: " (COLD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>COLD</td></tr>
    <tr><td>Web</td><td><a href="https://www.americold.com">www.americold.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.22 |
| 2020 | 0.84 |
| 2019 | 0.8 |
| 2018 | 0.704 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
