---
title: "SCULPTOR CAPITAL MANAGEMENT INC (SCU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SCULPTOR CAPITAL MANAGEMENT INC</td></tr>
    <tr><td>Symbol</td><td>SCU</td></tr>
    <tr><td>Web</td><td><a href="https://www.sculptor.com">www.sculptor.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.35 |
| 2020 | 0.53 |
| 2019 | 0.35 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
