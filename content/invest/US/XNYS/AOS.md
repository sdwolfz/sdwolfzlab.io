---
title: "SMITH A O CORP (AOS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SMITH A O CORP</td></tr>
    <tr><td>Symbol</td><td>AOS</td></tr>
    <tr><td>Web</td><td><a href="https://www.aosmith.com">www.aosmith.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.52 |
| 2020 | 0.98 |
| 2019 | 0.9 |
| 2018 | 0.76 |
| 2017 | 0.56 |
| 2016 | 0.72 |
| 2015 | 0.76 |
| 2014 | 0.6 |
| 2013 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
