---
title: "Fundsmith Emerg (FEET)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Fundsmith Emerg</td></tr>
    <tr><td>Symbol</td><td>FEET</td></tr>
    <tr><td>Web</td><td><a href="https://www.feetplc.co.uk">www.feetplc.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 3.2 |
| 2018 | 2.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
