---
title: "COMMUNICATIONS SYSTEMS INC (JCS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COMMUNICATIONS SYSTEMS INC</td></tr>
    <tr><td>Symbol</td><td>JCS</td></tr>
    <tr><td>Web</td><td><a href="https://www.commsystems.com">www.commsystems.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.04 |
| 2019 | 0.08 |
| 2018 | 0.14 |
| 2017 | 0.16 |
| 2016 | 0.4 |
| 2015 | 0.64 |
| 2014 | 0.64 |
| 2013 | 0.64 |
| 2012 | 0.64 |
| 2011 | 0.6 |
| 2010 | 0.59 |
| 2009 | 0.54 |
| 2008 | 0.48 |
| 2007 | 0.44 |
| 2006 | 0.36 |
| 2005 | 0.29 |
| 2004 | 0.19 |
| 2003 | 0.12 |
| 2002 | 0.04 |
| 2001 | 0.2 |
| 2000 | 0.4 |
| 1999 | 0.4 |
| 1998 | 0.39 |
| 1997 | 0.35 |
| 1996 | 0.31 |
| 1995 | 0.27 |
| 1994 | 0.23 |
| 1993 | 0.24 |
| 1992 | 0.35 |
| 1991 | 0.3 |
| 1990 | 0.24 |
| 1989 | 0.24 |
| 1988 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
