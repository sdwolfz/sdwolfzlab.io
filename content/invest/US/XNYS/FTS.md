---
title: "FORTIS INC (FTS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FORTIS INC</td></tr>
    <tr><td>Symbol</td><td>FTS</td></tr>
    <tr><td>Web</td><td><a href="https://www.fortisinc.com">www.fortisinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.386 |
| 2020 | 1.44 |
| 2019 | 1.378 |
| 2018 | 1.327 |
| 2017 | 1.258 |
| 2016 | 0.298 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
