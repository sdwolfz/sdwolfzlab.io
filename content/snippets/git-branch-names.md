---
title: "Git branch names"
date: 2021-10-13T13:54:36Z
type: posts
slug: git-branch-names
---

## Rules

Here are my rules for git branch names:

1. Don't start with the letter `m`, that's reserved for `master` or `main`.
2. No prefixes!
3. No ids inside branch names.
4. Use english words, more than one...
5. Pick either `-` or `_` for separator, but be consistent, never mix.
6. Use common sense!

That's it, simple to follow and tab complete in a terminal!

> **Note:** don't forget to delete barnches once they're merged, there's no use
> for them.

### Good

```sh
git switch -c resize-gallery
git switch -c navbar-for-admin-section
git switch -c concurrent-processing
```

### Bad

```sh
git switch -c my-new-feature         # Breaks rule 1. (starts with an `m`).
git switch -c feature/navbar         # Breaks rule 2. (prefixed with the ticket taxonomy).
git switch -c ZGLV-123865-new-colors # Breaks rule 3. (prefixed with the ticket ID).
git switch -c new-colors-ZGLV-123865 # Breaks rule 3. (contains a ticket ID).
git switch -c fix                    # Breaks rule 4. (uses one word).
git switch -c brenci                 # Breaks rule 4. (uses made up non english word).
git switch -c did-some_stuff         # Breaks rule 5. (uses multiple word separators).
git switch -c fixes-everywhere       # Breaks rule 6. (minimum neuron activity not reached).
```

### Exceptions

If you work with semver release branches, you can do soemthing like:

```sh
git switch -c 1.0-stable
git switch -c 1.1-stable
git switch -c 1.2-stable
git switch -c 2.0-stable
```

This is perfectly acceptable since it derives from rule `6`, by far the most
important one!

## Master vs Main

Use whichever, I don't care, but be consistent!
