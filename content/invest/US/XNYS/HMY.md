---
title: " (HMY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>HMY</td></tr>
    <tr><td>Web</td><td><a href="https://www.harmony.co.za">www.harmony.co.za</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.062 |
| 2017 | 0.055 |
| 2016 | 0.036 |
| 2013 | 0.047 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
