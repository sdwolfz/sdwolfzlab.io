---
title: "Balfour Beatty (BBY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Balfour Beatty</td></tr>
    <tr><td>Symbol</td><td>BBY</td></tr>
    <tr><td>Web</td><td><a href="https://www.balfourbeatty.com">www.balfourbeatty.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 6.4 |
| 2018 | 4.8 |
| 2017 | 3.6 |
| 2016 | 2.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
