---
title: " (HPP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>HPP</td></tr>
    <tr><td>Web</td><td><a href="https://www.hudsonpacificproperties.com">www.hudsonpacificproperties.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 1.0 |
| 2019 | 1.0 |
| 2018 | 1.0 |
| 2017 | 1.0 |
| 2016 | 0.8 |
| 2015 | 0.575 |
| 2014 | 0.5 |
| 2013 | 0.375 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
