---
title: "CROWN HOLDINGS INC (CCK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CROWN HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>CCK</td></tr>
    <tr><td>Web</td><td><a href="https://www.crowncork.com">www.crowncork.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
