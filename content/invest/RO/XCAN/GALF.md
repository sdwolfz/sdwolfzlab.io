---
title: "GALFINBAND SA GALATI (GALF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>GALFINBAND SA GALATI</td></tr>
    <tr><td>Symbol</td><td>GALF</td></tr>
    <tr><td>Web</td><td><a href="https://www.galfinband.ro">www.galfinband.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2003 | 0.22 |
| 2002 | 0.22 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
