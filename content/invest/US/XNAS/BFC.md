---
title: "BANK FIRST CORPORATION (BFC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BANK FIRST CORPORATION</td></tr>
    <tr><td>Symbol</td><td>BFC</td></tr>
    <tr><td>Web</td><td><a href="https://www.bankfirstwi.bank">www.bankfirstwi.bank</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.42 |
| 2020 | 0.81 |
| 2019 | 0.8 |
| 2018 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
