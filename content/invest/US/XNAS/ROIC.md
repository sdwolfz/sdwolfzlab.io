---
title: " (ROIC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ROIC</td></tr>
    <tr><td>Web</td><td><a href="https://www.roireit.net">www.roireit.net</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.22 |
| 2020 | 0.2 |
| 2019 | 0.788 |
| 2018 | 0.78 |
| 2017 | 0.75 |
| 2016 | 0.72 |
| 2015 | 0.68 |
| 2014 | 0.64 |
| 2013 | 0.6 |
| 2012 | 0.53 |
| 2011 | 0.39 |
| 2010 | 0.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
