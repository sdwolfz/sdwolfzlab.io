---
title: "AMERICAN INTERNATIONAL GROUP INC (AIG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AMERICAN INTERNATIONAL GROUP INC</td></tr>
    <tr><td>Symbol</td><td>AIG</td></tr>
    <tr><td>Web</td><td><a href="https://www.aig.com">www.aig.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.32 |
| 2020 | 1.28 |
| 2019 | 1.28 |
| 2018 | 1.28 |
| 2017 | 1.28 |
| 2016 | 1.28 |
| 2015 | 0.81 |
| 2014 | 0.5 |
| 2013 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
