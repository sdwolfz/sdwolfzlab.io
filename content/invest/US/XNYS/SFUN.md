---
title: " (SFUN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SFUN</td></tr>
    <tr><td>Web</td><td><a href="https://www.fang.com">www.fang.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 0.19 |
| 2014 | 0.19 |
| 2013 | 0.196 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
