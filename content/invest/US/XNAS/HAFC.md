---
title: "HANMI FINANCIAL CORP (HAFC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HANMI FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>HAFC</td></tr>
    <tr><td>Web</td><td><a href="https://www.hanmi.com">www.hanmi.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.22 |
| 2020 | 0.52 |
| 2019 | 0.96 |
| 2018 | 0.96 |
| 2017 | 0.8 |
| 2016 | 0.66 |
| 2015 | 0.47 |
| 2014 | 0.28 |
| 2013 | 0.14 |
| 2008 | 0.09 |
| 2007 | 0.24 |
| 2006 | 0.24 |
| 2005 | 0.2 |
| 2004 | 0.4 |
| 2003 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
