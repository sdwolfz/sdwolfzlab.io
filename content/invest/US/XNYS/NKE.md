---
title: "NIKE INC (NKE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NIKE INC</td></tr>
    <tr><td>Symbol</td><td>NKE</td></tr>
    <tr><td>Web</td><td><a href="https://www.nike.com">www.nike.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.275 |
| 2020 | 1.01 |
| 2019 | 0.905 |
| 2018 | 0.82 |
| 2017 | 0.74 |
| 2016 | 0.66 |
| 2015 | 1.0 |
| 2014 | 1.0 |
| 2013 | 0.45 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
