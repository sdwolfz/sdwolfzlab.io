---
title: "ASTRONICS CORP (ATRO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ASTRONICS CORP</td></tr>
    <tr><td>Symbol</td><td>ATRO</td></tr>
    <tr><td>Web</td><td><a href="https://www.astronics.com">www.astronics.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2000 | 0.9 |
| 1994 | 0.01 |
| 1988 | 0.035 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
