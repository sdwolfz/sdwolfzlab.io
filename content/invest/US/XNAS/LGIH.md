---
title: "LGI HOMES INC (LGIH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LGI HOMES INC</td></tr>
    <tr><td>Symbol</td><td>LGIH</td></tr>
    <tr><td>Web</td><td><a href="https://www.lgihomes.com">www.lgihomes.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
