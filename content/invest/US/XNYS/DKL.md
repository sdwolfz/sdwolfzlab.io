---
title: " (DKL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>DKL</td></tr>
    <tr><td>Web</td><td><a href="https://www.deleklogistics.com">www.deleklogistics.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.83 |
| 2020 | 3.58 |
| 2019 | 3.36 |
| 2018 | 3.035 |
| 2017 | 2.79 |
| 2016 | 2.485 |
| 2015 | 2.16 |
| 2014 | 1.805 |
| 2013 | 0.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
