---
title: "XEROX HOLDINGS CORPORATION (XRX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>XEROX HOLDINGS CORPORATION</td></tr>
    <tr><td>Symbol</td><td>XRX</td></tr>
    <tr><td>Web</td><td><a href="https://www.xerox.com">www.xerox.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 1.0 |
| 2019 | 1.0 |
| 2018 | 1.0 |
| 2017 | 0.626 |
| 2016 | 0.312 |
| 2015 | 0.28 |
| 2014 | 0.252 |
| 2013 | 0.116 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
