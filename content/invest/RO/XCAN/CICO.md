---
title: "CI-CO SA (CICO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>CI-CO SA</td></tr>
    <tr><td>Symbol</td><td>CICO</td></tr>
    <tr><td>Web</td><td><a href="https://www.ci-co.ro">www.ci-co.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.1438 |
| 2019 | 1.2986 |
| 2018 | 0.6385 |
| 2017 | 0.792826 |
| 2016 | 9.97 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
