---
title: "SERVOTRONICS INC (SVT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SERVOTRONICS INC</td></tr>
    <tr><td>Symbol</td><td>SVT</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.16 |
| 2018 | 0.16 |
| 2017 | 0.15 |
| 2016 | 0.15 |
| 2015 | 0.15 |
| 2013 | 0.16 |
| 2012 | 0.3 |
| 2011 | 0.3 |
| 2010 | 0.15 |
| 2009 | 0.15 |
| 2008 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
