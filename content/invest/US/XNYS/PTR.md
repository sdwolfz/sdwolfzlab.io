---
title: " (PTR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>PTR</td></tr>
    <tr><td>Web</td><td><a href="https://www.petrochina.com.cn">www.petrochina.com.cn</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.182 |
| 2020 | 1.937 |
| 2019 | 2.156 |
| 2018 | 1.962 |
| 2017 | 1.393 |
| 2016 | 0.591 |
| 2015 | 2.252 |
| 2014 | 4.701 |
| 2013 | 4.214 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
