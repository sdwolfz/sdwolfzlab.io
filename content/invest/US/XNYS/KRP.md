---
title: " (KRP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>KRP</td></tr>
    <tr><td>Web</td><td><a href="https://www.kimbellrp.com">www.kimbellrp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.46 |
| 2020 | 0.87 |
| 2019 | 1.58 |
| 2018 | 1.66 |
| 2017 | 0.84 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
