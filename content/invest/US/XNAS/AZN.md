---
title: " (AZN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>AZN</td></tr>
    <tr><td>Web</td><td><a href="https://www.astrazeneca.com">www.astrazeneca.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.95 |
| 2020 | 1.4 |
| 2019 | 1.4 |
| 2018 | 1.4 |
| 2017 | 1.4 |
| 2016 | 1.4 |
| 2015 | 2.35 |
| 2014 | 2.8 |
| 2013 | 2.8 |
| 2012 | 2.85 |
| 2011 | 2.7 |
| 2010 | 2.41 |
| 2009 | 2.09 |
| 2008 | 1.9 |
| 2007 | 1.75 |
| 2006 | 1.41 |
| 2005 | 1.025 |
| 2004 | 0.835 |
| 2003 | 0.777 |
| 2002 | 0.778 |
| 2001 | 0.778 |
| 2000 | 0.256 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
