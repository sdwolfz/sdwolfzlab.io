---
title: "PETAL SA HUSI (PETY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>PETAL SA HUSI</td></tr>
    <tr><td>Symbol</td><td>PETY</td></tr>
    <tr><td>Web</td><td><a href="https://www.petal.ro">www.petal.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.005 |
| 2019 | 0.00525 |
| 2018 | 0.01 |
| 2017 | 0.01 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
