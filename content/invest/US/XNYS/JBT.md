---
title: "JOHN BEAN TECHNOLOGIES CORPORATION (JBT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>JOHN BEAN TECHNOLOGIES CORPORATION</td></tr>
    <tr><td>Symbol</td><td>JBT</td></tr>
    <tr><td>Web</td><td><a href="https://www.jbtc.com">www.jbtc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.4 |
| 2019 | 0.4 |
| 2018 | 0.4 |
| 2017 | 0.4 |
| 2016 | 0.4 |
| 2015 | 0.37 |
| 2014 | 0.36 |
| 2013 | 0.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
