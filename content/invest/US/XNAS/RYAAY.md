---
title: " (RYAAY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>RYAAY</td></tr>
    <tr><td>Web</td><td><a href="https://www.ryanair.com">www.ryanair.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 3.786 |
| 2012 | 2.208 |
| 2010 | 2.29 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
