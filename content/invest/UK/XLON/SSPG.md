---
title: "SSP Group (SSPG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>SSP Group</td></tr>
    <tr><td>Symbol</td><td>SSPG</td></tr>
    <tr><td>Web</td><td><a href="https://www.foodtravelexperts.com">www.foodtravelexperts.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 11.8 |
| 2018 | 10.2 |
| 2017 | 8.37 |
| 2016 | 5.58 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
