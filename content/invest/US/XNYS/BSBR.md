---
title: " (BSBR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BSBR</td></tr>
    <tr><td>Web</td><td><a href="https://www.santander.com.br">www.santander.com.br</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.188 |
| 2020 | 0.545 |
| 2019 | 0.305 |
| 2018 | 0.493 |
| 2017 | 0.226 |
| 2016 | 0.108 |
| 2015 | 0.273 |
| 2014 | 0.791 |
| 2013 | 0.108 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
