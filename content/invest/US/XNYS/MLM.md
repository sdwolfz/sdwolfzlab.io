---
title: "MARTIN MARIETTA MATERIALS INC (MLM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MARTIN MARIETTA MATERIALS INC</td></tr>
    <tr><td>Symbol</td><td>MLM</td></tr>
    <tr><td>Web</td><td><a href="https://www.martinmarietta.com">www.martinmarietta.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.57 |
| 2020 | 2.24 |
| 2019 | 2.06 |
| 2018 | 1.84 |
| 2017 | 1.72 |
| 2016 | 1.64 |
| 2015 | 1.6 |
| 2014 | 1.6 |
| 2013 | 1.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
