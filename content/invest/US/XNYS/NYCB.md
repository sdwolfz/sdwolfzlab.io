---
title: "NEW YORK COMMUNITY BANCORP INC (NYCB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NEW YORK COMMUNITY BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>NYCB</td></tr>
    <tr><td>Web</td><td><a href="https://www.myNYCB.com">www.myNYCB.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.34 |
| 2020 | 0.68 |
| 2019 | 0.68 |
| 2018 | 0.68 |
| 2017 | 0.68 |
| 2016 | 0.68 |
| 2015 | 1.0 |
| 2014 | 1.0 |
| 2013 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
