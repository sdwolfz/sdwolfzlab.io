---
title: "JAKKS PACIFIC INC (JAKK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>JAKKS PACIFIC INC</td></tr>
    <tr><td>Symbol</td><td>JAKK</td></tr>
    <tr><td>Web</td><td><a href="https://www.jakks.com">www.jakks.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2013 | 0.14 |
| 2012 | 0.4 |
| 2011 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
