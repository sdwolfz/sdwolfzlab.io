---
title: "CEMTREX INC (CETX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CEMTREX INC</td></tr>
    <tr><td>Symbol</td><td>CETX</td></tr>
    <tr><td>Web</td><td><a href="https://www.cemtrex.com">www.cemtrex.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 0.02 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
