---
title: "Macfarlane Grp. (MACF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Macfarlane Grp.</td></tr>
    <tr><td>Symbol</td><td>MACF</td></tr>
    <tr><td>Web</td><td><a href="https://www.macfarlanegroup.net">www.macfarlanegroup.net</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 2.45 |
| 2018 | 2.3 |
| 2017 | 2.1 |
| 2016 | 1.95 |
| 2015 | 1.82 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
