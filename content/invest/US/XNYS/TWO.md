---
title: "TWO HARBORS INVESMENT CORP (TWO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TWO HARBORS INVESMENT CORP</td></tr>
    <tr><td>Symbol</td><td>TWO</td></tr>
    <tr><td>Web</td><td><a href="http://www.twoharborsinvestment.com">www.twoharborsinvestment.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.5 |
| 2019 | 1.67 |
| 2018 | 1.88 |
| 2017 | 5.155 |
| 2016 | 0.93 |
| 2015 | 1.04 |
| 2014 | 1.04 |
| 2013 | 0.85 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
