---
title: "Asia Dragon (DGN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Asia Dragon</td></tr>
    <tr><td>Symbol</td><td>DGN</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 4.75 |
| 2019 | 4.75 |
| 2018 | 4.0 |
| 2017 | 3.3 |
| 2016 | 3.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
