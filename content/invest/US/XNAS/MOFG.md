---
title: "MIDWESTONE FINANCIAL GROUP INC (MOFG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MIDWESTONE FINANCIAL GROUP INC</td></tr>
    <tr><td>Symbol</td><td>MOFG</td></tr>
    <tr><td>Web</td><td><a href="https://www.midwestone.com">www.midwestone.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.45 |
| 2020 | 0.88 |
| 2019 | 0.81 |
| 2018 | 0.78 |
| 2017 | 0.67 |
| 2016 | 0.64 |
| 2015 | 0.6 |
| 2014 | 0.58 |
| 2013 | 0.5 |
| 2012 | 0.36 |
| 2011 | 0.22 |
| 2010 | 0.2 |
| 2009 | 0.303 |
| 2008 | 0.458 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
