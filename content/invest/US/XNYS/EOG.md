---
title: "EOG RESOURCES INC (EOG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>EOG RESOURCES INC</td></tr>
    <tr><td>Symbol</td><td>EOG</td></tr>
    <tr><td>Web</td><td><a href="https://www.eogresources.com">www.eogresources.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.787 |
| 2020 | 1.412 |
| 2019 | 1.014 |
| 2018 | 0.758 |
| 2017 | 0.672 |
| 2016 | 0.672 |
| 2015 | 0.672 |
| 2014 | 0.606 |
| 2013 | 0.376 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
