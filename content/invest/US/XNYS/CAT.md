---
title: "CATERPILLAR INC (CAT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CATERPILLAR INC</td></tr>
    <tr><td>Symbol</td><td>CAT</td></tr>
    <tr><td>Web</td><td><a href="https://www.caterpillar.com">www.caterpillar.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.06 |
| 2020 | 4.12 |
| 2019 | 3.78 |
| 2018 | 3.28 |
| 2017 | 3.1 |
| 2016 | 3.08 |
| 2015 | 2.94 |
| 2014 | 2.6 |
| 2013 | 1.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
