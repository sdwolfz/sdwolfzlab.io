---
title: "Xp Power (XPP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Xp Power</td></tr>
    <tr><td>Symbol</td><td>XPP</td></tr>
    <tr><td>Web</td><td><a href="https://www.xppower.com">www.xppower.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 91.0 |
| 2018 | 85.0 |
| 2017 | 78.0 |
| 2016 | 71.0 |
| 2015 | 66.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
