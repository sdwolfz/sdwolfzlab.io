---
title: "TOMPKINS FINANCIAL CORPORATION (TMP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TOMPKINS FINANCIAL CORPORATION</td></tr>
    <tr><td>Symbol</td><td>TMP</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.08 |
| 2020 | 2.1 |
| 2019 | 2.02 |
| 2018 | 1.94 |
| 2017 | 1.82 |
| 2016 | 1.77 |
| 2015 | 1.7 |
| 2014 | 1.62 |
| 2013 | 1.54 |
| 2012 | 1.46 |
| 2011 | 1.4 |
| 2010 | 1.02 |
| 2009 | 1.36 |
| 2008 | 1.32 |
| 2007 | 1.24 |
| 2006 | 0.9 |
| 2005 | 0.9 |
| 2004 | 1.2 |
| 2003 | 0.9 |
| 2002 | 1.16 |
| 2001 | 1.1 |
| 2000 | 1.08 |
| 1999 | 0.27 |
| 1996 | 1.1 |
| 1995 | 0.78 |
| 1994 | 1.0 |
| 1993 | 0.96 |
| 1992 | 0.78 |
| 1991 | 0.64 |
| 1990 | 0.58 |
| 1989 | 0.625 |
| 1988 | 0.175 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
