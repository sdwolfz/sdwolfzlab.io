---
title: "TOWNSQUARE MEDIA INC (TSQ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TOWNSQUARE MEDIA INC</td></tr>
    <tr><td>Symbol</td><td>TSQ</td></tr>
    <tr><td>Web</td><td><a href="https://www.townsquaremedia.com">www.townsquaremedia.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.075 |
| 2019 | 0.3 |
| 2018 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
