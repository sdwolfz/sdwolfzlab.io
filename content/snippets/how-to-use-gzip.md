---
title: "How to Use: gzip"
date: 2021-05-03T20:09:12Z
type: posts
slug: how-to-use-gzip
series: ["How to Use"]
tags: ["gzip", "linux", "archive"]
---

## Install

```sh
sudo pacman -S gzip
```

## Prerequisites

1. Create a playground:

```sh
mkdir archive-test
cd archive-test
```

2. Create some content:

```sh
mkdir content
echo content1 >> content/file1
echo content2 >> content/file2
echo content3 >> content/file3
```

3. Back up your content:

```sh
tar -cf archive-1.tar content
```

## Compress

```sh
gzip -9 -f -k archive-1.tar
```

## Extract

```sh
gunzip -f -k archive-1.tar.gz
```

Or:

```sh
gzip -d -f -k archive-1.tar.gz
```

## Cleanup

Don't forget to remove your test files after playing with this:

```sh
cd ..
rm -rf archive-test
```
