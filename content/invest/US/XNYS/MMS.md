---
title: "MAXIMUS INC (MMS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MAXIMUS INC</td></tr>
    <tr><td>Symbol</td><td>MMS</td></tr>
    <tr><td>Web</td><td><a href="https://www.maximus.com">www.maximus.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.56 |
| 2020 | 1.12 |
| 2019 | 1.03 |
| 2018 | 0.385 |
| 2017 | 0.18 |
| 2016 | 0.18 |
| 2015 | 0.18 |
| 2014 | 0.18 |
| 2013 | 0.09 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
