---
title: " (ORAN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ORAN</td></tr>
    <tr><td>Web</td><td><a href="https://www.orange.com">www.orange.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.526 |
| 2019 | 0.615 |
| 2018 | 0.645 |
| 2017 | 0.519 |
| 2016 | 0.427 |
| 2015 | 0.43 |
| 2014 | 0.735 |
| 2013 | 0.326 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
