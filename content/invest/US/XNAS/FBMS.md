---
title: "FIRST BANCSHARES INC MISS (FBMS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST BANCSHARES INC MISS</td></tr>
    <tr><td>Symbol</td><td>FBMS</td></tr>
    <tr><td>Web</td><td><a href="https://www.thefirstbank.com">www.thefirstbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.27 |
| 2020 | 0.42 |
| 2019 | 0.31 |
| 2018 | 0.2 |
| 2017 | 0.15 |
| 2016 | 0.15 |
| 2015 | 0.15 |
| 2014 | 0.15 |
| 2013 | 0.15 |
| 2012 | 0.15 |
| 2011 | 0.15 |
| 2010 | 0.15 |
| 2008 | 0.225 |
| 2007 | 0.525 |
| 2006 | 0.32 |
| 2005 | 0.1 |
| 2004 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
