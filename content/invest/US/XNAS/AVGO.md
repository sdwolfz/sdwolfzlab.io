---
title: "BROADCOM INC (AVGO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BROADCOM INC</td></tr>
    <tr><td>Symbol</td><td>AVGO</td></tr>
    <tr><td>Web</td><td><a href="https://www.broadcom.com">www.broadcom.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 3.6 |
| 2020 | 13.35 |
| 2019 | 11.2 |
| 2018 | 7.9 |
| 2017 | 4.81 |
| 2016 | 2.52 |
| 2015 | 1.64 |
| 2014 | 1.23 |
| 2013 | 0.88 |
| 2012 | 0.61 |
| 2011 | 0.4 |
| 2010 | 0.07 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
