---
title: "GOLAR LNG LIMITED (GLNG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GOLAR LNG LIMITED</td></tr>
    <tr><td>Symbol</td><td>GLNG</td></tr>
    <tr><td>Web</td><td><a href="https://www.golarlng.com">www.golarlng.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.3 |
| 2018 | 0.375 |
| 2017 | 0.2 |
| 2016 | 0.2 |
| 2015 | 1.8 |
| 2014 | 1.8 |
| 2013 | 1.35 |
| 2012 | 1.925 |
| 2011 | 1.125 |
| 2010 | 1.14 |
| 2008 | 1.0 |
| 2007 | 2.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
