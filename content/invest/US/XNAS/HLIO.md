---
title: "HELIOS TECHNOLOGIES INC (HLIO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HELIOS TECHNOLOGIES INC</td></tr>
    <tr><td>Symbol</td><td>HLIO</td></tr>
    <tr><td>Web</td><td><a href="https://www.heliostechnologies.com">www.heliostechnologies.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.18 |
| 2020 | 0.36 |
| 2019 | 0.36 |
| 2018 | 0.36 |
| 2017 | 0.29 |
| 2016 | 0.4 |
| 2015 | 0.45 |
| 2014 | 1.45 |
| 2013 | 0.45 |
| 2012 | 1.48 |
| 2011 | 0.47 |
| 2010 | 0.86 |
| 2009 | 0.45 |
| 2008 | 0.45 |
| 2007 | 0.415 |
| 2006 | 0.4 |
| 2005 | 0.35 |
| 2004 | 0.215 |
| 2003 | 2.16 |
| 2002 | 0.16 |
| 2001 | 0.16 |
| 2000 | 0.16 |
| 1999 | 0.16 |
| 1998 | 0.16 |
| 1997 | 0.14 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
