---
title: "SUNESIS PHARMACEUTICALS INC (SNSS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SUNESIS PHARMACEUTICALS INC</td></tr>
    <tr><td>Symbol</td><td>SNSS</td></tr>
    <tr><td>Web</td><td><a href="http://www.sunesis.com">www.sunesis.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
