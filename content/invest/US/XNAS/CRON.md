---
title: "CRONOS GROUP INC (CRON)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CRONOS GROUP INC</td></tr>
    <tr><td>Symbol</td><td>CRON</td></tr>
    <tr><td>Web</td><td><a href="https://www.thecronosgroup.com">www.thecronosgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
