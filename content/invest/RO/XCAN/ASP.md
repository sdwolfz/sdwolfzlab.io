---
title: "RAFINARIA ASTRA ROMANA S.A. (ASP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>RAFINARIA ASTRA ROMANA S.A.</td></tr>
    <tr><td>Symbol</td><td>ASP</td></tr>
    <tr><td>Web</td><td><a href="https://www.astrarom.ro">www.astrarom.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2001 | 0.002 |
| 2000 | 0.0389 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
