---
title: "CINTAS CORP (CTAS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CINTAS CORP</td></tr>
    <tr><td>Symbol</td><td>CTAS</td></tr>
    <tr><td>Web</td><td><a href="https://www.cintas.com">www.cintas.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.5 |
| 2020 | 3.51 |
| 2019 | 2.55 |
| 2018 | 2.05 |
| 2017 | 1.62 |
| 2016 | 1.33 |
| 2015 | 1.05 |
| 2014 | 1.7 |
| 2013 | 0.77 |
| 2012 | 0.64 |
| 2011 | 0.54 |
| 2010 | 0.97 |
| 2009 | 0.47 |
| 2008 | 0.46 |
| 2007 | 0.39 |
| 2006 | 0.35 |
| 2005 | 0.32 |
| 2004 | 0.29 |
| 2003 | 0.27 |
| 2002 | 0.25 |
| 2001 | 0.22 |
| 2000 | 0.28 |
| 1999 | 0.22 |
| 1998 | 0.18 |
| 1997 | 0.3 |
| 1996 | 0.25 |
| 1995 | 0.2 |
| 1994 | 0.17 |
| 1993 | 0.14 |
| 1992 | 0.22 |
| 1991 | 0.285 |
| 1990 | 0.23 |
| 1989 | 0.17 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
