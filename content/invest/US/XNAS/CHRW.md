---
title: "CH ROBINSON WORLDWIDE INC (CHRW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CH ROBINSON WORLDWIDE INC</td></tr>
    <tr><td>Symbol</td><td>CHRW</td></tr>
    <tr><td>Web</td><td><a href="https://www.chrobinson.com">www.chrobinson.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.51 |
| 2020 | 2.04 |
| 2019 | 2.01 |
| 2018 | 1.88 |
| 2017 | 1.81 |
| 2016 | 1.74 |
| 2015 | 1.57 |
| 2014 | 1.43 |
| 2013 | 1.4 |
| 2012 | 1.34 |
| 2011 | 1.2 |
| 2010 | 1.33 |
| 2009 | 0.97 |
| 2008 | 0.9 |
| 2007 | 0.76 |
| 2006 | 0.57 |
| 2005 | 0.58 |
| 2004 | 0.51 |
| 2003 | 0.36 |
| 2002 | 0.26 |
| 2001 | 0.21 |
| 2000 | 0.29 |
| 1999 | 0.29 |
| 1998 | 0.25 |
| 1997 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
