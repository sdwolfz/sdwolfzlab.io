---
title: "BEASLEY BROADCAST GROUP INC (BBGI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BEASLEY BROADCAST GROUP INC</td></tr>
    <tr><td>Symbol</td><td>BBGI</td></tr>
    <tr><td>Web</td><td><a href="https://www.bbgi.com">www.bbgi.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.05 |
| 2019 | 0.2 |
| 2018 | 0.2 |
| 2017 | 0.18 |
| 2016 | 0.18 |
| 2015 | 0.18 |
| 2014 | 0.18 |
| 2013 | 0.045 |
| 2012 | 0.085 |
| 2008 | 0.175 |
| 2007 | 0.25 |
| 2006 | 0.25 |
| 2005 | 0.063 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
