---
title: " (EARN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>EARN</td></tr>
    <tr><td>Web</td><td><a href="https://www.earnreit.com">www.earnreit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 1.12 |
| 2019 | 1.18 |
| 2018 | 1.45 |
| 2017 | 1.57 |
| 2016 | 1.65 |
| 2015 | 2.0 |
| 2014 | 2.2 |
| 2013 | 1.14 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
