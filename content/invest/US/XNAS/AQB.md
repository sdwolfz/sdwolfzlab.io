---
title: "AQUABOUNTY TECHNOLOGIES INC (AQB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AQUABOUNTY TECHNOLOGIES INC</td></tr>
    <tr><td>Symbol</td><td>AQB</td></tr>
    <tr><td>Web</td><td><a href="https://www.aquabounty.com">www.aquabounty.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
