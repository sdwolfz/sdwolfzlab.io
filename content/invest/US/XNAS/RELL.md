---
title: "RICHARDSON ELECTRONICS (RELL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>RICHARDSON ELECTRONICS</td></tr>
    <tr><td>Symbol</td><td>RELL</td></tr>
    <tr><td>Web</td><td><a href="https://www.rell.com">www.rell.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 0.24 |
| 2019 | 0.24 |
| 2018 | 0.24 |
| 2017 | 0.24 |
| 2016 | 0.24 |
| 2015 | 0.24 |
| 2014 | 0.24 |
| 2013 | 0.24 |
| 2012 | 0.22 |
| 2011 | 0.17 |
| 2010 | 0.08 |
| 2009 | 0.08 |
| 2008 | 0.08 |
| 2007 | 0.16 |
| 2006 | 0.16 |
| 2005 | 0.16 |
| 2004 | 0.16 |
| 2003 | 0.16 |
| 2002 | 0.16 |
| 2001 | 0.16 |
| 2000 | 0.16 |
| 1999 | 0.16 |
| 1998 | 0.16 |
| 1997 | 0.16 |
| 1996 | 0.16 |
| 1995 | 0.16 |
| 1994 | 0.16 |
| 1993 | 0.16 |
| 1992 | 0.16 |
| 1991 | 0.16 |
| 1990 | 0.16 |
| 1989 | 0.16 |
| 1988 | 0.04 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
