---
title: " (HEP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>HEP</td></tr>
    <tr><td>Web</td><td><a href="https://www.hollyenergy.com">www.hollyenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.7 |
| 2020 | 1.723 |
| 2019 | 2.684 |
| 2018 | 2.63 |
| 2017 | 2.505 |
| 2016 | 2.32 |
| 2015 | 2.168 |
| 2014 | 2.044 |
| 2013 | 0.978 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
