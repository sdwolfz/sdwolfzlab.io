---
title: "REPUBLIC BANCORP INC (RBCAA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>REPUBLIC BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>RBCAA</td></tr>
    <tr><td>Web</td><td><a href="https://www.republicbank.com">www.republicbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.308 |
| 2020 | 1.144 |
| 2019 | 1.056 |
| 2018 | 0.968 |
| 2017 | 0.869 |
| 2016 | 0.825 |
| 2015 | 0.781 |
| 2014 | 0.737 |
| 2013 | 0.693 |
| 2012 | 1.749 |
| 2011 | 0.605 |
| 2010 | 0.561 |
| 2009 | 0.517 |
| 2008 | 0.473 |
| 2007 | 0.429 |
| 2006 | 0.385 |
| 2005 | 0.341 |
| 2004 | 0.297 |
| 2003 | 0.506 |
| 2002 | 0.209 |
| 2001 | 0.176 |
| 2000 | 0.151 |
| 1999 | 0.118 |
| 1998 | 0.055 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
