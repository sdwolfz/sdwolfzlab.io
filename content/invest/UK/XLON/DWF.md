---
title: "Dwf Group (DWF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Dwf Group</td></tr>
    <tr><td>Symbol</td><td>DWF</td></tr>
    <tr><td>Web</td><td><a href="https://www.dwf.law">www.dwf.law</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 3.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
