---
title: " (SNR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SNR</td></tr>
    <tr><td>Web</td><td><a href="https://www.newseniorinv.com">www.newseniorinv.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.13 |
| 2020 | 0.325 |
| 2019 | 0.52 |
| 2018 | 0.78 |
| 2017 | 1.04 |
| 2016 | 1.04 |
| 2015 | 0.75 |
| 2014 | 0.23 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
