---
title: "KEWAUNEE SCIENTIFIC CORP (KEQU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>KEWAUNEE SCIENTIFIC CORP</td></tr>
    <tr><td>Symbol</td><td>KEQU</td></tr>
    <tr><td>Web</td><td><a href="https://www.kewaunee.com">www.kewaunee.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.57 |
| 2018 | 0.72 |
| 2017 | 0.64 |
| 2016 | 0.56 |
| 2015 | 0.5 |
| 2014 | 0.46 |
| 2013 | 0.42 |
| 2012 | 0.4 |
| 2011 | 0.4 |
| 2010 | 0.4 |
| 2009 | 0.36 |
| 2008 | 0.31 |
| 2007 | 0.28 |
| 2006 | 0.28 |
| 2005 | 0.28 |
| 2004 | 0.28 |
| 2003 | 0.28 |
| 2002 | 0.28 |
| 2001 | 0.28 |
| 2000 | 0.28 |
| 1999 | 0.25 |
| 1998 | 0.21 |
| 1997 | 0.17 |
| 1996 | 0.04 |
| 1992 | 0.12 |
| 1991 | 0.16 |
| 1990 | 0.16 |
| 1989 | 0.14 |
| 1988 | 0.02 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
