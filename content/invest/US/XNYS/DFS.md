---
title: "DISCOVER FINANCIAL SERVICES (DFS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DISCOVER FINANCIAL SERVICES</td></tr>
    <tr><td>Symbol</td><td>DFS</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.88 |
| 2020 | 1.76 |
| 2019 | 1.68 |
| 2018 | 1.5 |
| 2017 | 1.3 |
| 2016 | 1.16 |
| 2015 | 1.08 |
| 2014 | 0.92 |
| 2013 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
