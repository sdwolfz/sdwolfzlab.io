---
title: "Brewin Dolphin (BRW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Brewin Dolphin</td></tr>
    <tr><td>Symbol</td><td>BRW</td></tr>
    <tr><td>Web</td><td><a href="https://www.brewindolphin.co.uk">www.brewindolphin.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 14.3 |
| 2019 | 16.4 |
| 2018 | 16.4 |
| 2017 | 15.0 |
| 2016 | 13.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
