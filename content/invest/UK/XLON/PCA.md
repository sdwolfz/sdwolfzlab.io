---
title: "Palace Capital (PCA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Palace Capital</td></tr>
    <tr><td>Symbol</td><td>PCA</td></tr>
    <tr><td>Web</td><td><a href="https://www.palacecapitalplc.com">www.palacecapitalplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 12.0 |
| 2019 | 19.0 |
| 2018 | 19.0 |
| 2017 | 18.5 |
| 2016 | 16.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
