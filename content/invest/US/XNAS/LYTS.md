---
title: "LSI INDUSTRIES (LYTS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LSI INDUSTRIES</td></tr>
    <tr><td>Symbol</td><td>LYTS</td></tr>
    <tr><td>Web</td><td><a href="https://www.lsi-industries.com">www.lsi-industries.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.2 |
| 2019 | 0.2 |
| 2018 | 0.2 |
| 2017 | 0.2 |
| 2016 | 0.2 |
| 2015 | 0.12 |
| 2014 | 0.19 |
| 2013 | 0.18 |
| 2012 | 0.42 |
| 2011 | 0.21 |
| 2010 | 0.2 |
| 2009 | 0.2 |
| 2008 | 0.5 |
| 2007 | 0.59 |
| 2006 | 0.49 |
| 2005 | 0.52 |
| 2004 | 0.288 |
| 2003 | 0.27 |
| 2002 | 0.24 |
| 2001 | 0.345 |
| 2000 | 0.375 |
| 1999 | 0.363 |
| 1998 | 0.318 |
| 1997 | 0.263 |
| 1996 | 0.21 |
| 1995 | 0.12 |
| 1994 | 0.04 |
| 1993 | 0.05 |
| 1992 | 0.05 |
| 1991 | 0.05 |
| 1990 | 0.05 |
| 1989 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
