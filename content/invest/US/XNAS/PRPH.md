---
title: "PROPHASE LABS INC (PRPH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PROPHASE LABS INC</td></tr>
    <tr><td>Symbol</td><td>PRPH</td></tr>
    <tr><td>Web</td><td><a href="https://www.prophaselabs.com">www.prophaselabs.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.5 |
| 2018 | 1.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
