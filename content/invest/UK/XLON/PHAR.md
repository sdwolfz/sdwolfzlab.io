---
title: "Pharos Energy (PHAR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Pharos Energy</td></tr>
    <tr><td>Symbol</td><td>PHAR</td></tr>
    <tr><td>Web</td><td><a href="https://www.pharos.energy">www.pharos.energy</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 5.5 |
| 2017 | 5.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
