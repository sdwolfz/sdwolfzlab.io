---
title: "Hochschild (HOC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Hochschild</td></tr>
    <tr><td>Symbol</td><td>HOC</td></tr>
    <tr><td>Web</td><td><a href="https://www.hochschildmining.com">www.hochschildmining.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 4.34 |
| 2018 | 3.92 |
| 2017 | 3.35 |
| 2016 | 2.76 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
