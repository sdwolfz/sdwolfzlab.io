---
title: "AUTOLIV INC (ALV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AUTOLIV INC</td></tr>
    <tr><td>Symbol</td><td>ALV</td></tr>
    <tr><td>Web</td><td><a href="https://www.autoliv.com">www.autoliv.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.24 |
| 2019 | 2.48 |
| 2018 | 42.51 |
| 2017 | 2.38 |
| 2016 | 2.3 |
| 2015 | 2.22 |
| 2014 | 2.12 |
| 2013 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
