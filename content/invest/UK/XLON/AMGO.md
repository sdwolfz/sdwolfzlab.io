---
title: "Amigo (AMGO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Amigo</td></tr>
    <tr><td>Symbol</td><td>AMGO</td></tr>
    <tr><td>Web</td><td><a href="https://www.amigoplc.com">www.amigoplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 3.1 |
| 2019 | 9.32 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
