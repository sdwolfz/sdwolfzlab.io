---
title: " (CUBE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CUBE</td></tr>
    <tr><td>Web</td><td><a href="https://www.cubesmart.com">www.cubesmart.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.34 |
| 2020 | 1.33 |
| 2019 | 1.29 |
| 2018 | 1.22 |
| 2017 | 1.11 |
| 2016 | 0.9 |
| 2015 | 0.69 |
| 2014 | 0.55 |
| 2013 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
