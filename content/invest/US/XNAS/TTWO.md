---
title: "TAKE TWO INTERACTIVE SOFTWARE INC (TTWO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TAKE TWO INTERACTIVE SOFTWARE INC</td></tr>
    <tr><td>Symbol</td><td>TTWO</td></tr>
    <tr><td>Web</td><td><a href="https://www.take2games.com">www.take2games.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
