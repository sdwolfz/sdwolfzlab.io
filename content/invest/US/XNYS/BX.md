---
title: "BLACKSTONE GROUP INC (BX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BLACKSTONE GROUP INC</td></tr>
    <tr><td>Symbol</td><td>BX</td></tr>
    <tr><td>Web</td><td><a href="https://www.blackstone.com">www.blackstone.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.78 |
| 2020 | 1.91 |
| 2019 | 1.92 |
| 2018 | 2.42 |
| 2017 | 2.32 |
| 2016 | 1.66 |
| 2015 | 2.9 |
| 2014 | 1.92 |
| 2013 | 0.46 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
