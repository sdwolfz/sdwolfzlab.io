---
title: " (AFIN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>AFIN</td></tr>
    <tr><td>Web</td><td><a href="https://www.americanfinancetrust.com">www.americanfinancetrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.425 |
| 2020 | 0.7 |
| 2019 | 1.1 |
| 2018 | 0.406 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
