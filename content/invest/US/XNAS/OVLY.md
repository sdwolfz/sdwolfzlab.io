---
title: "OAK VALLEY BANCORP (OVLY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>OAK VALLEY BANCORP</td></tr>
    <tr><td>Symbol</td><td>OVLY</td></tr>
    <tr><td>Web</td><td><a href="https://www.ovcb.com">www.ovcb.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.145 |
| 2020 | 0.28 |
| 2019 | 0.27 |
| 2018 | 0.26 |
| 2017 | 0.25 |
| 2016 | 0.24 |
| 2015 | 0.21 |
| 2014 | 0.165 |
| 2009 | 0.025 |
| 2008 | 0.075 |
| 2007 | 0.19 |
| 2006 | 0.19 |
| 2005 | 0.25 |
| 2004 | 0.29 |
| 2003 | 0.275 |
| 2002 | 0.375 |
| 2001 | 0.35 |
| 2000 | 0.3 |
| 1999 | 0.35 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
