---
title: "Derwent London (DLN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Derwent London</td></tr>
    <tr><td>Symbol</td><td>DLN</td></tr>
    <tr><td>Web</td><td><a href="https://www.derwentlondon.com">www.derwentlondon.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 72.45 |
| 2018 | 65.85 |
| 2017 | 59.73 |
| 2016 | 52.36 |
| 2015 | 43.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
