---
title: "INVESTORS TITLE CO (ITIC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>INVESTORS TITLE CO</td></tr>
    <tr><td>Symbol</td><td>ITIC</td></tr>
    <tr><td>Web</td><td><a href="https://www.invtitle.com">www.invtitle.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.44 |
| 2020 | 16.76 |
| 2019 | 9.6 |
| 2018 | 12.2 |
| 2017 | 3.75 |
| 2016 | 0.72 |
| 2015 | 0.4 |
| 2014 | 0.32 |
| 2013 | 0.32 |
| 2012 | 0.29 |
| 2011 | 0.28 |
| 2010 | 0.28 |
| 2009 | 0.28 |
| 2008 | 0.28 |
| 2007 | 0.24 |
| 2006 | 0.24 |
| 2005 | 0.16 |
| 2004 | 0.15 |
| 2003 | 0.12 |
| 2002 | 0.12 |
| 2001 | 0.12 |
| 2000 | 0.12 |
| 1999 | 0.12 |
| 1998 | 0.12 |
| 1997 | 0.12 |
| 1996 | 0.095 |
| 1995 | 0.08 |
| 1994 | 0.08 |
| 1993 | 0.055 |
| 1992 | 0.04 |
| 1991 | 0.04 |
| 1990 | 0.03 |
| 1989 | 0.04 |
| 1988 | 0.01 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
