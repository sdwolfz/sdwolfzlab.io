---
title: "GORMAN RUPP COMPANY (GRC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GORMAN RUPP COMPANY</td></tr>
    <tr><td>Symbol</td><td>GRC</td></tr>
    <tr><td>Web</td><td><a href="https://www.gormanrupp.com">www.gormanrupp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.31 |
| 2020 | 0.59 |
| 2019 | 0.55 |
| 2018 | 2.51 |
| 2017 | 0.47 |
| 2016 | 0.43 |
| 2015 | 0.405 |
| 2014 | 0.37 |
| 2013 | 0.172 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
