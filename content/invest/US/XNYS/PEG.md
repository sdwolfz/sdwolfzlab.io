---
title: "PUBLIC SERVICE ENTERPRISE GROUP INC (PEG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PUBLIC SERVICE ENTERPRISE GROUP INC</td></tr>
    <tr><td>Symbol</td><td>PEG</td></tr>
    <tr><td>Web</td><td><a href="https://www.pseg.com">www.pseg.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.02 |
| 2020 | 1.96 |
| 2019 | 1.88 |
| 2018 | 1.8 |
| 2017 | 1.72 |
| 2016 | 1.64 |
| 2015 | 1.56 |
| 2014 | 1.48 |
| 2013 | 0.72 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
