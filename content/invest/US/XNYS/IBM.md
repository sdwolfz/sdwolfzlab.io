---
title: "INTERNATIONAL BUS MACH CORP (IBM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>INTERNATIONAL BUS MACH CORP</td></tr>
    <tr><td>Symbol</td><td>IBM</td></tr>
    <tr><td>Web</td><td><a href="https://www.ibm.com">www.ibm.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 3.27 |
| 2020 | 6.51 |
| 2019 | 6.43 |
| 2018 | 6.21 |
| 2017 | 5.9 |
| 2016 | 5.5 |
| 2015 | 5.0 |
| 2014 | 4.25 |
| 2013 | 1.9 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
