---
title: "FAIMAR SA BAIA MARE (FMAR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>FAIMAR SA BAIA MARE</td></tr>
    <tr><td>Symbol</td><td>FMAR</td></tr>
    <tr><td>Web</td><td><a href="https://www.faimar.ro">www.faimar.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2005 | 0.0018 |
| 2004 | 0.0018 |
| 2003 | 0.0018 |
| 2001 | 0.0057 |
| 2000 | 0.0082 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
