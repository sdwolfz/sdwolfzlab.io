---
title: "SINTX TECHNOLOGIES INC (SINT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SINTX TECHNOLOGIES INC</td></tr>
    <tr><td>Symbol</td><td>SINT</td></tr>
    <tr><td>Web</td><td><a href="https://www.sintx.com">www.sintx.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
