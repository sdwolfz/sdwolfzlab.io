---
title: " (AMT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>AMT</td></tr>
    <tr><td>Web</td><td><a href="https://www.americantower.com">www.americantower.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.24 |
| 2020 | 4.53 |
| 2019 | 3.78 |
| 2018 | 3.15 |
| 2017 | 2.62 |
| 2016 | 2.17 |
| 2015 | 1.81 |
| 2014 | 1.4 |
| 2013 | 0.84 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
