---
title: "PNC FINANCIAL SERVICES GROUP (PNC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PNC FINANCIAL SERVICES GROUP</td></tr>
    <tr><td>Symbol</td><td>PNC</td></tr>
    <tr><td>Web</td><td><a href="https://www.pnc.com">www.pnc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.3 |
| 2020 | 4.6 |
| 2019 | 4.2 |
| 2018 | 3.4 |
| 2017 | 2.6 |
| 2016 | 2.12 |
| 2015 | 2.01 |
| 2014 | 1.88 |
| 2013 | 0.88 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
