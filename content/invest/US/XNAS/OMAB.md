---
title: " (OMAB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>OMAB</td></tr>
    <tr><td>Web</td><td><a href="https://www.oma.aero">www.oma.aero</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 1.698 |
| 2018 | 1.621 |
| 2017 | 1.712 |
| 2016 | 1.605 |
| 2015 | 1.547 |
| 2014 | 1.842 |
| 2013 | 1.885 |
| 2012 | 1.007 |
| 2011 | 0.657 |
| 2010 | 0.569 |
| 2009 | 0.636 |
| 2008 | 0.742 |
| 2007 | 0.356 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
