---
title: "SAGA COMMUNICATIONS (SGA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SAGA COMMUNICATIONS</td></tr>
    <tr><td>Symbol</td><td>SGA</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.32 |
| 2019 | 1.2 |
| 2018 | 1.45 |
| 2017 | 2.0 |
| 2016 | 1.3 |
| 2015 | 1.1 |
| 2014 | 1.8 |
| 2013 | 1.8 |
| 2012 | 1.65 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
