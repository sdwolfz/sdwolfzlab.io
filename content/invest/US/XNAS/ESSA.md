---
title: "ESSA BANCORP INC (ESSA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ESSA BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>ESSA</td></tr>
    <tr><td>Web</td><td><a href="https://www.essabank.com">www.essabank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 0.44 |
| 2019 | 0.41 |
| 2018 | 0.37 |
| 2017 | 0.36 |
| 2016 | 0.36 |
| 2015 | 0.36 |
| 2014 | 0.28 |
| 2013 | 0.2 |
| 2012 | 0.2 |
| 2011 | 0.2 |
| 2010 | 0.2 |
| 2009 | 0.18 |
| 2008 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
