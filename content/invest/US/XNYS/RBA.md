---
title: "RITCHIE BROS AUCTIONEERS (RBA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>RITCHIE BROS AUCTIONEERS</td></tr>
    <tr><td>Symbol</td><td>RBA</td></tr>
    <tr><td>Web</td><td><a href="https://www.rbauction.com">www.rbauction.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.84 |
| 2019 | 0.76 |
| 2018 | 0.7 |
| 2017 | 0.68 |
| 2016 | 0.66 |
| 2015 | 0.6 |
| 2014 | 0.54 |
| 2013 | 0.26 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
