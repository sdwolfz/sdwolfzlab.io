---
title: "HOME BANCORP INC (HBCP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HOME BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>HBCP</td></tr>
    <tr><td>Web</td><td><a href="https://www.home24bank.com">www.home24bank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.45 |
| 2020 | 0.88 |
| 2019 | 0.84 |
| 2018 | 0.71 |
| 2017 | 0.55 |
| 2016 | 0.41 |
| 2015 | 0.3 |
| 2014 | 0.07 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
