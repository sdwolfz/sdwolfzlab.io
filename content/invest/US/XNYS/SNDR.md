---
title: "SCHNEIDER NATIONAL INC (SNDR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SCHNEIDER NATIONAL INC</td></tr>
    <tr><td>Symbol</td><td>SNDR</td></tr>
    <tr><td>Web</td><td><a href="https://www.schneider.com">www.schneider.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.14 |
| 2020 | 2.26 |
| 2019 | 0.24 |
| 2018 | 0.24 |
| 2017 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
