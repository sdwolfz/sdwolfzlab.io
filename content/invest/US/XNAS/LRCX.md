---
title: "LAM RESEARCH CORP (LRCX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LAM RESEARCH CORP</td></tr>
    <tr><td>Symbol</td><td>LRCX</td></tr>
    <tr><td>Web</td><td><a href="https://www.lamresearch.com">www.lamresearch.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.3 |
| 2020 | 4.9 |
| 2019 | 4.5 |
| 2018 | 3.8 |
| 2017 | 1.85 |
| 2016 | 1.35 |
| 2015 | 1.08 |
| 2014 | 0.54 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
