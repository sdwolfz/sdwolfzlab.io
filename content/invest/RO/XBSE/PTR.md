---
title: "ROMPETROL WELL SERVICES S.A. (PTR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>ROMPETROL WELL SERVICES S.A.</td></tr>
    <tr><td>Symbol</td><td>PTR</td></tr>
    <tr><td>Web</td><td><a href="https://www.petros.ro">www.petros.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.120683 |
| 2019 | 0.0437 |
| 2018 | 0.018 |
| 2017 | 0.0026 |
| 2014 | 0.02 |
| 2013 | 0.03 |
| 2012 | 0.029 |
| 2011 | 0.029 |
| 2010 | 0.015 |
| 2009 | 0.015 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
