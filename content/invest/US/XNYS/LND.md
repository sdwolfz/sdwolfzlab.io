---
title: " (LND)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>LND</td></tr>
    <tr><td>Web</td><td><a href="https://www.brasil-agro.com">www.brasil-agro.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.115 |
| 2019 | 0.202 |
| 2018 | 0.181 |
| 2017 | 0.065 |
| 2016 | 0.265 |
| 2015 | 0.345 |
| 2013 | 0.085 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
