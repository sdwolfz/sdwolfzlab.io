---
title: "Costain (COST)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Costain</td></tr>
    <tr><td>Symbol</td><td>COST</td></tr>
    <tr><td>Web</td><td><a href="https://www.costain.com">www.costain.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 3.8 |
| 2018 | 15.15 |
| 2017 | 14.0 |
| 2016 | 12.7 |
| 2015 | 11.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
