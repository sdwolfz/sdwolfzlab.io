---
title: "Polar Cap Gbl (PCFT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Polar Cap Gbl</td></tr>
    <tr><td>Symbol</td><td>PCFT</td></tr>
    <tr><td>Web</td><td><a href="https://www.polarcapitalglobalfinancialstrust.com">www.polarcapitalglobalfinancialstrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 4.4 |
| 2019 | 4.4 |
| 2018 | 4.05 |
| 2017 | 3.9 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
