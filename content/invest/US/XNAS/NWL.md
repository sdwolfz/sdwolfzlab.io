---
title: "NEWELL BRANDS INC (NWL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NEWELL BRANDS INC</td></tr>
    <tr><td>Symbol</td><td>NWL</td></tr>
    <tr><td>Web</td><td><a href="https://www.newellbrands.com">www.newellbrands.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.46 |
| 2020 | 0.92 |
| 2019 | 0.92 |
| 2018 | 0.92 |
| 2017 | 0.88 |
| 2016 | 0.76 |
| 2015 | 0.76 |
| 2014 | 0.66 |
| 2013 | 0.6 |
| 2012 | 0.43 |
| 2011 | 0.29 |
| 2010 | 0.2 |
| 2009 | 0.255 |
| 2008 | 0.84 |
| 2007 | 0.84 |
| 2006 | 0.84 |
| 2005 | 0.84 |
| 2004 | 0.84 |
| 2003 | 0.84 |
| 2002 | 0.84 |
| 2001 | 0.84 |
| 2000 | 0.42 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
