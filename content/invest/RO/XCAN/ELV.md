---
title: "ELVILA S.A. BUCURESTI (ELV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>ELVILA S.A. BUCURESTI</td></tr>
    <tr><td>Symbol</td><td>ELV</td></tr>
    <tr><td>Web</td><td><a href="https://www.elvila.ro">www.elvila.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
