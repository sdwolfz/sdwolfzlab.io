---
title: "CASEY'S GENERAL STORES INC (CASY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CASEY'S GENERAL STORES INC</td></tr>
    <tr><td>Symbol</td><td>CASY</td></tr>
    <tr><td>Web</td><td><a href="https://www.caseys.com">www.caseys.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.68 |
| 2020 | 1.28 |
| 2019 | 1.22 |
| 2018 | 1.1 |
| 2017 | 1.0 |
| 2016 | 0.92 |
| 2015 | 0.84 |
| 2014 | 0.76 |
| 2013 | 0.69 |
| 2012 | 0.63 |
| 2011 | 0.57 |
| 2010 | 0.405 |
| 2009 | 0.32 |
| 2008 | 0.28 |
| 2007 | 0.23 |
| 2006 | 0.19 |
| 2005 | 0.17 |
| 2004 | 0.15 |
| 2003 | 0.12 |
| 2002 | 0.1 |
| 2001 | 0.08 |
| 2000 | 0.07 |
| 1999 | 0.06 |
| 1998 | 0.075 |
| 1997 | 0.11 |
| 1996 | 0.1 |
| 1995 | 0.09 |
| 1994 | 0.098 |
| 1993 | 0.135 |
| 1992 | 0.12 |
| 1991 | 0.11 |
| 1990 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
