---
title: "SS&C TECHNOLOGIES HOLDINGS INC (SSNC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SS&C TECHNOLOGIES HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>SSNC</td></tr>
    <tr><td>Web</td><td><a href="https://www.ssctech.com">www.ssctech.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.16 |
| 2020 | 0.53 |
| 2019 | 0.425 |
| 2018 | 0.3 |
| 2017 | 0.265 |
| 2016 | 0.375 |
| 2015 | 0.5 |
| 2014 | 0.125 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
