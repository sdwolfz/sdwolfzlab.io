---
title: "CSG SYSTEMS INTL INC (CSGS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CSG SYSTEMS INTL INC</td></tr>
    <tr><td>Symbol</td><td>CSGS</td></tr>
    <tr><td>Web</td><td><a href="https://www.csgi.com">www.csgi.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 0.94 |
| 2019 | 0.89 |
| 2018 | 0.84 |
| 2017 | 0.79 |
| 2016 | 0.74 |
| 2015 | 0.7 |
| 2014 | 0.623 |
| 2013 | 0.45 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
