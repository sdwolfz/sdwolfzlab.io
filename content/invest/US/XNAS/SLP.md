---
title: "SIMULATIONS PLUS INC (SLP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SIMULATIONS PLUS INC</td></tr>
    <tr><td>Symbol</td><td>SLP</td></tr>
    <tr><td>Web</td><td><a href="https://www.simulations-plus.com">www.simulations-plus.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 0.24 |
| 2019 | 0.24 |
| 2018 | 0.24 |
| 2017 | 0.21 |
| 2016 | 0.2 |
| 2015 | 0.2 |
| 2014 | 0.2 |
| 2013 | 0.1 |
| 2012 | 0.34 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
