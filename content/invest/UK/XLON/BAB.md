---
title: "Babcock (BAB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Babcock</td></tr>
    <tr><td>Symbol</td><td>BAB</td></tr>
    <tr><td>Web</td><td><a href="https://www.babcockinternational.com">www.babcockinternational.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 7.2 |
| 2019 | 30.0 |
| 2018 | 29.5 |
| 2017 | 28.15 |
| 2016 | 25.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
