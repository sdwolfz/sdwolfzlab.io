---
title: "ASSOCIATED CAP GROUP INC (AC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ASSOCIATED CAP GROUP INC</td></tr>
    <tr><td>Symbol</td><td>AC</td></tr>
    <tr><td>Web</td><td><a href="https://www.associated-capital-group.com">www.associated-capital-group.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.2 |
| 2019 | 0.2 |
| 2018 | 0.2 |
| 2017 | 0.3 |
| 2016 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
