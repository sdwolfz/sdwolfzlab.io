---
title: "ELLOMAY CAPITAL LTD (ELLO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ELLOMAY CAPITAL LTD</td></tr>
    <tr><td>Symbol</td><td>ELLO</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.225 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
