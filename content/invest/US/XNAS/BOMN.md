---
title: "BOSTON OMAHA CORPORATION (BOMN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BOSTON OMAHA CORPORATION</td></tr>
    <tr><td>Symbol</td><td>BOMN</td></tr>
    <tr><td>Web</td><td><a href="https://www.bostonomaha.com">www.bostonomaha.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
