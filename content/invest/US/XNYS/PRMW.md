---
title: "PRIMO WATER CORPORATION CANADA (PRMW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PRIMO WATER CORPORATION CANADA</td></tr>
    <tr><td>Symbol</td><td>PRMW</td></tr>
    <tr><td>Web</td><td><a href="https://www.primowatercorp.com">www.primowatercorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
