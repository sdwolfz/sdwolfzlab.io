---
title: " (DX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>DX</td></tr>
    <tr><td>Web</td><td><a href="https://www.dynexcapital.com">www.dynexcapital.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.52 |
| 2020 | 1.66 |
| 2019 | 1.41 |
| 2018 | 0.72 |
| 2017 | 0.72 |
| 2016 | 0.84 |
| 2015 | 0.96 |
| 2014 | 1.0 |
| 2013 | 0.83 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
