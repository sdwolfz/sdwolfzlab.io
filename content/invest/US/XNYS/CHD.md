---
title: "CHURCH & DWIGHT (CHD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CHURCH & DWIGHT</td></tr>
    <tr><td>Symbol</td><td>CHD</td></tr>
    <tr><td>Web</td><td><a href="https://www.churchdwight.com">www.churchdwight.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.504 |
| 2020 | 0.96 |
| 2019 | 0.912 |
| 2018 | 0.872 |
| 2017 | 0.76 |
| 2016 | 1.064 |
| 2015 | 1.34 |
| 2014 | 1.24 |
| 2013 | 0.56 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
