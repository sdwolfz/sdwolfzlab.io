---
title: "METHANEX CORP (MEOH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>METHANEX CORP</td></tr>
    <tr><td>Symbol</td><td>MEOH</td></tr>
    <tr><td>Web</td><td><a href="https://www.methanex.com">www.methanex.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.075 |
| 2020 | 0.473 |
| 2019 | 1.41 |
| 2018 | 1.32 |
| 2017 | 1.175 |
| 2016 | 1.1 |
| 2015 | 1.075 |
| 2014 | 0.95 |
| 2013 | 0.785 |
| 2012 | 0.725 |
| 2011 | 0.665 |
| 2010 | 0.62 |
| 2009 | 0.62 |
| 2008 | 0.605 |
| 2007 | 0.545 |
| 2006 | 0.485 |
| 2005 | 0.41 |
| 2004 | 0.28 |
| 2003 | 0.47 |
| 2002 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
