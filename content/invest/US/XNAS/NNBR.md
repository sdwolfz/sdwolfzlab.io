---
title: "NN INC (NNBR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NN INC</td></tr>
    <tr><td>Symbol</td><td>NNBR</td></tr>
    <tr><td>Web</td><td><a href="https://www.nninc.com">www.nninc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.21 |
| 2018 | 0.28 |
| 2017 | 0.28 |
| 2016 | 0.28 |
| 2015 | 0.28 |
| 2014 | 0.28 |
| 2013 | 0.18 |
| 2008 | 0.24 |
| 2007 | 0.32 |
| 2006 | 0.32 |
| 2005 | 0.32 |
| 2004 | 0.32 |
| 2003 | 0.32 |
| 2002 | 0.32 |
| 2001 | 0.32 |
| 2000 | 0.32 |
| 1999 | 0.32 |
| 1998 | 0.32 |
| 1997 | 0.32 |
| 1996 | 0.32 |
| 1995 | 0.32 |
| 1994 | 0.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
