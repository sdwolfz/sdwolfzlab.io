---
title: "Whitbread (WTB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Whitbread</td></tr>
    <tr><td>Symbol</td><td>WTB</td></tr>
    <tr><td>Web</td><td><a href="https://www.whitbread.co.uk">www.whitbread.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 32.65 |
| 2019 | 99.65 |
| 2018 | 101.15 |
| 2017 | 95.8 |
| 2016 | 90.35 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
