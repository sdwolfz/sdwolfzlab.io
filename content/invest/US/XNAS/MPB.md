---
title: "MID PENN BANCORP INC (MPB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MID PENN BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>MPB</td></tr>
    <tr><td>Web</td><td><a href="https://www.midpennbank.com">www.midpennbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.44 |
| 2020 | 0.77 |
| 2019 | 0.79 |
| 2018 | 0.45 |
| 2017 | 0.77 |
| 2016 | 0.68 |
| 2015 | 0.44 |
| 2014 | 0.45 |
| 2013 | 0.25 |
| 2012 | 0.25 |
| 2011 | 0.2 |
| 2009 | 0.52 |
| 2008 | 0.8 |
| 2007 | 0.6 |
| 2006 | 0.6 |
| 2005 | 0.8 |
| 2004 | 1.8 |
| 2003 | 0.6 |
| 2002 | 0.8 |
| 2001 | 0.8 |
| 2000 | 0.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
