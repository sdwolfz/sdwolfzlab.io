---
title: "SILVANA SA CEHU SILVANIEI (SIVX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/bvb/">BVB</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/bvb/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>SILVANA SA CEHU SILVANIEI</td></tr>
    <tr><td>Symbol</td><td>SIVX</td></tr>
    <tr><td>Web</td><td><a href="http://www.silvana.ro">www.silvana.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 0.5 |
| 2014 | 0.5 |
| 2010 | 0.35 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
