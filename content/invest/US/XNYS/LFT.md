---
title: "LUMENT FINANCE TRUST INC (LFT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>LUMENT FINANCE TRUST INC</td></tr>
    <tr><td>Symbol</td><td>LFT</td></tr>
    <tr><td>Web</td><td><a href="https://www.lumentfinancetrust.com">www.lumentfinancetrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.09 |
| 2020 | 0.13 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
