---
title: "FORAJ SONDE SA VIDELE (FOJE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>FORAJ SONDE SA VIDELE</td></tr>
    <tr><td>Symbol</td><td>FOJE</td></tr>
    <tr><td>Web</td><td><a href="https://www.fsv.ro">www.fsv.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 1.0 |
| 2015 | 2.75 |
| 2007 | 2.016 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
