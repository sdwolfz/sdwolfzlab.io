---
title: "TSR INC (TSRI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TSR INC</td></tr>
    <tr><td>Symbol</td><td>TSRI</td></tr>
    <tr><td>Web</td><td><a href="https://www.tsrconsulting.com">www.tsrconsulting.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 1.0 |
| 2012 | 1.5 |
| 2009 | 0.05 |
| 2008 | 0.29 |
| 2007 | 0.32 |
| 2006 | 0.32 |
| 2005 | 0.53 |
| 2004 | 0.6 |
| 2003 | 2.15 |
| 1995 | 0.4 |
| 1991 | 1.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
