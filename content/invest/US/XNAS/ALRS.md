---
title: "ALERUS FINANCIAL CORP (ALRS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ALERUS FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>ALRS</td></tr>
    <tr><td>Web</td><td><a href="https://www.alerus.com">www.alerus.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.15 |
| 2020 | 0.6 |
| 2019 | 0.29 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
