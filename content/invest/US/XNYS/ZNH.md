---
title: " (ZNH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ZNH</td></tr>
    <tr><td>Web</td><td><a href="https://www.csair.com">www.csair.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.308 |
| 2018 | 0.679 |
| 2017 | 0.639 |
| 2016 | 0.53 |
| 2015 | 0.274 |
| 2014 | 0.27 |
| 2013 | 0.346 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
