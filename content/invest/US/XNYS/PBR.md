---
title: " (PBR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>PBR</td></tr>
    <tr><td>Web</td><td><a href="https://www.petrobras.com.br">www.petrobras.com.br</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.285 |
| 2020 | 0.183 |
| 2019 | 0.195 |
| 2018 | 0.116 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
