---
title: "HACKETT GROUP INC (HCKT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HACKETT GROUP INC</td></tr>
    <tr><td>Symbol</td><td>HCKT</td></tr>
    <tr><td>Web</td><td><a href="https://www.thehackettgroup.com">www.thehackettgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.285 |
| 2019 | 0.36 |
| 2018 | 0.34 |
| 2017 | 0.3 |
| 2016 | 0.26 |
| 2015 | 0.2 |
| 2014 | 0.12 |
| 2013 | 0.1 |
| 2012 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
