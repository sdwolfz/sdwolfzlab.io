---
title: "Basic Makefile starter"
date: 2020-07-17T18:14:29Z
type: posts
slug: basic-makefile-starter
tags: ["make", "linux", "shell", "productivity", "development"]
---

Use this template as a sterter for your `Makefile`, making sure you intent using
tabs as that's a hard requirement of the `make` tool.

```makefile
#-------------------------------------------------------------------------------
# Settings
#-------------------------------------------------------------------------------

# By default the `help` goal is executed.
.DEFAULT_GOAL := help

#-------------------------------------------------------------------------------
# Goals
#-------------------------------------------------------------------------------

.PHONY: help
help:
	@echo 'Usage: make [<GOAL_1>, ...] [<VARIABLE_1>=value_1, ...]'
	@echo ''
	@echo 'Examples:'
	@echo '  make'
	@echo '  make help'
	@echo ''
	@echo 'Goals:'
	@echo '  - help: Displays this help message.'
	@echo '  - here: Displays the directory where this Makefile is located.'
	@echo ''
	@echo 'Default Goal: help'
	@echo ''
```
