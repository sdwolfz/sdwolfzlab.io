---
title: "BRICKELL BIOTECH INC (BBI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BRICKELL BIOTECH INC</td></tr>
    <tr><td>Symbol</td><td>BBI</td></tr>
    <tr><td>Web</td><td><a href="https://www.brickellbio.com">www.brickellbio.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
