---
title: "NATIONAL INSTRUMENTS CORP (NATI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NATIONAL INSTRUMENTS CORP</td></tr>
    <tr><td>Symbol</td><td>NATI</td></tr>
    <tr><td>Web</td><td><a href="https://www.ni.com">www.ni.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.54 |
| 2020 | 1.04 |
| 2019 | 1.0 |
| 2018 | 0.92 |
| 2017 | 0.84 |
| 2016 | 0.8 |
| 2015 | 0.76 |
| 2014 | 0.6 |
| 2013 | 0.56 |
| 2012 | 0.56 |
| 2011 | 0.45 |
| 2010 | 0.52 |
| 2009 | 0.48 |
| 2008 | 0.44 |
| 2007 | 0.34 |
| 2006 | 0.24 |
| 2005 | 0.2 |
| 2004 | 0.2 |
| 2003 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
