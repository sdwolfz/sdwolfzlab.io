---
title: "FAUQUIER BANKSHARES INC (FBSS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FAUQUIER BANKSHARES INC</td></tr>
    <tr><td>Symbol</td><td>FBSS</td></tr>
    <tr><td>Web</td><td><a href="http://www.tfb.bank">www.tfb.bank</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.5 |
| 2019 | 0.485 |
| 2018 | 0.48 |
| 2017 | 0.48 |
| 2016 | 0.48 |
| 2015 | 0.48 |
| 2014 | 0.53 |
| 2013 | 0.48 |
| 2012 | 0.48 |
| 2011 | 0.48 |
| 2010 | 0.72 |
| 2009 | 0.8 |
| 2008 | 0.8 |
| 2007 | 0.79 |
| 2006 | 0.745 |
| 2005 | 0.645 |
| 2004 | 0.56 |
| 2003 | 0.48 |
| 2002 | 0.51 |
| 2001 | 0.72 |
| 2000 | 0.64 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
