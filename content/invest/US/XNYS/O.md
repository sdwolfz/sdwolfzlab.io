---
title: " (O)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>O</td></tr>
    <tr><td>Web</td><td><a href="https://www.realtyincome.com">www.realtyincome.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.94 |
| 2020 | 2.804 |
| 2019 | 2.72 |
| 2018 | 2.642 |
| 2017 | 2.54 |
| 2016 | 2.403 |
| 2015 | 2.282 |
| 2014 | 2.191 |
| 2013 | 1.092 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
