---
title: "British American Tobacco (BATS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>British American Tobacco</td></tr>
    <tr><td>Symbol</td><td>BATS</td></tr>
    <tr><td>Web</td><td><a href="https://www.bat.com">www.bat.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 215.6 |
| 2019 | 210.4 |
| 2018 | 203.0 |
| 2017 | 195.2 |
| 2016 | 169.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
