---
title: " (SOHO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SOHO</td></tr>
    <tr><td>Web</td><td><a href="https://www.sotherlyhotels.com">www.sotherlyhotels.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.13 |
| 2019 | 0.515 |
| 2018 | 0.485 |
| 2017 | 0.425 |
| 2016 | 0.365 |
| 2015 | 0.305 |
| 2014 | 0.225 |
| 2013 | 0.155 |
| 2012 | 0.1 |
| 2011 | 0.04 |
| 2009 | 0.02 |
| 2008 | 0.51 |
| 2007 | 0.68 |
| 2006 | 0.68 |
| 2005 | 0.85 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
