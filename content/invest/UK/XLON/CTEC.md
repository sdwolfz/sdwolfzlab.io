---
title: "ConvaTec (CTEC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>ConvaTec</td></tr>
    <tr><td>Symbol</td><td>CTEC</td></tr>
    <tr><td>Web</td><td><a href="https://www.convatecgroup.com">www.convatecgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 5.7 |
| 2018 | 5.7 |
| 2017 | 5.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
