---
title: "Prepend to $PATH in bash"
date: 2022-10-01T10:49:32Z
type: posts
slug: prepend-to-path-in-bash
series: ["Bash Tricks"]
tags: ["linux", "bash"]
---

You should always check if the value is present in a list before adding it in.

```bash
function prepend_path () {
  case ":${PATH}:" in
    *:"$1":*)
    ;;
    *)
      PATH="$1:${PATH}"
  esac
}
```

## Usage

```sh
prepend_path ~/.local/bin
```

## Cleanup

```bash
unset prepend_path
```
