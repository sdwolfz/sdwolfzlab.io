---
title: "SABRE CORP (SABR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SABRE CORP</td></tr>
    <tr><td>Symbol</td><td>SABR</td></tr>
    <tr><td>Web</td><td><a href="https://investors.sabre.com">investors.sabre.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.14 |
| 2019 | 0.56 |
| 2018 | 0.56 |
| 2017 | 0.56 |
| 2016 | 0.52 |
| 2015 | 0.36 |
| 2014 | 0.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
