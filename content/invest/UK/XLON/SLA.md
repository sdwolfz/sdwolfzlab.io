---
title: "Standard Life Aberdeen (SLA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Standard Life Aberdeen</td></tr>
    <tr><td>Symbol</td><td>SLA</td></tr>
    <tr><td>Web</td><td><a href="https://www.standardlifeaberdeen.com">www.standardlifeaberdeen.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 21.6 |
| 2019 | 21.6 |
| 2018 | 21.6 |
| 2017 | 21.3 |
| 2016 | 19.82 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
