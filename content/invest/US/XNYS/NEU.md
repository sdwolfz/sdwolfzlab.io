---
title: "NEWMARKET CORP (NEU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NEWMARKET CORP</td></tr>
    <tr><td>Symbol</td><td>NEU</td></tr>
    <tr><td>Web</td><td><a href="https://www.newmarket.com">www.newmarket.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 3.8 |
| 2020 | 7.6 |
| 2019 | 7.3 |
| 2018 | 7.0 |
| 2017 | 7.0 |
| 2016 | 6.4 |
| 2015 | 5.8 |
| 2014 | 4.7 |
| 2013 | 2.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
