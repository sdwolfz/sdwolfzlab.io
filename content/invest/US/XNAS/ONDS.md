---
title: "ONDAS HOLDINGS INC (ONDS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ONDAS HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>ONDS</td></tr>
    <tr><td>Web</td><td><a href="https://www.ondas.com">www.ondas.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
