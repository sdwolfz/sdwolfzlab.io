---
title: "ALLETE INC (ALE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ALLETE INC</td></tr>
    <tr><td>Symbol</td><td>ALE</td></tr>
    <tr><td>Web</td><td><a href="https://www.allete.com">www.allete.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.26 |
| 2020 | 2.472 |
| 2019 | 2.348 |
| 2018 | 2.24 |
| 2017 | 2.14 |
| 2016 | 2.08 |
| 2015 | 2.02 |
| 2014 | 1.96 |
| 2013 | 0.95 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
