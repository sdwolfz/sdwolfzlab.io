---
title: "SIF OLTENIA S.A. (SIF5)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>SIF OLTENIA S.A.</td></tr>
    <tr><td>Symbol</td><td>SIF5</td></tr>
    <tr><td>Web</td><td><a href="https://www.sifolt.ro">www.sifolt.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.05 |
| 2019 | 0.1 |
| 2018 | 0.15 |
| 2017 | 0.07 |
| 2016 | 0.09 |
| 2015 | 0.13 |
| 2014 | 0.12 |
| 2013 | 0.16 |
| 2012 | 0.13 |
| 2011 | 0.13 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
