---
title: " (NLY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>NLY</td></tr>
    <tr><td>Web</td><td><a href="https://www.annaly.com">www.annaly.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.22 |
| 2020 | 0.91 |
| 2019 | 1.05 |
| 2018 | 0.978 |
| 2017 | 1.2 |
| 2016 | 1.164 |
| 2015 | 1.2 |
| 2014 | 1.2 |
| 2013 | 1.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
