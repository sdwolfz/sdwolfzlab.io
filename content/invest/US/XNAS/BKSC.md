---
title: "BANK OF SOUTH CAROLINA CORP (BKSC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BANK OF SOUTH CAROLINA CORP</td></tr>
    <tr><td>Symbol</td><td>BKSC</td></tr>
    <tr><td>Web</td><td><a href="https://www.banksc.com">www.banksc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.27 |
| 2020 | 0.66 |
| 2019 | 0.89 |
| 2018 | 0.55 |
| 2017 | 0.58 |
| 2016 | 0.54 |
| 2015 | 0.52 |
| 2014 | 0.62 |
| 2013 | 0.5 |
| 2012 | 0.45 |
| 2011 | 0.42 |
| 2010 | 0.4 |
| 2009 | 0.32 |
| 2008 | 0.64 |
| 2007 | 0.62 |
| 2006 | 0.67 |
| 2005 | 0.51 |
| 2004 | 0.44 |
| 2003 | 0.33 |
| 2002 | 0.59 |
| 2001 | 0.44 |
| 2000 | 0.52 |
| 1999 | 0.44 |
| 1998 | 0.26 |
| 1997 | 0.78 |
| 1996 | 0.4 |
| 1995 | 0.36 |
| 1994 | 0.54 |
| 1993 | 0.28 |
| 1992 | 0.33 |
| 1991 | 0.3 |
| 1990 | 0.29 |
| 1989 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
