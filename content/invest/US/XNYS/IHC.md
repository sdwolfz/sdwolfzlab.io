---
title: "INDEPENDENCE HLDGS (IHC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>INDEPENDENCE HLDGS</td></tr>
    <tr><td>Symbol</td><td>IHC</td></tr>
    <tr><td>Web</td><td><a href="https://www.ihcgroup.com">www.ihcgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.44 |
| 2019 | 0.55 |
| 2018 | 0.15 |
| 2017 | 0.16 |
| 2016 | 0.15 |
| 2015 | 0.08 |
| 2014 | 0.07 |
| 2013 | 0.035 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
