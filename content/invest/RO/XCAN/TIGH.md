---
title: "TARNAVA SA SIGHISOARA (TIGH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>TARNAVA SA SIGHISOARA</td></tr>
    <tr><td>Symbol</td><td>TIGH</td></tr>
    <tr><td>Web</td><td><a href="https://www.tarnava.ro">www.tarnava.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2004 | 0.054 |
| 2003 | 0.0428 |
| 2002 | 0.0428 |
| 2001 | 0.057 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
