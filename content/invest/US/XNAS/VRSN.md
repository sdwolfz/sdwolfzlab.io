---
title: "VERISIGN (VRSN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>VERISIGN</td></tr>
    <tr><td>Symbol</td><td>VRSN</td></tr>
    <tr><td>Web</td><td><a href="https://www.verisign.com">www.verisign.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2011 | 2.75 |
| 2010 | 3.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
