---
title: "NEW FORTRESS ENERGY INC (NFE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NEW FORTRESS ENERGY INC</td></tr>
    <tr><td>Symbol</td><td>NFE</td></tr>
    <tr><td>Web</td><td><a href="https://www.newfortressenergy.com">www.newfortressenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
