---
title: "IMPAC MORTGAGE HOLDINGS INC (IMH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>IMPAC MORTGAGE HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>IMH</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2007 | 0.35 |
| 2006 | 0.95 |
| 2005 | 1.95 |
| 2004 | 2.9 |
| 2003 | 2.05 |
| 2002 | 1.76 |
| 2001 | 0.69 |
| 2000 | 0.36 |
| 1999 | 0.26 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
