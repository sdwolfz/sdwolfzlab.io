---
title: " (XHR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>XHR</td></tr>
    <tr><td>Web</td><td><a href="https://www.xeniareit.com">www.xeniareit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.275 |
| 2019 | 1.1 |
| 2018 | 1.1 |
| 2017 | 1.1 |
| 2016 | 1.1 |
| 2015 | 0.836 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
