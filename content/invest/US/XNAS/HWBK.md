---
title: "HAWTHORNE BANCSHARES INC (HWBK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HAWTHORNE BANCSHARES INC</td></tr>
    <tr><td>Symbol</td><td>HWBK</td></tr>
    <tr><td>Web</td><td><a href="https://www.hawthornbancshares.com">www.hawthornbancshares.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.13 |
| 2020 | 0.37 |
| 2019 | 0.34 |
| 2018 | 0.27 |
| 2017 | 0.2 |
| 2016 | 0.16 |
| 2015 | 0.15 |
| 2014 | 0.15 |
| 2013 | 0.15 |
| 2012 | 0.15 |
| 2011 | 0.15 |
| 2010 | 0.21 |
| 2009 | 0.43 |
| 2008 | 0.84 |
| 2007 | 0.84 |
| 2006 | 0.84 |
| 2005 | 0.84 |
| 2004 | 0.81 |
| 2003 | 0.92 |
| 2002 | 0.89 |
| 2001 | 0.85 |
| 2000 | 0.47 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
