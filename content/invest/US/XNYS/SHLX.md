---
title: " (SHLX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SHLX</td></tr>
    <tr><td>Web</td><td><a href="https://www.shellmidstreampartners.com">www.shellmidstreampartners.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.92 |
| 2020 | 1.84 |
| 2019 | 1.69 |
| 2018 | 1.428 |
| 2017 | 1.19 |
| 2016 | 0.969 |
| 2015 | 0.674 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
