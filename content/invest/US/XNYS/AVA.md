---
title: "AVISTA CORP (AVA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AVISTA CORP</td></tr>
    <tr><td>Symbol</td><td>AVA</td></tr>
    <tr><td>Web</td><td><a href="https://www.avistacorp.com">www.avistacorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.423 |
| 2020 | 1.62 |
| 2019 | 1.548 |
| 2018 | 1.492 |
| 2017 | 1.428 |
| 2016 | 1.372 |
| 2015 | 1.32 |
| 2014 | 1.268 |
| 2013 | 0.61 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
