---
title: " (VLYPO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>VLYPO</td></tr>
    <tr><td>Web</td><td><a href="https://www.valley.com">www.valley.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.344 |
| 2020 | 1.375 |
| 2019 | 1.375 |
| 2018 | 1.375 |
| 2017 | 0.565 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
