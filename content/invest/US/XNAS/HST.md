---
title: "HOST HOTELS & RESORTS INC (HST)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HOST HOTELS & RESORTS INC</td></tr>
    <tr><td>Symbol</td><td>HST</td></tr>
    <tr><td>Web</td><td><a href="https://www.hosthotels.com">www.hosthotels.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.2 |
| 2019 | 0.85 |
| 2018 | 0.85 |
| 2017 | 0.85 |
| 2016 | 0.85 |
| 2015 | 0.8 |
| 2014 | 0.75 |
| 2013 | 0.46 |
| 2012 | 0.3 |
| 2011 | 0.11 |
| 2010 | 0.04 |
| 2009 | 0.25 |
| 2008 | 0.65 |
| 2007 | 1.0 |
| 2006 | 0.51 |
| 2005 | 0.41 |
| 2004 | 0.675 |
| 2001 | 0.78 |
| 2000 | 0.49 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
