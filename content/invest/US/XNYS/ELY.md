---
title: "CALLAWAY GOLF COMPANY (ELY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CALLAWAY GOLF COMPANY</td></tr>
    <tr><td>Symbol</td><td>ELY</td></tr>
    <tr><td>Web</td><td><a href="https://www.callawaygolf.com">www.callawaygolf.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.02 |
| 2019 | 0.04 |
| 2018 | 0.04 |
| 2017 | 0.04 |
| 2016 | 0.04 |
| 2015 | 0.04 |
| 2014 | 0.04 |
| 2013 | 0.02 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
