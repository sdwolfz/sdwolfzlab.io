---
title: "A.T.C. - AGRO TOTAL CONSTRUCT SA GHIMBAV (GHIM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>A.T.C. - AGRO TOTAL CONSTRUCT SA GHIMBAV</td></tr>
    <tr><td>Symbol</td><td>GHIM</td></tr>
    <tr><td>Web</td><td><a href="https://www.agrototalconstruct.ro">www.agrototalconstruct.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
