---
title: "TCG BDC INC (CGBD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TCG BDC INC</td></tr>
    <tr><td>Symbol</td><td>CGBD</td></tr>
    <tr><td>Web</td><td><a href="https://www.tcgbdc.com">www.tcgbdc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.73 |
| 2020 | 1.47 |
| 2019 | 1.74 |
| 2018 | 1.68 |
| 2017 | 1.23 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
