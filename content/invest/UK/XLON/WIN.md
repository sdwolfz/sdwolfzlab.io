---
title: "Wincanton (WIN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Wincanton</td></tr>
    <tr><td>Symbol</td><td>WIN</td></tr>
    <tr><td>Web</td><td><a href="https://www.wincanton.co.uk">www.wincanton.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 3.9 |
| 2019 | 10.89 |
| 2018 | 9.9 |
| 2017 | 9.1 |
| 2016 | 5.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
