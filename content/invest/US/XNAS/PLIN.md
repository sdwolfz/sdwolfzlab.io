---
title: "CHINA XIANGTAI FOOD CO LTD (PLIN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CHINA XIANGTAI FOOD CO LTD</td></tr>
    <tr><td>Symbol</td><td>PLIN</td></tr>
    <tr><td>Web</td><td><a href="https://ir.plinfood.com">ir.plinfood.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
