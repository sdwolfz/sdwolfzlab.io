---
title: "Edinburgh Investment Trust PLC (EDIN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Edinburgh Investment Trust PLC</td></tr>
    <tr><td>Symbol</td><td>EDIN</td></tr>
    <tr><td>Web</td><td><a href="https://www.edinburghinvestmenttrust.com">www.edinburghinvestmenttrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 28.65 |
| 2019 | 28.0 |
| 2018 | 26.6 |
| 2017 | 25.35 |
| 2016 | 24.35 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
