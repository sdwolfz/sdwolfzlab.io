---
title: "ICF INTERNATIONAL INC (ICFI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ICF INTERNATIONAL INC</td></tr>
    <tr><td>Symbol</td><td>ICFI</td></tr>
    <tr><td>Web</td><td><a href="https://www.icf.com">www.icf.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 0.56 |
| 2019 | 0.56 |
| 2018 | 0.56 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
