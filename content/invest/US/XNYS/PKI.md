---
title: "PERKINELMER INC (PKI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PERKINELMER INC</td></tr>
    <tr><td>Symbol</td><td>PKI</td></tr>
    <tr><td>Web</td><td><a href="https://www.perkinelmer.com">www.perkinelmer.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.21 |
| 2020 | 0.28 |
| 2019 | 0.28 |
| 2018 | 0.28 |
| 2017 | 0.28 |
| 2016 | 0.28 |
| 2015 | 0.28 |
| 2014 | 0.28 |
| 2013 | 0.07 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
