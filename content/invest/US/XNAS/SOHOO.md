---
title: " (SOHOO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SOHOO</td></tr>
    <tr><td>Web</td><td><a href="https://www.sotherlyhotels.com">www.sotherlyhotels.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 1.969 |
| 2018 | 1.969 |
| 2017 | 0.432 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
