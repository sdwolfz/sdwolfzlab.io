---
title: "UNIVERSAL HEALTH SERVICES INC. (UHS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>UNIVERSAL HEALTH SERVICES INC.</td></tr>
    <tr><td>Symbol</td><td>UHS</td></tr>
    <tr><td>Web</td><td><a href="https://www.uhsinc.com">www.uhsinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.2 |
| 2019 | 0.6 |
| 2018 | 0.4 |
| 2017 | 0.4 |
| 2016 | 0.4 |
| 2015 | 0.4 |
| 2014 | 0.3 |
| 2013 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
