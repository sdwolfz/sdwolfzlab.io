---
title: " (LTC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>LTC</td></tr>
    <tr><td>Web</td><td><a href="https://www.ltcreit.com">www.ltcreit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.95 |
| 2020 | 2.28 |
| 2019 | 2.28 |
| 2018 | 2.28 |
| 2017 | 2.28 |
| 2016 | 2.19 |
| 2015 | 2.07 |
| 2014 | 2.04 |
| 2013 | 0.82 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
