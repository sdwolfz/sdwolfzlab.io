---
title: "AUDIOCODES (AUDC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AUDIOCODES</td></tr>
    <tr><td>Symbol</td><td>AUDC</td></tr>
    <tr><td>Web</td><td><a href="https://www.audiocodes.com">www.audiocodes.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.16 |
| 2020 | 0.27 |
| 2019 | 0.23 |
| 2018 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
