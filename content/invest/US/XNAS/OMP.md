---
title: " (OMP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>OMP</td></tr>
    <tr><td>Web</td><td><a href="https://www.oasismidstream.com">www.oasismidstream.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.54 |
| 2020 | 2.16 |
| 2019 | 1.925 |
| 2018 | 1.608 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
