---
title: "CLEARWAY ENERGY INC (CWEN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CLEARWAY ENERGY INC</td></tr>
    <tr><td>Symbol</td><td>CWEN</td></tr>
    <tr><td>Web</td><td><a href="https://www.clearwayenergy.com">www.clearwayenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.653 |
| 2020 | 1.051 |
| 2019 | 0.8 |
| 2018 | 0.651 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
