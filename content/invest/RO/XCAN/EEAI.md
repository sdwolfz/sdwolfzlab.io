---
title: "ELECTROCONSTRUCTIA ELCO ALBA IULIA SA (EEAI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>ELECTROCONSTRUCTIA ELCO ALBA IULIA SA</td></tr>
    <tr><td>Symbol</td><td>EEAI</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 0.049806 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
