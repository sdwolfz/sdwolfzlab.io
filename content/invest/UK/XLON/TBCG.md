---
title: "TBC Bank Group (TBCG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>TBC Bank Group</td></tr>
    <tr><td>Symbol</td><td>TBCG</td></tr>
    <tr><td>Web</td><td><a href="https://www.tbcbank.ge">www.tbcbank.ge</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 198.0 |
| 2017 | 164.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
