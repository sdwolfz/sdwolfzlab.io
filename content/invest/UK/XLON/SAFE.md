---
title: "Safestore (SAFE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Safestore</td></tr>
    <tr><td>Symbol</td><td>SAFE</td></tr>
    <tr><td>Web</td><td><a href="https://www.safestore.co.uk">www.safestore.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 18.6 |
| 2019 | 17.5 |
| 2018 | 16.25 |
| 2017 | 14.0 |
| 2016 | 11.65 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
