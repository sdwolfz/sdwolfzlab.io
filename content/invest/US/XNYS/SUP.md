---
title: "SUPERIOR INDUSTRIES INTERNATIONAL (SUP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SUPERIOR INDUSTRIES INTERNATIONAL</td></tr>
    <tr><td>Symbol</td><td>SUP</td></tr>
    <tr><td>Web</td><td><a href="https://www.supind.com">www.supind.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.27 |
| 2018 | 0.36 |
| 2017 | 0.54 |
| 2016 | 0.54 |
| 2015 | 0.72 |
| 2014 | 0.72 |
| 2013 | 0.36 |
| 2012 | 0.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
