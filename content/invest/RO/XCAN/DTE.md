---
title: "DEUTSCHE TELEKOM AG. (DTE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>DEUTSCHE TELEKOM AG.</td></tr>
    <tr><td>Symbol</td><td>DTE</td></tr>
    <tr><td>Web</td><td><a href="https://www.deutschetelekom.com">www.deutschetelekom.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
