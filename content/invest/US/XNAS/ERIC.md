---
title: " (ERIC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ERIC</td></tr>
    <tr><td>Web</td><td><a href="https://www.ericsson.com">www.ericsson.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.117 |
| 2020 | 0.159 |
| 2019 | 0.108 |
| 2018 | 0.119 |
| 2017 | 0.111 |
| 2016 | 0.457 |
| 2015 | 0.391 |
| 2014 | 0.459 |
| 2013 | 0.42 |
| 2011 | 0.369 |
| 2010 | 0.193 |
| 2009 | 0.155 |
| 2008 | 0.572 |
| 2007 | 0.5 |
| 2006 | 0.406 |
| 2005 | 0.251 |
| 2002 | 0.01 |
| 2001 | 0.035 |
| 2000 | 0.162 |
| 1999 | 0.212 |
| 1998 | 0.376 |
| 1997 | 0.277 |
| 1996 | 0.22 |
| 1995 | 0.638 |
| 1994 | 0.485 |
| 1993 | 0.389 |
| 1992 | 0.496 |
| 1991 | 0.485 |
| 1990 | 1.949 |
| 1989 | 1.399 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
