---
title: "FAIR ISAAC CORP (FICO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FAIR ISAAC CORP</td></tr>
    <tr><td>Symbol</td><td>FICO</td></tr>
    <tr><td>Web</td><td><a href="https://www.fico.com">www.fico.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 0.02 |
| 2016 | 0.08 |
| 2015 | 0.08 |
| 2014 | 0.08 |
| 2013 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
