---
title: "ELECTROMONTAJ CARPATI SA Sibiu (ELJA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>ELECTROMONTAJ CARPATI SA Sibiu</td></tr>
    <tr><td>Symbol</td><td>ELJA</td></tr>
    <tr><td>Web</td><td><a href="https://www.elmontaj.ro">www.elmontaj.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2000 | 962.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
