---
title: "Cvc Credit Gbp (CCPG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Cvc Credit Gbp</td></tr>
    <tr><td>Symbol</td><td>CCPG</td></tr>
    <tr><td>Web</td><td><a href="https://www.ccpeol.com">www.ccpeol.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 5.5 |
| 2018 | 5.5 |
| 2017 | 5.25 |
| 2016 | 6.25 |
| 2015 | 5.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
