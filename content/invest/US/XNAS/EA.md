---
title: "ELECTRONIC ARTS INC (EA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ELECTRONIC ARTS INC</td></tr>
    <tr><td>Symbol</td><td>EA</td></tr>
    <tr><td>Web</td><td><a href="https://www.ea.com">www.ea.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.17 |
| 2020 | 0.17 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
