---
title: "UNION BANKSHARES INC/VT (UNB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>UNION BANKSHARES INC/VT</td></tr>
    <tr><td>Symbol</td><td>UNB</td></tr>
    <tr><td>Web</td><td><a href="https://www.ublocal.com">www.ublocal.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.66 |
| 2020 | 1.28 |
| 2019 | 1.24 |
| 2018 | 1.2 |
| 2017 | 1.16 |
| 2016 | 1.11 |
| 2015 | 1.08 |
| 2014 | 1.04 |
| 2013 | 1.01 |
| 2012 | 1.0 |
| 2011 | 1.0 |
| 2010 | 1.0 |
| 2009 | 1.03 |
| 2008 | 1.12 |
| 2007 | 1.12 |
| 2006 | 1.06 |
| 2005 | 1.38 |
| 2004 | 0.9 |
| 2003 | 1.12 |
| 2002 | 1.14 |
| 2001 | 1.06 |
| 2000 | 0.26 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
