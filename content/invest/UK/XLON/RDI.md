---
title: "RDI REIT P.L.C. ORD 40P (RDI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>RDI REIT P.L.C. ORD 40P</td></tr>
    <tr><td>Symbol</td><td>RDI</td></tr>
    <tr><td>Web</td><td><a href="http://www.rdireit.com">www.rdireit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 2.7 |
| 2017 | 2.6 |
| 2016 | 3.2 |
| 2015 | 3.25 |
| 2014 | 3.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
