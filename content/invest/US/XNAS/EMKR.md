---
title: "EMCORE CORP (EMKR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>EMCORE CORP</td></tr>
    <tr><td>Symbol</td><td>EMKR</td></tr>
    <tr><td>Web</td><td><a href="https://www.emcore.com">www.emcore.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 1.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
