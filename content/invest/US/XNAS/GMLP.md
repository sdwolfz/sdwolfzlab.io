---
title: "GOLAR LNG PARTNERS LP (GMLP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GOLAR LNG PARTNERS LP</td></tr>
    <tr><td>Symbol</td><td>GMLP</td></tr>
    <tr><td>Web</td><td><a href="http://www.golarlngpartners.com">www.golarlngpartners.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.465 |
| 2019 | 1.617 |
| 2018 | 2.137 |
| 2017 | 2.31 |
| 2016 | 2.31 |
| 2015 | 2.295 |
| 2014 | 2.14 |
| 2013 | 2.053 |
| 2012 | 1.775 |
| 2011 | 0.734 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
