---
title: "MICROSOFT CORP (MSFT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MICROSOFT CORP</td></tr>
    <tr><td>Symbol</td><td>MSFT</td></tr>
    <tr><td>Web</td><td><a href="https://www.microsoft.com">www.microsoft.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.12 |
| 2020 | 2.09 |
| 2019 | 1.89 |
| 2018 | 1.72 |
| 2017 | 1.59 |
| 2016 | 1.47 |
| 2015 | 1.29 |
| 2014 | 1.15 |
| 2013 | 0.97 |
| 2012 | 0.83 |
| 2011 | 0.68 |
| 2010 | 0.55 |
| 2009 | 0.52 |
| 2008 | 0.46 |
| 2007 | 0.41 |
| 2006 | 0.37 |
| 2005 | 0.32 |
| 2004 | 3.16 |
| 2003 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
