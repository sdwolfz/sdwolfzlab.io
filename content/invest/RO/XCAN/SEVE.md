---
title: "SEVERNAV SA DR. TR. SEVERIN (SEVE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>SEVERNAV SA DR. TR. SEVERIN</td></tr>
    <tr><td>Symbol</td><td>SEVE</td></tr>
    <tr><td>Web</td><td><a href="https://www.severnav.ro">www.severnav.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2006 | 0.1669 |
| 2003 | 0.0004 |
| 2002 | 0.0138 |
| 2001 | 0.0132 |
| 2000 | 0.0252 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
