---
title: "SUN LIFE FINANCIAL INC (SLF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SUN LIFE FINANCIAL INC</td></tr>
    <tr><td>Symbol</td><td>SLF</td></tr>
    <tr><td>Web</td><td><a href="https://www.sunlife.com">www.sunlife.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.633 |
| 2019 | 1.587 |
| 2018 | 1.479 |
| 2017 | 1.346 |
| 2016 | 1.235 |
| 2015 | 1.798 |
| 2014 | 1.44 |
| 2013 | 0.72 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
