---
title: "HUDSON EXECUTIVE INVT CORP III (HIII)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HUDSON EXECUTIVE INVT CORP III</td></tr>
    <tr><td>Symbol</td><td>HIII</td></tr>
    <tr><td>Web</td><td><a href="https://www.heic3.com">www.heic3.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
