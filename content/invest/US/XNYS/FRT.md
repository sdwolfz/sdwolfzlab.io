---
title: " (FRT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>FRT</td></tr>
    <tr><td>Web</td><td><a href="https://www.federalrealty.com">www.federalrealty.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.12 |
| 2020 | 4.22 |
| 2019 | 4.14 |
| 2018 | 4.04 |
| 2017 | 3.96 |
| 2016 | 3.84 |
| 2015 | 3.62 |
| 2014 | 3.3 |
| 2013 | 1.56 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
