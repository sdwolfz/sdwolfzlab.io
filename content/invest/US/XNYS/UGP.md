---
title: " (UGP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>UGP</td></tr>
    <tr><td>Web</td><td><a href="https://www.ultra.com.br">www.ultra.com.br</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.071 |
| 2020 | 0.047 |
| 2019 | 0.128 |
| 2018 | 0.402 |
| 2017 | 0.543 |
| 2016 | 0.462 |
| 2015 | 0.447 |
| 2014 | 0.61 |
| 2013 | 0.276 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
