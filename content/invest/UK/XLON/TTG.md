---
title: "Tt Electronics (TTG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Tt Electronics</td></tr>
    <tr><td>Symbol</td><td>TTG</td></tr>
    <tr><td>Web</td><td><a href="https://www.ttelectronics.com">www.ttelectronics.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 7.0 |
| 2018 | 6.5 |
| 2017 | 5.8 |
| 2016 | 5.6 |
| 2015 | 5.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
