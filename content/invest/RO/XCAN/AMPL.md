---
title: "AMPLO SA PLOIESTI (AMPL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>AMPLO SA PLOIESTI</td></tr>
    <tr><td>Symbol</td><td>AMPL</td></tr>
    <tr><td>Web</td><td><a href="https://www.amplo.ro">www.amplo.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 0.253 |
| 2014 | 0.15 |
| 2012 | 0.255 |
| 2009 | 0.2 |
| 2008 | 0.41 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
