---
title: "SCHWAB(CHARLES)CORP (SCHW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SCHWAB(CHARLES)CORP</td></tr>
    <tr><td>Symbol</td><td>SCHW</td></tr>
    <tr><td>Web</td><td><a href="https://www.aboutschwab.com">www.aboutschwab.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.36 |
| 2020 | 0.72 |
| 2019 | 0.68 |
| 2018 | 0.46 |
| 2017 | 0.32 |
| 2016 | 0.27 |
| 2015 | 0.24 |
| 2014 | 0.24 |
| 2013 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
