---
title: "Jp Morg.as (JAGI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Jp Morg.as</td></tr>
    <tr><td>Symbol</td><td>JAGI</td></tr>
    <tr><td>Web</td><td><a href="https://am.jpmorgan.com">am.jpmorgan.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 15.8 |
| 2019 | 15.7 |
| 2018 | 15.7 |
| 2017 | 13.9 |
| 2016 | 3.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
