---
title: "FNCB BANCORP INC (FNCB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FNCB BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>FNCB</td></tr>
    <tr><td>Web</td><td><a href="https://www.fncb.com">www.fncb.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 0.22 |
| 2019 | 0.2 |
| 2018 | 0.13 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
