---
title: " (ARI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ARI</td></tr>
    <tr><td>Web</td><td><a href="https://www.apolloreit.com">www.apolloreit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.35 |
| 2020 | 1.45 |
| 2019 | 1.84 |
| 2018 | 1.84 |
| 2017 | 1.84 |
| 2016 | 1.84 |
| 2015 | 1.78 |
| 2014 | 1.6 |
| 2013 | 0.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
