---
title: " (CEQP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CEQP</td></tr>
    <tr><td>Web</td><td><a href="https://www.crestwoodlp.com">www.crestwoodlp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.25 |
| 2020 | 2.5 |
| 2019 | 2.4 |
| 2018 | 2.4 |
| 2017 | 2.4 |
| 2016 | 3.175 |
| 2015 | 1.789 |
| 2014 | 0.552 |
| 2013 | 0.265 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
