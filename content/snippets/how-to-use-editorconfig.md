---
title: "How to Use: editorconfig"
date: 2020-06-11T19:15:32Z
type: posts
slug: how-to-use-editorconfig
series: ["How to Use"]
tags: ["editorconfig", "linux", "git", "productivity", "spacemacs"]
---

This is editorconfig:
* https://editorconfig.org

This is the checker:
* https://github.com/editorconfig-checker/editorconfig-checker

## Install

The CLI tool:

```sh
# Manjaro / Arch
sudo pacman -S git editorconfig-checker

# Alpine
apk add --update git editorconfig-checker
```

Check your text editor's plugins for editorconfig integration. Spacemacs has it
already built in.

## Usage

Add an `.editorconfig` file to your project root:

```ini
root = true

[*]
charset                  = utf-8
end_of_line              = lf
indent_style             = space
insert_final_newline     = true
tab_width                = 2
trim_trailing_whitespace = true

[Makefile]
indent_style = tab

[{tmp/**/*,cache/**/*}]
charset                  = unset
end_of_line              = unset
indent_style             = unset
insert_final_newline     = unset
tab_width                = unset
trim_trailing_whitespace = unset
```

Now you can lint your repository:

```sh
editorconfig-checker $(git ls-files)
```

Use my docker image so you don't have to build your own:

```sh
docker run --rm -it -v "$PWD":/work -w /work sdwolfz/editorconfig editorconfig-checker .
```
