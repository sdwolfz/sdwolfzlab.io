---
title: "COCA-COLA CO (KO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>COCA-COLA CO</td></tr>
    <tr><td>Symbol</td><td>KO</td></tr>
    <tr><td>Web</td><td><a href="https://www.coca-colacompany.com">www.coca-colacompany.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.84 |
| 2020 | 1.64 |
| 2019 | 1.6 |
| 2018 | 1.56 |
| 2017 | 1.48 |
| 2016 | 1.4 |
| 2015 | 1.32 |
| 2014 | 1.22 |
| 2013 | 0.56 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
