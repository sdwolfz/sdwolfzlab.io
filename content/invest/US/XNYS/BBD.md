---
title: " (BBD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BBD</td></tr>
    <tr><td>Web</td><td><a href="https://www.bradesco.com.br">www.bradesco.com.br</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.014 |
| 2020 | 0.105 |
| 2019 | 0.376 |
| 2018 | 0.2 |
| 2017 | 0.29 |
| 2016 | 0.366 |
| 2015 | 0.28 |
| 2014 | 0.18 |
| 2013 | 0.034 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
