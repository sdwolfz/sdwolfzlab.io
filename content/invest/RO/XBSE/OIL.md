---
title: "OIL TERMINAL S.A. (OIL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>OIL TERMINAL S.A.</td></tr>
    <tr><td>Symbol</td><td>OIL</td></tr>
    <tr><td>Web</td><td><a href="https://www.oil-terminal.com">www.oil-terminal.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.002762 |
| 2019 | 0.001952 |
| 2018 | 7.8e-05 |
| 2017 | 0.007745 |
| 2016 | 0.024424 |
| 2015 | 0.004899 |
| 2014 | 0.000449 |
| 2012 | 0.000852 |
| 2011 | 0.0034 |
| 2010 | 0.003 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
