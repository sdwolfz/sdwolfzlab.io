---
title: "Fix docker shell history navigation"
date: 2020-12-29T11:07:12Z
type: posts
slug: fix-docker-shell-history-navigation
tags: ["docker", "shell", "linux"]
---

## Prerequisites

1. Start a docekr container:

```sh
docker run --rm -it alpine sh
```

2. Run some random commands in that shell:

```sh
ls -la
pwd
cd ~
uname -a
```

## Problem

Try to navigate the history with `Ctrl + p` and notice it is broken. When you
press it the first time it does nothing, press it a second time it jumps 2
commands.

## Reason

* The first time you press `Ctrl + p`, it waits to see if you press `Ctrl + q`
  to detach from the docker container.
* The second time you press `Ctrl + p`, it notices you did not press `Ctrl + q`,
  so it issues both escape sequences to the shell, navigating you 2 positions
  back in the command history.

## Pause

Yes I know, `docker` devs should have chosen a sequence not already reserved by
literally all shells ever, but don't blame just them, `vim`, `emacs`, `tmux`,
and more, do this sort of common sense clobbering too. And they won't change
defaults because they're too big and arrogant to care.

## Solution

Add this key to your `~/.docker/config.json`:

```
"detachKeys": "ctrl-@"
```

It should look something like:

```json
{
  "HttpHeaders": {
    "User-Agent": "Docker-Client/19.03.13-ce (linux)"
  },
  "detachKeys": "ctrl-@"
}
```

Now you:
* Press `Ctrl + p` and what should have always happened, will happen.
* Hit `Ctrl + Shift + @` to detach, instead of `Ctrl + p | Ctrl + q`.
