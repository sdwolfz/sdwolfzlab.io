---
title: " (ITCB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ITCB</td></tr>
    <tr><td>Web</td><td><a href="https://www.itau.cl">www.itau.cl</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.308 |
| 2019 | 0.128 |
| 2018 | 0.092 |
| 2017 | 0.002 |
| 2016 | 0.535 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
