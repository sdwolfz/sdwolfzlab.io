---
title: "One Liners: Generate random password"
date: 2020-07-28T18:15:13Z
type: posts
slug: one-liners-generate-random-password
series: ["One Liners"]
tags: ["bash"]
---

Basic command:

```sh
tr -dc "a-zA-Z0-9" < /dev/urandom | head -c 32
```

Print it to STDOUT:

```sh
echo $(tr -dc "a-zA-Z0-9" < /dev/urandom | head -c 32)
```

Save it in an environment variable:

```sh
export SECRET_PASSWORD=$(tr -dc "a-zA-Z0-9" < /dev/urandom | head -c 32)
```
