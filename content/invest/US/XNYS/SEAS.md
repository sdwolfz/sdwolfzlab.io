---
title: "SEAWORLD ENTERTAINMENT INC (SEAS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SEAWORLD ENTERTAINMENT INC</td></tr>
    <tr><td>Symbol</td><td>SEAS</td></tr>
    <tr><td>Web</td><td><a href="https://www.seaworldentertainment.com">www.seaworldentertainment.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.73 |
| 2015 | 0.84 |
| 2014 | 0.62 |
| 2013 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
