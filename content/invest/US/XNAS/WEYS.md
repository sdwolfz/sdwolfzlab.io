---
title: "WEYCO GROUP (WEYS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>WEYCO GROUP</td></tr>
    <tr><td>Symbol</td><td>WEYS</td></tr>
    <tr><td>Web</td><td><a href="https://www.weycogroup.com">www.weycogroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.48 |
| 2020 | 0.96 |
| 2019 | 0.95 |
| 2018 | 0.91 |
| 2017 | 0.87 |
| 2016 | 0.83 |
| 2015 | 0.79 |
| 2014 | 0.75 |
| 2013 | 0.54 |
| 2012 | 0.84 |
| 2011 | 0.64 |
| 2010 | 0.63 |
| 2009 | 0.59 |
| 2008 | 0.53 |
| 2007 | 0.42 |
| 2006 | 0.34 |
| 2005 | 0.32 |
| 2004 | 0.43 |
| 2003 | 0.52 |
| 2002 | 0.51 |
| 2001 | 0.47 |
| 2000 | 0.43 |
| 1999 | 0.39 |
| 1998 | 0.35 |
| 1997 | 0.61 |
| 1996 | 0.87 |
| 1995 | 0.83 |
| 1994 | 0.8 |
| 1993 | 0.78 |
| 1992 | 0.7 |
| 1991 | 0.63 |
| 1990 | 0.85 |
| 1989 | 0.95 |
| 1988 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
