---
title: "Lowland Inv. (LWI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Lowland Inv.</td></tr>
    <tr><td>Symbol</td><td>LWI</td></tr>
    <tr><td>Web</td><td><a href="https://www.lowlandinvestment.com">www.lowlandinvestment.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 60.0 |
| 2019 | 59.5 |
| 2018 | 54.0 |
| 2017 | 49.0 |
| 2016 | 45.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
