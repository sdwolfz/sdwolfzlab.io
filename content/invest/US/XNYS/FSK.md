---
title: "FS KKR CAPITAL CORP (FSK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FS KKR CAPITAL CORP</td></tr>
    <tr><td>Symbol</td><td>FSK</td></tr>
    <tr><td>Web</td><td><a href="https://www.fskkradvisor.com">www.fskkradvisor.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.6 |
| 2020 | 1.99 |
| 2019 | 0.76 |
| 2018 | 0.19 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
