---
title: "STANLEY BLACK & DECKER INC (SWK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>STANLEY BLACK & DECKER INC</td></tr>
    <tr><td>Symbol</td><td>SWK</td></tr>
    <tr><td>Web</td><td><a href="https://www.stanleyblackanddecker.com">www.stanleyblackanddecker.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.4 |
| 2020 | 2.78 |
| 2019 | 2.7 |
| 2018 | 2.58 |
| 2017 | 2.42 |
| 2016 | 2.26 |
| 2015 | 2.14 |
| 2014 | 2.04 |
| 2013 | 1.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
