---
title: "FAMOS SA ODORHEIU SECUIESC (FAMS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>FAMOS SA ODORHEIU SECUIESC</td></tr>
    <tr><td>Symbol</td><td>FAMS</td></tr>
    <tr><td>Web</td><td><a href="https://www.famos.ro">www.famos.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.25 |
| 2018 | 0.25 |
| 2017 | 0.25 |
| 2016 | 0.25 |
| 2015 | 0.25 |
| 2014 | 0.25 |
| 2013 | 0.2499 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
