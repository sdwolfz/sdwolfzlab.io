---
title: "BP (BP.)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>BP</td></tr>
    <tr><td>Symbol</td><td>BP.</td></tr>
    <tr><td>Web</td><td><a href="https://www.bp.com">www.bp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 31.5 |
| 2019 | 41.0 |
| 2018 | 40.5 |
| 2017 | 40.0 |
| 2016 | 40.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
