---
title: "SOUTHERN NATIONAL BANCORP OF VA INC (SONA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SOUTHERN NATIONAL BANCORP OF VA INC</td></tr>
    <tr><td>Symbol</td><td>SONA</td></tr>
    <tr><td>Web</td><td><a href="http://www.sonabank.com">www.sonabank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.4 |
| 2019 | 0.36 |
| 2018 | 0.32 |
| 2017 | 0.32 |
| 2016 | 0.32 |
| 2015 | 0.52 |
| 2014 | 0.6 |
| 2013 | 0.25 |
| 2012 | 0.245 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
