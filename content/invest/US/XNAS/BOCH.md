---
title: "BANK OF COMMERCE HOLDINGS INC (BOCH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BANK OF COMMERCE HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>BOCH</td></tr>
    <tr><td>Web</td><td><a href="https://www.bankofcommerceholdings.com">www.bankofcommerceholdings.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.06 |
| 2020 | 0.21 |
| 2019 | 0.19 |
| 2018 | 0.15 |
| 2017 | 0.12 |
| 2016 | 0.12 |
| 2015 | 0.12 |
| 2014 | 0.12 |
| 2013 | 0.14 |
| 2012 | 0.12 |
| 2011 | 0.12 |
| 2010 | 0.18 |
| 2009 | 0.24 |
| 2008 | 0.32 |
| 2007 | 0.32 |
| 2006 | 0.3 |
| 2005 | 0.26 |
| 2004 | 0.23 |
| 2003 | 0.65 |
| 2002 | 0.65 |
| 2001 | 0.65 |
| 1999 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
