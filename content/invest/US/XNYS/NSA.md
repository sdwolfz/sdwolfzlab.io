---
title: " (NSA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>NSA</td></tr>
    <tr><td>Web</td><td><a href="https://www.nationalstorageaffiliates.com">www.nationalstorageaffiliates.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.35 |
| 2020 | 1.35 |
| 2019 | 1.27 |
| 2018 | 1.16 |
| 2017 | 1.04 |
| 2016 | 0.88 |
| 2015 | 0.54 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
