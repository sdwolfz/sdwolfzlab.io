---
title: " (WHLRD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>WHLRD</td></tr>
    <tr><td>Web</td><td><a href="https://www.whlr.us">www.whlr.us</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 1.641 |
| 2017 | 2.188 |
| 2016 | 0.607 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
