---
title: "OLIN CORP (OLN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>OLIN CORP</td></tr>
    <tr><td>Symbol</td><td>OLN</td></tr>
    <tr><td>Web</td><td><a href="https://www.olin.com">www.olin.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.4 |
| 2020 | 0.8 |
| 2019 | 0.8 |
| 2018 | 0.8 |
| 2017 | 0.8 |
| 2016 | 0.8 |
| 2015 | 0.8 |
| 2014 | 0.8 |
| 2013 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
