---
title: "FIRST AMERICAN FINANCIAL CORP (FAF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FIRST AMERICAN FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>FAF</td></tr>
    <tr><td>Web</td><td><a href="https://www.firstam.com">www.firstam.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.46 |
| 2020 | 1.78 |
| 2019 | 1.68 |
| 2018 | 1.6 |
| 2017 | 1.44 |
| 2016 | 1.2 |
| 2015 | 1.0 |
| 2014 | 0.84 |
| 2013 | 0.36 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
