---
title: "FONAR CORP (FONR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FONAR CORP</td></tr>
    <tr><td>Symbol</td><td>FONR</td></tr>
    <tr><td>Web</td><td><a href="https://www.fonar.com">www.fonar.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 1999 | 0.008 |
| 1998 | 0.038 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
