---
title: "NELNET INC (NNI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NELNET INC</td></tr>
    <tr><td>Symbol</td><td>NNI</td></tr>
    <tr><td>Web</td><td><a href="https://www.nelnet.com">www.nelnet.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.22 |
| 2020 | 0.82 |
| 2019 | 0.74 |
| 2018 | 0.66 |
| 2017 | 0.58 |
| 2016 | 0.5 |
| 2015 | 0.42 |
| 2014 | 0.4 |
| 2013 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
