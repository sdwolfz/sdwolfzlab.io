---
title: "JAMES FINANCIAL GROUP INC (BOTJ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>JAMES FINANCIAL GROUP INC</td></tr>
    <tr><td>Symbol</td><td>BOTJ</td></tr>
    <tr><td>Web</td><td><a href="https://www.bankofthejames.bank">www.bankofthejames.bank</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.14 |
| 2020 | 0.28 |
| 2019 | 0.28 |
| 2018 | 0.24 |
| 2017 | 0.24 |
| 2016 | 0.24 |
| 2015 | 0.22 |
| 2014 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
