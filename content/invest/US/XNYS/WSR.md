---
title: " (WSR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>WSR</td></tr>
    <tr><td>Web</td><td><a href="https://www.whitestonereit.com">www.whitestonereit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.213 |
| 2020 | 0.565 |
| 2019 | 0.95 |
| 2018 | 0.95 |
| 2017 | 1.045 |
| 2016 | 1.14 |
| 2015 | 1.14 |
| 2014 | 1.14 |
| 2013 | 0.38 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
