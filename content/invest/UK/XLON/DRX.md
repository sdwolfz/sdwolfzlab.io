---
title: "Drax (DRX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Drax</td></tr>
    <tr><td>Symbol</td><td>DRX</td></tr>
    <tr><td>Web</td><td><a href="https://www.drax.com">www.drax.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 17.1 |
| 2019 | 15.9 |
| 2018 | 13.0 |
| 2017 | 4.53 |
| 2016 | 2.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
