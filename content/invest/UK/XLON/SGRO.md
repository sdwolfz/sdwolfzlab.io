---
title: "Segro (SGRO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Segro</td></tr>
    <tr><td>Symbol</td><td>SGRO</td></tr>
    <tr><td>Web</td><td><a href="https://www.segro.com">www.segro.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 22.1 |
| 2019 | 20.7 |
| 2018 | 18.8 |
| 2017 | 16.6 |
| 2016 | 16.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
