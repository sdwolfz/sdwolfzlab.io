---
title: " (NRT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>NRT</td></tr>
    <tr><td>Web</td><td><a href="https://www.neort.com">www.neort.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.18 |
| 2020 | 0.32 |
| 2019 | 0.82 |
| 2018 | 0.7 |
| 2017 | 0.76 |
| 2016 | 0.67 |
| 2015 | 1.27 |
| 2014 | 1.95 |
| 2013 | 1.02 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
