---
title: "CINCINNATI FINANCIAL CORP (CINF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CINCINNATI FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>CINF</td></tr>
    <tr><td>Web</td><td><a href="https://www.cinfin.com">www.cinfin.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.63 |
| 2020 | 2.4 |
| 2019 | 2.24 |
| 2018 | 2.12 |
| 2017 | 2.5 |
| 2016 | 1.92 |
| 2015 | 2.3 |
| 2014 | 1.76 |
| 2013 | 1.655 |
| 2012 | 1.62 |
| 2011 | 1.605 |
| 2010 | 1.59 |
| 2009 | 1.57 |
| 2008 | 1.56 |
| 2007 | 1.42 |
| 2006 | 1.34 |
| 2005 | 1.22 |
| 2004 | 1.1 |
| 2003 | 1.0 |
| 2002 | 0.89 |
| 2001 | 0.84 |
| 2000 | 0.76 |
| 1999 | 0.68 |
| 1998 | 0.92 |
| 1997 | 1.64 |
| 1996 | 1.11 |
| 1995 | 1.02 |
| 1994 | 1.28 |
| 1993 | 1.12 |
| 1992 | 1.54 |
| 1991 | 2.72 |
| 1990 | 2.44 |
| 1989 | 2.16 |
| 1988 | 0.44 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
