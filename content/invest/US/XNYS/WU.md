---
title: "WESTERN UNION COMPANY (THE) (WU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>WESTERN UNION COMPANY (THE)</td></tr>
    <tr><td>Symbol</td><td>WU</td></tr>
    <tr><td>Web</td><td><a href="https://www.westernunion.com">www.westernunion.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.235 |
| 2020 | 0.9 |
| 2019 | 0.8 |
| 2018 | 0.76 |
| 2017 | 0.7 |
| 2016 | 0.64 |
| 2015 | 0.62 |
| 2014 | 0.5 |
| 2013 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
