---
title: "QUANTA SERVICES (PWR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>QUANTA SERVICES</td></tr>
    <tr><td>Symbol</td><td>PWR</td></tr>
    <tr><td>Web</td><td><a href="https://www.quantaservices.com">www.quantaservices.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.06 |
| 2020 | 0.21 |
| 2019 | 0.17 |
| 2018 | 0.04 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
