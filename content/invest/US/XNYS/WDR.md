---
title: "WADDELL & REED FINANCIAL INC (WDR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>WADDELL & REED FINANCIAL INC</td></tr>
    <tr><td>Symbol</td><td>WDR</td></tr>
    <tr><td>Web</td><td><a href="http://www.waddell.com">www.waddell.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 1.0 |
| 2019 | 1.0 |
| 2018 | 1.0 |
| 2017 | 1.84 |
| 2016 | 1.84 |
| 2015 | 1.72 |
| 2014 | 1.36 |
| 2013 | 0.56 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
