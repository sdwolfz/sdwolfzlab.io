---
title: "XCEL ENERGY INC (XEL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>XCEL ENERGY INC</td></tr>
    <tr><td>Symbol</td><td>XEL</td></tr>
    <tr><td>Web</td><td><a href="https://www.xcelenergy.com">www.xcelenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.458 |
| 2020 | 1.72 |
| 2019 | 1.62 |
| 2018 | 1.52 |
| 2017 | 1.44 |
| 2016 | 1.36 |
| 2015 | 1.28 |
| 2014 | 1.2 |
| 2013 | 1.11 |
| 2012 | 1.07 |
| 2011 | 1.033 |
| 2010 | 0.75 |
| 2009 | 0.972 |
| 2008 | 0.942 |
| 2007 | 0.913 |
| 2006 | 0.883 |
| 2005 | 0.853 |
| 2004 | 0.81 |
| 2003 | 0.75 |
| 2002 | 1.125 |
| 2001 | 1.5 |
| 2000 | 0.752 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
