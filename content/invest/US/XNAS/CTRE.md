---
title: " (CTRE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CTRE</td></tr>
    <tr><td>Web</td><td><a href="https://www.caretrustreit.com">www.caretrustreit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.265 |
| 2020 | 1.0 |
| 2019 | 0.9 |
| 2018 | 0.82 |
| 2017 | 0.74 |
| 2016 | 0.68 |
| 2015 | 0.64 |
| 2014 | 6.005 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
