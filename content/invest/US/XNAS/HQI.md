---
title: "HIREQUEST INC (HQI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HIREQUEST INC</td></tr>
    <tr><td>Symbol</td><td>HQI</td></tr>
    <tr><td>Web</td><td><a href="https://www.hirequest.com">www.hirequest.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.05 |
| 2020 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
