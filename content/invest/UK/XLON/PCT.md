---
title: "Polar Capital Technology Trust (PCT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Polar Capital Technology Trust</td></tr>
    <tr><td>Symbol</td><td>PCT</td></tr>
    <tr><td>Web</td><td><a href="https://www.polarcapitaltechnologytrust.co.uk">www.polarcapitaltechnologytrust.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
