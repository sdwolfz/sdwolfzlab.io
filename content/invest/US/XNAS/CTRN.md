---
title: "CITI TRENDS INC (CTRN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CITI TRENDS INC</td></tr>
    <tr><td>Symbol</td><td>CTRN</td></tr>
    <tr><td>Web</td><td><a href="https://www.cititrends.com">www.cititrends.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.08 |
| 2019 | 0.32 |
| 2018 | 0.32 |
| 2017 | 0.3 |
| 2016 | 0.24 |
| 2015 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
