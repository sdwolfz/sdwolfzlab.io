---
title: "How to Use: yamllint"
date: 2020-06-28T12:01:44Z
type: posts
slug: how-to-use-yamllint
series: ["How to Use"]
tags: ["yaml", "tutorial", "linting", "productivity"]
---

## Install

From package:

```sh
# Manjaro / Arch
sudo pacman -S yamllint

# Alpine
apk add --update yamllint
```

From Docker (no official image so use mine):

```sh
docker run --rm -it -v "$PWD":/work -w /work sdwolfz/yamllint:latest sh
```

## Configure

Add a `.yamllint.yml` file containing your rules:

```yml
---

extends: default

rules:
  line-length:
    max: 80
    level: warning
```

## Use

Single file:

```sh
yamllint ./path/to/yaml-file.yml
```

All files:

```sh
yamllint .
```
