---
title: "KEYSTONE INV TST PLC ORD 10P (KIT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>KEYSTONE INV TST PLC ORD 10P</td></tr>
    <tr><td>Symbol</td><td>KIT</td></tr>
    <tr><td>Web</td><td><a href="http://www.invesco.co.uk">www.invesco.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 59.67 |
| 2018 | 57.75 |
| 2017 | 59.7 |
| 2016 | 58.3 |
| 2015 | 63.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
