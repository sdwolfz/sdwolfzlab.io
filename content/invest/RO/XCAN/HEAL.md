---
title: "HELIOS SA ASTILEU (HEAL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>HELIOS SA ASTILEU</td></tr>
    <tr><td>Symbol</td><td>HEAL</td></tr>
    <tr><td>Web</td><td><a href="https://www.heliossa.ro">www.heliossa.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
