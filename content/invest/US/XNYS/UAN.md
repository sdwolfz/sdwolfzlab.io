---
title: "CVR PARTNERS LP (UAN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CVR PARTNERS LP</td></tr>
    <tr><td>Symbol</td><td>UAN</td></tr>
    <tr><td>Web</td><td><a href="https://www.cvrpartners.com">www.cvrpartners.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.4 |
| 2017 | 0.02 |
| 2016 | 0.71 |
| 2015 | 1.25 |
| 2014 | 1.41 |
| 2013 | 0.943 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
