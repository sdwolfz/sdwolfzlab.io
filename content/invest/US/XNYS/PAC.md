---
title: " (PAC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>PAC</td></tr>
    <tr><td>Web</td><td><a href="https://www.aeropuertosgap.com.mx">www.aeropuertosgap.com.mx</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 3.853 |
| 2018 | 4.704 |
| 2017 | 2.787 |
| 2016 | 1.886 |
| 2015 | 1.761 |
| 2014 | 4.532 |
| 2013 | 1.807 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
