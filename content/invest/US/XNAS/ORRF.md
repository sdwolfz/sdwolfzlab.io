---
title: "ORRSTOWN FINANCIAL SERVICES INC (ORRF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ORRSTOWN FINANCIAL SERVICES INC</td></tr>
    <tr><td>Symbol</td><td>ORRF</td></tr>
    <tr><td>Web</td><td><a href="https://www.orrstown.com">www.orrstown.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.36 |
| 2020 | 0.68 |
| 2019 | 0.6 |
| 2018 | 0.51 |
| 2017 | 0.42 |
| 2016 | 0.35 |
| 2015 | 0.22 |
| 2011 | 0.69 |
| 2010 | 0.89 |
| 2009 | 0.88 |
| 2008 | 0.87 |
| 2007 | 0.84 |
| 2006 | 0.78 |
| 2005 | 0.72 |
| 2004 | 0.37 |
| 2003 | 0.85 |
| 2002 | 0.72 |
| 2001 | 0.61 |
| 2000 | 0.57 |
| 1999 | 0.54 |
| 1998 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
