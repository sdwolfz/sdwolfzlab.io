---
title: "FULCRUM THERAPEUTICS INC (FULC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FULCRUM THERAPEUTICS INC</td></tr>
    <tr><td>Symbol</td><td>FULC</td></tr>
    <tr><td>Web</td><td><a href="https://www.fulcrumtx.com">www.fulcrumtx.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
