---
title: "FEDERAL AGRICULTURAL MORTGAGE CORP (AGM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FEDERAL AGRICULTURAL MORTGAGE CORP</td></tr>
    <tr><td>Symbol</td><td>AGM</td></tr>
    <tr><td>Web</td><td><a href="https://www.farmermac.com">www.farmermac.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.88 |
| 2020 | 3.2 |
| 2019 | 2.8 |
| 2018 | 2.32 |
| 2017 | 1.44 |
| 2016 | 1.04 |
| 2015 | 0.64 |
| 2014 | 0.7 |
| 2013 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
