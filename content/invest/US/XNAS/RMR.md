---
title: "THE RMR GROUP INC (RMR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>THE RMR GROUP INC</td></tr>
    <tr><td>Symbol</td><td>RMR</td></tr>
    <tr><td>Web</td><td><a href="https://www.rmrgroup.com">www.rmrgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.76 |
| 2020 | 1.52 |
| 2019 | 1.43 |
| 2018 | 1.1 |
| 2017 | 1.0 |
| 2016 | 0.799 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
