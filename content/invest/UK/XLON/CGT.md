---
title: "Capital Gearing (CGT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Capital Gearing</td></tr>
    <tr><td>Symbol</td><td>CGT</td></tr>
    <tr><td>Web</td><td><a href="https://www.capitalgearingtrust.com">www.capitalgearingtrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 42.0 |
| 2019 | 35.0 |
| 2018 | 27.0 |
| 2017 | 20.0 |
| 2016 | 20.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
