---
title: "PARK NATIONAL CORP (PRK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PARK NATIONAL CORP</td></tr>
    <tr><td>Symbol</td><td>PRK</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.26 |
| 2020 | 4.28 |
| 2019 | 4.24 |
| 2018 | 4.07 |
| 2017 | 3.76 |
| 2016 | 3.76 |
| 2015 | 3.76 |
| 2014 | 3.76 |
| 2013 | 3.76 |
| 2012 | 3.76 |
| 2011 | 3.76 |
| 2010 | 3.76 |
| 2009 | 3.76 |
| 2008 | 3.77 |
| 2007 | 3.73 |
| 2006 | 3.69 |
| 2005 | 3.62 |
| 2004 | 3.54 |
| 2003 | 2.49 |
| 2002 | 3.11 |
| 2001 | 2.89 |
| 2000 | 2.66 |
| 1999 | 0.65 |
| 1994 | 0.45 |
| 1993 | 2.1 |
| 1992 | 1.75 |
| 1991 | 1.53 |
| 1990 | 1.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
