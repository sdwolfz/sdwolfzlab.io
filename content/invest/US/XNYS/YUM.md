---
title: "YUM BRANDS INC (YUM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>YUM BRANDS INC</td></tr>
    <tr><td>Symbol</td><td>YUM</td></tr>
    <tr><td>Web</td><td><a href="https://www.yum.com">www.yum.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.5 |
| 2020 | 1.88 |
| 2019 | 1.68 |
| 2018 | 1.44 |
| 2017 | 1.2 |
| 2016 | 1.89 |
| 2015 | 1.69 |
| 2014 | 1.52 |
| 2013 | 0.705 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
