---
title: " (BTI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BTI</td></tr>
    <tr><td>Web</td><td><a href="https://www.bat.com">www.bat.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.482 |
| 2020 | 2.729 |
| 2019 | 2.659 |
| 2018 | 2.649 |
| 2017 | 2.844 |
| 2016 | 3.68 |
| 2015 | 4.535 |
| 2014 | 4.82 |
| 2013 | 1.451 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
