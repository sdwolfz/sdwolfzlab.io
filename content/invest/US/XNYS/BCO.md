---
title: "BRINK'S COMPANY (BCO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BRINK'S COMPANY</td></tr>
    <tr><td>Symbol</td><td>BCO</td></tr>
    <tr><td>Web</td><td><a href="https://www.brinks.com">www.brinks.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.15 |
| 2020 | 0.6 |
| 2019 | 0.6 |
| 2018 | 0.6 |
| 2017 | 0.55 |
| 2016 | 0.4 |
| 2015 | 0.4 |
| 2014 | 0.4 |
| 2013 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
