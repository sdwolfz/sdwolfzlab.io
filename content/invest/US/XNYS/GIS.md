---
title: "GENERAL MILLS INC (GIS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GENERAL MILLS INC</td></tr>
    <tr><td>Symbol</td><td>GIS</td></tr>
    <tr><td>Web</td><td><a href="https://www.generalmills.com">www.generalmills.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.02 |
| 2020 | 1.98 |
| 2019 | 1.96 |
| 2018 | 1.96 |
| 2017 | 1.94 |
| 2016 | 1.86 |
| 2015 | 1.73 |
| 2014 | 1.61 |
| 2013 | 0.76 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
