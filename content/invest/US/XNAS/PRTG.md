---
title: "PORTAGE BIOTECH INC (PRTG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PORTAGE BIOTECH INC</td></tr>
    <tr><td>Symbol</td><td>PRTG</td></tr>
    <tr><td>Web</td><td><a href="https://www.portagebiotech.com">www.portagebiotech.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
