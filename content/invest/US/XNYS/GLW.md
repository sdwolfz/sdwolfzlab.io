---
title: "CORNING INC (GLW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CORNING INC</td></tr>
    <tr><td>Symbol</td><td>GLW</td></tr>
    <tr><td>Web</td><td><a href="https://www.corning.com">www.corning.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.48 |
| 2020 | 0.88 |
| 2019 | 0.8 |
| 2018 | 0.72 |
| 2017 | 0.62 |
| 2016 | 0.54 |
| 2015 | 0.48 |
| 2014 | 0.4 |
| 2013 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
