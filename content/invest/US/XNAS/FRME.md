---
title: "FIRST MERCHANTS CORP (FRME)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST MERCHANTS CORP</td></tr>
    <tr><td>Symbol</td><td>FRME</td></tr>
    <tr><td>Web</td><td><a href="https://www.firstmerchants.com">www.firstmerchants.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.26 |
| 2020 | 1.04 |
| 2019 | 1.0 |
| 2018 | 0.84 |
| 2017 | 0.69 |
| 2016 | 0.54 |
| 2015 | 0.41 |
| 2014 | 0.29 |
| 2013 | 0.18 |
| 2012 | 0.1 |
| 2011 | 0.04 |
| 2010 | 0.04 |
| 2009 | 0.47 |
| 2008 | 0.92 |
| 2007 | 0.92 |
| 2006 | 0.92 |
| 2005 | 0.92 |
| 2004 | 0.92 |
| 2003 | 0.92 |
| 2002 | 0.92 |
| 2001 | 0.92 |
| 2000 | 0.9 |
| 1999 | 0.84 |
| 1998 | 1.06 |
| 1997 | 1.04 |
| 1996 | 0.88 |
| 1995 | 1.06 |
| 1994 | 1.34 |
| 1993 | 0.94 |
| 1992 | 0.95 |
| 1991 | 1.18 |
| 1990 | 1.06 |
| 1989 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
