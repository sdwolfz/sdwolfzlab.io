---
title: "STOCK YARDS BANCORP INC (SYBT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>STOCK YARDS BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>SYBT</td></tr>
    <tr><td>Web</td><td><a href="https://www.syb.com">www.syb.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.27 |
| 2020 | 1.08 |
| 2019 | 1.04 |
| 2018 | 0.96 |
| 2017 | 0.8 |
| 2016 | 0.8 |
| 2015 | 0.96 |
| 2014 | 0.88 |
| 2013 | 0.81 |
| 2012 | 0.77 |
| 2011 | 0.72 |
| 2010 | 0.69 |
| 2009 | 0.68 |
| 2008 | 0.68 |
| 2007 | 0.63 |
| 2006 | 0.58 |
| 2005 | 0.47 |
| 2004 | 0.29 |
| 2003 | 0.45 |
| 2002 | 0.52 |
| 2001 | 0.45 |
| 2000 | 0.39 |
| 1999 | 0.09 |
| 1997 | 0.48 |
| 1996 | 0.6 |
| 1995 | 0.72 |
| 1994 | 0.61 |
| 1993 | 0.37 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
