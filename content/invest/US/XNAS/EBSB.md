---
title: "MERIDIAN BANCORP INC (EBSB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MERIDIAN BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>EBSB</td></tr>
    <tr><td>Web</td><td><a href="https://www.ebsb.com">www.ebsb.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.32 |
| 2019 | 0.29 |
| 2018 | 0.22 |
| 2017 | 0.17 |
| 2016 | 0.12 |
| 2015 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
