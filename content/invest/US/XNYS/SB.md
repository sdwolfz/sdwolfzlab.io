---
title: "SAFE BULKERS INC (SB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SAFE BULKERS INC</td></tr>
    <tr><td>Symbol</td><td>SB</td></tr>
    <tr><td>Web</td><td><a href="https://www.safebulkers.com">www.safebulkers.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 0.04 |
| 2014 | 0.22 |
| 2013 | 0.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
