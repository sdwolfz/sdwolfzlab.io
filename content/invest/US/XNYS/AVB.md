---
title: " (AVB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>AVB</td></tr>
    <tr><td>Web</td><td><a href="https://www.avalonbay.com">www.avalonbay.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.59 |
| 2020 | 6.36 |
| 2019 | 6.08 |
| 2018 | 5.88 |
| 2017 | 5.68 |
| 2016 | 5.4 |
| 2015 | 5.0 |
| 2014 | 4.64 |
| 2013 | 3.21 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
