---
title: " (BCS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BCS</td></tr>
    <tr><td>Web</td><td><a href="https://www.barclays.com">www.barclays.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.054 |
| 2019 | 0.348 |
| 2018 | 0.236 |
| 2017 | 0.148 |
| 2016 | 0.242 |
| 2015 | 0.39 |
| 2014 | 0.42 |
| 2013 | 0.124 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
