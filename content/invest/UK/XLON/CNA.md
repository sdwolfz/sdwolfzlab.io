---
title: "Centrica (CNA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Centrica</td></tr>
    <tr><td>Symbol</td><td>CNA</td></tr>
    <tr><td>Web</td><td><a href="https://www.centrica.com">www.centrica.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 5.0 |
| 2018 | 12.0 |
| 2017 | 12.0 |
| 2016 | 12.0 |
| 2015 | 12.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
