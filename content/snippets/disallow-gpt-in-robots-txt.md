---
title: "Disallow GPT in robots.txt"
date: 2023-08-12T05:50:06Z
type: posts
slug: disallow-gpt-in-robots-txt
tags: ["security", "humanity"]
---

Enable `robots.txt` in your `hugo.toml` (formerly `config.toml`):

```toml
enableRobotsTXT = true
```

Add a file `layouts/robots.txt` with:

```txt
User-agent: GPTBot
Disallow: /

User-agent: *
Allow: /
```
