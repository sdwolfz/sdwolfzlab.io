---
title: "ARTESIAN RESOURCES CORP (ARTNA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ARTESIAN RESOURCES CORP</td></tr>
    <tr><td>Symbol</td><td>ARTNA</td></tr>
    <tr><td>Web</td><td><a href="https://www.artesianwater.com">www.artesianwater.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.518 |
| 2020 | 1.006 |
| 2019 | 0.984 |
| 2018 | 0.955 |
| 2017 | 0.927 |
| 2016 | 0.9 |
| 2015 | 0.873 |
| 2014 | 0.848 |
| 2013 | 0.823 |
| 2012 | 0.791 |
| 2011 | 0.763 |
| 2010 | 0.753 |
| 2009 | 0.723 |
| 2008 | 0.707 |
| 2007 | 0.664 |
| 2006 | 0.764 |
| 2005 | 0.871 |
| 2004 | 0.83 |
| 2003 | 0.996 |
| 2002 | 1.16 |
| 2001 | 1.11 |
| 2000 | 1.095 |
| 1999 | 1.06 |
| 1998 | 0.97 |
| 1997 | 0.92 |
| 1996 | 0.46 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
