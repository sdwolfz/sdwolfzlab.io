---
title: " (TAK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>TAK</td></tr>
    <tr><td>Web</td><td><a href="https://www.takeda.com">www.takeda.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.666 |
| 2019 | 0.604 |
| 2018 | 0.288 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
