---
title: "REGAL BELOIT CORPORATION (RBC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>REGAL BELOIT CORPORATION</td></tr>
    <tr><td>Symbol</td><td>RBC</td></tr>
    <tr><td>Web</td><td><a href="https://www.regalbeloit.com">www.regalbeloit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.63 |
| 2020 | 1.2 |
| 2019 | 1.18 |
| 2018 | 1.1 |
| 2017 | 1.02 |
| 2016 | 0.95 |
| 2015 | 0.91 |
| 2014 | 0.86 |
| 2013 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
