---
title: " (ACC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ACC</td></tr>
    <tr><td>Web</td><td><a href="https://www.americancampus.com">www.americancampus.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.94 |
| 2020 | 1.88 |
| 2019 | 1.87 |
| 2018 | 1.82 |
| 2017 | 1.74 |
| 2016 | 1.66 |
| 2015 | 1.58 |
| 2014 | 1.5 |
| 2013 | 0.72 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
