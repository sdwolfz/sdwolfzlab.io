---
title: "FERROGLOBE PLC (GSM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FERROGLOBE PLC</td></tr>
    <tr><td>Symbol</td><td>GSM</td></tr>
    <tr><td>Web</td><td><a href="https://www.ferroglobe.com">www.ferroglobe.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.12 |
| 2016 | 0.32 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
