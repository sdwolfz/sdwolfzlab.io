---
title: " (ZIONP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ZIONP</td></tr>
    <tr><td>Web</td><td><a href="https://www.zionsbancorp.com">www.zionsbancorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 1.017 |
| 2019 | 1.006 |
| 2018 | 1.011 |
| 2017 | 1.014 |
| 2016 | 1.017 |
| 2015 | 1.018 |
| 2014 | 1.014 |
| 2013 | 1.014 |
| 2012 | 1.017 |
| 2011 | 1.014 |
| 2010 | 1.014 |
| 2009 | 0.764 |
| 2008 | 0.604 |
| 2007 | 0.893 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
