---
title: "SCOTTS MIRACLE-GRO COMPANY (SMG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SCOTTS MIRACLE-GRO COMPANY</td></tr>
    <tr><td>Symbol</td><td>SMG</td></tr>
    <tr><td>Web</td><td><a href="https://www.scottsmiraclegro.com">www.scottsmiraclegro.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.24 |
| 2020 | 7.4 |
| 2019 | 2.26 |
| 2018 | 2.16 |
| 2017 | 2.06 |
| 2016 | 1.94 |
| 2015 | 1.84 |
| 2014 | 1.776 |
| 2013 | 0.876 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
