---
title: "EASTMAN CHEMICAL CO (EMN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>EASTMAN CHEMICAL CO</td></tr>
    <tr><td>Symbol</td><td>EMN</td></tr>
    <tr><td>Web</td><td><a href="https://www.eastman.com">www.eastman.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.69 |
| 2020 | 2.67 |
| 2019 | 2.52 |
| 2018 | 2.3 |
| 2017 | 2.09 |
| 2016 | 1.89 |
| 2015 | 1.66 |
| 2014 | 1.45 |
| 2013 | 0.65 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
