---
title: "BMO Private Equity Trust (BPET)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>BMO Private Equity Trust</td></tr>
    <tr><td>Symbol</td><td>BPET</td></tr>
    <tr><td>Web</td><td><a href="https://www.bmoprivateequitytrust.com">www.bmoprivateequitytrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 15.33 |
| 2018 | 14.37 |
| 2017 | 14.04 |
| 2016 | 12.6 |
| 2015 | 11.41 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
