---
title: " (BDN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BDN</td></tr>
    <tr><td>Web</td><td><a href="https://www.brandywinerealty.com">www.brandywinerealty.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.38 |
| 2020 | 0.76 |
| 2019 | 0.76 |
| 2018 | 0.72 |
| 2017 | 0.64 |
| 2016 | 0.75 |
| 2015 | 0.6 |
| 2014 | 0.6 |
| 2013 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
