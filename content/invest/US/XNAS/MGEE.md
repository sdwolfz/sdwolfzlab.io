---
title: "MGE ENERGY INC (MGEE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MGE ENERGY INC</td></tr>
    <tr><td>Symbol</td><td>MGEE</td></tr>
    <tr><td>Web</td><td><a href="https://www.mgeenergy.com">www.mgeenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.37 |
| 2020 | 1.445 |
| 2019 | 1.38 |
| 2018 | 1.32 |
| 2017 | 1.26 |
| 2016 | 1.205 |
| 2015 | 1.155 |
| 2014 | 1.108 |
| 2013 | 1.605 |
| 2012 | 1.555 |
| 2011 | 1.515 |
| 2010 | 1.487 |
| 2009 | 1.46 |
| 2008 | 1.433 |
| 2007 | 1.407 |
| 2006 | 1.387 |
| 2005 | 1.373 |
| 2004 | 1.36 |
| 2003 | 1.348 |
| 2002 | 1.338 |
| 2001 | 1.328 |
| 2000 | 1.318 |
| 1999 | 1.308 |
| 1998 | 1.298 |
| 1997 | 1.287 |
| 1996 | 1.273 |
| 1995 | 1.89 |
| 1994 | 1.87 |
| 1993 | 1.84 |
| 1992 | 1.79 |
| 1991 | 2.62 |
| 1990 | 2.58 |
| 1989 | 2.52 |
| 1988 | 0.62 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
