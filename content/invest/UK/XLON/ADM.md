---
title: "Admiral (ADM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Admiral</td></tr>
    <tr><td>Symbol</td><td>ADM</td></tr>
    <tr><td>Web</td><td><a href="https://www.admiralgroup.co.uk">www.admiralgroup.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 129.0 |
| 2018 | 118.0 |
| 2017 | 107.5 |
| 2016 | 126.3 |
| 2015 | 100.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
