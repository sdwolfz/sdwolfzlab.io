---
title: "LINCOLN NATIONAL CORP (LNC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>LINCOLN NATIONAL CORP</td></tr>
    <tr><td>Symbol</td><td>LNC</td></tr>
    <tr><td>Web</td><td><a href="https://www.lfg.com">www.lfg.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.84 |
| 2020 | 1.6 |
| 2019 | 1.48 |
| 2018 | 1.32 |
| 2017 | 1.16 |
| 2016 | 1.0 |
| 2015 | 0.8 |
| 2014 | 0.64 |
| 2013 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
