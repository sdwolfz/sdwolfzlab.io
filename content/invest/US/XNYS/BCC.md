---
title: "BOISE CASCADE COMPANY (BCC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BOISE CASCADE COMPANY</td></tr>
    <tr><td>Symbol</td><td>BCC</td></tr>
    <tr><td>Web</td><td><a href="https://www.bc.com">www.bc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 2.0 |
| 2019 | 1.37 |
| 2018 | 1.3 |
| 2017 | 0.07 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
