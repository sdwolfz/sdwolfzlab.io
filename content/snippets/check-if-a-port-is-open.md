---
title: "Check if a port is open"
date: 2021-07-01T11:58:08Z
type: posts
slug: check-if-a-port-is-open
series: ["Networking"]
tags: ["port", "networking", "linux"]
---

## Install

```sh
sudo pacman -S openbsd-netcat
```

## Use

With an IP:

```sh
nc -v 127.0.0.1 5432
```

Or a URL:

```sh
nc -v www.codrut.pro 443
```
