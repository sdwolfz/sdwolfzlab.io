---
title: "VICTORY CAPITAL HOLDINGS INC (VCTR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>VICTORY CAPITAL HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>VCTR</td></tr>
    <tr><td>Web</td><td><a href="https://www.vcm.com">www.vcm.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.21 |
| 2020 | 0.23 |
| 2019 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
