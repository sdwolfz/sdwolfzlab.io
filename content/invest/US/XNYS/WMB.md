---
title: "WILLIAMS COMPANIES INC (WMB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>WILLIAMS COMPANIES INC</td></tr>
    <tr><td>Symbol</td><td>WMB</td></tr>
    <tr><td>Web</td><td><a href="https://www.williams.com">www.williams.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.82 |
| 2020 | 1.6 |
| 2019 | 1.52 |
| 2018 | 1.36 |
| 2017 | 1.2 |
| 2016 | 1.68 |
| 2015 | 2.45 |
| 2014 | 1.958 |
| 2013 | 1.098 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
