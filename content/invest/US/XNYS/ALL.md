---
title: "ALLSTATE CORP (ALL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ALLSTATE CORP</td></tr>
    <tr><td>Symbol</td><td>ALL</td></tr>
    <tr><td>Web</td><td><a href="https://www.allstate.com">www.allstate.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.81 |
| 2020 | 2.16 |
| 2019 | 2.0 |
| 2018 | 1.84 |
| 2017 | 1.48 |
| 2016 | 1.32 |
| 2015 | 1.2 |
| 2014 | 1.12 |
| 2013 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
