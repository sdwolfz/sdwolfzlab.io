---
title: "WALGREENS BOOTS ALLIANCE INC (WBA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>WALGREENS BOOTS ALLIANCE INC</td></tr>
    <tr><td>Symbol</td><td>WBA</td></tr>
    <tr><td>Web</td><td><a href="https://www.walgreens.com">www.walgreens.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.935 |
| 2020 | 1.85 |
| 2019 | 1.795 |
| 2018 | 1.68 |
| 2017 | 1.55 |
| 2016 | 1.47 |
| 2015 | 1.395 |
| 2014 | 1.305 |
| 2013 | 1.18 |
| 2012 | 1.0 |
| 2011 | 0.8 |
| 2010 | 0.625 |
| 2009 | 0.5 |
| 2008 | 0.415 |
| 2007 | 0.345 |
| 2006 | 0.285 |
| 2005 | 0.235 |
| 2004 | 0.664 |
| 2003 | 0.161 |
| 2002 | 0.146 |
| 2001 | 0.141 |
| 2000 | 0.069 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
