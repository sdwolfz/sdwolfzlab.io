---
title: "BORG WARNER INC (BWA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BORG WARNER INC</td></tr>
    <tr><td>Symbol</td><td>BWA</td></tr>
    <tr><td>Web</td><td><a href="https://www.borgwarner.com">www.borgwarner.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.34 |
| 2020 | 0.68 |
| 2019 | 0.68 |
| 2018 | 0.68 |
| 2017 | 0.59 |
| 2016 | 0.53 |
| 2015 | 0.52 |
| 2014 | 0.51 |
| 2013 | 0.375 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
