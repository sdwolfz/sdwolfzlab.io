---
title: "S&P GLOBAL INC (SPGI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>S&P GLOBAL INC</td></tr>
    <tr><td>Symbol</td><td>SPGI</td></tr>
    <tr><td>Web</td><td><a href="https://investor.spglobal.com">investor.spglobal.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.54 |
| 2020 | 2.68 |
| 2019 | 2.28 |
| 2018 | 2.0 |
| 2017 | 1.64 |
| 2016 | 1.44 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
