---
title: " (SIMO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SIMO</td></tr>
    <tr><td>Web</td><td><a href="https://www.siliconmotion.com">www.siliconmotion.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.7 |
| 2020 | 1.4 |
| 2019 | 1.25 |
| 2018 | 1.2 |
| 2017 | 0.9 |
| 2016 | 0.65 |
| 2015 | 0.6 |
| 2014 | 0.6 |
| 2013 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
