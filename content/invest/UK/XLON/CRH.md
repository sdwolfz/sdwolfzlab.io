---
title: "CRH (CRH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>CRH</td></tr>
    <tr><td>Symbol</td><td>CRH</td></tr>
    <tr><td>Web</td><td><a href="https://www.crh.com">www.crh.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 92.0 |
| 2019 | 72.4 |
| 2018 | 68.4 |
| 2017 | 65.4 |
| 2016 | 65.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
