---
title: "AMC ENTERTAINMENT HOLDINGS INC (AMC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AMC ENTERTAINMENT HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>AMC</td></tr>
    <tr><td>Web</td><td><a href="https://www.amctheatres.com">www.amctheatres.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.03 |
| 2019 | 0.8 |
| 2018 | 2.15 |
| 2017 | 0.8 |
| 2016 | 0.8 |
| 2015 | 0.8 |
| 2014 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
