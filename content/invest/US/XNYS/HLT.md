---
title: "HILTON WORLDWIDE HOLDINGS INC (HLT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HILTON WORLDWIDE HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>HLT</td></tr>
    <tr><td>Web</td><td><a href="https://www.hilton.com">www.hilton.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.15 |
| 2019 | 0.6 |
| 2018 | 0.6 |
| 2017 | 0.6 |
| 2016 | 0.42 |
| 2015 | 0.14 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
