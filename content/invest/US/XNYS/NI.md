---
title: "NISOURCE INC (NI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NISOURCE INC</td></tr>
    <tr><td>Symbol</td><td>NI</td></tr>
    <tr><td>Web</td><td><a href="https://www.nisource.com">www.nisource.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.44 |
| 2020 | 0.84 |
| 2019 | 0.8 |
| 2018 | 0.78 |
| 2017 | 0.7 |
| 2016 | 0.64 |
| 2015 | 0.83 |
| 2014 | 1.02 |
| 2013 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
