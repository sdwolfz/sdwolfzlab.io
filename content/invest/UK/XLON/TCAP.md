---
title: "TP ICAP (TCAP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>TP ICAP</td></tr>
    <tr><td>Symbol</td><td>TCAP</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 16.85 |
| 2018 | 16.85 |
| 2017 | 16.85 |
| 2016 | 16.85 |
| 2015 | 16.85 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
