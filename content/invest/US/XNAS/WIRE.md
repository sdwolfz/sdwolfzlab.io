---
title: "ENCORE WIRE CORP (WIRE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ENCORE WIRE CORP</td></tr>
    <tr><td>Symbol</td><td>WIRE</td></tr>
    <tr><td>Web</td><td><a href="https://www.encorewire.com">www.encorewire.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.04 |
| 2020 | 0.1 |
| 2019 | 0.08 |
| 2018 | 0.08 |
| 2017 | 0.08 |
| 2016 | 0.08 |
| 2015 | 0.06 |
| 2014 | 0.08 |
| 2013 | 0.1 |
| 2012 | 0.08 |
| 2011 | 0.08 |
| 2010 | 0.08 |
| 2009 | 0.06 |
| 2008 | 0.1 |
| 2007 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
