---
title: "AUTODESK INC (ADSK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AUTODESK INC</td></tr>
    <tr><td>Symbol</td><td>ADSK</td></tr>
    <tr><td>Web</td><td><a href="https://www.autodesk.com">www.autodesk.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2005 | 0.015 |
| 2004 | 0.105 |
| 2003 | 0.12 |
| 2002 | 0.21 |
| 2001 | 0.24 |
| 2000 | 0.24 |
| 1999 | 0.24 |
| 1998 | 0.24 |
| 1997 | 0.24 |
| 1996 | 0.24 |
| 1995 | 0.24 |
| 1994 | 0.48 |
| 1993 | 0.48 |
| 1992 | 0.36 |
| 1991 | 0.46 |
| 1990 | 0.4 |
| 1989 | 1.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
