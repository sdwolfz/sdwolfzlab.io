---
title: "DIEBOLD NIXDORF INCORPORATED (DBD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DIEBOLD NIXDORF INCORPORATED</td></tr>
    <tr><td>Symbol</td><td>DBD</td></tr>
    <tr><td>Web</td><td><a href="https://www.dieboldnixdorf.com">www.dieboldnixdorf.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.1 |
| 2017 | 0.4 |
| 2016 | 0.961 |
| 2015 | 1.148 |
| 2014 | 1.148 |
| 2013 | 0.574 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
