---
title: "WILLIAMS-SONOMA INC (WSM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>WILLIAMS-SONOMA INC</td></tr>
    <tr><td>Symbol</td><td>WSM</td></tr>
    <tr><td>Web</td><td><a href="https://www.williams-sonomainc.com">www.williams-sonomainc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.12 |
| 2020 | 1.92 |
| 2019 | 1.87 |
| 2018 | 1.68 |
| 2017 | 1.54 |
| 2016 | 1.46 |
| 2015 | 1.38 |
| 2014 | 1.3 |
| 2013 | 0.62 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
