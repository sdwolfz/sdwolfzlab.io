---
title: " (MDRR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>MDRR</td></tr>
    <tr><td>Web</td><td><a href="https://www.medalistreit.com">www.medalistreit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.125 |
| 2019 | 0.525 |
| 2018 | 0.175 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
