---
title: "PETMED EXPRESS INC (PETS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PETMED EXPRESS INC</td></tr>
    <tr><td>Symbol</td><td>PETS</td></tr>
    <tr><td>Web</td><td><a href="https://www.1800petmeds.com">www.1800petmeds.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.58 |
| 2020 | 1.11 |
| 2019 | 1.08 |
| 2018 | 1.04 |
| 2017 | 0.79 |
| 2016 | 0.75 |
| 2015 | 0.71 |
| 2014 | 0.68 |
| 2013 | 0.64 |
| 2012 | 1.6 |
| 2011 | 0.5 |
| 2010 | 0.45 |
| 2009 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
