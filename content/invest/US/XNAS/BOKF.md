---
title: "BOK FINANCIAL CORP (BOKF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BOK FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>BOKF</td></tr>
    <tr><td>Web</td><td><a href="https://www.bokf.com">www.bokf.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.04 |
| 2020 | 2.05 |
| 2019 | 2.01 |
| 2018 | 1.9 |
| 2017 | 1.77 |
| 2016 | 1.73 |
| 2015 | 1.69 |
| 2014 | 1.62 |
| 2013 | 1.54 |
| 2012 | 2.47 |
| 2011 | 1.13 |
| 2010 | 0.99 |
| 2009 | 0.945 |
| 2008 | 0.875 |
| 2007 | 0.75 |
| 2006 | 0.55 |
| 2005 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
