---
title: "ALLIANCE DATA SYSTEM (ADS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ALLIANCE DATA SYSTEM</td></tr>
    <tr><td>Symbol</td><td>ADS</td></tr>
    <tr><td>Web</td><td><a href="https://www.alliancedata.com">www.alliancedata.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.42 |
| 2020 | 1.26 |
| 2019 | 2.52 |
| 2018 | 2.28 |
| 2017 | 2.08 |
| 2016 | 0.52 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
