---
title: "HYSTER-YALE MATLS HANDLING INC (HY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HYSTER-YALE MATLS HANDLING INC</td></tr>
    <tr><td>Symbol</td><td>HY</td></tr>
    <tr><td>Web</td><td><a href="https://www.hyster-yale.com">www.hyster-yale.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.317 |
| 2020 | 1.268 |
| 2019 | 1.261 |
| 2018 | 1.233 |
| 2017 | 1.204 |
| 2016 | 1.17 |
| 2015 | 1.13 |
| 2014 | 1.075 |
| 2013 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
