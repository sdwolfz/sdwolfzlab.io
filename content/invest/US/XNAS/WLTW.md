---
title: "WILLIS TOWERS WATSON PLC (WLTW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>WILLIS TOWERS WATSON PLC</td></tr>
    <tr><td>Symbol</td><td>WLTW</td></tr>
    <tr><td>Web</td><td><a href="https://www.willistowerswatson.com">www.willistowerswatson.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.71 |
| 2020 | 2.75 |
| 2019 | 2.6 |
| 2018 | 2.4 |
| 2017 | 2.12 |
| 2016 | 1.92 |
| 2015 | 1.24 |
| 2014 | 1.2 |
| 2013 | 1.12 |
| 2012 | 1.08 |
| 2011 | 1.04 |
| 2010 | 1.04 |
| 2009 | 1.04 |
| 2008 | 1.04 |
| 2007 | 1.0 |
| 2006 | 0.94 |
| 2005 | 0.86 |
| 2004 | 0.725 |
| 2003 | 0.575 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
