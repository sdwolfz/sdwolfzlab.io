---
title: "CURO GROUP HOLDINGS CORP (CURO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CURO GROUP HOLDINGS CORP</td></tr>
    <tr><td>Symbol</td><td>CURO</td></tr>
    <tr><td>Web</td><td><a href="https://www.curo.com">www.curo.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.165 |
| 2020 | 0.22 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
