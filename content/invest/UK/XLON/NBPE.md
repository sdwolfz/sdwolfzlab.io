---
title: "Nb Priv. Eqty (NBPE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Nb Priv. Eqty</td></tr>
    <tr><td>Symbol</td><td>NBPE</td></tr>
    <tr><td>Web</td><td><a href="https://www.nbprivateequitypartners.com">www.nbprivateequitypartners.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 57.0 |
| 2018 | 53.0 |
| 2017 | 50.0 |
| 2016 | 50.0 |
| 2015 | 48.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
