---
title: " (MMLP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>MMLP</td></tr>
    <tr><td>Web</td><td><a href="https://www.mmlp.com">www.mmlp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.01 |
| 2020 | 0.135 |
| 2019 | 1.25 |
| 2018 | 2.0 |
| 2017 | 2.0 |
| 2016 | 2.938 |
| 2015 | 3.25 |
| 2014 | 3.178 |
| 2013 | 3.108 |
| 2012 | 3.058 |
| 2011 | 3.047 |
| 2010 | 3.0 |
| 2009 | 3.0 |
| 2008 | 2.91 |
| 2007 | 2.6 |
| 2006 | 2.44 |
| 2005 | 2.19 |
| 2004 | 2.1 |
| 2003 | 1.808 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
