---
title: " (TRMT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>TRMT</td></tr>
    <tr><td>Web</td><td><a href="https://www.trmtreit.com">www.trmtreit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.78 |
| 2019 | 0.77 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
