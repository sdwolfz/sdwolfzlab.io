---
title: "Basic Makefile CI steps"
date: 2020-11-14T11:59:51Z
type: posts
slug: basic-makefile-ci-steps
tags: ["make", "linux", "shell", "productivity", "development"]
---

Create general `ci` and `lint` goals for running things with and without docker,
and specific `ci/*` and `lint/*` goals for individual checks:

```makefile
#-------------------------------------------------------------------------------
# CI
#-------------------------------------------------------------------------------

# Absolute path to the directory of this Makefile.
HERE = $(realpath $(dir $(realpath $(firstword $(MAKEFILE_LIST)))))

# Run a docker container with the current directory mounted.
DOCKER_HERE := docker run --rm -it -u `id -u`:`id -g` -v "$(PWD)":/here -w /here

.PHONY: ci
ci: ci/editorconfig ci/hadolint ci/yamllint

.PHONY: ci/editorconfig
ci/editorconfig:
	@$(DOCKER_HERE) sdwolfz/editorconfig-checker:latest make lint/editorconfig

.PHONY: ci/hadolint
ci/hadolint:
	@$(DOCKER_HERE) sdwolfz/hadolint:latest make lint/hadolint

.PHONY: ci/yamllint
ci/yamllint:
	@$(DOCKER_HERE) sdwolfz/yamllint:latest make lint/yamllint

#-------------------------------------------------------------------------------
# Lint

.PHONY: lint
lint: lint/editorconfig lint/hadolint lint/yamllint

.PHONY: lint/editorconfig
lint/editorconfig:
	@editorconfig-checker $$(git ls-files)

.PHONY: lint/hadolint
lint/hadolint:
	@hadolint $$(git ls-files | grep Dockerfile)

.PHONY: lint/yamllint
lint/yamllint:
	@yamllint $$(git ls-files | grep -E \.ya?ml$$)
```
