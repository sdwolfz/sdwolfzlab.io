---
title: "SHERWIN-WILLIAMS CO (SHW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SHERWIN-WILLIAMS CO</td></tr>
    <tr><td>Symbol</td><td>SHW</td></tr>
    <tr><td>Web</td><td><a href="https://www.sherwin-williams.com">www.sherwin-williams.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.1 |
| 2020 | 5.36 |
| 2019 | 4.52 |
| 2018 | 3.44 |
| 2017 | 3.4 |
| 2016 | 3.36 |
| 2015 | 2.68 |
| 2014 | 2.2 |
| 2013 | 1.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
