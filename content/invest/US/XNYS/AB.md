---
title: " (AB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>AB</td></tr>
    <tr><td>Web</td><td><a href="https://www.alliancebernstein.com">www.alliancebernstein.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.78 |
| 2020 | 2.79 |
| 2019 | 2.32 |
| 2018 | 2.88 |
| 2017 | 2.13 |
| 2016 | 1.75 |
| 2015 | 1.93 |
| 2014 | 1.89 |
| 2013 | 0.81 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
