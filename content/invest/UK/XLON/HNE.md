---
title: "Hend.euro. (HNE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Hend.euro.</td></tr>
    <tr><td>Symbol</td><td>HNE</td></tr>
    <tr><td>Web</td><td><a href="https://www.hendersoneurotrust.com">www.hendersoneurotrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 17.0 |
| 2019 | 23.0 |
| 2018 | 30.5 |
| 2017 | 25.0 |
| 2016 | 20.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
