---
title: "SHENANDOAH TELECOMUNICATIONS (SHEN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SHENANDOAH TELECOMUNICATIONS</td></tr>
    <tr><td>Symbol</td><td>SHEN</td></tr>
    <tr><td>Web</td><td><a href="https://www.shentel.com">www.shentel.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.34 |
| 2019 | 0.29 |
| 2018 | 0.27 |
| 2017 | 0.26 |
| 2016 | 0.25 |
| 2015 | 0.48 |
| 2014 | 0.47 |
| 2013 | 0.36 |
| 2012 | 0.33 |
| 2011 | 0.33 |
| 2010 | 0.33 |
| 2009 | 0.32 |
| 2008 | 0.3 |
| 2007 | 0.27 |
| 2006 | 0.75 |
| 2005 | 0.46 |
| 2004 | 0.43 |
| 2003 | 0.78 |
| 2002 | 0.74 |
| 2001 | 0.7 |
| 2000 | 0.66 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
