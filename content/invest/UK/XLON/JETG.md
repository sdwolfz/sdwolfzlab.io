---
title: "Jpmorgan G (JETG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Jpmorgan G</td></tr>
    <tr><td>Symbol</td><td>JETG</td></tr>
    <tr><td>Web</td><td><a href="https://www.jpmfcontinentaleurope.com">www.jpmfcontinentaleurope.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 15.55 |
| 2019 | 8.6 |
| 2018 | 12.65 |
| 2017 | 11.85 |
| 2016 | 10.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
