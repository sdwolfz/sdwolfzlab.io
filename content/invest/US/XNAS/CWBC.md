---
title: "COMMUNITY WEST BANKSHARES (CWBC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COMMUNITY WEST BANKSHARES</td></tr>
    <tr><td>Symbol</td><td>CWBC</td></tr>
    <tr><td>Web</td><td><a href="https://www.communitywest.com">www.communitywest.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.13 |
| 2020 | 0.195 |
| 2019 | 0.215 |
| 2018 | 0.19 |
| 2017 | 0.155 |
| 2016 | 0.135 |
| 2015 | 0.11 |
| 2014 | 0.04 |
| 2008 | 0.12 |
| 2007 | 0.24 |
| 2006 | 0.23 |
| 2005 | 0.19 |
| 2004 | 0.12 |
| 2000 | 0.04 |
| 1999 | 0.12 |
| 1998 | 0.04 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
