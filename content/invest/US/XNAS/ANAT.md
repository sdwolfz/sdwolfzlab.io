---
title: "AMERICAN NATIONAL GROUP INC (ANAT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AMERICAN NATIONAL GROUP INC</td></tr>
    <tr><td>Symbol</td><td>ANAT</td></tr>
    <tr><td>Web</td><td><a href="https://www.americannational.com">www.americannational.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.64 |
| 2020 | 3.28 |
| 2019 | 3.28 |
| 2018 | 3.28 |
| 2017 | 3.28 |
| 2016 | 3.26 |
| 2015 | 3.14 |
| 2014 | 3.08 |
| 2013 | 3.08 |
| 2012 | 3.08 |
| 2011 | 3.08 |
| 2010 | 3.08 |
| 2009 | 3.08 |
| 2008 | 3.08 |
| 2007 | 3.05 |
| 2006 | 3.01 |
| 2005 | 2.97 |
| 2004 | 2.96 |
| 2003 | 2.96 |
| 2002 | 2.96 |
| 2001 | 2.93 |
| 2000 | 2.86 |
| 1999 | 2.78 |
| 1998 | 2.7 |
| 1997 | 2.62 |
| 1996 | 2.54 |
| 1995 | 2.4 |
| 1994 | 2.24 |
| 1993 | 2.08 |
| 1992 | 1.92 |
| 1991 | 1.76 |
| 1990 | 1.63 |
| 1989 | 1.51 |
| 1988 | 0.37 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
