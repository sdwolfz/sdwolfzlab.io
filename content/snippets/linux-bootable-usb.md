---
title: "Linux Bootable USB"
date: 2022-11-17T09:00:46Z
type: posts
slug: linux-bootable-usb
tags: ["linux", "USB"]
---

## Usage

```sh
sudo dd                                         \
  bs=4M status=progress oflag=direct conv=fsync \
  if=/path/to/linux.iso                         \
  of=/dev/sda
```

## Details

* `bs`: Buffer size
* `status=progress`: shows `3749707776 bytes (3.7 GB, 3.5 GiB) copied, 913 s, 4.1 MB/s`
* `oflag=direct`: actually write to USB instead of cache
* `conv=fsync`: actually write to USB instead of cache for real this time
* `if`: Input File
* `of`: USB drive

You find the USB drive using the `lsblk` command.
