---
title: "EVEREST RE GROUP (RE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>EVEREST RE GROUP</td></tr>
    <tr><td>Symbol</td><td>RE</td></tr>
    <tr><td>Web</td><td><a href="https://www.everestre.com">www.everestre.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.55 |
| 2020 | 6.2 |
| 2019 | 5.75 |
| 2018 | 5.3 |
| 2017 | 5.05 |
| 2016 | 4.7 |
| 2015 | 4.0 |
| 2014 | 3.2 |
| 2013 | 1.23 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
