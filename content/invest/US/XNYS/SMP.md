---
title: "STANDARD MOTOR PRODUCTS INC (SMP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>STANDARD MOTOR PRODUCTS INC</td></tr>
    <tr><td>Symbol</td><td>SMP</td></tr>
    <tr><td>Web</td><td><a href="https://www.smpcorp.com">www.smpcorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.5 |
| 2020 | 0.5 |
| 2019 | 0.92 |
| 2018 | 0.84 |
| 2017 | 0.76 |
| 2016 | 0.68 |
| 2015 | 0.6 |
| 2014 | 0.52 |
| 2013 | 0.22 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
