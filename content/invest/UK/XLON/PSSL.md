---
title: "POLLEN STREET SECURED LENDING PLC ORD GBP0.01 (PSSL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>POLLEN STREET SECURED LENDING PLC ORD GBP0.01</td></tr>
    <tr><td>Symbol</td><td>PSSL</td></tr>
    <tr><td>Web</td><td><a href="http://www.pollenstreetsecuredlending.com">www.pollenstreetsecuredlending.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 48.0 |
| 2018 | 48.0 |
| 2017 | 47.0 |
| 2016 | 47.2 |
| 2015 | 58.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
