---
title: "Jtc Plc (JTC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Jtc Plc</td></tr>
    <tr><td>Symbol</td><td>JTC</td></tr>
    <tr><td>Web</td><td><a href="https://www.jtcgroup.com">www.jtcgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 3.7 |
| 2018 | 2.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
