---
title: "WERNER ENTERPRISES INC (WERN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>WERNER ENTERPRISES INC</td></tr>
    <tr><td>Symbol</td><td>WERN</td></tr>
    <tr><td>Web</td><td><a href="https://www.werner.com">www.werner.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.45 |
| 2019 | 4.02 |
| 2018 | 0.34 |
| 2017 | 0.27 |
| 2016 | 0.24 |
| 2015 | 0.22 |
| 2014 | 0.25 |
| 2013 | 0.2 |
| 2012 | 1.7 |
| 2011 | 0.7 |
| 2010 | 1.8 |
| 2009 | 1.45 |
| 2008 | 2.3 |
| 2007 | 0.19 |
| 2006 | 0.17 |
| 2005 | 0.15 |
| 2004 | 0.12 |
| 2003 | 0.095 |
| 2002 | 0.085 |
| 2001 | 0.1 |
| 2000 | 0.1 |
| 1999 | 0.1 |
| 1998 | 0.098 |
| 1997 | 0.1 |
| 1996 | 0.12 |
| 1995 | 0.115 |
| 1994 | 0.105 |
| 1993 | 0.08 |
| 1992 | 0.14 |
| 1991 | 0.13 |
| 1990 | 0.12 |
| 1989 | 0.07 |
| 1988 | 0.02 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
