---
title: "GILDAN ACTIVEWEAR INC (GIL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GILDAN ACTIVEWEAR INC</td></tr>
    <tr><td>Symbol</td><td>GIL</td></tr>
    <tr><td>Web</td><td><a href="https://www.gildan.com">www.gildan.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.154 |
| 2019 | 0.536 |
| 2018 | 0.448 |
| 2017 | 0.376 |
| 2016 | 0.312 |
| 2015 | 0.26 |
| 2014 | 0.454 |
| 2013 | 0.198 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
