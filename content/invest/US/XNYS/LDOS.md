---
title: "LEIDOS HOLDINGS INC (LDOS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>LEIDOS HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>LDOS</td></tr>
    <tr><td>Web</td><td><a href="https://www.leidos.com">www.leidos.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.68 |
| 2020 | 1.36 |
| 2019 | 1.32 |
| 2018 | 1.28 |
| 2017 | 1.28 |
| 2016 | 1.28 |
| 2015 | 1.92 |
| 2014 | 1.28 |
| 2013 | 0.32 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
