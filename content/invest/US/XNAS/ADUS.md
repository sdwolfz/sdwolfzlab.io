---
title: "ADDUS HOMECARE CORP (ADUS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ADDUS HOMECARE CORP</td></tr>
    <tr><td>Symbol</td><td>ADUS</td></tr>
    <tr><td>Web</td><td><a href="https://www.addus.com">www.addus.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
