---
title: " (BRT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BRT</td></tr>
    <tr><td>Web</td><td><a href="https://www.brtapartments.com">www.brtapartments.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.22 |
| 2020 | 0.88 |
| 2019 | 0.84 |
| 2018 | 0.8 |
| 2017 | 0.36 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
