---
title: "COMPX INTERNATIONAL INC (CIX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COMPX INTERNATIONAL INC</td></tr>
    <tr><td>Symbol</td><td>CIX</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.4 |
| 2019 | 0.28 |
| 2018 | 0.2 |
| 2017 | 0.2 |
| 2016 | 0.2 |
| 2015 | 0.2 |
| 2014 | 0.2 |
| 2013 | 0.275 |
| 2012 | 0.5 |
| 2011 | 0.5 |
| 2010 | 0.5 |
| 2009 | 0.5 |
| 2008 | 0.5 |
| 2007 | 0.5 |
| 2006 | 0.5 |
| 2005 | 0.5 |
| 2004 | 0.125 |
| 2003 | 0.125 |
| 2002 | 0.5 |
| 2001 | 0.375 |
| 2000 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
