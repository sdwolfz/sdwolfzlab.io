---
title: "Avast (AVST)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Avast</td></tr>
    <tr><td>Symbol</td><td>AVST</td></tr>
    <tr><td>Web</td><td><a href="https://www.avast.com">www.avast.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 14.7 |
| 2018 | 8.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
