---
title: "LIFE IS HARD S.A. (LIH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>LIFE IS HARD S.A.</td></tr>
    <tr><td>Symbol</td><td>LIH</td></tr>
    <tr><td>Web</td><td><a href="https://www.lifeishard.ro">www.lifeishard.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.0987 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
