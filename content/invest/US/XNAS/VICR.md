---
title: "VICOR CORP (VICR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>VICOR CORP</td></tr>
    <tr><td>Symbol</td><td>VICR</td></tr>
    <tr><td>Web</td><td><a href="https://www.vicorpower.com">www.vicorpower.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2011 | 0.15 |
| 2010 | 0.3 |
| 2008 | 0.3 |
| 2007 | 0.3 |
| 2006 | 0.27 |
| 2005 | 0.12 |
| 2004 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
