---
title: "HARLEY DAVIDSON (HOG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HARLEY DAVIDSON</td></tr>
    <tr><td>Symbol</td><td>HOG</td></tr>
    <tr><td>Web</td><td><a href="https://www.harley-davidson.com">www.harley-davidson.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.15 |
| 2020 | 0.44 |
| 2019 | 1.5 |
| 2018 | 1.48 |
| 2017 | 1.46 |
| 2016 | 1.4 |
| 2015 | 1.24 |
| 2014 | 1.1 |
| 2013 | 0.63 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
