---
title: "MAT SA CRAIOVA (MTCR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>MAT SA CRAIOVA</td></tr>
    <tr><td>Symbol</td><td>MTCR</td></tr>
    <tr><td>Web</td><td><a href="https://www.matcraiova.ro">www.matcraiova.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2007 | 1.02 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
