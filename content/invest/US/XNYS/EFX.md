---
title: "EQUIFAX INC (EFX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>EQUIFAX INC</td></tr>
    <tr><td>Symbol</td><td>EFX</td></tr>
    <tr><td>Web</td><td><a href="https://www.equifax.com">www.equifax.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.39 |
| 2020 | 1.56 |
| 2019 | 1.56 |
| 2018 | 1.56 |
| 2017 | 1.56 |
| 2016 | 1.32 |
| 2015 | 1.16 |
| 2014 | 1.0 |
| 2013 | 0.66 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
