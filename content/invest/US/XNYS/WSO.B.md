---
title: "WATSCO INC (WSO.B)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>WATSCO INC</td></tr>
    <tr><td>Symbol</td><td>WSO.B</td></tr>
    <tr><td>Web</td><td><a href="https://www.watsco.com">www.watsco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 3.725 |
| 2020 | 6.925 |
| 2019 | 6.4 |
| 2018 | 5.6 |
| 2017 | 4.6 |
| 2016 | 3.6 |
| 2015 | 2.8 |
| 2014 | 2.0 |
| 2013 | 0.65 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
