---
title: " (KIM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>KIM</td></tr>
    <tr><td>Web</td><td><a href="https://www.kimcorealty.com">www.kimcorealty.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.34 |
| 2020 | 0.54 |
| 2019 | 1.12 |
| 2018 | 1.12 |
| 2017 | 1.09 |
| 2016 | 1.035 |
| 2015 | 0.975 |
| 2014 | 0.915 |
| 2013 | 0.435 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
