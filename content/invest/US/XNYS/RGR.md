---
title: "STURM RUGER & CO INC (RGR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>STURM RUGER & CO INC</td></tr>
    <tr><td>Symbol</td><td>RGR</td></tr>
    <tr><td>Web</td><td><a href="https://www.ruger.com">www.ruger.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.57 |
| 2020 | 6.51 |
| 2019 | 0.82 |
| 2018 | 1.1 |
| 2017 | 1.36 |
| 2016 | 1.73 |
| 2015 | 1.1 |
| 2014 | 1.62 |
| 2013 | 1.23 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
