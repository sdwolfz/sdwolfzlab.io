---
title: "COMCEREAL SA BOTOSANI (CBOT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>COMCEREAL SA BOTOSANI</td></tr>
    <tr><td>Symbol</td><td>CBOT</td></tr>
    <tr><td>Web</td><td><a href="https://www.comcerealbt.ro">www.comcerealbt.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
