---
title: "NINE ENERGY SERVICE INC (NINE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NINE ENERGY SERVICE INC</td></tr>
    <tr><td>Symbol</td><td>NINE</td></tr>
    <tr><td>Web</td><td><a href="https://www.nineenergyservice.com">www.nineenergyservice.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
