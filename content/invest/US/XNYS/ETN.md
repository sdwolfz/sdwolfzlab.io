---
title: "EATON CORPORATION PLC (ETN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>EATON CORPORATION PLC</td></tr>
    <tr><td>Symbol</td><td>ETN</td></tr>
    <tr><td>Web</td><td><a href="https://www.eaton.com">www.eaton.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.52 |
| 2020 | 3.65 |
| 2019 | 2.84 |
| 2018 | 2.64 |
| 2017 | 2.4 |
| 2016 | 2.26 |
| 2015 | 2.2 |
| 2014 | 1.96 |
| 2013 | 0.84 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
