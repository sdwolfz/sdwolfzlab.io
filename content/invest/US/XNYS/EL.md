---
title: "ESTEE LAUDER COMPANIES INC (EL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ESTEE LAUDER COMPANIES INC</td></tr>
    <tr><td>Symbol</td><td>EL</td></tr>
    <tr><td>Web</td><td><a href="https://www.elcompanies.com">www.elcompanies.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.06 |
| 2020 | 1.49 |
| 2019 | 1.77 |
| 2018 | 1.57 |
| 2017 | 1.4 |
| 2016 | 1.24 |
| 2015 | 1.02 |
| 2014 | 0.84 |
| 2013 | 0.38 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
