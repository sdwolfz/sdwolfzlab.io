---
title: "STAR EQUITY HOLDINGS INC (STRR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>STAR EQUITY HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>STRR</td></tr>
    <tr><td>Web</td><td><a href="https://www.starequity.com">www.starequity.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.165 |
| 2017 | 0.21 |
| 2016 | 0.2 |
| 2015 | 0.2 |
| 2014 | 0.2 |
| 2013 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
