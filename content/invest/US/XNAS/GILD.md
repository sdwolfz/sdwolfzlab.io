---
title: "GILEAD SCIENCES INC (GILD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GILEAD SCIENCES INC</td></tr>
    <tr><td>Symbol</td><td>GILD</td></tr>
    <tr><td>Web</td><td><a href="https://www.gilead.com">www.gilead.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.42 |
| 2020 | 2.72 |
| 2019 | 2.52 |
| 2018 | 2.28 |
| 2017 | 2.08 |
| 2016 | 1.84 |
| 2015 | 1.29 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
