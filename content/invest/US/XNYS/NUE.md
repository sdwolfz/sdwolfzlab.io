---
title: "NUCOR CORP (NUE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NUCOR CORP</td></tr>
    <tr><td>Symbol</td><td>NUE</td></tr>
    <tr><td>Web</td><td><a href="https://www.nucor.com">www.nucor.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.405 |
| 2020 | 1.614 |
| 2019 | 1.603 |
| 2018 | 1.54 |
| 2017 | 1.511 |
| 2016 | 1.502 |
| 2015 | 1.494 |
| 2014 | 1.483 |
| 2013 | 1.106 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
