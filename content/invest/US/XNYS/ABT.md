---
title: "ABBOTT LABORATORIES (ABT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ABBOTT LABORATORIES</td></tr>
    <tr><td>Symbol</td><td>ABT</td></tr>
    <tr><td>Web</td><td><a href="https://www.abbott.com">www.abbott.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.9 |
| 2020 | 1.44 |
| 2019 | 1.28 |
| 2018 | 1.12 |
| 2017 | 1.06 |
| 2016 | 1.04 |
| 2015 | 0.96 |
| 2014 | 0.88 |
| 2013 | 0.28 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
