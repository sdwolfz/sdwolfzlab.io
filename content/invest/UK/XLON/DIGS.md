---
title: "Gcp Student Liv (DIGS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Gcp Student Liv</td></tr>
    <tr><td>Symbol</td><td>DIGS</td></tr>
    <tr><td>Web</td><td><a href="https://www.graviscapital.com">www.graviscapital.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.15 |
| 2019 | 6.15 |
| 2018 | 5.95 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
