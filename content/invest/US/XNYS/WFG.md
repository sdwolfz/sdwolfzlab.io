---
title: "WEST FRASER TIMBER CO (WFG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>WEST FRASER TIMBER CO</td></tr>
    <tr><td>Symbol</td><td>WFG</td></tr>
    <tr><td>Web</td><td><a href="https://www.westfraser.com">www.westfraser.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
