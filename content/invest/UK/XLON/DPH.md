---
title: "Dechra (DPH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Dechra</td></tr>
    <tr><td>Symbol</td><td>DPH</td></tr>
    <tr><td>Web</td><td><a href="https://www.dechra.com">www.dechra.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 34.29 |
| 2019 | 31.6 |
| 2018 | 25.5 |
| 2017 | 21.44 |
| 2016 | 18.46 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
