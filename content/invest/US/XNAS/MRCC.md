---
title: "MONROE (MRCC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MONROE</td></tr>
    <tr><td>Symbol</td><td>MRCC</td></tr>
    <tr><td>Web</td><td><a href="https://www.monroebdc.com">www.monroebdc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 1.1 |
| 2019 | 1.4 |
| 2018 | 1.4 |
| 2017 | 1.4 |
| 2016 | 1.4 |
| 2015 | 1.4 |
| 2014 | 1.36 |
| 2013 | 1.36 |
| 2012 | 0.34 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
