---
title: "LIVE OAK BANCSHARES INC (LOB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LIVE OAK BANCSHARES INC</td></tr>
    <tr><td>Symbol</td><td>LOB</td></tr>
    <tr><td>Web</td><td><a href="https://www.liveoakbank.com">www.liveoakbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.03 |
| 2020 | 0.12 |
| 2019 | 0.12 |
| 2018 | 0.12 |
| 2017 | 0.1 |
| 2016 | 0.07 |
| 2015 | 0.02 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
