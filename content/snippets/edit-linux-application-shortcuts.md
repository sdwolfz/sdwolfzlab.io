---
title: "Edit linux application shortcuts"
date: 2021-10-31T12:38:12Z
type: posts
slug: edit-linux-application-shortcuts
tags: ["linux", "desktop"]
---

Locations:

```sh
# System
/usr/share/applications/
/usr/local/share/applications/

# Flatpak
/var/lib/flatpak/exports/share/applications/

# Snap
/var/lib/snapd/desktop/applications/

# Local
~/.local/share/applications/
```

Search for your application in those directories and edit the files, using
`root` privileges where necessary.

```sh
sudo vim /usr/share/applications/org.gnome.Terminal.desktop
```

Now edit what you need, like the application command to add extra flags:

```conf
TryExec=gnome-terminal
Exec=gnome-terminal --full-screen
Icon=org.gnome.Terminal
Type=Application
```

> Tip: leave `TryExec` alone!

You can also add environment variables using `env`:

```con
Exec=env LANG=en_GB.UTF-8 gnome-terminal --full-screen
```

Copy it to `~/.local/share/applications` if you don't want to edit the original.
