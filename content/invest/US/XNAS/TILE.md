---
title: "INTERFACE INC (TILE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>INTERFACE INC</td></tr>
    <tr><td>Symbol</td><td>TILE</td></tr>
    <tr><td>Web</td><td><a href="https://www.interface.com">www.interface.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.01 |
| 2020 | 0.095 |
| 2019 | 0.26 |
| 2018 | 0.26 |
| 2017 | 0.25 |
| 2016 | 0.22 |
| 2015 | 0.18 |
| 2014 | 0.14 |
| 2013 | 0.11 |
| 2012 | 0.09 |
| 2011 | 0.08 |
| 2010 | 0.043 |
| 2009 | 0.01 |
| 2008 | 0.12 |
| 2007 | 0.08 |
| 2002 | 0.045 |
| 2001 | 0.15 |
| 2000 | 0.18 |
| 1999 | 0.18 |
| 1998 | 0.24 |
| 1997 | 0.27 |
| 1996 | 0.245 |
| 1995 | 0.18 |
| 1994 | 0.24 |
| 1993 | 0.24 |
| 1992 | 0.24 |
| 1991 | 0.18 |
| 1990 | 0.18 |
| 1989 | 0.21 |
| 1988 | 0.04 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
