---
title: "PACKAGING CORP OF AMERICA (PKG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PACKAGING CORP OF AMERICA</td></tr>
    <tr><td>Symbol</td><td>PKG</td></tr>
    <tr><td>Web</td><td><a href="https://www.packagingcorp.com">www.packagingcorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.0 |
| 2020 | 3.37 |
| 2019 | 3.16 |
| 2018 | 3.0 |
| 2017 | 2.52 |
| 2016 | 2.36 |
| 2015 | 2.2 |
| 2014 | 1.6 |
| 2013 | 1.113 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
