---
title: "CHOICE HOTELS INTERNATIONAL INC (CHH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CHOICE HOTELS INTERNATIONAL INC</td></tr>
    <tr><td>Symbol</td><td>CHH</td></tr>
    <tr><td>Web</td><td><a href="https://www.choicehotels.com">www.choicehotels.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.225 |
| 2019 | 0.87 |
| 2018 | 0.86 |
| 2017 | 0.86 |
| 2016 | 0.83 |
| 2015 | 0.79 |
| 2014 | 0.75 |
| 2013 | 0.555 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
