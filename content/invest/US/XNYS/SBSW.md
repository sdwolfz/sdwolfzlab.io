---
title: " (SBSW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SBSW</td></tr>
    <tr><td>Web</td><td><a href="https://www.sibanyestillwater.com">www.sibanyestillwater.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.689 |
| 2020 | 0.095 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
