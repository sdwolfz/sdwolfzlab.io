---
title: "COMPUTER TASK GROUP INC (CTG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COMPUTER TASK GROUP INC</td></tr>
    <tr><td>Symbol</td><td>CTG</td></tr>
    <tr><td>Web</td><td><a href="https://www.ctg.com">www.ctg.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.18 |
| 2015 | 0.24 |
| 2014 | 0.24 |
| 2013 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
