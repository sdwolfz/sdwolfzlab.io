---
title: "BLUEGREEN VACATIONS CORP (BXG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BLUEGREEN VACATIONS CORP</td></tr>
    <tr><td>Symbol</td><td>BXG</td></tr>
    <tr><td>Web</td><td><a href="http://www.bluegreenvacations.com">www.bluegreenvacations.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.32 |
| 2019 | 0.64 |
| 2018 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
