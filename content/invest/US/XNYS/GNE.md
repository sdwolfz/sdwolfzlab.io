---
title: "GENIE ENERGY LTD (GNE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GENIE ENERGY LTD</td></tr>
    <tr><td>Symbol</td><td>GNE</td></tr>
    <tr><td>Web</td><td><a href="https://www.genie.com">www.genie.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.33 |
| 2019 | 0.3 |
| 2018 | 0.3 |
| 2017 | 0.3 |
| 2016 | 0.24 |
| 2015 | 0.06 |
| 2014 | 0.06 |
| 2012 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
