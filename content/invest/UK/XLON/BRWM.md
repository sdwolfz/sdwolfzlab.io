---
title: "Blackrock Wld (BRWM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Blackrock Wld</td></tr>
    <tr><td>Symbol</td><td>BRWM</td></tr>
    <tr><td>Web</td><td><a href="https://www.blackrock.com">www.blackrock.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 20.3 |
| 2019 | 22.0 |
| 2018 | 18.0 |
| 2017 | 15.6 |
| 2016 | 13.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
