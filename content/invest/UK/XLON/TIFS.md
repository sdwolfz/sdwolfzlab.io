---
title: "TI Fluid Systems (TIFS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>TI Fluid Systems</td></tr>
    <tr><td>Symbol</td><td>TIFS</td></tr>
    <tr><td>Web</td><td><a href="https://www.tifluidsystems.com">www.tifluidsystems.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 1.31 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
