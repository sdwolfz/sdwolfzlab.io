---
title: " (EPRT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>EPRT</td></tr>
    <tr><td>Web</td><td><a href="https://essentialproperties.com">essentialproperties.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.24 |
| 2020 | 0.93 |
| 2019 | 0.88 |
| 2018 | 0.434 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
