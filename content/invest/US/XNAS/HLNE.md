---
title: "HAMILTON LANE INC (HLNE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HAMILTON LANE INC</td></tr>
    <tr><td>Symbol</td><td>HLNE</td></tr>
    <tr><td>Web</td><td><a href="https://www.hamiltonlane.com">www.hamiltonlane.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.313 |
| 2020 | 1.213 |
| 2019 | 1.038 |
| 2018 | 0.813 |
| 2017 | 0.525 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
