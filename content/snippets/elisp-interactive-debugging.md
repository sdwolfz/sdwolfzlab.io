---
title: "Emacs Lisp interactive debugging"
date: 2021-05-10T11:05:44Z
type: posts
slug: elisp-interactive-debugging
tags: ["debugging", "emacs", "elisp"]
---

1. Navigate to your function.
2. Instrument it with `C-u M-x eval-defun`, or `C-u , e f` in Spacemacs.
3. Execute that function.
4. Debug with these shortcuts:

    | Key | Action     |
    |-----|------------|
    | `s` | Next sexp. |
    | `i` | Step in.   |
    | `o` | Step out.  |
    | `d` | Backtrace. |
    | `Q` | Quit.      |

5. Once you're done, re-evaluate all functions (without `C-u`), or simply
   restart.
