---
title: "GLEN BURNIE BANCORP (GLBZ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GLEN BURNIE BANCORP</td></tr>
    <tr><td>Symbol</td><td>GLBZ</td></tr>
    <tr><td>Web</td><td><a href="https://www.thebankofglenburnie.com">www.thebankofglenburnie.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.4 |
| 2019 | 0.4 |
| 2018 | 0.4 |
| 2017 | 0.4 |
| 2016 | 0.4 |
| 2015 | 0.27 |
| 2014 | 0.4 |
| 2013 | 0.4 |
| 2012 | 0.4 |
| 2011 | 0.4 |
| 2010 | 0.4 |
| 2009 | 0.4 |
| 2008 | 0.45 |
| 2007 | 0.54 |
| 2006 | 0.54 |
| 2005 | 0.59 |
| 2004 | 0.52 |
| 2003 | 0.54 |
| 2002 | 0.5 |
| 2001 | 0.55 |
| 2000 | 0.675 |
| 1999 | 0.35 |
| 1998 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
