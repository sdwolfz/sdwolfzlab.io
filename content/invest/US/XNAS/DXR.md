---
title: "DAXOR CORP (DXR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>DAXOR CORP</td></tr>
    <tr><td>Symbol</td><td>DXR</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.03 |
| 2016 | 0.03 |
| 2015 | 0.04 |
| 2014 | 0.03 |
| 2013 | 0.05 |
| 2012 | 0.2 |
| 2011 | 0.25 |
| 2010 | 1.0 |
| 2009 | 1.35 |
| 2008 | 1.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
