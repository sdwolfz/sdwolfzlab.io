---
title: "RPM INTERNATIONAL INC (RPM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>RPM INTERNATIONAL INC</td></tr>
    <tr><td>Symbol</td><td>RPM</td></tr>
    <tr><td>Web</td><td><a href="https://www.rpminc.com">www.rpminc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.76 |
| 2020 | 1.46 |
| 2019 | 1.41 |
| 2018 | 1.31 |
| 2017 | 1.22 |
| 2016 | 1.125 |
| 2015 | 1.055 |
| 2014 | 0.98 |
| 2013 | 0.465 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
