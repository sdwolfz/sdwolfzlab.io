---
title: " (PEI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>PEI</td></tr>
    <tr><td>Web</td><td><a href="https://www.preit.com">www.preit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.23 |
| 2019 | 0.84 |
| 2018 | 0.84 |
| 2017 | 0.84 |
| 2016 | 0.84 |
| 2015 | 0.84 |
| 2014 | 0.8 |
| 2013 | 0.38 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
