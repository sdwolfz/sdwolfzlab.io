---
title: "Vesuvius (VSVS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Vesuvius</td></tr>
    <tr><td>Symbol</td><td>VSVS</td></tr>
    <tr><td>Web</td><td><a href="https://www.vesuvius.com">www.vesuvius.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 20.5 |
| 2018 | 19.8 |
| 2017 | 18.0 |
| 2016 | 16.55 |
| 2015 | 16.28 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
