---
title: "JPMorgan Japanese (JFJ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>JPMorgan Japanese</td></tr>
    <tr><td>Symbol</td><td>JFJ</td></tr>
    <tr><td>Web</td><td><a href="https://www.jpmfjapanese.com">www.jpmfjapanese.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 5.1 |
| 2019 | 5.0 |
| 2018 | 5.0 |
| 2017 | 5.0 |
| 2016 | 3.65 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
