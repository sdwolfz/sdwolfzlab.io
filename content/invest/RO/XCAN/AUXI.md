---
title: "ATM - CONSTRUCT SA PLOIESTI (AUXI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>ATM - CONSTRUCT SA PLOIESTI</td></tr>
    <tr><td>Symbol</td><td>AUXI</td></tr>
    <tr><td>Web</td><td><a href="https://www.atmconstruct.com">www.atmconstruct.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 13.37232 |
| 2019 | 4.32985 |
| 2018 | 3.97047 |
| 2017 | 3.39727 |
| 2014 | 3.09259 |
| 2013 | 3.0652 |
| 2008 | 8.8541 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
