---
title: "DOUGLAS DYNAMICS INC (PLOW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DOUGLAS DYNAMICS INC</td></tr>
    <tr><td>Symbol</td><td>PLOW</td></tr>
    <tr><td>Web</td><td><a href="https://www.douglasdynamics.com">www.douglasdynamics.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.285 |
| 2020 | 1.12 |
| 2019 | 1.092 |
| 2018 | 1.06 |
| 2017 | 0.96 |
| 2016 | 0.94 |
| 2015 | 0.888 |
| 2014 | 0.872 |
| 2013 | 0.627 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
