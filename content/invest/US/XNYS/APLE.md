---
title: " (APLE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>APLE</td></tr>
    <tr><td>Web</td><td><a href="https://www.applehospitalityreit.com">www.applehospitalityreit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.01 |
| 2020 | 0.3 |
| 2019 | 0.9 |
| 2018 | 1.0 |
| 2017 | 1.1 |
| 2016 | 1.2 |
| 2015 | 0.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
