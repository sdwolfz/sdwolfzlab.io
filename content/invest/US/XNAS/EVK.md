---
title: "EVER-GLORY INTERNATIONAL GROUP INC (EVK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>EVER-GLORY INTERNATIONAL GROUP INC</td></tr>
    <tr><td>Symbol</td><td>EVK</td></tr>
    <tr><td>Web</td><td><a href="https://www.everglorygroup.com">www.everglorygroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 1999 | 0.04 |
| 1997 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
