---
title: "INTERNATIONAL SEAWAYS INC (INSW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>INTERNATIONAL SEAWAYS INC</td></tr>
    <tr><td>Symbol</td><td>INSW</td></tr>
    <tr><td>Web</td><td><a href="https://www.intlseas.com">www.intlseas.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.06 |
| 2020 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
