---
title: "GENMARK DIAGNOSTICS INC (GNMK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GENMARK DIAGNOSTICS INC</td></tr>
    <tr><td>Symbol</td><td>GNMK</td></tr>
    <tr><td>Web</td><td><a href="http://www.genmarkdx.com">www.genmarkdx.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
