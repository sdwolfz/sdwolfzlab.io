---
title: "MURPHY USA INC (MUSA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MURPHY USA INC</td></tr>
    <tr><td>Symbol</td><td>MUSA</td></tr>
    <tr><td>Web</td><td><a href="https://www.murphyusa.com">www.murphyusa.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.5 |
| 2020 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
