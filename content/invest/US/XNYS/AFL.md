---
title: "AFLAC INC (AFL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AFLAC INC</td></tr>
    <tr><td>Symbol</td><td>AFL</td></tr>
    <tr><td>Web</td><td><a href="https://www.aflac.com">www.aflac.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.66 |
| 2020 | 1.12 |
| 2019 | 1.08 |
| 2018 | 1.04 |
| 2017 | 1.74 |
| 2016 | 1.66 |
| 2015 | 1.58 |
| 2014 | 1.5 |
| 2013 | 0.72 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
