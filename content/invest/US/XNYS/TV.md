---
title: " (TV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>TV</td></tr>
    <tr><td>Web</td><td><a href="https://www.televisa.com">www.televisa.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.079 |
| 2018 | 0.078 |
| 2017 | 0.084 |
| 2016 | 0.084 |
| 2015 | 0.103 |
| 2013 | 0.251 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
