---
title: "UNITED FIRE GROUP INC (UFCS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>UNITED FIRE GROUP INC</td></tr>
    <tr><td>Symbol</td><td>UFCS</td></tr>
    <tr><td>Web</td><td><a href="https://www.ufginsurance.com">www.ufginsurance.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.15 |
| 2020 | 1.14 |
| 2019 | 1.3 |
| 2018 | 4.21 |
| 2017 | 1.09 |
| 2016 | 0.97 |
| 2015 | 0.86 |
| 2014 | 0.78 |
| 2013 | 0.69 |
| 2012 | 0.6 |
| 2011 | 0.6 |
| 2010 | 0.6 |
| 2009 | 0.6 |
| 2008 | 0.6 |
| 2007 | 0.555 |
| 2006 | 0.495 |
| 2005 | 0.48 |
| 2004 | 0.6 |
| 2003 | 0.763 |
| 2002 | 0.728 |
| 2001 | 0.72 |
| 2000 | 0.71 |
| 1999 | 0.68 |
| 1998 | 0.67 |
| 1997 | 0.63 |
| 1996 | 0.6 |
| 1995 | 0.75 |
| 1994 | 1.01 |
| 1993 | 1.02 |
| 1992 | 1.205 |
| 1991 | 1.32 |
| 1990 | 1.23 |
| 1989 | 1.17 |
| 1988 | 0.27 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
