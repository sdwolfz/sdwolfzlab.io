---
title: "AMERICAN EXPRESS CO (AXP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AMERICAN EXPRESS CO</td></tr>
    <tr><td>Symbol</td><td>AXP</td></tr>
    <tr><td>Web</td><td><a href="https://www.americanexpress.com">www.americanexpress.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.86 |
| 2020 | 1.72 |
| 2019 | 1.6 |
| 2018 | 1.44 |
| 2017 | 1.31 |
| 2016 | 1.19 |
| 2015 | 1.1 |
| 2014 | 0.98 |
| 2013 | 0.46 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
