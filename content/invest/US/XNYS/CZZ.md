---
title: "COSAN LTD (CZZ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>COSAN LTD</td></tr>
    <tr><td>Symbol</td><td>CZZ</td></tr>
    <tr><td>Web</td><td><a href="http://ri.cosanlimited.com">ri.cosanlimited.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.135 |
| 2018 | 0.082 |
| 2017 | 0.076 |
| 2016 | 0.182 |
| 2015 | 0.112 |
| 2014 | 0.3 |
| 2013 | 0.304 |
| 2012 | 0.286 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
