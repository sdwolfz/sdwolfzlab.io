---
title: "Go-Ahead (GOG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Go-Ahead</td></tr>
    <tr><td>Symbol</td><td>GOG</td></tr>
    <tr><td>Web</td><td><a href="https://www.go-ahead.com">www.go-ahead.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 71.91 |
| 2019 | 102.08 |
| 2018 | 102.08 |
| 2017 | 102.08 |
| 2016 | 95.85 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
