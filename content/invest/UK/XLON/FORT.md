---
title: "Forterra (FORT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Forterra</td></tr>
    <tr><td>Symbol</td><td>FORT</td></tr>
    <tr><td>Web</td><td><a href="https://www.forterra.co.uk">www.forterra.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 11.5 |
| 2018 | 10.5 |
| 2017 | 9.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
