---
title: "BED BATH AND BEYOND (BBBY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BED BATH AND BEYOND</td></tr>
    <tr><td>Symbol</td><td>BBBY</td></tr>
    <tr><td>Web</td><td><a href="https://www.bedbathandbeyond.com">www.bedbathandbeyond.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.17 |
| 2019 | 0.67 |
| 2018 | 0.63 |
| 2017 | 0.575 |
| 2016 | 0.375 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
