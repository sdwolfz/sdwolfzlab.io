---
title: "COLUMBUS MCKINNON CORPORATION (CMCO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COLUMBUS MCKINNON CORPORATION</td></tr>
    <tr><td>Symbol</td><td>CMCO</td></tr>
    <tr><td>Web</td><td><a href="https://www.columbusmckinnon.com">www.columbusmckinnon.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 0.24 |
| 2019 | 0.23 |
| 2018 | 0.19 |
| 2017 | 0.16 |
| 2016 | 0.16 |
| 2015 | 0.16 |
| 2014 | 0.12 |
| 2001 | 0.21 |
| 2000 | 0.28 |
| 1999 | 0.28 |
| 1998 | 0.28 |
| 1997 | 0.28 |
| 1996 | 0.26 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
