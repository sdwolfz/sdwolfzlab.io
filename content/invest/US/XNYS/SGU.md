---
title: " (SGU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SGU</td></tr>
    <tr><td>Web</td><td><a href="https://www.stargrouplp.com">www.stargrouplp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.274 |
| 2020 | 0.521 |
| 2019 | 0.492 |
| 2018 | 0.461 |
| 2017 | 0.432 |
| 2016 | 0.401 |
| 2015 | 0.372 |
| 2014 | 0.344 |
| 2013 | 0.166 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
