---
title: " (BKEP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BKEP</td></tr>
    <tr><td>Web</td><td><a href="https://www.bkep.com">www.bkep.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.08 |
| 2020 | 0.16 |
| 2019 | 0.2 |
| 2018 | 0.45 |
| 2017 | 0.58 |
| 2016 | 0.58 |
| 2015 | 0.564 |
| 2014 | 0.524 |
| 2013 | 0.475 |
| 2012 | 0.443 |
| 2008 | 0.738 |
| 2007 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
