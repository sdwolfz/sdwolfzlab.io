---
title: "ITT INC (ITT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ITT INC</td></tr>
    <tr><td>Symbol</td><td>ITT</td></tr>
    <tr><td>Web</td><td><a href="https://www.itt.com">www.itt.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.22 |
| 2020 | 0.676 |
| 2019 | 0.588 |
| 2018 | 0.536 |
| 2017 | 0.512 |
| 2016 | 0.496 |
| 2015 | 0.472 |
| 2014 | 0.44 |
| 2013 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
