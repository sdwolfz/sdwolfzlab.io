---
title: "FABRICA DE SCULE RASNOV SA (FACY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>FABRICA DE SCULE RASNOV SA</td></tr>
    <tr><td>Symbol</td><td>FACY</td></tr>
    <tr><td>Web</td><td><a href="https://www.fsr.ro">www.fsr.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.18 |
| 2015 | 0.18 |
| 2014 | 0.177 |
| 2013 | 0.2976 |
| 2012 | 0.1769 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
