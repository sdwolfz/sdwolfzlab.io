---
title: " (VLYPP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>VLYPP</td></tr>
    <tr><td>Web</td><td><a href="https://www.valley.com">www.valley.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.391 |
| 2020 | 1.563 |
| 2019 | 1.563 |
| 2018 | 1.563 |
| 2017 | 1.563 |
| 2016 | 1.563 |
| 2015 | 0.829 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
