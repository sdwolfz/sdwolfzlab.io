---
title: "PINNACLE FINANCIAL PARTNERS (PNFP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PINNACLE FINANCIAL PARTNERS</td></tr>
    <tr><td>Symbol</td><td>PNFP</td></tr>
    <tr><td>Web</td><td><a href="https://www.pnfp.com">www.pnfp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.36 |
| 2020 | 0.64 |
| 2019 | 0.64 |
| 2018 | 0.58 |
| 2017 | 0.56 |
| 2016 | 0.56 |
| 2015 | 0.48 |
| 2014 | 0.32 |
| 2013 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
