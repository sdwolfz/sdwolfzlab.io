---
title: "VALMONT INDUSTRIES INC (VMI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>VALMONT INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>VMI</td></tr>
    <tr><td>Web</td><td><a href="https://www.valmont.com">www.valmont.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.5 |
| 2020 | 1.8 |
| 2019 | 1.5 |
| 2018 | 1.5 |
| 2017 | 1.5 |
| 2016 | 1.5 |
| 2015 | 1.5 |
| 2014 | 1.375 |
| 2013 | 0.75 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
