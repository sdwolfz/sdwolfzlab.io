---
title: "PARSLEY ENERGY INC (PE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PARSLEY ENERGY INC</td></tr>
    <tr><td>Symbol</td><td>PE</td></tr>
    <tr><td>Web</td><td><a href="http://www.parsleyenergy.com">www.parsleyenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.2 |
| 2019 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
