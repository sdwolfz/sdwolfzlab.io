---
title: " (CHSCO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CHSCO</td></tr>
    <tr><td>Web</td><td><a href="https://www.chsinc.com">www.chsinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.492 |
| 2020 | 1.969 |
| 2019 | 1.969 |
| 2018 | 1.969 |
| 2017 | 1.969 |
| 2016 | 1.969 |
| 2015 | 1.969 |
| 2014 | 1.969 |
| 2013 | 0.52 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
