---
title: "PCTEL INC (PCTI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PCTEL INC</td></tr>
    <tr><td>Symbol</td><td>PCTI</td></tr>
    <tr><td>Web</td><td><a href="https://www.pctel.com">www.pctel.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.11 |
| 2020 | 0.22 |
| 2019 | 0.22 |
| 2018 | 0.22 |
| 2017 | 0.21 |
| 2016 | 0.2 |
| 2015 | 0.2 |
| 2014 | 0.16 |
| 2013 | 0.14 |
| 2012 | 0.12 |
| 2011 | 0.03 |
| 2008 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
