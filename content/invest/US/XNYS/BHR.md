---
title: " (BHR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BHR</td></tr>
    <tr><td>Web</td><td><a href="https://www.bhrreit.com">www.bhrreit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.684 |
| 2018 | 0.64 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
