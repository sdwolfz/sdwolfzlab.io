---
title: "GLADSTONE INVESTMENT CORPORATION 6.25% CUM TERM PFD SER D USD25 (GAINM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GLADSTONE INVESTMENT CORPORATION 6.25% CUM TERM PFD SER D USD25</td></tr>
    <tr><td>Symbol</td><td>GAINM</td></tr>
    <tr><td>Web</td><td><a href="http://www.gladstoneinvestment.com">www.gladstoneinvestment.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.562 |
| 2019 | 1.562 |
| 2018 | 1.562 |
| 2017 | 1.562 |
| 2016 | 0.412 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
