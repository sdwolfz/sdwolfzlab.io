---
title: "EXELON CORP (EXC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>EXELON CORP</td></tr>
    <tr><td>Symbol</td><td>EXC</td></tr>
    <tr><td>Web</td><td><a href="https://www.exeloncorp.com">www.exeloncorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.765 |
| 2020 | 1.53 |
| 2019 | 1.45 |
| 2018 | 1.38 |
| 2017 | 1.31 |
| 2016 | 1.264 |
| 2015 | 1.24 |
| 2014 | 1.24 |
| 2013 | 1.455 |
| 2012 | 2.1 |
| 2011 | 2.1 |
| 2010 | 2.1 |
| 2009 | 2.1 |
| 2008 | 2.025 |
| 2007 | 1.76 |
| 2006 | 1.6 |
| 2005 | 1.6 |
| 2004 | 1.53 |
| 2003 | 1.92 |
| 2002 | 1.76 |
| 2001 | 1.82 |
| 2000 | 0.658 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
