---
title: "GOODYEAR TIRE & RUBBER CO (GT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GOODYEAR TIRE & RUBBER CO</td></tr>
    <tr><td>Symbol</td><td>GT</td></tr>
    <tr><td>Web</td><td><a href="https://www.goodyear.com">www.goodyear.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.16 |
| 2019 | 0.64 |
| 2018 | 0.58 |
| 2017 | 0.44 |
| 2016 | 0.31 |
| 2015 | 0.25 |
| 2014 | 0.22 |
| 2013 | 0.05 |
| 2002 | 0.48 |
| 2001 | 1.02 |
| 2000 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
