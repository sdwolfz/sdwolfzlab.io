---
title: "BARNWELL INDUSTRIES INC (BRN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BARNWELL INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>BRN</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2008 | 0.225 |
| 2007 | 0.15 |
| 2006 | 0.25 |
| 2005 | 0.195 |
| 2004 | 0.9 |
| 2003 | 0.2 |
| 2001 | 0.5 |
| 2000 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
