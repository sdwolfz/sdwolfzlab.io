---
title: "RED ROCK RESORTS INC (RRR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>RED ROCK RESORTS INC</td></tr>
    <tr><td>Symbol</td><td>RRR</td></tr>
    <tr><td>Web</td><td><a href="https://www.redrockresorts.com">www.redrockresorts.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.1 |
| 2019 | 0.4 |
| 2018 | 0.4 |
| 2017 | 0.4 |
| 2016 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
