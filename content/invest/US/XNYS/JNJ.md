---
title: "JOHNSON & JOHNSON (JNJ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>JOHNSON & JOHNSON</td></tr>
    <tr><td>Symbol</td><td>JNJ</td></tr>
    <tr><td>Web</td><td><a href="https://www.jnj.com">www.jnj.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.07 |
| 2020 | 3.98 |
| 2019 | 3.75 |
| 2018 | 3.54 |
| 2017 | 3.32 |
| 2016 | 3.15 |
| 2015 | 2.95 |
| 2014 | 2.76 |
| 2013 | 1.32 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
