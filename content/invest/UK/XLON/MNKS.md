---
title: "Monks Inv (MNKS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Monks Inv</td></tr>
    <tr><td>Symbol</td><td>MNKS</td></tr>
    <tr><td>Web</td><td><a href="https://www.bailliegifford.com">www.bailliegifford.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.85 |
| 2019 | 1.85 |
| 2018 | 1.4 |
| 2017 | 1.25 |
| 2016 | 1.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
