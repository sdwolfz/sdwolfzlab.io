---
title: "IOR SA BUCURESTI (IORB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>IOR SA BUCURESTI</td></tr>
    <tr><td>Symbol</td><td>IORB</td></tr>
    <tr><td>Web</td><td><a href="https://www.ior.ro">www.ior.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2007 | 0.0089 |
| 2005 | 0.0003 |
| 2004 | 0.0015 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
