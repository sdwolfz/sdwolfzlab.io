---
title: "NORTHWESTERN CORPORATION (NWE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NORTHWESTERN CORPORATION</td></tr>
    <tr><td>Symbol</td><td>NWE</td></tr>
    <tr><td>Web</td><td><a href="https://www.northwesternenergy.com">www.northwesternenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.24 |
| 2020 | 2.4 |
| 2019 | 2.3 |
| 2018 | 2.2 |
| 2017 | 2.1 |
| 2016 | 2.0 |
| 2015 | 1.92 |
| 2014 | 1.6 |
| 2013 | 1.52 |
| 2012 | 1.48 |
| 2011 | 1.44 |
| 2010 | 1.36 |
| 2009 | 1.34 |
| 2008 | 1.32 |
| 2007 | 1.28 |
| 2006 | 1.24 |
| 2005 | 1.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
