---
title: " (CHSCL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CHSCL</td></tr>
    <tr><td>Web</td><td><a href="https://www.chsinc.com">www.chsinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.469 |
| 2020 | 1.875 |
| 2019 | 1.875 |
| 2018 | 1.875 |
| 2017 | 1.875 |
| 2016 | 1.875 |
| 2015 | 1.776 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
