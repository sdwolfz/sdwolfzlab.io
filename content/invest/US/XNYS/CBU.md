---
title: "COMMUNITY BANK SYSTEMS INC (CBU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>COMMUNITY BANK SYSTEMS INC</td></tr>
    <tr><td>Symbol</td><td>CBU</td></tr>
    <tr><td>Web</td><td><a href="https://www.cbna.com">www.cbna.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.42 |
| 2020 | 1.66 |
| 2019 | 1.58 |
| 2018 | 1.44 |
| 2017 | 1.32 |
| 2016 | 1.26 |
| 2015 | 1.22 |
| 2014 | 1.16 |
| 2013 | 0.83 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
