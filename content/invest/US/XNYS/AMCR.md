---
title: "AMCOR PLC (AMCR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AMCOR PLC</td></tr>
    <tr><td>Symbol</td><td>AMCR</td></tr>
    <tr><td>Web</td><td><a href="https://www.amcor.com">www.amcor.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.234 |
| 2020 | 0.462 |
| 2019 | 0.298 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
