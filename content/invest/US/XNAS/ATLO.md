---
title: "AMES NATIONAL CORP (ATLO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AMES NATIONAL CORP</td></tr>
    <tr><td>Symbol</td><td>ATLO</td></tr>
    <tr><td>Web</td><td><a href="https://www.amesnational.com">www.amesnational.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.51 |
| 2020 | 0.99 |
| 2019 | 0.95 |
| 2018 | 1.16 |
| 2017 | 0.87 |
| 2016 | 0.83 |
| 2015 | 0.78 |
| 2014 | 0.7 |
| 2013 | 0.63 |
| 2012 | 0.58 |
| 2011 | 0.5 |
| 2010 | 0.43 |
| 2009 | 0.58 |
| 2008 | 1.11 |
| 2007 | 1.07 |
| 2006 | 1.03 |
| 2005 | 1.74 |
| 2004 | 2.39 |
| 2003 | 2.26 |
| 2002 | 1.32 |
| 2001 | 1.24 |
| 2000 | 1.58 |
| 1999 | 1.85 |
| 1998 | 0.355 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
