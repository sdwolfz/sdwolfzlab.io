---
title: "Ruby HTTP calls"
date: 2022-01-03T09:30:29Z
type: posts
slug: ruby-http-calls
tags: ["ruby"]
---

## Setup

Use the basic, standard, bundled, ruby HTTP library, no other gem needed.

```ruby
require 'net/http'

url     = 'https://www.codrut.pro'
headers = { 'X-HELLO' => 'world' }
use_ssl = true
```

## GET

```ruby
params = { 'a' => 1 }
result = nil

uri = URI(url)
Net::HTTP.start(uri.host, uri.port, use_ssl: use_ssl) do |http|
  uri.query = URI.encode_www_form(params) if params
  request   = Net::HTTP::Get.new(uri)

  headers&.each_pair do |key, value|
    request[key] = value
  end

  result = http.request(request)
end

puts result.code
puts result.body
```

## POST

```ruby
body   = { 'b' => 2 }.to_json
result = nil

uri = URI(url)
Net::HTTP.start(uri.host, uri.port, use_ssl: use_ssl) do |http|
  request = Net::HTTP::Post.new(uri)

  headers&.each_pair do |key, value|
    request[key] = value
  end

  result http.request(request, body)
end

puts result.code
puts result.body
```
