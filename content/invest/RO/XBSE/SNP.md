---
title: "OMV PETROM S.A. (SNP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>OMV PETROM S.A.</td></tr>
    <tr><td>Symbol</td><td>SNP</td></tr>
    <tr><td>Web</td><td><a href="https://www.omvpetrom.com">www.omvpetrom.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.031 |
| 2019 | 0.031 |
| 2018 | 0.027 |
| 2017 | 0.02 |
| 2016 | 0.015 |
| 2014 | 0.0112 |
| 2013 | 0.0308 |
| 2012 | 0.028 |
| 2011 | 0.031 |
| 2010 | 0.0177 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
