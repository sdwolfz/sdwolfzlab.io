---
title: " (PDM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>PDM</td></tr>
    <tr><td>Web</td><td><a href="https://www.piedmontreit.com">www.piedmontreit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.42 |
| 2020 | 0.84 |
| 2019 | 0.84 |
| 2018 | 0.84 |
| 2017 | 1.34 |
| 2016 | 0.84 |
| 2015 | 0.84 |
| 2014 | 0.81 |
| 2013 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
