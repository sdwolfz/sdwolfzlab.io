---
title: " (CMO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CMO</td></tr>
    <tr><td>Web</td><td><a href="https://www.capstead.com">www.capstead.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.15 |
| 2020 | 0.6 |
| 2019 | 0.47 |
| 2018 | 0.49 |
| 2017 | 0.8 |
| 2016 | 0.95 |
| 2015 | 1.14 |
| 2014 | 1.36 |
| 2013 | 0.93 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
