---
title: "HENDERSON ALTERNATIVE STRAT.TST PLC ORD 25P (HAST)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>HENDERSON ALTERNATIVE STRAT.TST PLC ORD 25P</td></tr>
    <tr><td>Symbol</td><td>HAST</td></tr>
    <tr><td>Web</td><td><a href="http://www.hendersonalternativestrategies.com">www.hendersonalternativestrategies.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 5.5 |
| 2019 | 7.5 |
| 2017 | 4.75 |
| 2016 | 6.4 |
| 2015 | 3.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
