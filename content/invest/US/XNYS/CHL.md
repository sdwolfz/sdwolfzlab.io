---
title: "CHINA MOBILE LTD SPON ADR REP 5 ORD HKD0.10 (CHL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CHINA MOBILE LTD SPON ADR REP 5 ORD HKD0.10</td></tr>
    <tr><td>Symbol</td><td>CHL</td></tr>
    <tr><td>Web</td><td><a href="http://www.chinamobileltd.com">www.chinamobileltd.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.888 |
| 2019 | 1.678 |
| 2018 | 1.957 |
| 2017 | 3.498 |
| 2016 | 1.557 |
| 2015 | 1.686 |
| 2014 | 2.724 |
| 2013 | 2.015 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
