---
title: "AMERICAN SHARED HOSPITAL SERVICES (AMS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AMERICAN SHARED HOSPITAL SERVICES</td></tr>
    <tr><td>Symbol</td><td>AMS</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2007 | 0.095 |
| 2006 | 0.19 |
| 2005 | 0.188 |
| 2004 | 0.173 |
| 2003 | 0.2 |
| 2002 | 0.12 |
| 2001 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
