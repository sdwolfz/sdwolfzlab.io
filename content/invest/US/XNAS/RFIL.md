---
title: "RF INDUSTRIES (RFIL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>RF INDUSTRIES</td></tr>
    <tr><td>Symbol</td><td>RFIL</td></tr>
    <tr><td>Web</td><td><a href="https://www.rfindustries.com">www.rfindustries.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.02 |
| 2019 | 0.08 |
| 2018 | 0.08 |
| 2017 | 0.08 |
| 2016 | 0.08 |
| 2015 | 0.28 |
| 2014 | 0.28 |
| 2013 | 0.28 |
| 2012 | 0.25 |
| 2011 | 0.395 |
| 2010 | 0.06 |
| 2008 | 0.15 |
| 2007 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
