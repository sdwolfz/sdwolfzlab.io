---
title: "CLS Holdings (CLI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>CLS Holdings</td></tr>
    <tr><td>Symbol</td><td>CLI</td></tr>
    <tr><td>Web</td><td><a href="https://www.clsholdings.com">www.clsholdings.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 7.4 |
| 2018 | 6.9 |
| 2017 | 6.35 |
| 2016 | 5.75 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
