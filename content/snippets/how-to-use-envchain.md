---
title: "How to Use: envchain"
date: 2020-03-28T19:59:20Z
type: posts
slug: how-to-use-envchain
series: ["How to Use"]
tags: ["envchain", "tutorial", "linux"]
---

## Install

```sh
git clone https://github.com/sorah/envchain.git
cd envchain
make
sudo make install
```

## Usage

Store a new environment variable with the following command, adding
the value after the prompt:

```sh
envchain -s -n namespace MY_NEW_ENV_VAR
```

Execute a command with your new environment variable set:

```sh
envchain namespace env | grep MY_NEW_ENV_VAR=
```
