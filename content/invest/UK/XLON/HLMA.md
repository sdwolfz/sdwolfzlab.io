---
title: "Halma (HLMA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Halma</td></tr>
    <tr><td>Symbol</td><td>HLMA</td></tr>
    <tr><td>Web</td><td><a href="https://www.halma.com">www.halma.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 16.5 |
| 2019 | 15.71 |
| 2018 | 14.68 |
| 2017 | 13.71 |
| 2016 | 12.81 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
