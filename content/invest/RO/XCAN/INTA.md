---
title: "INDEPENDENTA SA SIBIU (INTA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>INDEPENDENTA SA SIBIU</td></tr>
    <tr><td>Symbol</td><td>INTA</td></tr>
    <tr><td>Web</td><td><a href="https://www.independentasa.ro">www.independentasa.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 1.0447 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
