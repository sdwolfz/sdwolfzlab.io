---
title: "GASLOG LTD (GLOG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GASLOG LTD</td></tr>
    <tr><td>Symbol</td><td>GLOG</td></tr>
    <tr><td>Web</td><td><a href="https://www.gaslogltd.com">www.gaslogltd.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.3 |
| 2019 | 0.98 |
| 2018 | 0.99 |
| 2017 | 0.56 |
| 2016 | 0.56 |
| 2015 | 0.56 |
| 2014 | 0.5 |
| 2013 | 0.34 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
