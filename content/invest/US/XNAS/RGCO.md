---
title: "RGC RESOURCES INC (RGCO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>RGC RESOURCES INC</td></tr>
    <tr><td>Symbol</td><td>RGCO</td></tr>
    <tr><td>Web</td><td><a href="https://www.rgcresources.com">www.rgcresources.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.555 |
| 2020 | 0.7 |
| 2019 | 0.66 |
| 2018 | 0.62 |
| 2017 | 0.653 |
| 2016 | 0.81 |
| 2015 | 0.77 |
| 2014 | 0.74 |
| 2013 | 0.72 |
| 2012 | 1.7 |
| 2011 | 1.19 |
| 2010 | 1.32 |
| 2009 | 1.28 |
| 2008 | 1.25 |
| 2007 | 1.22 |
| 2006 | 1.2 |
| 2005 | 1.18 |
| 2004 | 5.67 |
| 2003 | 1.14 |
| 2002 | 1.14 |
| 2001 | 1.12 |
| 2000 | 1.1 |
| 1999 | 1.08 |
| 1998 | 1.06 |
| 1997 | 1.3 |
| 1996 | 1.02 |
| 1995 | 1.0 |
| 1994 | 1.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
