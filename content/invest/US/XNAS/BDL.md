---
title: "FLANIGAN'S ENTERPRISES INC (BDL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FLANIGAN'S ENTERPRISES INC</td></tr>
    <tr><td>Symbol</td><td>BDL</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.3 |
| 2019 | 0.28 |
| 2018 | 0.25 |
| 2017 | 0.2 |
| 2016 | 0.18 |
| 2015 | 0.15 |
| 2011 | 0.1 |
| 2006 | 0.35 |
| 2005 | 0.32 |
| 2003 | 0.57 |
| 2002 | 0.11 |
| 2001 | 0.37 |
| 2000 | 0.11 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
