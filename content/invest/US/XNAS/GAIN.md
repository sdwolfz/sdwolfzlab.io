---
title: "GLADSTONE INVESTMENT CORPORATION (GAIN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GLADSTONE INVESTMENT CORPORATION</td></tr>
    <tr><td>Symbol</td><td>GAIN</td></tr>
    <tr><td>Web</td><td><a href="https://www.gladstoneinvestment.com">www.gladstoneinvestment.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.48 |
| 2020 | 0.93 |
| 2019 | 1.026 |
| 2018 | 0.921 |
| 2017 | 0.887 |
| 2016 | 0.75 |
| 2015 | 0.743 |
| 2014 | 0.77 |
| 2013 | 0.68 |
| 2012 | 0.63 |
| 2011 | 0.555 |
| 2010 | 0.48 |
| 2009 | 0.6 |
| 2008 | 0.96 |
| 2007 | 0.915 |
| 2006 | 0.84 |
| 2005 | 0.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
