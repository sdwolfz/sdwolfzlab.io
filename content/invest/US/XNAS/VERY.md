---
title: "VERICITY INC (VERY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>VERICITY INC</td></tr>
    <tr><td>Symbol</td><td>VERY</td></tr>
    <tr><td>Web</td><td><a href="https://www.vericity.com">www.vericity.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 6.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
