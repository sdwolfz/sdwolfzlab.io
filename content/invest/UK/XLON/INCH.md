---
title: "Inchcape (INCH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Inchcape</td></tr>
    <tr><td>Symbol</td><td>INCH</td></tr>
    <tr><td>Web</td><td><a href="https://www.inchcape.com">www.inchcape.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 26.8 |
| 2018 | 26.8 |
| 2017 | 26.8 |
| 2016 | 23.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
