---
title: "GENASYS INC (GNSS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GENASYS INC</td></tr>
    <tr><td>Symbol</td><td>GNSS</td></tr>
    <tr><td>Web</td><td><a href="https://www.genasys.com">www.genasys.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.03 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
