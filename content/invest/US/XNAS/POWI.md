---
title: "POWER INTEGRATIONS INC (POWI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>POWER INTEGRATIONS INC</td></tr>
    <tr><td>Symbol</td><td>POWI</td></tr>
    <tr><td>Web</td><td><a href="https://www.power.com">www.power.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.26 |
| 2020 | 0.62 |
| 2019 | 0.7 |
| 2018 | 0.64 |
| 2017 | 0.56 |
| 2016 | 0.52 |
| 2015 | 0.48 |
| 2014 | 0.44 |
| 2013 | 0.32 |
| 2012 | 0.2 |
| 2011 | 0.2 |
| 2010 | 0.2 |
| 2009 | 0.1 |
| 2008 | 0.025 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
