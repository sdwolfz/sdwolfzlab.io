---
title: "RYDER SYSTEM INC (R)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>RYDER SYSTEM INC</td></tr>
    <tr><td>Symbol</td><td>R</td></tr>
    <tr><td>Web</td><td><a href="https://www.ryder.com">www.ryder.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.56 |
| 2020 | 2.24 |
| 2019 | 2.2 |
| 2018 | 2.12 |
| 2017 | 1.8 |
| 2016 | 1.7 |
| 2015 | 1.56 |
| 2014 | 1.42 |
| 2013 | 0.68 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
