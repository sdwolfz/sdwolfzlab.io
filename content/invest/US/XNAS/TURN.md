---
title: " (TURN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>TURN</td></tr>
    <tr><td>Web</td><td><a href="https://www.180degreecapital.com">www.180degreecapital.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2002 | 0.126 |
| 2000 | 0.02 |
| 1999 | 0.35 |
| 1998 | 0.75 |
| 1990 | 0.04 |
| 1989 | 0.04 |
| 1988 | 0.01 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
