---
title: " (CONE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CONE</td></tr>
    <tr><td>Web</td><td><a href="https://www.cyrusone.com">www.cyrusone.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.02 |
| 2020 | 2.02 |
| 2019 | 1.92 |
| 2018 | 1.84 |
| 2017 | 1.68 |
| 2016 | 1.52 |
| 2015 | 1.26 |
| 2014 | 0.84 |
| 2013 | 0.64 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
