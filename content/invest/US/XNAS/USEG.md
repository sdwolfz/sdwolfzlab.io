---
title: "U.S.ENERGY CORP (USEG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>U.S.ENERGY CORP</td></tr>
    <tr><td>Symbol</td><td>USEG</td></tr>
    <tr><td>Web</td><td><a href="https://www.usnrg.com">www.usnrg.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2007 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
