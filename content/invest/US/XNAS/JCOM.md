---
title: "J2 GLOBAL INC (JCOM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>J2 GLOBAL INC</td></tr>
    <tr><td>Symbol</td><td>JCOM</td></tr>
    <tr><td>Web</td><td><a href="https://www.j2global.com">www.j2global.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.9 |
| 2018 | 1.68 |
| 2017 | 1.52 |
| 2016 | 1.36 |
| 2015 | 1.215 |
| 2014 | 1.095 |
| 2013 | 0.975 |
| 2012 | 0.87 |
| 2011 | 0.405 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
