---
title: "Rathbone (RAT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Rathbone</td></tr>
    <tr><td>Symbol</td><td>RAT</td></tr>
    <tr><td>Web</td><td><a href="https://www.rathbones.com">www.rathbones.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 70.0 |
| 2018 | 66.0 |
| 2017 | 61.0 |
| 2016 | 57.0 |
| 2015 | 55.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
