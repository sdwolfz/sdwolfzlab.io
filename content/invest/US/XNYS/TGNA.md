---
title: "TEGNA INC (TGNA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TEGNA INC</td></tr>
    <tr><td>Symbol</td><td>TGNA</td></tr>
    <tr><td>Web</td><td><a href="https://www.tegna.com">www.tegna.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.165 |
| 2020 | 0.28 |
| 2019 | 0.28 |
| 2018 | 0.28 |
| 2017 | 0.35 |
| 2016 | 0.56 |
| 2015 | 0.48 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
