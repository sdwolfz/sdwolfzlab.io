---
title: "Install Cask from source"
date: 2020-04-10T20:40:09Z
type: posts
slug: install-cask-from-source
tags: ["cask", "emacs", "spacemacs", "linux", "development"]
---

## Prerequisites

* [Install Emacs](/snippets/install-emacs-from-source/)

## Install

Clone and link the repository:

```sh
git clone https://github.com/cask/cask.git
rm -rf ~/.cask
ln -s "$PWD"/cask ~/.cask
```

Add this to your `.bash_profile`:

```bash
export PATH=~/.cask/bin:$PATH
```

Install lazy loaded packages:

```sh
cask --help
```
