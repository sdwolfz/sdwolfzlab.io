---
title: "PRICE T ROWE GROUPS (TROW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PRICE T ROWE GROUPS</td></tr>
    <tr><td>Symbol</td><td>TROW</td></tr>
    <tr><td>Web</td><td><a href="https://www.troweprice.com">www.troweprice.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.08 |
| 2020 | 3.6 |
| 2019 | 3.04 |
| 2018 | 2.8 |
| 2017 | 2.28 |
| 2016 | 2.16 |
| 2015 | 4.08 |
| 2014 | 1.76 |
| 2013 | 1.52 |
| 2012 | 2.36 |
| 2011 | 1.24 |
| 2010 | 1.08 |
| 2009 | 1.0 |
| 2008 | 0.96 |
| 2007 | 0.75 |
| 2006 | 0.87 |
| 2005 | 0.97 |
| 2004 | 0.8 |
| 2003 | 0.7 |
| 2002 | 0.65 |
| 2001 | 0.61 |
| 2000 | 0.54 |
| 1999 | 0.43 |
| 1998 | 0.44 |
| 1997 | 0.56 |
| 1996 | 0.55 |
| 1995 | 0.69 |
| 1994 | 0.55 |
| 1993 | 0.55 |
| 1992 | 0.75 |
| 1991 | 0.66 |
| 1990 | 0.61 |
| 1989 | 0.93 |
| 1988 | 0.21 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
