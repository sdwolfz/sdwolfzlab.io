---
title: "HAVERTY FURNITURE COS INC (HVT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HAVERTY FURNITURE COS INC</td></tr>
    <tr><td>Symbol</td><td>HVT</td></tr>
    <tr><td>Web</td><td><a href="https://www.havertys.com">www.havertys.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.22 |
| 2020 | 2.77 |
| 2019 | 0.76 |
| 2018 | 1.72 |
| 2017 | 0.54 |
| 2016 | 1.44 |
| 2015 | 0.355 |
| 2014 | 0.32 |
| 2013 | 0.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
