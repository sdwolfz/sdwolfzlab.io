---
title: "COPA HOLDINGS SA (CPA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>COPA HOLDINGS SA</td></tr>
    <tr><td>Symbol</td><td>CPA</td></tr>
    <tr><td>Web</td><td><a href="https://www.copaair.com">www.copaair.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.8 |
| 2019 | 2.6 |
| 2018 | 3.48 |
| 2017 | 2.52 |
| 2016 | 2.04 |
| 2015 | 3.36 |
| 2014 | 3.84 |
| 2013 | 1.46 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
