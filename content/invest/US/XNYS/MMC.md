---
title: "MARSH & MCLENNAN COMPANIES INC (MMC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MARSH & MCLENNAN COMPANIES INC</td></tr>
    <tr><td>Symbol</td><td>MMC</td></tr>
    <tr><td>Web</td><td><a href="https://www.mmc.com">www.mmc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.93 |
| 2020 | 1.84 |
| 2019 | 1.74 |
| 2018 | 1.58 |
| 2017 | 1.43 |
| 2016 | 1.3 |
| 2015 | 1.18 |
| 2014 | 1.06 |
| 2013 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
