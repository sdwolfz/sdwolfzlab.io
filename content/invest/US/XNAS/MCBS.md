---
title: "METROCITY BANKSHARES INC (MCBS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>METROCITY BANKSHARES INC</td></tr>
    <tr><td>Symbol</td><td>MCBS</td></tr>
    <tr><td>Web</td><td><a href="https://www.metrocitybank.com">www.metrocitybank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.4 |
| 2019 | 0.11 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
