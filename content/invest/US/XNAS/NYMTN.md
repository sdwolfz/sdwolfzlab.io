---
title: " (NYMTN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>NYMTN</td></tr>
    <tr><td>Web</td><td><a href="https://www.nymtrust.com">www.nymtrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.5 |
| 2020 | 2.0 |
| 2019 | 2.0 |
| 2018 | 2.0 |
| 2017 | 0.511 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
