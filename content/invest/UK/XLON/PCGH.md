---
title: "Polar Cap Glbl (PCGH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Polar Cap Glbl</td></tr>
    <tr><td>Symbol</td><td>PCGH</td></tr>
    <tr><td>Web</td><td><a href="https://www.polarcapitalhealthcaretrust.co.uk">www.polarcapitalhealthcaretrust.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.0 |
| 2019 | 2.1 |
| 2018 | 2.0 |
| 2017 | 3.4 |
| 2016 | 4.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
