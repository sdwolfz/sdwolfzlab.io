---
title: "Alliance Trust (ATST)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Alliance Trust</td></tr>
    <tr><td>Symbol</td><td>ATST</td></tr>
    <tr><td>Web</td><td><a href="https://www.alliancetrust.co.uk">www.alliancetrust.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 14.38 |
| 2019 | 13.96 |
| 2018 | 13.55 |
| 2017 | 13.16 |
| 2016 | 12.77 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
