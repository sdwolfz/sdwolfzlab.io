---
title: " (PBFX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>PBFX</td></tr>
    <tr><td>Web</td><td><a href="https://www.pbflogistics.com">www.pbflogistics.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.6 |
| 2020 | 1.42 |
| 2019 | 2.05 |
| 2018 | 1.97 |
| 2017 | 1.86 |
| 2016 | 1.7 |
| 2015 | 1.44 |
| 2014 | 0.46 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
