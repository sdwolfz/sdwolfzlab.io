---
title: "PREFORMED LINE PRODUCTS CO (PLPC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PREFORMED LINE PRODUCTS CO</td></tr>
    <tr><td>Symbol</td><td>PLPC</td></tr>
    <tr><td>Web</td><td><a href="https://www.preformed.com">www.preformed.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.8 |
| 2019 | 0.8 |
| 2018 | 0.8 |
| 2017 | 0.8 |
| 2016 | 0.8 |
| 2015 | 0.8 |
| 2014 | 0.8 |
| 2013 | 0.6 |
| 2012 | 1.0 |
| 2011 | 0.8 |
| 2010 | 0.8 |
| 2009 | 0.8 |
| 2008 | 0.8 |
| 2007 | 0.8 |
| 2006 | 0.8 |
| 2005 | 0.8 |
| 2004 | 0.8 |
| 2003 | 0.8 |
| 2002 | 0.8 |
| 2001 | 0.75 |
| 2000 | 0.6 |
| 1999 | 0.6 |
| 1998 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
