---
title: "MONOLITHIC POWER SYSTEM INC (MPWR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MONOLITHIC POWER SYSTEM INC</td></tr>
    <tr><td>Symbol</td><td>MPWR</td></tr>
    <tr><td>Web</td><td><a href="https://www.monolithicpower.com">www.monolithicpower.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.6 |
| 2020 | 2.0 |
| 2019 | 1.6 |
| 2018 | 1.2 |
| 2017 | 0.8 |
| 2016 | 0.8 |
| 2015 | 0.8 |
| 2014 | 0.45 |
| 2012 | 1.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
