---
title: "BANC OF CALIFORNIA INC (BANC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BANC OF CALIFORNIA INC</td></tr>
    <tr><td>Symbol</td><td>BANC</td></tr>
    <tr><td>Web</td><td><a href="https://www.bancofcal.com">www.bancofcal.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.06 |
| 2020 | 0.24 |
| 2019 | 0.31 |
| 2018 | 0.52 |
| 2017 | 0.52 |
| 2016 | 0.49 |
| 2015 | 0.48 |
| 2014 | 0.48 |
| 2013 | 0.36 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
