---
title: "BLACKROCK HEALTH SCIENCES TR II (BMEZ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BLACKROCK HEALTH SCIENCES TR II</td></tr>
    <tr><td>Symbol</td><td>BMEZ</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.635 |
| 2020 | 0.9 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
