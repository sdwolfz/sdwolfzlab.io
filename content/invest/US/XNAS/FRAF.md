---
title: "FRANKLIN FINANCIAL SERVICES CORP (FRAF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FRANKLIN FINANCIAL SERVICES CORP</td></tr>
    <tr><td>Symbol</td><td>FRAF</td></tr>
    <tr><td>Web</td><td><a href="https://www.franklinfin.com">www.franklinfin.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.61 |
| 2020 | 1.2 |
| 2019 | 0.9 |
| 2015 | 0.19 |
| 2014 | 0.68 |
| 2013 | 0.68 |
| 2012 | 0.78 |
| 2011 | 1.08 |
| 2010 | 1.08 |
| 2009 | 1.08 |
| 2008 | 1.07 |
| 2007 | 1.03 |
| 2006 | 0.99 |
| 2005 | 0.95 |
| 2004 | 0.98 |
| 2003 | 1.02 |
| 2002 | 1.19 |
| 2001 | 0.86 |
| 2000 | 0.76 |
| 1999 | 1.08 |
| 1998 | 0.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
