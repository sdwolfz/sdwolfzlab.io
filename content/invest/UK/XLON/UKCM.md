---
title: "UK Commercial Property Trust (UKCM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>UK Commercial Property Trust</td></tr>
    <tr><td>Symbol</td><td>UKCM</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 3.68 |
| 2018 | 3.68 |
| 2017 | 3.68 |
| 2016 | 3.68 |
| 2015 | 3.68 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
