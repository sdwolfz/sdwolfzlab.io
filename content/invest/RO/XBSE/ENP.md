---
title: "COMPANIA ENERGOPETROL S.A. (ENP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>COMPANIA ENERGOPETROL S.A.</td></tr>
    <tr><td>Symbol</td><td>ENP</td></tr>
    <tr><td>Web</td><td><a href="https://www.energo.ro">www.energo.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2006 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
