---
title: "SOUTHERN CO. (SO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SOUTHERN CO.</td></tr>
    <tr><td>Symbol</td><td>SO</td></tr>
    <tr><td>Web</td><td><a href="https://www.southerncompany.com">www.southerncompany.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.3 |
| 2020 | 2.54 |
| 2019 | 2.46 |
| 2018 | 2.38 |
| 2017 | 2.3 |
| 2016 | 2.223 |
| 2015 | 2.154 |
| 2014 | 2.082 |
| 2013 | 1.014 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
