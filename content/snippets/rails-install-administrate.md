---
title: "Rails: install Administrate"
date: 2024-01-23T08:21:51Z
type: posts
slug: rails-install-administrate
tags: ["development", "rails", "ruby"]
---

## Gems

Add these to your `Gemfile`:

```ruby
# Administration
gem 'administrate', '~> 0.20.0'

# Authentication
gem 'devise', '~> 4.9', '>= 4.9.3'
```

Then install your new gems:

```sh
bundle install
```

## Generate

```sh
bundle exec rails generate devise:install
bundle exec rails generate devise SystemOperator
bundle exec rails generate administrate:install --namespace=operations
```

```sh
bundle exec rails generate administrate:dashboard SystemOperator --namespace=operations
```

## Changes

```ruby
  devise_for :system_operators

  namespace :operations do
    resources :system_operators, only: %i[index show new create edit update destroy]

    root to: 'system_operators#index'
  end
```

> NOTE: use the PLURAL form of words:

```ruby
resourceSSSSSSSSSSSSSSSSSSSSSS :system_operatorSSSSSSSSSSSSSSSSSSSS
```

## Generate

Now you can generate new models:

```sh
bundle exec rails g model Organisation name:string slug:string --migration
```

Run migrations:

```sh
bundle exec rails db:migrate
```

Then generate the dashboard:

```sh
bundle exec rails generate administrate:dashboard Organisation --namespace=operations
```

## Tweaking

You can rollback your migration to edit, then gneerate models again:

```sh
bundle exec rails db:rollback STEP=1
```
