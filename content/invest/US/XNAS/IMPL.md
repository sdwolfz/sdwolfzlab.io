---
title: "IMPEL NEUROPHARMA INC (IMPL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>IMPEL NEUROPHARMA INC</td></tr>
    <tr><td>Symbol</td><td>IMPL</td></tr>
    <tr><td>Web</td><td><a href="https://www.impelnp.com">www.impelnp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
