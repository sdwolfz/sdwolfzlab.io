---
title: " (BBDO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BBDO</td></tr>
    <tr><td>Web</td><td><a href="https://www.bradesco.com.br">www.bradesco.com.br</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.014 |
| 2020 | 0.095 |
| 2019 | 0.341 |
| 2018 | 0.179 |
| 2017 | 0.272 |
| 2016 | 0.331 |
| 2015 | 0.255 |
| 2014 | 0.164 |
| 2013 | 0.031 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
