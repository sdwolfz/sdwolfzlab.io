---
title: "LAKELAND BANCORP INC (LBAI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LAKELAND BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>LBAI</td></tr>
    <tr><td>Web</td><td><a href="https://www.lakelandbank.com">www.lakelandbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.26 |
| 2020 | 0.5 |
| 2019 | 0.49 |
| 2018 | 0.445 |
| 2017 | 0.395 |
| 2016 | 0.37 |
| 2015 | 0.33 |
| 2014 | 0.3 |
| 2013 | 0.285 |
| 2012 | 0.25 |
| 2011 | 0.18 |
| 2010 | 0.21 |
| 2009 | 0.3 |
| 2008 | 0.4 |
| 2007 | 0.3 |
| 2006 | 0.3 |
| 2005 | 0.3 |
| 2004 | 0.4 |
| 2003 | 0.39 |
| 2002 | 0.275 |
| 2001 | 0.25 |
| 2000 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
