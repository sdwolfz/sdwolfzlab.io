---
title: "KKR REAL ESTATE FINANCE TRUST INC (KREF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>KKR REAL ESTATE FINANCE TRUST INC</td></tr>
    <tr><td>Symbol</td><td>KREF</td></tr>
    <tr><td>Web</td><td><a href="https://www.kkrreit.com">www.kkrreit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.43 |
| 2020 | 1.72 |
| 2019 | 1.72 |
| 2018 | 1.69 |
| 2017 | 0.99 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
