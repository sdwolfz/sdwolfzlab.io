---
title: "RESOURCES CONNECTION INC (RGP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>RESOURCES CONNECTION INC</td></tr>
    <tr><td>Symbol</td><td>RGP</td></tr>
    <tr><td>Web</td><td><a href="https://www.rgp.com">www.rgp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 0.56 |
| 2019 | 0.54 |
| 2018 | 0.5 |
| 2017 | 0.46 |
| 2016 | 0.42 |
| 2015 | 0.36 |
| 2014 | 0.3 |
| 2013 | 0.26 |
| 2012 | 0.22 |
| 2011 | 0.18 |
| 2010 | 0.08 |
| 2007 | 1.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
