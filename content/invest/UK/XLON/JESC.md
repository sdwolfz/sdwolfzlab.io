---
title: "Jpmorg.eur (JESC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Jpmorg.eur</td></tr>
    <tr><td>Symbol</td><td>JESC</td></tr>
    <tr><td>Web</td><td><a href="https://am.jpmorgan.com">am.jpmorgan.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.7 |
| 2019 | 6.7 |
| 2018 | 6.7 |
| 2017 | 4.7 |
| 2016 | 3.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
