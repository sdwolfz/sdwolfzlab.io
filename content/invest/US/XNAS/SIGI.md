---
title: "SELECTIVE INSURANCE GROUP (SIGI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SELECTIVE INSURANCE GROUP</td></tr>
    <tr><td>Symbol</td><td>SIGI</td></tr>
    <tr><td>Web</td><td><a href="https://www.selective.com">www.selective.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.5 |
| 2020 | 0.94 |
| 2019 | 0.83 |
| 2018 | 0.74 |
| 2017 | 0.66 |
| 2016 | 0.61 |
| 2015 | 0.57 |
| 2014 | 0.53 |
| 2013 | 0.52 |
| 2012 | 0.52 |
| 2011 | 0.52 |
| 2010 | 0.52 |
| 2009 | 0.52 |
| 2008 | 0.52 |
| 2007 | 0.61 |
| 2006 | 0.88 |
| 2005 | 0.79 |
| 2004 | 0.7 |
| 2003 | 0.62 |
| 2002 | 0.6 |
| 2001 | 0.6 |
| 2000 | 0.6 |
| 1999 | 0.59 |
| 1998 | 0.56 |
| 1997 | 1.12 |
| 1996 | 1.12 |
| 1995 | 1.12 |
| 1994 | 1.12 |
| 1993 | 1.12 |
| 1992 | 1.1 |
| 1991 | 1.04 |
| 1990 | 1.02 |
| 1989 | 1.22 |
| 1988 | 0.31 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
