---
title: "ARK RESTAURANTS CORP (ARKR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ARK RESTAURANTS CORP</td></tr>
    <tr><td>Symbol</td><td>ARKR</td></tr>
    <tr><td>Web</td><td><a href="https://www.arkrestaurants.com">www.arkrestaurants.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.25 |
| 2019 | 1.0 |
| 2018 | 1.0 |
| 2017 | 1.0 |
| 2016 | 1.0 |
| 2015 | 1.0 |
| 2014 | 1.0 |
| 2013 | 1.0 |
| 2012 | 1.0 |
| 2011 | 1.0 |
| 2010 | 1.0 |
| 2009 | 1.25 |
| 2008 | 1.76 |
| 2007 | 4.58 |
| 2006 | 1.4 |
| 2005 | 1.4 |
| 2004 | 0.35 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
