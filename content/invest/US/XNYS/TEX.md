---
title: "TEREX CORP (TEX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TEREX CORP</td></tr>
    <tr><td>Symbol</td><td>TEX</td></tr>
    <tr><td>Web</td><td><a href="https://www.terex.com">www.terex.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.24 |
| 2020 | 0.12 |
| 2019 | 0.44 |
| 2018 | 0.4 |
| 2017 | 0.32 |
| 2016 | 0.28 |
| 2015 | 0.24 |
| 2014 | 0.2 |
| 2013 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
