---
title: " (CRT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CRT</td></tr>
    <tr><td>Web</td><td><a href="https://www.crt-crosstimbers.com">www.crt-crosstimbers.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.269 |
| 2020 | 0.778 |
| 2019 | 0.877 |
| 2018 | 1.428 |
| 2017 | 1.007 |
| 2016 | 1.061 |
| 2015 | 1.354 |
| 2014 | 2.656 |
| 2013 | 1.356 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
