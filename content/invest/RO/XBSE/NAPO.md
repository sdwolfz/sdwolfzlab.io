---
title: "SOCIETATEA DE CONSTRUCTII NAPOCA SA (NAPO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>SOCIETATEA DE CONSTRUCTII NAPOCA SA</td></tr>
    <tr><td>Symbol</td><td>NAPO</td></tr>
    <tr><td>Web</td><td><a href="https://www.sccnapoca.ro">www.sccnapoca.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2002 | 0.0385 |
| 2001 | 0.0285 |
| 2000 | 0.0154 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
