---
title: "SHIFT4 PAYMENTS INC (FOUR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SHIFT4 PAYMENTS INC</td></tr>
    <tr><td>Symbol</td><td>FOUR</td></tr>
    <tr><td>Web</td><td><a href="https://www.shift4.com">www.shift4.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
