---
title: "MERCANTILE BANK CORP (MBWM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MERCANTILE BANK CORP</td></tr>
    <tr><td>Symbol</td><td>MBWM</td></tr>
    <tr><td>Web</td><td><a href="https://www.mercbank.com">www.mercbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.58 |
| 2020 | 1.12 |
| 2019 | 1.06 |
| 2018 | 1.68 |
| 2017 | 0.74 |
| 2016 | 1.16 |
| 2015 | 0.58 |
| 2014 | 2.48 |
| 2013 | 0.45 |
| 2012 | 0.09 |
| 2010 | 0.01 |
| 2009 | 0.07 |
| 2008 | 0.31 |
| 2007 | 0.56 |
| 2006 | 0.51 |
| 2005 | 0.43 |
| 2004 | 0.36 |
| 2003 | 0.32 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
