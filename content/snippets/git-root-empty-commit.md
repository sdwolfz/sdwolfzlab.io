---
title: "Git root empty commit"
date: 2023-01-08T13:15:48Z
type: posts
slug: git-root-empty-commit
tags: ["git", "sh"]
---

The best way to structure yout repository is to have an empty root commit and to
squash all the other commits periodically onto the second. This reduces the
repository size making it easier to clone in CI and for your end users.

## Empty root commit

Create an empty commit:

```sh
git commit -m 'Initial commit' --allow-empty
```

Rebase until your root commit:

```sh
git rebase -i --root
```

Move the last commit at the very top of your commit list.

## Clean history

Rebase until your root commit:

```sh
git rebase -i --root
```

Now you do the follwoing changes:

1. Mark your second commit from the top from `pick` to `reword`.
2. Vertical select and change all other commits from line 3 downwards from
   `pick` to `fixup`.

## Result

After this you will be left with 2 commits:

* An empty initial commit
* A second commit that contains the entire diff up to this point.

Now your repository is properly nice an clean, like it's supposed to be.
