---
title: "How to Use: shellcheck"
date: 2020-06-25T19:38:41Z
type: posts
slug: how-to-use-shellcheck
series: ["How to Use"]
tags: ["bash", "tutorial", "linting", "productivity", "scripting"]
---

## Install

From package:

```sh
# Manjaro / Arch
sudo pacman -S shellcheck

# Alpine
apk add --update shellcheck
```

From Docker:

```sh
docker run --rm -it -v "$PWD":/work -w /work koalaman/shellcheck-alpine:latest sh
```

## Configure

Add a `.shellcheckrc` file containing your rules:

```
disable=SC2059
```

## Use

```sh
shellcheck ./path/to/script.sh
```
