---
title: "L3 HARRIS TECHNOLOGIES INC (LHX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>L3 HARRIS TECHNOLOGIES INC</td></tr>
    <tr><td>Symbol</td><td>LHX</td></tr>
    <tr><td>Web</td><td><a href="https://www.l3harris.com">www.l3harris.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.04 |
| 2020 | 3.4 |
| 2019 | 2.185 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
