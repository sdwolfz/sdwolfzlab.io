---
title: "FIRST CONSTITUTION BANCORP (FCCY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST CONSTITUTION BANCORP</td></tr>
    <tr><td>Symbol</td><td>FCCY</td></tr>
    <tr><td>Web</td><td><a href="https://www.1stconstitution.com">www.1stconstitution.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.19 |
| 2020 | 0.36 |
| 2019 | 0.3 |
| 2018 | 0.255 |
| 2017 | 0.16 |
| 2016 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
