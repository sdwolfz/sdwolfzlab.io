---
title: "CATHAY GENERAL BANCORP (CATY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CATHAY GENERAL BANCORP</td></tr>
    <tr><td>Symbol</td><td>CATY</td></tr>
    <tr><td>Web</td><td><a href="https://www.cathaybank.com">www.cathaybank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.31 |
| 2020 | 1.24 |
| 2019 | 1.24 |
| 2018 | 1.03 |
| 2017 | 0.87 |
| 2016 | 0.75 |
| 2015 | 0.56 |
| 2014 | 0.29 |
| 2013 | 0.08 |
| 2012 | 0.04 |
| 2011 | 0.04 |
| 2010 | 0.04 |
| 2009 | 0.205 |
| 2008 | 0.42 |
| 2007 | 0.405 |
| 2006 | 0.36 |
| 2005 | 0.36 |
| 2004 | 0.51 |
| 2003 | 0.56 |
| 2002 | 0.81 |
| 2001 | 1.0 |
| 2000 | 0.88 |
| 1999 | 0.805 |
| 1998 | 0.7 |
| 1997 | 0.625 |
| 1996 | 0.6 |
| 1995 | 0.6 |
| 1994 | 0.6 |
| 1993 | 0.6 |
| 1992 | 0.6 |
| 1991 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
