---
title: "Mckay Securities (MCKS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Mckay Securities</td></tr>
    <tr><td>Symbol</td><td>MCKS</td></tr>
    <tr><td>Web</td><td><a href="https://www.mckaysecurities.plc.uk">www.mckaysecurities.plc.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 7.2 |
| 2019 | 10.2 |
| 2018 | 10.0 |
| 2017 | 9.0 |
| 2016 | 8.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
