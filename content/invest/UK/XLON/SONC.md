---
title: "HIPGNOSIS SONGS FUND LD C SHS NPV (SONC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>HIPGNOSIS SONGS FUND LD C SHS NPV</td></tr>
    <tr><td>Symbol</td><td>SONC</td></tr>
    <tr><td>Web</td><td><a href="http://www.hipgnosissongs.com">www.hipgnosissongs.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 2.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
