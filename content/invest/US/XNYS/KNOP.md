---
title: " (KNOP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>KNOP</td></tr>
    <tr><td>Web</td><td><a href="https://www.knotoffshorepartners.com">www.knotoffshorepartners.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.04 |
| 2020 | 2.08 |
| 2019 | 2.08 |
| 2018 | 2.08 |
| 2017 | 2.08 |
| 2016 | 2.08 |
| 2015 | 2.03 |
| 2014 | 1.795 |
| 2013 | 0.752 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
