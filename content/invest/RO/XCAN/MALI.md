---
title: "COMALIM SA ARAD (MALI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>COMALIM SA ARAD</td></tr>
    <tr><td>Symbol</td><td>MALI</td></tr>
    <tr><td>Web</td><td><a href="https://www.comalim.ro">www.comalim.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.16 |
| 2019 | 0.63 |
| 2018 | 0.51 |
| 2017 | 0.27 |
| 2016 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
