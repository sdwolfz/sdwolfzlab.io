---
title: "MCCARTHY & STONE PLC ORD 8P (MCS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>MCCARTHY & STONE PLC ORD 8P</td></tr>
    <tr><td>Symbol</td><td>MCS</td></tr>
    <tr><td>Web</td><td><a href="http://www.mccarthyandstone.co.uk">www.mccarthyandstone.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
