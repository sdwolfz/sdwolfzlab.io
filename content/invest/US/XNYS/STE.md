---
title: "STERIS PLC (STE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>STERIS PLC</td></tr>
    <tr><td>Symbol</td><td>STE</td></tr>
    <tr><td>Web</td><td><a href="https://www.steris.com">www.steris.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.4 |
| 2020 | 1.54 |
| 2019 | 1.42 |
| 2018 | 1.3 |
| 2017 | 1.18 |
| 2016 | 1.06 |
| 2015 | 0.96 |
| 2014 | 0.88 |
| 2013 | 0.42 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
