---
title: "SPIRIT OF TEXAS BANCSHARES INC (STXB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SPIRIT OF TEXAS BANCSHARES INC</td></tr>
    <tr><td>Symbol</td><td>STXB</td></tr>
    <tr><td>Web</td><td><a href="https://www.sotb.com">www.sotb.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.18 |
| 2020 | 0.07 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
