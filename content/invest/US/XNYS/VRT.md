---
title: "VERTIV HOLDINGS CO (VRT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>VERTIV HOLDINGS CO</td></tr>
    <tr><td>Symbol</td><td>VRT</td></tr>
    <tr><td>Web</td><td><a href="https://www.vertiv.com">www.vertiv.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.01 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
