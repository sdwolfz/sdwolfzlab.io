---
title: "FIRST CITIZENS BANCSHARES INC NRTH (FCNCA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST CITIZENS BANCSHARES INC NRTH</td></tr>
    <tr><td>Symbol</td><td>FCNCA</td></tr>
    <tr><td>Web</td><td><a href="https://www.firstcitizens.com">www.firstcitizens.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.94 |
| 2020 | 1.67 |
| 2019 | 1.6 |
| 2018 | 1.45 |
| 2017 | 1.25 |
| 2016 | 1.2 |
| 2015 | 1.2 |
| 2014 | 1.2 |
| 2013 | 1.2 |
| 2012 | 1.2 |
| 2011 | 1.2 |
| 2010 | 1.2 |
| 2009 | 1.2 |
| 2008 | 1.1 |
| 2007 | 1.1 |
| 2006 | 1.1 |
| 2005 | 1.1 |
| 2004 | 1.1 |
| 2003 | 1.1 |
| 2002 | 1.0 |
| 2001 | 1.0 |
| 2000 | 1.0 |
| 1999 | 1.0 |
| 1998 | 1.0 |
| 1997 | 1.0 |
| 1996 | 0.925 |
| 1995 | 0.825 |
| 1994 | 0.725 |
| 1993 | 0.625 |
| 1992 | 0.525 |
| 1991 | 0.425 |
| 1990 | 0.3 |
| 1989 | 0.4 |
| 1988 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
