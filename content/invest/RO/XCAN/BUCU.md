---
title: "BUCUR OBOR SA BUCURESTI (BUCU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>BUCUR OBOR SA BUCURESTI</td></tr>
    <tr><td>Symbol</td><td>BUCU</td></tr>
    <tr><td>Web</td><td><a href="https://www.bucurobor.ro">www.bucurobor.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.367 |
| 2019 | 1.608 |
| 2018 | 1.483 |
| 2017 | 1.2398 |
| 2016 | 1.9127 |
| 2014 | 4.734095 |
| 2004 | 0.2888 |
| 2002 | 0.1464 |
| 2001 | 0.1132 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
