---
title: " (BBU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BBU</td></tr>
    <tr><td>Web</td><td><a href="https://www.bbu.brookfield.com">www.bbu.brookfield.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.252 |
| 2019 | 0.252 |
| 2018 | 0.252 |
| 2017 | 0.252 |
| 2016 | 0.133 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
