---
title: " (ADC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ADC</td></tr>
    <tr><td>Web</td><td><a href="https://www.agreerealty.com">www.agreerealty.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.838 |
| 2020 | 2.405 |
| 2019 | 2.28 |
| 2018 | 2.155 |
| 2017 | 2.025 |
| 2016 | 1.92 |
| 2015 | 1.845 |
| 2014 | 1.74 |
| 2013 | 1.23 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
