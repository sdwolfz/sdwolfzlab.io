---
title: "TIMKENSTEEL CORP (TMST)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TIMKENSTEEL CORP</td></tr>
    <tr><td>Symbol</td><td>TMST</td></tr>
    <tr><td>Web</td><td><a href="https://www.timkensteel.com">www.timkensteel.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 0.42 |
| 2014 | 0.28 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
