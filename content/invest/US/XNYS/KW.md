---
title: "KENNEDY-WILSON HOLDINGS INC (KW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>KENNEDY-WILSON HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>KW</td></tr>
    <tr><td>Web</td><td><a href="https://www.kennedywilson.com">www.kennedywilson.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.44 |
| 2020 | 0.88 |
| 2019 | 0.85 |
| 2018 | 0.78 |
| 2017 | 0.7 |
| 2016 | 0.56 |
| 2015 | 0.48 |
| 2014 | 0.36 |
| 2013 | 0.21 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
