---
title: "Ricardo (RCDO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Ricardo</td></tr>
    <tr><td>Symbol</td><td>RCDO</td></tr>
    <tr><td>Web</td><td><a href="https://www.ricardo.com">www.ricardo.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.24 |
| 2019 | 21.28 |
| 2018 | 20.46 |
| 2017 | 19.3 |
| 2016 | 18.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
