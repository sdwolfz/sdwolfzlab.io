---
title: "VES SA (VESY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>VES SA</td></tr>
    <tr><td>Symbol</td><td>VESY</td></tr>
    <tr><td>Web</td><td><a href="https://www.ves.ro">www.ves.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2008 | 0.0035 |
| 2002 | 0.0114 |
| 2001 | 0.0155 |
| 2000 | 0.008 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
