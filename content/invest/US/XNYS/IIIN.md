---
title: "INSTEEL INDUSTRIES INC (IIIN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>INSTEEL INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>IIIN</td></tr>
    <tr><td>Web</td><td><a href="https://www.insteel.com">www.insteel.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.03 |
| 2020 | 1.62 |
| 2019 | 0.12 |
| 2018 | 0.12 |
| 2017 | 1.12 |
| 2016 | 1.37 |
| 2015 | 1.12 |
| 2014 | 0.12 |
| 2013 | 0.09 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
