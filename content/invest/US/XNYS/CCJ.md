---
title: "CAMECO CORP (CCJ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CAMECO CORP</td></tr>
    <tr><td>Symbol</td><td>CCJ</td></tr>
    <tr><td>Web</td><td><a href="https://www.cameco.com">www.cameco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.06 |
| 2019 | 0.061 |
| 2018 | 0.061 |
| 2017 | 0.309 |
| 2016 | 0.3 |
| 2015 | 0.334 |
| 2014 | 0.4 |
| 2013 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
