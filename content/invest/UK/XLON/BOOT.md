---
title: "Henry Boot (BOOT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Henry Boot</td></tr>
    <tr><td>Symbol</td><td>BOOT</td></tr>
    <tr><td>Web</td><td><a href="https://www.henryboot.co.uk">www.henryboot.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 5.0 |
| 2018 | 9.0 |
| 2017 | 8.0 |
| 2016 | 7.0 |
| 2015 | 6.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
