---
title: "BRADY CORP (BRC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BRADY CORP</td></tr>
    <tr><td>Symbol</td><td>BRC</td></tr>
    <tr><td>Web</td><td><a href="https://www.bradycorp.com">www.bradycorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.44 |
| 2020 | 0.874 |
| 2019 | 0.857 |
| 2018 | 0.834 |
| 2017 | 0.822 |
| 2016 | 0.814 |
| 2015 | 0.803 |
| 2014 | 0.785 |
| 2013 | 0.385 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
