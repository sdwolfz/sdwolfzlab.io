---
title: "ART'S-WAY MANUFACTURING CO INC (ARTW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ART'S-WAY MANUFACTURING CO INC</td></tr>
    <tr><td>Symbol</td><td>ARTW</td></tr>
    <tr><td>Web</td><td><a href="https://www.artsway-mfg.com">www.artsway-mfg.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 0.05 |
| 2013 | 0.1 |
| 2012 | 0.1 |
| 2011 | 0.06 |
| 2010 | 0.06 |
| 2009 | 0.06 |
| 2008 | 0.06 |
| 2007 | 0.1 |
| 2006 | 0.05 |
| 2005 | 0.05 |
| 1992 | 0.1 |
| 1991 | 0.4 |
| 1990 | 0.4 |
| 1989 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
