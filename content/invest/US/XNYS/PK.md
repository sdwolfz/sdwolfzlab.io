---
title: " (PK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>PK</td></tr>
    <tr><td>Web</td><td><a href="https://www.pkhotelsandresorts.com">www.pkhotelsandresorts.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.45 |
| 2019 | 1.9 |
| 2018 | 2.74 |
| 2017 | 1.84 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
