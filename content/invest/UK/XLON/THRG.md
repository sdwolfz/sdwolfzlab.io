---
title: "Throgmorton Trust (THRG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Throgmorton Trust</td></tr>
    <tr><td>Symbol</td><td>THRG</td></tr>
    <tr><td>Web</td><td><a href="https://www.blackrock.co.uk">www.blackrock.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 10.2 |
| 2019 | 10.2 |
| 2018 | 10.0 |
| 2017 | 9.0 |
| 2016 | 7.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
