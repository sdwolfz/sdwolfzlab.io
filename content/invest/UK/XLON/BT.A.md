---
title: "BT (BT.A)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>BT</td></tr>
    <tr><td>Symbol</td><td>BT.A</td></tr>
    <tr><td>Web</td><td><a href="https://www.btplc.com">www.btplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 4.62 |
| 2019 | 15.4 |
| 2018 | 15.4 |
| 2017 | 15.4 |
| 2016 | 14.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
