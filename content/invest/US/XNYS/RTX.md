---
title: "RAYTHEON TECHNOLOGIES CORPORATION (RTX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>RAYTHEON TECHNOLOGIES CORPORATION</td></tr>
    <tr><td>Symbol</td><td>RTX</td></tr>
    <tr><td>Web</td><td><a href="https://www.rtx.com">www.rtx.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.985 |
| 2020 | 2.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
