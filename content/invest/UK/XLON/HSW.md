---
title: "Hostelworld (HSW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Hostelworld</td></tr>
    <tr><td>Symbol</td><td>HSW</td></tr>
    <tr><td>Web</td><td><a href="https://www.hostelworld.com">www.hostelworld.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 6.3 |
| 2018 | 13.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
