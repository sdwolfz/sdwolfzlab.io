---
title: "CEPROHART SA BRAILA (CPHA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>CEPROHART SA BRAILA</td></tr>
    <tr><td>Symbol</td><td>CPHA</td></tr>
    <tr><td>Web</td><td><a href="https://www.ceprohart.ro">www.ceprohart.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 0.0807 |
| 2013 | 0.0688 |
| 2012 | 0.0976 |
| 2011 | 0.067 |
| 2010 | 0.0413 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
