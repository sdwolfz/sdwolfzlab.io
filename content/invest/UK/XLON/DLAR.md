---
title: "De La Rue (DLAR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>De La Rue</td></tr>
    <tr><td>Symbol</td><td>DLAR</td></tr>
    <tr><td>Web</td><td><a href="https://www.delarue.com">www.delarue.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 14.82 |
| 2019 | 25.0 |
| 2018 | 25.0 |
| 2017 | 25.0 |
| 2016 | 25.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
