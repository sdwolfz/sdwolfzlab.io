---
title: " (PGRE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>PGRE</td></tr>
    <tr><td>Web</td><td><a href="https://www.paramount-group.com">www.paramount-group.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.07 |
| 2020 | 0.37 |
| 2019 | 0.4 |
| 2018 | 0.4 |
| 2017 | 0.38 |
| 2016 | 0.38 |
| 2015 | 0.419 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
