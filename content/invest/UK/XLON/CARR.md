---
title: "Carrs Group (CARR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Carrs Group</td></tr>
    <tr><td>Symbol</td><td>CARR</td></tr>
    <tr><td>Web</td><td><a href="https://www.carrsgroup-ir.com">www.carrsgroup-ir.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 4.75 |
| 2019 | 4.75 |
| 2018 | 4.5 |
| 2017 | 4.0 |
| 2016 | 3.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
