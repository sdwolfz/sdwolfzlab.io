---
title: "HOUGHTON MIFFLIN LLC (HMHC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HOUGHTON MIFFLIN LLC</td></tr>
    <tr><td>Symbol</td><td>HMHC</td></tr>
    <tr><td>Web</td><td><a href="https://www.hmhco.com">www.hmhco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
