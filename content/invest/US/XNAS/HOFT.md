---
title: "HOOKER FURNITURE CORP (HOFT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HOOKER FURNITURE CORP</td></tr>
    <tr><td>Symbol</td><td>HOFT</td></tr>
    <tr><td>Web</td><td><a href="https://investors.hookerfurniture.com">investors.hookerfurniture.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.18 |
| 2020 | 0.66 |
| 2019 | 0.61 |
| 2018 | 0.57 |
| 2017 | 0.5 |
| 2016 | 0.42 |
| 2015 | 0.4 |
| 2014 | 0.4 |
| 2013 | 0.4 |
| 2012 | 0.5 |
| 2011 | 0.4 |
| 2010 | 0.4 |
| 2009 | 0.4 |
| 2008 | 0.4 |
| 2007 | 0.4 |
| 2006 | 0.31 |
| 2005 | 0.28 |
| 2004 | 0.24 |
| 2003 | 0.43 |
| 2002 | 0.38 |
| 2001 | 0.27 |
| 2000 | 0.085 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
