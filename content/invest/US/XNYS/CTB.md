---
title: "COOPER TIRE & RUBBER CO (CTB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>COOPER TIRE & RUBBER CO</td></tr>
    <tr><td>Symbol</td><td>CTB</td></tr>
    <tr><td>Web</td><td><a href="https://www.coopertire.com">www.coopertire.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.105 |
| 2020 | 0.42 |
| 2019 | 0.42 |
| 2018 | 0.42 |
| 2017 | 0.42 |
| 2016 | 0.42 |
| 2015 | 0.42 |
| 2014 | 0.42 |
| 2013 | 0.21 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
