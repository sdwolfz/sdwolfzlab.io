---
title: "NEW YORK CITY REIT INC (NYC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NEW YORK CITY REIT INC</td></tr>
    <tr><td>Symbol</td><td>NYC</td></tr>
    <tr><td>Web</td><td><a href="https://www.newyorkcityreit.com">www.newyorkcityreit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.049 |
| 2014 | 1.573 |
| 2013 | 1.621 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
