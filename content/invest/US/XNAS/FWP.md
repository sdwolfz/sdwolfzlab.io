---
title: " (FWP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>FWP</td></tr>
    <tr><td>Web</td><td><a href="https://www.forward-pharma.com">www.forward-pharma.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 23.123 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
