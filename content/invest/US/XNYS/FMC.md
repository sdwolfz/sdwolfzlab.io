---
title: "FMC CORP (FMC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FMC CORP</td></tr>
    <tr><td>Symbol</td><td>FMC</td></tr>
    <tr><td>Web</td><td><a href="https://www.fmc.com">www.fmc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.96 |
| 2020 | 1.8 |
| 2019 | 1.64 |
| 2018 | 0.895 |
| 2017 | 0.66 |
| 2016 | 0.66 |
| 2015 | 0.66 |
| 2014 | 0.6 |
| 2013 | 0.27 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
