---
title: "Jpmorgan Clav (JCH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Jpmorgan Clav</td></tr>
    <tr><td>Symbol</td><td>JCH</td></tr>
    <tr><td>Web</td><td><a href="https://www.jpmclaverhouse.co.uk">www.jpmclaverhouse.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 29.0 |
| 2018 | 27.5 |
| 2017 | 26.0 |
| 2016 | 23.0 |
| 2015 | 21.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
