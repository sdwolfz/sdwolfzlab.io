---
title: " (HNP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>HNP</td></tr>
    <tr><td>Web</td><td><a href="https://www.hpi.com.cn">www.hpi.com.cn</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.678 |
| 2019 | 0.517 |
| 2018 | 0.53 |
| 2017 | 1.489 |
| 2016 | 2.583 |
| 2015 | 2.201 |
| 2014 | 2.211 |
| 2013 | 1.185 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
