---
title: "Motorpoint (MOTR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Motorpoint</td></tr>
    <tr><td>Symbol</td><td>MOTR</td></tr>
    <tr><td>Web</td><td><a href="https://www.motorpoint.co.uk">www.motorpoint.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.6 |
| 2019 | 7.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
