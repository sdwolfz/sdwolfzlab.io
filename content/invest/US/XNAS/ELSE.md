---
title: "ELECTRO-SENSORS INC (ELSE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ELECTRO-SENSORS INC</td></tr>
    <tr><td>Symbol</td><td>ELSE</td></tr>
    <tr><td>Web</td><td><a href="https://www.electro-sensors.com">www.electro-sensors.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2013 | 0.08 |
| 2012 | 0.16 |
| 2011 | 0.16 |
| 2010 | 0.16 |
| 2009 | 0.16 |
| 2008 | 0.16 |
| 2007 | 0.16 |
| 2006 | 1.16 |
| 2005 | 0.16 |
| 2004 | 0.16 |
| 2003 | 0.13 |
| 2002 | 0.12 |
| 2001 | 0.12 |
| 2000 | 0.12 |
| 1999 | 0.12 |
| 1998 | 0.12 |
| 1997 | 0.12 |
| 1996 | 0.12 |
| 1995 | 0.6 |
| 1994 | 0.1 |
| 1993 | 0.1 |
| 1992 | 0.1 |
| 1991 | 0.1 |
| 1990 | 0.1 |
| 1989 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
