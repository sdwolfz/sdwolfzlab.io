---
title: "ECOLAB INC (ECL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ECOLAB INC</td></tr>
    <tr><td>Symbol</td><td>ECL</td></tr>
    <tr><td>Web</td><td><a href="https://www.ecolab.com">www.ecolab.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.48 |
| 2020 | 1.89 |
| 2019 | 1.85 |
| 2018 | 1.69 |
| 2017 | 1.52 |
| 2016 | 1.42 |
| 2015 | 1.34 |
| 2014 | 1.155 |
| 2013 | 0.505 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
