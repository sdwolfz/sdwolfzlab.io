---
title: "BUSINESS FIRST BANCSHARES INC (BFST)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BUSINESS FIRST BANCSHARES INC</td></tr>
    <tr><td>Symbol</td><td>BFST</td></tr>
    <tr><td>Web</td><td><a href="https://www.b1bank.com">www.b1bank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.22 |
| 2020 | 0.4 |
| 2019 | 0.38 |
| 2018 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
