---
title: " (PEAK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>PEAK</td></tr>
    <tr><td>Web</td><td><a href="https://www.healthpeak.com">www.healthpeak.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.6 |
| 2020 | 1.48 |
| 2019 | 0.37 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
