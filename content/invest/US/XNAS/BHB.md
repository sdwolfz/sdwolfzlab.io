---
title: "BAR HARBOUR BANKSHARES (BHB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BAR HARBOUR BANKSHARES</td></tr>
    <tr><td>Symbol</td><td>BHB</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.46 |
| 2020 | 0.88 |
| 2019 | 0.86 |
| 2018 | 0.787 |
| 2017 | 0.84 |
| 2016 | 1.09 |
| 2015 | 1.01 |
| 2014 | 1.013 |
| 2013 | 1.25 |
| 2012 | 1.17 |
| 2011 | 1.095 |
| 2010 | 1.045 |
| 2009 | 1.04 |
| 2008 | 1.02 |
| 2007 | 0.955 |
| 2006 | 0.905 |
| 2005 | 0.84 |
| 2004 | 0.8 |
| 2003 | 0.76 |
| 2002 | 0.76 |
| 2001 | 0.76 |
| 2000 | 0.57 |
| 1999 | 0.19 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
