---
title: "SOUTHERN MISSOURI BANCORP (SMBC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SOUTHERN MISSOURI BANCORP</td></tr>
    <tr><td>Symbol</td><td>SMBC</td></tr>
    <tr><td>Web</td><td><a href="https://www.bankwithsouthern.com">www.bankwithsouthern.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.32 |
| 2020 | 0.6 |
| 2019 | 0.56 |
| 2018 | 0.48 |
| 2017 | 0.42 |
| 2016 | 0.38 |
| 2015 | 0.35 |
| 2014 | 0.66 |
| 2013 | 0.62 |
| 2012 | 0.54 |
| 2011 | 0.48 |
| 2010 | 0.48 |
| 2009 | 0.48 |
| 2008 | 0.44 |
| 2007 | 0.38 |
| 2006 | 0.36 |
| 2005 | 0.36 |
| 2004 | 0.36 |
| 2003 | 0.55 |
| 2002 | 0.53 |
| 2001 | 0.5 |
| 2000 | 0.5 |
| 1999 | 0.5 |
| 1998 | 0.5 |
| 1997 | 0.5 |
| 1996 | 0.5 |
| 1995 | 0.375 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
