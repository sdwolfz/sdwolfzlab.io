---
title: "PREFAB SA (PREH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>PREFAB SA</td></tr>
    <tr><td>Symbol</td><td>PREH</td></tr>
    <tr><td>Web</td><td><a href="https://www.prefab.ro">www.prefab.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.09 |
| 2018 | 0.019 |
| 2017 | 0.0134 |
| 2016 | 0.011629 |
| 2015 | 0.01337 |
| 2008 | 0.0923 |
| 2002 | 0.2796 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
