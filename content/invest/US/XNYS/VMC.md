---
title: "VULCAN MATERIALS CO (VMC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>VULCAN MATERIALS CO</td></tr>
    <tr><td>Symbol</td><td>VMC</td></tr>
    <tr><td>Web</td><td><a href="https://www.vulcanmaterials.com">www.vulcanmaterials.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.37 |
| 2020 | 1.36 |
| 2019 | 1.24 |
| 2018 | 1.12 |
| 2017 | 1.0 |
| 2016 | 0.8 |
| 2015 | 0.4 |
| 2014 | 0.22 |
| 2013 | 0.02 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
