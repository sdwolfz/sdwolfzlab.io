---
title: "WALMART INC (WMT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>WALMART INC</td></tr>
    <tr><td>Symbol</td><td>WMT</td></tr>
    <tr><td>Web</td><td><a href="https://stock.walmart.com">stock.walmart.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.65 |
| 2020 | 2.16 |
| 2019 | 2.12 |
| 2018 | 2.08 |
| 2017 | 2.04 |
| 2016 | 2.0 |
| 2015 | 1.96 |
| 2014 | 1.92 |
| 2013 | 0.47 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
