---
title: "Jupiter Fund Management (JUP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Jupiter Fund Management</td></tr>
    <tr><td>Symbol</td><td>JUP</td></tr>
    <tr><td>Web</td><td><a href="https://www.jupiteram.com">www.jupiteram.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 20.1 |
| 2019 | 17.1 |
| 2018 | 28.5 |
| 2017 | 32.6 |
| 2016 | 27.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
