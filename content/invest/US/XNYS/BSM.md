---
title: " (BSM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BSM</td></tr>
    <tr><td>Web</td><td><a href="https://www.blackstoneminerals.com">www.blackstoneminerals.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.35 |
| 2020 | 0.68 |
| 2019 | 1.48 |
| 2018 | 1.334 |
| 2017 | 1.2 |
| 2016 | 1.098 |
| 2015 | 0.424 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
