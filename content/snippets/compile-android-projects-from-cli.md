---
title: "Compile Android projects from CLI"
date: 2020-06-08T17:21:28Z
type: posts
slug: compile-android-projects-from-cli
series: ["Android"]
tags: ["android", "java", "bash", "linux", "CI", "snap"]
---

1. Install java:
```sh
sudo pacman -S jdk8-openjdk jre8-openjdk jre8-openjdk-headless
```

2. Install Android SDK:
```sh
sudo pacman -S snapd
sudo snap install androidsdk
```

3. Configure your SDK location in your `~/.bash_profile` or `~/.bashrc`:
```bash
export ANDROID_SDK_ROOT=~/snap/androidsdk/current/AndroidSDK/
```

4. Install the platform tools for your target android version:
```sh
androidsdk "platform-tools" "platforms;android-28"
```

5. Compile the project. This will install all dependencies, make sure to accept
   licenses when prompted.

```sh
./gradlew assembleDebug
```

6. If you haven't done already, then
   [Enable 'Developer options' on your phone](/snippets/enable-developer-options-on-your-phone/)

7. Connect your phone and install the debug APK
```sh
adb install ./app/build/outputs/apk/debug/app-debug.apk
```
