---
title: "PageGroup (PAGE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>PageGroup</td></tr>
    <tr><td>Symbol</td><td>PAGE</td></tr>
    <tr><td>Web</td><td><a href="https://www.page.com">www.page.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 26.43 |
| 2018 | 25.83 |
| 2017 | 25.23 |
| 2016 | 18.44 |
| 2015 | 27.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
