---
title: "Display key presses on screen"
date: 2020-12-05T11:50:10Z
type: posts
slug: display-key-presses-on-screen
tags: ["linux", "showcase"]
---

Useful for presenting at conferences or webinars.

## Install

```sh
sudo pacman -S screenkey
```

## Run

Start it with:

```sh
screenkey -s small -t 0.5 --opacity 0.3 --compr-cnt 2
```

Stop it by killing the process:

```sh
killall screenkey
```

## Key Bindings

Better have it as a system wide key binding for fast access:

| Key                 | Description      |
|---------------------|------------------|
| `Super + K`         | Start screenkey. |
| `Super + Shift + K` | Stop screenkey.  |

Set them with:

```sh
xfconf-query --reset                 \
  --channel xfce4-keyboard-shortcuts \
  --property /commands/custom/'<Super>k'

xfconf-query --create                    \
  --channel xfce4-keyboard-shortcuts     \
  --property /commands/custom/'<Super>k' \
  --type string                          \
  --set 'screenkey -s small -t 0.5 --opacity 0.3 --compr-cnt 2'

xfconf-query --reset                 \
  --channel xfce4-keyboard-shortcuts \
  --property /commands/custom/'<Super><Shift>K'

xfconf-query --create                           \
  --channel xfce4-keyboard-shortcuts            \
  --property /commands/custom/'<Super><Shift>K' \
  --type string                                 \
  --set 'killall screenkey'
```

Add them to any automation script you have for maximum productivity!

> **WARNING!** Don't forget to turn it off when typing passwords...
