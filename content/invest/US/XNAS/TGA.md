---
title: "TRANSGLOBE ENERGY CORP (TGA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TRANSGLOBE ENERGY CORP</td></tr>
    <tr><td>Symbol</td><td>TGA</td></tr>
    <tr><td>Web</td><td><a href="https://www.trans-globe.com">www.trans-globe.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.07 |
| 2018 | 0.035 |
| 2015 | 0.175 |
| 2014 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
