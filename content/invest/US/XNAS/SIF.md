---
title: "SIFCO INDUSTRIES INC (SIF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SIFCO INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>SIF</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 0.2 |
| 2013 | 0.2 |
| 2012 | 0.2 |
| 2011 | 0.2 |
| 2010 | 0.15 |
| 2009 | 0.1 |
| 2000 | 0.2 |
| 1999 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
