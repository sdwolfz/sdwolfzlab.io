---
title: "FOLEY TRASIMENE ACQUISITION CORP II (BFT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FOLEY TRASIMENE ACQUISITION CORP II</td></tr>
    <tr><td>Symbol</td><td>BFT</td></tr>
    <tr><td>Web</td><td><a href="http://www.foleytrasimene2.com">www.foleytrasimene2.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
