---
title: "SOUTHWEST GAS HOLDINGS INC (SWX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SOUTHWEST GAS HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>SWX</td></tr>
    <tr><td>Web</td><td><a href="https://www.swgasholdings.com">www.swgasholdings.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.165 |
| 2020 | 2.255 |
| 2019 | 2.155 |
| 2018 | 2.055 |
| 2017 | 1.935 |
| 2016 | 1.755 |
| 2015 | 1.58 |
| 2014 | 1.425 |
| 2013 | 0.66 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
