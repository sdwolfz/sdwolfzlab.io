---
title: " (FPI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>FPI</td></tr>
    <tr><td>Web</td><td><a href="https://www.farmlandpartners.com">www.farmlandpartners.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.05 |
| 2020 | 0.2 |
| 2019 | 0.2 |
| 2018 | 0.354 |
| 2017 | 0.508 |
| 2016 | 0.508 |
| 2015 | 0.497 |
| 2014 | 0.326 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
