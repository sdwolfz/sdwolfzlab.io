---
title: "MORGAN STANLEY (MS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MORGAN STANLEY</td></tr>
    <tr><td>Symbol</td><td>MS</td></tr>
    <tr><td>Web</td><td><a href="https://www.morganstanley.com">www.morganstanley.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.7 |
| 2020 | 1.4 |
| 2019 | 1.3 |
| 2018 | 1.1 |
| 2017 | 0.9 |
| 2016 | 0.7 |
| 2015 | 0.55 |
| 2014 | 0.35 |
| 2013 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
