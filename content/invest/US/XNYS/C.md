---
title: "CITIGROUP INC (C)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CITIGROUP INC</td></tr>
    <tr><td>Symbol</td><td>C</td></tr>
    <tr><td>Web</td><td><a href="https://www.citigroup.com">www.citigroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.02 |
| 2020 | 2.04 |
| 2019 | 1.92 |
| 2018 | 1.54 |
| 2017 | 0.96 |
| 2016 | 0.42 |
| 2015 | 0.16 |
| 2014 | 0.04 |
| 2013 | 0.02 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
