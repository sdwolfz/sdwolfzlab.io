---
title: "SERVICE CORPORATION INTERNATIONAL (SCI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SERVICE CORPORATION INTERNATIONAL</td></tr>
    <tr><td>Symbol</td><td>SCI</td></tr>
    <tr><td>Web</td><td><a href="https://www.sci-corp.com">www.sci-corp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.21 |
| 2020 | 0.78 |
| 2019 | 0.72 |
| 2018 | 0.68 |
| 2017 | 0.58 |
| 2016 | 0.51 |
| 2015 | 0.44 |
| 2014 | 0.34 |
| 2013 | 0.14 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
