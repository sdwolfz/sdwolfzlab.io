---
title: "EVOLUTION PETROLEUM CORPORATION (EPM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>EVOLUTION PETROLEUM CORPORATION</td></tr>
    <tr><td>Symbol</td><td>EPM</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.03 |
| 2020 | 0.175 |
| 2019 | 0.4 |
| 2018 | 0.4 |
| 2017 | 0.29 |
| 2016 | 0.215 |
| 2015 | 0.2 |
| 2014 | 0.4 |
| 2013 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
