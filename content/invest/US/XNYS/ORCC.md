---
title: "OWL ROCK CAPITAL CORPORATION (ORCC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>OWL ROCK CAPITAL CORPORATION</td></tr>
    <tr><td>Symbol</td><td>ORCC</td></tr>
    <tr><td>Web</td><td><a href="https://www.owlrockcapitalcorporation.com">www.owlrockcapitalcorporation.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.62 |
| 2020 | 1.56 |
| 2019 | 0.68 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
