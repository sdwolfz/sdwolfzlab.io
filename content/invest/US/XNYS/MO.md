---
title: "ALTRIA GROUP INC (MO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ALTRIA GROUP INC</td></tr>
    <tr><td>Symbol</td><td>MO</td></tr>
    <tr><td>Web</td><td><a href="https://www.altria.com">www.altria.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.86 |
| 2020 | 3.4 |
| 2019 | 3.28 |
| 2018 | 3.0 |
| 2017 | 2.54 |
| 2016 | 2.35 |
| 2015 | 2.17 |
| 2014 | 2.0 |
| 2013 | 1.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
