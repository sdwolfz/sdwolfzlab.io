---
title: "NORTHRIM BANCORP INC (NRIM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NORTHRIM BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>NRIM</td></tr>
    <tr><td>Web</td><td><a href="https://www.northrim.com">www.northrim.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.37 |
| 2020 | 1.38 |
| 2019 | 1.26 |
| 2018 | 1.02 |
| 2017 | 0.86 |
| 2016 | 0.78 |
| 2015 | 0.74 |
| 2014 | 0.7 |
| 2013 | 0.64 |
| 2012 | 0.56 |
| 2011 | 0.5 |
| 2010 | 0.44 |
| 2009 | 0.4 |
| 2008 | 0.66 |
| 2007 | 0.575 |
| 2006 | 0.47 |
| 2005 | 0.425 |
| 2004 | 0.38 |
| 2003 | 0.335 |
| 2002 | 0.2 |
| 2001 | 0.2 |
| 2000 | 0.2 |
| 1999 | 0.2 |
| 1998 | 0.2 |
| 1997 | 0.2 |
| 1996 | 0.2 |
| 1995 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
