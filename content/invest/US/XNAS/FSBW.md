---
title: "FS BANCORP INC (FSBW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FS BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>FSBW</td></tr>
    <tr><td>Web</td><td><a href="https://www.fsbwa.com">www.fsbwa.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.53 |
| 2020 | 0.84 |
| 2019 | 0.65 |
| 2018 | 0.53 |
| 2017 | 0.43 |
| 2016 | 0.37 |
| 2015 | 0.27 |
| 2014 | 0.23 |
| 2013 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
