---
title: "Greencoat UK Wind (UKW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Greencoat UK Wind</td></tr>
    <tr><td>Symbol</td><td>UKW</td></tr>
    <tr><td>Web</td><td><a href="https://www.greencoat-ukwind.com">www.greencoat-ukwind.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 7.1 |
| 2019 | 6.69 |
| 2018 | 6.76 |
| 2017 | 6.49 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
