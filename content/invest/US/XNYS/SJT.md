---
title: " (SJT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SJT</td></tr>
    <tr><td>Web</td><td><a href="https://www.sjbrt.com">www.sjbrt.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.263 |
| 2020 | 0.158 |
| 2019 | 0.174 |
| 2018 | 0.386 |
| 2017 | 0.84 |
| 2016 | 0.299 |
| 2015 | 0.366 |
| 2014 | 1.285 |
| 2013 | 0.592 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
