---
title: "SPX FLOW INC (FLOW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SPX FLOW INC</td></tr>
    <tr><td>Symbol</td><td>FLOW</td></tr>
    <tr><td>Web</td><td><a href="https://www.spxflow.com">www.spxflow.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.09 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
