---
title: "UPRUC CTR SA FAGARAS (UPRR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>UPRUC CTR SA FAGARAS</td></tr>
    <tr><td>Symbol</td><td>UPRR</td></tr>
    <tr><td>Web</td><td><a href="https://www.uprucctr.com">www.uprucctr.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2012 | 0.6784 |
| 2001 | 1.616 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
