---
title: "COMMUNITY FINANCIAL CORPORATION MD (TCFC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COMMUNITY FINANCIAL CORPORATION MD</td></tr>
    <tr><td>Symbol</td><td>TCFC</td></tr>
    <tr><td>Web</td><td><a href="https://www.cbtc.com">www.cbtc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.275 |
| 2020 | 0.5 |
| 2019 | 0.5 |
| 2018 | 0.4 |
| 2017 | 0.4 |
| 2016 | 0.4 |
| 2015 | 0.3 |
| 2014 | 0.4 |
| 2013 | 0.5 |
| 2012 | 0.4 |
| 2011 | 0.4 |
| 2010 | 0.4 |
| 2009 | 0.4 |
| 2008 | 0.4 |
| 2007 | 0.4 |
| 2006 | 0.55 |
| 2005 | 0.8 |
| 2004 | 0.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
