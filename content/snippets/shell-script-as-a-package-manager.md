---
title: "Shell scripts as a package manager"
date: 2020-10-30T21:31:42Z
type: posts
slug: shell-scripts-as-a-package-manager
tags: ["shell", "linux", "development", "appimage"]
---

Sometimes code is not available from package managers, just like in the stone
age. Until those maintainers wake up to the basic modern programming practices
of our glorious present, you can do this:

1. Create a `scripts/sspm.sh` file with the code from this repository:

* https://gitlab.com/sdwolfz/sspm/-/blob/master/sspm.sh

2. Create a package specific file `scripts/download/footool.sh` and use one of
   those functions:

```sh
#!/usr/bin/env sh

set -ex

. ./scripts/sspm.sh

download_from_git                          \
  footool                                  \
  master                                   \
  6f6944bee215d391d7c80c529028003ba3b838da \
  https://gitlab.com/sdwolfz/footool.git
```

3. Create an equivalent build file `scripts/build/footool.sh` to compile it:

```sh
#!/usr/bin/env sh
set -ex

APPDIR="$PWD"/lib

cd vendor/footool

./autogen.sh
./configure \
  --prefix="$APPDIR" \
  --enable-bar=yes

make -j`nproc`
make install -j`nproc`
```

4. Create a `Makefile` for easy execution:

```Makefile
.DEFAULT_GOAL = all

# ------------------------------------------------------------------------------
# Package Management

.PHONY: all
all: download clean build

# ------------------------------------------------------------------------------
# Build

.PHONY: build
build: \
	build/footool

.PHONY: build/footool
build/footool:
	@sh scripts/build/footool.sh

# ------------------------------------------------------------------------------
# Clean

.PHONY: clean
clean: \
	clean/footool

.PHONY: clean/footool
clean/footool:
	@rm -f lib/footool

# ------------------------------------------------------------------------------
# Download

.PHONY: download
download: \
	download/footool

.PHONY: download/footool
download/footool:
	@sh scripts/download/footool.sh
```

5. Add as many other files and `make` goals as you want, each for every
   independent package you need.

6. Run it with `make all` and enjoy the benefits of the modern package
   manager you were forced to write yourself just now.
