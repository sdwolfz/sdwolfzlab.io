---
title: "FONDUL PROPRIETATEA (FP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>FONDUL PROPRIETATEA</td></tr>
    <tr><td>Symbol</td><td>FP</td></tr>
    <tr><td>Web</td><td><a href="https://www.fondulproprietatea.ro">www.fondulproprietatea.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.072 |
| 2019 | 0.0642 |
| 2018 | 0.0903 |
| 2017 | 0.0678 |
| 2012 | 0.04089 |
| 2011 | 0.0385 |
| 2010 | 0.0314 |
| 2009 | 0.0816 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
