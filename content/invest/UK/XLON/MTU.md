---
title: "Montanaro Uk (MTU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Montanaro Uk</td></tr>
    <tr><td>Symbol</td><td>MTU</td></tr>
    <tr><td>Web</td><td><a href="https://montanaro.co.uk">montanaro.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 5.3 |
| 2019 | 3.89 |
| 2018 | 2.2 |
| 2017 | 2.1 |
| 2016 | 2.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
