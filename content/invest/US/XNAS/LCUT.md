---
title: "LIFETIME BRANDS IN (LCUT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LIFETIME BRANDS IN</td></tr>
    <tr><td>Symbol</td><td>LCUT</td></tr>
    <tr><td>Web</td><td><a href="https://www.lifetimebrands.com">www.lifetimebrands.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.085 |
| 2020 | 0.17 |
| 2019 | 0.17 |
| 2018 | 0.17 |
| 2017 | 0.17 |
| 2016 | 0.17 |
| 2015 | 0.155 |
| 2014 | 0.15 |
| 2013 | 0.119 |
| 2012 | 0.1 |
| 2011 | 0.075 |
| 2008 | 0.25 |
| 2007 | 0.25 |
| 2006 | 0.25 |
| 2005 | 0.25 |
| 2004 | 0.25 |
| 2003 | 0.25 |
| 2002 | 0.25 |
| 2001 | 0.25 |
| 2000 | 0.25 |
| 1999 | 0.25 |
| 1998 | 0.25 |
| 1997 | 0.063 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
