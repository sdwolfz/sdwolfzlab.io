---
title: "AIR PRODUCTS & CHEMICALS INC (APD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AIR PRODUCTS & CHEMICALS INC</td></tr>
    <tr><td>Symbol</td><td>APD</td></tr>
    <tr><td>Web</td><td><a href="https://www.airproducts.com">www.airproducts.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.5 |
| 2020 | 5.36 |
| 2019 | 4.64 |
| 2018 | 4.4 |
| 2017 | 3.8 |
| 2016 | 4.3 |
| 2015 | 3.24 |
| 2014 | 3.08 |
| 2013 | 2.13 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
