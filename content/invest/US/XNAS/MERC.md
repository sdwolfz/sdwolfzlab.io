---
title: "MERCER INTERNATIONAL INC (MERC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MERCER INTERNATIONAL INC</td></tr>
    <tr><td>Symbol</td><td>MERC</td></tr>
    <tr><td>Web</td><td><a href="https://www.mercerint.com">www.mercerint.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.13 |
| 2020 | 0.333 |
| 2019 | 0.538 |
| 2018 | 0.5 |
| 2017 | 0.47 |
| 2016 | 0.46 |
| 2015 | 0.23 |
| 1999 | 0.05 |
| 1998 | 0.04 |
| 1997 | 0.03 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
