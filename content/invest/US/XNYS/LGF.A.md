---
title: "LIONS GATE ENTERTAINMENT CORP (LGF.A)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>LIONS GATE ENTERTAINMENT CORP</td></tr>
    <tr><td>Symbol</td><td>LGF.A</td></tr>
    <tr><td>Web</td><td><a href="https://www.lionsgate.com">www.lionsgate.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.27 |
| 2016 | 0.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
