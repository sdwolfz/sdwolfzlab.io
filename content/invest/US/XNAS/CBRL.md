---
title: "CRACKER BARREL OLD COUNTRY STORE (CBRL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CRACKER BARREL OLD COUNTRY STORE</td></tr>
    <tr><td>Symbol</td><td>CBRL</td></tr>
    <tr><td>Web</td><td><a href="https://www.crackerbarrel.com">www.crackerbarrel.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.6 |
| 2019 | 8.1 |
| 2018 | 8.65 |
| 2017 | 8.2 |
| 2016 | 7.75 |
| 2015 | 7.2 |
| 2014 | 3.5 |
| 2013 | 2.5 |
| 2012 | 1.4 |
| 2011 | 0.91 |
| 2010 | 0.82 |
| 2009 | 0.8 |
| 2008 | 0.74 |
| 2007 | 0.6 |
| 2006 | 0.53 |
| 2005 | 0.49 |
| 2004 | 0.45 |
| 2003 | 0.11 |
| 2002 | 0.02 |
| 2001 | 0.02 |
| 2000 | 0.02 |
| 1999 | 0.02 |
| 1998 | 0.02 |
| 1997 | 0.02 |
| 1996 | 0.02 |
| 1995 | 0.02 |
| 1994 | 0.02 |
| 1993 | 0.02 |
| 1992 | 0.028 |
| 1991 | 0.026 |
| 1990 | 0.055 |
| 1989 | 0.07 |
| 1988 | 0.025 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
