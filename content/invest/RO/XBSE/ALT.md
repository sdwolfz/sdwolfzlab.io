---
title: "ALTUR S.A. (ALT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>ALTUR S.A.</td></tr>
    <tr><td>Symbol</td><td>ALT</td></tr>
    <tr><td>Web</td><td><a href="https://www.altursa.ro">www.altursa.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2004 | 0.0237 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
