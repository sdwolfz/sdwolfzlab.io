---
title: "EMERALD HOLDING INC (EEX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>EMERALD HOLDING INC</td></tr>
    <tr><td>Symbol</td><td>EEX</td></tr>
    <tr><td>Web</td><td><a href="https://www.emeraldexpositions.com">www.emeraldexpositions.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.075 |
| 2019 | 0.297 |
| 2018 | 0.286 |
| 2017 | 0.21 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
