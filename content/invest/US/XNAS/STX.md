---
title: "SEAGATE TECHNOLOGY PLC (STX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SEAGATE TECHNOLOGY PLC</td></tr>
    <tr><td>Symbol</td><td>STX</td></tr>
    <tr><td>Web</td><td><a href="https://www.seagate.com">www.seagate.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.34 |
| 2020 | 2.62 |
| 2019 | 2.54 |
| 2018 | 2.52 |
| 2017 | 2.52 |
| 2016 | 2.52 |
| 2015 | 2.25 |
| 2014 | 1.83 |
| 2013 | 1.19 |
| 2012 | 1.52 |
| 2011 | 0.54 |
| 2009 | 0.03 |
| 2008 | 0.46 |
| 2007 | 0.4 |
| 2006 | 0.34 |
| 2005 | 0.3 |
| 2004 | 0.24 |
| 2003 | 0.14 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
