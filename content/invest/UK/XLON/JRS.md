---
title: "Jpmorgan Rus (JRS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Jpmorgan Rus</td></tr>
    <tr><td>Symbol</td><td>JRS</td></tr>
    <tr><td>Web</td><td><a href="https://www.jpmfrussian.com">www.jpmfrussian.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 35.0 |
| 2019 | 35.0 |
| 2018 | 26.0 |
| 2017 | 21.0 |
| 2016 | 14.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
