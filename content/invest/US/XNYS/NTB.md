---
title: "BANK NT BUTTERFIELD (NTB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BANK NT BUTTERFIELD</td></tr>
    <tr><td>Symbol</td><td>NTB</td></tr>
    <tr><td>Web</td><td><a href="https://www.butterfieldgroup.com">www.butterfieldgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.88 |
| 2020 | 0.88 |
| 2018 | 0.38 |
| 2017 | 1.28 |
| 2016 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
