---
title: "Fuller Smith & Turner (FSTA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Fuller Smith & Turner</td></tr>
    <tr><td>Symbol</td><td>FSTA</td></tr>
    <tr><td>Web</td><td><a href="https://www.fullers.co.uk">www.fullers.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 132.8 |
| 2019 | 20.15 |
| 2019 | 20.15 |
| 2018 | 19.55 |
| 2017 | 18.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
