---
title: " (HASI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>HASI</td></tr>
    <tr><td>Web</td><td><a href="https://www.hannonarmstrong.com">www.hannonarmstrong.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.7 |
| 2020 | 1.36 |
| 2019 | 1.34 |
| 2018 | 1.32 |
| 2017 | 1.32 |
| 2016 | 1.23 |
| 2015 | 1.08 |
| 2014 | 0.92 |
| 2013 | 0.42 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
