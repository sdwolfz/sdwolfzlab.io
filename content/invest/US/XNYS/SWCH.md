---
title: "SWITCH INC (SWCH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SWITCH INC</td></tr>
    <tr><td>Symbol</td><td>SWCH</td></tr>
    <tr><td>Web</td><td><a href="https://www.switch.com">www.switch.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.05 |
| 2020 | 0.158 |
| 2019 | 0.116 |
| 2018 | 0.06 |
| 2017 | 0.014 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
