---
title: "PICO HOLDING INC (PICO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PICO HOLDING INC</td></tr>
    <tr><td>Symbol</td><td>PICO</td></tr>
    <tr><td>Web</td><td><a href="http://www.picoholdings.com">www.picoholdings.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 5.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
