---
title: "Spectris (SXS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Spectris</td></tr>
    <tr><td>Symbol</td><td>SXS</td></tr>
    <tr><td>Web</td><td><a href="https://www.spectris.com">www.spectris.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 65.1 |
| 2018 | 61.0 |
| 2017 | 53.0 |
| 2016 | 50.2 |
| 2015 | 47.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
