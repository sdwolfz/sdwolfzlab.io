---
title: "ADAMS RESOURCES & ENERGY INC (AE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ADAMS RESOURCES & ENERGY INC</td></tr>
    <tr><td>Symbol</td><td>AE</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.48 |
| 2020 | 0.96 |
| 2019 | 0.94 |
| 2018 | 0.88 |
| 2017 | 0.88 |
| 2016 | 0.88 |
| 2015 | 0.88 |
| 2014 | 0.88 |
| 2013 | 0.66 |
| 2012 | 0.62 |
| 2011 | 0.57 |
| 2010 | 0.54 |
| 2009 | 0.5 |
| 2008 | 0.5 |
| 2007 | 0.47 |
| 2006 | 0.42 |
| 2005 | 0.37 |
| 2004 | 0.3 |
| 2003 | 0.23 |
| 2002 | 0.13 |
| 2001 | 0.13 |
| 2000 | 0.13 |
| 1999 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
