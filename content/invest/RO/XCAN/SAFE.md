---
title: "SAFETECH INNOVATIONS (SAFE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>SAFETECH INNOVATIONS</td></tr>
    <tr><td>Symbol</td><td>SAFE</td></tr>
    <tr><td>Web</td><td><a href="https://www.safetech.ro">www.safetech.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.14 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
