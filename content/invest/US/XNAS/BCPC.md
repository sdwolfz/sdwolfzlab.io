---
title: "BALCHEM CORP (BCPC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BALCHEM CORP</td></tr>
    <tr><td>Symbol</td><td>BCPC</td></tr>
    <tr><td>Web</td><td><a href="https://www.balchem.com">www.balchem.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.58 |
| 2019 | 0.52 |
| 2018 | 0.47 |
| 2017 | 0.42 |
| 2016 | 0.38 |
| 2015 | 0.34 |
| 2014 | 0.3 |
| 2013 | 0.26 |
| 2012 | 0.22 |
| 2011 | 0.18 |
| 2010 | 0.15 |
| 2009 | 0.11 |
| 2008 | 0.11 |
| 2007 | 0.11 |
| 2006 | 0.09 |
| 2005 | 0.09 |
| 2004 | 0.09 |
| 2003 | 0.08 |
| 2002 | 0.08 |
| 2001 | 0.065 |
| 2000 | 0.06 |
| 1999 | 0.05 |
| 1994 | 0.028 |
| 1993 | 0.034 |
| 1992 | 0.034 |
| 1991 | 0.045 |
| 1990 | 0.04 |
| 1989 | 0.03 |
| 1988 | 0.03 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
