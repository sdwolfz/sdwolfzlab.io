---
title: " (PBT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>PBT</td></tr>
    <tr><td>Web</td><td><a href="https://www.pbt-permian.com">www.pbt-permian.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.057 |
| 2020 | 0.235 |
| 2019 | 0.418 |
| 2018 | 0.661 |
| 2017 | 0.629 |
| 2016 | 0.416 |
| 2015 | 0.345 |
| 2014 | 1.025 |
| 2013 | 0.497 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
