---
title: "INSTALLED BUILDING PRODUCTS INC (IBP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>INSTALLED BUILDING PRODUCTS INC</td></tr>
    <tr><td>Symbol</td><td>IBP</td></tr>
    <tr><td>Web</td><td><a href="https://www.installedbuildingproducts.com">www.installedbuildingproducts.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
