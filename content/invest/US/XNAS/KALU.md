---
title: "KAISER ALUMINUM CORP (KALU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>KAISER ALUMINUM CORP</td></tr>
    <tr><td>Symbol</td><td>KALU</td></tr>
    <tr><td>Web</td><td><a href="https://www.kaiseraluminum.com">www.kaiseraluminum.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.44 |
| 2020 | 2.68 |
| 2019 | 2.4 |
| 2018 | 2.2 |
| 2017 | 2.0 |
| 2016 | 1.8 |
| 2015 | 1.6 |
| 2014 | 1.4 |
| 2013 | 1.2 |
| 2012 | 1.0 |
| 2011 | 0.96 |
| 2010 | 0.96 |
| 2009 | 0.96 |
| 2008 | 0.84 |
| 2007 | 0.36 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
