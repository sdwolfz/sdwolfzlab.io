---
title: "Bytes Tech (BYIT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Bytes Tech</td></tr>
    <tr><td>Symbol</td><td>BYIT</td></tr>
    <tr><td>Web</td><td><a href="https://www.bytes.co.uk">www.bytes.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
