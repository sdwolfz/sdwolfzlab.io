---
title: " (SKT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SKT</td></tr>
    <tr><td>Web</td><td><a href="https://www.tangeroutlet.com">www.tangeroutlet.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.354 |
| 2020 | 0.712 |
| 2019 | 1.415 |
| 2018 | 1.393 |
| 2017 | 1.354 |
| 2016 | 1.26 |
| 2015 | 1.095 |
| 2014 | 0.945 |
| 2013 | 0.45 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
