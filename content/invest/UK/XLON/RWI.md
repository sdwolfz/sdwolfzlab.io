---
title: "Renewi Plc (RWI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Renewi Plc</td></tr>
    <tr><td>Symbol</td><td>RWI</td></tr>
    <tr><td>Web</td><td><a href="https://www.renewi.com">www.renewi.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.45 |
| 2019 | 1.45 |
| 2018 | 3.05 |
| 2017 | 3.05 |
| 2016 | 3.45 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
