---
title: "SPIRE INC (SR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SPIRE INC</td></tr>
    <tr><td>Symbol</td><td>SR</td></tr>
    <tr><td>Web</td><td><a href="https://www.spireenergy.com">www.spireenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.65 |
| 2020 | 2.519 |
| 2019 | 2.399 |
| 2018 | 2.281 |
| 2017 | 2.138 |
| 2016 | 1.995 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
