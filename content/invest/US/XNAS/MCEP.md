---
title: "MID-CON ENERGY PARTNERS LP (MCEP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MID-CON ENERGY PARTNERS LP</td></tr>
    <tr><td>Symbol</td><td>MCEP</td></tr>
    <tr><td>Web</td><td><a href="http://www.midconenergypartners.com">www.midconenergypartners.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 0.375 |
| 2014 | 2.06 |
| 2013 | 2.03 |
| 2012 | 1.492 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
