---
title: " (SUN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SUN</td></tr>
    <tr><td>Web</td><td><a href="https://www.sunocolp.com">www.sunocolp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.652 |
| 2020 | 3.304 |
| 2019 | 3.304 |
| 2018 | 3.304 |
| 2017 | 3.304 |
| 2016 | 3.27 |
| 2015 | 2.683 |
| 2014 | 1.066 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
