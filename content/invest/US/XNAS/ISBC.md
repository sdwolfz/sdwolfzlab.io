---
title: "INVESTORS BANCORP INC (ISBC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>INVESTORS BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>ISBC</td></tr>
    <tr><td>Web</td><td><a href="https://www.investorsbank.com">www.investorsbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 0.48 |
| 2019 | 0.44 |
| 2018 | 0.38 |
| 2017 | 0.33 |
| 2016 | 0.26 |
| 2015 | 0.25 |
| 2014 | 0.18 |
| 2013 | 0.2 |
| 2012 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
