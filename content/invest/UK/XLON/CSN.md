---
title: "Chesnara (CSN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Chesnara</td></tr>
    <tr><td>Symbol</td><td>CSN</td></tr>
    <tr><td>Web</td><td><a href="https://www.chesnara.co.uk">www.chesnara.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 21.3 |
| 2018 | 20.67 |
| 2017 | 20.07 |
| 2016 | 19.49 |
| 2015 | 18.94 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
