---
title: "KINSALE CAPITAL GROUP INC (KNSL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>KINSALE CAPITAL GROUP INC</td></tr>
    <tr><td>Symbol</td><td>KNSL</td></tr>
    <tr><td>Web</td><td><a href="https://www.kinsalecapitalgroup.com">www.kinsalecapitalgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.22 |
| 2020 | 0.36 |
| 2019 | 0.32 |
| 2018 | 0.28 |
| 2017 | 0.24 |
| 2016 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
