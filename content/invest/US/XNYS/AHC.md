---
title: "A H BELO CORP (AHC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>A H BELO CORP</td></tr>
    <tr><td>Symbol</td><td>AHC</td></tr>
    <tr><td>Web</td><td><a href="https://www.ahbelo.com">www.ahbelo.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.08 |
| 2020 | 0.24 |
| 2019 | 0.32 |
| 2018 | 0.32 |
| 2017 | 0.46 |
| 2016 | 0.32 |
| 2015 | 0.32 |
| 2014 | 0.32 |
| 2013 | 0.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
