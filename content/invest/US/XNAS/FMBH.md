---
title: "FIRST MID BANCSHARES INC (FMBH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST MID BANCSHARES INC</td></tr>
    <tr><td>Symbol</td><td>FMBH</td></tr>
    <tr><td>Web</td><td><a href="https://www.firstmid.com">www.firstmid.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.41 |
| 2020 | 0.81 |
| 2019 | 0.76 |
| 2018 | 0.7 |
| 2017 | 0.66 |
| 2016 | 0.62 |
| 2015 | 0.59 |
| 2014 | 0.55 |
| 2013 | 0.46 |
| 2012 | 0.42 |
| 2011 | 0.4 |
| 2010 | 0.38 |
| 2009 | 0.38 |
| 2008 | 0.38 |
| 2007 | 0.47 |
| 2006 | 0.52 |
| 2005 | 0.5 |
| 2004 | 0.56 |
| 2003 | 0.65 |
| 2002 | 0.5 |
| 2001 | 0.29 |
| 2000 | 0.32 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
