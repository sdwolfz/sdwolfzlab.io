---
title: " (MOMO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>MOMO</td></tr>
    <tr><td>Web</td><td><a href="https://www.immomo.com">www.immomo.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.64 |
| 2020 | 0.76 |
| 2019 | 0.62 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
