---
title: "PROGRESS SOFTWARE CORP (PRGS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PROGRESS SOFTWARE CORP</td></tr>
    <tr><td>Symbol</td><td>PRGS</td></tr>
    <tr><td>Web</td><td><a href="https://www.progress.com">www.progress.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.35 |
| 2020 | 0.67 |
| 2019 | 0.63 |
| 2018 | 0.575 |
| 2017 | 0.515 |
| 2016 | 0.125 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
