---
title: "SINTEZA S.A. (STZ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>SINTEZA S.A.</td></tr>
    <tr><td>Symbol</td><td>STZ</td></tr>
    <tr><td>Web</td><td><a href="https://www.sinteza.ro">www.sinteza.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.06806 |
| 2012 | 0.01866 |
| 2002 | 0.0147 |
| 2001 | 0.0136 |
| 2000 | 0.0198 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
