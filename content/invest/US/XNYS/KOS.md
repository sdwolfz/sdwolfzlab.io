---
title: "KOSMOS ENERGY LTD (NA) (KOS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>KOSMOS ENERGY LTD (NA)</td></tr>
    <tr><td>Symbol</td><td>KOS</td></tr>
    <tr><td>Web</td><td><a href="https://www.kosmosenergy.com">www.kosmosenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.045 |
| 2019 | 0.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
