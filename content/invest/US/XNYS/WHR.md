---
title: "WHIRLPOOL CORP (WHR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>WHIRLPOOL CORP</td></tr>
    <tr><td>Symbol</td><td>WHR</td></tr>
    <tr><td>Web</td><td><a href="https://www.whirlpoolcorp.com">www.whirlpoolcorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.65 |
| 2020 | 4.85 |
| 2019 | 4.75 |
| 2018 | 4.55 |
| 2017 | 4.3 |
| 2016 | 3.9 |
| 2015 | 3.45 |
| 2014 | 2.875 |
| 2013 | 1.875 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
