---
title: "How to Use: ansible-lint"
date: 2022-07-23T09:47:49Z
type: posts
slug: how-to-use-ansible-lint
series: ["How to Use"]
tags: ["ansible", "tutorial", "linting", "productivity"]
---

## Install

From package:

```sh
# Manjaro / Arch
sudo pacman -S ansible-lint

# Alpine
apk add --update ansible-lint
```

From Docker (no official image so use mine):

```sh
docker run --rm -it -v "$PWD":/work -w /work sdwolfz/ansible-lint:latest sh
```

## Configure

Add a `.ansible-lint` file containing your rules:

```yml
---

skip_list:
  - yaml  # Violations reported by yamllint.
```

## Use

```sh
ansible-lint roles/**/*
```
