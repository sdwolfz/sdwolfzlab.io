---
title: "ENSIGN GROUP INC (ENSG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ENSIGN GROUP INC</td></tr>
    <tr><td>Symbol</td><td>ENSG</td></tr>
    <tr><td>Web</td><td><a href="https://www.ensigngroup.net">www.ensigngroup.net</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.053 |
| 2020 | 0.203 |
| 2019 | 0.193 |
| 2018 | 0.183 |
| 2017 | 0.173 |
| 2016 | 0.163 |
| 2015 | 0.265 |
| 2014 | 0.285 |
| 2013 | 0.265 |
| 2012 | 0.245 |
| 2011 | 0.225 |
| 2010 | 0.205 |
| 2009 | 0.185 |
| 2008 | 0.165 |
| 2007 | 0.04 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
