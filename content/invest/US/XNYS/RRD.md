---
title: "DONNELLEY RR & SONS CO (RRD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DONNELLEY RR & SONS CO</td></tr>
    <tr><td>Symbol</td><td>RRD</td></tr>
    <tr><td>Web</td><td><a href="https://www.rrd.com">www.rrd.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.03 |
| 2019 | 0.12 |
| 2018 | 0.34 |
| 2017 | 0.56 |
| 2016 | 1.44 |
| 2015 | 1.04 |
| 2014 | 1.04 |
| 2013 | 0.52 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
