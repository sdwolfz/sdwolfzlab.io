---
title: "AMERICAN NATIONAL BANKSHARES (AMNB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AMERICAN NATIONAL BANKSHARES</td></tr>
    <tr><td>Symbol</td><td>AMNB</td></tr>
    <tr><td>Web</td><td><a href="https://www.amnb.com">www.amnb.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.27 |
| 2020 | 1.08 |
| 2019 | 1.04 |
| 2018 | 1.0 |
| 2017 | 0.97 |
| 2016 | 0.96 |
| 2015 | 0.93 |
| 2014 | 0.92 |
| 2013 | 0.92 |
| 2012 | 0.92 |
| 2011 | 0.92 |
| 2010 | 0.92 |
| 2009 | 0.92 |
| 2008 | 0.92 |
| 2007 | 0.91 |
| 2006 | 0.87 |
| 2005 | 0.83 |
| 2004 | 0.79 |
| 2003 | 0.75 |
| 2002 | 0.71 |
| 2001 | 0.66 |
| 2000 | 0.585 |
| 1999 | 0.54 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
