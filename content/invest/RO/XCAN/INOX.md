---
title: "INOX SA MAGURELE (INOX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>INOX SA MAGURELE</td></tr>
    <tr><td>Symbol</td><td>INOX</td></tr>
    <tr><td>Web</td><td><a href="https://www.inoxsa.ro">www.inoxsa.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2005 | 3.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
