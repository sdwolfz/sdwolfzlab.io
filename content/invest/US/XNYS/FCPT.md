---
title: " (FCPT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>FCPT</td></tr>
    <tr><td>Web</td><td><a href="https://fcpt.com">fcpt.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.317 |
| 2020 | 1.537 |
| 2019 | 1.148 |
| 2018 | 0.825 |
| 2017 | 1.004 |
| 2016 | 9.292 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
