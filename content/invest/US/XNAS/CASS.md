---
title: "CASS INFORMATION SYSTEMS (CASS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CASS INFORMATION SYSTEMS</td></tr>
    <tr><td>Symbol</td><td>CASS</td></tr>
    <tr><td>Web</td><td><a href="https://www.cassinfo.com">www.cassinfo.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.54 |
| 2020 | 1.08 |
| 2019 | 1.05 |
| 2018 | 0.76 |
| 2017 | 0.69 |
| 2016 | 0.89 |
| 2015 | 0.85 |
| 2014 | 0.81 |
| 2013 | 0.74 |
| 2012 | 0.51 |
| 2011 | 0.48 |
| 2010 | 0.58 |
| 2009 | 0.53 |
| 2008 | 0.49 |
| 2007 | 0.36 |
| 2006 | 0.6 |
| 2005 | 0.79 |
| 2004 | 0.63 |
| 2003 | 0.84 |
| 2002 | 0.6 |
| 2001 | 0.8 |
| 2000 | 0.8 |
| 1999 | 0.76 |
| 1998 | 0.72 |
| 1997 | 0.78 |
| 1996 | 0.73 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
