---
title: "PC CONNECTION (CNXN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PC CONNECTION</td></tr>
    <tr><td>Symbol</td><td>CNXN</td></tr>
    <tr><td>Web</td><td><a href="https://www.connection.com">www.connection.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.32 |
| 2019 | 0.32 |
| 2018 | 0.32 |
| 2017 | 0.34 |
| 2016 | 0.34 |
| 2015 | 0.4 |
| 2014 | 0.4 |
| 2013 | 0.4 |
| 2012 | 0.38 |
| 2011 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
