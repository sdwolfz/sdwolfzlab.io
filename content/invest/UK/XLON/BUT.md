---
title: "Brunner Inv.tst (BUT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Brunner Inv.tst</td></tr>
    <tr><td>Symbol</td><td>BUT</td></tr>
    <tr><td>Web</td><td><a href="https://www.brunner.co.uk">www.brunner.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 10.72 |
| 2019 | 19.98 |
| 2018 | 18.15 |
| 2017 | 16.5 |
| 2016 | 15.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
