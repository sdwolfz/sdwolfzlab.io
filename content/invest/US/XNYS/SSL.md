---
title: " (SSL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SSL</td></tr>
    <tr><td>Web</td><td><a href="https://www.sasol.com">www.sasol.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.306 |
| 2018 | 0.706 |
| 2017 | 0.744 |
| 2016 | 0.853 |
| 2015 | 1.174 |
| 2014 | 1.637 |
| 2013 | 1.661 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
