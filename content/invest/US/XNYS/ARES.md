---
title: " (ARES)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ARES</td></tr>
    <tr><td>Web</td><td><a href="https://www.aresmgmt.com">www.aresmgmt.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.94 |
| 2020 | 1.6 |
| 2019 | 1.28 |
| 2018 | 1.333 |
| 2017 | 1.13 |
| 2016 | 0.83 |
| 2015 | 0.88 |
| 2014 | 0.42 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
