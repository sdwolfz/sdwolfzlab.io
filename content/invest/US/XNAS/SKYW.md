---
title: "SKYWEST INC (SKYW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SKYWEST INC</td></tr>
    <tr><td>Symbol</td><td>SKYW</td></tr>
    <tr><td>Web</td><td><a href="https://www.skywest.com">www.skywest.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.14 |
| 2019 | 0.48 |
| 2018 | 0.4 |
| 2017 | 0.32 |
| 2016 | 0.19 |
| 2015 | 0.16 |
| 2014 | 0.2 |
| 2013 | 0.16 |
| 2012 | 0.12 |
| 2011 | 0.16 |
| 2010 | 0.16 |
| 2009 | 0.16 |
| 2008 | 0.13 |
| 2007 | 0.12 |
| 2006 | 0.12 |
| 2005 | 0.12 |
| 2004 | 0.12 |
| 2003 | 0.08 |
| 2002 | 0.08 |
| 2001 | 0.08 |
| 2000 | 0.14 |
| 1999 | 0.12 |
| 1998 | 0.14 |
| 1997 | 0.15 |
| 1996 | 0.18 |
| 1995 | 0.25 |
| 1994 | 0.28 |
| 1993 | 0.15 |
| 1992 | 0.08 |
| 1991 | 0.085 |
| 1990 | 0.185 |
| 1989 | 0.145 |
| 1988 | 0.055 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
