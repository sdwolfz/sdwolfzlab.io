---
title: "MCEWEN MINING INC (MUX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MCEWEN MINING INC</td></tr>
    <tr><td>Symbol</td><td>MUX</td></tr>
    <tr><td>Web</td><td><a href="https://www.mcewenmining.com">www.mcewenmining.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.005 |
| 2018 | 0.01 |
| 2017 | 0.005 |
| 2016 | 0.005 |
| 2015 | 0.004 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
