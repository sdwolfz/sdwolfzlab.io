---
title: " (BAK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BAK</td></tr>
    <tr><td>Web</td><td><a href="https://www.braskem.com.br">www.braskem.com.br</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 2.07 |
| 2018 | 1.017 |
| 2017 | 0.755 |
| 2016 | 1.443 |
| 2015 | 0.361 |
| 2014 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
