---
title: "UTILAJ GREU SA MURFATLAR (UTGR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>UTILAJ GREU SA MURFATLAR</td></tr>
    <tr><td>Symbol</td><td>UTGR</td></tr>
    <tr><td>Web</td><td><a href="https://www.utilaj-greu.ro">www.utilaj-greu.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.2538 |
| 2019 | 1.2156 |
| 2018 | 0.4928 |
| 2017 | 0.1498 |
| 2015 | 1.6 |
| 2009 | 0.2526 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
