---
title: "Travis Perkins (TPK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Travis Perkins</td></tr>
    <tr><td>Symbol</td><td>TPK</td></tr>
    <tr><td>Web</td><td><a href="https://www.travisperkinsplc.co.uk">www.travisperkinsplc.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 48.5 |
| 2018 | 47.0 |
| 2017 | 46.0 |
| 2016 | 45.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
