---
title: "AVIENT CORPORATION (AVNT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AVIENT CORPORATION</td></tr>
    <tr><td>Symbol</td><td>AVNT</td></tr>
    <tr><td>Web</td><td><a href="https://www.avient.com">www.avient.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.213 |
| 2020 | 0.619 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
