---
title: "NEENAH INC (NP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NEENAH INC</td></tr>
    <tr><td>Symbol</td><td>NP</td></tr>
    <tr><td>Web</td><td><a href="https://www.neenah.com">www.neenah.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.94 |
| 2020 | 1.88 |
| 2019 | 1.8 |
| 2018 | 1.64 |
| 2017 | 1.48 |
| 2016 | 1.32 |
| 2015 | 1.2 |
| 2014 | 1.02 |
| 2013 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
