---
title: "Expose a web server on local network"
date: 2020-10-17T20:38:17Z
type: posts
slug: expose-a-web-server-on-local-network
series: ["Android"]
tags: ["http", "productivity"]
---

1. Connect your laptop and phone to the same network (Wi-Fi is a network too).

2. Create a `index.html` file to share:

```html
<div>Hello!</div>
```

3. Start a web server in the same directory as the file:

```sh
ruby -run -e httpd -- --bind-address=0.0.0.0 --port=9090
```

4. Find your hostname, or ip address:

```sh
hostname
# my-pc

ip addr
# ... some wall of text, one line containing something like:
# inet 192.168.0.38/24 ...
```

5. Now you can use either your hostname or ip address to access your web page
   from your phone:

```
http://my-pc:9090/index.html

http://192.168.0.38:9090/index.html
```

6. Install [`Termux`](https://f-droid.org/en/packages/com.termux/) on your phone
   and do the exact same to access web pages from your laptop.
