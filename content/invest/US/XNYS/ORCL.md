---
title: "ORACLE CORP (ORCL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ORACLE CORP</td></tr>
    <tr><td>Symbol</td><td>ORCL</td></tr>
    <tr><td>Web</td><td><a href="https://www.oracle.com">www.oracle.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.56 |
| 2020 | 0.96 |
| 2019 | 0.91 |
| 2018 | 0.76 |
| 2017 | 0.72 |
| 2016 | 0.6 |
| 2015 | 0.57 |
| 2014 | 0.48 |
| 2013 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
