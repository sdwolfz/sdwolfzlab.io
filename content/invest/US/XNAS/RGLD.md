---
title: "ROYAL GOLD INC (RGLD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ROYAL GOLD INC</td></tr>
    <tr><td>Symbol</td><td>RGLD</td></tr>
    <tr><td>Web</td><td><a href="https://www.royalgold.com">www.royalgold.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.6 |
| 2020 | 1.12 |
| 2019 | 1.06 |
| 2018 | 1.0 |
| 2017 | 0.96 |
| 2016 | 0.92 |
| 2015 | 0.66 |
| 2014 | 0.85 |
| 2013 | 1.01 |
| 2012 | 0.6 |
| 2011 | 0.44 |
| 2010 | 0.27 |
| 2009 | 0.33 |
| 2008 | 0.36 |
| 2007 | 0.26 |
| 2006 | 0.22 |
| 2005 | 0.2 |
| 2004 | 0.112 |
| 2003 | 0.088 |
| 2002 | 0.125 |
| 2001 | 0.05 |
| 2000 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
