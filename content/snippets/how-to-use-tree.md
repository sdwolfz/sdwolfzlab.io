---
title: "How to Use: tree"
date: 2020-12-16T11:55:31Z
type: posts
slug: how-to-use-tree
series: ["How to Use"]
tags: ["linux", "terminal"]
---

## Install

```sh
sudo pacman -S tree
```

## Usage

Basic usage is like this:

```sh
tree
```

But that is incomplete, lacking hidden files and colors...

## Proper Usage

```sh
tree -aCI '.git'
```

And it looks like this:

```sh
.
├── Dockerfile
├── .dockerignore -> .gitignore
├── .editorconfig
├── .gitignore
├── README.md
└── src
    └── main.sh
```

> You won't see it in this snippet, but `-C` gives it nice easy to read colors!

Separate multiple ignore patters by `|` inside the string:

```sh
tree -aCI '.git|tmp|vendor'
```
