---
title: "GRAHAM CORP (GHM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GRAHAM CORP</td></tr>
    <tr><td>Symbol</td><td>GHM</td></tr>
    <tr><td>Web</td><td><a href="https://www.graham-mfg.com">www.graham-mfg.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.11 |
| 2020 | 0.33 |
| 2019 | 0.42 |
| 2018 | 0.38 |
| 2017 | 0.36 |
| 2016 | 0.36 |
| 2015 | 0.32 |
| 2014 | 0.16 |
| 2013 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
