---
title: " (WELL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>WELL</td></tr>
    <tr><td>Web</td><td><a href="https://www.welltower.com">www.welltower.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.22 |
| 2020 | 2.7 |
| 2019 | 3.48 |
| 2018 | 3.48 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
