---
title: "TJX COS INC (TJX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TJX COS INC</td></tr>
    <tr><td>Symbol</td><td>TJX</td></tr>
    <tr><td>Web</td><td><a href="https://www.tjx.com">www.tjx.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.52 |
| 2020 | 0.23 |
| 2019 | 0.885 |
| 2018 | 1.288 |
| 2017 | 1.199 |
| 2016 | 0.99 |
| 2015 | 0.805 |
| 2014 | 0.67 |
| 2013 | 0.29 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
