---
title: "SIF HOTELURI SA (CAOR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>SIF HOTELURI SA</td></tr>
    <tr><td>Symbol</td><td>CAOR</td></tr>
    <tr><td>Web</td><td><a href="https://www.sif-hoteluri.ro">www.sif-hoteluri.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2004 | 0.89 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
