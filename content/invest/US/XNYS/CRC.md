---
title: "CALIFORNIA RESOURCES CORP (CRC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CALIFORNIA RESOURCES CORP</td></tr>
    <tr><td>Symbol</td><td>CRC</td></tr>
    <tr><td>Web</td><td><a href="https://www.crc.com">www.crc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 0.03 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
