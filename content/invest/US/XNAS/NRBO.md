---
title: "NEUROBO PHARMACEUTICALS INC (NRBO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NEUROBO PHARMACEUTICALS INC</td></tr>
    <tr><td>Symbol</td><td>NRBO</td></tr>
    <tr><td>Web</td><td><a href="https://www.neurobopharma.com">www.neurobopharma.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
