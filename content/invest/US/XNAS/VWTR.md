---
title: "VIDLER WATER RESOUCES INC (VWTR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>VIDLER WATER RESOUCES INC</td></tr>
    <tr><td>Symbol</td><td>VWTR</td></tr>
    <tr><td>Web</td><td><a href="https://www.vidlerwater.com">www.vidlerwater.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 5.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
