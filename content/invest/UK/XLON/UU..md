---
title: "United Utilities (UU.)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>United Utilities</td></tr>
    <tr><td>Symbol</td><td>UU.</td></tr>
    <tr><td>Web</td><td><a href="https://www.unitedutilities.com">www.unitedutilities.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 42.6 |
| 2019 | 41.28 |
| 2018 | 39.73 |
| 2017 | 38.87 |
| 2016 | 38.45 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
