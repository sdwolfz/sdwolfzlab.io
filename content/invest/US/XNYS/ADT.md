---
title: "ADT INC (ADT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ADT INC</td></tr>
    <tr><td>Symbol</td><td>ADT</td></tr>
    <tr><td>Web</td><td><a href="https://www.adt.com">www.adt.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.07 |
| 2020 | 0.14 |
| 2019 | 0.84 |
| 2018 | 0.14 |
| 2016 | 0.22 |
| 2015 | 0.84 |
| 2014 | 0.8 |
| 2013 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
