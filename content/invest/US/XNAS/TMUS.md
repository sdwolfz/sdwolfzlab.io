---
title: "T-MOBILE US INC (TMUS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>T-MOBILE US INC</td></tr>
    <tr><td>Symbol</td><td>TMUS</td></tr>
    <tr><td>Web</td><td><a href="https://www.t-mobile.com">www.t-mobile.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2013 | 4.049 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
