---
title: "CMC MATERIALS INC (CCMP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CMC MATERIALS INC</td></tr>
    <tr><td>Symbol</td><td>CCMP</td></tr>
    <tr><td>Web</td><td><a href="https://www.cmcmaterials.com">www.cmcmaterials.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.46 |
| 2020 | 1.76 |
| 2019 | 1.68 |
| 2018 | 1.6 |
| 2017 | 0.8 |
| 2016 | 0.72 |
| 2012 | 15.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
