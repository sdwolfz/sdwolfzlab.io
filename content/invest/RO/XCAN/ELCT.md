---
title: "EL-CO SA TARGU SECUIESC (ELCT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>EL-CO SA TARGU SECUIESC</td></tr>
    <tr><td>Symbol</td><td>ELCT</td></tr>
    <tr><td>Web</td><td><a href="https://www.el-co.ro">www.el-co.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2004 | 2.2092 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
