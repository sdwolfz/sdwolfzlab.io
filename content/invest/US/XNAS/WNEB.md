---
title: "WESTERN NEW ENG BANCORP INC (WNEB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>WESTERN NEW ENG BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>WNEB</td></tr>
    <tr><td>Web</td><td><a href="https://www.westfieldbank.com">www.westfieldbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.2 |
| 2019 | 0.2 |
| 2018 | 0.16 |
| 2017 | 0.12 |
| 2016 | 0.12 |
| 2015 | 0.12 |
| 2014 | 0.21 |
| 2013 | 0.29 |
| 2012 | 0.44 |
| 2011 | 0.54 |
| 2010 | 0.52 |
| 2009 | 0.5 |
| 2008 | 0.6 |
| 2007 | 0.4 |
| 2006 | 1.0 |
| 2005 | 0.9 |
| 2004 | 0.25 |
| 2003 | 0.2 |
| 2002 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
