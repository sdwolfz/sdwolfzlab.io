---
title: "JD Sports (JD.)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>JD Sports</td></tr>
    <tr><td>Symbol</td><td>JD.</td></tr>
    <tr><td>Web</td><td><a href="https://www.jdsports.co.uk">www.jdsports.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.72 |
| 2019 | 1.71 |
| 2018 | 1.63 |
| 2017 | 1.5 |
| 2016 | 1.48 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
