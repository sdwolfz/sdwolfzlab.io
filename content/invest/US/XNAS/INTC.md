---
title: "INTEL CORP (INTC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>INTEL CORP</td></tr>
    <tr><td>Symbol</td><td>INTC</td></tr>
    <tr><td>Web</td><td><a href="https://www.intel.com">www.intel.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.695 |
| 2020 | 1.32 |
| 2019 | 1.26 |
| 2018 | 1.2 |
| 2017 | 1.078 |
| 2016 | 1.04 |
| 2015 | 0.96 |
| 2014 | 0.9 |
| 2013 | 0.9 |
| 2012 | 0.87 |
| 2011 | 0.782 |
| 2010 | 0.63 |
| 2009 | 0.56 |
| 2008 | 0.548 |
| 2007 | 0.45 |
| 2006 | 0.4 |
| 2005 | 0.32 |
| 2004 | 0.16 |
| 2003 | 0.08 |
| 2002 | 0.08 |
| 2001 | 0.08 |
| 2000 | 0.1 |
| 1999 | 0.13 |
| 1998 | 0.13 |
| 1997 | 0.16 |
| 1996 | 0.18 |
| 1995 | 0.2 |
| 1994 | 0.225 |
| 1993 | 0.3 |
| 1992 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
