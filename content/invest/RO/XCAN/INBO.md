---
title: "LACTATE NATURA SA TARGOVISTE (INBO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>LACTATE NATURA SA TARGOVISTE</td></tr>
    <tr><td>Symbol</td><td>INBO</td></tr>
    <tr><td>Web</td><td><a href="https://www.lactatenatura.ro">www.lactatenatura.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2008 | 0.2384 |
| 2004 | 0.2551 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
