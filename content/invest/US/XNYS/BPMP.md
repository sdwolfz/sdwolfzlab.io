---
title: " (BPMP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BPMP</td></tr>
    <tr><td>Web</td><td><a href="https://www.bpmidstreampartners.com">www.bpmidstreampartners.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.694 |
| 2020 | 1.388 |
| 2019 | 1.274 |
| 2018 | 1.013 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
