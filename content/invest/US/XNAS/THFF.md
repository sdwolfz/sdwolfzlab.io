---
title: "FIRST FINANCIAL CORPORATION IND (THFF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST FINANCIAL CORPORATION IND</td></tr>
    <tr><td>Symbol</td><td>THFF</td></tr>
    <tr><td>Web</td><td><a href="https://www.first-online.com">www.first-online.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.53 |
| 2020 | 1.04 |
| 2019 | 1.03 |
| 2018 | 1.02 |
| 2017 | 2.5 |
| 2016 | 0.99 |
| 2015 | 0.98 |
| 2014 | 0.97 |
| 2013 | 0.48 |
| 2012 | 0.95 |
| 2011 | 0.94 |
| 2010 | 0.92 |
| 2009 | 0.9 |
| 2008 | 0.89 |
| 2007 | 0.87 |
| 2006 | 0.85 |
| 2005 | 0.82 |
| 2004 | 0.79 |
| 2003 | 1.04 |
| 2002 | 1.24 |
| 2001 | 1.14 |
| 2000 | 1.08 |
| 1999 | 0.94 |
| 1998 | 0.84 |
| 1997 | 0.76 |
| 1996 | 0.35 |
| 1995 | 0.28 |
| 1994 | 0.55 |
| 1993 | 0.77 |
| 1992 | 0.9 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
