---
title: "HOWMET AEROSPACE INC (HWM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HOWMET AEROSPACE INC</td></tr>
    <tr><td>Symbol</td><td>HWM</td></tr>
    <tr><td>Web</td><td><a href="https://www.howmet.com">www.howmet.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 3.77 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
