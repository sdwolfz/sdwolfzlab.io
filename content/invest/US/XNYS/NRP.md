---
title: " (NRP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>NRP</td></tr>
    <tr><td>Web</td><td><a href="https://www.nrplp.com">www.nrplp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.9 |
| 2020 | 1.35 |
| 2019 | 2.65 |
| 2018 | 1.8 |
| 2017 | 1.8 |
| 2016 | 1.8 |
| 2015 | 0.575 |
| 2014 | 1.4 |
| 2013 | 1.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
