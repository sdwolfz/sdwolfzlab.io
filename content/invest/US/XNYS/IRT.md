---
title: " (IRT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>IRT</td></tr>
    <tr><td>Web</td><td><a href="https://www.irtliving.com">www.irtliving.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 0.54 |
| 2019 | 0.72 |
| 2018 | 0.72 |
| 2017 | 0.72 |
| 2016 | 0.72 |
| 2015 | 0.754 |
| 2014 | 0.72 |
| 2013 | 0.265 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
