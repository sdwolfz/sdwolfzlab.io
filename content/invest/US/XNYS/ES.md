---
title: "EVERSOURCE ENERGY (ES)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>EVERSOURCE ENERGY</td></tr>
    <tr><td>Symbol</td><td>ES</td></tr>
    <tr><td>Web</td><td><a href="https://www.eversource.com">www.eversource.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.603 |
| 2020 | 2.268 |
| 2019 | 2.14 |
| 2018 | 2.02 |
| 2017 | 1.9 |
| 2016 | 1.78 |
| 2015 | 1.668 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
