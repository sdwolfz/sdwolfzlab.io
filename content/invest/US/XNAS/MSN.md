---
title: "EMERSON RADIO CORP (MSN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>EMERSON RADIO CORP</td></tr>
    <tr><td>Symbol</td><td>MSN</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 0.7 |
| 2010 | 1.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
