---
title: "LYONDELLBASELL INDUSTRIES N V (LYB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>LYONDELLBASELL INDUSTRIES N V</td></tr>
    <tr><td>Symbol</td><td>LYB</td></tr>
    <tr><td>Web</td><td><a href="https://www.lyondellbasell.com">www.lyondellbasell.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.05 |
| 2020 | 4.2 |
| 2019 | 19.15 |
| 2018 | 4.0 |
| 2017 | 3.55 |
| 2016 | 3.33 |
| 2015 | 3.04 |
| 2014 | 2.7 |
| 2013 | 1.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
