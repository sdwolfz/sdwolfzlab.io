---
title: "MERCHANTS BANCORP (MBIN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MERCHANTS BANCORP</td></tr>
    <tr><td>Symbol</td><td>MBIN</td></tr>
    <tr><td>Web</td><td><a href="https://www.merchantsbankofindiana.com">www.merchantsbankofindiana.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.09 |
| 2020 | 0.32 |
| 2019 | 0.28 |
| 2018 | 0.24 |
| 2017 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
