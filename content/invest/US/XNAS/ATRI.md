---
title: "ATRION CORP (ATRI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ATRION CORP</td></tr>
    <tr><td>Symbol</td><td>ATRI</td></tr>
    <tr><td>Web</td><td><a href="https://www.atrioncorp.com">www.atrioncorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.75 |
| 2020 | 6.6 |
| 2019 | 5.8 |
| 2018 | 5.1 |
| 2017 | 4.5 |
| 2016 | 3.9 |
| 2015 | 3.3 |
| 2014 | 2.78 |
| 2013 | 2.4 |
| 2012 | 12.1 |
| 2011 | 1.82 |
| 2010 | 10.56 |
| 2009 | 1.32 |
| 2008 | 1.08 |
| 2007 | 0.88 |
| 2006 | 0.74 |
| 2005 | 0.62 |
| 2004 | 0.52 |
| 2003 | 0.24 |
| 1997 | 0.6 |
| 1996 | 1.2 |
| 1995 | 1.2 |
| 1994 | 1.2 |
| 1993 | 1.2 |
| 1992 | 1.175 |
| 1991 | 1.1 |
| 1990 | 1.1 |
| 1989 | 1.1 |
| 1988 | 0.275 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
