---
title: "ARLINGTON ASSET INVESTMENT CORP (AAIC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ARLINGTON ASSET INVESTMENT CORP</td></tr>
    <tr><td>Symbol</td><td>AAIC</td></tr>
    <tr><td>Web</td><td><a href="https://www.arlingtonasset.com">www.arlingtonasset.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
