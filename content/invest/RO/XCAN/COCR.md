---
title: "COCOR SA Bucuresti (COCR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>COCOR SA Bucuresti</td></tr>
    <tr><td>Symbol</td><td>COCR</td></tr>
    <tr><td>Web</td><td><a href="https://www.cocor.ro">www.cocor.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2007 | 10.0 |
| 2005 | 6.0 |
| 2004 | 7.1982 |
| 2003 | 19.9783 |
| 2001 | 7.125 |
| 2000 | 9.1675 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
