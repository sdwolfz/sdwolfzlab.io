---
title: "CREDICORP (BAP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CREDICORP</td></tr>
    <tr><td>Symbol</td><td>BAP</td></tr>
    <tr><td>Web</td><td><a href="https://www.grupocredicorp.com">www.grupocredicorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 8.658 |
| 2019 | 8.434 |
| 2018 | 4.394 |
| 2017 | 8.573 |
| 2016 | 2.316 |
| 2015 | 2.187 |
| 2014 | 1.9 |
| 2012 | 2.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
