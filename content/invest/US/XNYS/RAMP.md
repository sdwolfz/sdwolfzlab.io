---
title: "LIVERAMP HOLDINGS INC (RAMP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>LIVERAMP HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>RAMP</td></tr>
    <tr><td>Web</td><td><a href="https://www.liveramp.com">www.liveramp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
