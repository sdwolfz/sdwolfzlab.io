---
title: "WAYSIDE TECH GROUP INC (WSTG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>WAYSIDE TECH GROUP INC</td></tr>
    <tr><td>Symbol</td><td>WSTG</td></tr>
    <tr><td>Web</td><td><a href="https://www.waysidetechnology.com">www.waysidetechnology.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.34 |
| 2020 | 0.68 |
| 2019 | 0.68 |
| 2018 | 0.68 |
| 2017 | 0.68 |
| 2016 | 0.68 |
| 2015 | 0.68 |
| 2014 | 0.68 |
| 2013 | 0.65 |
| 2012 | 0.64 |
| 2011 | 0.64 |
| 2010 | 0.61 |
| 2009 | 0.6 |
| 2008 | 0.6 |
| 2007 | 0.44 |
| 2006 | 0.54 |
| 2005 | 0.49 |
| 2004 | 0.43 |
| 2003 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
