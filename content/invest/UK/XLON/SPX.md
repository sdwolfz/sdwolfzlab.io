---
title: "Spirax-Sarco (SPX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Spirax-Sarco</td></tr>
    <tr><td>Symbol</td><td>SPX</td></tr>
    <tr><td>Web</td><td><a href="https://www.spiraxsarcoengineering.com">www.spiraxsarcoengineering.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 110.0 |
| 2018 | 100.0 |
| 2017 | 87.5 |
| 2016 | 76.0 |
| 2015 | 69.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
