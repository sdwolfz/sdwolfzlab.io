---
title: "CECO ENVIRONMENTAL CORP (CECE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CECO ENVIRONMENTAL CORP</td></tr>
    <tr><td>Symbol</td><td>CECE</td></tr>
    <tr><td>Web</td><td><a href="https://www.cecoenviro.com">www.cecoenviro.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 0.225 |
| 2016 | 0.264 |
| 2015 | 0.264 |
| 2014 | 0.23 |
| 2013 | 0.2 |
| 2012 | 0.16 |
| 2011 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
