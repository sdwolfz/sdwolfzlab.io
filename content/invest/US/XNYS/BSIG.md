---
title: "BRIGHTSPHERE INVESTMENT GROUP INC (BSIG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BRIGHTSPHERE INVESTMENT GROUP INC</td></tr>
    <tr><td>Symbol</td><td>BSIG</td></tr>
    <tr><td>Web</td><td><a href="https://www.bsig.com">www.bsig.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.02 |
| 2020 | 0.23 |
| 2019 | 0.2 |
| 2018 | 0.09 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
