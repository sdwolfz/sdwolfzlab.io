---
title: "BEL FUSE INC (BELFA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BEL FUSE INC</td></tr>
    <tr><td>Symbol</td><td>BELFA</td></tr>
    <tr><td>Web</td><td><a href="https://www.belfuse.com">www.belfuse.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.18 |
| 2020 | 0.24 |
| 2019 | 0.24 |
| 2018 | 0.24 |
| 2017 | 0.24 |
| 2016 | 0.24 |
| 2015 | 0.24 |
| 2014 | 0.24 |
| 2013 | 0.24 |
| 2012 | 0.24 |
| 2011 | 0.24 |
| 2010 | 0.24 |
| 2009 | 0.24 |
| 2008 | 0.24 |
| 2007 | 0.18 |
| 2006 | 0.16 |
| 2005 | 0.16 |
| 2004 | 0.16 |
| 2003 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
