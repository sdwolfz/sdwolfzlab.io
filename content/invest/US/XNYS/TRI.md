---
title: "THOMSON-REUTERS CORP (TRI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>THOMSON-REUTERS CORP</td></tr>
    <tr><td>Symbol</td><td>TRI</td></tr>
    <tr><td>Web</td><td><a href="https://www.thomsonreuters.com">www.thomsonreuters.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.52 |
| 2019 | 1.44 |
| 2018 | 5.936 |
| 2017 | 1.38 |
| 2016 | 1.36 |
| 2015 | 1.34 |
| 2014 | 1.32 |
| 2013 | 0.65 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
