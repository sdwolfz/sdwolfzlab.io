---
title: "POLARITYTE INC (PTE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>POLARITYTE INC</td></tr>
    <tr><td>Symbol</td><td>PTE</td></tr>
    <tr><td>Web</td><td><a href="https://www.polarityte.com">www.polarityte.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.336 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
