---
title: "SOUTH JERSEY INDUSTRIES INC (SJI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SOUTH JERSEY INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>SJI</td></tr>
    <tr><td>Web</td><td><a href="https://www.sjindustries.com">www.sjindustries.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.606 |
| 2020 | 1.188 |
| 2019 | 1.156 |
| 2018 | 1.127 |
| 2017 | 1.099 |
| 2016 | 1.065 |
| 2015 | 1.017 |
| 2014 | 1.918 |
| 2013 | 0.914 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
