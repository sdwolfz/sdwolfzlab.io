---
title: "S.N.T.G.N. TRANSGAZ S.A. (TGN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>S.N.T.G.N. TRANSGAZ S.A.</td></tr>
    <tr><td>Symbol</td><td>TGN</td></tr>
    <tr><td>Web</td><td><a href="https://www.transgaz.ro">www.transgaz.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 8.14 |
| 2019 | 15.47 |
| 2018 | 21.66 |
| 2017 | 1.14 |
| 2017 | 45.38 |
| 2016 | 14.52 |
| 2016 | 46.33 |
| 2015 | 27.61 |
| 2014 | 21.8 |
| 2013 | 17.58 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
