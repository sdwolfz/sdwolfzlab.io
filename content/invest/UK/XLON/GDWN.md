---
title: "Goodwin (GDWN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Goodwin</td></tr>
    <tr><td>Symbol</td><td>GDWN</td></tr>
    <tr><td>Web</td><td><a href="https://www.goodwin.co.uk">www.goodwin.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 81.71 |
| 2019 | 96.21 |
| 2018 | 83.47 |
| 2017 | 42.35 |
| 2016 | 42.35 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
