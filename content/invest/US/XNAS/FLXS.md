---
title: "FLEXSTEEL INDUSTRIES INC (FLXS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FLEXSTEEL INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>FLXS</td></tr>
    <tr><td>Web</td><td><a href="https://www.flexsteel.com">www.flexsteel.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.15 |
| 2020 | 0.42 |
| 2019 | 0.88 |
| 2018 | 0.88 |
| 2017 | 0.84 |
| 2016 | 0.76 |
| 2015 | 0.72 |
| 2014 | 0.66 |
| 2013 | 0.6 |
| 2012 | 0.55 |
| 2011 | 0.35 |
| 2010 | 0.25 |
| 2009 | 0.2 |
| 2008 | 0.52 |
| 2007 | 0.52 |
| 2006 | 0.52 |
| 2005 | 0.52 |
| 2004 | 0.52 |
| 2003 | 0.52 |
| 2002 | 0.52 |
| 2001 | 0.52 |
| 2000 | 0.52 |
| 1999 | 0.5 |
| 1998 | 0.48 |
| 1997 | 0.48 |
| 1996 | 0.48 |
| 1995 | 0.48 |
| 1994 | 0.48 |
| 1993 | 0.48 |
| 1992 | 0.48 |
| 1991 | 0.48 |
| 1990 | 0.48 |
| 1989 | 0.6 |
| 1988 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
