---
title: "SCORPIO BULKERS INC (SALT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SCORPIO BULKERS INC</td></tr>
    <tr><td>Symbol</td><td>SALT</td></tr>
    <tr><td>Web</td><td><a href="http://www.scorpiobulkers.com">www.scorpiobulkers.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.35 |
| 2019 | 0.53 |
| 2018 | 0.08 |
| 2017 | 0.02 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
