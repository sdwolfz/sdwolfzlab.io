---
title: "Countryside Properties (CSP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Countryside Properties</td></tr>
    <tr><td>Symbol</td><td>CSP</td></tr>
    <tr><td>Web</td><td><a href="https://www.countryside-properties-corporate.com">www.countryside-properties-corporate.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 10.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
