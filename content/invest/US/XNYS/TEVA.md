---
title: " (TEVA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>TEVA</td></tr>
    <tr><td>Web</td><td><a href="https://www.tevapharm.com">www.tevapharm.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 0.722 |
| 2016 | 1.156 |
| 2015 | 1.161 |
| 2014 | 1.152 |
| 2013 | 0.552 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
