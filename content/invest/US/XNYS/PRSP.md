---
title: "PERSPECTA INC (PRSP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PERSPECTA INC</td></tr>
    <tr><td>Symbol</td><td>PRSP</td></tr>
    <tr><td>Web</td><td><a href="http://www.perspecta.com">www.perspecta.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.27 |
| 2019 | 0.23 |
| 2018 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
