---
title: "MJGleeson (GLE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>MJGleeson</td></tr>
    <tr><td>Symbol</td><td>GLE</td></tr>
    <tr><td>Web</td><td><a href="https://www.mjgleesonplc.com">www.mjgleesonplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 34.5 |
| 2018 | 32.0 |
| 2017 | 24.0 |
| 2016 | 14.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
