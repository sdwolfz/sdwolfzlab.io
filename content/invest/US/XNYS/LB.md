---
title: "LIMITED BRANDS INC (LB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>LIMITED BRANDS INC</td></tr>
    <tr><td>Symbol</td><td>LB</td></tr>
    <tr><td>Web</td><td><a href="https://www.lb.com">www.lb.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.3 |
| 2019 | 1.2 |
| 2018 | 2.4 |
| 2017 | 2.4 |
| 2016 | 4.4 |
| 2015 | 2.0 |
| 2014 | 1.36 |
| 2013 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
