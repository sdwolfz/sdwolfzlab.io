---
title: "Savills (SVS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Savills</td></tr>
    <tr><td>Symbol</td><td>SVS</td></tr>
    <tr><td>Web</td><td><a href="https://www.savills.com">www.savills.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 32.0 |
| 2018 | 31.2 |
| 2017 | 30.2 |
| 2016 | 29.0 |
| 2015 | 26.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
