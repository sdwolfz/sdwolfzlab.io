---
title: "MOARA CIBIN SA SIBIU (MOIB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>MOARA CIBIN SA SIBIU</td></tr>
    <tr><td>Symbol</td><td>MOIB</td></tr>
    <tr><td>Web</td><td><a href="https://www.moaracibin.ro">www.moaracibin.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.0187 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
