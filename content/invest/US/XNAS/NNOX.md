---
title: "NANO-X IMAGING LTD (NNOX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NANO-X IMAGING LTD</td></tr>
    <tr><td>Symbol</td><td>NNOX</td></tr>
    <tr><td>Web</td><td><a href="https://www.nanox.vision">www.nanox.vision</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
