---
title: "Croda International (CRDA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Croda International</td></tr>
    <tr><td>Symbol</td><td>CRDA</td></tr>
    <tr><td>Web</td><td><a href="https://www.croda.com">www.croda.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 90.0 |
| 2018 | 87.0 |
| 2017 | 81.0 |
| 2016 | 74.0 |
| 2015 | 69.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
