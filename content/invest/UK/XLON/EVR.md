---
title: "Evraz (EVR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Evraz</td></tr>
    <tr><td>Symbol</td><td>EVR</td></tr>
    <tr><td>Web</td><td><a href="https://www.evraz.com">www.evraz.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 60.0 |
| 2019 | 75.0 |
| 2018 | 78.0 |
| 2017 | 3.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
