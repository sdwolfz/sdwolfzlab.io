---
title: "GERMAN AMERN BANCORP (GABC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GERMAN AMERN BANCORP</td></tr>
    <tr><td>Symbol</td><td>GABC</td></tr>
    <tr><td>Web</td><td><a href="https://www.germanamerican.com">www.germanamerican.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.42 |
| 2020 | 0.76 |
| 2019 | 0.68 |
| 2018 | 0.6 |
| 2017 | 0.58 |
| 2016 | 0.72 |
| 2015 | 0.68 |
| 2014 | 0.64 |
| 2013 | 0.6 |
| 2012 | 0.56 |
| 2011 | 0.56 |
| 2010 | 0.56 |
| 2009 | 0.56 |
| 2008 | 0.56 |
| 2007 | 0.56 |
| 2006 | 0.56 |
| 2005 | 0.56 |
| 2004 | 0.56 |
| 2003 | 0.56 |
| 2002 | 0.56 |
| 2001 | 0.67 |
| 2000 | 0.55 |
| 1999 | 0.51 |
| 1998 | 0.47 |
| 1997 | 0.86 |
| 1996 | 0.83 |
| 1995 | 0.6 |
| 1994 | 0.72 |
| 1993 | 0.43 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
