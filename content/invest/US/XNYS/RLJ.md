---
title: " (RLJ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>RLJ</td></tr>
    <tr><td>Web</td><td><a href="https://www.rljlodgingtrust.com">www.rljlodgingtrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.01 |
| 2020 | 0.04 |
| 2019 | 1.32 |
| 2018 | 1.32 |
| 2017 | 1.32 |
| 2016 | 1.32 |
| 2015 | 1.32 |
| 2014 | 1.04 |
| 2013 | 0.615 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
