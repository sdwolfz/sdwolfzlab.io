---
title: "LAMB WESTON HLDGS INC (LW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>LAMB WESTON HLDGS INC</td></tr>
    <tr><td>Symbol</td><td>LW</td></tr>
    <tr><td>Web</td><td><a href="https://www.lambweston.com">www.lambweston.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.47 |
| 2020 | 0.92 |
| 2019 | 0.8 |
| 2018 | 0.764 |
| 2017 | 0.752 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
