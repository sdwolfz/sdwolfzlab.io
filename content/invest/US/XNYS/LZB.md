---
title: "LA-Z-BOY INC (LZB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>LA-Z-BOY INC</td></tr>
    <tr><td>Symbol</td><td>LZB</td></tr>
    <tr><td>Web</td><td><a href="https://www.la-z-boy.com">www.la-z-boy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 0.35 |
| 2019 | 0.53 |
| 2018 | 0.49 |
| 2017 | 0.45 |
| 2016 | 0.41 |
| 2015 | 0.34 |
| 2014 | 0.26 |
| 2013 | 0.14 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
