---
title: " (STAG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>STAG</td></tr>
    <tr><td>Web</td><td><a href="https://www.stagindustrial.com">www.stagindustrial.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.605 |
| 2020 | 1.44 |
| 2019 | 1.428 |
| 2018 | 1.416 |
| 2017 | 1.404 |
| 2016 | 1.392 |
| 2015 | 1.362 |
| 2014 | 1.29 |
| 2013 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
