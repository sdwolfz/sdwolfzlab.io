---
title: "COTY INC (COTY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>COTY INC</td></tr>
    <tr><td>Symbol</td><td>COTY</td></tr>
    <tr><td>Web</td><td><a href="https://www.coty.com">www.coty.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.125 |
| 2019 | 0.5 |
| 2018 | 0.5 |
| 2017 | 0.5 |
| 2016 | 0.4 |
| 2015 | 0.25 |
| 2014 | 0.2 |
| 2013 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
