---
title: "Blackrock Lat A (BRLA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Blackrock Lat A</td></tr>
    <tr><td>Symbol</td><td>BRLA</td></tr>
    <tr><td>Web</td><td><a href="https://www.blackrock.com">www.blackrock.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 34.89 |
| 2018 | 23.55 |
| 2017 | 13.0 |
| 2016 | 15.0 |
| 2015 | 21.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
