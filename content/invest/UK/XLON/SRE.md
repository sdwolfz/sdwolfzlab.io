---
title: "Sirius R E. (SRE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Sirius R E.</td></tr>
    <tr><td>Symbol</td><td>SRE</td></tr>
    <tr><td>Web</td><td><a href="https://www.sirius-real-estate.com">www.sirius-real-estate.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 3.57 |
| 2019 | 3.36 |
| 2018 | 3.16 |
| 2017 | 2.92 |
| 2016 | 2.22 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
