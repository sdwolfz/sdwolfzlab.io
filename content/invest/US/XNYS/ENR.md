---
title: "ENERGIZER HOLDINGS INC (ENR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ENERGIZER HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>ENR</td></tr>
    <tr><td>Web</td><td><a href="https://www.energizerholdings.com">www.energizerholdings.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.6 |
| 2020 | 1.2 |
| 2019 | 1.2 |
| 2018 | 1.17 |
| 2017 | 1.115 |
| 2016 | 1.025 |
| 2015 | 1.5 |
| 2014 | 2.0 |
| 2013 | 1.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
