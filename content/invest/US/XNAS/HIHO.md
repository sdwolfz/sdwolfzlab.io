---
title: "HIGHWAY HLDGS (HIHO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HIGHWAY HLDGS</td></tr>
    <tr><td>Symbol</td><td>HIHO</td></tr>
    <tr><td>Web</td><td><a href="https://www.highwayholdings.com">www.highwayholdings.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.02 |
| 2020 | 0.24 |
| 2018 | 0.35 |
| 2017 | 0.29 |
| 2016 | 0.3 |
| 2015 | 0.4 |
| 2014 | 0.24 |
| 2013 | 0.11 |
| 2012 | 0.09 |
| 2011 | 0.28 |
| 2010 | 0.16 |
| 2009 | 0.03 |
| 2007 | 0.035 |
| 2006 | 0.36 |
| 2005 | 0.4 |
| 2004 | 0.1 |
| 2003 | 0.08 |
| 1999 | 0.044 |
| 1998 | 0.044 |
| 1997 | 0.013 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
