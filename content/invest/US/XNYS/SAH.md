---
title: "SONIC AUTOMOTIVE INC (SAH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SONIC AUTOMOTIVE INC</td></tr>
    <tr><td>Symbol</td><td>SAH</td></tr>
    <tr><td>Web</td><td><a href="https://www.sonicautomotive.com">www.sonicautomotive.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.22 |
| 2020 | 0.4 |
| 2019 | 0.4 |
| 2018 | 0.24 |
| 2017 | 0.2 |
| 2016 | 0.2 |
| 2015 | 0.113 |
| 2014 | 0.1 |
| 2013 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
