---
title: " (EDU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>EDU</td></tr>
    <tr><td>Web</td><td><a href="https://www.neworiental.org">www.neworiental.org</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2012 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
