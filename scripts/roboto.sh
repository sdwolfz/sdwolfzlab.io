#!/usr/bin/env sh

set -ex

ROBOTO_NAME=Roboto
ROBOTO_SHA=1a70915f0169d0a92414ae58337809372ed09b75f877fc570e905abe83d50550
ROBOTO_ZIP="$ROBOTO_NAME".zip
ROBOTO_URL='https://fonts.google.com/download?family=Roboto'
ROBOTO_TMP=./tmp

rm -rf "$ROBOTO_TMP"
mkdir -p "$ROBOTO_TMP"
wget "$ROBOTO_URL" -O "$ROBOTO_TMP"/"$ROBOTO_ZIP"
sha256sum "$ROBOTO_TMP"/"$ROBOTO_ZIP"
echo "$ROBOTO_SHA  $ROBOTO_TMP/$ROBOTO_ZIP" | sha256sum -c -
unzip "$ROBOTO_TMP"/"$ROBOTO_ZIP" -d "$ROBOTO_TMP"

cp -r "$ROBOTO_TMP"/Roboto-*.ttf static/webfonts/
