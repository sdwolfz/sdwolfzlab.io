---
title: "Halfords (HFD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Halfords</td></tr>
    <tr><td>Symbol</td><td>HFD</td></tr>
    <tr><td>Web</td><td><a href="https://www.halfords.com">www.halfords.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.18 |
| 2019 | 18.57 |
| 2018 | 18.0 |
| 2017 | 17.5 |
| 2016 | 17.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
