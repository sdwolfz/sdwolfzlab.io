---
title: "VERU INC (VERU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>VERU INC</td></tr>
    <tr><td>Symbol</td><td>VERU</td></tr>
    <tr><td>Web</td><td><a href="https://www.verupharma.com">www.verupharma.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 0.14 |
| 2013 | 0.27 |
| 2012 | 0.23 |
| 2011 | 0.2 |
| 2010 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
