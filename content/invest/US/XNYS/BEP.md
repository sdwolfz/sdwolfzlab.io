---
title: " (BEP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BEP</td></tr>
    <tr><td>Web</td><td><a href="https://www.bep.brookfield.com">www.bep.brookfield.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.351 |
| 2019 | 2.06 |
| 2018 | 2.45 |
| 2017 | 1.872 |
| 2016 | 1.78 |
| 2015 | 1.66 |
| 2014 | 1.419 |
| 2013 | 1.089 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
