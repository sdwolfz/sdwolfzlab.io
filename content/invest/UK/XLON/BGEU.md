---
title: "Baillie Geu (BGEU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Baillie Geu</td></tr>
    <tr><td>Symbol</td><td>BGEU</td></tr>
    <tr><td>Web</td><td><a href="https://www.bgeuropeangrowth.com">www.bgeuropeangrowth.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 3.5 |
| 2019 | 31.0 |
| 2018 | 27.0 |
| 2017 | 23.0 |
| 2016 | 19.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
