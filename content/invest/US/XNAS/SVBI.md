---
title: "SEVERN BANCORP INC (SVBI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SEVERN BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>SVBI</td></tr>
    <tr><td>Web</td><td><a href="https://www.severnbank.com">www.severnbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.05 |
| 2020 | 0.16 |
| 2019 | 0.14 |
| 2018 | 0.12 |
| 2009 | 0.09 |
| 2008 | 0.24 |
| 2007 | 0.24 |
| 2006 | 0.24 |
| 2005 | 0.24 |
| 2004 | 0.42 |
| 2003 | 0.34 |
| 2002 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
