---
title: "CANADIAN NATURAL RESOURCES LTD (CNQ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CANADIAN NATURAL RESOURCES LTD</td></tr>
    <tr><td>Symbol</td><td>CNQ</td></tr>
    <tr><td>Web</td><td><a href="https://www.cnrl.com">www.cnrl.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.28 |
| 2019 | 1.127 |
| 2018 | 1.029 |
| 2017 | 0.847 |
| 2016 | 0.707 |
| 2015 | 0.862 |
| 2014 | 0.9 |
| 2013 | 0.325 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
