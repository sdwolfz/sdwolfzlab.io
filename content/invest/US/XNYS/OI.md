---
title: "O-I GLASS INC (OI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>O-I GLASS INC</td></tr>
    <tr><td>Symbol</td><td>OI</td></tr>
    <tr><td>Web</td><td><a href="https://www.o-i.com">www.o-i.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.05 |
| 2019 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
