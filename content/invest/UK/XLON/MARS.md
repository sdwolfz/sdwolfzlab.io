---
title: "Marstons (MARS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Marstons</td></tr>
    <tr><td>Symbol</td><td>MARS</td></tr>
    <tr><td>Web</td><td><a href="https://www.marstons.co.uk">www.marstons.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 4.8 |
| 2019 | 7.5 |
| 2018 | 7.5 |
| 2017 | 7.5 |
| 2016 | 7.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
