---
title: " (CHSCN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CHSCN</td></tr>
    <tr><td>Web</td><td><a href="https://www.chsinc.com">www.chsinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.444 |
| 2020 | 1.775 |
| 2019 | 1.775 |
| 2018 | 1.775 |
| 2017 | 1.775 |
| 2016 | 1.775 |
| 2015 | 1.775 |
| 2014 | 1.435 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
