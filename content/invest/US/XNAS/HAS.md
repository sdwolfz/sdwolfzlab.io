---
title: "HASBRO INC (HAS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HASBRO INC</td></tr>
    <tr><td>Symbol</td><td>HAS</td></tr>
    <tr><td>Web</td><td><a href="https://www.hasbro.com">www.hasbro.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.36 |
| 2020 | 2.72 |
| 2019 | 2.67 |
| 2018 | 2.46 |
| 2017 | 2.22 |
| 2016 | 1.99 |
| 2015 | 1.81 |
| 2014 | 1.69 |
| 2013 | 1.2 |
| 2012 | 1.74 |
| 2011 | 1.15 |
| 2010 | 0.95 |
| 2009 | 0.8 |
| 2008 | 0.76 |
| 2007 | 0.6 |
| 2006 | 0.45 |
| 2005 | 0.33 |
| 2004 | 0.21 |
| 2003 | 0.15 |
| 2002 | 0.12 |
| 2001 | 0.12 |
| 2000 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
