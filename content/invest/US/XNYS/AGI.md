---
title: "ALAMOS GOLD INC (AGI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ALAMOS GOLD INC</td></tr>
    <tr><td>Symbol</td><td>AGI</td></tr>
    <tr><td>Web</td><td><a href="https://www.alamosgold.com">www.alamosgold.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.065 |
| 2019 | 0.04 |
| 2018 | 0.02 |
| 2017 | 0.02 |
| 2016 | 0.02 |
| 2015 | 0.05 |
| 2014 | 0.2 |
| 2013 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
