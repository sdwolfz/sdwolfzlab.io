---
title: "AIR LEASE CORP (AL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AIR LEASE CORP</td></tr>
    <tr><td>Symbol</td><td>AL</td></tr>
    <tr><td>Web</td><td><a href="https://www.airleasecorp.com">www.airleasecorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.16 |
| 2020 | 0.61 |
| 2019 | 0.54 |
| 2018 | 0.43 |
| 2017 | 0.325 |
| 2016 | 0.225 |
| 2015 | 0.17 |
| 2014 | 0.13 |
| 2013 | 0.055 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
