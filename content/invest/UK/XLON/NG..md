---
title: "National Grid (NG.)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>National Grid</td></tr>
    <tr><td>Symbol</td><td>NG.</td></tr>
    <tr><td>Web</td><td><a href="https://www.nationalgrid.com">www.nationalgrid.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 48.57 |
| 2019 | 47.34 |
| 2018 | 128.97 |
| 2017 | 44.27 |
| 2016 | 43.34 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
