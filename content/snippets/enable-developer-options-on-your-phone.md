---
title: "Enable 'Developer options' on your phone"
date: 2020-11-15T12:21:12Z
type: posts
slug: enable-developer-options-on-your-phone
series: ["Android"]
tags: ["android"]
---

1. Open your phone's `Settings`.
2. Go to `About phone` > `Software information`.
3. Tap 7 times in quick succession on `Build number`.
4. Unlock your phone using your passcode, fingerprint, or pattern.
5. Go back to the `Settings` main menu, you will see `Developer options` at the
   bottom.
6. Enable whatever you want, most likely you are after the `USB debugging`
   setting.

Don't forget to disable `Developer options` when done with your work.
