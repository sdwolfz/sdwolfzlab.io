---
title: "FIRST UNITED CORP (FUNC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST UNITED CORP</td></tr>
    <tr><td>Symbol</td><td>FUNC</td></tr>
    <tr><td>Web</td><td><a href="https://www.mybank.com">www.mybank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 0.52 |
| 2019 | 0.4 |
| 2018 | 0.27 |
| 2010 | 0.13 |
| 2009 | 0.8 |
| 2008 | 0.8 |
| 2007 | 0.78 |
| 2006 | 0.76 |
| 2005 | 0.74 |
| 2004 | 0.72 |
| 2003 | 0.7 |
| 2002 | 0.68 |
| 2001 | 0.66 |
| 2000 | 0.64 |
| 1999 | 0.62 |
| 1998 | 0.6 |
| 1997 | 0.56 |
| 1996 | 0.52 |
| 1995 | 0.48 |
| 1994 | 0.5 |
| 1993 | 0.78 |
| 1992 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
