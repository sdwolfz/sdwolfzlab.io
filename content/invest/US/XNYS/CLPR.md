---
title: " (CLPR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CLPR</td></tr>
    <tr><td>Web</td><td><a href="https://www.clipperrealty.com">www.clipperrealty.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.095 |
| 2020 | 0.38 |
| 2019 | 0.38 |
| 2018 | 0.38 |
| 2017 | 0.37 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
