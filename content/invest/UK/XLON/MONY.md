---
title: "Moneysupermarket.Com (MONY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Moneysupermarket.Com</td></tr>
    <tr><td>Symbol</td><td>MONY</td></tr>
    <tr><td>Web</td><td><a href="https://www.moneysupermarket.com">www.moneysupermarket.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 11.71 |
| 2019 | 11.71 |
| 2018 | 11.05 |
| 2017 | 10.44 |
| 2016 | 9.85 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
