---
title: "ZYNGA INC (ZNGA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ZYNGA INC</td></tr>
    <tr><td>Symbol</td><td>ZNGA</td></tr>
    <tr><td>Web</td><td><a href="https://www.zynga.com">www.zynga.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
