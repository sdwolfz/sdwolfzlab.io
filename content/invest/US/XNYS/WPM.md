---
title: "WHEATON PRECIOUS METALS CORP (WPM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>WHEATON PRECIOUS METALS CORP</td></tr>
    <tr><td>Symbol</td><td>WPM</td></tr>
    <tr><td>Web</td><td><a href="https://www.wheatonpm.com">www.wheatonpm.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.42 |
| 2019 | 0.36 |
| 2018 | 0.36 |
| 2017 | 0.26 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
