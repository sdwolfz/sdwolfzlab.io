---
title: "ACUITY BRANDS INC (AYI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ACUITY BRANDS INC</td></tr>
    <tr><td>Symbol</td><td>AYI</td></tr>
    <tr><td>Web</td><td><a href="https://www.acuitybrands.com">www.acuitybrands.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.26 |
| 2020 | 0.52 |
| 2019 | 0.52 |
| 2018 | 0.52 |
| 2017 | 0.52 |
| 2016 | 0.52 |
| 2015 | 0.52 |
| 2014 | 0.52 |
| 2013 | 0.26 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
