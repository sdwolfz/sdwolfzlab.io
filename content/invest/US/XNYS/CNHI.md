---
title: "CNH INDUSTRIAL NV (CNHI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CNH INDUSTRIAL NV</td></tr>
    <tr><td>Symbol</td><td>CNHI</td></tr>
    <tr><td>Web</td><td><a href="https://www.cnhindustrial.com">www.cnhindustrial.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 0.118 |
| 2016 | 0.148 |
| 2015 | 0.2 |
| 2014 | 0.277 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
