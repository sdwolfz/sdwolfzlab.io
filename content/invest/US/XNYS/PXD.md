---
title: "PIONEER NATURAL RESOURCES CO (PXD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PIONEER NATURAL RESOURCES CO</td></tr>
    <tr><td>Symbol</td><td>PXD</td></tr>
    <tr><td>Web</td><td><a href="https://www.pxd.com">www.pxd.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.56 |
| 2020 | 2.2 |
| 2019 | 1.2 |
| 2018 | 0.32 |
| 2017 | 0.08 |
| 2016 | 0.08 |
| 2015 | 0.08 |
| 2014 | 0.08 |
| 2013 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
