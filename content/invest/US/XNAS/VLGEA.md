---
title: "VILLAGE SUPER MARKET INC (VLGEA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>VILLAGE SUPER MARKET INC</td></tr>
    <tr><td>Symbol</td><td>VLGEA</td></tr>
    <tr><td>Web</td><td><a href="https://www.villagesupermarkets.com">www.villagesupermarkets.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 1.0 |
| 2019 | 1.25 |
| 2018 | 1.0 |
| 2017 | 1.0 |
| 2016 | 0.75 |
| 2015 | 1.0 |
| 2014 | 1.0 |
| 2013 | 1.0 |
| 2012 | 2.25 |
| 2011 | 0.3 |
| 2010 | 2.0 |
| 2009 | 0.885 |
| 2008 | 4.0 |
| 2007 | 0.92 |
| 2006 | 1.06 |
| 2005 | 0.92 |
| 2004 | 0.31 |
| 2003 | 0.13 |
| 1992 | 0.038 |
| 1991 | 0.15 |
| 1990 | 0.15 |
| 1989 | 0.15 |
| 1988 | 0.038 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
