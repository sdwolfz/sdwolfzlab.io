---
title: "DELTA AIR LINES INC (DAL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DELTA AIR LINES INC</td></tr>
    <tr><td>Symbol</td><td>DAL</td></tr>
    <tr><td>Web</td><td><a href="https://www.delta.com">www.delta.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.403 |
| 2019 | 1.506 |
| 2018 | 1.31 |
| 2017 | 1.016 |
| 2016 | 0.676 |
| 2015 | 0.45 |
| 2014 | 0.3 |
| 2013 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
