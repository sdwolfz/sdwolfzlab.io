---
title: "INVESTCORP CREDIT MGMT BDC INC (ICMB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>INVESTCORP CREDIT MGMT BDC INC</td></tr>
    <tr><td>Symbol</td><td>ICMB</td></tr>
    <tr><td>Web</td><td><a href="https://www.icmbdc.com">www.icmbdc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.18 |
| 2020 | 0.76 |
| 2019 | 1.0 |
| 2018 | 1.0 |
| 2017 | 1.0 |
| 2016 | 1.406 |
| 2015 | 1.818 |
| 2014 | 1.194 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
