---
title: "Extract text from page using XPath"
date: 2023-03-30T20:17:39Z
type: posts
slug: extract-text-from-page-using-xpath
tags: ["javascript"]
---

```js
let result = document.evaluate(
  '/html/body/div/main/div[1]/div[3]/div',
  document,
  null,
  XPathResult.STRING_TYPE,
  null
)?.stringValue || ''

return result
```
