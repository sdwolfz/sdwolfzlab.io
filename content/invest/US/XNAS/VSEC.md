---
title: "VSE CORP (VSEC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>VSE CORP</td></tr>
    <tr><td>Symbol</td><td>VSEC</td></tr>
    <tr><td>Web</td><td><a href="https://www.vsecorp.com">www.vsecorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.27 |
| 2020 | 0.36 |
| 2019 | 0.34 |
| 2018 | 0.3 |
| 2017 | 0.26 |
| 2016 | 0.4 |
| 2015 | 0.42 |
| 2014 | 0.38 |
| 2013 | 0.34 |
| 2012 | 0.3 |
| 2011 | 0.26 |
| 2010 | 0.22 |
| 2009 | 0.19 |
| 2008 | 0.17 |
| 2007 | 0.22 |
| 2006 | 0.26 |
| 2005 | 0.22 |
| 2004 | 0.18 |
| 2003 | 0.16 |
| 2002 | 0.16 |
| 2001 | 0.16 |
| 2000 | 0.156 |
| 1999 | 0.144 |
| 1998 | 0.504 |
| 1997 | 0.18 |
| 1996 | 0.258 |
| 1995 | 0.325 |
| 1994 | 0.305 |
| 1993 | 0.285 |
| 1992 | 0.28 |
| 1991 | 0.28 |
| 1990 | 0.28 |
| 1989 | 0.28 |
| 1988 | 0.07 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
