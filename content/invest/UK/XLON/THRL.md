---
title: "Target Healthc. (THRL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Target Healthc.</td></tr>
    <tr><td>Symbol</td><td>THRL</td></tr>
    <tr><td>Web</td><td><a href="https://www.targethealthcarereit.co.uk">www.targethealthcarereit.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.65 |
| 2019 | 6.58 |
| 2018 | 6.41 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
