---
title: "ALLY FINANCIAL INC (ALLY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ALLY FINANCIAL INC</td></tr>
    <tr><td>Symbol</td><td>ALLY</td></tr>
    <tr><td>Web</td><td><a href="https://www.ally.com">www.ally.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.38 |
| 2020 | 0.76 |
| 2019 | 0.68 |
| 2018 | 0.56 |
| 2017 | 0.4 |
| 2016 | 0.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
