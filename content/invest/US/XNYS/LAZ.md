---
title: "LAZARD LTD (LAZ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>LAZARD LTD</td></tr>
    <tr><td>Symbol</td><td>LAZ</td></tr>
    <tr><td>Web</td><td><a href="https://www.lazard.com">www.lazard.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.94 |
| 2020 | 1.88 |
| 2019 | 2.35 |
| 2018 | 3.03 |
| 2017 | 2.81 |
| 2016 | 2.69 |
| 2015 | 1.35 |
| 2014 | 1.2 |
| 2013 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
