---
title: "TERADYNE INC (TER)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TERADYNE INC</td></tr>
    <tr><td>Symbol</td><td>TER</td></tr>
    <tr><td>Web</td><td><a href="https://www.teradyne.com">www.teradyne.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.4 |
| 2019 | 0.36 |
| 2018 | 0.36 |
| 2017 | 0.28 |
| 2016 | 0.24 |
| 2015 | 0.24 |
| 2014 | 0.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
