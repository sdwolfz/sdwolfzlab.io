---
title: "CONTED SA (CNTE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>CONTED SA</td></tr>
    <tr><td>Symbol</td><td>CNTE</td></tr>
    <tr><td>Web</td><td><a href="https://www.conted.ro">www.conted.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.81 |
| 2015 | 4.207 |
| 2014 | 7.04975 |
| 2013 | 11.277 |
| 2012 | 9.81 |
| 2010 | 3.024 |
| 2009 | 6.41 |
| 2008 | 5.64 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
