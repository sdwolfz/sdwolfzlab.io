---
title: "DIGI INTERNATIONAL INC (DGII)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>DIGI INTERNATIONAL INC</td></tr>
    <tr><td>Symbol</td><td>DGII</td></tr>
    <tr><td>Web</td><td><a href="https://www.digi.com">www.digi.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
