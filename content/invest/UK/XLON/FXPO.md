---
title: "Ferrexpo (FXPO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Ferrexpo</td></tr>
    <tr><td>Symbol</td><td>FXPO</td></tr>
    <tr><td>Web</td><td><a href="https://www.ferrexpo.com">www.ferrexpo.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 13.2 |
| 2018 | 23.1 |
| 2017 | 16.5 |
| 2016 | 6.6 |
| 2015 | 3.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
