---
title: "How to Use: flameshot"
date: 2020-12-06T13:26:47Z
type: posts
slug: how-to-use-flameshot
series: ["How to Use"]
tags: ["linux", "productivity", "showcase"]
---

## Install

```sh
sudo pacman -S flameshot
```

## Key Bindings

The default system screenshot key bindings are bad, so overwrite them:

| Key            | Description                             |
|----------------|-----------------------------------------|
| `Print`        | Take a screenshot of the entire screen. |
| `Ctrl + Print` | Take a screenshot of a region.          |

> **Note:** The `<Primary>` key you see below means `<Ctrl>`!

Set them with:

```sh
xfconf-query --reset                 \
  --channel xfce4-keyboard-shortcuts \
  --property /commands/custom/'Print'

xfconf-query --create                 \
  --channel xfce4-keyboard-shortcuts  \
  --property /commands/custom/'Print' \
  --type string                       \
  --set 'flameshot screen -c'

xfconf-query --reset                 \
  --channel xfce4-keyboard-shortcuts \
  --property /commands/custom/'<Primary>Print'

xfconf-query --create                          \
  --channel xfce4-keyboard-shortcuts           \
  --property /commands/custom/'<Primary>Print' \
  --type string                                \
  --set 'flameshot gui'
```

Make sure to run these in your automation scripts so they don't get lost.
