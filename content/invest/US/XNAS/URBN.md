---
title: "URBAN OUTFITTERS INC (URBN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>URBAN OUTFITTERS INC</td></tr>
    <tr><td>Symbol</td><td>URBN</td></tr>
    <tr><td>Web</td><td><a href="https://www.urbn.com">www.urbn.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
