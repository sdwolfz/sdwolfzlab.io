---
title: "APTIV PLC (APTV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>APTIV PLC</td></tr>
    <tr><td>Symbol</td><td>APTV</td></tr>
    <tr><td>Web</td><td><a href="https://www.aptiv.com">www.aptiv.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.22 |
| 2019 | 0.88 |
| 2018 | 0.88 |
| 2017 | 17.273 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
