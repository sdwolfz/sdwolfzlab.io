---
title: "DONEGAL GROUP (DGICB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>DONEGAL GROUP</td></tr>
    <tr><td>Symbol</td><td>DGICB</td></tr>
    <tr><td>Web</td><td><a href="https://www.donegalgroup.com">www.donegalgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.275 |
| 2020 | 0.525 |
| 2019 | 0.508 |
| 2018 | 0.498 |
| 2017 | 0.488 |
| 2016 | 0.478 |
| 2015 | 0.469 |
| 2014 | 0.463 |
| 2013 | 0.455 |
| 2012 | 0.438 |
| 2011 | 0.425 |
| 2010 | 0.408 |
| 2009 | 0.393 |
| 2008 | 0.355 |
| 2007 | 0.303 |
| 2006 | 0.225 |
| 2005 | 0.36 |
| 2004 | 0.415 |
| 2003 | 0.39 |
| 2002 | 0.36 |
| 2001 | 0.36 |
| 2000 | 0.36 |
| 1999 | 0.355 |
| 1998 | 0.38 |
| 1997 | 0.43 |
| 1996 | 0.43 |
| 1995 | 0.39 |
| 1994 | 0.35 |
| 1993 | 0.24 |
| 1992 | 0.27 |
| 1991 | 0.24 |
| 1990 | 0.23 |
| 1989 | 0.2 |
| 1988 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
