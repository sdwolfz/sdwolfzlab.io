---
title: "ATMOS ENERGY CORP (ATO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ATMOS ENERGY CORP</td></tr>
    <tr><td>Symbol</td><td>ATO</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.25 |
| 2020 | 2.35 |
| 2019 | 2.15 |
| 2018 | 1.98 |
| 2017 | 1.835 |
| 2016 | 1.71 |
| 2015 | 1.59 |
| 2014 | 1.5 |
| 2013 | 0.72 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
