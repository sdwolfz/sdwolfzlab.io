---
title: "FREIGHTCAR AMERICA INC (RAIL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FREIGHTCAR AMERICA INC</td></tr>
    <tr><td>Symbol</td><td>RAIL</td></tr>
    <tr><td>Web</td><td><a href="https://www.freightcaramerica.com">www.freightcaramerica.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 0.27 |
| 2016 | 0.36 |
| 2015 | 0.36 |
| 2014 | 0.24 |
| 2013 | 0.24 |
| 2012 | 0.24 |
| 2010 | 0.06 |
| 2009 | 0.24 |
| 2008 | 0.24 |
| 2007 | 0.24 |
| 2006 | 0.15 |
| 2005 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
