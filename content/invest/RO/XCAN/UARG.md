---
title: "ARGUS SA Constanta (UARG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>ARGUS SA Constanta</td></tr>
    <tr><td>Symbol</td><td>UARG</td></tr>
    <tr><td>Web</td><td><a href="https://www.argus-oil.ro">www.argus-oil.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2008 | 0.07 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
