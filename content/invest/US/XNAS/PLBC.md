---
title: "PLUMAS BANCORP (PLBC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PLUMAS BANCORP</td></tr>
    <tr><td>Symbol</td><td>PLBC</td></tr>
    <tr><td>Web</td><td><a href="https://www.plumasbank.com">www.plumasbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 0.36 |
| 2019 | 0.46 |
| 2018 | 0.36 |
| 2017 | 0.28 |
| 2016 | 0.1 |
| 2008 | 0.24 |
| 2007 | 0.3 |
| 2006 | 0.26 |
| 2005 | 0.27 |
| 2004 | 0.28 |
| 2003 | 0.24 |
| 2002 | 0.405 |
| 2001 | 0.3 |
| 2000 | 0.3 |
| 1999 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
