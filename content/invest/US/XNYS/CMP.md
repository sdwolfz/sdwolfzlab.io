---
title: "COMPASS MINERALS INTERNATIONAL INC (CMP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>COMPASS MINERALS INTERNATIONAL INC</td></tr>
    <tr><td>Symbol</td><td>CMP</td></tr>
    <tr><td>Web</td><td><a href="https://www.compassminerals.com">www.compassminerals.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.72 |
| 2020 | 2.88 |
| 2019 | 2.88 |
| 2018 | 2.88 |
| 2017 | 2.88 |
| 2016 | 2.78 |
| 2015 | 2.64 |
| 2014 | 2.4 |
| 2013 | 1.09 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
