---
title: "TENNANT CO (TNC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TENNANT CO</td></tr>
    <tr><td>Symbol</td><td>TNC</td></tr>
    <tr><td>Web</td><td><a href="https://www.tennantco.com">www.tennantco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.46 |
| 2020 | 0.89 |
| 2019 | 0.88 |
| 2018 | 0.85 |
| 2017 | 0.84 |
| 2016 | 0.81 |
| 2015 | 0.8 |
| 2014 | 0.78 |
| 2013 | 0.36 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
