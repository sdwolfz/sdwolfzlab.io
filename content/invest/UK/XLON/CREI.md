---
title: "Custodian Reit (CREI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Custodian Reit</td></tr>
    <tr><td>Symbol</td><td>CREI</td></tr>
    <tr><td>Web</td><td><a href="https://www.custodianreit.com">www.custodianreit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.65 |
| 2019 | 6.55 |
| 2018 | 6.45 |
| 2017 | 6.35 |
| 2016 | 6.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
