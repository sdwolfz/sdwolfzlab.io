---
title: "FIDES CAPITAL SA (PVBS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>FIDES CAPITAL SA</td></tr>
    <tr><td>Symbol</td><td>PVBS</td></tr>
    <tr><td>Web</td><td><a href="https://www.fidescapital.ro">www.fidescapital.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 0.057 |
| 2014 | 0.0762 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
