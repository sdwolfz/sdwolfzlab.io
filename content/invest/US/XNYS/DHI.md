---
title: "DR HORTON INC (DHI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DR HORTON INC</td></tr>
    <tr><td>Symbol</td><td>DHI</td></tr>
    <tr><td>Web</td><td><a href="https://www.drhorton.com">www.drhorton.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.4 |
| 2020 | 0.725 |
| 2019 | 0.625 |
| 2018 | 0.525 |
| 2017 | 0.425 |
| 2016 | 0.34 |
| 2015 | 0.269 |
| 2014 | 0.202 |
| 2012 | 0.038 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
