---
title: "Telecom Plus (TEP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Telecom Plus</td></tr>
    <tr><td>Symbol</td><td>TEP</td></tr>
    <tr><td>Web</td><td><a href="https://www.telecomplus.co.uk">www.telecomplus.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 57.0 |
| 2019 | 52.0 |
| 2018 | 50.0 |
| 2017 | 48.0 |
| 2016 | 46.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
