---
title: " (FMX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>FMX</td></tr>
    <tr><td>Web</td><td><a href="https://www.femsa.com">www.femsa.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.562 |
| 2020 | 1.433 |
| 2019 | 1.483 |
| 2018 | 1.388 |
| 2017 | 1.347 |
| 2016 | 1.347 |
| 2015 | 1.372 |
| 2013 | 3.117 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
