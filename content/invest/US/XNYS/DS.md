---
title: "DRIVE SHACK INC (DS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DRIVE SHACK INC</td></tr>
    <tr><td>Symbol</td><td>DS</td></tr>
    <tr><td>Web</td><td><a href="https://www.driveshack.com">www.driveshack.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
