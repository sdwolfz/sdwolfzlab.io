---
title: "TURNING POINT BRANDS INC (TPB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TURNING POINT BRANDS INC</td></tr>
    <tr><td>Symbol</td><td>TPB</td></tr>
    <tr><td>Web</td><td><a href="https://www.turningpointbrands.com">www.turningpointbrands.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.11 |
| 2020 | 0.2 |
| 2019 | 0.18 |
| 2018 | 0.165 |
| 2017 | 0.04 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
