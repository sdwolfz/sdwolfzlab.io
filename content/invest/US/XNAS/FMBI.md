---
title: "FIRST MIDWEST BANCORP INC (FMBI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST MIDWEST BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>FMBI</td></tr>
    <tr><td>Web</td><td><a href="https://www.firstmidwest.com">www.firstmidwest.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.14 |
| 2020 | 0.56 |
| 2019 | 0.54 |
| 2018 | 0.45 |
| 2017 | 0.39 |
| 2016 | 0.36 |
| 2015 | 0.36 |
| 2014 | 0.31 |
| 2013 | 0.16 |
| 2012 | 0.04 |
| 2011 | 0.04 |
| 2010 | 0.04 |
| 2009 | 0.04 |
| 2008 | 1.155 |
| 2007 | 1.195 |
| 2006 | 1.12 |
| 2005 | 1.015 |
| 2004 | 0.9 |
| 2003 | 0.79 |
| 2002 | 0.7 |
| 2001 | 0.77 |
| 2000 | 0.74 |
| 1999 | 0.9 |
| 1998 | 0.915 |
| 1997 | 0.825 |
| 1996 | 0.83 |
| 1995 | 0.76 |
| 1994 | 0.68 |
| 1993 | 0.45 |
| 1992 | 0.52 |
| 1991 | 0.52 |
| 1990 | 0.52 |
| 1989 | 0.54 |
| 1988 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
