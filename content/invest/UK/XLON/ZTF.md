---
title: "Zotefoams (ZTF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Zotefoams</td></tr>
    <tr><td>Symbol</td><td>ZTF</td></tr>
    <tr><td>Web</td><td><a href="https://www.zotefoams.com">www.zotefoams.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 2.03 |
| 2018 | 6.12 |
| 2017 | 5.93 |
| 2016 | 5.75 |
| 2015 | 5.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
