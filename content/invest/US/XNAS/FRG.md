---
title: "FRANCHISE GROUP INC (FRG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FRANCHISE GROUP INC</td></tr>
    <tr><td>Symbol</td><td>FRG</td></tr>
    <tr><td>Web</td><td><a href="https://www.franchisegrp.com">www.franchisegrp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.75 |
| 2020 | 1.125 |
| 2019 | 0.25 |
| 2018 | 0.48 |
| 2017 | 0.64 |
| 2016 | 0.64 |
| 2015 | 0.48 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
