---
title: "DONALDSON CO INC (DCI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DONALDSON CO INC</td></tr>
    <tr><td>Symbol</td><td>DCI</td></tr>
    <tr><td>Web</td><td><a href="https://www.donaldson.com">www.donaldson.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.21 |
| 2020 | 0.84 |
| 2019 | 0.82 |
| 2018 | 0.75 |
| 2017 | 0.71 |
| 2016 | 0.695 |
| 2015 | 0.675 |
| 2014 | 0.635 |
| 2013 | 0.27 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
