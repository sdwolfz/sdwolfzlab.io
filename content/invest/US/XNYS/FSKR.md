---
title: "FS KKR CAPITAL CORPORATION II (FSKR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FS KKR CAPITAL CORPORATION II</td></tr>
    <tr><td>Symbol</td><td>FSKR</td></tr>
    <tr><td>Web</td><td><a href="https://www.fsinvestments.com">www.fsinvestments.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.55 |
| 2020 | 1.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
