---
title: "AMERICAN WATER WORKS COMPANY INC (AWK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AMERICAN WATER WORKS COMPANY INC</td></tr>
    <tr><td>Symbol</td><td>AWK</td></tr>
    <tr><td>Web</td><td><a href="https://www.amwater.com">www.amwater.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.153 |
| 2020 | 2.15 |
| 2019 | 1.955 |
| 2018 | 1.78 |
| 2017 | 1.62 |
| 2016 | 1.465 |
| 2015 | 1.33 |
| 2014 | 1.21 |
| 2013 | 0.56 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
