---
title: "CROWN CRAFTS INC (CRWS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CROWN CRAFTS INC</td></tr>
    <tr><td>Symbol</td><td>CRWS</td></tr>
    <tr><td>Web</td><td><a href="https://www.crowncrafts.com">www.crowncrafts.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.08 |
| 2020 | 0.49 |
| 2019 | 0.57 |
| 2018 | 0.32 |
| 2017 | 0.32 |
| 2016 | 0.97 |
| 2015 | 0.32 |
| 2014 | 0.32 |
| 2013 | 0.32 |
| 2012 | 0.78 |
| 2011 | 0.13 |
| 2010 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
