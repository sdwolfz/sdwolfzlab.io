---
title: "HEADHUNTER GROUP PLC (HHR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HEADHUNTER GROUP PLC</td></tr>
    <tr><td>Symbol</td><td>HHR</td></tr>
    <tr><td>Web</td><td><a href="https://www.hh.ru">www.hh.ru</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.5 |
| 2019 | 0.36 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
