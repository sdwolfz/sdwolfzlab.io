---
title: "How to benchmark the speed of your SSD"
date: 2020-11-21T14:43:23Z
type: posts
slug: how-to-benchmark-speed-of-ssd
tags: ["linux", "performance"]
---

Use `fio` to do a benchamrk from the CLI.

## Install

```sh
# Manjaro / Arch
sudo pacman -S fio

# Alpine
apk add --update fio
```

## Usage

Run the benchamrk with:

```sh
fio --loops=5 --size=2000m --stonewall --ioengine=libaio --direct=1 \
  --name=Seqread     --bs=1m   --rw=read                            \
  --name=Seqwrite    --bs=1m   --rw=write                           \
  --name=512Kread    --bs=512k --rw=randread                        \
  --name=512Kwrite   --bs=512k --rw=randwrite                       \
  --name=4kQD32read  --bs=4k   --rw=randread  --iodepth=32          \
  --name=4kQD32write --bs=4k   --rw=randwrite --iodepth=32          \
  --filename=./fiotest.tmp
```

Then you can remove the temporary benchmark file:

```sh
rm -f ./fiotest.tmp
```
