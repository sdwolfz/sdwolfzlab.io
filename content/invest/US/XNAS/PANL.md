---
title: "PANGAEA LOGISTICS SOLUTIONS LTD (PANL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PANGAEA LOGISTICS SOLUTIONS LTD</td></tr>
    <tr><td>Symbol</td><td>PANL</td></tr>
    <tr><td>Web</td><td><a href="https://www.pangaeals.com">www.pangaeals.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.02 |
| 2019 | 0.105 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
