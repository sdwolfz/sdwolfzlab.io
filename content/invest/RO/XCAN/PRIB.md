---
title: "PRIMCOM SA BUCURESTI (PRIB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>PRIMCOM SA BUCURESTI</td></tr>
    <tr><td>Symbol</td><td>PRIB</td></tr>
    <tr><td>Web</td><td><a href="https://www.primcom.ro">www.primcom.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2010 | 11.6699 |
| 2009 | 1.5 |
| 2008 | 1.3206 |
| 2007 | 1.0 |
| 2005 | 0.5883 |
| 2004 | 1.7329 |
| 2003 | 0.2985 |
| 2002 | 0.2121 |
| 2001 | 0.2631 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
