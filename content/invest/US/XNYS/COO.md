---
title: "COOPER COS INC (COO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>COOPER COS INC</td></tr>
    <tr><td>Symbol</td><td>COO</td></tr>
    <tr><td>Web</td><td><a href="https://www.coopercos.com">www.coopercos.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.03 |
| 2020 | 0.06 |
| 2019 | 0.06 |
| 2018 | 0.06 |
| 2017 | 0.06 |
| 2016 | 0.06 |
| 2015 | 0.06 |
| 2014 | 0.06 |
| 2013 | 0.03 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
