---
title: "SUPERIOR GROUP OF COMPANIES INC (SGC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SUPERIOR GROUP OF COMPANIES INC</td></tr>
    <tr><td>Symbol</td><td>SGC</td></tr>
    <tr><td>Web</td><td><a href="https://www.superiorgroupofcompanies.com">www.superiorgroupofcompanies.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.4 |
| 2019 | 0.4 |
| 2018 | 0.39 |
| 2017 | 0.365 |
| 2016 | 0.34 |
| 2015 | 0.315 |
| 2014 | 0.57 |
| 2013 | 0.135 |
| 2012 | 1.08 |
| 2011 | 0.54 |
| 2010 | 0.54 |
| 2009 | 0.54 |
| 2008 | 0.54 |
| 2007 | 0.54 |
| 2006 | 0.54 |
| 2005 | 0.54 |
| 2004 | 0.54 |
| 2003 | 0.54 |
| 2002 | 0.54 |
| 2001 | 0.54 |
| 2000 | 0.54 |
| 1999 | 0.135 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
