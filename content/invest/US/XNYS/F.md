---
title: "FORD MOTOR CO (F)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FORD MOTOR CO</td></tr>
    <tr><td>Symbol</td><td>F</td></tr>
    <tr><td>Web</td><td><a href="https://www.ford.com">www.ford.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.15 |
| 2019 | 0.6 |
| 2018 | 0.73 |
| 2017 | 0.65 |
| 2016 | 1.0 |
| 2015 | 0.6 |
| 2014 | 0.5 |
| 2013 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
