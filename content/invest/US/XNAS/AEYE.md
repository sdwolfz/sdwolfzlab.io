---
title: "AUDIOEYE INC (AEYE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AUDIOEYE INC</td></tr>
    <tr><td>Symbol</td><td>AEYE</td></tr>
    <tr><td>Web</td><td><a href="https://www.audioeye.com">www.audioeye.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
