---
title: " (NMFC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>NMFC</td></tr>
    <tr><td>Web</td><td><a href="https://www.newmountainfinance.com">www.newmountainfinance.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.6 |
| 2020 | 1.24 |
| 2019 | 1.36 |
| 2018 | 1.36 |
| 2017 | 1.36 |
| 2016 | 1.36 |
| 2015 | 1.36 |
| 2014 | 1.48 |
| 2013 | 1.48 |
| 2012 | 1.71 |
| 2011 | 0.86 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
