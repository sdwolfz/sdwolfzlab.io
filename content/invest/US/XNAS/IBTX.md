---
title: "INDEPENDENT BANK GROUP INC (IBTX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>INDEPENDENT BANK GROUP INC</td></tr>
    <tr><td>Symbol</td><td>IBTX</td></tr>
    <tr><td>Web</td><td><a href="https://www.ibtx.com">www.ibtx.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.62 |
| 2020 | 1.05 |
| 2019 | 1.0 |
| 2018 | 0.54 |
| 2017 | 0.4 |
| 2016 | 0.34 |
| 2015 | 0.32 |
| 2014 | 0.24 |
| 2013 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
