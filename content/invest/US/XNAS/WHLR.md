---
title: " (WHLR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>WHLR</td></tr>
    <tr><td>Web</td><td><a href="https://www.whlr.us">www.whlr.us</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 1.073 |
| 2016 | 0.21 |
| 2015 | 0.245 |
| 2014 | 0.42 |
| 2013 | 0.42 |
| 2012 | 0.049 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
