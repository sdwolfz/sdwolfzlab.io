---
title: "AMERICAN RIVER BANKSHARES (AMRB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AMERICAN RIVER BANKSHARES</td></tr>
    <tr><td>Symbol</td><td>AMRB</td></tr>
    <tr><td>Web</td><td><a href="https://www.americanriverbank.com">www.americanriverbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.14 |
| 2020 | 0.28 |
| 2019 | 0.24 |
| 2018 | 0.2 |
| 2017 | 0.2 |
| 2009 | 0.286 |
| 2008 | 0.593 |
| 2007 | 0.75 |
| 2006 | 0.6 |
| 2005 | 0.52 |
| 2004 | 0.345 |
| 2003 | 0.36 |
| 2002 | 0.285 |
| 2001 | 0.265 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
