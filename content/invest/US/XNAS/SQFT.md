---
title: "PRESIDIO PPTY TR INC (SQFT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PRESIDIO PPTY TR INC</td></tr>
    <tr><td>Symbol</td><td>SQFT</td></tr>
    <tr><td>Web</td><td><a href="https://www.presidiopt.com">www.presidiopt.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.101 |
| 2020 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
