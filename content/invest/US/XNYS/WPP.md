---
title: " (WPP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>WPP</td></tr>
    <tr><td>Web</td><td><a href="https://www.wpp.com">www.wpp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.978 |
| 2020 | 3.053 |
| 2019 | 3.752 |
| 2018 | 3.972 |
| 2017 | 1.453 |
| 2015 | 0.11 |
| 2014 | 0.12 |
| 2013 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
