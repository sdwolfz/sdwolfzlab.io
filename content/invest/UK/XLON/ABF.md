---
title: "AB Foods (ABF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>AB Foods</td></tr>
    <tr><td>Symbol</td><td>ABF</td></tr>
    <tr><td>Web</td><td><a href="https://www.abf.co.uk">www.abf.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 46.35 |
| 2018 | 45.0 |
| 2017 | 41.0 |
| 2016 | 36.75 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
