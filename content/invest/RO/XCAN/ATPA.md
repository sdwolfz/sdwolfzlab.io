---
title: "ATHENEE PALACE SA BUCURESTI (ATPA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/bvb/">BVB</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/bvb/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>ATHENEE PALACE SA BUCURESTI</td></tr>
    <tr><td>Symbol</td><td>ATPA</td></tr>
    <tr><td>Web</td><td><a href="http://www.athenee-palace.ro">www.athenee-palace.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2000 | 0.017 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
