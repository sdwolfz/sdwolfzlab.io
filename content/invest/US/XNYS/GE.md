---
title: "GENERAL ELECTRIC CO (GE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GENERAL ELECTRIC CO</td></tr>
    <tr><td>Symbol</td><td>GE</td></tr>
    <tr><td>Web</td><td><a href="https://www.ge.com">www.ge.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.01 |
| 2020 | 0.04 |
| 2019 | 0.04 |
| 2018 | 0.37 |
| 2017 | 0.84 |
| 2016 | 0.93 |
| 2015 | 0.92 |
| 2014 | 0.89 |
| 2013 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
