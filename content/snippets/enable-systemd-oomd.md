---
title: "Enable systemd-oomd"
date: 2021-05-12T18:55:30Z
type: posts
slug: enable-systemd-oomd
tags: ["linux", "systemd"]
---

## Prerequisites

Make sure you have `systemd` installed with  at least version `248`:

```sh
pacman -Ss systemd | grep "core/systemd "

# core/systemd 248.2-1.1 [installed]
```

## Setup

Check if the service is active:

```sh
systemctl status systemd-oomd.service
```

Enable and start it with:

```sh
sudo systemctl enable systemd-oomd.service
sudo systemctl start  systemd-oomd.service
```

## Trial

Check it's working properly by starting htop in a terminal window:
```sh
htop
```

And in another fill up your memory with a process:

```sh
systemd-run --user tail /dev/zero
```

If all goes well that process will be killed an you won't notice any slowdown of
your machine. Worst case, and common before `systemd-oomd`, your machine would
have been usuable and you needed a forced restart.

## Config

Edit `/etc/systemd/oomd.conf` with your custom settings:

```conf
[OOM]
SwapUsedLimit=90%
DefaultMemoryPressureLimit=60%
DefaultMemoryPressureDurationSec=30s
```
