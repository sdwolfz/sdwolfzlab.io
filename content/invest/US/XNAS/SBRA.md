---
title: " (SBRA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SBRA</td></tr>
    <tr><td>Web</td><td><a href="https://www.sabrahealth.com">www.sabrahealth.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.6 |
| 2020 | 1.35 |
| 2019 | 1.8 |
| 2018 | 1.8 |
| 2017 | 1.73 |
| 2016 | 1.67 |
| 2015 | 1.6 |
| 2014 | 1.51 |
| 2013 | 1.36 |
| 2012 | 1.32 |
| 2011 | 0.96 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
