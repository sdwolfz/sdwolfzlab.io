---
title: " (MGP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>MGP</td></tr>
    <tr><td>Web</td><td><a href="https://www.mgmgrowthproperties.com">www.mgmgrowthproperties.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.495 |
| 2020 | 1.939 |
| 2019 | 1.873 |
| 2018 | 1.735 |
| 2017 | 1.597 |
| 2016 | 1.037 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
