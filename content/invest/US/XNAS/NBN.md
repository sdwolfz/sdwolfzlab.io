---
title: "NORTHEAST BK LEWISTON ME (NBN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NORTHEAST BK LEWISTON ME</td></tr>
    <tr><td>Symbol</td><td>NBN</td></tr>
    <tr><td>Web</td><td><a href="https://www.northeastbank.com">www.northeastbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.02 |
| 2020 | 0.04 |
| 2019 | 0.04 |
| 2018 | 0.04 |
| 2017 | 0.04 |
| 2016 | 0.04 |
| 2015 | 0.04 |
| 2014 | 0.12 |
| 2013 | 0.36 |
| 2012 | 0.36 |
| 2011 | 0.36 |
| 2010 | 0.36 |
| 2009 | 0.36 |
| 2008 | 0.36 |
| 2007 | 0.36 |
| 2006 | 0.36 |
| 2005 | 0.36 |
| 2004 | 0.36 |
| 2003 | 0.33 |
| 2002 | 0.285 |
| 2001 | 0.25 |
| 2000 | 0.25 |
| 1999 | 0.053 |
| 1997 | 0.08 |
| 1996 | 0.32 |
| 1995 | 0.32 |
| 1994 | 0.32 |
| 1993 | 0.32 |
| 1992 | 0.32 |
| 1991 | 0.32 |
| 1990 | 0.24 |
| 1989 | 0.57 |
| 1988 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
