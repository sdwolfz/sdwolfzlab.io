---
title: "DILLARDS INC (DDS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DILLARDS INC</td></tr>
    <tr><td>Symbol</td><td>DDS</td></tr>
    <tr><td>Web</td><td><a href="https://www.dillards.com">www.dillards.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.15 |
| 2020 | 0.6 |
| 2019 | 0.5 |
| 2018 | 0.4 |
| 2017 | 0.34 |
| 2016 | 0.28 |
| 2015 | 0.26 |
| 2014 | 0.24 |
| 2013 | 0.17 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
