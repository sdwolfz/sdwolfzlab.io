---
title: "OSHKOSH CORPORATION (OSK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>OSHKOSH CORPORATION</td></tr>
    <tr><td>Symbol</td><td>OSK</td></tr>
    <tr><td>Web</td><td><a href="https://www.oshkoshcorp.com">www.oshkoshcorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.66 |
| 2020 | 1.23 |
| 2019 | 1.11 |
| 2018 | 0.99 |
| 2017 | 0.87 |
| 2016 | 0.78 |
| 2015 | 0.7 |
| 2014 | 0.62 |
| 2013 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
