---
title: "JANUS HENDERSON GROUP PLC (JHG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>JANUS HENDERSON GROUP PLC</td></tr>
    <tr><td>Symbol</td><td>JHG</td></tr>
    <tr><td>Web</td><td><a href="https://www.janushenderson.com">www.janushenderson.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.74 |
| 2020 | 1.44 |
| 2019 | 1.44 |
| 2018 | 1.4 |
| 2017 | 0.873 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
