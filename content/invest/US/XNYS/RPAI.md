---
title: " (RPAI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>RPAI</td></tr>
    <tr><td>Web</td><td><a href="https://www.rpai.com">www.rpai.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.07 |
| 2020 | 0.276 |
| 2019 | 0.664 |
| 2018 | 0.664 |
| 2017 | 0.664 |
| 2016 | 0.664 |
| 2015 | 0.664 |
| 2014 | 0.664 |
| 2013 | 0.332 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
