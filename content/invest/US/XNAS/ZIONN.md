---
title: " (ZIONN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ZIONN</td></tr>
    <tr><td>Web</td><td><a href="https://www.zionsbancorp.com">www.zionsbancorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.359 |
| 2020 | 1.438 |
| 2019 | 1.438 |
| 2018 | 1.438 |
| 2017 | 1.438 |
| 2016 | 1.438 |
| 2015 | 1.438 |
| 2014 | 1.438 |
| 2013 | 0.894 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
