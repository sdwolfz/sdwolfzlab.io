---
title: " (ITUB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ITUB</td></tr>
    <tr><td>Web</td><td><a href="https://www.itau.com.br">www.itau.com.br</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.031 |
| 2020 | 0.137 |
| 2019 | 0.519 |
| 2018 | 0.572 |
| 2017 | 0.412 |
| 2016 | 0.334 |
| 2015 | 0.173 |
| 2014 | 0.077 |
| 2013 | 0.027 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
