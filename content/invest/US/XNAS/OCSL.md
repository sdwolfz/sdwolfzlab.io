---
title: "OAKTREE SPECIALTY LENDING CORP (OCSL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>OAKTREE SPECIALTY LENDING CORP</td></tr>
    <tr><td>Symbol</td><td>OCSL</td></tr>
    <tr><td>Web</td><td><a href="https://www.oaktreespecialtylending.com">www.oaktreespecialtylending.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 0.405 |
| 2019 | 0.38 |
| 2018 | 0.37 |
| 2017 | 0.41 |
| 2016 | 0.72 |
| 2015 | 0.692 |
| 2014 | 1.033 |
| 2013 | 1.104 |
| 2012 | 1.15 |
| 2011 | 1.173 |
| 2010 | 1.147 |
| 2009 | 0.77 |
| 2008 | 1.01 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
