---
title: "PAN AMERICAN SILVER CORP (PAAS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PAN AMERICAN SILVER CORP</td></tr>
    <tr><td>Symbol</td><td>PAAS</td></tr>
    <tr><td>Web</td><td><a href="https://www.panamericansilver.com">www.panamericansilver.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.07 |
| 2020 | 0.22 |
| 2019 | 0.14 |
| 2018 | 0.14 |
| 2017 | 0.1 |
| 2016 | 0.05 |
| 2015 | 0.275 |
| 2014 | 0.5 |
| 2013 | 0.5 |
| 2012 | 0.175 |
| 2011 | 0.1 |
| 2010 | 0.075 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
