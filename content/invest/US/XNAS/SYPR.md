---
title: "SYPRIS SOLUTIONS INC (SYPR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SYPRIS SOLUTIONS INC</td></tr>
    <tr><td>Symbol</td><td>SYPR</td></tr>
    <tr><td>Web</td><td><a href="https://www.sypris.com">www.sypris.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 0.08 |
| 2013 | 0.08 |
| 2012 | 0.08 |
| 2008 | 0.11 |
| 2007 | 0.12 |
| 2006 | 0.12 |
| 2005 | 0.12 |
| 2004 | 0.12 |
| 2003 | 0.12 |
| 2002 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
