---
title: " (FMS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>FMS</td></tr>
    <tr><td>Web</td><td><a href="https://www.freseniusmedicalcare.com">www.freseniusmedicalcare.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.498 |
| 2019 | 0.45 |
| 2018 | 0.427 |
| 2017 | 0.37 |
| 2016 | 0.313 |
| 2015 | 0.298 |
| 2014 | 0.368 |
| 2013 | 0.334 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
