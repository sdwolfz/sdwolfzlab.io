---
title: " (LAND)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>LAND</td></tr>
    <tr><td>Web</td><td><a href="https://www.GladstoneFarms.com">www.GladstoneFarms.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.27 |
| 2020 | 0.537 |
| 2019 | 0.534 |
| 2018 | 0.532 |
| 2017 | 0.524 |
| 2016 | 0.495 |
| 2015 | 0.465 |
| 2014 | 0.36 |
| 2013 | 1.49 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
