---
title: "Vp (VP.)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Vp</td></tr>
    <tr><td>Symbol</td><td>VP.</td></tr>
    <tr><td>Web</td><td><a href="https://www.vpplc.com">www.vpplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 8.45 |
| 2019 | 30.2 |
| 2018 | 26.0 |
| 2017 | 22.0 |
| 2016 | 18.85 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
