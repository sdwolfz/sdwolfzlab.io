---
title: " (HMLP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>HMLP</td></tr>
    <tr><td>Web</td><td><a href="https://www.hoeghlngpartners.com">www.hoeghlngpartners.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.88 |
| 2020 | 1.76 |
| 2019 | 1.76 |
| 2018 | 1.75 |
| 2017 | 1.702 |
| 2016 | 1.648 |
| 2015 | 1.352 |
| 2014 | 0.183 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
