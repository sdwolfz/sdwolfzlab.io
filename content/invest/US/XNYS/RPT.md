---
title: "RPT REALTY (RPT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>RPT REALTY</td></tr>
    <tr><td>Symbol</td><td>RPT</td></tr>
    <tr><td>Web</td><td><a href="http://www.rptrealty.com">www.rptrealty.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.22 |
| 2019 | 0.88 |
| 2018 | 0.88 |
| 2017 | 0.88 |
| 2016 | 0.86 |
| 2015 | 0.82 |
| 2014 | 0.776 |
| 2013 | 0.544 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
