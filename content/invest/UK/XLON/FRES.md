---
title: "Fresnillo (FRES)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Fresnillo</td></tr>
    <tr><td>Symbol</td><td>FRES</td></tr>
    <tr><td>Web</td><td><a href="https://www.fresnilloplc.com">www.fresnilloplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 14.5 |
| 2018 | 27.4 |
| 2017 | 40.4 |
| 2016 | 11.9 |
| 2015 | 5.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
