---
title: "DXC TECHNOLOGY COMPANY (DXC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DXC TECHNOLOGY COMPANY</td></tr>
    <tr><td>Symbol</td><td>DXC</td></tr>
    <tr><td>Web</td><td><a href="https://www.dxc.technology">www.dxc.technology</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.21 |
| 2019 | 0.82 |
| 2018 | 0.75 |
| 2017 | 0.68 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
