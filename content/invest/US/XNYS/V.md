---
title: "VISA INC (V)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>VISA INC</td></tr>
    <tr><td>Symbol</td><td>V</td></tr>
    <tr><td>Web</td><td><a href="https://www.usa.visa.com">www.usa.visa.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.64 |
| 2020 | 1.22 |
| 2019 | 1.05 |
| 2018 | 0.88 |
| 2017 | 0.69 |
| 2016 | 0.585 |
| 2015 | 0.5 |
| 2014 | 1.68 |
| 2013 | 0.73 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
