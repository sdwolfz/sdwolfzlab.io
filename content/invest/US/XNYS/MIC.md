---
title: "MACQUARIE INFRASTRUCTURE CORP (MIC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MACQUARIE INFRASTRUCTURE CORP</td></tr>
    <tr><td>Symbol</td><td>MIC</td></tr>
    <tr><td>Web</td><td><a href="https://www.macquarie.com">www.macquarie.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 11.0 |
| 2020 | 1.0 |
| 2019 | 4.0 |
| 2018 | 4.44 |
| 2017 | 5.43 |
| 2016 | 4.89 |
| 2015 | 4.33 |
| 2014 | 3.781 |
| 2013 | 1.75 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
