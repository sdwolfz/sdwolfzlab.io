---
title: " (TSCAP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>TSCAP</td></tr>
    <tr><td>Web</td><td><a href="https://www.tristatecapitalbank.com">www.tristatecapitalbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.844 |
| 2020 | 1.688 |
| 2019 | 1.688 |
| 2018 | 1.317 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
