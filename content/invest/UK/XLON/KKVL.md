---
title: "Kkv Sec Loan (KKVL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Kkv Sec Loan</td></tr>
    <tr><td>Symbol</td><td>KKVL</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 6.65 |
| 2018 | 7.85 |
| 2017 | 7.25 |
| 2016 | 7.12 |
| 2015 | 3.93 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
