---
title: "Caledonia (CLDN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Caledonia</td></tr>
    <tr><td>Symbol</td><td>CLDN</td></tr>
    <tr><td>Web</td><td><a href="https://www.caledonia.com">www.caledonia.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 61.1 |
| 2019 | 59.3 |
| 2018 | 57.0 |
| 2017 | 154.8 |
| 2016 | 52.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
