---
title: "PARKER-HANNIFIN CORP (PH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PARKER-HANNIFIN CORP</td></tr>
    <tr><td>Symbol</td><td>PH</td></tr>
    <tr><td>Web</td><td><a href="https://www.phstock.com">www.phstock.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.91 |
| 2020 | 3.52 |
| 2019 | 3.4 |
| 2018 | 2.94 |
| 2017 | 2.64 |
| 2016 | 2.52 |
| 2015 | 2.52 |
| 2014 | 2.07 |
| 2013 | 0.9 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
