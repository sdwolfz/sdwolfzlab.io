---
title: " (MPW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>MPW</td></tr>
    <tr><td>Web</td><td><a href="https://www.medicalpropertiestrust.com">www.medicalpropertiestrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 1.08 |
| 2019 | 1.02 |
| 2018 | 1.0 |
| 2017 | 0.96 |
| 2016 | 0.91 |
| 2015 | 0.88 |
| 2014 | 0.84 |
| 2013 | 0.41 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
