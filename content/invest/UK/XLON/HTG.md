---
title: "Hunting (HTG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Hunting</td></tr>
    <tr><td>Symbol</td><td>HTG</td></tr>
    <tr><td>Web</td><td><a href="https://www.huntingplc.com">www.huntingplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 5.0 |
| 2019 | 11.0 |
| 2018 | 9.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
