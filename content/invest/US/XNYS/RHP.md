---
title: " (RHP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>RHP</td></tr>
    <tr><td>Web</td><td><a href="https://www.rymanhp.com">www.rymanhp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.95 |
| 2019 | 3.6 |
| 2018 | 3.4 |
| 2017 | 3.2 |
| 2016 | 3.0 |
| 2015 | 2.7 |
| 2014 | 2.2 |
| 2013 | 1.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
