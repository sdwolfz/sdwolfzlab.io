---
title: "INDEPENDENT BANK CORP(MASS) (INDB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>INDEPENDENT BANK CORP(MASS)</td></tr>
    <tr><td>Symbol</td><td>INDB</td></tr>
    <tr><td>Web</td><td><a href="https://www.rocklandtrust.com">www.rocklandtrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.48 |
| 2020 | 1.84 |
| 2019 | 1.76 |
| 2018 | 1.52 |
| 2017 | 1.28 |
| 2016 | 1.16 |
| 2015 | 1.04 |
| 2014 | 0.96 |
| 2013 | 0.88 |
| 2012 | 0.84 |
| 2011 | 0.76 |
| 2010 | 0.72 |
| 2009 | 0.72 |
| 2008 | 0.72 |
| 2007 | 0.68 |
| 2006 | 0.64 |
| 2005 | 0.6 |
| 2004 | 0.56 |
| 2003 | 0.52 |
| 2002 | 0.48 |
| 2001 | 0.44 |
| 2000 | 0.4 |
| 1999 | 0.4 |
| 1998 | 0.4 |
| 1997 | 0.34 |
| 1996 | 0.25 |
| 1995 | 0.18 |
| 1994 | 0.08 |
| 1990 | 0.09 |
| 1989 | 0.34 |
| 1988 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
