---
title: "RENAISSANCERE HOLDINGS LTD (RNR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>RENAISSANCERE HOLDINGS LTD</td></tr>
    <tr><td>Symbol</td><td>RNR</td></tr>
    <tr><td>Web</td><td><a href="https://www.renre.com">www.renre.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.36 |
| 2020 | 1.4 |
| 2019 | 1.36 |
| 2018 | 1.32 |
| 2017 | 1.28 |
| 2016 | 1.24 |
| 2015 | 1.2 |
| 2014 | 1.16 |
| 2013 | 0.56 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
