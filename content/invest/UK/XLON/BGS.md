---
title: "Baillie Gifford Shin Nippon PLC (BGS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Baillie Gifford Shin Nippon PLC</td></tr>
    <tr><td>Symbol</td><td>BGS</td></tr>
    <tr><td>Web</td><td><a href="https://www.bailliegifford.co.uk">www.bailliegifford.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
