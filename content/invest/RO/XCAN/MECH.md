---
title: "MECANICA 94 SA DR. T. SEVERIN (MECH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>MECANICA 94 SA DR. T. SEVERIN</td></tr>
    <tr><td>Symbol</td><td>MECH</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2012 | 0.26 |
| 2007 | 0.31 |
| 2005 | 0.21 |
| 2004 | 0.2929 |
| 2003 | 0.2477 |
| 2002 | 0.123 |
| 2001 | 0.2729 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
