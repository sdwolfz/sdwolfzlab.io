---
title: "MVB FINANCIAL CORP (MVBF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MVB FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>MVBF</td></tr>
    <tr><td>Web</td><td><a href="https://www.mvbbanking.com">www.mvbbanking.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.36 |
| 2019 | 0.195 |
| 2018 | 0.11 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
