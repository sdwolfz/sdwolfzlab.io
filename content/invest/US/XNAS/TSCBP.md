---
title: " (TSCBP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>TSCBP</td></tr>
    <tr><td>Web</td><td><a href="https://www.tristatecapitalbank.com">www.tristatecapitalbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.797 |
| 2020 | 1.594 |
| 2019 | 0.943 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
