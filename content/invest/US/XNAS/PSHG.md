---
title: "PERFORMANCE SHIPPING INC (PSHG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PERFORMANCE SHIPPING INC</td></tr>
    <tr><td>Symbol</td><td>PSHG</td></tr>
    <tr><td>Web</td><td><a href="https://www.pshipping.com">www.pshipping.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.01 |
| 2016 | 0.005 |
| 2015 | 0.01 |
| 2014 | 0.205 |
| 2013 | 0.9 |
| 2012 | 1.0 |
| 2011 | 0.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
