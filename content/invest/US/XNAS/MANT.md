---
title: "MANTECH INTERNATIONAL CORP (MANT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MANTECH INTERNATIONAL CORP</td></tr>
    <tr><td>Symbol</td><td>MANT</td></tr>
    <tr><td>Web</td><td><a href="https://www.mantech.com">www.mantech.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.76 |
| 2020 | 1.28 |
| 2019 | 1.08 |
| 2018 | 1.0 |
| 2017 | 0.84 |
| 2016 | 0.84 |
| 2015 | 0.84 |
| 2014 | 0.84 |
| 2013 | 0.84 |
| 2012 | 0.84 |
| 2011 | 0.84 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
