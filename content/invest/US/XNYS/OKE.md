---
title: "ONEOK INC (OKE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ONEOK INC</td></tr>
    <tr><td>Symbol</td><td>OKE</td></tr>
    <tr><td>Web</td><td><a href="https://www.oneok.com">www.oneok.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.87 |
| 2020 | 3.74 |
| 2019 | 3.53 |
| 2018 | 3.245 |
| 2017 | 2.72 |
| 2016 | 2.46 |
| 2015 | 2.43 |
| 2014 | 2.125 |
| 2013 | 0.74 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
