---
title: "TRIPLE-S MANAGEMENT CORPORATION (GTS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TRIPLE-S MANAGEMENT CORPORATION</td></tr>
    <tr><td>Symbol</td><td>GTS</td></tr>
    <tr><td>Web</td><td><a href="https://www.triplesmanagement.com">www.triplesmanagement.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.051 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
