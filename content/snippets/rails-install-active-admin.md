---
title: "Rails: install ActiveAdmin"
date: 2024-01-22T08:41:29Z
type: posts
slug: rails-install-active-admin
tags: ["development", "rails", "ruby"]
---

## Gems

Add these to your `Gemfile`:

```ruby
# Administration
gem 'activeadmin', '~> 4.0.0.beta4'

# Authentication
gem 'devise', '~> 4.9', '>= 4.9.3'
```

Then install your new gems:

```sh
bundle install
```

## Generate

```sh
bundle exec rails generate active_admin:install \
  SystemOperator                                \
  --skip-comments                               \
  --no-registerable                             \
  --no-default-user
```

## Changes

1. Configure the `operations` path in `config/initializers/active_admin.rb`:

    ```ruby
    config.default_namespace = :operations
    config.load_paths = [File.join(Rails.root, 'app', 'operations')]
    ```

2. Rename `app/admin` to `app/operations`.
3. Edit your `module.export` contents in ` tailwind-active_admin.config.js` to
   contain the new operations path:

    ```javascript
    './app/operations/**/*.{arb,erb,html,rb}',
    ```
4. Delete `test/fixtures`.
5. Un-comment `Trackable`, `Confirmable`, and `Lockable` in the generated
   migration file.
6. Add `:confirmable`, `:lockable`, `:timeoutable`, and `:trackable` to your
   `SystemOperator` model.

## Execute

```sh
bundle exec rails db:migrate
```

## Restart

Restort your webserver and visit `/operations` in your browser.
