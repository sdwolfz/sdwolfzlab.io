---
title: "FIRST BANCORP INC ME (FNLC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST BANCORP INC ME</td></tr>
    <tr><td>Symbol</td><td>FNLC</td></tr>
    <tr><td>Web</td><td><a href="https://www.thefirstbancorp.com">www.thefirstbancorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.62 |
| 2020 | 1.22 |
| 2019 | 1.18 |
| 2018 | 1.06 |
| 2017 | 1.06 |
| 2016 | 0.9 |
| 2015 | 0.65 |
| 2014 | 1.03 |
| 2013 | 0.78 |
| 2012 | 0.78 |
| 2011 | 0.78 |
| 2010 | 0.78 |
| 2009 | 0.585 |
| 2008 | 0.945 |
| 2007 | 0.67 |
| 2006 | 0.59 |
| 2005 | 0.51 |
| 2004 | 0.835 |
| 2003 | 1.1 |
| 2002 | 0.94 |
| 2001 | 0.6 |
| 2000 | 0.66 |
| 1999 | 0.27 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
