---
title: "MEDLEY CAPITAL CORPORATION (MCC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MEDLEY CAPITAL CORPORATION</td></tr>
    <tr><td>Symbol</td><td>MCC</td></tr>
    <tr><td>Web</td><td><a href="http://www.medleycapitalcorp.com">www.medleycapitalcorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.7 |
| 2019 | 0.156 |
| 2018 | 0.46 |
| 2017 | 0.7 |
| 2016 | 1.04 |
| 2015 | 1.2 |
| 2014 | 1.48 |
| 2013 | 0.74 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
