---
title: " (NOK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>NOK</td></tr>
    <tr><td>Web</td><td><a href="https://www.nokia.com">www.nokia.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.091 |
| 2018 | 0.196 |
| 2017 | 0.133 |
| 2016 | 0.203 |
| 2015 | 0.109 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
