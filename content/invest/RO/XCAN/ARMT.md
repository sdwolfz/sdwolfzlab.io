---
title: "CAROMET SA CARANSEBES (ARMT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>CAROMET SA CARANSEBES</td></tr>
    <tr><td>Symbol</td><td>ARMT</td></tr>
    <tr><td>Web</td><td><a href="https://www.caromet.ro">www.caromet.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2002 | 0.0171 |
| 2001 | 0.0675 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
