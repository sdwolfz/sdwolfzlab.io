---
title: "SCIENCE APPLICATION INTL CORP (SAIC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SCIENCE APPLICATION INTL CORP</td></tr>
    <tr><td>Symbol</td><td>SAIC</td></tr>
    <tr><td>Web</td><td><a href="https://www.saic.com">www.saic.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.74 |
| 2020 | 1.48 |
| 2019 | 1.42 |
| 2018 | 1.24 |
| 2017 | 1.24 |
| 2016 | 1.24 |
| 2015 | 1.18 |
| 2014 | 1.12 |
| 2013 | 0.28 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
