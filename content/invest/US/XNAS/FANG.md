---
title: "DIAMONDBACK ENERGY INC (FANG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>DIAMONDBACK ENERGY INC</td></tr>
    <tr><td>Symbol</td><td>FANG</td></tr>
    <tr><td>Web</td><td><a href="https://www.diamondbackenergy.com">www.diamondbackenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.8 |
| 2020 | 1.5 |
| 2019 | 0.688 |
| 2018 | 0.375 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
