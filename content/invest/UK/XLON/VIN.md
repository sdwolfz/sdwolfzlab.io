---
title: "VALUE AND INCOME TRUST PLC ORD 10P (VIN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>VALUE AND INCOME TRUST PLC ORD 10P</td></tr>
    <tr><td>Symbol</td><td>VIN</td></tr>
    <tr><td>Web</td><td><a href="http://www.olim.co.uk">www.olim.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 12.1 |
| 2019 | 11.8 |
| 2018 | 11.4 |
| 2017 | 11.0 |
| 2016 | 10.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
