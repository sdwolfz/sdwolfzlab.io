---
title: "DESIGNER BRANDS INC (DBI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DESIGNER BRANDS INC</td></tr>
    <tr><td>Symbol</td><td>DBI</td></tr>
    <tr><td>Web</td><td><a href="https://www.designerbrands.com">www.designerbrands.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.1 |
| 2019 | 1.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
