---
title: "UNIREA SA CLUJ NAPOCA (UNIR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>UNIREA SA CLUJ NAPOCA</td></tr>
    <tr><td>Symbol</td><td>UNIR</td></tr>
    <tr><td>Web</td><td><a href="https://www.unirea-cluj.ro">www.unirea-cluj.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
