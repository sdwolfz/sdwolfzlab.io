---
title: "Mitie (MTO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Mitie</td></tr>
    <tr><td>Symbol</td><td>MTO</td></tr>
    <tr><td>Web</td><td><a href="https://www.mitie.com">www.mitie.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 3.4 |
| 2019 | 4.0 |
| 2018 | 4.0 |
| 2017 | 4.0 |
| 2016 | 12.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
