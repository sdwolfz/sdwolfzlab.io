---
title: " (CTT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CTT</td></tr>
    <tr><td>Web</td><td><a href="https://www.catchmark.com">www.catchmark.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.135 |
| 2020 | 0.54 |
| 2019 | 0.54 |
| 2018 | 0.54 |
| 2017 | 0.54 |
| 2016 | 0.53 |
| 2015 | 0.5 |
| 2014 | 0.47 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
