---
title: "Blackrock Fr (BRFI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Blackrock Fr</td></tr>
    <tr><td>Symbol</td><td>BRFI</td></tr>
    <tr><td>Web</td><td><a href="https://blackrock.com">blackrock.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 7.0 |
| 2019 | 8.4 |
| 2018 | 8.2 |
| 2017 | 6.9 |
| 2016 | 6.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
