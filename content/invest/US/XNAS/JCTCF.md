---
title: "JEWETT-CAMERON TRADING CO (JCTCF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>JEWETT-CAMERON TRADING CO</td></tr>
    <tr><td>Symbol</td><td>JCTCF</td></tr>
    <tr><td>Web</td><td><a href="https://www.jewettcameron.com">www.jewettcameron.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
