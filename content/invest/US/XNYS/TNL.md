---
title: "TRAVEL PLUS LEISURE CO (TNL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TRAVEL PLUS LEISURE CO</td></tr>
    <tr><td>Symbol</td><td>TNL</td></tr>
    <tr><td>Web</td><td><a href="https://www.travelandleisureco.com">www.travelandleisureco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
