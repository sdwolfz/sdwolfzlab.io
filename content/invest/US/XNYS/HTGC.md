---
title: "HERCULES CAPITAL INC (HTGC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HERCULES CAPITAL INC</td></tr>
    <tr><td>Symbol</td><td>HTGC</td></tr>
    <tr><td>Web</td><td><a href="https://www.htgc.com">www.htgc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.76 |
| 2020 | 1.38 |
| 2019 | 1.33 |
| 2018 | 1.26 |
| 2017 | 1.24 |
| 2016 | 1.24 |
| 2015 | 1.24 |
| 2014 | 1.24 |
| 2013 | 0.59 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
