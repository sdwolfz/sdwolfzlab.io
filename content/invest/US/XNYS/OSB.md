---
title: "NORBORD INC (OSB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NORBORD INC</td></tr>
    <tr><td>Symbol</td><td>OSB</td></tr>
    <tr><td>Web</td><td><a href="http://www.norbord.com">www.norbord.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.88 |
| 2019 | 1.048 |
| 2018 | 4.824 |
| 2017 | 1.166 |
| 2016 | 0.304 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
