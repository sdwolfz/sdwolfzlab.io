---
title: "WABTEC CORP (WAB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>WABTEC CORP</td></tr>
    <tr><td>Symbol</td><td>WAB</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.24 |
| 2020 | 0.48 |
| 2019 | 0.48 |
| 2018 | 0.48 |
| 2017 | 0.44 |
| 2016 | 0.36 |
| 2015 | 0.28 |
| 2014 | 0.2 |
| 2013 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
