---
title: "Airtel Africa (AAF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Airtel Africa</td></tr>
    <tr><td>Symbol</td><td>AAF</td></tr>
    <tr><td>Web</td><td><a href="https://www.africa.airtel.com">www.africa.airtel.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
