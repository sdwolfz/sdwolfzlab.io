---
title: "Wizz Air (WIZZ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Wizz Air</td></tr>
    <tr><td>Symbol</td><td>WIZZ</td></tr>
    <tr><td>Web</td><td><a href="https://www.wizzair.com">www.wizzair.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
