---
title: "John Laing (JLG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>John Laing</td></tr>
    <tr><td>Symbol</td><td>JLG</td></tr>
    <tr><td>Web</td><td><a href="https://www.laing.com">www.laing.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 9.5 |
| 2018 | 9.5 |
| 2017 | 10.61 |
| 2016 | 8.15 |
| 2015 | 6.9 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
