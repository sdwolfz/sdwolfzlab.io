---
title: "GROUP 1 AUTOMOTIVE INC (GPI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GROUP 1 AUTOMOTIVE INC</td></tr>
    <tr><td>Symbol</td><td>GPI</td></tr>
    <tr><td>Web</td><td><a href="https://www.group1auto.com">www.group1auto.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.31 |
| 2020 | 0.6 |
| 2019 | 1.09 |
| 2018 | 1.04 |
| 2017 | 0.97 |
| 2016 | 0.91 |
| 2015 | 0.83 |
| 2014 | 0.7 |
| 2013 | 0.34 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
