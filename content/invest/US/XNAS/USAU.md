---
title: "U S GOLD CORP (USAU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>U S GOLD CORP</td></tr>
    <tr><td>Symbol</td><td>USAU</td></tr>
    <tr><td>Web</td><td><a href="https://www.usgoldcorp.gold">www.usgoldcorp.gold</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2008 | 0.06 |
| 2007 | 0.24 |
| 2006 | 0.24 |
| 2005 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
