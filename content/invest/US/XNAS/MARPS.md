---
title: " (MARPS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>MARPS</td></tr>
    <tr><td>Web</td><td><a href="https://www.marps-marine.com">www.marps-marine.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.018 |
| 2020 | 0.221 |
| 2019 | 0.274 |
| 2018 | 0.354 |
| 2017 | 0.321 |
| 2016 | 0.298 |
| 2015 | 0.633 |
| 2014 | 1.318 |
| 2013 | 1.46 |
| 2012 | 1.61 |
| 2011 | 1.835 |
| 2010 | 1.418 |
| 2009 | 1.132 |
| 2008 | 3.046 |
| 2007 | 2.849 |
| 2006 | 1.983 |
| 2005 | 2.482 |
| 2004 | 2.294 |
| 2003 | 2.607 |
| 2002 | 2.115 |
| 2001 | 3.967 |
| 2000 | 2.682 |
| 1999 | 1.61 |
| 1998 | 1.552 |
| 1997 | 1.771 |
| 1996 | 1.696 |
| 1995 | 1.082 |
| 1994 | 1.485 |
| 1993 | 0.938 |
| 1992 | 1.109 |
| 1991 | 1.269 |
| 1990 | 1.541 |
| 1989 | 0.996 |
| 1988 | 0.247 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
