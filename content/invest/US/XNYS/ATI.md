---
title: "ALLEGHENY TECHNOLOGIES INC (ATI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ALLEGHENY TECHNOLOGIES INC</td></tr>
    <tr><td>Symbol</td><td>ATI</td></tr>
    <tr><td>Web</td><td><a href="https://www.atimetals.com">www.atimetals.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.24 |
| 2015 | 0.62 |
| 2014 | 0.72 |
| 2013 | 0.36 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
