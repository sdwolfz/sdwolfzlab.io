---
title: "Howden Joinery (HWDN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Howden Joinery</td></tr>
    <tr><td>Symbol</td><td>HWDN</td></tr>
    <tr><td>Web</td><td><a href="https://www.howdenjoinerygroupplc.com">www.howdenjoinerygroupplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 18.2 |
| 2019 | 13.0 |
| 2018 | 11.6 |
| 2017 | 11.1 |
| 2016 | 10.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
