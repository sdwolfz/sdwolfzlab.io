---
title: "ENEL CHILE SA SPON ADR EACH REPR 50 ORD (ENIC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ENEL CHILE SA SPON ADR EACH REPR 50 ORD</td></tr>
    <tr><td>Symbol</td><td>ENIC</td></tr>
    <tr><td>Web</td><td><a href="http://www.enelchile.cl">www.enelchile.cl</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.211 |
| 2019 | 0.182 |
| 2018 | 0.199 |
| 2017 | 0.156 |
| 2016 | 0.112 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
