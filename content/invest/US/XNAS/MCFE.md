---
title: "MCAFEE CORP (MCFE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MCAFEE CORP</td></tr>
    <tr><td>Symbol</td><td>MCFE</td></tr>
    <tr><td>Web</td><td><a href="https://www.mcafee.com">www.mcafee.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.115 |
| 2020 | 0.087 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
