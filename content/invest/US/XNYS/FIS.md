---
title: "FIDELITY NATL INFORMATION SERVICES (FIS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FIDELITY NATL INFORMATION SERVICES</td></tr>
    <tr><td>Symbol</td><td>FIS</td></tr>
    <tr><td>Web</td><td><a href="https://www.fisglobal.com">www.fisglobal.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.78 |
| 2020 | 1.4 |
| 2019 | 1.4 |
| 2018 | 1.28 |
| 2017 | 1.16 |
| 2016 | 1.04 |
| 2015 | 1.04 |
| 2014 | 0.96 |
| 2013 | 0.44 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
