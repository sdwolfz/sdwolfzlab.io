---
title: "How to Use: hadolint"
date: 2020-06-24T18:45:41Z
type: posts
slug: how-to-use-hadolint
series: ["How to Use"]
tags: ["docker", "tutorial", "linting", "productivity", "development"]
---

## Install

It's not available in any package manager, so use the Docker image:

```sh
docker run --rm -it -v "$PWD":/work -w /work hadolint/hadolint:latest-alpine sh
```

## Configure

Add a `.hadolint.yaml` file containing your rules:

```yml
---

ignored:
  - DL3018
```

## Use

Single file:

```sh
hadolint ./path/to/Dockerfile
```

All files:

```sh
find . -name Dockerfile | xargs hadolint
```

Files known to git:

```sh
hadolint $(git ls-files | grep Dockerfile)
```
