---
title: "KANSAS CITY SOUTHERN (KSU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>KANSAS CITY SOUTHERN</td></tr>
    <tr><td>Symbol</td><td>KSU</td></tr>
    <tr><td>Web</td><td><a href="https://www.kcsouthern.com">www.kcsouthern.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.54 |
| 2020 | 1.64 |
| 2019 | 1.48 |
| 2018 | 1.44 |
| 2017 | 1.38 |
| 2016 | 1.32 |
| 2015 | 1.32 |
| 2014 | 1.12 |
| 2013 | 0.43 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
