---
title: "LPL FINL HLDGS INC (LPLA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LPL FINL HLDGS INC</td></tr>
    <tr><td>Symbol</td><td>LPLA</td></tr>
    <tr><td>Web</td><td><a href="https://www.lpl.com">www.lpl.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 1.0 |
| 2019 | 1.0 |
| 2018 | 1.0 |
| 2017 | 1.0 |
| 2016 | 1.0 |
| 2015 | 1.0 |
| 2014 | 0.96 |
| 2013 | 0.65 |
| 2012 | 2.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
