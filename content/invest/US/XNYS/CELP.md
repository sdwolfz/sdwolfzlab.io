---
title: " (CELP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CELP</td></tr>
    <tr><td>Web</td><td><a href="https://www.cypressenvironmental.biz">www.cypressenvironmental.biz</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.42 |
| 2019 | 0.84 |
| 2018 | 0.84 |
| 2017 | 1.036 |
| 2016 | 1.624 |
| 2015 | 1.624 |
| 2014 | 1.104 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
