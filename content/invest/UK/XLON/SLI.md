---
title: "Standard Life (SLI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Standard Life</td></tr>
    <tr><td>Symbol</td><td>SLI</td></tr>
    <tr><td>Web</td><td><a href="https://www.standardlifeinvestments.com">www.standardlifeinvestments.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 4.76 |
| 2018 | 4.76 |
| 2017 | 4.76 |
| 2016 | 4.76 |
| 2015 | 4.64 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
