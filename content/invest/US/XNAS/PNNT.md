---
title: "PENNANTPARK INVESTMENT CORPORATION (PNNT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PENNANTPARK INVESTMENT CORPORATION</td></tr>
    <tr><td>Symbol</td><td>PNNT</td></tr>
    <tr><td>Web</td><td><a href="https://www.pennantpark.com">www.pennantpark.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 0.54 |
| 2019 | 0.72 |
| 2018 | 0.72 |
| 2017 | 0.72 |
| 2016 | 1.12 |
| 2015 | 1.12 |
| 2014 | 1.12 |
| 2013 | 1.12 |
| 2012 | 1.12 |
| 2011 | 1.09 |
| 2010 | 1.04 |
| 2009 | 0.97 |
| 2008 | 0.92 |
| 2007 | 0.58 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
