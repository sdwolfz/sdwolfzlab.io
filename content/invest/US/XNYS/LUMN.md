---
title: "LUMEN TECHNOLOGIES INC (LUMN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>LUMEN TECHNOLOGIES INC</td></tr>
    <tr><td>Symbol</td><td>LUMN</td></tr>
    <tr><td>Web</td><td><a href="https://www.lumen.com">www.lumen.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
