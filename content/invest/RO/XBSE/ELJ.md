---
title: "ELECTROAPARATAJ S.A. (ELJ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>ELECTROAPARATAJ S.A.</td></tr>
    <tr><td>Symbol</td><td>ELJ</td></tr>
    <tr><td>Web</td><td><a href="https://www.electroaparataj.ro">www.electroaparataj.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2000 | 0.0053 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
