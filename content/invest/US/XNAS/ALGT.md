---
title: "ALLEGIANT TRAVEL COMPANY (ALGT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ALLEGIANT TRAVEL COMPANY</td></tr>
    <tr><td>Symbol</td><td>ALGT</td></tr>
    <tr><td>Web</td><td><a href="https://www.allegiant.com">www.allegiant.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.7 |
| 2019 | 2.8 |
| 2018 | 2.8 |
| 2017 | 2.8 |
| 2016 | 2.4 |
| 2015 | 2.75 |
| 2014 | 2.5 |
| 2013 | 2.25 |
| 2012 | 2.0 |
| 2010 | 0.75 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
