---
title: "COLONY BANKCORP INC (CBAN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COLONY BANKCORP INC</td></tr>
    <tr><td>Symbol</td><td>CBAN</td></tr>
    <tr><td>Web</td><td><a href="https://www.colonybank.com">www.colonybank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.205 |
| 2020 | 0.4 |
| 2019 | 0.3 |
| 2018 | 0.2 |
| 2017 | 0.1 |
| 2009 | 0.146 |
| 2008 | 0.39 |
| 2007 | 0.365 |
| 2006 | 0.325 |
| 2005 | 0.302 |
| 2004 | 0.315 |
| 2003 | 0.303 |
| 2002 | 0.275 |
| 2001 | 0.24 |
| 2000 | 0.19 |
| 1999 | 0.14 |
| 1998 | 0.175 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
