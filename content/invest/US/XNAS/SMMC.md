---
title: "SOUTH MOUNTAIN MERGER CORP (SMMC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SOUTH MOUNTAIN MERGER CORP</td></tr>
    <tr><td>Symbol</td><td>SMMC</td></tr>
    <tr><td>Web</td><td><a href="http://www.southmountainmerger.com">www.southmountainmerger.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
