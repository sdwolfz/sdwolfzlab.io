---
title: "EVANS BANCORP INC (EVBN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>EVANS BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>EVBN</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.6 |
| 2020 | 1.16 |
| 2019 | 1.04 |
| 2018 | 0.92 |
| 2017 | 0.8 |
| 2016 | 0.76 |
| 2015 | 0.72 |
| 2014 | 0.65 |
| 2013 | 0.26 |
| 2012 | 0.68 |
| 2011 | 0.4 |
| 2010 | 0.4 |
| 2009 | 0.61 |
| 2008 | 0.78 |
| 2007 | 0.71 |
| 2006 | 0.68 |
| 2005 | 0.68 |
| 2004 | 0.67 |
| 2003 | 0.66 |
| 2002 | 0.59 |
| 2001 | 0.27 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
