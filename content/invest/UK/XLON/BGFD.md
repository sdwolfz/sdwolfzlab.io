---
title: "Baillie Gifford Japan Trust PLC (BGFD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Baillie Gifford Japan Trust PLC</td></tr>
    <tr><td>Symbol</td><td>BGFD</td></tr>
    <tr><td>Web</td><td><a href="https://www.bailliegifford.com">www.bailliegifford.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 4.5 |
| 2019 | 3.5 |
| 2018 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
