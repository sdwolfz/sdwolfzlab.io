---
title: "MARVELL TECHNOLOGY INC (MRVL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MARVELL TECHNOLOGY INC</td></tr>
    <tr><td>Symbol</td><td>MRVL</td></tr>
    <tr><td>Web</td><td><a href="https://www.marvell.com">www.marvell.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.06 |
| 2020 | 0.24 |
| 2019 | 0.24 |
| 2018 | 0.24 |
| 2017 | 0.24 |
| 2016 | 0.24 |
| 2015 | 0.24 |
| 2014 | 0.24 |
| 2013 | 0.24 |
| 2012 | 0.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
