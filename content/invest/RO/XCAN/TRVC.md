---
title: "TRANSILVANIA-COM SA MEDIAS (TRVC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>TRANSILVANIA-COM SA MEDIAS</td></tr>
    <tr><td>Symbol</td><td>TRVC</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 2.6413 |
| 2018 | 2.8416 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
