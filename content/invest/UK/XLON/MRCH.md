---
title: "Merchants Trust (MRCH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Merchants Trust</td></tr>
    <tr><td>Symbol</td><td>MRCH</td></tr>
    <tr><td>Web</td><td><a href="https://www.merchantstrust.co.uk">www.merchantstrust.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 27.1 |
| 2019 | 26.0 |
| 2018 | 24.8 |
| 2017 | 24.2 |
| 2016 | 24.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
