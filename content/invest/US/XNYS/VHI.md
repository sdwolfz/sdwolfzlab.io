---
title: "VALHI INC (VHI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>VALHI INC</td></tr>
    <tr><td>Symbol</td><td>VHI</td></tr>
    <tr><td>Web</td><td><a href="https://www.valhi.net">www.valhi.net</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.08 |
| 2020 | 0.26 |
| 2019 | 0.08 |
| 2018 | 0.08 |
| 2017 | 0.08 |
| 2016 | 0.08 |
| 2015 | 0.08 |
| 2014 | 0.11 |
| 2013 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
