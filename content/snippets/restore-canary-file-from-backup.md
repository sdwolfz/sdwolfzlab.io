---
title: "Restore canary file from backup"
date: 2020-12-17T11:19:37Z
type: posts
slug: restore-canary-file-from-backup
tags: ["backup", "linux"]
---

## Why

To ensure you can successfully restore a file from a `tar` backup. A `sha256sum`
only checks the integrity of the created archive, not it's restorability.

## Prerequisites

1. Create a playground:

```sh
mkdir archive-test
cd archive-test
```

2. Create some content:

```sh
mkdir content
echo content1 >> content/file1
echo content2 >> content/file2
echo content3 >> content/file3
```

3. Back up your content:

```sh
tar -cf backup1.tar content
```

## Canary

1. Generate a random id:

```sh
export CANARY_ID=$(tr -dc "a-zA-Z0-9" < /dev/urandom | head -c 32)
```

2. Save that id to a file:

```sh
echo $CANARY_ID > canary
```

3. Add it to your archive:

```sh
tar -rf backup1.tar canary
```

4. Rename your archive to contain the id:

```sh
mv backup1.tar backup1."$CANARY_ID".tar
```

5. Remove the unarchived canary file:

```sh
rm -f canary
```

6. Optionally compress/encrypt/checksum the archive with `gzip`, `gpg`, and
   `sha256sum`.

## Restore

1. Extract the canary id:

```sh
export RESTORE_ID=$(ls backup1.$CANARY_ID.tar | cut -d '.' -f 2)
```

2. Extract the canary file:

```sh
tar -xf backup1."$CANARY_ID".tar canary
```

3. Compare the two values:

```sh
[ "$CANARY_ID" == "$RESTORE_ID" ] && echo "Match!" || echo "Fail!" && false
```

## Cleanup

Don't forget to remove your test files after playing with this:

```sh
cd ..
rm -rf archive-test
```
