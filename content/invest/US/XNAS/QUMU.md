---
title: "QUMU CORPORATION (QUMU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>QUMU CORPORATION</td></tr>
    <tr><td>Symbol</td><td>QUMU</td></tr>
    <tr><td>Web</td><td><a href="https://www.qumu.com">www.qumu.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2012 | 0.51 |
| 2011 | 0.47 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
