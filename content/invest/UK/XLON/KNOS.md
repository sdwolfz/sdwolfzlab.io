---
title: "Kainos Group (KNOS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Kainos Group</td></tr>
    <tr><td>Symbol</td><td>KNOS</td></tr>
    <tr><td>Web</td><td><a href="https://www.kainos.com">www.kainos.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 3.5 |
| 2019 | 9.3 |
| 2018 | 6.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
