---
title: " (ARR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ARR</td></tr>
    <tr><td>Web</td><td><a href="https://www.armourreit.com">www.armourreit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.5 |
| 2020 | 1.2 |
| 2019 | 2.16 |
| 2018 | 2.28 |
| 2017 | 2.28 |
| 2016 | 3.02 |
| 2015 | 1.93 |
| 2014 | 0.6 |
| 2013 | 0.22 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
