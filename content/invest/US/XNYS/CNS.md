---
title: "COHEN & STEERS INC (CNS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>COHEN & STEERS INC</td></tr>
    <tr><td>Symbol</td><td>CNS</td></tr>
    <tr><td>Web</td><td><a href="https://www.cohenandsteers.com">www.cohenandsteers.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.45 |
| 2020 | 2.56 |
| 2019 | 3.44 |
| 2018 | 3.82 |
| 2017 | 2.12 |
| 2016 | 1.54 |
| 2015 | 1.656 |
| 2014 | 0.88 |
| 2013 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
