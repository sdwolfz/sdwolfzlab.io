---
title: "How to Use: rubocop"
date: 2021-05-04T10:00:23Z
type: posts
slug: how-to-use-rubocop
series: ["How to Use"]
tags: ["ruby", "linter", "LSP"]
---

## Install

```sh
gem install rubocop
```

## Configure

Create a global `.rubocop.yml` file in your home directory:

```sh
touch ~/.rubocop.yml
```

And add in any general rules:

```yaml
---

Layout/HashAlignment:
  EnforcedColonStyle: table

Style/WordArray:
  EnforcedStyle: brackets
```

## Project

Do the same thing as above, but in your project root directory.

> NOTE: Do not inherit from global config inside your project, EVER!

## Run

Standalone:

```sh
rubocop
```

Or in a project:

```sh
bundle exec rubocop
```
