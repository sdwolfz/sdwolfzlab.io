---
title: "TEXAS ROADHOUSE INC (TXRH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TEXAS ROADHOUSE INC</td></tr>
    <tr><td>Symbol</td><td>TXRH</td></tr>
    <tr><td>Web</td><td><a href="https://www.texasroadhouse.com">www.texasroadhouse.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.4 |
| 2020 | 0.36 |
| 2019 | 1.2 |
| 2018 | 1.0 |
| 2017 | 0.84 |
| 2016 | 0.76 |
| 2015 | 0.68 |
| 2014 | 0.6 |
| 2013 | 0.48 |
| 2012 | 0.46 |
| 2011 | 0.32 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
