---
title: "MOBAM SA BAIA MARE (MOBD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>MOBAM SA BAIA MARE</td></tr>
    <tr><td>Symbol</td><td>MOBD</td></tr>
    <tr><td>Web</td><td><a href="https://www.mobam.ro">www.mobam.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.7 |
| 2020 | 3.85 |
| 2012 | 0.0786 |
| 2009 | 0.175 |
| 2008 | 3.8092 |
| 2003 | 0.0423 |
| 2002 | 0.0368 |
| 2001 | 0.0168 |
| 2000 | 0.0199 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
