---
title: " (SKM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SKM</td></tr>
    <tr><td>Web</td><td><a href="https://www.sktelecom.com">www.sktelecom.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.791 |
| 2019 | 0.721 |
| 2018 | 0.778 |
| 2017 | 0.828 |
| 2016 | 0.724 |
| 2015 | 0.054 |
| 2014 | 0.707 |
| 2013 | 0.735 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
