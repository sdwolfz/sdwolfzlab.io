---
title: "Topps Tiles (TPT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Topps Tiles</td></tr>
    <tr><td>Symbol</td><td>TPT</td></tr>
    <tr><td>Web</td><td><a href="https://www.toppstiles.co.uk">www.toppstiles.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 3.4 |
| 2018 | 3.4 |
| 2017 | 3.4 |
| 2016 | 3.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
