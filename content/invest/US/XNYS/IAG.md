---
title: "IAMGOLD CORP (IAG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>IAMGOLD CORP</td></tr>
    <tr><td>Symbol</td><td>IAG</td></tr>
    <tr><td>Web</td><td><a href="https://www.iamgold.com">www.iamgold.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2013 | 0.125 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
