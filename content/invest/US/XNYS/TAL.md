---
title: " (TAL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>TAL</td></tr>
    <tr><td>Web</td><td><a href="https://en.100tal.com">en.100tal.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 0.9 |
| 2015 | 1.89 |
| 2014 | 2.88 |
| 2013 | 1.38 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
