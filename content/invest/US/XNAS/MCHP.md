---
title: "MICROCHIP TECHNOLOGY (MCHP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MICROCHIP TECHNOLOGY</td></tr>
    <tr><td>Symbol</td><td>MCHP</td></tr>
    <tr><td>Web</td><td><a href="https://www.microchip.com">www.microchip.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.39 |
| 2020 | 1.471 |
| 2019 | 1.463 |
| 2018 | 1.455 |
| 2017 | 1.447 |
| 2016 | 1.439 |
| 2015 | 1.431 |
| 2014 | 1.423 |
| 2013 | 1.415 |
| 2012 | 1.402 |
| 2011 | 1.041 |
| 2010 | 1.715 |
| 2009 | 1.357 |
| 2008 | 1.327 |
| 2007 | 1.15 |
| 2006 | 0.89 |
| 2005 | 0.45 |
| 2004 | 0.173 |
| 2003 | 0.098 |
| 2002 | 0.02 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
