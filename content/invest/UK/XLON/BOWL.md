---
title: "Hollywood Bwl (BOWL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Hollywood Bwl</td></tr>
    <tr><td>Symbol</td><td>BOWL</td></tr>
    <tr><td>Web</td><td><a href="https://www.hollywoodbowl.co.uk">www.hollywoodbowl.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
