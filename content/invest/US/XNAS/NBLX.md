---
title: " (NBLX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>NBLX</td></tr>
    <tr><td>Web</td><td><a href="https://www.nblmidstream.com">www.nblmidstream.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.188 |
| 2020 | 1.25 |
| 2019 | 2.512 |
| 2018 | 2.094 |
| 2017 | 1.756 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
