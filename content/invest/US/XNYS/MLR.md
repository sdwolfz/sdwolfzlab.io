---
title: "MILLER INDUSTRIES INC (MLR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MILLER INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>MLR</td></tr>
    <tr><td>Web</td><td><a href="https://www.millerind.com">www.millerind.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.36 |
| 2020 | 0.72 |
| 2019 | 0.72 |
| 2018 | 0.72 |
| 2017 | 0.72 |
| 2016 | 0.68 |
| 2015 | 0.64 |
| 2014 | 0.6 |
| 2013 | 0.28 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
