---
title: "COMPLEX COMET SA BUCURESTI (COET)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>COMPLEX COMET SA BUCURESTI</td></tr>
    <tr><td>Symbol</td><td>COET</td></tr>
    <tr><td>Web</td><td><a href="https://www.complexcomet.ro">www.complexcomet.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
