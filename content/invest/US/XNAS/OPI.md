---
title: " (OPI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>OPI</td></tr>
    <tr><td>Web</td><td><a href="https://www.opireit.com">www.opireit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.1 |
| 2020 | 2.2 |
| 2019 | 2.2 |
| 2018 | 1.72 |
| 2017 | 1.72 |
| 2016 | 1.72 |
| 2015 | 1.72 |
| 2014 | 1.72 |
| 2013 | 1.72 |
| 2012 | 1.69 |
| 2011 | 1.67 |
| 2010 | 1.22 |
| 2009 | 0.9 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
