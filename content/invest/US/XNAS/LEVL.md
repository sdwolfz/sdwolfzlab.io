---
title: "LEVEL ONE BANCORP INC (LEVL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LEVEL ONE BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>LEVL</td></tr>
    <tr><td>Web</td><td><a href="https://www.levelonebank.com">www.levelonebank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.06 |
| 2020 | 0.2 |
| 2019 | 0.16 |
| 2018 | 0.09 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
