---
title: "TUPPERWARE BRANDS CORPORATION (TUP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TUPPERWARE BRANDS CORPORATION</td></tr>
    <tr><td>Symbol</td><td>TUP</td></tr>
    <tr><td>Web</td><td><a href="https://www.tupperwarebrands.com">www.tupperwarebrands.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.81 |
| 2018 | 2.72 |
| 2017 | 2.72 |
| 2016 | 2.72 |
| 2015 | 2.72 |
| 2014 | 2.72 |
| 2013 | 1.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
