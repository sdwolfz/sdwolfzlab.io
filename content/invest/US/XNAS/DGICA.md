---
title: "DONEGAL GROUP (DGICA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>DONEGAL GROUP</td></tr>
    <tr><td>Symbol</td><td>DGICA</td></tr>
    <tr><td>Web</td><td><a href="https://www.donegalgroup.com">www.donegalgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.31 |
| 2020 | 0.595 |
| 2019 | 0.577 |
| 2018 | 0.568 |
| 2017 | 0.558 |
| 2016 | 0.548 |
| 2015 | 0.537 |
| 2014 | 0.522 |
| 2013 | 0.505 |
| 2012 | 0.488 |
| 2011 | 0.475 |
| 2010 | 0.458 |
| 2009 | 0.443 |
| 2008 | 0.405 |
| 2007 | 0.353 |
| 2006 | 0.265 |
| 2005 | 0.42 |
| 2004 | 0.47 |
| 2003 | 0.43 |
| 2002 | 0.4 |
| 2001 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
