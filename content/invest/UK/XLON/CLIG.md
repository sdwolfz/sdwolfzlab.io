---
title: "City Lon Inv (CLIG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>City Lon Inv</td></tr>
    <tr><td>Symbol</td><td>CLIG</td></tr>
    <tr><td>Web</td><td><a href="https://www.clig.co.uk">www.clig.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 30.0 |
| 2019 | 40.5 |
| 2018 | 27.0 |
| 2017 | 25.0 |
| 2016 | 24.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
