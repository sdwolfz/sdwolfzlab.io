---
title: "Tyman (TYMN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Tyman</td></tr>
    <tr><td>Symbol</td><td>TYMN</td></tr>
    <tr><td>Web</td><td><a href="https://www.tymanplc.com">www.tymanplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 12.2 |
| 2018 | 12.0 |
| 2017 | 11.25 |
| 2016 | 10.5 |
| 2015 | 8.75 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
