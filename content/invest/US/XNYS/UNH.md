---
title: "UNITEDHEALTH GROUP INC (UNH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>UNITEDHEALTH GROUP INC</td></tr>
    <tr><td>Symbol</td><td>UNH</td></tr>
    <tr><td>Web</td><td><a href="https://www.unitedhealthgroup.com">www.unitedhealthgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.25 |
| 2020 | 4.83 |
| 2019 | 4.14 |
| 2018 | 3.45 |
| 2017 | 2.875 |
| 2016 | 2.375 |
| 2015 | 1.875 |
| 2014 | 1.405 |
| 2013 | 0.56 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
