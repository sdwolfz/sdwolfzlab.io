---
title: "KULICKE & SOFFA INDUSTRIES INC (KLIC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>KULICKE & SOFFA INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>KLIC</td></tr>
    <tr><td>Web</td><td><a href="https://www.kns.com">www.kns.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.14 |
| 2020 | 0.5 |
| 2019 | 0.48 |
| 2018 | 0.36 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
