---
title: "FIRST GUARANTY BANCSHARES INC (FGBI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST GUARANTY BANCSHARES INC</td></tr>
    <tr><td>Symbol</td><td>FGBI</td></tr>
    <tr><td>Web</td><td><a href="https://www.fgb.net">www.fgb.net</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.16 |
| 2020 | 0.64 |
| 2019 | 0.64 |
| 2018 | 0.64 |
| 2017 | 0.64 |
| 2016 | 0.64 |
| 2015 | 0.64 |
| 2014 | 0.48 |
| 2013 | 0.64 |
| 2012 | 0.64 |
| 2011 | 0.64 |
| 2010 | 0.8 |
| 2009 | 0.32 |
| 2008 | 0.64 |
| 2007 | 0.47 |
| 2006 | 0.75 |
| 2005 | 0.55 |
| 2004 | 0.48 |
| 2003 | 0.22 |
| 2002 | 0.31 |
| 2001 | 0.5 |
| 2000 | 0.4 |
| 1999 | 0.1 |
| 1998 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
