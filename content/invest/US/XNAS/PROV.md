---
title: "PROVIDENT FINANCIAL HOLDINGS (PROV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PROVIDENT FINANCIAL HOLDINGS</td></tr>
    <tr><td>Symbol</td><td>PROV</td></tr>
    <tr><td>Web</td><td><a href="https://www.myprovident.com">www.myprovident.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 0.56 |
| 2019 | 0.56 |
| 2018 | 0.56 |
| 2017 | 0.54 |
| 2016 | 0.5 |
| 2015 | 0.47 |
| 2014 | 0.42 |
| 2013 | 0.34 |
| 2012 | 0.18 |
| 2011 | 0.08 |
| 2010 | 0.04 |
| 2009 | 0.08 |
| 2008 | 0.38 |
| 2007 | 0.72 |
| 2006 | 0.63 |
| 2005 | 0.56 |
| 2004 | 0.44 |
| 2003 | 0.3 |
| 2002 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
