---
title: "ENTERGY CORP (ETR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ENTERGY CORP</td></tr>
    <tr><td>Symbol</td><td>ETR</td></tr>
    <tr><td>Web</td><td><a href="https://www.entergy.com">www.entergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.9 |
| 2020 | 3.74 |
| 2019 | 3.66 |
| 2018 | 3.58 |
| 2017 | 3.5 |
| 2016 | 3.42 |
| 2015 | 3.34 |
| 2014 | 3.32 |
| 2013 | 1.66 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
