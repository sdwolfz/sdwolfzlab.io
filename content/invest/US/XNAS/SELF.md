---
title: " (SELF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SELF</td></tr>
    <tr><td>Web</td><td><a href="https://www.globalselfstorage.us">www.globalselfstorage.us</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.065 |
| 2020 | 0.26 |
| 2019 | 0.26 |
| 2018 | 0.26 |
| 2017 | 0.26 |
| 2016 | 0.26 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
