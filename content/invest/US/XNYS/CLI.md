---
title: " (CLI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CLI</td></tr>
    <tr><td>Web</td><td><a href="https://www.mack-cali.com">www.mack-cali.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.6 |
| 2019 | 0.8 |
| 2018 | 0.8 |
| 2017 | 0.7 |
| 2016 | 0.6 |
| 2015 | 0.6 |
| 2014 | 0.9 |
| 2013 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
