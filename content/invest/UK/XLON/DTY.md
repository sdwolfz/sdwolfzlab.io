---
title: "Dignity (DTY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Dignity</td></tr>
    <tr><td>Symbol</td><td>DTY</td></tr>
    <tr><td>Web</td><td><a href="https://www.dignityfunerals.co.uk">www.dignityfunerals.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 24.38 |
| 2017 | 24.38 |
| 2016 | 22.16 |
| 2015 | 20.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
