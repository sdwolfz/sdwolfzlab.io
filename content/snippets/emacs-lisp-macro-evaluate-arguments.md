---
title: "Emacs Lisp macro to evaluate arguments"
date: 2021-10-18T23:42:59Z
type: posts
slug: emacs-lisp-macro-evaluate-arguments
tags: ["emacs", "lisp"]
---

One argument:

```elisp
(defmacro macro-evaluates-arg (arg)
  "Evaluate `ARG'."
  `(when (not (null arg))
     ,arg))
```

Multiple arguments:

```elisp
(defmacro macro-evaluates-args (&rest args)
  "Evaluate `ARGS'."
  `(when (not (null args))
     ,@args))
```
