---
title: "EURONAV (EURN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>EURONAV</td></tr>
    <tr><td>Symbol</td><td>EURN</td></tr>
    <tr><td>Web</td><td><a href="https://www.euronav.com">www.euronav.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.021 |
| 2020 | 1.139 |
| 2017 | 0.28 |
| 2016 | 1.37 |
| 2015 | 0.87 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
