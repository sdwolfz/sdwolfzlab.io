---
title: " (GLOP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>GLOP</td></tr>
    <tr><td>Web</td><td><a href="https://www.gaslogmlp.com">www.gaslogmlp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.02 |
| 2020 | 0.821 |
| 2019 | 2.2 |
| 2018 | 2.114 |
| 2017 | 2.017 |
| 2016 | 1.912 |
| 2015 | 1.783 |
| 2014 | 0.581 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
