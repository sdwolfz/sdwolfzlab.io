---
title: "EDESA BIOTECH INC (EDSA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>EDESA BIOTECH INC</td></tr>
    <tr><td>Symbol</td><td>EDSA</td></tr>
    <tr><td>Web</td><td><a href="https://www.edesabiotech.com">www.edesabiotech.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
