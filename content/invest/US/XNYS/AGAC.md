---
title: "AFRICAN GOLD ACQUISITION CORP (AGAC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AFRICAN GOLD ACQUISITION CORP</td></tr>
    <tr><td>Symbol</td><td>AGAC</td></tr>
    <tr><td>Web</td><td><a href="https://www.africangoldcorp.com">www.africangoldcorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
