---
title: "Temple Bar Investment Trust (TMPL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Temple Bar Investment Trust</td></tr>
    <tr><td>Symbol</td><td>TMPL</td></tr>
    <tr><td>Web</td><td><a href="https://www.templebarinvestments.co.uk">www.templebarinvestments.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 51.39 |
| 2018 | 46.72 |
| 2017 | 42.47 |
| 2016 | 40.45 |
| 2015 | 39.66 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
