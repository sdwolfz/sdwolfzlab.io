---
title: "GULF ISLAND FABRICATION INC (GIFI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GULF ISLAND FABRICATION INC</td></tr>
    <tr><td>Symbol</td><td>GIFI</td></tr>
    <tr><td>Web</td><td><a href="https://www.gulfisland.com">www.gulfisland.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 0.04 |
| 2016 | 0.04 |
| 2015 | 0.4 |
| 2014 | 0.4 |
| 2013 | 0.4 |
| 2012 | 0.4 |
| 2011 | 0.24 |
| 2010 | 0.04 |
| 2009 | 0.13 |
| 2008 | 0.4 |
| 2007 | 0.4 |
| 2006 | 0.3 |
| 2005 | 0.3 |
| 2004 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
