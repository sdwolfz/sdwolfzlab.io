---
title: "RIVERVIEW BANCORP INC (RVSB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>RIVERVIEW BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>RVSB</td></tr>
    <tr><td>Web</td><td><a href="https://www.riverviewbank.com">www.riverviewbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.2 |
| 2019 | 0.17 |
| 2018 | 0.13 |
| 2017 | 0.085 |
| 2016 | 0.06 |
| 2015 | 0.056 |
| 2008 | 0.225 |
| 2007 | 0.43 |
| 2006 | 0.56 |
| 2005 | 0.665 |
| 2004 | 0.605 |
| 2003 | 0.545 |
| 2002 | 0.485 |
| 2001 | 0.43 |
| 2000 | 0.39 |
| 1999 | 0.33 |
| 1998 | 0.215 |
| 1997 | 0.09 |
| 1996 | 0.165 |
| 1995 | 0.15 |
| 1994 | 0.375 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
