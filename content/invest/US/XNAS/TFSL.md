---
title: "TFS FINANCIAL CORPORATION (TFSL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TFS FINANCIAL CORPORATION</td></tr>
    <tr><td>Symbol</td><td>TFSL</td></tr>
    <tr><td>Web</td><td><a href="https://www.thirdfederal.com">www.thirdfederal.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 1.12 |
| 2019 | 1.04 |
| 2018 | 0.84 |
| 2017 | 0.59 |
| 2016 | 0.45 |
| 2015 | 0.34 |
| 2014 | 0.14 |
| 2010 | 0.14 |
| 2009 | 0.28 |
| 2008 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
