---
title: "TRANSALTA CORP MTN (TAC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TRANSALTA CORP MTN</td></tr>
    <tr><td>Symbol</td><td>TAC</td></tr>
    <tr><td>Web</td><td><a href="https://www.transalta.com">www.transalta.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.035 |
| 2020 | 0.129 |
| 2019 | 0.12 |
| 2018 | 0.122 |
| 2017 | 0.124 |
| 2016 | 0.122 |
| 2015 | 0.612 |
| 2014 | 0.72 |
| 2013 | 0.58 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
