---
title: "CAPITAL ONE FINANCIAL CORP (COF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CAPITAL ONE FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>COF</td></tr>
    <tr><td>Web</td><td><a href="https://www.capitalone.com">www.capitalone.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.8 |
| 2020 | 1.0 |
| 2019 | 1.6 |
| 2018 | 1.6 |
| 2017 | 1.6 |
| 2016 | 1.6 |
| 2015 | 1.5 |
| 2014 | 1.2 |
| 2013 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
