---
title: "MUELLER INDUSTRIES INC (MLI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MUELLER INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>MLI</td></tr>
    <tr><td>Web</td><td><a href="https://www.muellerindustries.com">www.muellerindustries.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.13 |
| 2020 | 0.4 |
| 2019 | 0.4 |
| 2018 | 0.4 |
| 2017 | 0.4 |
| 2016 | 0.375 |
| 2015 | 0.3 |
| 2014 | 0.3 |
| 2013 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
