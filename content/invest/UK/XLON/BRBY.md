---
title: "Burberry (BRBY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Burberry</td></tr>
    <tr><td>Symbol</td><td>BRBY</td></tr>
    <tr><td>Web</td><td><a href="https://www.burberryplc.com">www.burberryplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 11.3 |
| 2019 | 42.5 |
| 2018 | 41.3 |
| 2017 | 38.9 |
| 2016 | 37.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
