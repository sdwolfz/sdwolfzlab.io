---
title: "How to Use: fzf"
date: 2020-04-01T19:00:53Z
type: posts
slug: how-to-use-fzf
series: ["How to Use"]
tags: ["fzf", "tutorial", "linux", "bash", "productivity"]
---

## Install

```sh
sudo pacman -S fzf
```

## Setup

Add the following lines to your `.bash_profile`:

```sh
[[ -f /usr/share/fzf/completion.bash   ]] && . /usr/share/fzf/completion.bash
[[ -f /usr/share/fzf/key-bindings.bash ]] && . /usr/share/fzf/key-bindings.bash
```

## Usage

You have the following keybindings in an interactive bash shell:

| Key binding | Description                                    |
|-------------|------------------------------------------------|
| `Ctrl + r`  | Fuzzy search through your bash history.        |
| `Ctrl + t`  | fuzzy search a file in your current directory. |

Best used with the following bash config:
* [Configure your bash history](/snippets/configure-your-bash-history/)
