---
title: "BRIDGFORD FOODS CORP (BRID)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BRIDGFORD FOODS CORP</td></tr>
    <tr><td>Symbol</td><td>BRID</td></tr>
    <tr><td>Web</td><td><a href="https://www.bridgford.com">www.bridgford.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2012 | 0.05 |
| 2010 | 0.1 |
| 2009 | 0.1 |
| 2004 | 0.02 |
| 2003 | 0.14 |
| 2002 | 0.24 |
| 2001 | 0.28 |
| 2000 | 0.28 |
| 1999 | 0.25 |
| 1998 | 0.18 |
| 1997 | 0.18 |
| 1996 | 0.24 |
| 1995 | 0.21 |
| 1994 | 0.23 |
| 1993 | 0.14 |
| 1992 | 0.16 |
| 1991 | 0.165 |
| 1990 | 0.21 |
| 1989 | 0.225 |
| 1988 | 0.075 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
