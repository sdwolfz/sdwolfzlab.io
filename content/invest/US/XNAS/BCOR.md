---
title: "BLUCORA INC (BCOR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BLUCORA INC</td></tr>
    <tr><td>Symbol</td><td>BCOR</td></tr>
    <tr><td>Web</td><td><a href="https://www.blucora.com">www.blucora.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2008 | 9.0 |
| 2007 | 6.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
