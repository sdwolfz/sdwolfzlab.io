---
title: " (REXR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>REXR</td></tr>
    <tr><td>Web</td><td><a href="https://www.rexfordindustrial.com">www.rexfordindustrial.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.48 |
| 2020 | 0.86 |
| 2019 | 0.74 |
| 2018 | 0.64 |
| 2017 | 0.947 |
| 2016 | 0.54 |
| 2015 | 0.51 |
| 2014 | 0.48 |
| 2013 | 0.21 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
