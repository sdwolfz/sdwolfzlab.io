---
title: "FIRST HORIZON CORPORATION (FHN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FIRST HORIZON CORPORATION</td></tr>
    <tr><td>Symbol</td><td>FHN</td></tr>
    <tr><td>Web</td><td><a href="https://www.firsthorizon.com">www.firsthorizon.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 0.6 |
| 2019 | 0.56 |
| 2018 | 0.48 |
| 2017 | 0.36 |
| 2016 | 0.28 |
| 2015 | 0.24 |
| 2014 | 0.2 |
| 2013 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
