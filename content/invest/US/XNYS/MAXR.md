---
title: "MAXAR TECHNOLOGIES INC (MAXR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MAXAR TECHNOLOGIES INC</td></tr>
    <tr><td>Symbol</td><td>MAXR</td></tr>
    <tr><td>Web</td><td><a href="https://www.maxar.com">www.maxar.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.02 |
| 2020 | 0.04 |
| 2019 | 0.05 |
| 2018 | 1.428 |
| 2017 | 0.295 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
