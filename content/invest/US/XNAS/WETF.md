---
title: "WISDOMTREE INVESTMENTS INC (WETF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>WISDOMTREE INVESTMENTS INC</td></tr>
    <tr><td>Symbol</td><td>WETF</td></tr>
    <tr><td>Web</td><td><a href="https://www.wisdomtree.com">www.wisdomtree.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.06 |
| 2020 | 0.12 |
| 2019 | 0.12 |
| 2018 | 0.12 |
| 2017 | 0.32 |
| 2016 | 0.32 |
| 2015 | 0.57 |
| 2014 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
