---
title: "CONCHO RESOURCES INC (CXO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CONCHO RESOURCES INC</td></tr>
    <tr><td>Symbol</td><td>CXO</td></tr>
    <tr><td>Web</td><td><a href="http://www.concho.com">www.concho.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.8 |
| 2019 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
