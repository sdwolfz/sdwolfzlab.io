---
title: "STANTEC INC (STN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>STANTEC INC</td></tr>
    <tr><td>Symbol</td><td>STN</td></tr>
    <tr><td>Web</td><td><a href="https://www.stantec.com">www.stantec.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.467 |
| 2019 | 0.435 |
| 2018 | 0.422 |
| 2017 | 0.387 |
| 2016 | 0.339 |
| 2015 | 0.356 |
| 2014 | 0.74 |
| 2013 | 0.33 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
