---
title: "BROWN-FORMAN CORP (BF.B)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BROWN-FORMAN CORP</td></tr>
    <tr><td>Symbol</td><td>BF.B</td></tr>
    <tr><td>Web</td><td><a href="https://www.brown-forman.com">www.brown-forman.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.179 |
| 2020 | 0.701 |
| 2019 | 0.672 |
| 2018 | 1.482 |
| 2017 | 0.747 |
| 2016 | 1.033 |
| 2015 | 1.285 |
| 2014 | 1.185 |
| 2013 | 0.545 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
