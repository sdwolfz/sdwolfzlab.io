---
title: "Legal & General (LGEN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Legal & General</td></tr>
    <tr><td>Symbol</td><td>LGEN</td></tr>
    <tr><td>Web</td><td><a href="https://www.legalandgeneralgroup.com">www.legalandgeneralgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 17.57 |
| 2018 | 16.42 |
| 2017 | 15.35 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
