---
title: "CONSTRUCTII COMPLEXE SA BUZAU (CPLB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>CONSTRUCTII COMPLEXE SA BUZAU</td></tr>
    <tr><td>Symbol</td><td>CPLB</td></tr>
    <tr><td>Web</td><td><a href="https://www.scccbuzau.ro">www.scccbuzau.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 1.150084 |
| 2013 | 0.525636 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
