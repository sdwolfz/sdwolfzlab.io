---
title: "KASPIEN HOLDINGS INC (KSPN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>KASPIEN HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>KSPN</td></tr>
    <tr><td>Web</td><td><a href="https://www.kaspien.com">www.kaspien.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 0.5 |
| 2012 | 0.47 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
