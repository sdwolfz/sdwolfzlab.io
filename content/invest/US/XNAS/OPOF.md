---
title: "OLD POINT FINANCIAL CORP (OPOF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>OLD POINT FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>OPOF</td></tr>
    <tr><td>Web</td><td><a href="https://www.oldpoint.com">www.oldpoint.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 0.48 |
| 2019 | 0.48 |
| 2018 | 0.44 |
| 2017 | 0.44 |
| 2016 | 0.4 |
| 2015 | 0.34 |
| 2014 | 0.26 |
| 2013 | 0.22 |
| 2012 | 0.2 |
| 2011 | 0.2 |
| 2010 | 0.25 |
| 2009 | 0.47 |
| 2008 | 0.66 |
| 2007 | 0.72 |
| 2006 | 0.7 |
| 2005 | 0.66 |
| 2004 | 0.62 |
| 2003 | 0.54 |
| 2002 | 0.62 |
| 2001 | 0.62 |
| 2000 | 0.44 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
