---
title: "MANITOWOC CO INC (MTW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MANITOWOC CO INC</td></tr>
    <tr><td>Symbol</td><td>MTW</td></tr>
    <tr><td>Web</td><td><a href="https://www.manitowoc.com">www.manitowoc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 0.08 |
| 2014 | 0.08 |
| 2013 | 0.08 |
| 2012 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
