---
title: "CNB FINANCIAL CORP(PA) (CCNE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CNB FINANCIAL CORP(PA)</td></tr>
    <tr><td>Symbol</td><td>CCNE</td></tr>
    <tr><td>Web</td><td><a href="https://www.cnbbank.bank">www.cnbbank.bank</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.17 |
| 2020 | 0.68 |
| 2019 | 0.68 |
| 2018 | 0.67 |
| 2017 | 0.66 |
| 2016 | 0.66 |
| 2015 | 0.66 |
| 2014 | 0.66 |
| 2013 | 0.66 |
| 2012 | 0.66 |
| 2011 | 0.66 |
| 2010 | 0.66 |
| 2009 | 0.66 |
| 2008 | 0.645 |
| 2007 | 0.62 |
| 2006 | 0.57 |
| 2005 | 0.55 |
| 2004 | 0.71 |
| 2003 | 1.18 |
| 2002 | 1.18 |
| 2001 | 0.93 |
| 2000 | 0.84 |
| 1999 | 0.8 |
| 1998 | 0.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
