---
title: "GOLDEN ENTMT INC (GDEN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GOLDEN ENTMT INC</td></tr>
    <tr><td>Symbol</td><td>GDEN</td></tr>
    <tr><td>Web</td><td><a href="https://www.goldenent.com">www.goldenent.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2016 | 1.71 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
