---
title: "Senior (SNR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Senior</td></tr>
    <tr><td>Symbol</td><td>SNR</td></tr>
    <tr><td>Web</td><td><a href="https://www.seniorplc.com">www.seniorplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 7.51 |
| 2018 | 7.42 |
| 2017 | 6.95 |
| 2016 | 6.57 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
