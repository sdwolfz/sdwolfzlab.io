---
title: "KOSS CORP (KOSS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>KOSS CORP</td></tr>
    <tr><td>Symbol</td><td>KOSS</td></tr>
    <tr><td>Web</td><td><a href="https://www.koss.com">www.koss.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 0.06 |
| 2013 | 0.24 |
| 2012 | 0.24 |
| 2011 | 0.24 |
| 2010 | 0.24 |
| 2009 | 0.45 |
| 2008 | 0.52 |
| 2007 | 1.52 |
| 2006 | 1.52 |
| 2005 | 0.52 |
| 2004 | 0.52 |
| 2003 | 0.52 |
| 2002 | 0.51 |
| 2001 | 0.37 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
