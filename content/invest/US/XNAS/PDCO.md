---
title: "PATTERSON COMPANIES INC (PDCO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PATTERSON COMPANIES INC</td></tr>
    <tr><td>Symbol</td><td>PDCO</td></tr>
    <tr><td>Web</td><td><a href="https://www.pattersondental.com">www.pattersondental.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.52 |
| 2020 | 1.04 |
| 2019 | 1.04 |
| 2018 | 1.04 |
| 2017 | 1.02 |
| 2016 | 0.94 |
| 2015 | 0.86 |
| 2014 | 0.76 |
| 2013 | 0.48 |
| 2012 | 0.68 |
| 2011 | 0.46 |
| 2010 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
