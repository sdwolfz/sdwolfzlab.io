---
title: "Get SSH key fingerprint"
date: 2021-02-11T07:29:57Z
type: posts
slug: get-ssh-key-fingerprint
tags: ["linux", "security"]
---

```sh
ssh-keygen -l -E md5 -f ~/.ssh/my-ssh-key
```

It will display something like this:

```
256 MD5:8e:1c:b5:a3:f5:09:ca:72:ad:f0:1b:5d:ed:2a:03:38 my-ssh-key (ED25519)
```
