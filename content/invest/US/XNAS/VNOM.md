---
title: " (VNOM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>VNOM</td></tr>
    <tr><td>Web</td><td><a href="https://www.viperenergy.com">www.viperenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.39 |
| 2020 | 0.68 |
| 2019 | 1.82 |
| 2018 | 2.12 |
| 2017 | 1.229 |
| 2016 | 0.773 |
| 2015 | 0.86 |
| 2014 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
