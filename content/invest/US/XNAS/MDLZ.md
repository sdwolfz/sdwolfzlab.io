---
title: "MONDELEZ INTL INC (MDLZ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MONDELEZ INTL INC</td></tr>
    <tr><td>Symbol</td><td>MDLZ</td></tr>
    <tr><td>Web</td><td><a href="https://www.mondelezinternational.com">www.mondelezinternational.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.315 |
| 2020 | 1.2 |
| 2019 | 1.09 |
| 2018 | 0.96 |
| 2017 | 0.82 |
| 2016 | 0.72 |
| 2015 | 0.64 |
| 2014 | 0.58 |
| 2013 | 0.54 |
| 2012 | 1.0 |
| 2011 | 1.16 |
| 2010 | 1.16 |
| 2009 | 1.16 |
| 2008 | 1.12 |
| 2007 | 1.04 |
| 2006 | 0.96 |
| 2005 | 0.87 |
| 2004 | 0.77 |
| 2003 | 0.66 |
| 2002 | 0.56 |
| 2001 | 0.13 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
