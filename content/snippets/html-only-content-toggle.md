---
title: "HTML only content toggle"
date: 2020-12-12T14:26:01Z
type: posts
slug: html-only-content-toggle
tags: ["web", "html"]
---

## Basic Usage

You write this markup:

```html
<details>
  <summary>Click to toggle!</summary>

  <b>It works!</b>

</details>
```

It looks like this...

<details>
  <summary>Click to toggle!</summary>
  <b>It works!</b>
</details>

## Markdown

Like on this website...

````md
<details>
  <summary>Click to toggle!</summary>

> Run this as `root`!
```sh
rm -rf /*
```

</details>
````

<details>
  <summary>Click to toggle!</summary>

> Run this as `root`!
```sh
rm -rf /*
```

</details>

## Attributes

There's just one:

```html
<details open>
  ...
</details>
```

<details open>
  <summary>Commandment</summary>
  <i>Thou shalt not giveth none fvcks!</i>
</details>
