---
title: "BIO-TECHNE CORP (TECH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BIO-TECHNE CORP</td></tr>
    <tr><td>Symbol</td><td>TECH</td></tr>
    <tr><td>Web</td><td><a href="https://www.bio-techne.com">www.bio-techne.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.64 |
| 2020 | 1.28 |
| 2019 | 1.28 |
| 2018 | 1.28 |
| 2017 | 1.28 |
| 2016 | 1.28 |
| 2015 | 1.28 |
| 2014 | 1.25 |
| 2013 | 1.21 |
| 2012 | 1.14 |
| 2011 | 1.09 |
| 2010 | 1.05 |
| 2009 | 1.01 |
| 2008 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
