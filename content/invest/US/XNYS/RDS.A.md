---
title: " (RDS.A)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>RDS.A</td></tr>
    <tr><td>Web</td><td><a href="https://www.shell.com">www.shell.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.63 |
| 2020 | 1.626 |
| 2019 | 3.196 |
| 2018 | 3.196 |
| 2017 | 3.196 |
| 2016 | 3.196 |
| 2015 | 3.196 |
| 2014 | 3.162 |
| 2013 | 1.53 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
