---
title: "SIGNATURE BANK (SBNY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SIGNATURE BANK</td></tr>
    <tr><td>Symbol</td><td>SBNY</td></tr>
    <tr><td>Web</td><td><a href="https://www.signatureny.com">www.signatureny.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.12 |
| 2020 | 2.24 |
| 2019 | 2.24 |
| 2018 | 1.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
