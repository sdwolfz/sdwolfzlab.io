---
title: "WASHINGTON FEDERAL INC (WAFD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>WASHINGTON FEDERAL INC</td></tr>
    <tr><td>Symbol</td><td>WAFD</td></tr>
    <tr><td>Web</td><td><a href="https://www.wafdbank.com">www.wafdbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.46 |
| 2020 | 0.88 |
| 2019 | 0.82 |
| 2018 | 0.7 |
| 2017 | 0.85 |
| 2016 | 0.56 |
| 2015 | 0.52 |
| 2014 | 0.357 |
| 2013 | 0.38 |
| 2012 | 0.32 |
| 2011 | 0.26 |
| 2010 | 0.21 |
| 2009 | 0.2 |
| 2008 | 0.68 |
| 2007 | 0.835 |
| 2006 | 0.815 |
| 2005 | 0.79 |
| 2004 | 1.05 |
| 2003 | 0.65 |
| 2002 | 0.91 |
| 2001 | 0.95 |
| 2000 | 1.23 |
| 1999 | 0.92 |
| 1998 | 0.92 |
| 1997 | 0.92 |
| 1996 | 0.92 |
| 1995 | 0.9 |
| 1994 | 0.84 |
| 1993 | 0.84 |
| 1992 | 0.92 |
| 1991 | 1.08 |
| 1990 | 1.03 |
| 1989 | 0.873 |
| 1988 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
