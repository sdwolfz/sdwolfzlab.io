---
title: "NORTHROP GRUMMAN CORP (NOC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NORTHROP GRUMMAN CORP</td></tr>
    <tr><td>Symbol</td><td>NOC</td></tr>
    <tr><td>Web</td><td><a href="https://www.northropgrumman.com">www.northropgrumman.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.45 |
| 2020 | 5.67 |
| 2019 | 5.16 |
| 2018 | 4.7 |
| 2017 | 3.9 |
| 2016 | 3.5 |
| 2015 | 3.1 |
| 2014 | 2.71 |
| 2013 | 1.83 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
