---
title: "Berkeley Group (BKG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Berkeley Group</td></tr>
    <tr><td>Symbol</td><td>BKG</td></tr>
    <tr><td>Web</td><td><a href="https://www.berkeleygroup.co.uk">www.berkeleygroup.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 118.7 |
| 2019 | 40.6 |
| 2018 | 108.3 |
| 2017 | 185.24 |
| 2016 | 190.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
