---
title: "NATHANS FAMOUS INC (NATH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NATHANS FAMOUS INC</td></tr>
    <tr><td>Symbol</td><td>NATH</td></tr>
    <tr><td>Web</td><td><a href="https://www.nathansfamous.com">www.nathansfamous.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.35 |
| 2020 | 1.4 |
| 2019 | 1.3 |
| 2018 | 0.75 |
| 2017 | 5.0 |
| 2015 | 25.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
