---
title: "GAIA INC NEW (GAIA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GAIA INC NEW</td></tr>
    <tr><td>Symbol</td><td>GAIA</td></tr>
    <tr><td>Web</td><td><a href="https://www.gaia.com">www.gaia.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2010 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
