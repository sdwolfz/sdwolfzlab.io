---
title: "BTT SA BUCURESTI (BIBU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>BTT SA BUCURESTI</td></tr>
    <tr><td>Symbol</td><td>BIBU</td></tr>
    <tr><td>Web</td><td><a href="https://www.btt.ro">www.btt.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2000 | 2102.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
