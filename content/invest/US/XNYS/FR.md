---
title: " (FR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>FR</td></tr>
    <tr><td>Web</td><td><a href="https://www.firstindustrial.com">www.firstindustrial.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.54 |
| 2020 | 1.0 |
| 2019 | 0.92 |
| 2018 | 0.872 |
| 2017 | 0.84 |
| 2016 | 0.76 |
| 2015 | 0.508 |
| 2014 | 0.408 |
| 2013 | 0.17 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
