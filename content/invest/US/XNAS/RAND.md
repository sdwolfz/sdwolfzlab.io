---
title: "RAND CAPITAL CORP (RAND)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>RAND CAPITAL CORP</td></tr>
    <tr><td>Symbol</td><td>RAND</td></tr>
    <tr><td>Web</td><td><a href="https://www.randcapital.com">www.randcapital.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 2.95 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
