---
title: "ROSS STORES INC (ROST)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ROSS STORES INC</td></tr>
    <tr><td>Symbol</td><td>ROST</td></tr>
    <tr><td>Web</td><td><a href="https://www.rossstores.com">www.rossstores.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.285 |
| 2020 | 0.285 |
| 2019 | 1.02 |
| 2018 | 0.9 |
| 2017 | 0.64 |
| 2016 | 0.54 |
| 2015 | 0.705 |
| 2014 | 0.8 |
| 2013 | 0.68 |
| 2012 | 0.56 |
| 2011 | 0.88 |
| 2010 | 0.64 |
| 2009 | 0.44 |
| 2008 | 0.38 |
| 2007 | 0.3 |
| 2006 | 0.24 |
| 2005 | 0.21 |
| 2004 | 0.17 |
| 2003 | 0.23 |
| 2002 | 0.19 |
| 2001 | 0.17 |
| 2000 | 0.15 |
| 1999 | 0.228 |
| 1998 | 0.22 |
| 1997 | 0.135 |
| 1996 | 0.28 |
| 1995 | 0.24 |
| 1994 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
