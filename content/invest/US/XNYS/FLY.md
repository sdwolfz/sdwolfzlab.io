---
title: " (FLY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>FLY</td></tr>
    <tr><td>Web</td><td><a href="https://www.flyleasing.com">www.flyleasing.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 1.0 |
| 2014 | 0.85 |
| 2013 | 0.352 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
