---
title: "INTERNATIONAL CAVIAR CORPORATION SA CALAN (AMAL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>INTERNATIONAL CAVIAR CORPORATION SA CALAN</td></tr>
    <tr><td>Symbol</td><td>AMAL</td></tr>
    <tr><td>Web</td><td><a href="https://www.iccsa.ro">www.iccsa.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
