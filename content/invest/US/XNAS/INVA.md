---
title: "INNOVIVA INC (INVA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>INNOVIVA INC</td></tr>
    <tr><td>Symbol</td><td>INVA</td></tr>
    <tr><td>Web</td><td><a href="https://www.inva.com">www.inva.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 0.75 |
| 2014 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
