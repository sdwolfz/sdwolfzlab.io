---
title: "TRUSTMARK CORP (TRMK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TRUSTMARK CORP</td></tr>
    <tr><td>Symbol</td><td>TRMK</td></tr>
    <tr><td>Web</td><td><a href="https://www.trustmark.com">www.trustmark.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.46 |
| 2020 | 0.92 |
| 2019 | 0.92 |
| 2018 | 0.92 |
| 2017 | 0.92 |
| 2016 | 0.92 |
| 2015 | 0.92 |
| 2014 | 0.92 |
| 2013 | 0.92 |
| 2012 | 0.92 |
| 2011 | 0.92 |
| 2010 | 0.92 |
| 2009 | 0.92 |
| 2008 | 0.92 |
| 2007 | 0.89 |
| 2006 | 0.85 |
| 2005 | 0.81 |
| 2004 | 0.77 |
| 2003 | 0.685 |
| 2002 | 0.615 |
| 2001 | 0.555 |
| 2000 | 0.51 |
| 1999 | 0.44 |
| 1998 | 0.435 |
| 1997 | 0.585 |
| 1996 | 0.5 |
| 1995 | 0.442 |
| 1994 | 0.408 |
| 1993 | 0.66 |
| 1992 | 0.81 |
| 1991 | 1.038 |
| 1990 | 1.008 |
| 1989 | 0.72 |
| 1988 | 0.235 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
