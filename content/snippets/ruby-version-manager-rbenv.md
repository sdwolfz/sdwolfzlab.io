---
title: "Ruby Version Manager: rbenv"
date: 2022-09-18T10:01:05Z
type: posts
slug: ruby-version-manager-rbenv
series: ["Homebrew"]
tags: ["ruby", "linux", "bash", "development", "productivity"]
---

## Docker

The best way to manage ruby for your project is by using Docker:
* [Docker Images: basic Ruby setup](/snippets/docker-images-basic-ruby-setup/)
* [Docker Projects](https://gitlab.com/sdwolfz/docker-projects)

If you don't have a Docker setup yet, you have to suffer, temporarily, by using
`rbenv`. There used to be another tool called `rvm` for this sort of thing but
it's been abandoned for a long time now, it's insecure to install from their
website, has install errors, and it's completely broken for newer versions of
ruby and newer versions of OpenSSL. Your only real option is to use `rbenv`.

## Prerequsites

* [How to install Homebrew securely](/snippets/install-homebrew-securely/)

You can only install this via `homebrew`, unfortunately.

## Setup

1. Install it via `homebrew`:
    ```sh
    brew install rbenv
    ```
2. Get the shell setup command:

    ```sh
    rbenv init
    ```

3. Update your `Enable` funtion in `.bash_profile` to contain `rbenv`:

    ```bash
    function Enable {
      case "$1" in
        "brew")
          eval "$(~/Workspace/Hub/brew/bin/brew shellenv)"
          shift
          ;;
        "rbenv")
          Enable rbenv

          eval "$(rbenv init - bash)"
          shift
          ;;
        *)
          return
          ;;
      esac
    }
    ```
    > See: [How to install Homebrew securely](/snippets/install-homebrew-securely/#automation)

4. Finally, execute the same `eval` in your current shell:

    ```sh
    eval "$(rbenv init - bash)"
    ```
## Usage

FIrst you must make sure to enable it in your current shell:

```sh
Enable rbenv
```

Install the ruby versions you need:

```sh
rbenv install 2.7.6
rbenv install 3.0.4
rbenv install 3.1.2
```

List versions:

```sh
rbenv versions
```

Set a default global ruby version:

```sh
rbenv global 3.1.2
```

## OpenSSL

If you have OpenSSL issues, search for an older or compatible version:

```sh
brew search /openssl@/
```

Install it if you don't have it already, then configure the builder to use it:

```bash
export RUBY_CONFIGURE_OPTS="--with-openssl-dir=$(brew --prefix openssl@1.1)"
```

Now you can retry your install command!

## Project

Set up your project by creating a `.ruby-version` file in the root directory,
with the desired version:

```
3.1.2
```

Now when you `cd .` you should see the `ruby --version` changing, or a warning
if you don't have the appropriate ruby installed.
