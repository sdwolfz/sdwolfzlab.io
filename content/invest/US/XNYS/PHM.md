---
title: "PULTE GROUP INC (PHM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PULTE GROUP INC</td></tr>
    <tr><td>Symbol</td><td>PHM</td></tr>
    <tr><td>Web</td><td><a href="https://www.pultegroupinc.com">www.pultegroupinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.14 |
| 2020 | 0.5 |
| 2019 | 0.45 |
| 2018 | 0.38 |
| 2017 | 0.36 |
| 2016 | 0.36 |
| 2015 | 0.33 |
| 2014 | 0.23 |
| 2013 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
