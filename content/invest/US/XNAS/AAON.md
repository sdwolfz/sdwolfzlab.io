---
title: "AAON INC (AAON)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AAON INC</td></tr>
    <tr><td>Symbol</td><td>AAON</td></tr>
    <tr><td>Web</td><td><a href="https://www.aaon.com">www.aaon.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.38 |
| 2019 | 0.32 |
| 2018 | 0.32 |
| 2017 | 0.26 |
| 2016 | 0.24 |
| 2015 | 0.22 |
| 2014 | 0.22 |
| 2013 | 0.2 |
| 2012 | 0.36 |
| 2011 | 0.3 |
| 2010 | 0.36 |
| 2009 | 0.36 |
| 2008 | 0.32 |
| 2007 | 0.36 |
| 2006 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
