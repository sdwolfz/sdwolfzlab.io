---
title: "AMERICAN EQUITY INVT LIFE HLDG CO (AEL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AMERICAN EQUITY INVT LIFE HLDG CO</td></tr>
    <tr><td>Symbol</td><td>AEL</td></tr>
    <tr><td>Web</td><td><a href="https://www.american-equity.com">www.american-equity.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.32 |
| 2019 | 0.3 |
| 2018 | 0.28 |
| 2017 | 0.26 |
| 2016 | 0.24 |
| 2015 | 0.22 |
| 2014 | 0.2 |
| 2013 | 0.18 |
| 2012 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
