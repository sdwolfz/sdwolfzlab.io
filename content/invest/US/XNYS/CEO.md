---
title: "CNOOC LIMITED ADS EACH REP 100 ORD HKD0.02 (CEO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CNOOC LIMITED ADS EACH REP 100 ORD HKD0.02</td></tr>
    <tr><td>Symbol</td><td>CEO</td></tr>
    <tr><td>Web</td><td><a href="http://www.cnoocltd.com">www.cnoocltd.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 7.493 |
| 2019 | 8.349 |
| 2018 | 6.845 |
| 2017 | 4.917 |
| 2016 | 4.253 |
| 2015 | 6.617 |
| 2014 | 6.616 |
| 2013 | 6.615 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
