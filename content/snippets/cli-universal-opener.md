---
title: "CLI universal opener"
date: 2021-04-22T21:16:34Z
type: posts
slug: cli-universal-opener
tags: ["linux", "cli", "productivity"]
---

## Install

```sh
sudo pacman -S xdg-utils
```

## Use

```sh
xdg-open path/to/file/or/direcotry
```

But typing `xdg-open` is hard, shorten it with:

```sh
sudo ln -s /usr/bin/xdg-open /usr/bin/open
```

Why this is not set up like so by default is beyond me...
