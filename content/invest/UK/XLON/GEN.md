---
title: "Genuit Grp Plc (GEN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Genuit Grp Plc</td></tr>
    <tr><td>Symbol</td><td>GEN</td></tr>
    <tr><td>Web</td><td><a href="https://www.genuitgroup.com">www.genuitgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 12.1 |
| 2018 | 11.6 |
| 2017 | 11.1 |
| 2016 | 10.1 |
| 2015 | 7.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
