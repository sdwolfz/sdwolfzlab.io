---
title: "Replace string in all files with sed"
date: 2021-05-09T01:55:19Z
type: posts
slug: replace-string-in-all-files-with-sed
tags: ["linux", "productivity", "sed"]
---

```sh
find . -type f -exec sed -i 's/STRING GOES HERE/REPLACEMENT HERE/g' {} +
```

Make sure to escape brackets (`\[\]`):

```sh
find . -type f -exec sed -i 's|Click \[here\](/here/)!|Click HERE!|g' {} +
```
