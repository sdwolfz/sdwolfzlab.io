---
title: "PAM TRANSPORTATION SERVICES INC. (PTSI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PAM TRANSPORTATION SERVICES INC.</td></tr>
    <tr><td>Symbol</td><td>PTSI</td></tr>
    <tr><td>Web</td><td><a href="https://www.pamtransport.com">www.pamtransport.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2012 | 2.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
