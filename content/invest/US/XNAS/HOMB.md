---
title: "HOME BANCSHARES INC. (HOMB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HOME BANCSHARES INC.</td></tr>
    <tr><td>Symbol</td><td>HOMB</td></tr>
    <tr><td>Web</td><td><a href="https://www.homebancshares.com">www.homebancshares.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 0.53 |
| 2019 | 0.51 |
| 2018 | 0.46 |
| 2017 | 0.4 |
| 2016 | 0.505 |
| 2015 | 0.55 |
| 2014 | 0.35 |
| 2013 | 0.43 |
| 2012 | 0.58 |
| 2011 | 0.268 |
| 2010 | 0.228 |
| 2009 | 0.24 |
| 2008 | 0.23 |
| 2007 | 0.145 |
| 2006 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
