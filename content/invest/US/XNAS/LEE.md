---
title: "LEE ENTERPRISES INC (LEE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LEE ENTERPRISES INC</td></tr>
    <tr><td>Symbol</td><td>LEE</td></tr>
    <tr><td>Web</td><td><a href="https://www.lee.net">www.lee.net</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2008 | 0.57 |
| 2007 | 0.73 |
| 2006 | 0.72 |
| 2005 | 0.54 |
| 2004 | 0.72 |
| 2003 | 0.69 |
| 2002 | 0.68 |
| 2001 | 0.68 |
| 2000 | 0.33 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
