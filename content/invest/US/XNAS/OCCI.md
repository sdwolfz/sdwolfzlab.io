---
title: "OFS CREDIT COMPANY INC (OCCI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>OFS CREDIT COMPANY INC</td></tr>
    <tr><td>Symbol</td><td>OCCI</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.53 |
| 2020 | 2.25 |
| 2019 | 2.01 |
| 2018 | 0.447 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
