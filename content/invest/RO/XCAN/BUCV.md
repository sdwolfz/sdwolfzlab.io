---
title: "BUCUR SA Bucuresti (BUCV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>BUCUR SA Bucuresti</td></tr>
    <tr><td>Symbol</td><td>BUCV</td></tr>
    <tr><td>Web</td><td><a href="https://www.bucurcom.ro">www.bucurcom.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2006 | 0.134 |
| 2005 | 0.0442 |
| 2004 | 0.2247 |
| 2003 | 0.2069 |
| 2002 | 0.7163 |
| 2001 | 0.3871 |
| 2000 | 1.0456 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
