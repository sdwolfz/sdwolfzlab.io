---
title: "Marshalls (MSLH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Marshalls</td></tr>
    <tr><td>Symbol</td><td>MSLH</td></tr>
    <tr><td>Web</td><td><a href="https://www.marshalls.co.uk">www.marshalls.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 16.7 |
| 2018 | 16.0 |
| 2017 | 12.2 |
| 2016 | 9.65 |
| 2015 | 6.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
