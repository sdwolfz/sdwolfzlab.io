---
title: " (NGG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>NGG</td></tr>
    <tr><td>Web</td><td><a href="https://www.nationalgrid.com">www.nationalgrid.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 3.111 |
| 2019 | 3.063 |
| 2018 | 3.093 |
| 2017 | 1.872 |
| 2016 | 2.957 |
| 2015 | 3.289 |
| 2014 | 5.744 |
| 2013 | 3.148 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
