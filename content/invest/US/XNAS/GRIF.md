---
title: "GRIFFIN INDUSTRIAL REALTY INC (GRIF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GRIFFIN INDUSTRIAL REALTY INC</td></tr>
    <tr><td>Symbol</td><td>GRIF</td></tr>
    <tr><td>Web</td><td><a href="http://www.griffinindustrial.com">www.griffinindustrial.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.5 |
| 2018 | 0.45 |
| 2017 | 0.4 |
| 2016 | 0.3 |
| 2015 | 0.3 |
| 2014 | 0.2 |
| 2013 | 0.2 |
| 2012 | 0.2 |
| 2011 | 0.4 |
| 2010 | 0.4 |
| 2009 | 0.4 |
| 2008 | 0.4 |
| 2007 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
