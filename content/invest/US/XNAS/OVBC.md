---
title: "OHIO VALLEY BANCORP (OVBC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>OHIO VALLEY BANCORP</td></tr>
    <tr><td>Symbol</td><td>OVBC</td></tr>
    <tr><td>Web</td><td><a href="https://www.ovbc.com">www.ovbc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.42 |
| 2020 | 0.84 |
| 2019 | 0.84 |
| 2018 | 0.84 |
| 2017 | 0.84 |
| 2016 | 0.84 |
| 2015 | 0.89 |
| 2014 | 0.84 |
| 2013 | 0.73 |
| 2012 | 1.09 |
| 2011 | 0.84 |
| 2010 | 0.84 |
| 2009 | 0.8 |
| 2008 | 0.76 |
| 2007 | 0.71 |
| 2006 | 0.67 |
| 2005 | 0.71 |
| 2004 | 0.94 |
| 2003 | 0.71 |
| 2002 | 0.67 |
| 2001 | 0.79 |
| 2000 | 0.59 |
| 1999 | 0.56 |
| 1998 | 0.69 |
| 1997 | 0.85 |
| 1996 | 0.75 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
