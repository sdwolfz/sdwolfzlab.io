---
title: "MMA CAPITAL HOLDINGS INC (MMAC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MMA CAPITAL HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>MMAC</td></tr>
    <tr><td>Web</td><td><a href="https://www.mmacapitalholdings.com">www.mmacapitalholdings.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2008 | 0.33 |
| 2007 | 2.077 |
| 2006 | 2.0 |
| 2005 | 1.92 |
| 2004 | 1.84 |
| 2003 | 1.785 |
| 2002 | 1.745 |
| 2001 | 1.705 |
| 2000 | 0.42 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
