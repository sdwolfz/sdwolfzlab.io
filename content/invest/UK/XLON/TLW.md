---
title: "Tullow Oil (TLW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Tullow Oil</td></tr>
    <tr><td>Symbol</td><td>TLW</td></tr>
    <tr><td>Web</td><td><a href="https://www.tullowoil.com">www.tullowoil.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 2.35 |
| 2017 | 63.29 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
