---
title: "Foresight Solar (FSFL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Foresight Solar</td></tr>
    <tr><td>Symbol</td><td>FSFL</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 6.91 |
| 2019 | 6.68 |
| 2018 | 6.44 |
| 2017 | 6.32 |
| 2016 | 6.17 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
