---
title: "AURORA S.A. (AUR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>AURORA S.A.</td></tr>
    <tr><td>Symbol</td><td>AUR</td></tr>
    <tr><td>Web</td><td><a href="https://www.aurorashoes.ro">www.aurorashoes.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2007 | 0.9807 |
| 2006 | 0.8123 |
| 2000 | 2.6059 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
