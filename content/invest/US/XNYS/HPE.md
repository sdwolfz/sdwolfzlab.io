---
title: "HEWLETT PACKARD ENTERPRISE CO (HPE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HEWLETT PACKARD ENTERPRISE CO</td></tr>
    <tr><td>Symbol</td><td>HPE</td></tr>
    <tr><td>Web</td><td><a href="https://www.hpe.com">www.hpe.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 0.48 |
| 2019 | 0.456 |
| 2018 | 0.411 |
| 2017 | 0.27 |
| 2016 | 0.23 |
| 2015 | 0.055 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
