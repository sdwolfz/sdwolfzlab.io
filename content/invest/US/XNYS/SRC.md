---
title: " (SRC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SRC</td></tr>
    <tr><td>Web</td><td><a href="https://www.spiritrealty.com">www.spiritrealty.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.625 |
| 2020 | 2.5 |
| 2019 | 2.5 |
| 2018 | 1.11 |
| 2017 | 0.72 |
| 2016 | 0.705 |
| 2015 | 0.685 |
| 2014 | 0.668 |
| 2013 | 0.332 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
