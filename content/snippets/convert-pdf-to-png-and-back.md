---
title: "Convert PDF to PNG and back"
date: 2021-05-19T08:48:08Z
type: posts
slug: convert-pdf-to-png-and-back
tags: ["linux", "pdf", "png", "document", "gimp"]
---

## Prerequisites

```sh
sudo pacman -S gimp imagemagick poppler
```

## PDF to PNG

Create a directory with your `INITAIL.pdf` then execute this inside of it:

```sh
pdftoppm INITIAL.pdf prefix-for-images -png
```

## Do whatever

Edit your images in `gimp` for example:

```sh
gimp prefix-for-images-1.png
```

## Join PNGs to PDF

```sh
convert prefix-for-images-*.png FINAL.pdf
```
