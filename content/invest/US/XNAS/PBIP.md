---
title: "PRUDENTIAL BANCORP INC NEW (PBIP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PRUDENTIAL BANCORP INC NEW</td></tr>
    <tr><td>Symbol</td><td>PBIP</td></tr>
    <tr><td>Web</td><td><a href="https://www.psbanker.com">www.psbanker.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.07 |
| 2020 | 0.71 |
| 2019 | 0.67 |
| 2018 | 0.55 |
| 2017 | 0.29 |
| 2016 | 0.12 |
| 2015 | 0.27 |
| 2014 | 0.09 |
| 2011 | 0.1 |
| 2010 | 0.2 |
| 2009 | 0.2 |
| 2008 | 0.2 |
| 2007 | 0.19 |
| 2006 | 0.16 |
| 2005 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
