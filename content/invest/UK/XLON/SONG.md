---
title: "Hipgnosis Song. (SONG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Hipgnosis Song.</td></tr>
    <tr><td>Symbol</td><td>SONG</td></tr>
    <tr><td>Web</td><td><a href="https://www.hipgnosissongs.com">www.hipgnosissongs.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 5.0 |
| 2019 | 2.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
