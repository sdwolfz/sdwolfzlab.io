---
title: " (GOOD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>GOOD</td></tr>
    <tr><td>Web</td><td><a href="https://www.gladstonecommercial.com">www.gladstonecommercial.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.751 |
| 2020 | 1.502 |
| 2019 | 1.5 |
| 2018 | 1.5 |
| 2017 | 1.5 |
| 2016 | 1.5 |
| 2015 | 1.5 |
| 2014 | 1.5 |
| 2013 | 1.5 |
| 2012 | 1.5 |
| 2011 | 1.5 |
| 2010 | 1.5 |
| 2009 | 1.5 |
| 2008 | 1.5 |
| 2007 | 1.44 |
| 2006 | 1.44 |
| 2005 | 0.96 |
| 2004 | 0.48 |
| 2003 | 0.01 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
