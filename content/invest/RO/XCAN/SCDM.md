---
title: "UNIREA SHOPPING CENTER SA Bucuresti (SCDM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>UNIREA SHOPPING CENTER SA Bucuresti</td></tr>
    <tr><td>Symbol</td><td>SCDM</td></tr>
    <tr><td>Web</td><td><a href="https://www.unireashop.ro">www.unireashop.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2008 | 3.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
