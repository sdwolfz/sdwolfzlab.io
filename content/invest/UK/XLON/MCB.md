---
title: "Mcbride (MCB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Mcbride</td></tr>
    <tr><td>Symbol</td><td>MCB</td></tr>
    <tr><td>Web</td><td><a href="https://www.mcbride.co.uk">www.mcbride.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.1 |
| 2019 | 3.3 |
| 2018 | 4.3 |
| 2017 | 4.3 |
| 2016 | 3.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
