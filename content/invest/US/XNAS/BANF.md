---
title: "BANCFIRST CORP (BANF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BANCFIRST CORP</td></tr>
    <tr><td>Symbol</td><td>BANF</td></tr>
    <tr><td>Web</td><td><a href="https://www.bancfirst.bank">www.bancfirst.bank</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.34 |
| 2020 | 1.32 |
| 2019 | 1.24 |
| 2018 | 1.02 |
| 2017 | 1.18 |
| 2016 | 1.48 |
| 2015 | 1.4 |
| 2014 | 1.3 |
| 2013 | 1.2 |
| 2012 | 1.12 |
| 2011 | 1.04 |
| 2010 | 0.96 |
| 2009 | 0.9 |
| 2008 | 0.84 |
| 2007 | 0.76 |
| 2006 | 0.68 |
| 2005 | 1.2 |
| 2004 | 1.06 |
| 2003 | 0.94 |
| 2002 | 0.8 |
| 2001 | 0.72 |
| 2000 | 0.66 |
| 1999 | 0.58 |
| 1998 | 0.5 |
| 1997 | 0.42 |
| 1996 | 0.42 |
| 1995 | 0.21 |
| 1994 | 0.25 |
| 1993 | 0.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
