---
title: "JABIL INC (JBL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>JABIL INC</td></tr>
    <tr><td>Symbol</td><td>JBL</td></tr>
    <tr><td>Web</td><td><a href="https://www.jabil.com">www.jabil.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.16 |
| 2020 | 0.32 |
| 2019 | 0.32 |
| 2018 | 0.32 |
| 2017 | 0.32 |
| 2016 | 0.32 |
| 2015 | 0.32 |
| 2014 | 0.32 |
| 2013 | 0.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
