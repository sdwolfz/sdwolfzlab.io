---
title: "ENDEAVOR GROUP HOLDINGS INC (EDR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ENDEAVOR GROUP HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>EDR</td></tr>
    <tr><td>Web</td><td><a href="https://www.endeavorco.com">www.endeavorco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.78 |
| 2017 | 1.54 |
| 2016 | 1.5 |
| 2015 | 1.46 |
| 2014 | 0.7 |
| 2013 | 0.22 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
