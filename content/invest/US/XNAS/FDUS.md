---
title: "FIDUS INVESTMENT CORPORATION (FDUS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIDUS INVESTMENT CORPORATION</td></tr>
    <tr><td>Symbol</td><td>FDUS</td></tr>
    <tr><td>Web</td><td><a href="https://www.fdus.com">www.fdus.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.38 |
| 2020 | 1.33 |
| 2019 | 1.6 |
| 2018 | 1.6 |
| 2017 | 1.6 |
| 2016 | 1.6 |
| 2015 | 1.6 |
| 2014 | 1.72 |
| 2013 | 1.94 |
| 2012 | 1.46 |
| 2011 | 0.64 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
