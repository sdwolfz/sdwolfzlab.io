---
title: "NATIONAL HEALTH CARE CORP (NHC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NATIONAL HEALTH CARE CORP</td></tr>
    <tr><td>Symbol</td><td>NHC</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.04 |
| 2020 | 2.08 |
| 2019 | 2.06 |
| 2018 | 1.98 |
| 2017 | 1.89 |
| 2016 | 1.75 |
| 2015 | 1.54 |
| 2014 | 1.34 |
| 2013 | 1.26 |
| 2012 | 2.2 |
| 2011 | 1.18 |
| 2010 | 1.1 |
| 2009 | 1.02 |
| 2008 | 0.93 |
| 2007 | 0.81 |
| 2006 | 0.69 |
| 2005 | 0.575 |
| 2004 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
