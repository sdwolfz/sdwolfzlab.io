---
title: "SIMMONS FIRST NATIONAL CORP (SFNC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SIMMONS FIRST NATIONAL CORP</td></tr>
    <tr><td>Symbol</td><td>SFNC</td></tr>
    <tr><td>Web</td><td><a href="https://www.simmonsbank.com">www.simmonsbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.36 |
| 2020 | 0.68 |
| 2019 | 0.64 |
| 2018 | 0.6 |
| 2017 | 1.0 |
| 2016 | 0.96 |
| 2015 | 0.92 |
| 2014 | 0.88 |
| 2013 | 0.84 |
| 2012 | 0.8 |
| 2011 | 0.76 |
| 2010 | 0.76 |
| 2009 | 0.76 |
| 2008 | 0.76 |
| 2007 | 0.73 |
| 2006 | 0.68 |
| 2005 | 0.61 |
| 2004 | 0.57 |
| 2003 | 0.65 |
| 2002 | 0.96 |
| 2001 | 0.88 |
| 2000 | 0.8 |
| 1999 | 0.72 |
| 1998 | 0.64 |
| 1997 | 0.56 |
| 1996 | 0.65 |
| 1995 | 0.59 |
| 1994 | 0.47 |
| 1993 | 0.3 |
| 1992 | 0.4 |
| 1991 | 0.73 |
| 1990 | 0.68 |
| 1989 | 0.6 |
| 1988 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
