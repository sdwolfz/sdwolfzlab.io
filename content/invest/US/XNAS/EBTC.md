---
title: "ENTERPRISE BANCORP INC (EBTC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ENTERPRISE BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>EBTC</td></tr>
    <tr><td>Web</td><td><a href="https://www.enterprisebanking.com">www.enterprisebanking.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.37 |
| 2020 | 0.7 |
| 2019 | 0.64 |
| 2018 | 0.58 |
| 2017 | 0.54 |
| 2016 | 0.52 |
| 2015 | 0.5 |
| 2014 | 0.48 |
| 2013 | 0.46 |
| 2012 | 0.44 |
| 2011 | 0.42 |
| 2010 | 0.4 |
| 2009 | 0.38 |
| 2008 | 0.36 |
| 2007 | 0.32 |
| 2006 | 0.42 |
| 2005 | 0.48 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
