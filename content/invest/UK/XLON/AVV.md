---
title: "Aveva Group (AVV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Aveva Group</td></tr>
    <tr><td>Symbol</td><td>AVV</td></tr>
    <tr><td>Web</td><td><a href="https://www.aveva.com">www.aveva.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 44.5 |
| 2019 | 29.0 |
| 2018 | 27.0 |
| 2017 | 40.0 |
| 2016 | 36.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
