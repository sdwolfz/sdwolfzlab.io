---
title: "SCRIPPS (EW) COMPANY (SSP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SCRIPPS (EW) COMPANY</td></tr>
    <tr><td>Symbol</td><td>SSP</td></tr>
    <tr><td>Web</td><td><a href="https://www.scripps.com">www.scripps.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.2 |
| 2019 | 0.2 |
| 2018 | 0.2 |
| 2008 | 0.43 |
| 2007 | 0.54 |
| 2006 | 0.47 |
| 2005 | 0.43 |
| 2004 | 0.675 |
| 2003 | 0.6 |
| 2002 | 0.6 |
| 2001 | 0.6 |
| 2000 | 0.28 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
