---
title: "TAPESTRY INC (TPR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TAPESTRY INC</td></tr>
    <tr><td>Symbol</td><td>TPR</td></tr>
    <tr><td>Web</td><td><a href="https://www.tapestry.com">www.tapestry.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.338 |
| 2019 | 1.352 |
| 2018 | 1.352 |
| 2017 | 0.676 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
