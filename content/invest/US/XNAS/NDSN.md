---
title: "NORDSON CORP (NDSN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NORDSON CORP</td></tr>
    <tr><td>Symbol</td><td>NDSN</td></tr>
    <tr><td>Web</td><td><a href="https://www.nordson.com">www.nordson.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.39 |
| 2020 | 1.54 |
| 2019 | 1.46 |
| 2018 | 1.3 |
| 2017 | 1.14 |
| 2016 | 1.02 |
| 2015 | 0.92 |
| 2014 | 0.8 |
| 2013 | 0.66 |
| 2012 | 0.55 |
| 2011 | 0.565 |
| 2010 | 0.8 |
| 2009 | 0.745 |
| 2008 | 0.73 |
| 2007 | 0.708 |
| 2006 | 0.68 |
| 2005 | 0.65 |
| 2004 | 0.63 |
| 2003 | 0.61 |
| 2002 | 0.58 |
| 2001 | 0.56 |
| 2000 | 0.92 |
| 1999 | 0.98 |
| 1998 | 0.9 |
| 1997 | 0.82 |
| 1996 | 0.74 |
| 1995 | 0.66 |
| 1994 | 0.58 |
| 1993 | 0.5 |
| 1992 | 0.45 |
| 1991 | 0.71 |
| 1990 | 0.74 |
| 1989 | 0.66 |
| 1988 | 0.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
