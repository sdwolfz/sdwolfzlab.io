---
title: " (VEON)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>VEON</td></tr>
    <tr><td>Web</td><td><a href="https://www.veon.com">www.veon.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.15 |
| 2019 | 0.3 |
| 2018 | 0.29 |
| 2017 | 0.305 |
| 2016 | 0.035 |
| 2015 | 0.035 |
| 2014 | 0.035 |
| 2013 | 1.59 |
| 2012 | 1.15 |
| 2011 | 0.79 |
| 2010 | 0.46 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
