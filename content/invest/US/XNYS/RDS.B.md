---
title: " (RDS.B)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>RDS.B</td></tr>
    <tr><td>Web</td><td><a href="https://www.shell.com">www.shell.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.68 |
| 2020 | 1.913 |
| 2019 | 3.76 |
| 2018 | 3.76 |
| 2017 | 2.82 |
| 2016 | 3.76 |
| 2015 | 3.76 |
| 2014 | 3.72 |
| 2013 | 1.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
