---
title: "ACUSHNET HOLDINGS CORP (GOLF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ACUSHNET HOLDINGS CORP</td></tr>
    <tr><td>Symbol</td><td>GOLF</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.165 |
| 2020 | 0.62 |
| 2019 | 0.56 |
| 2018 | 0.52 |
| 2017 | 0.48 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
