---
title: "PATTERSON UTI ENERGY INC (PTEN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PATTERSON UTI ENERGY INC</td></tr>
    <tr><td>Symbol</td><td>PTEN</td></tr>
    <tr><td>Web</td><td><a href="https://www.patenergy.com">www.patenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.04 |
| 2020 | 0.1 |
| 2019 | 0.16 |
| 2018 | 0.14 |
| 2017 | 0.08 |
| 2016 | 0.16 |
| 2015 | 0.4 |
| 2014 | 0.4 |
| 2013 | 0.2 |
| 2012 | 0.2 |
| 2011 | 0.2 |
| 2010 | 0.2 |
| 2009 | 0.2 |
| 2008 | 0.6 |
| 2007 | 0.44 |
| 2006 | 0.28 |
| 2005 | 0.16 |
| 2004 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
