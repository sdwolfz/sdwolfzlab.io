---
title: "NOV INC (NOV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NOV INC</td></tr>
    <tr><td>Symbol</td><td>NOV</td></tr>
    <tr><td>Web</td><td><a href="https://www.nov.com">www.nov.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.05 |
| 2019 | 0.2 |
| 2018 | 0.2 |
| 2017 | 0.2 |
| 2016 | 0.61 |
| 2015 | 1.84 |
| 2014 | 1.64 |
| 2013 | 0.52 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
