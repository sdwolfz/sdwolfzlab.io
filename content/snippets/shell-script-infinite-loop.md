---
title: "Shell script infinite loop"
date: 2021-06-11T13:24:23Z
type: posts
slug: shell-script-infinite-loop
tags: ["shell", "bash", "linux"]
---

Useful for all sorts of things:

```sh
while true; do echo 'Inside the loop'; sleep 5; done
```
