---
title: "WESTWOOD HOLDINGS GROUP INC (WHG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>WESTWOOD HOLDINGS GROUP INC</td></tr>
    <tr><td>Symbol</td><td>WHG</td></tr>
    <tr><td>Web</td><td><a href="https://www.westwoodgroup.com">www.westwoodgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.43 |
| 2019 | 2.88 |
| 2018 | 2.76 |
| 2017 | 2.54 |
| 2016 | 2.33 |
| 2015 | 2.07 |
| 2014 | 1.82 |
| 2013 | 0.84 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
