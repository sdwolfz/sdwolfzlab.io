---
title: "KIRKLAND LAKE GOLD (KL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>KIRKLAND LAKE GOLD</td></tr>
    <tr><td>Symbol</td><td>KL</td></tr>
    <tr><td>Web</td><td><a href="https://www.klgold.com">www.klgold.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.563 |
| 2019 | 0.17 |
| 2018 | 0.092 |
| 2017 | 0.024 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
