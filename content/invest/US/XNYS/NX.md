---
title: "QUANEX BUILDING PRODUCTS CORPORATIO (NX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>QUANEX BUILDING PRODUCTS CORPORATIO</td></tr>
    <tr><td>Symbol</td><td>NX</td></tr>
    <tr><td>Web</td><td><a href="https://www.quanex.com">www.quanex.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.08 |
| 2020 | 0.32 |
| 2019 | 0.32 |
| 2018 | 0.24 |
| 2017 | 0.16 |
| 2016 | 0.16 |
| 2015 | 0.16 |
| 2014 | 0.16 |
| 2013 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
