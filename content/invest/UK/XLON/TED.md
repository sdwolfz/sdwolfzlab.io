---
title: "Ted Baker (TED)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Ted Baker</td></tr>
    <tr><td>Symbol</td><td>TED</td></tr>
    <tr><td>Web</td><td><a href="https://www.tedbaker.co.uk">www.tedbaker.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 7.8 |
| 2019 | 58.6 |
| 2018 | 60.1 |
| 2017 | 53.6 |
| 2016 | 47.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
