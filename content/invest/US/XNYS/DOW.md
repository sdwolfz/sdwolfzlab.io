---
title: "DOW INC (DOW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DOW INC</td></tr>
    <tr><td>Symbol</td><td>DOW</td></tr>
    <tr><td>Web</td><td><a href="https://www.dow.com">www.dow.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.4 |
| 2020 | 2.8 |
| 2019 | 2.1 |
| 2017 | 1.38 |
| 2016 | 1.84 |
| 2015 | 1.72 |
| 2014 | 1.53 |
| 2013 | 0.96 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
