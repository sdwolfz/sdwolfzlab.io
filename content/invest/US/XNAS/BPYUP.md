---
title: " (BPYUP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BPYUP</td></tr>
    <tr><td>Web</td><td><a href="https://www.bpy.brookfield.com">www.bpy.brookfield.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.398 |
| 2020 | 1.594 |
| 2019 | 1.594 |
| 2018 | 0.797 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
