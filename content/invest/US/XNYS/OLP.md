---
title: " (OLP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>OLP</td></tr>
    <tr><td>Web</td><td><a href="https://www.1liberty.com">www.1liberty.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.45 |
| 2020 | 1.8 |
| 2019 | 1.8 |
| 2018 | 1.8 |
| 2017 | 1.74 |
| 2016 | 1.66 |
| 2015 | 1.58 |
| 2014 | 1.89 |
| 2013 | 1.07 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
