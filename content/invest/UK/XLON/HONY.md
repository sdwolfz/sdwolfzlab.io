---
title: "Honeycomb Inv. (HONY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Honeycomb Inv.</td></tr>
    <tr><td>Symbol</td><td>HONY</td></tr>
    <tr><td>Web</td><td><a href="https://www.honeycombplc.com">www.honeycombplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 80.0 |
| 2018 | 80.0 |
| 2017 | 84.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
