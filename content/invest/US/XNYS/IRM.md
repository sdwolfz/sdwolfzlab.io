---
title: " (IRM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>IRM</td></tr>
    <tr><td>Web</td><td><a href="https://www.ironmountain.com">www.ironmountain.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.236 |
| 2020 | 2.472 |
| 2019 | 2.451 |
| 2018 | 2.372 |
| 2017 | 2.237 |
| 2016 | 2.005 |
| 2015 | 1.91 |
| 2014 | 1.745 |
| 2013 | 0.81 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
