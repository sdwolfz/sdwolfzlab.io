---
title: "PREMIER OIL PLC ORD 12.5P (PMO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>PREMIER OIL PLC ORD 12.5P</td></tr>
    <tr><td>Symbol</td><td>PMO</td></tr>
    <tr><td>Web</td><td><a href="http://www.premier-oil.com">www.premier-oil.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
