---
title: "WESTAMERICA BANCORP (WABC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>WESTAMERICA BANCORP</td></tr>
    <tr><td>Symbol</td><td>WABC</td></tr>
    <tr><td>Web</td><td><a href="https://www.westamerica.com">www.westamerica.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.82 |
| 2020 | 1.64 |
| 2019 | 1.63 |
| 2018 | 1.6 |
| 2017 | 1.57 |
| 2016 | 1.56 |
| 2015 | 1.53 |
| 2014 | 1.52 |
| 2013 | 1.49 |
| 2012 | 1.48 |
| 2011 | 1.45 |
| 2010 | 1.44 |
| 2009 | 1.41 |
| 2008 | 1.39 |
| 2007 | 1.36 |
| 2006 | 1.3 |
| 2005 | 1.22 |
| 2004 | 1.1 |
| 2003 | 1.0 |
| 2002 | 0.9 |
| 2001 | 0.82 |
| 2000 | 0.74 |
| 1999 | 0.66 |
| 1998 | 0.76 |
| 1997 | 1.08 |
| 1996 | 0.89 |
| 1995 | 0.74 |
| 1994 | 0.32 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
