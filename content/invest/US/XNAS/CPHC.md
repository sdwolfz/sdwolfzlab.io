---
title: "CANTERBURY PARK HLDGS CORP (CPHC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CANTERBURY PARK HLDGS CORP</td></tr>
    <tr><td>Symbol</td><td>CPHC</td></tr>
    <tr><td>Web</td><td><a href="https://www.canterburypark.com">www.canterburypark.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.28 |
| 2018 | 0.28 |
| 2017 | 0.23 |
| 2016 | 0.35 |
| 2015 | 0.25 |
| 2012 | 0.5 |
| 2008 | 0.25 |
| 2007 | 0.25 |
| 2006 | 0.25 |
| 2005 | 0.25 |
| 2004 | 0.25 |
| 2003 | 0.15 |
| 2002 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
