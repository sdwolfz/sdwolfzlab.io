---
title: "BEL FUSE INC (BELFB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BEL FUSE INC</td></tr>
    <tr><td>Symbol</td><td>BELFB</td></tr>
    <tr><td>Web</td><td><a href="https://www.belfuse.com">www.belfuse.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.21 |
| 2020 | 0.28 |
| 2019 | 0.28 |
| 2018 | 0.28 |
| 2017 | 0.28 |
| 2016 | 0.28 |
| 2015 | 0.28 |
| 2014 | 0.28 |
| 2013 | 0.28 |
| 2012 | 0.28 |
| 2011 | 0.28 |
| 2010 | 0.28 |
| 2009 | 0.28 |
| 2008 | 0.28 |
| 2007 | 0.22 |
| 2006 | 0.2 |
| 2005 | 0.2 |
| 2004 | 0.2 |
| 2003 | 0.2 |
| 2002 | 0.2 |
| 2001 | 0.2 |
| 2000 | 0.2 |
| 1999 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
