---
title: "VALARIS LTD (VAL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>VALARIS LTD</td></tr>
    <tr><td>Symbol</td><td>VAL</td></tr>
    <tr><td>Web</td><td><a href="https://www.valaris.com">www.valaris.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.04 |
| 2017 | 0.74 |
| 2016 | 1.36 |
| 2015 | 1.23 |
| 2014 | 1.08 |
| 2013 | 0.72 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
