---
title: "OCEAN BIO CHEM INC (OBCI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>OCEAN BIO CHEM INC</td></tr>
    <tr><td>Symbol</td><td>OBCI</td></tr>
    <tr><td>Web</td><td><a href="https://www.oceanbiochem.com">www.oceanbiochem.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.03 |
| 2020 | 0.08 |
| 2019 | 0.05 |
| 2018 | 0.06 |
| 2017 | 0.06 |
| 2016 | 0.06 |
| 2014 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
