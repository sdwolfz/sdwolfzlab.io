---
title: "FG NEW AMERICA ACQUISITION CORP (FGNA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FG NEW AMERICA ACQUISITION CORP</td></tr>
    <tr><td>Symbol</td><td>FGNA</td></tr>
    <tr><td>Web</td><td><a href="https://www.fgnewamerica.com">www.fgnewamerica.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
