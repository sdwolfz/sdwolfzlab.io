---
title: "AMGEN INC (AMGN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AMGEN INC</td></tr>
    <tr><td>Symbol</td><td>AMGN</td></tr>
    <tr><td>Web</td><td><a href="https://www.amgen.com">www.amgen.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 3.52 |
| 2020 | 6.4 |
| 2019 | 5.8 |
| 2018 | 5.28 |
| 2017 | 4.6 |
| 2016 | 4.0 |
| 2015 | 3.16 |
| 2014 | 2.44 |
| 2013 | 1.88 |
| 2012 | 1.44 |
| 2011 | 0.56 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
