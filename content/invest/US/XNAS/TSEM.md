---
title: "TOWER SEMICONDUCTOR LTD (TSEM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TOWER SEMICONDUCTOR LTD</td></tr>
    <tr><td>Symbol</td><td>TSEM</td></tr>
    <tr><td>Web</td><td><a href="https://www.towersemi.com">www.towersemi.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 1997 | 1.0 |
| 1996 | 1.275 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
