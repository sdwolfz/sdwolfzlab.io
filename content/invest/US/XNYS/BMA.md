---
title: " (BMA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BMA</td></tr>
    <tr><td>Web</td><td><a href="https://www.macro.com.ar">www.macro.com.ar</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 2.182 |
| 2018 | 2.235 |
| 2017 | 0.002 |
| 2016 | 0.983 |
| 2014 | 1.128 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
