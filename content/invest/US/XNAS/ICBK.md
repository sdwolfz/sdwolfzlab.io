---
title: "COUNTY BANCORP (ICBK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COUNTY BANCORP</td></tr>
    <tr><td>Symbol</td><td>ICBK</td></tr>
    <tr><td>Web</td><td><a href="https://www.investorscommunitybank.com">www.investorscommunitybank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.31 |
| 2019 | 0.2 |
| 2018 | 0.28 |
| 2017 | 0.24 |
| 2016 | 0.2 |
| 2015 | 0.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
