---
title: "BARNES GROUP INC (B)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BARNES GROUP INC</td></tr>
    <tr><td>Symbol</td><td>B</td></tr>
    <tr><td>Web</td><td><a href="https://www.barnesgroupinc.com">www.barnesgroupinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.16 |
| 2020 | 0.64 |
| 2019 | 0.64 |
| 2018 | 0.62 |
| 2017 | 0.55 |
| 2016 | 0.51 |
| 2015 | 0.48 |
| 2014 | 0.45 |
| 2013 | 0.22 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
