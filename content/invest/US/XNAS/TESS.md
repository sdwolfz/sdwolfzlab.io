---
title: "TESSCO TECHOLOGIES INC (TESS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TESSCO TECHOLOGIES INC</td></tr>
    <tr><td>Symbol</td><td>TESS</td></tr>
    <tr><td>Web</td><td><a href="https://www.tessco.com">www.tessco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.02 |
| 2019 | 0.8 |
| 2018 | 0.8 |
| 2017 | 0.8 |
| 2016 | 0.8 |
| 2015 | 0.8 |
| 2014 | 0.8 |
| 2013 | 0.72 |
| 2012 | 1.44 |
| 2011 | 0.5 |
| 2010 | 0.45 |
| 2009 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
