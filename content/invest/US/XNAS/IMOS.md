---
title: " (IMOS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>IMOS</td></tr>
    <tr><td>Web</td><td><a href="https://www.chipmos.com">www.chipmos.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.227 |
| 2019 | 0.764 |
| 2018 | 1.162 |
| 2017 | 0.655 |
| 2016 | 1.309 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
