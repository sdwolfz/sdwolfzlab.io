---
title: "ENTEGRIS INC (ENTG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ENTEGRIS INC</td></tr>
    <tr><td>Symbol</td><td>ENTG</td></tr>
    <tr><td>Web</td><td><a href="https://www.entegris.com">www.entegris.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.16 |
| 2020 | 0.32 |
| 2019 | 0.3 |
| 2018 | 0.28 |
| 2017 | 0.07 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
