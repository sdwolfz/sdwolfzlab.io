---
title: "EASTERN CO (EML)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>EASTERN CO</td></tr>
    <tr><td>Symbol</td><td>EML</td></tr>
    <tr><td>Web</td><td><a href="https://www.easterncompany.com">www.easterncompany.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.22 |
| 2020 | 0.44 |
| 2019 | 0.44 |
| 2018 | 0.44 |
| 2017 | 0.44 |
| 2016 | 0.44 |
| 2015 | 0.45 |
| 2014 | 0.48 |
| 2013 | 0.42 |
| 2012 | 0.5 |
| 2011 | 0.36 |
| 2010 | 0.52 |
| 2009 | 0.36 |
| 2008 | 0.33 |
| 2007 | 0.32 |
| 2006 | 0.43 |
| 2005 | 0.44 |
| 2004 | 0.44 |
| 2003 | 0.44 |
| 2002 | 0.44 |
| 2001 | 0.44 |
| 2000 | 0.44 |
| 1999 | 0.11 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
