---
title: "BROOKFIELD ASSET MANAGEMENT INC (BAM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BROOKFIELD ASSET MANAGEMENT INC</td></tr>
    <tr><td>Symbol</td><td>BAM</td></tr>
    <tr><td>Web</td><td><a href="https://www.brookfield.com">www.brookfield.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.48 |
| 2019 | 0.64 |
| 2018 | 0.6 |
| 2017 | 0.56 |
| 2016 | 0.52 |
| 2015 | 0.59 |
| 2014 | 0.68 |
| 2013 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
