---
title: "ROBERT HALF INTERNATIONAL INC (RHI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ROBERT HALF INTERNATIONAL INC</td></tr>
    <tr><td>Symbol</td><td>RHI</td></tr>
    <tr><td>Web</td><td><a href="https://www.roberthalf.com">www.roberthalf.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.76 |
| 2020 | 1.36 |
| 2019 | 1.24 |
| 2018 | 1.12 |
| 2017 | 0.96 |
| 2016 | 0.88 |
| 2015 | 0.8 |
| 2014 | 0.72 |
| 2013 | 0.32 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
