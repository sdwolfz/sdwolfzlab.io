---
title: "WEBSTER FINANCIAL CORP (WBS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>WEBSTER FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>WBS</td></tr>
    <tr><td>Web</td><td><a href="https://www.websterbank.com">www.websterbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.8 |
| 2020 | 1.6 |
| 2019 | 1.53 |
| 2018 | 1.25 |
| 2017 | 1.03 |
| 2016 | 0.98 |
| 2015 | 0.89 |
| 2014 | 0.75 |
| 2013 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
