---
title: "ROGERS COMMUNICATIONS INC (RCI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ROGERS COMMUNICATIONS INC</td></tr>
    <tr><td>Symbol</td><td>RCI</td></tr>
    <tr><td>Web</td><td><a href="https://www.rogers.com">www.rogers.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.504 |
| 2019 | 1.501 |
| 2018 | 1.488 |
| 2017 | 1.485 |
| 2016 | 1.444 |
| 2015 | 1.799 |
| 2014 | 1.832 |
| 2013 | 0.87 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
