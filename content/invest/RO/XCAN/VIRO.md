---
title: "VIROLA - INDEPENDENTA SA SIBIU (VIRO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>VIROLA - INDEPENDENTA SA SIBIU</td></tr>
    <tr><td>Symbol</td><td>VIRO</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 52.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
