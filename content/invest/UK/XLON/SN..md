---
title: "Smith & Nephew (SN.)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Smith & Nephew</td></tr>
    <tr><td>Symbol</td><td>SN.</td></tr>
    <tr><td>Web</td><td><a href="https://www.smith-nephew.com">www.smith-nephew.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 37.5 |
| 2019 | 37.5 |
| 2018 | 36.0 |
| 2017 | 35.0 |
| 2016 | 30.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
