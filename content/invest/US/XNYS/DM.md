---
title: "DESKTOP METAL INC (DM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DESKTOP METAL INC</td></tr>
    <tr><td>Symbol</td><td>DM</td></tr>
    <tr><td>Web</td><td><a href="https://www.desktopmetal.com">www.desktopmetal.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.369 |
| 2018 | 1.372 |
| 2017 | 1.126 |
| 2016 | 0.921 |
| 2015 | 0.702 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
