---
title: "NEWS CORP NEW (NWS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NEWS CORP NEW</td></tr>
    <tr><td>Symbol</td><td>NWS</td></tr>
    <tr><td>Web</td><td><a href="https://www.newscorp.com">www.newscorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.2 |
| 2019 | 0.2 |
| 2018 | 0.2 |
| 2017 | 0.2 |
| 2016 | 0.2 |
| 2015 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
