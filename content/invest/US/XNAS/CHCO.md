---
title: "CITY HLDG CO (CHCO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CITY HLDG CO</td></tr>
    <tr><td>Symbol</td><td>CHCO</td></tr>
    <tr><td>Web</td><td><a href="https://www.bankatcity.com">www.bankatcity.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.16 |
| 2020 | 2.28 |
| 2019 | 2.16 |
| 2018 | 1.91 |
| 2017 | 1.75 |
| 2016 | 1.71 |
| 2015 | 1.66 |
| 2014 | 1.57 |
| 2013 | 1.46 |
| 2012 | 1.4 |
| 2011 | 1.36 |
| 2010 | 1.36 |
| 2009 | 1.36 |
| 2008 | 1.33 |
| 2007 | 1.21 |
| 2006 | 1.09 |
| 2005 | 0.97 |
| 2004 | 0.86 |
| 2003 | 0.75 |
| 2002 | 0.3 |
| 2000 | 0.44 |
| 1999 | 0.8 |
| 1998 | 0.77 |
| 1997 | 0.73 |
| 1996 | 0.68 |
| 1995 | 0.66 |
| 1994 | 0.65 |
| 1993 | 0.62 |
| 1992 | 0.57 |
| 1991 | 0.51 |
| 1990 | 0.43 |
| 1989 | 0.4 |
| 1988 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
