---
title: "INTERNATIONAL BANCSHARES CORP (IBOC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>INTERNATIONAL BANCSHARES CORP</td></tr>
    <tr><td>Symbol</td><td>IBOC</td></tr>
    <tr><td>Web</td><td><a href="https://www.ibc.com">www.ibc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.55 |
| 2020 | 1.1 |
| 2019 | 1.05 |
| 2018 | 0.75 |
| 2017 | 0.66 |
| 2016 | 0.6 |
| 2015 | 0.58 |
| 2014 | 0.52 |
| 2013 | 0.43 |
| 2012 | 0.4 |
| 2011 | 0.38 |
| 2010 | 0.36 |
| 2009 | 0.34 |
| 2008 | 0.66 |
| 2007 | 0.68 |
| 2006 | 0.7 |
| 2005 | 0.72 |
| 2004 | 0.9 |
| 2003 | 0.93 |
| 2002 | 0.77 |
| 2001 | 0.9 |
| 2000 | 1.1 |
| 1999 | 1.1 |
| 1998 | 0.9 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
