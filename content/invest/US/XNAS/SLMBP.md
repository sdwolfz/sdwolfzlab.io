---
title: " (SLMBP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SLMBP</td></tr>
    <tr><td>Web</td><td><a href="https://www.salliemae.com">www.salliemae.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.963 |
| 2020 | 2.559 |
| 2019 | 4.234 |
| 2018 | 3.882 |
| 2017 | 2.91 |
| 2016 | 2.406 |
| 2015 | 2.011 |
| 2014 | 1.958 |
| 2013 | 2.0 |
| 2012 | 2.219 |
| 2011 | 1.523 |
| 2010 | 1.05 |
| 2009 | 1.099 |
| 2008 | 4.086 |
| 2007 | 6.248 |
| 2006 | 5.816 |
| 2005 | 2.276 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
