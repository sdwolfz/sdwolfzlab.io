---
title: "DEL TACO RESTAURANTS INC NEW (TACO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>DEL TACO RESTAURANTS INC NEW</td></tr>
    <tr><td>Symbol</td><td>TACO</td></tr>
    <tr><td>Web</td><td><a href="https://www.deltaco.com">www.deltaco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
