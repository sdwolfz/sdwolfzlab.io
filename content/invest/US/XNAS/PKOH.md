---
title: "PARK-OHIO HOLDINGS CORP (PKOH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PARK-OHIO HOLDINGS CORP</td></tr>
    <tr><td>Symbol</td><td>PKOH</td></tr>
    <tr><td>Web</td><td><a href="https://www.pkoh.com">www.pkoh.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 0.25 |
| 2019 | 0.5 |
| 2018 | 0.5 |
| 2017 | 0.5 |
| 2016 | 0.5 |
| 2015 | 0.5 |
| 2014 | 0.375 |
| 1989 | 0.19 |
| 1988 | 0.19 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
