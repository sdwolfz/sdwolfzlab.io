---
title: "CIVEO CORP CDA (CVEO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CIVEO CORP CDA</td></tr>
    <tr><td>Symbol</td><td>CVEO</td></tr>
    <tr><td>Web</td><td><a href="https://www.civeo.com">www.civeo.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2014 | 0.26 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
