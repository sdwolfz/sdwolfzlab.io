---
title: "LOGITECH INTERNATIONAL SA (LOGI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LOGITECH INTERNATIONAL SA</td></tr>
    <tr><td>Symbol</td><td>LOGI</td></tr>
    <tr><td>Web</td><td><a href="https://www.logitech.com">www.logitech.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.869 |
| 2019 | 0.744 |
| 2018 | 0.688 |
| 2017 | 0.634 |
| 2016 | 0.568 |
| 2015 | 0.53 |
| 2014 | 0.267 |
| 2013 | 0.23 |
| 2012 | 0.84 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
