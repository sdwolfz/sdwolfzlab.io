---
title: "WATERSTONE FINANCIAL INC MD (WSBF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>WATERSTONE FINANCIAL INC MD</td></tr>
    <tr><td>Symbol</td><td>WSBF</td></tr>
    <tr><td>Web</td><td><a href="https://www.wsbonline.com">www.wsbonline.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.4 |
| 2020 | 1.28 |
| 2019 | 0.98 |
| 2018 | 0.98 |
| 2017 | 0.98 |
| 2016 | 0.26 |
| 2015 | 0.2 |
| 2014 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
