---
title: "STELLUS CAPITAL INVESTMENT CORP (SCM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>STELLUS CAPITAL INVESTMENT CORP</td></tr>
    <tr><td>Symbol</td><td>SCM</td></tr>
    <tr><td>Web</td><td><a href="https://www.stelluscapital.com">www.stelluscapital.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.415 |
| 2020 | 1.149 |
| 2019 | 1.356 |
| 2018 | 1.356 |
| 2017 | 1.356 |
| 2016 | 1.356 |
| 2015 | 1.356 |
| 2014 | 1.356 |
| 2013 | 1.02 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
