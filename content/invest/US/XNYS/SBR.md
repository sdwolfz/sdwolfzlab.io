---
title: " (SBR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SBR</td></tr>
    <tr><td>Web</td><td><a href="https://www.sbr-sabine.com">www.sbr-sabine.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.771 |
| 2020 | 2.397 |
| 2019 | 3.02 |
| 2018 | 3.349 |
| 2017 | 2.368 |
| 2016 | 1.832 |
| 2015 | 3.107 |
| 2014 | 4.098 |
| 2013 | 1.735 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
