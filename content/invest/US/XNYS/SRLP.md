---
title: " (SRLP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>SRLP</td></tr>
    <tr><td>Web</td><td><a href="https://www.spragueenergy.com">www.spragueenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.336 |
| 2020 | 2.672 |
| 2019 | 2.672 |
| 2018 | 2.625 |
| 2017 | 2.4 |
| 2016 | 2.161 |
| 2015 | 1.92 |
| 2014 | 1.564 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
