---
title: "BRAICONF SA BRAILA (BRCR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>BRAICONF SA BRAILA</td></tr>
    <tr><td>Symbol</td><td>BRCR</td></tr>
    <tr><td>Web</td><td><a href="https://www.braiconf.ro">www.braiconf.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 0.027 |
| 2016 | 0.0275 |
| 2015 | 0.045 |
| 2014 | 0.025 |
| 2013 | 0.02 |
| 2005 | 0.01 |
| 2004 | 0.27 |
| 2003 | 0.323 |
| 2002 | 0.247 |
| 2001 | 0.209 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
