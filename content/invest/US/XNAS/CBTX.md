---
title: "CBTX INC (CBTX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CBTX INC</td></tr>
    <tr><td>Symbol</td><td>CBTX</td></tr>
    <tr><td>Web</td><td><a href="https://www.communitybankoftx.com">www.communitybankoftx.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.13 |
| 2020 | 0.4 |
| 2019 | 0.4 |
| 2018 | 0.2 |
| 2017 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
