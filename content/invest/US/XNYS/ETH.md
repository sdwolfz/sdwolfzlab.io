---
title: "ETHAN ALLEN INTERIORS INC (ETH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ETHAN ALLEN INTERIORS INC</td></tr>
    <tr><td>Symbol</td><td>ETH</td></tr>
    <tr><td>Web</td><td><a href="https://www.ethanallen.com">www.ethanallen.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.5 |
| 2020 | 0.63 |
| 2019 | 1.78 |
| 2018 | 1.07 |
| 2017 | 0.76 |
| 2016 | 0.65 |
| 2015 | 0.52 |
| 2014 | 0.42 |
| 2013 | 0.19 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
