---
title: "Ten Ent Grp (TEG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Ten Ent Grp</td></tr>
    <tr><td>Symbol</td><td>TEG</td></tr>
    <tr><td>Web</td><td><a href="https://www.tenpin.co.uk">www.tenpin.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 3.7 |
| 2018 | 11.0 |
| 2017 | 10.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
