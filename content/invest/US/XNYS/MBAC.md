---
title: "M3 BRIGADE ACQUISITION II CORP (MBAC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>M3 BRIGADE ACQUISITION II CORP</td></tr>
    <tr><td>Symbol</td><td>MBAC</td></tr>
    <tr><td>Web</td><td><a href="https://www.m3-partners.com">www.m3-partners.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
