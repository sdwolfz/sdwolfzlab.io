---
title: " (FTAI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>FTAI</td></tr>
    <tr><td>Web</td><td><a href="https://www.ftandi.com">www.ftandi.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.66 |
| 2020 | 1.32 |
| 2019 | 1.32 |
| 2018 | 1.32 |
| 2017 | 1.32 |
| 2016 | 1.32 |
| 2015 | 0.48 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
