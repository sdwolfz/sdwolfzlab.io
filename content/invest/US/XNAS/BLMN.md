---
title: "BLOOMIN BRANDS INC (BLMN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BLOOMIN BRANDS INC</td></tr>
    <tr><td>Symbol</td><td>BLMN</td></tr>
    <tr><td>Web</td><td><a href="https://www.bloominbrands.com">www.bloominbrands.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.2 |
| 2019 | 0.4 |
| 2018 | 0.36 |
| 2017 | 0.32 |
| 2016 | 0.28 |
| 2015 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
