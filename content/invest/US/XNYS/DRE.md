---
title: " (DRE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>DRE</td></tr>
    <tr><td>Web</td><td><a href="https://www.dukerealty.com">www.dukerealty.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.51 |
| 2020 | 0.96 |
| 2019 | 0.88 |
| 2018 | 0.815 |
| 2017 | 1.42 |
| 2016 | 0.73 |
| 2015 | 0.89 |
| 2014 | 0.68 |
| 2013 | 0.34 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
