---
title: " (CLDT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CLDT</td></tr>
    <tr><td>Web</td><td><a href="https://www.chathamlodgingtrust.com">www.chathamlodgingtrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.22 |
| 2019 | 1.32 |
| 2018 | 1.32 |
| 2017 | 1.32 |
| 2016 | 1.3 |
| 2015 | 1.2 |
| 2014 | 0.93 |
| 2013 | 0.35 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
