---
title: "J & J SNACK FOODS CORP (JJSF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>J & J SNACK FOODS CORP</td></tr>
    <tr><td>Symbol</td><td>JJSF</td></tr>
    <tr><td>Web</td><td><a href="https://www.jjsnack.com">www.jjsnack.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.575 |
| 2020 | 2.3 |
| 2019 | 2.075 |
| 2018 | 1.85 |
| 2017 | 1.71 |
| 2016 | 1.59 |
| 2015 | 1.47 |
| 2014 | 1.32 |
| 2013 | 0.8 |
| 2012 | 0.55 |
| 2011 | 0.483 |
| 2010 | 0.44 |
| 2009 | 0.4 |
| 2008 | 0.375 |
| 2007 | 0.348 |
| 2006 | 0.31 |
| 2005 | 0.525 |
| 2004 | 0.125 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
