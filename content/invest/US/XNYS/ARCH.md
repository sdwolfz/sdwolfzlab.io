---
title: "ARCH RESOURCES INC (ARCH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ARCH RESOURCES INC</td></tr>
    <tr><td>Symbol</td><td>ARCH</td></tr>
    <tr><td>Web</td><td><a href="https://www.archrsc.com">www.archrsc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.5 |
| 2019 | 1.8 |
| 2018 | 1.6 |
| 2017 | 1.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
