---
title: "NAVIOS MARITIME HOLDINGS INC (NM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>NAVIOS MARITIME HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>NM</td></tr>
    <tr><td>Web</td><td><a href="https://www.navios.com">www.navios.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 0.18 |
| 2014 | 0.24 |
| 2013 | 0.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
