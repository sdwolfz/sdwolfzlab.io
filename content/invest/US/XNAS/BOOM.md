---
title: "DMC GLOBAL INC (BOOM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>DMC GLOBAL INC</td></tr>
    <tr><td>Symbol</td><td>BOOM</td></tr>
    <tr><td>Web</td><td><a href="https://www.dmcglobal.com">www.dmcglobal.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.125 |
| 2019 | 0.29 |
| 2018 | 0.08 |
| 2017 | 0.08 |
| 2016 | 0.08 |
| 2015 | 0.14 |
| 2014 | 0.16 |
| 2013 | 0.16 |
| 2012 | 0.16 |
| 2011 | 0.16 |
| 2010 | 0.16 |
| 2009 | 0.12 |
| 2008 | 0.15 |
| 2007 | 0.15 |
| 2006 | 0.15 |
| 2005 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
