---
title: "HUNTINGTON INGALLS INDUSTRIES (HII)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HUNTINGTON INGALLS INDUSTRIES</td></tr>
    <tr><td>Symbol</td><td>HII</td></tr>
    <tr><td>Web</td><td><a href="https://www.huntingtoningalls.com">www.huntingtoningalls.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 2.28 |
| 2020 | 4.23 |
| 2019 | 3.61 |
| 2018 | 3.02 |
| 2017 | 2.52 |
| 2016 | 2.1 |
| 2015 | 1.7 |
| 2014 | 1.0 |
| 2013 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
