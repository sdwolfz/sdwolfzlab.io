---
title: "NORWOOD FINANCIAL CORP (NWFL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NORWOOD FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>NWFL</td></tr>
    <tr><td>Web</td><td><a href="https://www.waynebank.com">www.waynebank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.52 |
| 2020 | 1.0 |
| 2019 | 0.96 |
| 2018 | 0.88 |
| 2017 | 1.18 |
| 2016 | 1.24 |
| 2015 | 1.23 |
| 2014 | 1.2 |
| 2013 | 1.2 |
| 2012 | 1.2 |
| 2011 | 1.16 |
| 2010 | 1.12 |
| 2009 | 1.08 |
| 2008 | 1.0 |
| 2007 | 0.92 |
| 2006 | 0.84 |
| 2005 | 0.72 |
| 2004 | 0.68 |
| 2003 | 0.8 |
| 2002 | 0.88 |
| 2001 | 0.8 |
| 2000 | 0.51 |
| 1999 | 0.42 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
