---
title: "SYNOVUS FINANCIAL CORP (SNV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SYNOVUS FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>SNV</td></tr>
    <tr><td>Web</td><td><a href="https://www.synovus.com">www.synovus.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.33 |
| 2020 | 1.32 |
| 2019 | 1.2 |
| 2018 | 1.0 |
| 2017 | 0.6 |
| 2016 | 0.48 |
| 2015 | 0.42 |
| 2014 | 0.31 |
| 2013 | 0.03 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
