---
title: "AES CORP (AES)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AES CORP</td></tr>
    <tr><td>Symbol</td><td>AES</td></tr>
    <tr><td>Web</td><td><a href="https://www.aes.com">www.aes.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 0.572 |
| 2019 | 0.548 |
| 2018 | 0.52 |
| 2017 | 0.48 |
| 2016 | 0.44 |
| 2015 | 0.4 |
| 2014 | 0.2 |
| 2013 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
