---
title: "INFORMATION SERVICES GROUP INC (III)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>INFORMATION SERVICES GROUP INC</td></tr>
    <tr><td>Symbol</td><td>III</td></tr>
    <tr><td>Web</td><td><a href="https://www.isg-one.com">www.isg-one.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 0.14 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
