---
title: "GLOBE LIFE INC (GL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GLOBE LIFE INC</td></tr>
    <tr><td>Symbol</td><td>GL</td></tr>
    <tr><td>Web</td><td><a href="https://www.globelifeinsurance.com">www.globelifeinsurance.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.584 |
| 2020 | 0.736 |
| 2019 | 0.344 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
