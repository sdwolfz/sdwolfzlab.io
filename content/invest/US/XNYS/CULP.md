---
title: "CULP INC (CULP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CULP INC</td></tr>
    <tr><td>Symbol</td><td>CULP</td></tr>
    <tr><td>Web</td><td><a href="https://www.culp.com">www.culp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.22 |
| 2020 | 0.42 |
| 2019 | 0.3 |
| 2018 | 0.37 |
| 2017 | 0.46 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
