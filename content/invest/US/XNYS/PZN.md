---
title: "PZENA INVESTMENT MANAGEMENT  INC (PZN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PZENA INVESTMENT MANAGEMENT  INC</td></tr>
    <tr><td>Symbol</td><td>PZN</td></tr>
    <tr><td>Web</td><td><a href="https://www.pzena.com">www.pzena.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 0.55 |
| 2019 | 0.58 |
| 2018 | 0.51 |
| 2017 | 0.37 |
| 2016 | 1.05 |
| 2015 | 0.122 |
| 2014 | 0.35 |
| 2013 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
