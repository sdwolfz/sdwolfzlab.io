---
title: "Smurfit Kappa (SKG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Smurfit Kappa</td></tr>
    <tr><td>Symbol</td><td>SKG</td></tr>
    <tr><td>Web</td><td><a href="https://www.smurfitkappa.com">www.smurfitkappa.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 80.9 |
| 2018 | 97.6 |
| 2017 | 87.6 |
| 2016 | 79.6 |
| 2015 | 68.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
