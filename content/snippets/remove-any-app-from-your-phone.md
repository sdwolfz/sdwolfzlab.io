---
title: "Remove any app from your phone"
date: 2020-11-27T14:16:37Z
type: posts
slug: remove-any-app-from-your-phone
tags: ["android", "linux"]
---

Every app can be uninstalled, you just need to do it via CLI.

1. [Enable 'Developer options' on your phone](/snippets/enable-developer-options-on-your-phone/)
2. Install `adb` via the `android-tools` package:

```sh
sudo pacman -S android-tools
```

3. Plug in the USB cable between phone and computer, you will have to accept the
debugging authorization on your phone.

4. Start the shell:

```sh
adb shell
```

5. List packages:

```sh
pm list packages
```

6. Copy the list and filter it to only contain the packages you want to remove.

7. You can remove a package by adding it's ID to the end of this command:

```sh
pm uninstall --user 0 -k
```

8. Save a list of all the commands you've executed, you will need to re-run it
   after every phone update.

## Find package ID

Some apps have weird ids, and it's hard to find this information. There are a
few things you can do:

1. Search for the app in the `Play Store`, press `Share`, you will see the ID in the URL.
2. Search for the app on your phone, find it's version, then search for that in
   the output of the `dumpsys` command in the `adb shell`:

```sh
dumpsys 2&>/dev/null | grep -A10 -B10
```

## DO NOT RUN THIS!

This is a list of all the rubbish I've removed from my phone. **DO NOT RUN IT!**
It's not intended for your use... It's for me to re-run after every update. If
you run this without first installing replacement apps, you will suffer!

<details>
  <summary>Click to toggle!</summary>

```sh
pm uninstall --user 0 -k com.android.bookmarkprovider
pm uninstall --user 0 -k com.android.chrome
pm uninstall --user 0 -k com.diotek.sec.lookup.dictionary
pm uninstall --user 0 -k com.dsi.ant.plugins.antplus
pm uninstall --user 0 -k com.dsi.ant.sample.acquirechannels
pm uninstall --user 0 -k com.dsi.ant.server
pm uninstall --user 0 -k com.dsi.ant.service.socket
pm uninstall --user 0 -k com.google.android.apps.photos
pm uninstall --user 0 -k com.google.android.apps.tachyon
pm uninstall --user 0 -k com.google.android.as
pm uninstall --user 0 -k com.google.android.gm
pm uninstall --user 0 -k com.google.android.googlequicksearchbox
pm uninstall --user 0 -k com.google.android.music
pm uninstall --user 0 -k com.google.android.projection.gearhead
pm uninstall --user 0 -k com.google.android.tts
pm uninstall --user 0 -k com.google.android.videos
pm uninstall --user 0 -k com.google.ar.core
pm uninstall --user 0 -k com.microsoft.appmanager
pm uninstall --user 0 -k com.osp.app.signin
pm uninstall --user 0 -k com.samsung.android.app.notes
pm uninstall --user 0 -k com.samsung.android.app.tips
pm uninstall --user 0 -k com.samsung.android.ardrawing
pm uninstall --user 0 -k com.samsung.android.aremoji
pm uninstall --user 0 -k com.samsung.android.arzone
pm uninstall --user 0 -k com.samsung.android.authfw
pm uninstall --user 0 -k com.samsung.android.dynamiclock
pm uninstall --user 0 -k com.samsung.android.forest
pm uninstall --user 0 -k com.samsung.android.hmt.vrsvc
pm uninstall --user 0 -k com.samsung.android.honeyboard
pm uninstall --user 0 -k com.samsung.android.mdx
pm uninstall --user 0 -k com.samsung.android.mdx.kit
pm uninstall --user 0 -k com.samsung.android.mobileservice
pm uninstall --user 0 -k com.samsung.android.samsungpositioning
pm uninstall --user 0 -k com.samsung.android.service.peoplestripe
pm uninstall --user 0 -k com.samsung.android.svoiceime
pm uninstall --user 0 -k com.samsung.android.themecenter
pm uninstall --user 0 -k com.samsung.android.themestore
pm uninstall --user 0 -k com.samsung.android.visionarapps
pm uninstall --user 0 -k com.sec.android.app.desktoplauncher
pm uninstall --user 0 -k com.sec.android.app.dexonpc
pm uninstall --user 0 -k com.sec.android.app.myfiles
pm uninstall --user 0 -k com.sec.android.daemonapp
pm uninstall --user 0 -k com.sec.android.desktopmode.uiservice
pm uninstall --user 0 -k com.sec.android.gallery3d
pm uninstall --user 0 -k com.sec.android.inputmethod
pm uninstall --user 0 -k com.sec.spp.push
```
</details>
