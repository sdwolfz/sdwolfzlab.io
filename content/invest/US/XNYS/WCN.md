---
title: "WASTE CONNECTIONS INC (CA) (WCN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>WASTE CONNECTIONS INC (CA)</td></tr>
    <tr><td>Symbol</td><td>WCN</td></tr>
    <tr><td>Web</td><td><a href="https://www.wasteconnections.com">www.wasteconnections.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.709 |
| 2019 | 0.665 |
| 2018 | 0.58 |
| 2017 | 0.56 |
| 2016 | 0.909 |
| 2015 | 0.535 |
| 2014 | 0.475 |
| 2013 | 0.215 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
