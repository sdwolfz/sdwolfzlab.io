---
title: "Scot.amer.inv. (SAIN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Scot.amer.inv.</td></tr>
    <tr><td>Symbol</td><td>SAIN</td></tr>
    <tr><td>Web</td><td><a href="https://www.bailliegifford.com">www.bailliegifford.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
