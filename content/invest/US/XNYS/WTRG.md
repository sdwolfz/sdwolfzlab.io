---
title: "ESSENTIAL UTILITIES INC (WTRG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ESSENTIAL UTILITIES INC</td></tr>
    <tr><td>Symbol</td><td>WTRG</td></tr>
    <tr><td>Web</td><td><a href="https://www.essential.co">www.essential.co</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.502 |
| 2020 | 0.97 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
