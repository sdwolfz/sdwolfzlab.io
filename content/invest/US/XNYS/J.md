---
title: "JACOBS ENGINEERING GROUP INC (J)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>JACOBS ENGINEERING GROUP INC</td></tr>
    <tr><td>Symbol</td><td>J</td></tr>
    <tr><td>Web</td><td><a href="https://www.jacobs.com">www.jacobs.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.42 |
| 2020 | 0.76 |
| 2019 | 0.17 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
