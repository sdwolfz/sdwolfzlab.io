---
title: " (AAT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>AAT</td></tr>
    <tr><td>Web</td><td><a href="https://www.americanassetstrust.com">www.americanassetstrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.56 |
| 2020 | 1.0 |
| 2019 | 1.14 |
| 2018 | 1.09 |
| 2017 | 1.05 |
| 2016 | 1.01 |
| 2015 | 0.949 |
| 2014 | 0.893 |
| 2013 | 0.43 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
