---
title: "BRIDGELINE DIGITAL INC (BLIN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BRIDGELINE DIGITAL INC</td></tr>
    <tr><td>Symbol</td><td>BLIN</td></tr>
    <tr><td>Web</td><td><a href="https://www.bridgeline.com">www.bridgeline.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
