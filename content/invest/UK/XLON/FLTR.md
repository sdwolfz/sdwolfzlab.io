---
title: "Flutter Ent (FLTR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Flutter Ent</td></tr>
    <tr><td>Symbol</td><td>FLTR</td></tr>
    <tr><td>Web</td><td><a href="https://www.flutter.com">www.flutter.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 196.7 |
| 2018 | 196.7 |
| 2016 | 162.28 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
