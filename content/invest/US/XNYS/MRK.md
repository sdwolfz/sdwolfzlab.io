---
title: "MERCK & CO INC (MRK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MERCK & CO INC</td></tr>
    <tr><td>Symbol</td><td>MRK</td></tr>
    <tr><td>Web</td><td><a href="https://www.merck.com">www.merck.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.65 |
| 2020 | 2.48 |
| 2019 | 2.26 |
| 2018 | 1.99 |
| 2017 | 1.89 |
| 2016 | 1.85 |
| 2015 | 1.81 |
| 2014 | 1.77 |
| 2013 | 0.87 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
