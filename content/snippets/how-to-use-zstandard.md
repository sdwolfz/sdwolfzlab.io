---
title: "How to Use: zstandard"
date: 2020-12-22T09:44:34Z
type: posts
slug: how-to-use-zstandard
series: ["How to Use"]
tags: ["zstandard", "linux", "archive"]
---

## Install

```sh
sudo pacman -S zstd
```

## Prerequisites

1. Create a playground:

```sh
mkdir archive-test
cd archive-test
```

2. Create some content:

```sh
mkdir content
echo content1 >> content/file1
echo content2 >> content/file2
echo content3 >> content/file3
```

3. Back up your content:

```sh
tar -cf archive1.tar content
```

## Compress

Create an archive, default compression `-3`:

```sh
zstd archive1.tar
```

Or specity a compression level:

```sh
zstd -9 archive1.tar
```

## Extract

```sh
unzstd archive1.tar.zst
```

## Cleanup

Don't forget to remove your test files after playing with this:

```sh
cd ..
rm -rf archive-test
```
