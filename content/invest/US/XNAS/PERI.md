---
title: "PERION NETWORK LTD (PERI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PERION NETWORK LTD</td></tr>
    <tr><td>Symbol</td><td>PERI</td></tr>
    <tr><td>Web</td><td><a href="https://www.perion.com">www.perion.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2011 | 0.4 |
| 2010 | 0.88 |
| 2009 | 0.9 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
