---
title: "CTS CORP (CTS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CTS CORP</td></tr>
    <tr><td>Symbol</td><td>CTS</td></tr>
    <tr><td>Web</td><td><a href="https://www.ctscorp.com">www.ctscorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.04 |
| 2020 | 0.16 |
| 2019 | 0.16 |
| 2018 | 0.16 |
| 2017 | 0.16 |
| 2016 | 0.16 |
| 2015 | 0.16 |
| 2014 | 0.16 |
| 2013 | 0.11 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
