---
title: "PATRIA BANK S.A. (PBK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>PATRIA BANK S.A.</td></tr>
    <tr><td>Symbol</td><td>PBK</td></tr>
    <tr><td>Web</td><td><a href="https://www.patriabank.ro">www.patriabank.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
