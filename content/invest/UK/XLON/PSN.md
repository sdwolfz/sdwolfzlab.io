---
title: "Persimmon (PSN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Persimmon</td></tr>
    <tr><td>Symbol</td><td>PSN</td></tr>
    <tr><td>Web</td><td><a href="https://www.corporate.persimmonhomes.com">www.corporate.persimmonhomes.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 235.0 |
| 2018 | 235.0 |
| 2017 | 235.0 |
| 2016 | 135.0 |
| 2015 | 110.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
