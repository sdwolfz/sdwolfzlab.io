---
title: "Ind.inv.tst (IIT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Ind.inv.tst</td></tr>
    <tr><td>Symbol</td><td>IIT</td></tr>
    <tr><td>Web</td><td><a href="https://www.independentinvestmenttrust.co.uk">www.independentinvestmenttrust.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 8.0 |
| 2019 | 13.0 |
| 2018 | 10.0 |
| 2017 | 8.0 |
| 2016 | 7.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
