---
title: "LIVEXLIVE MEDIA INC (LIVX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LIVEXLIVE MEDIA INC</td></tr>
    <tr><td>Symbol</td><td>LIVX</td></tr>
    <tr><td>Web</td><td><a href="https://www.livexlive.com">www.livexlive.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
