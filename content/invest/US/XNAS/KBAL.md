---
title: "KIMBALL INTERNATIONAL INC (KBAL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>KIMBALL INTERNATIONAL INC</td></tr>
    <tr><td>Symbol</td><td>KBAL</td></tr>
    <tr><td>Web</td><td><a href="https://www.kimballinternational.com">www.kimballinternational.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.18 |
| 2020 | 0.36 |
| 2019 | 0.34 |
| 2018 | 0.3 |
| 2017 | 0.26 |
| 2016 | 0.23 |
| 2015 | 0.21 |
| 2014 | 0.2 |
| 2013 | 0.2 |
| 2012 | 0.2 |
| 2011 | 0.2 |
| 2010 | 0.2 |
| 2009 | 0.2 |
| 2008 | 0.64 |
| 2007 | 0.64 |
| 2006 | 0.64 |
| 2005 | 0.64 |
| 2004 | 0.64 |
| 2003 | 0.64 |
| 2002 | 0.64 |
| 2001 | 0.64 |
| 2000 | 0.64 |
| 1999 | 0.64 |
| 1998 | 0.63 |
| 1997 | 0.99 |
| 1996 | 1.01 |
| 1995 | 0.9 |
| 1994 | 0.84 |
| 1993 | 0.82 |
| 1992 | 0.74 |
| 1991 | 0.68 |
| 1990 | 0.62 |
| 1989 | 0.54 |
| 1988 | 0.13 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
