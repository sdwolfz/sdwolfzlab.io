---
title: "OXBRIDGE RE HOLDINGS (OXBR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>OXBRIDGE RE HOLDINGS</td></tr>
    <tr><td>Symbol</td><td>OXBR</td></tr>
    <tr><td>Web</td><td><a href="https://www.oxbridgere.com">www.oxbridgere.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 0.36 |
| 2016 | 0.48 |
| 2015 | 0.48 |
| 2014 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
