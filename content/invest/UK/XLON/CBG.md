---
title: "Close Bros (CBG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Close Bros</td></tr>
    <tr><td>Symbol</td><td>CBG</td></tr>
    <tr><td>Web</td><td><a href="https://www.closebrothers.co.uk">www.closebrothers.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 40.0 |
| 2019 | 66.0 |
| 2018 | 63.0 |
| 2017 | 60.0 |
| 2016 | 57.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
