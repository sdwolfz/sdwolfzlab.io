---
title: "SOUTHERN COPPER CORPORATION (SCCO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SOUTHERN COPPER CORPORATION</td></tr>
    <tr><td>Symbol</td><td>SCCO</td></tr>
    <tr><td>Web</td><td><a href="https://www.southerncoppercorp.com">www.southerncoppercorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.3 |
| 2020 | 1.5 |
| 2019 | 1.6 |
| 2018 | 1.4 |
| 2017 | 0.59 |
| 2016 | 0.18 |
| 2015 | 0.34 |
| 2014 | 0.46 |
| 2013 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
