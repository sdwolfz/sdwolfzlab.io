---
title: "DOMINOS PIZZA INC (DPZ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DOMINOS PIZZA INC</td></tr>
    <tr><td>Symbol</td><td>DPZ</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.88 |
| 2020 | 3.12 |
| 2019 | 2.6 |
| 2018 | 2.2 |
| 2017 | 1.84 |
| 2016 | 1.52 |
| 2015 | 1.24 |
| 2014 | 1.0 |
| 2013 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
