---
title: " (CHSCM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CHSCM</td></tr>
    <tr><td>Web</td><td><a href="https://www.chsinc.com">www.chsinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.422 |
| 2020 | 1.688 |
| 2019 | 1.688 |
| 2018 | 1.688 |
| 2017 | 1.688 |
| 2016 | 1.688 |
| 2015 | 1.688 |
| 2014 | 0.497 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
