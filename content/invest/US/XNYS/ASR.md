---
title: " (ASR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ASR</td></tr>
    <tr><td>Web</td><td><a href="https://www.asur.com.mx">www.asur.com.mx</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 4.691 |
| 2018 | 2.958 |
| 2017 | 2.98 |
| 2016 | 2.592 |
| 2015 | 4.057 |
| 2013 | 6.626 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
