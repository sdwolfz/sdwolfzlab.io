---
title: "SHORE BANCSHARES INC (SHBI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SHORE BANCSHARES INC</td></tr>
    <tr><td>Symbol</td><td>SHBI</td></tr>
    <tr><td>Web</td><td><a href="https://www.shoreunitedbank.com">www.shoreunitedbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.24 |
| 2020 | 0.48 |
| 2019 | 0.42 |
| 2018 | 0.32 |
| 2017 | 0.22 |
| 2016 | 0.14 |
| 2015 | 0.04 |
| 2012 | 0.01 |
| 2011 | 0.09 |
| 2010 | 0.24 |
| 2009 | 0.64 |
| 2008 | 0.64 |
| 2007 | 0.64 |
| 2006 | 0.73 |
| 2005 | 0.8 |
| 2004 | 0.72 |
| 2003 | 0.66 |
| 2002 | 0.6 |
| 2001 | 0.6 |
| 2000 | 0.29 |
| 1999 | 0.54 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
