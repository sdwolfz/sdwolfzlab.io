---
title: " (CBD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CBD</td></tr>
    <tr><td>Web</td><td><a href="https://www.gpari.com.br">www.gpari.com.br</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.087 |
| 2020 | 0.113 |
| 2019 | 0.188 |
| 2018 | 0.312 |
| 2017 | 0.161 |
| 2016 | 0.004 |
| 2015 | 0.339 |
| 2014 | 0.417 |
| 2013 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
