---
title: "HORIZON BANCORP INC (HBNC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HORIZON BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>HBNC</td></tr>
    <tr><td>Web</td><td><a href="https://www.horizonbank.com">www.horizonbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 0.48 |
| 2019 | 0.44 |
| 2018 | 0.48 |
| 2017 | 0.48 |
| 2016 | 0.6 |
| 2015 | 0.43 |
| 2014 | 0.51 |
| 2013 | 0.42 |
| 2012 | 0.51 |
| 2011 | 0.64 |
| 2010 | 0.68 |
| 2009 | 0.68 |
| 2008 | 0.81 |
| 2007 | 0.58 |
| 2006 | 0.42 |
| 2005 | 0.53 |
| 2004 | 0.49 |
| 2003 | 0.6 |
| 2002 | 0.61 |
| 2001 | 1.5 |
| 2000 | 1.8 |
| 1999 | 1.8 |
| 1998 | 1.35 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
