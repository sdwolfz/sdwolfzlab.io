---
title: " (MNR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>MNR</td></tr>
    <tr><td>Web</td><td><a href="https://www.mreic.reit">www.mreic.reit</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.36 |
| 2020 | 0.68 |
| 2019 | 0.68 |
| 2018 | 0.68 |
| 2017 | 0.65 |
| 2016 | 0.64 |
| 2015 | 0.61 |
| 2014 | 0.6 |
| 2013 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
