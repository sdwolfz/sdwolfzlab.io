---
title: " (CORR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CORR</td></tr>
    <tr><td>Web</td><td><a href="https://www.corenergy.reit">www.corenergy.reit</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.9 |
| 2019 | 3.0 |
| 2018 | 3.0 |
| 2017 | 3.0 |
| 2016 | 3.0 |
| 2015 | 1.15 |
| 2014 | 0.514 |
| 2013 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
