---
title: "SANTIERUL NAVAL 2 MAI SA MANGALIA (STNM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>SANTIERUL NAVAL 2 MAI SA MANGALIA</td></tr>
    <tr><td>Symbol</td><td>STNM</td></tr>
    <tr><td>Web</td><td><a href="https://www.sn2mai.ro">www.sn2mai.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.068829 |
| 2018 | 0.001345 |
| 2017 | 0.002357 |
| 2016 | 0.04058 |
| 2015 | 0.011653 |
| 2014 | 0.009883 |
| 2013 | 0.0213 |
| 2012 | 0.0543 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
