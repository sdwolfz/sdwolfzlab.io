---
title: " (GNL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>GNL</td></tr>
    <tr><td>Web</td><td><a href="https://www.globalnetlease.com">www.globalnetlease.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.8 |
| 2020 | 1.733 |
| 2019 | 1.774 |
| 2018 | 2.124 |
| 2017 | 1.77 |
| 2016 | 0.649 |
| 2015 | 0.354 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
