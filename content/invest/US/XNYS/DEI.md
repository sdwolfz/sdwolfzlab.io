---
title: "DOUGLAS EMMETT INC (DEI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DOUGLAS EMMETT INC</td></tr>
    <tr><td>Symbol</td><td>DEI</td></tr>
    <tr><td>Web</td><td><a href="http://www.douglasemmett.com">www.douglasemmett.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.12 |
| 2019 | 1.06 |
| 2018 | 1.01 |
| 2017 | 0.94 |
| 2016 | 0.89 |
| 2015 | 0.85 |
| 2014 | 0.81 |
| 2013 | 0.56 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
