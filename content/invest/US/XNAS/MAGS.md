---
title: "MAGAL SECURITY SYSTEMS (MAGS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>MAGAL SECURITY SYSTEMS</td></tr>
    <tr><td>Symbol</td><td>MAGS</td></tr>
    <tr><td>Web</td><td><a href="https://www.magalsecurity.com">www.magalsecurity.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.079 |
| 2004 | 0.041 |
| 2001 | 0.13 |
| 2000 | 0.1 |
| 1999 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
