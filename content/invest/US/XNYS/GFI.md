---
title: " (GFI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>GFI</td></tr>
    <tr><td>Web</td><td><a href="https://www.goldfields.com">www.goldfields.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.171 |
| 2020 | 0.125 |
| 2019 | 0.043 |
| 2018 | 0.045 |
| 2017 | 0.061 |
| 2016 | 0.04 |
| 2015 | 0.016 |
| 2014 | 0.032 |
| 2013 | 0.069 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
