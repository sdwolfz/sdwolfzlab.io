---
title: "TRANSGEX SA ORADEA (TRNG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>TRANSGEX SA ORADEA</td></tr>
    <tr><td>Symbol</td><td>TRNG</td></tr>
    <tr><td>Web</td><td><a href="https://www.transgex.ro">www.transgex.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.34745 |
| 2018 | 0.08451 |
| 2017 | 0.16339 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
