---
title: "TELEFLEX INC (TFX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TELEFLEX INC</td></tr>
    <tr><td>Symbol</td><td>TFX</td></tr>
    <tr><td>Web</td><td><a href="https://www.teleflex.com">www.teleflex.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.68 |
| 2020 | 1.36 |
| 2019 | 1.36 |
| 2018 | 1.36 |
| 2017 | 1.7 |
| 2016 | 1.36 |
| 2015 | 1.36 |
| 2014 | 1.36 |
| 2013 | 0.68 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
