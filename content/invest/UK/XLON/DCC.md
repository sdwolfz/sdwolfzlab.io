---
title: "DCC (DCC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>DCC</td></tr>
    <tr><td>Symbol</td><td>DCC</td></tr>
    <tr><td>Web</td><td><a href="https://www.dcc.ie">www.dcc.ie</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 145.27 |
| 2019 | 138.35 |
| 2018 | 122.98 |
| 2017 | 111.8 |
| 2016 | 97.22 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
