---
title: "OCEANFIRST FINANCIAL CORP (OCFC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>OCEANFIRST FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>OCFC</td></tr>
    <tr><td>Web</td><td><a href="https://www.oceanfirst.com">www.oceanfirst.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.34 |
| 2020 | 0.68 |
| 2019 | 0.68 |
| 2018 | 0.62 |
| 2017 | 0.6 |
| 2016 | 0.54 |
| 2015 | 0.52 |
| 2014 | 0.49 |
| 2013 | 0.48 |
| 2012 | 0.48 |
| 2011 | 0.48 |
| 2010 | 0.48 |
| 2009 | 0.8 |
| 2008 | 0.8 |
| 2007 | 0.8 |
| 2006 | 0.8 |
| 2005 | 0.8 |
| 2004 | 0.8 |
| 2003 | 0.78 |
| 2002 | 0.86 |
| 2001 | 0.84 |
| 2000 | 0.72 |
| 1999 | 0.57 |
| 1998 | 0.68 |
| 1997 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
