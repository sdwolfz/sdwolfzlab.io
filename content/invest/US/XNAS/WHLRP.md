---
title: " (WHLRP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>WHLRP</td></tr>
    <tr><td>Web</td><td><a href="https://www.whlr.us">www.whlr.us</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 1.688 |
| 2017 | 2.25 |
| 2016 | 2.25 |
| 2015 | 2.25 |
| 2014 | 1.513 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
