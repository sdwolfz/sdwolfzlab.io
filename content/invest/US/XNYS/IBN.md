---
title: " (IBN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>IBN</td></tr>
    <tr><td>Web</td><td><a href="https://www.icicibank.com">www.icicibank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.028 |
| 2018 | 0.087 |
| 2017 | 0.077 |
| 2016 | 0.149 |
| 2015 | 0.314 |
| 2014 | 0.152 |
| 2013 | 0.667 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
