---
title: "Lindsell Train (LTI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Lindsell Train</td></tr>
    <tr><td>Symbol</td><td>LTI</td></tr>
    <tr><td>Web</td><td><a href="https://www.lindselltrain.com">www.lindselltrain.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 950.0 |
| 2018 | 180.0 |
| 2017 | 580.0 |
| 2016 | 890.0 |
| 2015 | 720.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
