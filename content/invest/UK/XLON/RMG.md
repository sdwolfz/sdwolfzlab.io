---
title: "Royal Mail (RMG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Royal Mail</td></tr>
    <tr><td>Symbol</td><td>RMG</td></tr>
    <tr><td>Web</td><td><a href="https://www.royalmailgroup.com">www.royalmailgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 24.5 |
| 2019 | 24.3 |
| 2018 | 20.65 |
| 2017 | 19.13 |
| 2016 | 18.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
