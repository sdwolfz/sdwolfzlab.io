---
title: "GLOBAL SHIP LEASE INC (GSL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GLOBAL SHIP LEASE INC</td></tr>
    <tr><td>Symbol</td><td>GSL</td></tr>
    <tr><td>Web</td><td><a href="https://www.globalshiplease.com">www.globalshiplease.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
