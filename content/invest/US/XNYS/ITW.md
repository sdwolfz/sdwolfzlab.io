---
title: "ILLINOIS TOOL WORKS INC (ITW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ILLINOIS TOOL WORKS INC</td></tr>
    <tr><td>Symbol</td><td>ITW</td></tr>
    <tr><td>Web</td><td><a href="https://www.itw.com">www.itw.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.14 |
| 2020 | 4.42 |
| 2019 | 4.14 |
| 2018 | 3.56 |
| 2017 | 2.86 |
| 2016 | 2.4 |
| 2015 | 2.07 |
| 2014 | 1.81 |
| 2013 | 0.84 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
