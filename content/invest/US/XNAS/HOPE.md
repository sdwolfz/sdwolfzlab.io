---
title: "HOPE BANCORP INC (HOPE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HOPE BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>HOPE</td></tr>
    <tr><td>Web</td><td><a href="https://www.bankofhope.com">www.bankofhope.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 0.56 |
| 2019 | 0.56 |
| 2018 | 0.54 |
| 2017 | 0.5 |
| 2016 | 0.45 |
| 2015 | 0.42 |
| 2014 | 0.35 |
| 2013 | 0.25 |
| 2012 | 0.05 |
| 2008 | 0.11 |
| 2007 | 0.11 |
| 2006 | 0.11 |
| 2005 | 0.11 |
| 2004 | 0.133 |
| 2003 | 0.2 |
| 2002 | 0.4 |
| 2001 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
