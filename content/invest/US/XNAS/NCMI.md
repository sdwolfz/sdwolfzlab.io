---
title: "NATIONAL CINEMEDIA INC (NCMI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NATIONAL CINEMEDIA INC</td></tr>
    <tr><td>Symbol</td><td>NCMI</td></tr>
    <tr><td>Web</td><td><a href="https://www.ncm.com">www.ncm.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.05 |
| 2020 | 0.4 |
| 2019 | 0.68 |
| 2018 | 0.68 |
| 2017 | 0.88 |
| 2016 | 0.88 |
| 2015 | 0.88 |
| 2014 | 1.38 |
| 2013 | 0.88 |
| 2012 | 0.88 |
| 2011 | 0.84 |
| 2010 | 0.72 |
| 2009 | 0.64 |
| 2008 | 0.62 |
| 2007 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
