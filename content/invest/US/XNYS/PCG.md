---
title: "PG&E CORP (PCG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PG&E CORP</td></tr>
    <tr><td>Symbol</td><td>PCG</td></tr>
    <tr><td>Web</td><td><a href="https://www.pgecorp.com">www.pgecorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 1.55 |
| 2016 | 1.925 |
| 2015 | 1.82 |
| 2014 | 1.82 |
| 2013 | 1.365 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
