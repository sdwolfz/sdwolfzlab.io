---
title: "Install Emacs from source"
date: 2020-03-27T19:25:47Z
type: posts
slug: install-emacs-from-source
tags: ["emacs", "linux", "productivity"]
---

## Prerequisites

```sh
sudo pacman -S libgccjit
```

## Compile

```sh
git clone git://git.sv.gnu.org/emacs.git
cd emacs

./autogen.sh git
./autogen.sh autoconf
./configure                                   \
  CFLAGS='-O3 -g0'                            \
  --program-transform-name='s/^ctags$/etags/' \
  --with-native-compilation

make -j`nproc`
sudo make install
```

## Checks

Press `Alt + Shift + :` and paste these...

* For JIT:

```elisp
(if (and
      (fboundp 'native-comp-available-p)
      (native-comp-available-p))
    (message "[SUCCESS] Native Compilation available")
  (message "[FAIL] Native Complation not available"))
```

* For fast JSON parser:

```elisp
(if (functionp 'json-serialize)
    (message "[SUCCESS] Native JSON available")
  (message "[FAIL] Native JSON not available"))
```

## Debug

```sh
./configure                                         \
  CFLAGS='-Og -gdwarf-5 -g3'                        \
  --program-transform-name='s/^ctags$/ctags.emacs/'
```

Hope you know how to use `gdb` when something goes wrong...
