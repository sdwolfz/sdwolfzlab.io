---
title: "GLOBAL PAYMENTS INC (GPN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GLOBAL PAYMENTS INC</td></tr>
    <tr><td>Symbol</td><td>GPN</td></tr>
    <tr><td>Web</td><td><a href="https://www.globalpaymentsinc.com">www.globalpaymentsinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.39 |
| 2020 | 0.78 |
| 2019 | 0.225 |
| 2018 | 0.04 |
| 2017 | 0.043 |
| 2016 | 0.04 |
| 2015 | 0.07 |
| 2014 | 0.08 |
| 2013 | 0.04 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
