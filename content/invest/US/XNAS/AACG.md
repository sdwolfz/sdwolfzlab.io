---
title: " (AACG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>AACG</td></tr>
    <tr><td>Web</td><td><a href="https://ir.atai.net.cn">ir.atai.net.cn</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 6.0 |
| 2017 | 0.41 |
| 2014 | 0.41 |
| 2012 | 0.174 |
| 2011 | 0.43 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
