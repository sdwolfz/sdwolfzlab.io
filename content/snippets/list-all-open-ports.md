---
title: "List all open ports"
date: 2021-06-29T08:33:30Z
type: posts
slug: list-all-open-ports
series: ["Networking"]
tags: ["netcat", "networking", "linux", "security"]
---

```sh
netstat -atun
```

Now either grep or browse to see if you accidentally exposed your database to
the world.
