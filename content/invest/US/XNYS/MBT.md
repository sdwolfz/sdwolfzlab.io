---
title: " (MBT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>MBT</td></tr>
    <tr><td>Web</td><td><a href="https://www.ir.mts.ru">www.ir.mts.ru</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.347 |
| 2019 | 0.727 |
| 2018 | 0.798 |
| 2017 | 0.705 |
| 2016 | 0.643 |
| 2015 | 0.66 |
| 2014 | 1.072 |
| 2013 | 1.005 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
