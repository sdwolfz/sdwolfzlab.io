---
title: "TALKTALK TELECOM GROUP PLC ORD 0.1P (TALK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>TALKTALK TELECOM GROUP PLC ORD 0.1P</td></tr>
    <tr><td>Symbol</td><td>TALK</td></tr>
    <tr><td>Web</td><td><a href="http://www.talktalkgroup.com">www.talktalkgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.5 |
| 2019 | 2.5 |
| 2018 | 4.0 |
| 2017 | 10.29 |
| 2016 | 15.87 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
