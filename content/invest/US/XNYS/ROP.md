---
title: "ROPER TECHNOLOGIES INC (ROP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ROPER TECHNOLOGIES INC</td></tr>
    <tr><td>Symbol</td><td>ROP</td></tr>
    <tr><td>Web</td><td><a href="https://www.ropertech.com">www.ropertech.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.126 |
| 2020 | 2.048 |
| 2019 | 1.852 |
| 2018 | 1.648 |
| 2017 | 1.4 |
| 2016 | 1.2 |
| 2015 | 1.0 |
| 2014 | 0.8 |
| 2013 | 0.33 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
