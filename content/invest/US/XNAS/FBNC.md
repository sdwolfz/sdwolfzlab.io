---
title: "FIRST BANCORP NC (FBNC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST BANCORP NC</td></tr>
    <tr><td>Symbol</td><td>FBNC</td></tr>
    <tr><td>Web</td><td><a href="https://www.localfirstbank.com">www.localfirstbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.72 |
| 2019 | 0.54 |
| 2018 | 0.4 |
| 2017 | 0.32 |
| 2016 | 0.32 |
| 2015 | 0.32 |
| 2014 | 0.32 |
| 2013 | 0.32 |
| 2012 | 0.32 |
| 2011 | 0.32 |
| 2010 | 0.32 |
| 2009 | 0.32 |
| 2008 | 0.76 |
| 2007 | 0.76 |
| 2006 | 0.74 |
| 2005 | 0.7 |
| 2004 | 0.9 |
| 2003 | 0.94 |
| 2002 | 0.9 |
| 2001 | 0.88 |
| 2000 | 0.7 |
| 1999 | 0.567 |
| 1998 | 0.6 |
| 1997 | 0.52 |
| 1996 | 0.66 |
| 1995 | 0.7 |
| 1994 | 0.65 |
| 1993 | 0.58 |
| 1992 | 0.425 |
| 1991 | 0.315 |
| 1990 | 0.205 |
| 1989 | 0.165 |
| 1988 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
