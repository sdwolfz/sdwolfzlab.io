---
title: "GOLAR LNG PARTNERS LP 8.75% CUM RED PFD UNIT SER A (GMLPP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GOLAR LNG PARTNERS LP 8.75% CUM RED PFD UNIT SER A</td></tr>
    <tr><td>Symbol</td><td>GMLPP</td></tr>
    <tr><td>Web</td><td><a href="http://www.golarlngpartners.com">www.golarlngpartners.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.188 |
| 2019 | 2.188 |
| 2018 | 2.279 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
