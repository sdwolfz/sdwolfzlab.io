---
title: "DORMAN PRODUCTS IN (DORM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>DORMAN PRODUCTS IN</td></tr>
    <tr><td>Symbol</td><td>DORM</td></tr>
    <tr><td>Web</td><td><a href="https://www.dormanproducts.com">www.dormanproducts.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2012 | 1.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
