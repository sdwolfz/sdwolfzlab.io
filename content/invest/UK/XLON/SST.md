---
title: "Scot.orntl.smll (SST)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Scot.orntl.smll</td></tr>
    <tr><td>Symbol</td><td>SST</td></tr>
    <tr><td>Web</td><td><a href="https://www.scottishoriental.com">www.scottishoriental.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 11.5 |
| 2019 | 11.5 |
| 2018 | 11.5 |
| 2017 | 11.5 |
| 2016 | 11.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
