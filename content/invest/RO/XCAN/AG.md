---
title: "AGROLAND BUSINESS SYSTEM (AG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>AGROLAND BUSINESS SYSTEM</td></tr>
    <tr><td>Symbol</td><td>AG</td></tr>
    <tr><td>Web</td><td><a href="https://www.agroland.ro">www.agroland.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
