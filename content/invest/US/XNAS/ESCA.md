---
title: "ESCALADE INC (ESCA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ESCALADE INC</td></tr>
    <tr><td>Symbol</td><td>ESCA</td></tr>
    <tr><td>Web</td><td><a href="https://www.escaladeinc.com">www.escaladeinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 0.53 |
| 2019 | 0.5 |
| 2018 | 0.5 |
| 2017 | 0.46 |
| 2016 | 0.44 |
| 2015 | 0.43 |
| 2014 | 0.38 |
| 2013 | 0.34 |
| 2012 | 0.31 |
| 2011 | 0.32 |
| 2010 | 0.1 |
| 2008 | 0.25 |
| 2007 | 0.22 |
| 2006 | 0.2 |
| 2005 | 0.15 |
| 2004 | 0.24 |
| 1999 | 1.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
