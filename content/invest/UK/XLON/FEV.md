---
title: "Fidelity European Values (FEV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Fidelity European Values</td></tr>
    <tr><td>Symbol</td><td>FEV</td></tr>
    <tr><td>Web</td><td><a href="https://www.fidelityinvestmenttrusts.com">www.fidelityinvestmenttrusts.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 6.47 |
| 2018 | 6.28 |
| 2017 | 4.35 |
| 2016 | 4.17 |
| 2015 | 3.33 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
