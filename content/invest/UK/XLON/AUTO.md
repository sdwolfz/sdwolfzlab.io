---
title: "Auto Trader (AUTO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Auto Trader</td></tr>
    <tr><td>Symbol</td><td>AUTO</td></tr>
    <tr><td>Web</td><td><a href="https://www.autotrader.co.uk">www.autotrader.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.4 |
| 2019 | 6.7 |
| 2018 | 5.9 |
| 2017 | 2.17 |
| 2016 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
