---
title: "WILLIAM HILL PLC ORD 10P (WMH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>WILLIAM HILL PLC ORD 10P</td></tr>
    <tr><td>Symbol</td><td>WMH</td></tr>
    <tr><td>Web</td><td><a href="http://www.williamhillplc.co.uk">www.williamhillplc.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 10.4 |
| 2019 | 13.2 |
| 2017 | 11.19 |
| 2016 | 12.5 |
| 2015 | 12.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
