---
title: "BERMAS S.A. (BRM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>BERMAS S.A.</td></tr>
    <tr><td>Symbol</td><td>BRM</td></tr>
    <tr><td>Web</td><td><a href="https://www.bermas.ro">www.bermas.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.07 |
| 2019 | 0.08 |
| 2018 | 0.07 |
| 2017 | 0.07 |
| 2016 | 0.065 |
| 2015 | 0.06 |
| 2014 | 0.04 |
| 2013 | 0.055 |
| 2012 | 0.065 |
| 2011 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
