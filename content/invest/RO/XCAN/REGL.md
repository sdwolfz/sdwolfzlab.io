---
title: "REGAL SA GALATI (REGL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>REGAL SA GALATI</td></tr>
    <tr><td>Symbol</td><td>REGL</td></tr>
    <tr><td>Web</td><td><a href="https://www.regalgl.ro">www.regalgl.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 1.74827 |
| 2018 | 2.38 |
| 2017 | 1.903753 |
| 2016 | 0.053737 |
| 2013 | 3.5198 |
| 2012 | 0.6696 |
| 2008 | 0.0244 |
| 2007 | 0.038 |
| 2006 | 0.0207 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
