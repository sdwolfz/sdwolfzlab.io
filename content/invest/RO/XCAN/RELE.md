---
title: "RELEE SA MEDIAS (RELE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>RELEE SA MEDIAS</td></tr>
    <tr><td>Symbol</td><td>RELE</td></tr>
    <tr><td>Web</td><td><a href="https://www.relee.ro">www.relee.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.21197 |
| 2017 | 0.2106 |
| 2016 | 0.3978 |
| 2015 | 0.7698 |
| 2007 | 0.6 |
| 2005 | 0.4 |
| 2004 | 0.73 |
| 2003 | 0.4068 |
| 2002 | 0.4051 |
| 2001 | 0.595 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
