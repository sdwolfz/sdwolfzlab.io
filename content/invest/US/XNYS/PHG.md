---
title: " (PHG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>PHG</td></tr>
    <tr><td>Web</td><td><a href="https://www.philips.com">www.philips.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.878 |
| 2020 | 1.725 |
| 2019 | 0.817 |
| 2018 | 0.798 |
| 2017 | 0.734 |
| 2016 | 0.751 |
| 2015 | 0.773 |
| 2014 | 0.933 |
| 2013 | 0.835 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
