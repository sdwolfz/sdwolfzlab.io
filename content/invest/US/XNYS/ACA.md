---
title: "ARCOSA INC (ACA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ARCOSA INC</td></tr>
    <tr><td>Symbol</td><td>ACA</td></tr>
    <tr><td>Web</td><td><a href="https://www.arcosa.com">www.arcosa.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.15 |
| 2020 | 0.2 |
| 2019 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
