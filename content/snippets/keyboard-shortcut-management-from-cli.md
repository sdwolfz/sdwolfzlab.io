---
title: "Keyboard shortcut management from CLI"
date: 2020-12-04T13:18:12Z
type: posts
slug: keyboard-shortcut-management-from-cli
tags: ["linux", "xfce4", "productivity"]
---

First remove an existing key binding with:

```sh
xfconf-query --reset                 \
  --channel xfce4-keyboard-shortcuts \
  --property /commands/custom/'<Super>f'
```

Then add yours:

```sh
xfconf-query --create                    \
  --channel xfce4-keyboard-shortcuts     \
  --property /commands/custom/'<Super>f' \
  --type string                          \
  --set 'exo-open --launch FileManager'
```

Customize these commands, and add them to any automation scripts you have!

> **Note:** If you want to bind `<Ctrl>` you have to call it `<Primary>` for
> some odd reason!
