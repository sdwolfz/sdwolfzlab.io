---
title: "HCI GROUP INC (HCI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HCI GROUP INC</td></tr>
    <tr><td>Symbol</td><td>HCI</td></tr>
    <tr><td>Web</td><td><a href="https://www.hcigroup.com">www.hcigroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.8 |
| 2020 | 1.6 |
| 2019 | 1.6 |
| 2018 | 1.475 |
| 2017 | 1.4 |
| 2016 | 1.2 |
| 2015 | 1.2 |
| 2014 | 1.1 |
| 2013 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
