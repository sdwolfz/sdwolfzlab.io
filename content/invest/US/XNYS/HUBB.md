---
title: "HUBBELL INC (HUBB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HUBBELL INC</td></tr>
    <tr><td>Symbol</td><td>HUBB</td></tr>
    <tr><td>Web</td><td><a href="https://www.hubbell.com">www.hubbell.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.96 |
| 2020 | 3.71 |
| 2019 | 3.43 |
| 2018 | 3.15 |
| 2017 | 2.87 |
| 2016 | 2.59 |
| 2015 | 0.63 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
