---
title: " (FBIOP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>FBIOP</td></tr>
    <tr><td>Web</td><td><a href="https://www.fortressbiotech.com">www.fortressbiotech.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.977 |
| 2020 | 2.344 |
| 2019 | 2.344 |
| 2018 | 2.344 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
