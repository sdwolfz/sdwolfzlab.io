---
title: " (DHC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>DHC</td></tr>
    <tr><td>Web</td><td><a href="https://www.dhcreit.com">www.dhcreit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.02 |
| 2020 | 0.18 |
| 2019 | 0.84 |
| 2018 | 1.56 |
| 2017 | 1.56 |
| 2016 | 1.56 |
| 2015 | 1.56 |
| 2014 | 1.56 |
| 2013 | 1.56 |
| 2012 | 1.53 |
| 2011 | 1.49 |
| 2010 | 1.45 |
| 2009 | 1.42 |
| 2008 | 1.4 |
| 2007 | 1.37 |
| 2006 | 1.3 |
| 2005 | 1.28 |
| 2004 | 1.25 |
| 2003 | 1.24 |
| 2002 | 1.23 |
| 2001 | 0.9 |
| 2000 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
