---
title: "NORTECH SYSTEMS INC (NSYS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NORTECH SYSTEMS INC</td></tr>
    <tr><td>Symbol</td><td>NSYS</td></tr>
    <tr><td>Web</td><td><a href="https://www.nortechsys.com">www.nortechsys.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 1993 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
