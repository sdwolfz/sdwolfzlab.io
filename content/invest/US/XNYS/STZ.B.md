---
title: "CONSTELLATION BRANDS INC (STZ.B)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CONSTELLATION BRANDS INC</td></tr>
    <tr><td>Symbol</td><td>STZ.B</td></tr>
    <tr><td>Web</td><td><a href="https://www.cbrands.com">www.cbrands.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.37 |
| 2020 | 2.72 |
| 2019 | 2.71 |
| 2018 | 2.48 |
| 2017 | 1.77 |
| 2016 | 1.36 |
| 2015 | 0.84 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
