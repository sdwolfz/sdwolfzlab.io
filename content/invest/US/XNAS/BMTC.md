---
title: "BRYN MAWR BANK CORP (BMTC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BRYN MAWR BANK CORP</td></tr>
    <tr><td>Symbol</td><td>BMTC</td></tr>
    <tr><td>Web</td><td><a href="https://www.bmt.com">www.bmt.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.54 |
| 2020 | 1.06 |
| 2019 | 1.02 |
| 2018 | 0.94 |
| 2017 | 0.86 |
| 2016 | 0.82 |
| 2015 | 0.78 |
| 2014 | 0.74 |
| 2013 | 0.69 |
| 2012 | 0.64 |
| 2011 | 0.6 |
| 2010 | 0.56 |
| 2009 | 0.56 |
| 2008 | 0.54 |
| 2007 | 0.5 |
| 2006 | 0.46 |
| 2005 | 0.42 |
| 2004 | 0.4 |
| 2003 | 0.7 |
| 2002 | 0.76 |
| 2001 | 0.72 |
| 2000 | 0.68 |
| 1999 | 0.6 |
| 1998 | 0.81 |
| 1997 | 0.72 |
| 1996 | 0.92 |
| 1995 | 1.0 |
| 1994 | 0.65 |
| 1993 | 0.4 |
| 1991 | 0.3 |
| 1990 | 1.29 |
| 1989 | 1.43 |
| 1988 | 0.68 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
