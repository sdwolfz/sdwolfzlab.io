---
title: "RUTHS HOSPITALITY GROUP INC (RUTH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>RUTHS HOSPITALITY GROUP INC</td></tr>
    <tr><td>Symbol</td><td>RUTH</td></tr>
    <tr><td>Web</td><td><a href="https://www.rhgi.com">www.rhgi.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.15 |
| 2019 | 0.52 |
| 2018 | 0.44 |
| 2017 | 0.36 |
| 2016 | 0.28 |
| 2015 | 0.24 |
| 2014 | 0.2 |
| 2013 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
