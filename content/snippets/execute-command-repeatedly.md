---
title: "Execute a command repeatedly"
date: 2020-11-19T18:09:07Z
type: posts
slug: execute-command-repeatedly
tags: ["linux", "productivity"]
---

## Install

It might not be available by default on your system so make sure you install it:

```sh
# Manjaro / Arch
sudo pacman -S procps-ng

# Alpine
apk add --update procps
```

## Run

Prefix all your commands with `watch` and they'll be executed every 2 seconds:

```sh
watch pwd
```

Press `CTRL + C` to quit.

## Utility

It's useful for viewing things that change over time, like your temperature
readings:

```sh
watch sensors
```
