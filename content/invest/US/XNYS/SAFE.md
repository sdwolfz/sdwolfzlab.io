---
title: "SAFEHOLD INC (SAFE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>SAFEHOLD INC</td></tr>
    <tr><td>Symbol</td><td>SAFE</td></tr>
    <tr><td>Web</td><td><a href="https://www.safeholdinc.com">www.safeholdinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.162 |
| 2020 | 0.798 |
| 2019 | 0.462 |
| 2018 | 0.6 |
| 2017 | 0.157 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
