---
title: "VALUE LINE INC (VALU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>VALUE LINE INC</td></tr>
    <tr><td>Symbol</td><td>VALU</td></tr>
    <tr><td>Web</td><td><a href="https://www.valueline.com">www.valueline.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.43 |
| 2020 | 0.83 |
| 2019 | 0.79 |
| 2018 | 0.95 |
| 2017 | 0.71 |
| 2016 | 0.67 |
| 2015 | 0.62 |
| 2014 | 0.6 |
| 2013 | 0.6 |
| 2012 | 0.6 |
| 2011 | 0.8 |
| 2010 | 5.4 |
| 2009 | 1.1 |
| 2008 | 1.4 |
| 2007 | 1.2 |
| 2006 | 1.05 |
| 2005 | 1.0 |
| 2004 | 18.5 |
| 2003 | 1.0 |
| 2002 | 1.0 |
| 2001 | 1.0 |
| 2000 | 1.0 |
| 1999 | 1.0 |
| 1998 | 1.0 |
| 1997 | 15.75 |
| 1996 | 0.85 |
| 1995 | 0.6 |
| 1994 | 1.0 |
| 1993 | 0.5 |
| 1992 | 0.6 |
| 1991 | 0.6 |
| 1990 | 0.6 |
| 1989 | 0.6 |
| 1988 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
