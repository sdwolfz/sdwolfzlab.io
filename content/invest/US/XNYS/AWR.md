---
title: "AMERICAN STATES WATER CO (AWR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AMERICAN STATES WATER CO</td></tr>
    <tr><td>Symbol</td><td>AWR</td></tr>
    <tr><td>Web</td><td><a href="https://www.aswater.com">www.aswater.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.67 |
| 2020 | 1.28 |
| 2019 | 1.16 |
| 2018 | 1.06 |
| 2017 | 0.994 |
| 2016 | 0.914 |
| 2015 | 0.874 |
| 2014 | 0.832 |
| 2013 | 0.406 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
