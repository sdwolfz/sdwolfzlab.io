---
title: "STIFEL FINANCIAL CORP (SF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>STIFEL FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>SF</td></tr>
    <tr><td>Web</td><td><a href="https://www.stifel.com">www.stifel.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 0.623 |
| 2019 | 0.6 |
| 2018 | 0.48 |
| 2017 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
