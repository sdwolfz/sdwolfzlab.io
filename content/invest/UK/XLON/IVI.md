---
title: "INVESCO INCOME GROWTH TRUST PLC ORD 25P (IVI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>INVESCO INCOME GROWTH TRUST PLC ORD 25P</td></tr>
    <tr><td>Symbol</td><td>IVI</td></tr>
    <tr><td>Web</td><td><a href="http://www.invesco.co.uk">www.invesco.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 11.75 |
| 2019 | 11.45 |
| 2018 | 11.05 |
| 2017 | 10.65 |
| 2016 | 10.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
