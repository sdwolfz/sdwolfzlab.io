---
title: "HOULIHAN LOKEY INC (HLI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HOULIHAN LOKEY INC</td></tr>
    <tr><td>Symbol</td><td>HLI</td></tr>
    <tr><td>Web</td><td><a href="https://www.hl.com">www.hl.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.33 |
| 2020 | 1.28 |
| 2019 | 1.2 |
| 2018 | 1.01 |
| 2017 | 0.8 |
| 2016 | 0.66 |
| 2015 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
