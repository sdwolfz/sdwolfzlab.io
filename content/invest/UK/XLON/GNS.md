---
title: "Genus (GNS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Genus</td></tr>
    <tr><td>Symbol</td><td>GNS</td></tr>
    <tr><td>Web</td><td><a href="https://www.genusplc.com">www.genusplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 29.1 |
| 2019 | 27.7 |
| 2018 | 16.0 |
| 2017 | 23.6 |
| 2016 | 21.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
