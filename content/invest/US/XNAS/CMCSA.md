---
title: "COMCAST CORP (CMCSA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COMCAST CORP</td></tr>
    <tr><td>Symbol</td><td>CMCSA</td></tr>
    <tr><td>Web</td><td><a href="https://www.comcastcorporation.com">www.comcastcorporation.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.48 |
| 2020 | 0.9 |
| 2019 | 0.63 |
| 2018 | 0.918 |
| 2017 | 0.473 |
| 2016 | 1.35 |
| 2015 | 0.975 |
| 2014 | 0.675 |
| 2013 | 0.78 |
| 2012 | 0.65 |
| 2011 | 0.545 |
| 2010 | 0.378 |
| 2009 | 0.265 |
| 2008 | 0.188 |
| 1999 | 37.82 |
| 1998 | 0.093 |
| 1997 | 0.093 |
| 1996 | 0.093 |
| 1995 | 0.093 |
| 1994 | 0.07 |
| 1993 | 0.14 |
| 1992 | 0.14 |
| 1991 | 0.135 |
| 1990 | 0.12 |
| 1989 | 0.097 |
| 1988 | 0.03 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
