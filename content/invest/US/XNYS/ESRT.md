---
title: " (ESRT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ESRT</td></tr>
    <tr><td>Web</td><td><a href="https://www.empirestaterealtytrust.com">www.empirestaterealtytrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.21 |
| 2019 | 0.42 |
| 2018 | 0.42 |
| 2017 | 0.42 |
| 2016 | 0.4 |
| 2015 | 0.34 |
| 2014 | 0.34 |
| 2013 | 0.079 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
