---
title: "TURQUOISE HILL RES LTD (TRQ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TURQUOISE HILL RES LTD</td></tr>
    <tr><td>Symbol</td><td>TRQ</td></tr>
    <tr><td>Web</td><td><a href="https://www.turquoisehill.com">www.turquoisehill.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
