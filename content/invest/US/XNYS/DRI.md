---
title: "DARDEN RESTAURANTS INC (DRI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>DARDEN RESTAURANTS INC</td></tr>
    <tr><td>Symbol</td><td>DRI</td></tr>
    <tr><td>Web</td><td><a href="https://www.darden.com">www.darden.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.25 |
| 2020 | 1.18 |
| 2019 | 3.26 |
| 2018 | 1.26 |
| 2017 | 2.38 |
| 2016 | 2.12 |
| 2015 | 2.2 |
| 2014 | 2.2 |
| 2013 | 1.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
