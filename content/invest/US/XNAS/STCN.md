---
title: "STEEL CONNECT INC (STCN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>STEEL CONNECT INC</td></tr>
    <tr><td>Symbol</td><td>STCN</td></tr>
    <tr><td>Web</td><td><a href="https://www.steelconnectinc.com">www.steelconnectinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2011 | 0.913 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
