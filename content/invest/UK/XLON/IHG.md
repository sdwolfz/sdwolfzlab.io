---
title: "InterContinental Hotels (IHG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>InterContinental Hotels</td></tr>
    <tr><td>Symbol</td><td>IHG</td></tr>
    <tr><td>Web</td><td><a href="https://www.ihgplc.com">www.ihgplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 125.8 |
| 2018 | 114.4 |
| 2017 | 104.0 |
| 2016 | 460.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
