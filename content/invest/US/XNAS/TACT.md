---
title: "TRANSACT TECHNOLOGIES INC (TACT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TRANSACT TECHNOLOGIES INC</td></tr>
    <tr><td>Symbol</td><td>TACT</td></tr>
    <tr><td>Web</td><td><a href="https://www.transact-tech.com">www.transact-tech.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.36 |
| 2018 | 0.36 |
| 2017 | 0.35 |
| 2016 | 0.32 |
| 2015 | 0.32 |
| 2014 | 0.31 |
| 2013 | 0.27 |
| 2012 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
