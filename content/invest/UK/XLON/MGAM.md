---
title: "Morgan Advanced Materials (MGAM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Morgan Advanced Materials</td></tr>
    <tr><td>Symbol</td><td>MGAM</td></tr>
    <tr><td>Web</td><td><a href="https://www.morganadvancedmaterials.com">www.morganadvancedmaterials.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 11.0 |
| 2018 | 11.0 |
| 2017 | 11.0 |
| 2016 | 11.0 |
| 2015 | 11.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
