---
title: "Remove node package funding spam"
date: 2021-09-30T14:33:45Z
type: posts
slug: remove-node-package-funding-spam
series: []
tags: ["npm", "node", "javascript"]
---

JavaScript developers have no dignity! Here is how you can restore it:

```sh
npm config set fund false
```

## Docker

Add this to your `Dockerfile`:

```dockerfile
# ------------------------------------------------------------------------------
# NPM

RUN set -ex                                            && \
                                                          \
    echo 'Restore dignity to the jacascript community' && \
    npm config set fund false
```
