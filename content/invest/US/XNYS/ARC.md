---
title: "ARC DOCUMENT SOLUTIONS INC (ARC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ARC DOCUMENT SOLUTIONS INC</td></tr>
    <tr><td>Symbol</td><td>ARC</td></tr>
    <tr><td>Web</td><td><a href="https://www.e-arc.com">www.e-arc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.05 |
| 2020 | 0.01 |
| 2019 | 0.01 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
