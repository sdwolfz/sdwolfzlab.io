---
title: "PAPA JOHNS INTERNATIONAL INC (PZZA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PAPA JOHNS INTERNATIONAL INC</td></tr>
    <tr><td>Symbol</td><td>PZZA</td></tr>
    <tr><td>Web</td><td><a href="https://www.papajohns.com">www.papajohns.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.45 |
| 2020 | 0.9 |
| 2019 | 0.9 |
| 2018 | 0.9 |
| 2017 | 0.85 |
| 2016 | 0.75 |
| 2015 | 0.63 |
| 2014 | 0.53 |
| 2013 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
