---
title: "LABORATORY CORP AMER HLDGS (LH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>LABORATORY CORP AMER HLDGS</td></tr>
    <tr><td>Symbol</td><td>LH</td></tr>
    <tr><td>Web</td><td><a href="https://www.labcorp.com">www.labcorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
