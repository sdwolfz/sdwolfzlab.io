---
title: "BROWN(N.)GROUP PLC ORD 11 1/19P (BWNG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>BROWN(N.)GROUP PLC ORD 11 1/19P</td></tr>
    <tr><td>Symbol</td><td>BWNG</td></tr>
    <tr><td>Web</td><td><a href="http://www.nbrown.co.uk">www.nbrown.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.83 |
| 2019 | 7.1 |
| 2018 | 14.23 |
| 2017 | 14.23 |
| 2016 | 14.23 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
