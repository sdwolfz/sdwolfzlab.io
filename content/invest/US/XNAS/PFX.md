---
title: "PHENIXFIN CORPORATION (PFX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PHENIXFIN CORPORATION</td></tr>
    <tr><td>Symbol</td><td>PFX</td></tr>
    <tr><td>Web</td><td><a href="https://www.medleycapitalcorp.com">www.medleycapitalcorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.05 |
| 2018 | 0.46 |
| 2017 | 0.7 |
| 2016 | 1.04 |
| 2015 | 1.2 |
| 2014 | 1.48 |
| 2013 | 1.46 |
| 2012 | 1.31 |
| 2011 | 0.62 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
