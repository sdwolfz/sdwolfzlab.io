class Game {
  constructor(document, root, params) {
    this.root   = root
    this.params = params

    this.nodes = {
      document: document,
      root:     root,
      restart:  null,
      log:      null
    }

    this.state   = this.#initialState()
    this.canvas  = null
    this.context = null

    if (this.params.log == 'debug') console.log('Game Initialized!')
  }

  mount() {
    this.canvas  = document.createElement('canvas')
    this.context = this.canvas.getContext('2d')

    this.canvas.width  = this.params.width
    this.canvas.height = this.params.height

    this.root.appendChild(this.canvas)

    if (this.params.log == 'debug') console.log('Game Mounted!')

    return this
  }

  start() {
    if (this.params.log == 'debug') console.log('Starting the game!')

    this.#bindClick(this.canvas)

    this.#generatePlayer(this.context)
    this.#generateTarget(this.context)

    this.#generateDistance(this.context)
  }

  restart() {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height)
    this.state = this.#initialState()

    if (this.nodes.trail) {
      this.nodes.trail.innerHTML = ''
    }

    this.start()
  }

  // ---------------------------------------------------------------------------

  bindRestart(element) {
    this.nodes.restart = element

    element.addEventListener('click', (event) => {
      event.preventDefault()

      this.restart()
    })
  }

  bindCheat(element) {
    this.nodes.cheat = element

    element.addEventListener('click', (event) => {
      event.preventDefault()

      if (!this.state.over) {
        if (this.params.log == 'debug') {
          console.log('Cheating by drawing radius!')
        }

        this.#appendTrail('I am a nasty cheater!')

        this.#drawCheat(
          this.context,
          this.state.player.x,
          this.state.player.y,
          this.state.distance
        )
      }
    })
  }

  bindTrail(element) {
    this.nodes.trail = element
  }

  #bindClick(canvas) {
    if (!this.bound) {
      canvas.addEventListener('mousedown', this.#handleClick.bind(this))

      this.bound = true
    }
  }

  // ---------------------------------------------------------------------------

  #handleClick(event) {
    event.preventDefault()

    if (this.#targetReached()) {
      this.state.over = true

      return
    }

    const boundary = this.canvas.getBoundingClientRect()

    let x = event.clientX - boundary.left
    let y = event.clientY - boundary.top

    if (this.params.log == 'debug') {
      console.log(`Clicked - x: ${x} y: ${y}`)
    }

    this.state.steps += 1

    x = x.toFixed(0)
    y = y.toFixed(0)

    this.#drawFootstep(this.context, this.state.player.x, this.state.player.y)
    this.#movePlayer(x, y)
    this.#drawPlayer(this.context, x, y)

    this.#generateDistance(this.context)

    if (this.#targetReached()) {
      this.state.over = true

      this.#drawTarget(this.context, this.state.target.x, this.state.target.y)
      this.#appendTrail(`You found your target in ${this.state.steps} steps.`)
    }
  }

  // ---------------------------------------------------------------------------

  #initialState() {
    return {
      player: {
        x: null,
          y: null
      },
      target: {
        x: null,
          y: null
      },
      distance: null,
      steps: 0,
      over: false
    }
  }

  #randomPosition(start, end, offset) {
    return Math.floor(Math.random() * (end - offset * 2) ) + start + offset * 2
  }

  #generatePlayer(context) {
    let x = this.#randomPosition(0, this.params.width, this.params.offset)
    let y = this.#randomPosition(0, this.params.height, this.params.offset)
    this.#movePlayer(x, y)
    this.#drawPlayer(context, x, y)
  }

  #generateTarget(_context) {
    let x = this.#randomPosition(0, this.params.width, this.params.offset)
    let y = this.#randomPosition(0, this.params.height, this.params.offset)
    this.#moveTarget(x, y)
  }

  #generateDistance(_context) {
    let distance = this.#distanceBetween(this.state.player, this.state.target)
    distance = distance.toFixed(2)

    if (this.params.log == 'debug') console.log(`Distance: ${distance}`)
    this.#appendTrail(`Target Distance: ${+distance}`)

    this.state.distance = distance
  }

  #movePlayer(x, y) {
    this.state.player.x = x
    this.state.player.y = y

    this.#appendTrail(`Your coordinates are | x: ${x} y: ${y} |`)

    if (this.params.log == 'debug') {
      console.log(`Player Location - x: ${x} y: ${y}`)
    }
  }

  #moveTarget(x, y) {
    this.state.target.x = x
    this.state.target.y = y

    if (this.params.log == 'debug') {
      console.log(`Target Location - x: ${x} y: ${y}`)
    }
  }

  #drawPlayer(context, x, y) {
    context.beginPath()
    context.fillStyle = 'black'
    context.fillRect(
      x - this.params.offset,
      y - this.params.offset,
      this.params.offset * 2,
      this.params.offset * 2
    )
    context.closePath()
  }

  #drawTarget(context, x, y) {
    context.beginPath()
    context.fillStyle = 'green'
    context.fillRect(
      x - this.params.offset,
      y - this.params.offset,
      this.params.offset * 2,
      this.params.offset * 2
    )
    context.closePath()
  }

  #drawFootstep(context, x, y) {
    context.beginPath()
    context.fillStyle = 'lightgray'
    context.fillRect(
      x - this.params.offset,
      y - this.params.offset,
      this.params.offset * 2,
      this.params.offset * 2
    )
    context.closePath()
  }

  #drawCheat(context, x, y, distance) {
    context.beginPath()
    context.arc(x, y, distance, 0, 2 * Math.PI)
    context.fillStyle = 'lightgreen'
    context.globalAlpha = 0.2
    context.fill()
    context.strokeStyle = 'green'
    context.globalAlpha = 0.5
    context.stroke()
    context.globalAlpha = 1
    context.closePath()
  }

  // ---------------------------------------------------------------------------

  // c = √((xA − xB)^2 + (yA − yB)^2)
  #distanceBetween(player, target) {
    const result = Math.sqrt(
      Math.pow(player.x - target.x, 2) + Math.pow(player.y - target.y, 2)
    )

    return result
  }

  // ---------------------------------------------------------------------------

  #appendTrail(text) {
    if (this.nodes.trail) {
      const textNode = this.nodes.document.createTextNode(text)
      const divNode  = this.nodes.document.createElement('div')

      divNode.append(textNode)
      this.nodes.trail.prepend(divNode)

      if (this.params.log == 'debug') {
        console.log(`Trail updated with: ${text}`)
      }

      return true
    }

    if (this.params.log == 'debug') {
      console.log(`Trail not configured, skipping`)
    }

    return false
  }

  // ---------------------------------------------------------------------------

  #targetReached() {
    if (!this.state.distance) {
      return false
    }

    if (this.state.distance > 10) {
      return false
    }

    return true
  }
}

// NOTE: Browser stuff, to be removed when packaged!
window.Game = Game
