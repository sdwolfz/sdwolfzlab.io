---
title: "Bakkavor (BAKK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Bakkavor</td></tr>
    <tr><td>Symbol</td><td>BAKK</td></tr>
    <tr><td>Web</td><td><a href="https://www.bakkavor.com">www.bakkavor.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 2.0 |
| 2018 | 6.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
