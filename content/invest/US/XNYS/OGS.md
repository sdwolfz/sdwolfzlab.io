---
title: "ONE GAS INC (OGS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ONE GAS INC</td></tr>
    <tr><td>Symbol</td><td>OGS</td></tr>
    <tr><td>Web</td><td><a href="https://www.onegas.com">www.onegas.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.16 |
| 2020 | 2.16 |
| 2019 | 2.0 |
| 2018 | 1.84 |
| 2017 | 1.68 |
| 2016 | 1.4 |
| 2015 | 1.2 |
| 2014 | 0.84 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
