---
title: "TECHNIPFMC PLC (FTI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>TECHNIPFMC PLC</td></tr>
    <tr><td>Symbol</td><td>FTI</td></tr>
    <tr><td>Web</td><td><a href="https://www.technipfmc.com">www.technipfmc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.13 |
| 2017 | 0.13 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
