---
title: "FRANCO NEVADA CORP (FNV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FRANCO NEVADA CORP</td></tr>
    <tr><td>Symbol</td><td>FNV</td></tr>
    <tr><td>Web</td><td><a href="https://www.franco-nevada.com">www.franco-nevada.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.03 |
| 2019 | 0.99 |
| 2018 | 0.95 |
| 2017 | 0.91 |
| 2016 | 0.87 |
| 2015 | 0.83 |
| 2014 | 0.78 |
| 2013 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
