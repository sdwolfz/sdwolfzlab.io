---
title: "PCB BANCORP (PCB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PCB BANCORP</td></tr>
    <tr><td>Symbol</td><td>PCB</td></tr>
    <tr><td>Web</td><td><a href="https://www.paccitybank.com">www.paccitybank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.4 |
| 2019 | 0.25 |
| 2018 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
