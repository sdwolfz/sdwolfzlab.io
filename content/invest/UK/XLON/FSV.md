---
title: "Fidelity (FSV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Fidelity</td></tr>
    <tr><td>Symbol</td><td>FSV</td></tr>
    <tr><td>Web</td><td><a href="https://www.fidelityinvestmenttrusts.com">www.fidelityinvestmenttrusts.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 5.8 |
| 2019 | 7.25 |
| 2018 | 5.0 |
| 2017 | 4.6 |
| 2016 | 3.7 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
