---
title: " (CSSEP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CSSEP</td></tr>
    <tr><td>Web</td><td><a href="https://www.cssentertainment.com">www.cssentertainment.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.812 |
| 2020 | 2.437 |
| 2019 | 2.437 |
| 2018 | 1.219 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
