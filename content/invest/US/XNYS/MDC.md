---
title: "M.D.C.HLDGS INC (MDC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>M.D.C.HLDGS INC</td></tr>
    <tr><td>Symbol</td><td>MDC</td></tr>
    <tr><td>Web</td><td><a href="https://www.mdcholdings.com">www.mdcholdings.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.77 |
| 2020 | 1.39 |
| 2019 | 1.178 |
| 2018 | 1.2 |
| 2017 | 0.231 |
| 2016 | 0.238 |
| 2012 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
