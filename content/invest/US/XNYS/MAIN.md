---
title: "MAIN STREET CAPITAL CORPORATION (MAIN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MAIN STREET CAPITAL CORPORATION</td></tr>
    <tr><td>Symbol</td><td>MAIN</td></tr>
    <tr><td>Web</td><td><a href="https://www.mainstcapital.com">www.mainstcapital.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.025 |
| 2020 | 2.255 |
| 2019 | 2.425 |
| 2018 | 2.3 |
| 2017 | 2.24 |
| 2016 | 2.0 |
| 2015 | 2.11 |
| 2014 | 2.0 |
| 2013 | 0.645 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
