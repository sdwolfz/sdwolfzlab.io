---
title: "CVB FINANCIAL (CVBF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CVB FINANCIAL</td></tr>
    <tr><td>Symbol</td><td>CVBF</td></tr>
    <tr><td>Web</td><td><a href="https://www.cbbank.com">www.cbbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.18 |
| 2020 | 0.9 |
| 2019 | 0.68 |
| 2018 | 0.56 |
| 2017 | 0.52 |
| 2016 | 0.36 |
| 2015 | 0.48 |
| 2014 | 0.4 |
| 2013 | 0.385 |
| 2012 | 0.425 |
| 2011 | 0.255 |
| 2010 | 0.34 |
| 2009 | 0.34 |
| 2008 | 0.425 |
| 2007 | 0.34 |
| 2006 | 0.383 |
| 2005 | 0.33 |
| 2004 | 0.508 |
| 2003 | 0.63 |
| 2002 | 0.55 |
| 2001 | 0.43 |
| 2000 | 0.48 |
| 1999 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
