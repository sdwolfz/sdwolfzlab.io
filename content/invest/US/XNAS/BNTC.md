---
title: "BENITEC BIOPHARMA INC (BNTC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BENITEC BIOPHARMA INC</td></tr>
    <tr><td>Symbol</td><td>BNTC</td></tr>
    <tr><td>Web</td><td><a href="https://www.benitec.com">www.benitec.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.003 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
