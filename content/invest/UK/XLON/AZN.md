---
title: "Astrazeneca (AZN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Astrazeneca</td></tr>
    <tr><td>Symbol</td><td>AZN</td></tr>
    <tr><td>Web</td><td><a href="https://www.astrazeneca.com">www.astrazeneca.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 280.0 |
| 2019 | 280.0 |
| 2018 | 280.0 |
| 2017 | 280.0 |
| 2016 | 280.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
