---
title: " (IIPR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>IIPR</td></tr>
    <tr><td>Web</td><td><a href="https://www.innovativeindustrialproperties.com">www.innovativeindustrialproperties.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.32 |
| 2020 | 4.47 |
| 2019 | 2.83 |
| 2018 | 1.2 |
| 2017 | 0.55 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
