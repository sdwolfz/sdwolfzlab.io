---
title: " (FRGAP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>FRGAP</td></tr>
    <tr><td>Web</td><td><a href="https://www.franchisegrp.com">www.franchisegrp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.938 |
| 2020 | 0.609 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
