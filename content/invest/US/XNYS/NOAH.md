---
title: " (NOAH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>NOAH</td></tr>
    <tr><td>Web</td><td><a href="https://ir.noahgroup.com">ir.noahgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2013 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
