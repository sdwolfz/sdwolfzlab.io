---
title: "Reach Plc (RCH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Reach Plc</td></tr>
    <tr><td>Symbol</td><td>RCH</td></tr>
    <tr><td>Web</td><td><a href="https://www.reachplc.com">www.reachplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 4.26 |
| 2019 | 6.55 |
| 2018 | 6.14 |
| 2017 | 5.8 |
| 2017 | 5.45 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
