---
title: "Install Ruby gems in home directory"
date: 2020-04-05T15:26:35Z
type: posts
slug: install-ruby-gems-in-home-directory
tags: ["ruby", "gems", "bundler", "bash", "linux", "productivity"]
---

Create the `~/.gemrc` file with the following content:

```
gem: --no-rdoc --no-ri --user-install
```

Add these lines to your `.bash_profile`:

```sh
for entry in ~/.gem/ruby/*; do PATH=$entry/bin:$PATH; done
export PATH

export BUNDLE_PATH=~/.gem/bundle
```

Now you can install gems without needing your root password:

```sh
gem install rspec

which rspec
# /home/me/.gem/ruby/2.7.0/bin/rspec
```

Bundler with a `Gemfile`:

```ruby
source 'https://rubygems.org'

gem 'rspec'
```

You can prefix your commands with `bundle exec`:

```sh
bundle install

bundle exec which rspec
# /home/me/.gem/bundle/ruby/2.7.0/bin/rspec
```

Don't forget to clean up your old ruby gems directories:

```
ls -la /home/me/.gem/ruby
ls -la /home/me/.gem/bundle/ruby
```

Keep only the version matching your latest ruby!
