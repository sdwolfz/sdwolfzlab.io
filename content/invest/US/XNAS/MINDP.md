---
title: " (MINDP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>MINDP</td></tr>
    <tr><td>Web</td><td><a href="https://www.mind-technology.com">www.mind-technology.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.125 |
| 2020 | 2.25 |
| 2019 | 2.25 |
| 2018 | 2.25 |
| 2017 | 2.25 |
| 2016 | 0.563 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
