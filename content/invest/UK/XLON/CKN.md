---
title: "Clarkson (CKN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Clarkson</td></tr>
    <tr><td>Symbol</td><td>CKN</td></tr>
    <tr><td>Web</td><td><a href="https://www.clarksons.co.uk">www.clarksons.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 78.0 |
| 2018 | 75.0 |
| 2017 | 73.0 |
| 2016 | 65.0 |
| 2015 | 62.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
