---
title: " (WAFDP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>WAFDP</td></tr>
    <tr><td>Web</td><td><a href="https://www.wafdbank.com">www.wafdbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.532 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
