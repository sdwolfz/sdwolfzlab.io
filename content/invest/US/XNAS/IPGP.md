---
title: "IPG PHOTONICS CORPORATION (IPGP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>IPG PHOTONICS CORPORATION</td></tr>
    <tr><td>Symbol</td><td>IPGP</td></tr>
    <tr><td>Web</td><td><a href="https://www.ipgphotonics.com">www.ipgphotonics.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2012 | 0.65 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
