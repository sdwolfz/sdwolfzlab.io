---
title: "NEXTGEN HEALTHCARE INC (NXGN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>NEXTGEN HEALTHCARE INC</td></tr>
    <tr><td>Symbol</td><td>NXGN</td></tr>
    <tr><td>Web</td><td><a href="https://www.nextgen.com">www.nextgen.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2015 | 0.7 |
| 2014 | 0.7 |
| 2013 | 0.7 |
| 2012 | 0.7 |
| 2011 | 1.225 |
| 2010 | 1.2 |
| 2009 | 1.2 |
| 2008 | 1.1 |
| 2007 | 1.75 |
| 2006 | 1.75 |
| 2005 | 3.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
