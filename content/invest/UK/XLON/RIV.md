---
title: "River Mer (RIV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>River Mer</td></tr>
    <tr><td>Symbol</td><td>RIV</td></tr>
    <tr><td>Web</td><td><a href="https://www.riverandmercantile.com">www.riverandmercantile.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 16.4 |
| 2018 | 18.6 |
| 2017 | 19.7 |
| 2016 | 9.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
