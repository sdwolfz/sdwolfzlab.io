---
title: "HEARTLAND EXPRESS INC (HTLD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HEARTLAND EXPRESS INC</td></tr>
    <tr><td>Symbol</td><td>HTLD</td></tr>
    <tr><td>Web</td><td><a href="https://www.heartlandexpress.com">www.heartlandexpress.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.02 |
| 2020 | 0.08 |
| 2019 | 0.08 |
| 2018 | 0.08 |
| 2017 | 0.08 |
| 2016 | 0.08 |
| 2015 | 0.08 |
| 2014 | 0.08 |
| 2013 | 0.08 |
| 2012 | 1.08 |
| 2011 | 0.08 |
| 2010 | 1.08 |
| 2009 | 0.08 |
| 2008 | 0.08 |
| 2007 | 2.08 |
| 2006 | 0.08 |
| 2005 | 0.08 |
| 2004 | 0.08 |
| 2003 | 0.04 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
