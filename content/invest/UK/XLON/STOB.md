---
title: "STOBART GROUP LD ORD 10P (STOB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/lse/">LSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/lse/">XLON</a></td></tr>
    <tr><td>Name</td><td>STOBART GROUP LD ORD 10P</td></tr>
    <tr><td>Symbol</td><td>STOB</td></tr>
    <tr><td>Web</td><td><a href="http://www.stobartgroup.co.uk">www.stobartgroup.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 15.0 |
| 2018 | 16.5 |
| 2017 | 10.0 |
| 2016 | 6.0 |
| 2015 | 6.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
