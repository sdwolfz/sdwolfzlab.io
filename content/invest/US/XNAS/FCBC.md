---
title: "FIRST COMMUNITY BANKSHARES INC (FCBC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FIRST COMMUNITY BANKSHARES INC</td></tr>
    <tr><td>Symbol</td><td>FCBC</td></tr>
    <tr><td>Web</td><td><a href="https://www.firstcommunitybank.com">www.firstcommunitybank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.5 |
| 2020 | 1.0 |
| 2019 | 0.96 |
| 2018 | 1.26 |
| 2017 | 0.68 |
| 2016 | 0.6 |
| 2015 | 0.54 |
| 2014 | 0.5 |
| 2013 | 0.48 |
| 2012 | 0.43 |
| 2011 | 0.4 |
| 2010 | 0.4 |
| 2009 | 0.3 |
| 2008 | 1.12 |
| 2007 | 1.08 |
| 2006 | 1.04 |
| 2005 | 1.02 |
| 2004 | 1.0 |
| 2003 | 1.02 |
| 2002 | 1.0 |
| 2001 | 0.98 |
| 2000 | 0.95 |
| 1999 | 0.93 |
| 1998 | 0.302 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
