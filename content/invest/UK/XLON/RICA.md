---
title: "Ruffer Inv. Co. (RICA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Ruffer Inv. Co.</td></tr>
    <tr><td>Symbol</td><td>RICA</td></tr>
    <tr><td>Web</td><td><a href="https://www.ruffer.co.uk">www.ruffer.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 185.0 |
| 2019 | 180.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
