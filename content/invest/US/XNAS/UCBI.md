---
title: "UNITED COMMUNITY BANKS(GEORGIA) (UCBI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>UNITED COMMUNITY BANKS(GEORGIA)</td></tr>
    <tr><td>Symbol</td><td>UCBI</td></tr>
    <tr><td>Web</td><td><a href="https://www.ucbi.com">www.ucbi.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.19 |
| 2020 | 0.72 |
| 2019 | 0.68 |
| 2018 | 0.58 |
| 2017 | 0.38 |
| 2016 | 0.3 |
| 2015 | 0.22 |
| 2014 | 0.11 |
| 2008 | 0.18 |
| 2007 | 0.36 |
| 2006 | 0.32 |
| 2005 | 0.28 |
| 2004 | 0.27 |
| 2003 | 0.3 |
| 2002 | 0.313 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
