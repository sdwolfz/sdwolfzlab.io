---
title: "IMI (IMI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>IMI</td></tr>
    <tr><td>Symbol</td><td>IMI</td></tr>
    <tr><td>Web</td><td><a href="https://www.imiplc.com">www.imiplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 41.1 |
| 2018 | 40.6 |
| 2017 | 39.4 |
| 2016 | 38.7 |
| 2015 | 38.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
