---
title: "Discoverie Grp. (DSCV)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Discoverie Grp.</td></tr>
    <tr><td>Symbol</td><td>DSCV</td></tr>
    <tr><td>Web</td><td><a href="https://www.discoverieplc.com">www.discoverieplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.97 |
| 2019 | 9.55 |
| 2018 | 9.0 |
| 2017 | 8.5 |
| 2016 | 8.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
