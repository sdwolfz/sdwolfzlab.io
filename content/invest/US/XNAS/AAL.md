---
title: "AMERICAN AIRLINES GROUP INC (AAL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>AMERICAN AIRLINES GROUP INC</td></tr>
    <tr><td>Symbol</td><td>AAL</td></tr>
    <tr><td>Web</td><td><a href="https://www.aa.com">www.aa.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.1 |
| 2019 | 0.4 |
| 2018 | 0.4 |
| 2017 | 0.4 |
| 2016 | 0.4 |
| 2015 | 0.4 |
| 2014 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
