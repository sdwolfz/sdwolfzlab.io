---
title: "Wood Group (J) (WG.)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Wood Group (J)</td></tr>
    <tr><td>Symbol</td><td>WG.</td></tr>
    <tr><td>Web</td><td><a href="https://www.woodplc.com">www.woodplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 35.3 |
| 2018 | 35.0 |
| 2017 | 34.3 |
| 2016 | 33.3 |
| 2015 | 30.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
