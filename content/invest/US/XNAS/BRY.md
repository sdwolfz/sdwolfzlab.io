---
title: "BERRY CORPORATION (BRY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BERRY CORPORATION</td></tr>
    <tr><td>Symbol</td><td>BRY</td></tr>
    <tr><td>Web</td><td><a href="https://www.bry.com">www.bry.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.08 |
| 2020 | 0.12 |
| 2019 | 0.48 |
| 2018 | 0.21 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
