---
title: "MINEXFOR SA DEVA (MINX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>MINEXFOR SA DEVA</td></tr>
    <tr><td>Symbol</td><td>MINX</td></tr>
    <tr><td>Web</td><td><a href="https://www.perlaapusenilor.ro">www.perlaapusenilor.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2003 | 0.2055 |
| 2002 | 0.1184 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
