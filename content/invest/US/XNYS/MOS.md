---
title: "MOSAIC CO (MOS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MOSAIC CO</td></tr>
    <tr><td>Symbol</td><td>MOS</td></tr>
    <tr><td>Web</td><td><a href="https://www.mosaicco.com">www.mosaicco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.05 |
| 2020 | 0.2 |
| 2019 | 0.175 |
| 2018 | 0.1 |
| 2017 | 0.6 |
| 2016 | 1.1 |
| 2015 | 1.075 |
| 2014 | 1.0 |
| 2013 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
