---
title: "CDW CORP (CDW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CDW CORP</td></tr>
    <tr><td>Symbol</td><td>CDW</td></tr>
    <tr><td>Web</td><td><a href="https://www.cdw.com">www.cdw.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.8 |
| 2020 | 1.54 |
| 2019 | 1.265 |
| 2018 | 0.925 |
| 2017 | 0.69 |
| 2016 | 0.483 |
| 2015 | 0.31 |
| 2014 | 0.195 |
| 2013 | 0.043 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
