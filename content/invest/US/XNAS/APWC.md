---
title: "ASIA PACIFIC WIRE & CABLE (APWC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ASIA PACIFIC WIRE & CABLE</td></tr>
    <tr><td>Symbol</td><td>APWC</td></tr>
    <tr><td>Web</td><td><a href="https://www.apwcc.com">www.apwcc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.08 |
| 2017 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
