---
title: "ANDERSONS INC (ANDE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ANDERSONS INC</td></tr>
    <tr><td>Symbol</td><td>ANDE</td></tr>
    <tr><td>Web</td><td><a href="https://www.andersonsinc.com">www.andersonsinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.175 |
| 2020 | 0.7 |
| 2019 | 0.685 |
| 2018 | 0.665 |
| 2017 | 0.645 |
| 2016 | 0.625 |
| 2015 | 0.575 |
| 2014 | 0.47 |
| 2013 | 0.645 |
| 2012 | 0.61 |
| 2011 | 0.48 |
| 2010 | 0.38 |
| 2009 | 0.35 |
| 2008 | 0.333 |
| 2007 | 0.25 |
| 2006 | 0.183 |
| 2005 | 0.335 |
| 2004 | 0.31 |
| 2003 | 0.285 |
| 2002 | 0.265 |
| 2001 | 0.26 |
| 2000 | 0.245 |
| 1999 | 0.21 |
| 1998 | 0.17 |
| 1997 | 0.13 |
| 1996 | 0.03 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
