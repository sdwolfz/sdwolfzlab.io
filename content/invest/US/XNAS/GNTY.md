---
title: "GUARANTY BANCSHARES INC TEX (GNTY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GUARANTY BANCSHARES INC TEX</td></tr>
    <tr><td>Symbol</td><td>GNTY</td></tr>
    <tr><td>Web</td><td><a href="https://www.gnty.com">www.gnty.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.78 |
| 2019 | 0.7 |
| 2018 | 0.6 |
| 2017 | 0.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
