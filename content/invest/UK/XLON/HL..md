---
title: "Hargreaves Lansdown (HL.)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Hargreaves Lansdown</td></tr>
    <tr><td>Symbol</td><td>HL.</td></tr>
    <tr><td>Web</td><td><a href="https://www.hl.co.uk">www.hl.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 54.9 |
| 2019 | 42.0 |
| 2018 | 40.0 |
| 2017 | 26.2 |
| 2016 | 34.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
