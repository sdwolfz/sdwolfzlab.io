---
title: "WSFS FINANCIAL CORP (WSFS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>WSFS FINANCIAL CORP</td></tr>
    <tr><td>Symbol</td><td>WSFS</td></tr>
    <tr><td>Web</td><td><a href="https://www.wsfsbank.com">www.wsfsbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 0.48 |
| 2019 | 0.47 |
| 2018 | 0.42 |
| 2017 | 0.3 |
| 2016 | 0.25 |
| 2015 | 0.41 |
| 2014 | 0.51 |
| 2013 | 0.48 |
| 2012 | 0.48 |
| 2011 | 0.48 |
| 2010 | 0.48 |
| 2009 | 0.48 |
| 2008 | 0.46 |
| 2007 | 0.38 |
| 2006 | 0.31 |
| 2005 | 0.27 |
| 2004 | 0.23 |
| 2003 | 0.2 |
| 2002 | 0.19 |
| 2001 | 0.16 |
| 2000 | 0.15 |
| 1999 | 0.12 |
| 1998 | 0.09 |
| 1990 | 0.1 |
| 1989 | 0.4 |
| 1988 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
