---
title: "GENCO SHIPPING & TRADING LIMITED (GNK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GENCO SHIPPING & TRADING LIMITED</td></tr>
    <tr><td>Symbol</td><td>GNK</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.07 |
| 2020 | 0.235 |
| 2019 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
