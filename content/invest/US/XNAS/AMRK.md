---
title: "A MARK PRECIOUS METALS INC (AMRK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>A MARK PRECIOUS METALS INC</td></tr>
    <tr><td>Symbol</td><td>AMRK</td></tr>
    <tr><td>Web</td><td><a href="https://www.amark.com">www.amark.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 3.0 |
| 2018 | 0.08 |
| 2017 | 0.32 |
| 2016 | 0.28 |
| 2015 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
