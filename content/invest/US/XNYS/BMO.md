---
title: "BANK OF MONTREAL (BMO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BANK OF MONTREAL</td></tr>
    <tr><td>Symbol</td><td>BMO</td></tr>
    <tr><td>Web</td><td><a href="https://www.bmo.com">www.bmo.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.832 |
| 2020 | 3.147 |
| 2019 | 3.072 |
| 2018 | 2.925 |
| 2017 | 3.432 |
| 2016 | 2.573 |
| 2015 | 2.073 |
| 2014 | 3.08 |
| 2013 | 1.48 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
