---
title: "Headlam (HEAD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Headlam</td></tr>
    <tr><td>Symbol</td><td>HEAD</td></tr>
    <tr><td>Web</td><td><a href="https://www.headlam.com">www.headlam.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 25.0 |
| 2018 | 25.0 |
| 2017 | 24.8 |
| 2016 | 26.7 |
| 2015 | 17.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
