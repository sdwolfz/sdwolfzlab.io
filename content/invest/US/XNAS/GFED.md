---
title: "GUARANTY FEDERAL BANCSHARES INC (GFED)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GUARANTY FEDERAL BANCSHARES INC</td></tr>
    <tr><td>Symbol</td><td>GFED</td></tr>
    <tr><td>Web</td><td><a href="https://www.gbankmo.com">www.gbankmo.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 0.6 |
| 2019 | 0.52 |
| 2018 | 0.48 |
| 2017 | 0.3 |
| 2016 | 0.34 |
| 2015 | 0.23 |
| 2014 | 0.15 |
| 2008 | 0.54 |
| 2007 | 0.69 |
| 2006 | 0.665 |
| 2005 | 0.645 |
| 2004 | 0.625 |
| 2003 | 0.605 |
| 2002 | 0.525 |
| 2001 | 0.5 |
| 2000 | 0.45 |
| 1999 | 0.38 |
| 1998 | 0.31 |
| 1997 | 0.42 |
| 1996 | 0.34 |
| 1995 | 0.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
