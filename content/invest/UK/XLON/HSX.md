---
title: "Hiscox (HSX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Hiscox</td></tr>
    <tr><td>Symbol</td><td>HSX</td></tr>
    <tr><td>Web</td><td><a href="https://www.hiscoxgroup.com">www.hiscoxgroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 43.35 |
| 2018 | 41.85 |
| 2017 | 29.0 |
| 2016 | 27.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
