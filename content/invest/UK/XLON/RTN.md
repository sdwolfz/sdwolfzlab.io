---
title: "Restaurant Gp (RTN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Restaurant Gp</td></tr>
    <tr><td>Symbol</td><td>RTN</td></tr>
    <tr><td>Web</td><td><a href="https://www.trgplc.com">www.trgplc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 2.1 |
| 2018 | 8.27 |
| 2017 | 17.4 |
| 2017 | 17.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
