---
title: "LIMONEIRA CO (LMNR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LIMONEIRA CO</td></tr>
    <tr><td>Symbol</td><td>LMNR</td></tr>
    <tr><td>Web</td><td><a href="https://www.limoneira.com">www.limoneira.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.075 |
| 2020 | 0.3 |
| 2019 | 0.3 |
| 2018 | 0.263 |
| 2017 | 0.227 |
| 2016 | 0.205 |
| 2015 | 0.185 |
| 2014 | 0.173 |
| 2013 | 0.15 |
| 2012 | 0.138 |
| 2011 | 0.125 |
| 2010 | 0.094 |
| 2009 | 0.313 |
| 2008 | 2.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
