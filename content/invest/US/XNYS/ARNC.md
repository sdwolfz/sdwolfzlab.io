---
title: "ARCONIC CORPORATION (ARNC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ARCONIC CORPORATION</td></tr>
    <tr><td>Symbol</td><td>ARNC</td></tr>
    <tr><td>Web</td><td><a href="https://www.arconic.com">www.arconic.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.02 |
| 2019 | 0.12 |
| 2018 | 0.24 |
| 2017 | 0.24 |
| 2016 | 0.09 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
