---
title: "Just Group (JUST)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Just Group</td></tr>
    <tr><td>Symbol</td><td>JUST</td></tr>
    <tr><td>Web</td><td><a href="https://www.justgroupplc.co.uk">www.justgroupplc.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 2.55 |
| 2017 | 3.72 |
| 2016 | 4.6 |
| 2015 | 3.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
