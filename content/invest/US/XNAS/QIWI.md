---
title: " (QIWI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>QIWI</td></tr>
    <tr><td>Web</td><td><a href="https://www.qiwi.com">www.qiwi.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.31 |
| 2020 | 1.03 |
| 2019 | 0.84 |
| 2017 | 0.62 |
| 2016 | 1.16 |
| 2015 | 0.25 |
| 2014 | 1.45 |
| 2013 | 0.93 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
