---
title: "Invesco Perp Uk (IPU)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Invesco Perp Uk</td></tr>
    <tr><td>Symbol</td><td>IPU</td></tr>
    <tr><td>Web</td><td><a href="https://www.invescoperpetual.co.uk">www.invescoperpetual.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 18.6 |
| 2018 | 20.8 |
| 2017 | 17.1 |
| 2016 | 14.3 |
| 2015 | 13.75 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
