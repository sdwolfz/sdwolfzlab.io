---
title: "BROOKS AUTOMATION INC (BRKS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BROOKS AUTOMATION INC</td></tr>
    <tr><td>Symbol</td><td>BRKS</td></tr>
    <tr><td>Web</td><td><a href="https://www.brooks.com">www.brooks.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.4 |
| 2019 | 0.4 |
| 2018 | 0.4 |
| 2017 | 0.4 |
| 2016 | 0.4 |
| 2015 | 0.4 |
| 2014 | 0.36 |
| 2013 | 0.32 |
| 2012 | 0.32 |
| 2011 | 0.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
