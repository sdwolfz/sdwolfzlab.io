---
title: "MSA SAFETY INC (MSA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MSA SAFETY INC</td></tr>
    <tr><td>Symbol</td><td>MSA</td></tr>
    <tr><td>Web</td><td><a href="https://www.msasafety.com">www.msasafety.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.87 |
| 2020 | 1.71 |
| 2019 | 1.64 |
| 2018 | 1.49 |
| 2017 | 1.38 |
| 2016 | 1.31 |
| 2015 | 1.27 |
| 2014 | 1.23 |
| 2013 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
