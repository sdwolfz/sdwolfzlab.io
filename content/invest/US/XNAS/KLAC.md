---
title: "KLA CORPORATION (KLAC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>KLA CORPORATION</td></tr>
    <tr><td>Symbol</td><td>KLAC</td></tr>
    <tr><td>Web</td><td><a href="https://www.kla.com">www.kla.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.9 |
| 2020 | 3.5 |
| 2019 | 3.1 |
| 2018 | 2.84 |
| 2017 | 2.26 |
| 2016 | 2.1 |
| 2015 | 2.04 |
| 2014 | 18.4 |
| 2013 | 1.7 |
| 2012 | 1.5 |
| 2011 | 1.2 |
| 2010 | 0.8 |
| 2009 | 0.6 |
| 2008 | 0.6 |
| 2007 | 0.54 |
| 2006 | 0.48 |
| 2005 | 0.36 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
