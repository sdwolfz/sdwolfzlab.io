---
title: " (CHT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>CHT</td></tr>
    <tr><td>Web</td><td><a href="https://www.cht.com.tw">www.cht.com.tw</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.114 |
| 2019 | 1.105 |
| 2018 | 1.211 |
| 2017 | 1.29 |
| 2016 | 1.364 |
| 2015 | 1.165 |
| 2014 | 0.616 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
