---
title: "COGNEX CORP (CGNX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COGNEX CORP</td></tr>
    <tr><td>Symbol</td><td>CGNX</td></tr>
    <tr><td>Web</td><td><a href="https://www.cognex.com">www.cognex.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 2.225 |
| 2019 | 0.205 |
| 2018 | 0.185 |
| 2017 | 0.335 |
| 2016 | 0.295 |
| 2015 | 0.21 |
| 2012 | 1.54 |
| 2011 | 0.36 |
| 2010 | 0.25 |
| 2009 | 0.3 |
| 2008 | 0.47 |
| 2007 | 0.34 |
| 2006 | 0.33 |
| 2005 | 0.32 |
| 2004 | 0.28 |
| 2003 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
