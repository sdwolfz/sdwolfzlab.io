---
title: "BLACK KNIGHT INC (BKI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>BLACK KNIGHT INC</td></tr>
    <tr><td>Symbol</td><td>BKI</td></tr>
    <tr><td>Web</td><td><a href="https://www.blackknightinc.com">www.blackknightinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2013 | 0.09 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
