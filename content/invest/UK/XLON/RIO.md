---
title: "Rio Tinto (RIO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Rio Tinto</td></tr>
    <tr><td>Symbol</td><td>RIO</td></tr>
    <tr><td>Web</td><td><a href="https://www.riotinto.com">www.riotinto.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 408.37 |
| 2019 | 492.65 |
| 2018 | 226.25 |
| 2017 | 212.56 |
| 2016 | 134.36 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
