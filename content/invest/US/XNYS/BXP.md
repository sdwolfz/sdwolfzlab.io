---
title: " (BXP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>BXP</td></tr>
    <tr><td>Web</td><td><a href="https://www.bxp.com">www.bxp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.98 |
| 2020 | 3.92 |
| 2019 | 3.83 |
| 2018 | 3.5 |
| 2017 | 3.05 |
| 2016 | 2.7 |
| 2015 | 3.85 |
| 2014 | 2.6 |
| 2013 | 1.95 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
