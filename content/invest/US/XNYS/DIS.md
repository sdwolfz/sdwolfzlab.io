---
title: "WALT DISNEY COMPANY (THE) (DIS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>WALT DISNEY COMPANY (THE)</td></tr>
    <tr><td>Symbol</td><td>DIS</td></tr>
    <tr><td>Web</td><td><a href="https://www.thewaltdisneycompany.com">www.thewaltdisneycompany.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 1.76 |
| 2018 | 1.72 |
| 2017 | 1.62 |
| 2016 | 1.49 |
| 2015 | 1.37 |
| 2014 | 1.15 |
| 2013 | 0.86 |
| 2012 | 0.75 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
