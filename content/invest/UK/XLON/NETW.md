---
title: "Network Intl (NETW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Network Intl</td></tr>
    <tr><td>Symbol</td><td>NETW</td></tr>
    <tr><td>Web</td><td><a href="https://www.network.ae">www.network.ae</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 3.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
