---
title: " (MTBCP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>MTBCP</td></tr>
    <tr><td>Web</td><td><a href="https://www.carecloud.com">www.carecloud.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.146 |
| 2020 | 2.75 |
| 2019 | 2.75 |
| 2018 | 2.75 |
| 2017 | 2.75 |
| 2016 | 2.75 |
| 2015 | 0.435 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
