---
title: "GRIFFON CORP (GFF)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GRIFFON CORP</td></tr>
    <tr><td>Symbol</td><td>GFF</td></tr>
    <tr><td>Web</td><td><a href="https://www.griffon.com">www.griffon.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.16 |
| 2020 | 0.305 |
| 2019 | 0.291 |
| 2018 | 1.282 |
| 2017 | 0.25 |
| 2016 | 0.21 |
| 2015 | 0.17 |
| 2014 | 0.13 |
| 2013 | 0.055 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
