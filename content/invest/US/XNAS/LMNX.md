---
title: "LUMINEX CORP(DEL) (LMNX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LUMINEX CORP(DEL)</td></tr>
    <tr><td>Symbol</td><td>LMNX</td></tr>
    <tr><td>Web</td><td><a href="https://www.luminexcorp.com">www.luminexcorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.37 |
| 2019 | 0.3 |
| 2018 | 0.24 |
| 2017 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
