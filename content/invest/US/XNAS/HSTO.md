---
title: "HISTOGEN INC (HSTO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HISTOGEN INC</td></tr>
    <tr><td>Symbol</td><td>HSTO</td></tr>
    <tr><td>Web</td><td><a href="https://www.histogen.com">www.histogen.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
