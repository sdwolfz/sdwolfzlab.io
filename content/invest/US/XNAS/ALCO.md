---
title: "ALICO INC (ALCO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ALICO INC</td></tr>
    <tr><td>Symbol</td><td>ALCO</td></tr>
    <tr><td>Web</td><td><a href="https://www.alicoinc.com">www.alicoinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.18 |
| 2020 | 0.45 |
| 2019 | 0.27 |
| 2018 | 0.24 |
| 2017 | 0.24 |
| 2016 | 0.24 |
| 2015 | 0.24 |
| 2014 | 0.3 |
| 2013 | 0.36 |
| 2012 | 0.2 |
| 2011 | 0.16 |
| 2010 | 0.1 |
| 2009 | 0.688 |
| 2008 | 0.825 |
| 2007 | 1.1 |
| 2006 | 1.05 |
| 2005 | 1.5 |
| 2003 | 0.6 |
| 2002 | 0.35 |
| 2001 | 1.0 |
| 2000 | 1.0 |
| 1999 | 0.3 |
| 1998 | 0.5 |
| 1997 | 0.6 |
| 1996 | 0.15 |
| 1995 | 0.35 |
| 1994 | 0.25 |
| 1993 | 0.15 |
| 1992 | 0.15 |
| 1991 | 0.15 |
| 1990 | 0.15 |
| 1989 | 0.2 |
| 1988 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
