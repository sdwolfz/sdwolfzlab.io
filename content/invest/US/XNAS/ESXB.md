---
title: "COMMUNITY BANKERS TRUST CORPORATION (ESXB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COMMUNITY BANKERS TRUST CORPORATION</td></tr>
    <tr><td>Symbol</td><td>ESXB</td></tr>
    <tr><td>Web</td><td><a href="https://www.cbtrustcorp.com">www.cbtrustcorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 0.21 |
| 2019 | 0.13 |
| 2010 | 0.04 |
| 2009 | 0.16 |
| 2008 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
