---
title: "CSW INDUSTRIALS INC (CSWI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CSW INDUSTRIALS INC</td></tr>
    <tr><td>Symbol</td><td>CSWI</td></tr>
    <tr><td>Web</td><td><a href="https://www.cswindustrials.com">www.cswindustrials.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.285 |
| 2020 | 0.54 |
| 2019 | 0.405 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
