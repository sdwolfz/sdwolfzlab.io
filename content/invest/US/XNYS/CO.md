---
title: "GLOBAL CORD BLOOD CORPORATION (CO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GLOBAL CORD BLOOD CORPORATION</td></tr>
    <tr><td>Symbol</td><td>CO</td></tr>
    <tr><td>Web</td><td><a href="https://www.globalcordbloodcorp.com">www.globalcordbloodcorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
