---
title: "Bloomsbury (BMY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Bloomsbury</td></tr>
    <tr><td>Symbol</td><td>BMY</td></tr>
    <tr><td>Web</td><td><a href="https://www.bloomsbury-ir.co.uk">www.bloomsbury-ir.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 8.17 |
| 2019 | 7.96 |
| 2018 | 7.51 |
| 2017 | 6.7 |
| 2016 | 6.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
