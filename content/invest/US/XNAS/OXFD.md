---
title: "OXFORD IMMUNOTEC GLOBAL PLC (OXFD)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>OXFORD IMMUNOTEC GLOBAL PLC</td></tr>
    <tr><td>Symbol</td><td>OXFD</td></tr>
    <tr><td>Web</td><td><a href="http://www.oxfordimmunotec.com">www.oxfordimmunotec.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
