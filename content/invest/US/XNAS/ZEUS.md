---
title: "OLYMPIC STEEL INC (ZEUS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>OLYMPIC STEEL INC</td></tr>
    <tr><td>Symbol</td><td>ZEUS</td></tr>
    <tr><td>Web</td><td><a href="https://www.olysteel.com">www.olysteel.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.04 |
| 2020 | 0.08 |
| 2019 | 0.08 |
| 2018 | 0.08 |
| 2017 | 0.08 |
| 2016 | 0.08 |
| 2015 | 0.08 |
| 2014 | 0.08 |
| 2013 | 0.08 |
| 2012 | 0.08 |
| 2011 | 0.08 |
| 2010 | 0.08 |
| 2009 | 0.11 |
| 2008 | 1.18 |
| 2007 | 0.14 |
| 2006 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
