---
title: "GREAT WESTERN BANCORP INC (GWB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>GREAT WESTERN BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>GWB</td></tr>
    <tr><td>Web</td><td><a href="https://www.greatwesternbank.com">www.greatwesternbank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.02 |
| 2020 | 0.47 |
| 2019 | 1.15 |
| 2018 | 0.95 |
| 2017 | 0.77 |
| 2016 | 0.59 |
| 2015 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
