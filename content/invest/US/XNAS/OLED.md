---
title: "UNIVERSAL DISPLAY CORP (OLED)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>UNIVERSAL DISPLAY CORP</td></tr>
    <tr><td>Symbol</td><td>OLED</td></tr>
    <tr><td>Web</td><td><a href="https://www.oled.com">www.oled.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.2 |
| 2020 | 0.6 |
| 2019 | 0.4 |
| 2018 | 0.24 |
| 2017 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
