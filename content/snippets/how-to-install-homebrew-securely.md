---
title: "How to install Homebrew securely"
date: 2022-09-17T12:18:08Z
type: posts
slug: install-homebrew-securely
series: ["Homebrew"]
tags: ["linux", "bash", "development", "security", "productivity"]
---

The instructions at https://brew.sh/ are extremely dangerous! Here is how to
install `homebrew` properly.

> NOTE: we assume your homebrew path is: `~/Workspace/Hub/brew`

## Code

1. Clone the repository:

    ```sh
    git clone https://github.com/Homebrew/brew.git ~/Workspace/Hub/brew

    cd ~/Workspace/Hub/brew
    ```

2. Review the code carefully!

3. Check the commit sha matches the one from the last commit on the repository
   website:

    ```sh
    git rev-parse HEAD
    ```

4. Now you have a secure copy of the repository and you can automate the
   process.

## Automation

> IMPORTANT: on Linux, `homebrew` adds itself in the front of your PATH, so any
> package installed by `homebrew` will have higher priority than your system
> packages. This includes critical ones like `pkg-config`, which means you will
> start seeing errors when compiling, unless you duplicate all your compile time
> or runtime libraries by installling them with the `brew` command.
>
> To avoid this, we will only enable homebrew manually in the shell we need it.

1. Add the following setup to your `~/.bash_profile` to allow enabling
   `homebrew` on demand:

    ```sh
    #-------------------------------------------------------------------------------
    # Homebrew
    #-------------------------------------------------------------------------------
    export HOMEBREW_NO_ANALYTICS=1

    function Enable {
      case "$1" in
        "brew")
          eval "$(~/Workspace/Hub/brew/bin/brew shellenv)"
          shift
          ;;
        *)
          return
          ;;
      esac
    }
    ```

2. Set up your current shell by executing this in your homebrew directory:

    ```sh
    eval `bin/brew shellenv`
    ```

3. Any future shells can have homebrew by executing this command:

    ```sh
    Enable brew
    ```

4. Ensure analytics are disabled at the command level:

    ```sh
    brew analytics off
    ```

5. Update everything:

    ```sh
    brew update --force
    brew upgrade
    ```

## Usage

Remember, you need to enable it first before running any commands:

```sh
Enable brew
```

Now you can install any package with a clear conscience.

```sh
brew install hello
```
