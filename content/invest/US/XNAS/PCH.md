---
title: " (PCH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>PCH</td></tr>
    <tr><td>Web</td><td><a href="https://www.potlatchdeltic.com">www.potlatchdeltic.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.41 |
| 2020 | 1.61 |
| 2019 | 1.6 |
| 2018 | 5.14 |
| 2017 | 1.525 |
| 2016 | 1.5 |
| 2015 | 1.5 |
| 2014 | 1.425 |
| 2013 | 1.28 |
| 2012 | 1.24 |
| 2011 | 1.84 |
| 2010 | 2.04 |
| 2009 | 2.04 |
| 2008 | 2.04 |
| 2007 | 1.98 |
| 2006 | 17.27 |
| 2005 | 0.6 |
| 2004 | 3.1 |
| 2003 | 0.6 |
| 2002 | 0.583 |
| 2001 | 1.17 |
| 2000 | 0.87 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
