---
title: " (WTFCM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>WTFCM</td></tr>
    <tr><td>Web</td><td><a href="https://www.wintrust.com">www.wintrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.82 |
| 2020 | 1.64 |
| 2019 | 1.64 |
| 2018 | 1.64 |
| 2017 | 1.64 |
| 2016 | 1.64 |
| 2015 | 0.91 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
