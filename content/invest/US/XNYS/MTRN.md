---
title: "MATERION CORP (MTRN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MATERION CORP</td></tr>
    <tr><td>Symbol</td><td>MTRN</td></tr>
    <tr><td>Web</td><td><a href="https://www.materion.com">www.materion.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.115 |
| 2020 | 0.455 |
| 2019 | 0.435 |
| 2018 | 0.415 |
| 2017 | 0.395 |
| 2016 | 0.375 |
| 2015 | 0.355 |
| 2014 | 0.335 |
| 2013 | 0.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
