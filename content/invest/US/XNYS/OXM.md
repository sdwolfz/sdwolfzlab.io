---
title: "OXFORD INDUSTRIES INC (OXM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>OXFORD INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>OXM</td></tr>
    <tr><td>Web</td><td><a href="https://www.oxfordinc.com">www.oxfordinc.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.62 |
| 2020 | 1.12 |
| 2019 | 1.45 |
| 2018 | 1.29 |
| 2017 | 1.08 |
| 2016 | 1.06 |
| 2015 | 0.96 |
| 2014 | 0.81 |
| 2013 | 0.36 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
