---
title: "FORRESTER RESEARCH INC (FORR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>FORRESTER RESEARCH INC</td></tr>
    <tr><td>Symbol</td><td>FORR</td></tr>
    <tr><td>Web</td><td><a href="https://www.forrester.com">www.forrester.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.8 |
| 2017 | 0.76 |
| 2016 | 0.72 |
| 2015 | 0.68 |
| 2014 | 0.64 |
| 2013 | 0.6 |
| 2012 | 0.56 |
| 2010 | 3.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
