---
title: "TRAVELCENTERS OF AMERICA INC (TA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TRAVELCENTERS OF AMERICA INC</td></tr>
    <tr><td>Symbol</td><td>TA</td></tr>
    <tr><td>Web</td><td><a href="https://www.ta-petro.com">www.ta-petro.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
