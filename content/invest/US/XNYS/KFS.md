---
title: "KINGSWAY FINANCIAL SERVICES(US) (KFS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>KINGSWAY FINANCIAL SERVICES(US)</td></tr>
    <tr><td>Symbol</td><td>KFS</td></tr>
    <tr><td>Web</td><td><a href="https://www.kingsway-financial.com">www.kingsway-financial.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
