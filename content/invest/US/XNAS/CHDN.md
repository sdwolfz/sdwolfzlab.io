---
title: "CHURCHILL DOWNS INC (CHDN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CHURCHILL DOWNS INC</td></tr>
    <tr><td>Symbol</td><td>CHDN</td></tr>
    <tr><td>Web</td><td><a href="https://www.churchilldownsincorporated.com">www.churchilldownsincorporated.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.622 |
| 2019 | 0.581 |
| 2018 | 1.63 |
| 2017 | 1.52 |
| 2016 | 1.32 |
| 2015 | 1.15 |
| 2014 | 1.0 |
| 2013 | 0.87 |
| 2012 | 0.72 |
| 2011 | 0.6 |
| 2010 | 0.5 |
| 2009 | 0.5 |
| 2008 | 0.5 |
| 2007 | 0.5 |
| 2006 | 0.5 |
| 2005 | 0.5 |
| 2004 | 0.5 |
| 2003 | 0.5 |
| 2002 | 0.5 |
| 2001 | 0.5 |
| 2000 | 0.5 |
| 1999 | 0.5 |
| 1998 | 0.5 |
| 1997 | 1.0 |
| 1996 | 0.65 |
| 1995 | 0.5 |
| 1994 | 0.5 |
| 1993 | 0.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
