---
title: "GASTRONOM SA BUZAU (GAOY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>GASTRONOM SA BUZAU</td></tr>
    <tr><td>Symbol</td><td>GAOY</td></tr>
    <tr><td>Web</td><td><a href="https://www.gastronombuzau.ro">www.gastronombuzau.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.65 |
| 2019 | 8.3 |
| 2018 | 5.09 |
| 2017 | 3.12 |
| 2016 | 1.9 |
| 2015 | 10.5 |
| 2014 | 2.0 |
| 2013 | 2.5 |
| 2012 | 2.8 |
| 2010 | 1.5 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
