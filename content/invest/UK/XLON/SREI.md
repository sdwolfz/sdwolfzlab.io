---
title: "Schroder Real (SREI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Schroder Real</td></tr>
    <tr><td>Symbol</td><td>SREI</td></tr>
    <tr><td>Web</td><td><a href="https://www.srei.co.uk">www.srei.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.72 |
| 2019 | 2.53 |
| 2018 | 2.48 |
| 2017 | 2.48 |
| 2016 | 2.48 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
