---
title: "ALTRA INDUSTRIAL MOTION CORP (AIMC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ALTRA INDUSTRIAL MOTION CORP</td></tr>
    <tr><td>Symbol</td><td>AIMC</td></tr>
    <tr><td>Web</td><td><a href="https://www.altramotion.com">www.altramotion.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.14 |
| 2020 | 0.31 |
| 2019 | 0.68 |
| 2018 | 0.68 |
| 2017 | 0.66 |
| 2016 | 0.6 |
| 2015 | 0.57 |
| 2014 | 0.46 |
| 2013 | 0.38 |
| 2012 | 0.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
