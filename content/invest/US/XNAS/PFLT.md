---
title: "PENNANTPARK FLOATING RATE CAP LTD (PFLT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PENNANTPARK FLOATING RATE CAP LTD</td></tr>
    <tr><td>Symbol</td><td>PFLT</td></tr>
    <tr><td>Web</td><td><a href="https://www.pennantpark.com">www.pennantpark.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.475 |
| 2020 | 1.14 |
| 2019 | 1.14 |
| 2018 | 1.14 |
| 2017 | 1.14 |
| 2016 | 1.14 |
| 2015 | 1.13 |
| 2014 | 1.08 |
| 2013 | 1.048 |
| 2012 | 0.947 |
| 2011 | 0.46 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
