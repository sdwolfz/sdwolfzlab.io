---
title: "CHILDREN'S PLACE INC (PLCE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CHILDREN'S PLACE INC</td></tr>
    <tr><td>Symbol</td><td>PLCE</td></tr>
    <tr><td>Web</td><td><a href="https://www.childrensplace.com">www.childrensplace.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 2.24 |
| 2018 | 2.0 |
| 2017 | 1.6 |
| 2016 | 0.8 |
| 2015 | 0.6 |
| 2014 | 0.53 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
