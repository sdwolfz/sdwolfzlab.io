---
title: "HEIDRICK & STRUGGLES (HSII)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HEIDRICK & STRUGGLES</td></tr>
    <tr><td>Symbol</td><td>HSII</td></tr>
    <tr><td>Web</td><td><a href="https://www.heidrick.com">www.heidrick.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 0.6 |
| 2019 | 0.6 |
| 2018 | 0.52 |
| 2017 | 0.52 |
| 2016 | 0.52 |
| 2015 | 0.52 |
| 2014 | 0.52 |
| 2013 | 0.39 |
| 2012 | 0.65 |
| 2011 | 0.52 |
| 2010 | 0.52 |
| 2009 | 0.52 |
| 2008 | 0.52 |
| 2007 | 0.13 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
