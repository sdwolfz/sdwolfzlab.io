---
title: "STEPAN CO (SCL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>STEPAN CO</td></tr>
    <tr><td>Symbol</td><td>SCL</td></tr>
    <tr><td>Web</td><td><a href="https://www.stepan.com">www.stepan.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.61 |
| 2020 | 1.13 |
| 2019 | 1.025 |
| 2018 | 0.925 |
| 2017 | 1.045 |
| 2016 | 0.775 |
| 2015 | 0.73 |
| 2014 | 0.69 |
| 2013 | 0.33 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
