---
title: "ENCOMPASS HEALTH CORPORATION (EHC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ENCOMPASS HEALTH CORPORATION</td></tr>
    <tr><td>Symbol</td><td>EHC</td></tr>
    <tr><td>Web</td><td><a href="https://www.encompasshealth.com">www.encompasshealth.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.56 |
| 2020 | 1.12 |
| 2019 | 1.1 |
| 2018 | 1.04 |
| 2017 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
