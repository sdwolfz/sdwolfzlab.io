---
title: "COGNIZANT TECHNOLOGY SOLUTIONS CORP (CTSH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>COGNIZANT TECHNOLOGY SOLUTIONS CORP</td></tr>
    <tr><td>Symbol</td><td>CTSH</td></tr>
    <tr><td>Web</td><td><a href="https://www.cognizant.com">www.cognizant.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.48 |
| 2020 | 0.88 |
| 2019 | 0.8 |
| 2018 | 0.8 |
| 2017 | 0.45 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
