---
title: "CME GROUP INC (CME)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CME GROUP INC</td></tr>
    <tr><td>Symbol</td><td>CME</td></tr>
    <tr><td>Web</td><td><a href="https://www.cmegroup.com">www.cmegroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.8 |
| 2020 | 5.9 |
| 2019 | 5.5 |
| 2018 | 4.55 |
| 2017 | 6.14 |
| 2016 | 5.65 |
| 2015 | 4.9 |
| 2014 | 3.88 |
| 2013 | 4.4 |
| 2012 | 9.66 |
| 2011 | 5.6 |
| 2010 | 4.6 |
| 2009 | 4.6 |
| 2008 | 9.6 |
| 2007 | 3.44 |
| 2006 | 2.52 |
| 2005 | 1.84 |
| 2004 | 1.04 |
| 2003 | 0.63 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
