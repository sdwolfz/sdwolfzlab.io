---
title: "BASSETT FURNITURE INDUSTRIES INC (BSET)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>BASSETT FURNITURE INDUSTRIES INC</td></tr>
    <tr><td>Symbol</td><td>BSET</td></tr>
    <tr><td>Web</td><td><a href="https://www.bassettfurniture.com">www.bassettfurniture.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.25 |
| 2020 | 0.705 |
| 2019 | 0.5 |
| 2018 | 0.47 |
| 2017 | 0.77 |
| 2016 | 0.68 |
| 2015 | 0.54 |
| 2014 | 0.48 |
| 2013 | 0.42 |
| 2012 | 1.45 |
| 2011 | 0.595 |
| 2008 | 1.5 |
| 2007 | 0.8 |
| 2006 | 0.8 |
| 2005 | 0.8 |
| 2004 | 0.8 |
| 2003 | 0.8 |
| 2002 | 0.8 |
| 2001 | 0.8 |
| 2000 | 1.0 |
| 1999 | 0.8 |
| 1998 | 0.8 |
| 1997 | 1.0 |
| 1996 | 0.8 |
| 1995 | 0.8 |
| 1994 | 0.8 |
| 1993 | 0.88 |
| 1992 | 0.88 |
| 1991 | 1.0 |
| 1990 | 1.25 |
| 1989 | 2.0 |
| 1988 | 0.25 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
