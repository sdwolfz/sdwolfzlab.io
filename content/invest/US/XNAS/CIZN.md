---
title: "CITIZENS HLDG CO(MISS) (CIZN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CITIZENS HLDG CO(MISS)</td></tr>
    <tr><td>Symbol</td><td>CIZN</td></tr>
    <tr><td>Web</td><td><a href="https://www.citizensholdingcompany.com">www.citizensholdingcompany.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.24 |
| 2020 | 0.96 |
| 2019 | 0.96 |
| 2018 | 0.96 |
| 2017 | 0.96 |
| 2016 | 0.96 |
| 2015 | 0.93 |
| 2014 | 0.89 |
| 2013 | 0.88 |
| 2012 | 0.88 |
| 2011 | 0.88 |
| 2010 | 0.85 |
| 2009 | 0.81 |
| 2008 | 0.77 |
| 2007 | 0.73 |
| 2006 | 0.69 |
| 2005 | 0.65 |
| 2004 | 0.6 |
| 2003 | 0.57 |
| 2002 | 0.52 |
| 2001 | 0.575 |
| 2000 | 0.425 |
| 1999 | 0.17 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
