---
title: " (COR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>COR</td></tr>
    <tr><td>Web</td><td><a href="https://www.coresite.com">www.coresite.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.23 |
| 2020 | 4.89 |
| 2019 | 4.76 |
| 2018 | 4.14 |
| 2017 | 3.58 |
| 2016 | 2.39 |
| 2015 | 1.79 |
| 2014 | 1.47 |
| 2013 | 0.89 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
