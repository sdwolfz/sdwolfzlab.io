---
title: "CARRIER GLOBAL CORPORATION (CARR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CARRIER GLOBAL CORPORATION</td></tr>
    <tr><td>Symbol</td><td>CARR</td></tr>
    <tr><td>Web</td><td><a href="https://www.corporate.carrier.com">www.corporate.carrier.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.12 |
| 2020 | 0.28 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
