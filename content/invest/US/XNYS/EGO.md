---
title: "ELDORADO GOLD CORP (EGO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ELDORADO GOLD CORP</td></tr>
    <tr><td>Symbol</td><td>EGO</td></tr>
    <tr><td>Web</td><td><a href="https://www.eldoradogold.com">www.eldoradogold.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2017 | 0.015 |
| 2015 | 0.016 |
| 2014 | 0.02 |
| 2013 | 0.05 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
