---
title: "RCI HOSPITALITY HOLDINGS INC (RICK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>RCI HOSPITALITY HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>RICK</td></tr>
    <tr><td>Web</td><td><a href="https://www.rcihospitality.com">www.rcihospitality.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.04 |
| 2020 | 0.15 |
| 2019 | 0.13 |
| 2018 | 0.12 |
| 2017 | 0.12 |
| 2016 | 0.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
