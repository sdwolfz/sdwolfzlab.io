---
title: "PORTLAND GENERAL ELECTRIC CO (POR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>PORTLAND GENERAL ELECTRIC CO</td></tr>
    <tr><td>Symbol</td><td>POR</td></tr>
    <tr><td>Web</td><td><a href="https://www.portlandgeneral.com">www.portlandgeneral.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.837 |
| 2020 | 1.584 |
| 2019 | 1.518 |
| 2018 | 1.429 |
| 2017 | 1.34 |
| 2016 | 1.26 |
| 2015 | 1.18 |
| 2014 | 1.115 |
| 2013 | 0.55 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
