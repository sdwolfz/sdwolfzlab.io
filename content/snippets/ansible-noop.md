---
title: "Ansible noop, resume, and skip"
date: 2023-02-21T13:29:11Z
type: posts
slug: ansible-noop-resume-skip
tags: ["ansible", "automation", "linux"]
---

Put one `noop` task at the beginning and end of your role:

```yml
---

- name: ToolInstall
  ansible.builtin.meta: noop

# ...

- name: ToolInstallEnd
  ansible.builtin.meta: noop
```

Now you can resume from where you left off with:

```sh
ansible-playbook playbooks/tool/install.yml --start-at-task=ToolInstall
```

And you can skip over that role and everything before it with:

```sh
ansible-playbook playbooks/tool/install.yml --start-at-task=ToolInstallEnd
```
