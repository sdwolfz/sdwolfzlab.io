---
title: "Mondi (MNDI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Mondi</td></tr>
    <tr><td>Symbol</td><td>MNDI</td></tr>
    <tr><td>Web</td><td><a href="https://www.mondigroup.com">www.mondigroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 83.0 |
| 2018 | 76.0 |
| 2017 | 162.0 |
| 2016 | 57.0 |
| 2015 | 52.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
