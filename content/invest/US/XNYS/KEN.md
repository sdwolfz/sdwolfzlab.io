---
title: "KENON HLDGS LTD (KEN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>KENON HLDGS LTD</td></tr>
    <tr><td>Symbol</td><td>KEN</td></tr>
    <tr><td>Web</td><td><a href="https://www.kenon-holdings.com">www.kenon-holdings.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.86 |
| 2020 | 2.23 |
| 2019 | 1.21 |
| 2018 | 1.86 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
