---
title: "VISHAY INTERTECHNOLOGY INC (VSH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>VISHAY INTERTECHNOLOGY INC</td></tr>
    <tr><td>Symbol</td><td>VSH</td></tr>
    <tr><td>Web</td><td><a href="https://www.vishay.com">www.vishay.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.095 |
| 2020 | 0.38 |
| 2019 | 0.37 |
| 2018 | 0.323 |
| 2017 | 0.257 |
| 2016 | 0.252 |
| 2015 | 0.24 |
| 2014 | 0.24 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
