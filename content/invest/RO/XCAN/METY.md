---
title: "MECANICA ROTES SA Targoviste (METY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>MECANICA ROTES SA Targoviste</td></tr>
    <tr><td>Symbol</td><td>METY</td></tr>
    <tr><td>Web</td><td><a href="https://www.rotes.ro">www.rotes.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
