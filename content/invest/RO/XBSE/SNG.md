---
title: "S.N.G.N. ROMGAZ S.A. (SNG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>S.N.G.N. ROMGAZ S.A.</td></tr>
    <tr><td>Symbol</td><td>SNG</td></tr>
    <tr><td>Web</td><td><a href="https://www.romgaz.ro">www.romgaz.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.79 |
| 2019 | 1.61 |
| 2018 | 4.17 |
| 2017 | 1.86 |
| 2017 | 4.99 |
| 2016 | 1.94 |
| 2016 | 3.82 |
| 2015 | 2.7 |
| 2014 | 3.15 |
| 2013 | 2.57 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
