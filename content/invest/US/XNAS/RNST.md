---
title: "RENASANT CORPORATION (RNST)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>RENASANT CORPORATION</td></tr>
    <tr><td>Symbol</td><td>RNST</td></tr>
    <tr><td>Web</td><td><a href="https://www.renasant.com">www.renasant.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.44 |
| 2020 | 0.88 |
| 2019 | 0.87 |
| 2018 | 0.8 |
| 2017 | 0.73 |
| 2016 | 0.71 |
| 2015 | 0.68 |
| 2014 | 0.68 |
| 2013 | 0.68 |
| 2012 | 0.68 |
| 2011 | 0.68 |
| 2010 | 0.68 |
| 2009 | 0.68 |
| 2008 | 0.68 |
| 2007 | 0.66 |
| 2006 | 0.78 |
| 2005 | 0.87 |
| 2004 | 0.82 |
| 2003 | 1.03 |
| 2002 | 1.04 |
| 2001 | 0.96 |
| 2000 | 0.88 |
| 1999 | 0.21 |
| 1997 | 0.42 |
| 1996 | 0.843 |
| 1995 | 1.028 |
| 1994 | 0.95 |
| 1993 | 0.91 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
