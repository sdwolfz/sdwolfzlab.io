---
title: "SILGAN HOLDINGS INC (SLGN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SILGAN HOLDINGS INC</td></tr>
    <tr><td>Symbol</td><td>SLGN</td></tr>
    <tr><td>Web</td><td><a href="https://www.silganholdings.com">www.silganholdings.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 0.48 |
| 2019 | 0.44 |
| 2018 | 0.4 |
| 2017 | 0.36 |
| 2016 | 0.68 |
| 2015 | 0.64 |
| 2014 | 0.6 |
| 2013 | 0.56 |
| 2012 | 0.48 |
| 2011 | 0.44 |
| 2010 | 0.525 |
| 2009 | 0.76 |
| 2008 | 0.68 |
| 2007 | 0.64 |
| 2006 | 0.48 |
| 2005 | 0.7 |
| 2004 | 0.45 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
