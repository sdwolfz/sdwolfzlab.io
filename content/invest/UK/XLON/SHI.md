---
title: "SIG (SHI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>SIG</td></tr>
    <tr><td>Symbol</td><td>SHI</td></tr>
    <tr><td>Web</td><td><a href="https://www.sigplc.co.uk">www.sigplc.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 1.25 |
| 2018 | 3.75 |
| 2017 | 3.75 |
| 2016 | 3.66 |
| 2015 | 4.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
