---
title: "KONTOOR BRANDS INC (KTB)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>KONTOOR BRANDS INC</td></tr>
    <tr><td>Symbol</td><td>KTB</td></tr>
    <tr><td>Web</td><td><a href="https://www.kontoorbrands.com">www.kontoorbrands.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.8 |
| 2020 | 0.96 |
| 2019 | 1.12 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
