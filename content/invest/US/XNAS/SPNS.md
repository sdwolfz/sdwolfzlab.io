---
title: "SAPIENS INTERNATIONAL CORP NV (SPNS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SAPIENS INTERNATIONAL CORP NV</td></tr>
    <tr><td>Symbol</td><td>SPNS</td></tr>
    <tr><td>Web</td><td><a href="https://www.sapiens.com">www.sapiens.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.37 |
| 2020 | 0.14 |
| 2019 | 0.22 |
| 2018 | 0.2 |
| 2017 | 0.2 |
| 2016 | 0.2 |
| 2015 | 0.15 |
| 2013 | 0.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
