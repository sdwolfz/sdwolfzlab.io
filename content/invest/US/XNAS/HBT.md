---
title: "HBT FINANCIAL INC (HBT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>HBT FINANCIAL INC</td></tr>
    <tr><td>Symbol</td><td>HBT</td></tr>
    <tr><td>Web</td><td><a href="https://ir.hbtfinancial.com">ir.hbtfinancial.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.3 |
| 2020 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
