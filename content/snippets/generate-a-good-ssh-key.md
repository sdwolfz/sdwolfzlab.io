---
title: "Generate a good SSH key"
date: 2020-08-03T20:51:47Z
type: posts
slug: generate-a-good-ssh-key
tags: ["ssh", "linux"]
---

## The proper one

If you don't have any keys, generate a default one:

```sh
ssh-keygen -t ed25519 -a 100 -q -N "" -f ~/.ssh/id_ed25519
```

And if you want it generated in the current directory do:

```sh
ssh-keygen -t ed25519 -a 100 -q -N "" -f "$PWD"/renameme
```

Change the name by passing the `-C` argument:

```sh
ssh-keygen -t ed25519 -a 100 -q -N "" -f "$PWD"/deploy_key_1 -C deploy_key_1
```

## The outdated one

Don't use RSA anymore, it's outdated and insecure, yet some servers have not
updated to the newer versions above...

```sh
ssh-keygen -t rsa -b 4096 -q -N "" -f ~/.ssh/id_rsa
ssh-keygen -t rsa -b 4096 -q -N "" -f "$PWD"/renameme
```
