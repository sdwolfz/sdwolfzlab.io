---
title: "CISCO SYSTEMS INC (CSCO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CISCO SYSTEMS INC</td></tr>
    <tr><td>Symbol</td><td>CSCO</td></tr>
    <tr><td>Web</td><td><a href="https://www.cisco.com">www.cisco.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.73 |
| 2020 | 1.43 |
| 2019 | 1.38 |
| 2018 | 1.28 |
| 2017 | 1.13 |
| 2016 | 0.99 |
| 2015 | 0.82 |
| 2014 | 0.74 |
| 2013 | 0.51 |
| 2012 | 0.5 |
| 2011 | 0.18 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
