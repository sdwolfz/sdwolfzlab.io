---
title: "AVERY DENNISON CORP (AVY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AVERY DENNISON CORP</td></tr>
    <tr><td>Symbol</td><td>AVY</td></tr>
    <tr><td>Web</td><td><a href="https://www.averydennison.com">www.averydennison.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 1.3 |
| 2020 | 2.36 |
| 2019 | 2.26 |
| 2018 | 2.01 |
| 2017 | 1.76 |
| 2016 | 1.6 |
| 2015 | 1.46 |
| 2014 | 1.34 |
| 2013 | 0.58 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
