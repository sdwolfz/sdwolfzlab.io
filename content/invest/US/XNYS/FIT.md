---
title: "FITBIT INC (FIT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>FITBIT INC</td></tr>
    <tr><td>Symbol</td><td>FIT</td></tr>
    <tr><td>Web</td><td><a href="http://www.fitbit.com">www.fitbit.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
