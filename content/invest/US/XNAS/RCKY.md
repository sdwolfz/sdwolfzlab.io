---
title: "ROCKY BRANDS INC (RCKY)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ROCKY BRANDS INC</td></tr>
    <tr><td>Symbol</td><td>RCKY</td></tr>
    <tr><td>Web</td><td><a href="https://www.rockybrands.com">www.rockybrands.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.14 |
| 2020 | 0.56 |
| 2019 | 0.54 |
| 2018 | 0.47 |
| 2017 | 0.44 |
| 2016 | 0.44 |
| 2015 | 0.43 |
| 2014 | 0.4 |
| 2013 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
