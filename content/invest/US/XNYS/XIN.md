---
title: " (XIN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>XIN</td></tr>
    <tr><td>Web</td><td><a href="https://www.xyre.com">www.xyre.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.166 |
| 2019 | 0.388 |
| 2018 | 0.388 |
| 2017 | 0.388 |
| 2016 | 0.288 |
| 2015 | 0.188 |
| 2014 | 0.188 |
| 2013 | 0.094 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
