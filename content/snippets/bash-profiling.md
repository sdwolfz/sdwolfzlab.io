---
title: "Bash profiling"
date: 2020-12-18T12:19:25Z
type: posts
slug: bash-profiling
tags: ["linux", "bash"]
---

Execute these lines in a shell, or put them at the top of your `bash` script:

```sh
set -x
PS4='+ $EPOCHREALTIME ($LINENO) '
```

Now, every time you execute a command or script, you will see a timestamp, line
number, and command/script executed.

```sh
export A=1
# + 1608294433.986333 (3) export A=1
# + 1608294433.986378 (3) A=1

export B=2
# + 1608294439.100780 (4) export B=2
# + 1608294439.100807 (4) B=2

[ "$A" == "$B" ] && echo 'Equal' || echo 'Not Equal' && false
# + 1608294491.748071 (5) '[' 1 == 2 ']'
# + 1608294491.748115 (5) echo 'Not Equal'
# Not Equal
# + 1608294491.748200 (5) false
```
