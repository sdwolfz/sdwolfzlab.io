---
title: "RPC INC (RES)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>RPC INC</td></tr>
    <tr><td>Symbol</td><td>RES</td></tr>
    <tr><td>Web</td><td><a href="https://www.rpc.net">www.rpc.net</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 0.15 |
| 2018 | 0.47 |
| 2017 | 0.2 |
| 2016 | 0.05 |
| 2015 | 0.205 |
| 2014 | 0.42 |
| 2013 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
