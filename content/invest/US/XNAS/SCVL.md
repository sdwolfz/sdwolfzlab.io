---
title: "SHOE CARNIVAL INC (SCVL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>SHOE CARNIVAL INC</td></tr>
    <tr><td>Symbol</td><td>SCVL</td></tr>
    <tr><td>Web</td><td><a href="https://www.shoecarnival.com">www.shoecarnival.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.23 |
| 2020 | 0.35 |
| 2019 | 0.33 |
| 2018 | 0.31 |
| 2017 | 0.29 |
| 2016 | 0.27 |
| 2015 | 0.25 |
| 2014 | 0.24 |
| 2013 | 0.18 |
| 2012 | 1.15 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
