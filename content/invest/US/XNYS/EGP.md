---
title: " (EGP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>EGP</td></tr>
    <tr><td>Web</td><td><a href="https://www.eastgroup.net">www.eastgroup.net</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.79 |
| 2020 | 3.08 |
| 2019 | 2.94 |
| 2018 | 2.72 |
| 2017 | 2.52 |
| 2016 | 2.44 |
| 2015 | 2.34 |
| 2014 | 2.22 |
| 2013 | 1.61 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
