---
title: "IASITEX SA IASI (IASX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>IASITEX SA IASI</td></tr>
    <tr><td>Symbol</td><td>IASX</td></tr>
    <tr><td>Web</td><td><a href="https://www.iasitex.ro">www.iasitex.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
