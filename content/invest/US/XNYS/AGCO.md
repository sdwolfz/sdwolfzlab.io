---
title: "AGCO CORP (AGCO)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AGCO CORP</td></tr>
    <tr><td>Symbol</td><td>AGCO</td></tr>
    <tr><td>Web</td><td><a href="https://www.agcocorp.com">www.agcocorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 4.16 |
| 2020 | 0.64 |
| 2019 | 0.63 |
| 2018 | 0.6 |
| 2017 | 0.56 |
| 2016 | 0.52 |
| 2015 | 0.48 |
| 2014 | 0.44 |
| 2013 | 0.2 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
