---
title: " (ABR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>ABR</td></tr>
    <tr><td>Web</td><td><a href="https://www.arbor.com">www.arbor.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.33 |
| 2020 | 1.23 |
| 2019 | 1.14 |
| 2018 | 1.13 |
| 2017 | 0.71 |
| 2016 | 0.62 |
| 2015 | 0.58 |
| 2014 | 0.52 |
| 2013 | 0.26 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
