---
title: "TRINITY CAPITAL INC (TRIN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>TRINITY CAPITAL INC</td></tr>
    <tr><td>Symbol</td><td>TRIN</td></tr>
    <tr><td>Web</td><td><a href="https://www.trincapinvestment.com">www.trincapinvestment.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
