---
title: "MANPOWERGROUP INC (MAN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>MANPOWERGROUP INC</td></tr>
    <tr><td>Symbol</td><td>MAN</td></tr>
    <tr><td>Web</td><td><a href="https://www.manpowergroup.com">www.manpowergroup.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.26 |
| 2019 | 2.18 |
| 2018 | 2.02 |
| 2017 | 1.86 |
| 2016 | 1.72 |
| 2015 | 1.6 |
| 2014 | 0.98 |
| 2013 | 0.92 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
