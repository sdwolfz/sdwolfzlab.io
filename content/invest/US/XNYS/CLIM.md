---
title: "CLIMATE REAL IMPACT SOL II ACQ CORP (CLIM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CLIMATE REAL IMPACT SOL II ACQ CORP</td></tr>
    <tr><td>Symbol</td><td>CLIM</td></tr>
    <tr><td>Web</td><td><a href="https://www.climaterealimpactsolutions.com">www.climaterealimpactsolutions.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
