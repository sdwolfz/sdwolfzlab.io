---
title: "CURTISS-WRIGHT CORP (CW)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CURTISS-WRIGHT CORP</td></tr>
    <tr><td>Symbol</td><td>CW</td></tr>
    <tr><td>Web</td><td><a href="https://www.curtisswright.com">www.curtisswright.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.17 |
| 2020 | 0.68 |
| 2019 | 0.66 |
| 2018 | 0.6 |
| 2017 | 0.56 |
| 2016 | 0.52 |
| 2015 | 0.52 |
| 2014 | 0.52 |
| 2013 | 0.3 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
