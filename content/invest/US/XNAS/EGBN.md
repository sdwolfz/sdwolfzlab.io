---
title: "EAGLE BANCORP INC (EGBN)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>EAGLE BANCORP INC</td></tr>
    <tr><td>Symbol</td><td>EGBN</td></tr>
    <tr><td>Web</td><td><a href="https://www.eaglebankcorp.com">www.eaglebankcorp.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.47 |
| 2020 | 0.88 |
| 2019 | 0.44 |
| 2008 | 0.12 |
| 2007 | 0.24 |
| 2006 | 0.26 |
| 2005 | 0.301 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
