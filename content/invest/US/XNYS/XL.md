---
title: "XL FLEET CORP (XL)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>XL FLEET CORP</td></tr>
    <tr><td>Symbol</td><td>XL</td></tr>
    <tr><td>Web</td><td><a href="https://www.xlfleet.com">www.xlfleet.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2018 | 0.66 |
| 2017 | 0.88 |
| 2016 | 0.8 |
| 2015 | 0.72 |
| 2014 | 0.64 |
| 2013 | 0.28 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
