---
title: "GLADSTONE LAND CORP 6.375% CUM TERM PFD SER A (LANDP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Market</td><td><a href="/invest/usa/">NYSE</a></td></tr>
    <tr><td>Segment</td><td><a href="/invest/usa/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>GLADSTONE LAND CORP 6.375% CUM TERM PFD SER A</td></tr>
    <tr><td>Symbol</td><td>LANDP</td></tr>
    <tr><td>Web</td><td><a href="http://www.GladstoneFarms.com">www.GladstoneFarms.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 1.594 |
| 2019 | 1.594 |
| 2018 | 1.594 |
| 2017 | 1.594 |
| 2016 | 0.589 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
