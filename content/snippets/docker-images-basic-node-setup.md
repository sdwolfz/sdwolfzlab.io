---
title: "Docker Images: basic Node setup"
date: 2020-10-14T18:32:57Z
type: posts
slug: docker-images-basic-node-setup
series: ["Docker Images"]
tags: ["docker", "node", "javascript", "shell", "productivity", "development"]
---

Use this as a template for node apps:

```dockerfile
#-------------------------------------------------------------------------------
# Base

FROM node:16.2.0-alpine

SHELL ["/bin/ash", "-c"]

#-------------------------------------------------------------------------------
# Packages

RUN set -exo pipefail              && \
                                      \
    echo 'Install system packages' && \
    apk add --update --no-cache \
      build-base                \
      shadow

#-------------------------------------------------------------------------------
# Workspace

RUN set -exo pipefail       && \
                               \
    echo 'Create workspace' && \
    mkdir -p /work

WORKDIR /work

#-------------------------------------------------------------------------------
# User

ARG HOST_USER_UID=1000
ARG HOST_USER_GID=1000

RUN set -exo pipefail                                  && \
                                                          \
    echo 'Create the notroot user and group from host' && \
    deluser --remove-home node                         && \
    groupadd -g $HOST_USER_GID notroot                 && \
    useradd -lm -u $HOST_USER_UID -g notroot notroot   && \
                                                          \
    echo 'Set direcotry permissions'                   && \
    chown notroot:notroot /work

#-------------------------------------------------------------------------------
# Command

CMD ["sh"]
```

Build it like this:

```sh
docker build \
  --build-arg=HOST_USER_UID=`id -u` \
  --build-arg=HOST_USER_GID=`id -g` \
  -t node-project \
  "$PWD"
```

Or in `docker-compose` using:

```yml
---

services:
  app:
    build:
      args:
        HOST_USER_UID: ${HOST_USER_UID}
        HOST_USER_GID: ${HOST_USER_GID}
      context: .
      dockerfile: ./Dockerfile
```

And have your host variables defined in your `.bash_profile`:

```sh
HOST_USER_UID=$(id -u)
HOST_USER_GID=$(id -g)

export HOST_USER_UID
export HOST_USER_GID
```
