---
title: "HCA HEALTHCARE INC (HCA)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>HCA HEALTHCARE INC</td></tr>
    <tr><td>Symbol</td><td>HCA</td></tr>
    <tr><td>Web</td><td><a href="https://www.hcahealthcare.com">www.hcahealthcare.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.96 |
| 2020 | 0.502 |
| 2019 | 1.6 |
| 2018 | 1.4 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
