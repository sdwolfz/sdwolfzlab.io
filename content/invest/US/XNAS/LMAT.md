---
title: "LEMAITRE VASCULAR INC (LMAT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>LEMAITRE VASCULAR INC</td></tr>
    <tr><td>Symbol</td><td>LMAT</td></tr>
    <tr><td>Web</td><td><a href="https://www.lemaitre.com">www.lemaitre.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.22 |
| 2020 | 0.38 |
| 2019 | 0.34 |
| 2018 | 0.28 |
| 2017 | 0.22 |
| 2016 | 0.18 |
| 2015 | 0.16 |
| 2014 | 0.14 |
| 2013 | 0.12 |
| 2012 | 0.1 |
| 2011 | 0.08 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
