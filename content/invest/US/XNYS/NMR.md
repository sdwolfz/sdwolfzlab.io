---
title: " (NMR)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>NMR</td></tr>
    <tr><td>Web</td><td><a href="https://www.nomura.com">www.nomura.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.201 |
| 2019 | 0.14 |
| 2018 | 0.11 |
| 2017 | 0.07 |
| 2016 | 0.093 |
| 2015 | 0.162 |
| 2014 | 0.121 |
| 2013 | 0.126 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
