---
title: "STARBUCKS CORP (SBUX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>STARBUCKS CORP</td></tr>
    <tr><td>Symbol</td><td>SBUX</td></tr>
    <tr><td>Web</td><td><a href="https://www.starbucks.com">www.starbucks.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.9 |
| 2020 | 1.68 |
| 2019 | 1.49 |
| 2018 | 1.32 |
| 2017 | 1.05 |
| 2016 | 0.85 |
| 2015 | 0.84 |
| 2014 | 1.1 |
| 2013 | 0.89 |
| 2012 | 0.72 |
| 2011 | 0.56 |
| 2010 | 0.36 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
