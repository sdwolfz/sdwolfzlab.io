---
title: "Empiric (ESP)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Empiric</td></tr>
    <tr><td>Symbol</td><td>ESP</td></tr>
    <tr><td>Web</td><td><a href="https://www.empiric.co.uk">www.empiric.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2019 | 500.0 |
| 2018 | 500.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
