---
title: "UZTEL S.A. (UZT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xbse/">XBSE</a></td></tr>
    <tr><td>Name</td><td>UZTEL S.A.</td></tr>
    <tr><td>Symbol</td><td>UZT</td></tr>
    <tr><td>Web</td><td><a href="https://www.uztel.ro">www.uztel.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2008 | 0.29 |
| 2007 | 0.65 |
| 2006 | 0.89 |
| 2005 | 0.6 |
| 2001 | 0.6181 |
| 2000 | 1.3873 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
