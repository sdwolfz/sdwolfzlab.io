---
title: "ALCOM SA TIMISOARA (ALCQ)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/ro/">RO</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/ro/xcan/">XCAN</a></td></tr>
    <tr><td>Name</td><td>ALCOM SA TIMISOARA</td></tr>
    <tr><td>Symbol</td><td>ALCQ</td></tr>
    <tr><td>Web</td><td><a href="https://www.alcomtimisoara.ro">www.alcomtimisoara.ro</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 2.6188 |
| 2019 | 3.0853 |
| 2018 | 2.9594 |
| 2017 | 15.0 |
| 2014 | 1.17115 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
