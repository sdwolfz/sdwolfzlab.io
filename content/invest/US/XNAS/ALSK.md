---
title: "ALASKA COMMUNICATIONS SYS GROUP INC (ALSK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>ALASKA COMMUNICATIONS SYS GROUP INC</td></tr>
    <tr><td>Symbol</td><td>ALSK</td></tr>
    <tr><td>Web</td><td><a href="https://www.alaskacommunications.com">www.alaskacommunications.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 0.09 |
| 2012 | 0.15 |
| 2011 | 0.695 |
| 2010 | 0.86 |
| 2009 | 0.86 |
| 2008 | 0.86 |
| 2007 | 0.86 |
| 2006 | 0.86 |
| 2005 | 0.8 |
| 2004 | 0.185 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
