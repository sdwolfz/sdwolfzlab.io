---
title: "CARTERS INC (CRI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>CARTERS INC</td></tr>
    <tr><td>Symbol</td><td>CRI</td></tr>
    <tr><td>Web</td><td><a href="https://www.carters.com">www.carters.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.4 |
| 2020 | 0.6 |
| 2019 | 2.0 |
| 2018 | 1.8 |
| 2017 | 1.48 |
| 2016 | 1.32 |
| 2015 | 0.88 |
| 2014 | 0.76 |
| 2013 | 0.48 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
