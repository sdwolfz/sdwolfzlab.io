---
title: "AFFILIATED MANAGERS GROUP (AMG)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>AFFILIATED MANAGERS GROUP</td></tr>
    <tr><td>Symbol</td><td>AMG</td></tr>
    <tr><td>Web</td><td><a href="https://www.amg.com">www.amg.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.02 |
| 2020 | 0.35 |
| 2019 | 1.28 |
| 2018 | 1.2 |
| 2017 | 0.8 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
