---
title: "How to Use: pry-byebug"
date: 2020-04-12T12:19:57Z
type: posts
slug: how-to-use-pry-byebug
series: ["How to Use"]
tags: ["ruby", "tutorial", "debugging", "productivity"]
---

1. Add the `pry-byebug` gem, together with `irb`, to your `Gemfile`:

```ruby
group :development, :test do
  gem 'irb'
  gem 'pry-byebug'
end
```

2. Add a `.pryrc` file to your project containing:

```ruby
# frozen_string_literal: true

if defined?(PryByebug)
  Pry.commands.alias_command('c', 'continue')
  Pry.commands.alias_command('f', 'finish')
  Pry.commands.alias_command('n', 'next')
  Pry.commands.alias_command('s', 'step')
  Pry.commands.alias_command('w', 'whereami')

  Pry.commands.alias_command('q', '!!!')

  Pry::Commands.command(/^$/, 'repeat last command') do
    _pry_.run_command(Pry.history.to_a.last)
  end
end
```

3. Now you can start debugging your code:

```ruby
require 'pry'
binding.pry
```
