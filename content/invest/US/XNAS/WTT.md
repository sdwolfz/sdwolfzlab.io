---
title: "WIRELESS TELECOM GROUP INC (WTT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>WIRELESS TELECOM GROUP INC</td></tr>
    <tr><td>Symbol</td><td>WTT</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2005 | 0.12 |
| 2004 | 0.12 |
| 2003 | 0.09 |
| 2002 | 0.06 |
| 2001 | 0.06 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
