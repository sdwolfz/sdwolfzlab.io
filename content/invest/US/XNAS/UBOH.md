---
title: "UNITED BANCSHARES INC(OHIO) (UBOH)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>UNITED BANCSHARES INC(OHIO)</td></tr>
    <tr><td>Symbol</td><td>UBOH</td></tr>
    <tr><td>Web</td><td><a href="https://www.theubank.com">www.theubank.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.33 |
| 2020 | 0.51 |
| 2019 | 0.52 |
| 2018 | 0.48 |
| 2017 | 0.48 |
| 2016 | 0.44 |
| 2015 | 0.36 |
| 2014 | 0.35 |
| 2013 | 0.2 |
| 2012 | 0.05 |
| 2010 | 0.45 |
| 2009 | 0.6 |
| 2008 | 0.6 |
| 2007 | 0.56 |
| 2006 | 0.52 |
| 2005 | 0.48 |
| 2004 | 0.44 |
| 2003 | 0.44 |
| 2002 | 0.44 |
| 2001 | 0.33 |
| 1999 | 0.212 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
