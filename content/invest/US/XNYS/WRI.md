---
title: " (WRI)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>WRI</td></tr>
    <tr><td>Web</td><td><a href="https://www.weingarten.com">www.weingarten.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.53 |
| 2020 | 1.295 |
| 2019 | 1.58 |
| 2018 | 2.585 |
| 2017 | 1.905 |
| 2016 | 1.46 |
| 2015 | 1.38 |
| 2014 | 1.55 |
| 2013 | 0.61 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
