---
title: "OWENS CORNING (OC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>OWENS CORNING</td></tr>
    <tr><td>Symbol</td><td>OC</td></tr>
    <tr><td>Web</td><td><a href="https://www.owenscorning.com">www.owenscorning.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.52 |
| 2020 | 0.96 |
| 2019 | 0.88 |
| 2018 | 0.63 |
| 2017 | 0.81 |
| 2016 | 0.74 |
| 2015 | 0.68 |
| 2014 | 0.64 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
