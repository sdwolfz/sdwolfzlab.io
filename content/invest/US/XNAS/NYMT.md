---
title: " (NYMT)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>NYMT</td></tr>
    <tr><td>Web</td><td><a href="https://www.nymtrust.com">www.nymtrust.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.225 |
| 2019 | 0.8 |
| 2018 | 0.8 |
| 2017 | 0.8 |
| 2016 | 0.96 |
| 2015 | 1.02 |
| 2014 | 1.08 |
| 2013 | 1.08 |
| 2012 | 1.06 |
| 2011 | 1.0 |
| 2010 | 1.04 |
| 2009 | 0.76 |
| 2008 | 0.38 |
| 2007 | 0.05 |
| 2006 | 0.63 |
| 2005 | 0.95 |
| 2004 | 0.16 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
