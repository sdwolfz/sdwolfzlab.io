---
title: "PROSPECT CAPITAL CORPORATION (PSEC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PROSPECT CAPITAL CORPORATION</td></tr>
    <tr><td>Symbol</td><td>PSEC</td></tr>
    <tr><td>Web</td><td>-</td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.24 |
| 2020 | 0.72 |
| 2019 | 0.72 |
| 2018 | 0.72 |
| 2017 | 0.907 |
| 2016 | 1.0 |
| 2015 | 1.027 |
| 2014 | 1.326 |
| 2013 | 1.322 |
| 2012 | 1.227 |
| 2011 | 1.215 |
| 2010 | 1.114 |
| 2009 | 1.628 |
| 2008 | 1.608 |
| 2007 | 1.565 |
| 2006 | 1.405 |
| 2005 | 0.755 |
| 2004 | 0.1 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
