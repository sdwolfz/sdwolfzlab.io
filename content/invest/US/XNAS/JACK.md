---
title: "JACK IN THE BOX INC (JACK)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>JACK IN THE BOX INC</td></tr>
    <tr><td>Symbol</td><td>JACK</td></tr>
    <tr><td>Web</td><td><a href="https://www.jackinthebox.com">www.jackinthebox.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.4 |
| 2020 | 1.2 |
| 2019 | 1.6 |
| 2018 | 1.6 |
| 2017 | 1.6 |
| 2016 | 1.3 |
| 2015 | 1.1 |
| 2014 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
