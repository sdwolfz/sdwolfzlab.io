---
title: " (NS)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/"></a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td></td></tr>
    <tr><td>Symbol</td><td>NS</td></tr>
    <tr><td>Web</td><td><a href="https://www.nustarenergy.com">www.nustarenergy.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.8 |
| 2020 | 1.8 |
| 2019 | 2.4 |
| 2018 | 2.895 |
| 2017 | 4.38 |
| 2016 | 4.38 |
| 2015 | 4.38 |
| 2014 | 4.38 |
| 2013 | 2.19 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
