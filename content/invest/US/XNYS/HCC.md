---
title: "WARRIOR MET COAL INC (HCC)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>WARRIOR MET COAL INC</td></tr>
    <tr><td>Symbol</td><td>HCC</td></tr>
    <tr><td>Web</td><td><a href="https://www.warriormetcoal.com">www.warriormetcoal.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.1 |
| 2020 | 0.2 |
| 2019 | 4.566 |
| 2018 | 6.729 |
| 2017 | 11.31 |
| 2015 | 0.885 |
| 2014 | 1.04 |
| 2013 | 0.615 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
