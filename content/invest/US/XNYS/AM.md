---
title: "ANTERO MIDSTREAM CORPORATION (AM)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnys/">XNYS</a></td></tr>
    <tr><td>Name</td><td>ANTERO MIDSTREAM CORPORATION</td></tr>
    <tr><td>Symbol</td><td>AM</td></tr>
    <tr><td>Web</td><td><a href="https://www.anteromidstream.com">www.anteromidstream.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.533 |
| 2020 | 1.232 |
| 2019 | 1.083 |
| 2018 | 1.61 |
| 2017 | 1.24 |
| 2016 | 0.97 |
| 2015 | 0.669 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
