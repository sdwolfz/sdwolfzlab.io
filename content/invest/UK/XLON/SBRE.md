---
title: "Sabre Insur (SBRE)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/uk/">UK</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/uk/xlon/">XLON</a></td></tr>
    <tr><td>Name</td><td>Sabre Insur</td></tr>
    <tr><td>Symbol</td><td>SBRE</td></tr>
    <tr><td>Web</td><td><a href="https://www.sabre.co.uk">www.sabre.co.uk</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2020 | 21.2 |
| 2019 | 12.8 |
| 2018 | 20.0 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
