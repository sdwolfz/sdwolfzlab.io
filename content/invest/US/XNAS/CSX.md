---
title: "CSX CORP (CSX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>CSX CORP</td></tr>
    <tr><td>Symbol</td><td>CSX</td></tr>
    <tr><td>Web</td><td><a href="https://www.csx.com">www.csx.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

| Year | Dividend per Share |
|------|--------------------|
| 2021 | 0.28 |
| 2020 | 1.04 |
| 2019 | 0.96 |
| 2018 | 0.88 |
| 2017 | 0.78 |
| 2016 | 0.72 |
| 2015 | 0.7 |
| 2014 | 0.63 |
| 2013 | 0.59 |
| 2012 | 0.54 |
| 2011 | 0.86 |
| 2010 | 0.98 |
| 2009 | 0.88 |
| 2008 | 0.77 |
| 2007 | 0.54 |
| 2006 | 0.46 |
| 2005 | 0.43 |
| 2004 | 0.4 |
| 2003 | 0.4 |
| 2002 | 0.4 |
| 2001 | 0.6 |
| 2000 | 0.6 |

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
