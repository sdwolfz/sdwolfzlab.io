---
title: "PIONEER MERGER CORP (PACX)"
type: share
---

## About
<table>
  <tbody>
    <tr><td>Geography</td><td><a href="/invest/us/">US</a></td></tr>
    <tr><td>Market</td><td><a href="/invest/us/xnas/">XNAS</a></td></tr>
    <tr><td>Name</td><td>PIONEER MERGER CORP</td></tr>
    <tr><td>Symbol</td><td>PACX</td></tr>
    <tr><td>Web</td><td><a href="https://www.pioneermerger.com">www.pioneermerger.com</a></td></tr>
  </tbody>
</table>

## Financials

### Dividends

This company does not pay any dividends!

### Reports

Not avaiable yet!

## Valuation

**Coming soon...**
